//
//  TribeHiostory.h
//  yun
//
//  Created by MacAir on 2017/6/6.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "BaseviewController.h"
#import "MyQuesModel.h"
#import "MessageModel.h"
#import "TribeHistoryCellStyleLeft.h"
@interface TribeHiostory : BaseviewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)MyQuesModel *model;
@end
