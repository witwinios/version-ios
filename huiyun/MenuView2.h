//
//  MenuView.h
//  
//
//  Created by 郑敏捷 on 17/3/1.
//  Copyright © 2017年 郑敏捷. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^vcBtnActionBlock)(NSInteger tag);

@interface MenuView2 : UIView

@property (copy  , nonatomic) vcBtnActionBlock   vcBtnBlock;

@property (strong, nonatomic)           NSArray *titleArray;

@property (strong, nonatomic, readonly) UIView  *lineView;

@end
