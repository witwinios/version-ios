//
//  TypicalCasesDetailsViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/12.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TypicalCasesDetailsViewController.h"

@interface TypicalCasesDetailsViewController (){
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *courseTable;
    NSMutableArray *dataArray;
    int currentPage;
    
    NSInteger line;
}

@end

@implementation TypicalCasesDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self setUpRefresh];
    [self setNav];
}
- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"病例记录";
    
    
    [backNavigation addSubview:titleLab];
    

    
}

- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUI{
    currentPage = 1;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    
    [courseTable registerNib:[UINib nibWithNibName:@"TypicalCasesDetailsCell" bundle:nil] forCellReuseIdentifier:@"DetailsCell"];
    [self.view addSubview:courseTable];
    
    dataArray=[NSMutableArray new];
    
}
-(void)setUpRefresh{
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中。。。";
}

//http://www.hzwitwin.cn:81/witwin-ctts-web/typicalCases/102/medicalRecords
-(void)loadData{
    currentPage=1;
    
    NSString *RecordId=[NSString stringWithFormat:@"%@",_Model.TypicalCasesId];
    
    NSString *Url=[NSString stringWithFormat:@"%@/typicalCases/%@/medicalRecords?pageStart=%d&pageSize=10",LocalIP,RecordId,currentPage];
    
    NSLog(@"%@",Url);
    
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        
        
       //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                    [Maneger showAlert:@"无病例记录!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    TypicalCasesDetailsModel *model=[TypicalCasesDetailsModel new];
                    model.RecordType=[dictionary objectForKey:@"recordType"];
                    model.RecordTime=[dictionary objectForKey:@"recordTime"];
                    model.RecordDescription=[dictionary objectForKey:@"description"];
                    
                    model.RecordFile=[NSMutableArray new];
                    
                    if(![[dictionary objectForKey:@"recordFile"] isKindOfClass:[NSNull class]]){
                        model.RecordFileName=[[dictionary objectForKey:@"recordFile"]objectForKey:@"fileName"];
                        model.RecordFileFormat=[[dictionary objectForKey:@"recordFile"]objectForKey:@"fileFormat"];
                        model.RecordUrl=[[dictionary objectForKey:@"recordFile"]objectForKey:@"fileUrl"];
                    }
                    
                
                    [dataArray addObject:model];
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
                
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}

-(void)loadRefresh{
    currentPage++;
    NSString *RecordId=[NSString stringWithFormat:@"%@",_Model.TypicalCasesId];
    
    NSString *Url=[NSString stringWithFormat:@"%@/typicalCases/%@/medicalRecords?pageStart=%d&pageSize=10",LocalIP,RecordId,currentPage];
    
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {

                 [Maneger showAlert:@"无更多病例记录!" andCurentVC:self];
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    TypicalCasesDetailsModel *model=[TypicalCasesDetailsModel new];
                    model.RecordType=[dictionary objectForKey:@"recordType"];
                    model.RecordTime=[dictionary objectForKey:@"recordTime"];
                    model.RecordDescription=[dictionary objectForKey:@"description"];
                    
                    model.RecordFile=[NSMutableArray new];
                    
                    if(![[dictionary objectForKey:@"recordFile"] isKindOfClass:[NSNull class]]){
                        model.RecordFileName=[[dictionary objectForKey:@"recordFile"]objectForKey:@"fileName"];
                        model.RecordFileFormat=[[dictionary objectForKey:@"recordFile"]objectForKey:@"fileFormat"];
                        model.RecordUrl=[[dictionary objectForKey:@"recordFile"]objectForKey:@"fileUrl"];
                    }
                    
                    [newData addObject:model];
                    
                }
                NSLog(@"newData=%lu",(unsigned long)newData.count);
                
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                
                
                [courseTable reloadData];
                
                
            }
        }
        else{
            currentPage--;
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}


#pragma mark---TableView
//行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

//点击Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
     UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"该功能正在开发" message:@"请持续关注！" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
     [AlertView addAction:BackAction];
     [self presentViewController:AlertView animated:YES completion:nil];
}

//加载的Cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TypicalCasesDetailsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"DetailsCell"];
    TypicalCasesDetailsModel *model=dataArray[indexPath.row];
    [cell setProperty:model];
    
    cell.TextView.userInteractionEnabled=NO;
    return  cell;
}
//返回行高：
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 155;
}


@end
