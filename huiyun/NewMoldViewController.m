//
//  NewMoldViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/27.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "NewMoldViewController.h"
#import "TCourseAddViewController.h"
@interface NewMoldViewController ()
{
    UITableView *courseTable;
    NSMutableArray *dataArray;
    int currentPage;
    
    NSInteger line;
    NSMutableArray *IDArr;
    NSMutableArray *NameArr;
}

@end

@implementation NewMoldViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    
    [self setUI];
    
    
    [self loadData];
}

- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"科目类型";
    [backNavigation addSubview:titleLab];
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)loadData{
    CourseMoldModel *Model=[CourseMoldModel new];
    

    NSLog(@"IDArr==%@",IDArr);
    NSLog(@"NameArr==%@",NameArr);
    


    
    
  
}

-(void)setUI{
    currentPage = 1;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    
    [courseTable registerNib:[UINib nibWithNibName:@"MoldCell" bundle:nil] forCellReuseIdentifier:@"cellStyle1"];
    [self.view addSubview:courseTable];
    currentPage=1;
    dataArray=[NSMutableArray new];
    IDArr=[NSMutableArray new];
    NameArr=[NSMutableArray new];
}
#pragma mark---TableView
//行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _DataArray.count;
}

//点击Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CourseMoldModel *model=_DataArray[indexPath.row];
    
    [self.delegate Moldtext:model.ChildMoldName MoldID:[NSString stringWithFormat:@"%@", model.ChildMoldId]];
  
  
    
    [self.navigationController popViewControllerAnimated:YES];
   
}




//加载的Cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MoldCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
    
    CourseMoldModel *model=_DataArray[indexPath.row];
    [cell setChilder:model];
    
    int Num=[model.ChildrenNum intValue];
    if (Num>0) {
        cell.RightBtn.hidden=NO;
    }else{
        cell.RightBtn.hidden=YES;
    }
    return  cell;
}
//返回行高：
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}




@end
