//
//  ControlManeger.h
//  yun
//
//  Created by MacAir on 2017/7/27.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TClassController.h"
#import "ListStudentController.h"
#import "ContentController.h"
#import "ExamPointController.h"
#import "ExamContentController.h"
#import "MarkTableController.h"
#import "AllStationController.h"
@interface ControlManeger : NSObject
@property (strong, nonatomic) TClassController *TClassVC;
@property (strong, nonatomic) ListStudentController *ListSVC;
@property (strong, nonatomic) ContentController *ContentVC;
@property (strong, nonatomic) ExamPointController *PointVC;
@property (strong, nonatomic) ExamContentController *ExamContentVC;
@property (strong, nonatomic) MarkTableController *MarkTableVC;
@property (strong, nonatomic) AllStationController *AllStationVC;
+ (id)share;
@end
