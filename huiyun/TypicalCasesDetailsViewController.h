//
//  TypicalCasesDetailsViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/12.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TypicalCasesDetailsModel.h"
#import "TypicalCasesDetailsCell.h"
#import "TypicalCasesModel.h"
#import "FileViewController.h"
@interface TypicalCasesDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,getter=isEditing) BOOL editing;
-(void)setEditing:(BOOL)editing animated:(BOOL)animated;

@property(nonatomic,strong)TypicalCasesModel *Model;

@end

