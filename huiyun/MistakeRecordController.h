//
//  MistakeRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DepartEntity.h"
#import "WXPPickerView.h"
@interface MistakeRecordController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PickerViewOneDelegate>
@property (weak, nonatomic) IBOutlet UITextField *oneField;
@property (weak, nonatomic) IBOutlet UITextField *twoField;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (weak, nonatomic) IBOutlet UIButton *departmentBtn;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview1;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview2;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)fileAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic)UIImagePickerController *imagePickController;
@property (weak, nonatomic) IBOutlet UIButton *fileAction;
@property (weak, nonatomic) IBOutlet UIButton *fileBtn;
- (IBAction)dateAction:(id)sender;
- (IBAction)saveAction:(id)sender;
- (IBAction)departmentAction:(id)sender;

@property (strong, nonatomic) CaseReuestModel *model;
@property (strong, nonatomic) CaseMistakeModel *myModel;
@end
