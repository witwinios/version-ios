//
//  ExamPointController.h
//  yun
//
//  Created by MacAir on 2017/7/19.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SkillModel.h"
#import "CallModel.h"
#import "ExamPointModel.h"
#import "ExamPointCell.h"
#import "ExamContentController.h"
@interface ExamPointController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) SkillModel *skillModel;
@property (strong, nonatomic) CallModel *callModel;

@property (strong, nonatomic) NSMutableArray *dataArray;
@end
