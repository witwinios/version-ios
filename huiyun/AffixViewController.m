//
//  AffixViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "AffixViewController.h"
#import "ApplyStudentCell.h"
@interface AffixViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *EditBtn;
    UIButton *SaveBtn;
    
    int currentPage;
    NSMutableArray *dataArray;
    
    
    
}
@property (nonatomic,retain)NSDictionary *itemDic; // 承载一个item上面显示的图片和文字
@property (nonatomic,retain)NSMutableArray *allDataArray; // 成方所有的item上显示的内容，其实就是盛放小字典
@end

@implementation AffixViewController



- (void)viewDidLoad {
    [super viewDidLoad];

    [self setNav];
    
    
    [self setUI];
    [self setUpRefresh];
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"附件查询";
    [backNavigation addSubview:titleLab];
    
    
    EditBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    EditBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [EditBtn setTitle:@"修改" forState:UIControlStateNormal];
    [EditBtn addTarget:self action:@selector(EditPlan:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:EditBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(EditSavePlan:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;
    
    
}

-(void)EditPlan:(UIButton *)sender{
    
}

-(void)EditSavePlan:(UIButton *)sender{
    
}

- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setUpRefresh{
    
    
    [self.MyCollectionView addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    self.MyCollectionView.headerPullToRefreshText = @"下拉刷新";
    self.MyCollectionView.headerReleaseToRefreshText = @"松开进行刷新";
    self.MyCollectionView.headerRefreshingText = @"刷新中。。。";
    [self.MyCollectionView headerBeginRefreshing];
    

}

-(void)loadData{
    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/files?pageStart=1&pageSize=999&currentPage=1",LocalIP,_scheduleIdString];
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
             [self.MyCollectionView headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        
        if([result[@"responseStatus"] isEqualToString:@"succeed"]){
            
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无附件!" andCurentVC:self];
                [self.MyCollectionView headerEndRefreshing];
            }else{
                NSLog(@"%@",result);
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    CourseAffixModel *model=[CourseAffixModel new];
                    
                    model.FileUrl=[dictionary objectForKey:@"fileUrl"];
                    
                    NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.FileUrl];
                    
                    model.FileUrl=url;
                    [dataArray addObject:model];
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [self.MyCollectionView reloadData];
                [self.MyCollectionView headerEndRefreshing];
            }
        }else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            NSLog(@"%@",result);
        }
        return;

    }];
}


-(void)setUI{
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    // 滚动方向
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.itemSize =CGSizeMake(150, 150);
    
    //设置CollectionView参数
    self.MyCollectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight - 64) collectionViewLayout:flowLayout];
    self.MyCollectionView.backgroundColor=[UIColor whiteColor];
    self.MyCollectionView.delegate=self;
    self.MyCollectionView.dataSource=self;
    self.MyCollectionView.scrollEnabled=YES;
    [self.view addSubview:self.MyCollectionView];
    
    [self.MyCollectionView registerClass:[ImgViewCell class] forCellWithReuseIdentifier:@"cell"];
 
    currentPage = 1;
    dataArray = [NSMutableArray new];
    
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark  设置CollectionView每组所包含的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return dataArray.count;
    
}

#pragma mark  设置CollectionCell的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify = @"cell";
    ImgViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
 
    CourseAffixModel *model=dataArray[indexPath.item];
    [cell setProperty:model];

    // 取出图片名称
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"点击了第 %ld 个图片",indexPath.item+1);
}

//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 130);
}

//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}



@end
