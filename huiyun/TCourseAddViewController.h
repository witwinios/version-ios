//
//  TCourseAddViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/10.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>

#import <MediaPlayer/MediaPlayer.h>
#import "Maneger.h"

#import "ZCAssetsPickerViewController.h"

#import "RequestTools.h"
#import "CourseAffixModel.h"
typedef void (^MyBasicBlock)(id result);
@interface TCourseAddViewController : UIViewController<ZCAssetsPickerViewControllerDelegate>


//课程名称：
@property (weak, nonatomic) IBOutlet UILabel *CourseNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *CourseName;

//课程类型：
@property (weak, nonatomic) IBOutlet UILabel *CourseMoldLabel;
@property (weak, nonatomic) IBOutlet UIButton *CourseMold;
- (IBAction)AddMoldAction:(id)sender;


//课程科目：
@property (weak, nonatomic) IBOutlet UILabel *CourseSubjectLabel;
@property (weak, nonatomic) IBOutlet UIButton *CourseSubject;
- (IBAction)AddSubjectAction:(id)sender;

//课程描述：
@property (weak, nonatomic) IBOutlet UITextView *CourExplain;
@property (weak, nonatomic) IBOutlet UILabel *stirngLenghLabel;

//上课时间
@property (weak, nonatomic) IBOutlet UILabel *StartTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *StartTime;
- (IBAction)StartAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *EndTime;
- (IBAction)EndAction:(id)sender;


//上课地点：
@property (weak, nonatomic) IBOutlet UILabel *LocationLabel;
@property (weak, nonatomic) IBOutlet UIButton *CourseLocation;
- (IBAction)AddLocationAction:(id)sender;


//附件：
@property (weak, nonatomic) IBOutlet UIButton *CourseOtherBtn;
- (IBAction)CourseOtherBtnAction:(id)sender;

//保存
@property (weak, nonatomic) IBOutlet UIButton *SaveBtn;
- (IBAction)SaveAction:(id)sender;

//展示
@property (weak, nonatomic) IBOutlet UIImageView *currentImage;


@property (nonatomic, copy) MyBasicBlock myBlock;
@property (strong, nonatomic)UIImagePickerController *imagePickController;
@end
