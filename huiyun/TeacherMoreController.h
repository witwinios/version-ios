//
//  TeacherMoreController.h
//  yun
//
//  Created by MacAir on 2017/8/28.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WXPPickerView.h"
#import "SchoolEntity.h"
typedef enum
{
    EditStayYes = 0,
    EditStayNo
}EditStay;//枚举
typedef void (^MyBasicBlock)(id result);
@interface TeacherMoreController : UIViewController<UITableViewDelegate,UITableViewDataSource,PickerViewOneDelegate>
@property (strong, nonatomic) AccountEntity *entity;
@property (nonatomic, copy) MyBasicBlock selfBlock;
@property(retain,nonatomic)UIView *mohuView;

@end
