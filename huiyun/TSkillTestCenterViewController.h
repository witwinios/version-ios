//
//  TSkillTestCenterViewController.h
//  huiyun
//
//  Created by Bad on 2018/3/23.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCell.h"
#import "TSkillTestCenterModel.h"
@interface TSkillTestCenterViewController : UIViewController

@property(nonatomic,strong)NSString *testId;

@end
