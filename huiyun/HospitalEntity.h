//
//  HospitalEntity.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/27.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HospitalEntity : NSObject<NSCoding>
@property (strong, nonatomic) NSString *hosIP;
@property (strong, nonatomic) NSString *hosName;
@end
