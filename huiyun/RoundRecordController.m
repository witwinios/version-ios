//
//  RoundRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "RoundRecordController.h"

@interface RoundRecordController ()
{
    UITableView *table;
    NSArray *dataArray;
}
@end

@implementation RoundRecordController
+ (id)shareObject{
    static RoundRecordController *recordVC = nil;
    if (recordVC == nil) {
        recordVC = [RoundRecordController new];
    }
    return recordVC;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    dataArray = self.departModel.SDepartRecordArray;
    [table reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    
}

- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"轮转记录";
    [backNavigation addSubview:titleLab];
    
    UILabel *keshiTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 64, Swidth/2, 50)];
    keshiTitle.text = @"所在科室:";
    [self.view addSubview:keshiTitle];
    
    UILabel *keshiContent = [[UILabel alloc]initWithFrame:CGRectMake(Swidth-20, 64, 10, 20)];
    keshiContent.center = CGPointMake(Swidth-20, keshiTitle.center.y);
    
    
    //
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64-200)];
    table.bounces = NO;
    table.delegate = self;
    table.dataSource = self;
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.backgroundColor = UIColorFromHex(0xF0F0F0);
    [table registerNib:[UINib nibWithNibName:@"RoundCell" bundle:nil] forCellReuseIdentifier:@"roundCell"];
    [table registerNib:[UINib nibWithNibName:@"RoundViewCell" bundle:nil] forCellReuseIdentifier:@"roundViewCell"];
    [self.view addSubview:table];
    //
    UIImageView *resultImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, Sheight-190, Swidth, 50)];
    resultImage.backgroundColor = [UIColor whiteColor];
    resultImage.userInteractionEnabled = YES;
    [self.view addSubview:resultImage];
    
    UILabel *resultLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100, 50)];
    resultLab.text = @"出科小结";
    resultLab.textColor = [UIColor lightGrayColor];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth-20, 15, 10, 20)];
    imgView.image = [UIImage imageNamed:@"youjiantou"];
    [resultImage addSubview:imgView];
    [resultImage addSubview:resultLab];
    //
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    tap.numberOfTapsRequired = 1;
    [resultImage addGestureRecognizer:tap];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
//出科小结
- (void)tapAction:(UITapGestureRecognizer *)tap{
//    PersonEntity *person = [[NSuserDefaultManager share] readCurrentUser];
    RecordResultController *resultVC = [RecordResultController new];
    resultVC.model = self.departModel;
    [self.navigationController pushViewController:resultVC animated:YES];
//    if (person.userID.intValue == self.departModel.teacherId.intValue) {
//        NSLog(@"%@",self.departModel.isSubmitTeacherSummary);
//        if (self.departModel.isSubmitTeacherSummary.intValue == 0) {
//            TeacherAdviceController *teaAdvice = [TeacherAdviceController new];
//            teaAdvice.model = self.departModel;
//            [self.navigationController pushViewController:teaAdvice animated:YES];
//        }else{
//            ResultSubmitedController *submitVC = [ResultSubmitedController new];
//            submitVC.departModel = self.departModel;
//            [self.navigationController pushViewController:submitVC animated:YES];
//        }
//        return;
//    }
//    if (person.userID.intValue == self.departModel.supervisorId.intValue) {
//        if (self.departModel.isSubmitSupervisorSummary.intValue == 0) {
//            SupervisorAdviceController *supAdvice = [SupervisorAdviceController new];
//            supAdvice.model = self.departModel;
//            [self.navigationController pushViewController:supAdvice animated:YES];
//        }else{
//            ResultSubmitedController *submitVC = [ResultSubmitedController new];
//            submitVC.departModel = self.departModel;
//            [self.navigationController pushViewController:submitVC animated:YES];
//        }
//        return;
//    }
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectRecordIndex = [NSNumber numberWithInteger:indexPath.row];
    
    if (indexPath.row > 2) {
        CaseHistoryController *hisVC = [CaseHistoryController shareObject];
        hisVC.from = @"teacher";
        [self.navigationController pushViewController:hisVC animated:YES];
    }else{
        CaseRequestController *hisVC = [CaseRequestController new];
        hisVC.model = dataArray[indexPath.row];
        hisVC.from = @"teacher";
        if (indexPath.row == 0) {
            hisVC.Num=@"0";
        }else if(indexPath.row == 1){
            hisVC.Num=@"1";
        }else{
            hisVC.Num=@"2";
        }
        [self.navigationController pushViewController:hisVC animated:YES];
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *head = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 100)];
    head.backgroundColor = [UIColor whiteColor];
    UILabel *keshiLab = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, Swidth/2-5, 50)];
    keshiLab.text = @"所在科室:";
    keshiLab.textColor = [UIColor lightGrayColor];
    keshiLab.textAlignment = 0;
    keshiLab.font = [UIFont systemFontOfSize:16];
    [head addSubview:keshiLab];
    
    UILabel *keshiContent = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2, 0, Swidth/2 - 10, 50)];
    keshiContent.font = [UIFont systemFontOfSize:12];
    keshiContent.text = self.departModel.SDepartName;
    keshiContent.textColor = [UIColor lightGrayColor];
    keshiContent.textAlignment = NSTextAlignmentCenter;
    [head addSubview:keshiContent];
    
    UIView *twoView = [[UIView alloc]initWithFrame:CGRectMake(0, 50, Swidth, 50)];
    twoView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [head addSubview:twoView];
    NSArray *array = @[@"记录名称",@"记录总数"];
    for (int i =0; i<2; i++) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake((Swidth-40)/2*i+i * 11, 0, (Swidth-40)/2, 50)];
        label.textAlignment = 1;
        label.text = array[i];
        label.font = [UIFont systemFontOfSize:24];
        label.textColor = [UIColor lightGrayColor];
        [twoView addSubview:label];
    }
    return head;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 100.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RoundViewCell *roundViewCell = [table dequeueReusableCellWithIdentifier:@"roundViewCell"];
    roundViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [roundViewCell setPro:indexPath.row Content:self.departModel.SDepartRecordArray];
    return roundViewCell;
}
@end


