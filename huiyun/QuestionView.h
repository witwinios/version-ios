//
//  QuestionView.h
//  yun
//
//  Created by MacAir on 2017/6/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionModel.h"
@interface QuestionView : UIView
@property (copy, nonatomic) QuestionModel *quesModel;
@property (strong, nonatomic)UILabel *quesTitle;
@property (strong, nonatomic)NSArray *choiceAnswer;
@property (strong, nonatomic)UIImageView *imgView;
    
@end
