//
//  CaseTreatModel.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseTreatModel.h"

@implementation CaseTreatModel
//@property (strong, nonatomic) NSString *caseTreatModelStatus;
- (void)setCaseTreatModelStatus:(NSString *)caseTreatModelStatus{
    if ([caseTreatModelStatus isKindOfClass:[NSNull class]]) {
        _caseTreatModelStatus = @"暂无";
    }else if ([caseTreatModelStatus isEqualToString:@"waiting_approval"]){
        _caseTreatModelStatus = @"待审核";
    }else if ([caseTreatModelStatus isEqualToString:@"approved"]){
        _caseTreatModelStatus = @"已通过";
    }else{
        _caseTreatModelStatus = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *caseTreatModelCategory;
- (void)setCaseTreatModelCategory:(NSString *)caseTreatModelCategory{
    if ([caseTreatModelCategory isKindOfClass:[NSNull class]]) {
        _caseTreatModelCategory = @"暂无";
    }else{
        _caseTreatModelCategory = caseTreatModelCategory;
    }
}
//@property (strong, nonatomic) NSNumber *caseTreatModelDate;
- (void)setCaseTreatModelDate:(NSNumber *)caseTreatModelDate{
    if ([caseTreatModelDate isKindOfClass:[NSNull class]]) {
        _caseTreatModelDate = @0;
    }else{
        _caseTreatModelDate = caseTreatModelDate;
    }
}
//@property (strong, nonatomic) NSString *caseTreatModelTeacher;
- (void)setCaseTreatModelTeacher:(NSString *)caseTreatModelTeacher{
    if ([caseTreatModelTeacher isKindOfClass:[NSNull class]]) {
        _caseTreatModelTeacher = @"暂无";
    }else{
        _caseTreatModelTeacher = caseTreatModelTeacher;
    }
}
//@property (strong, nonatomic) NSNumber *caseTreatModelTeacherId;
- (void)setCaseTreatModelTeacherId:(NSNumber *)caseTreatModelTeacherId{
    if ([caseTreatModelTeacherId isKindOfClass:[NSNull class]]) {
        _caseTreatModelTeacherId = @0;
    }else{
        _caseTreatModelTeacherId = caseTreatModelTeacherId;
    }
}
//@property (strong, nonatomic) NSString *caseTreatModelDescription;
- (void)setCaseTreatModelDescription:(NSString *)caseTreatModelDescription{
    if ([caseTreatModelDescription isKindOfClass:[NSNull class]]) {
        _caseTreatModelDescription = @"暂无";
    }else{
        _caseTreatModelDescription = caseTreatModelDescription;
    }
}
//@property (strong, nonatomic) NSString *caseTreatModelAdvice;
- (void)setCaseTreatModelAdvice:(NSString *)caseTreatModelAdvice{
    if ([caseTreatModelAdvice isKindOfClass:[NSNull class]]) {
        _caseTreatModelAdvice = @"暂无";
    }else{
        _caseTreatModelAdvice = caseTreatModelAdvice;
    }
}
//@property (strong, nonatomic) NSString *fileUrl;
- (void)setFileUrl:(NSString *)fileUrl{
    if ([fileUrl isKindOfClass:[NSNull class]]) {
        _fileUrl = @"暂无";
    }else{
        _fileUrl = fileUrl;
    }
}
@end
