//
//  MyButton.m
//  yun
//
//  Created by MacAir on 2017/6/13.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "MyButton.h"

@implementation MyButton

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self =[super initWithFrame:frame]) {
        self.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
        self.layer.borderWidth=1;
        self.layer.cornerRadius=10;
    }
    return self;
    
}

@end
