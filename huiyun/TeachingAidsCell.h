//
//  TeachingAidsCell.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeachingAidsModel.h"
@interface TeachingAidsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *AidsName;
@property (weak, nonatomic) IBOutlet UILabel *Repertory;
@property (weak, nonatomic) IBOutlet UILabel *Subscribe;
@property (weak, nonatomic) IBOutlet UILabel *Purpose;
@property (weak, nonatomic) IBOutlet UIButton *StatusBtn;
@property (weak, nonatomic) IBOutlet UIView *StatusView;

@property (weak, nonatomic) IBOutlet UILabel *Onelabel;
@property (weak, nonatomic) IBOutlet UILabel *TwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *ThreeLabel;
@property (weak, nonatomic) IBOutlet UILabel *FourLabel;


- (void)setProperty:(TeachingAidsModel *)model;


-(void)setModelAids:(TeachingAidsModel *)model;

@end
