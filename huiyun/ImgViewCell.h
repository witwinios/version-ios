//
//  ImgViewCell.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/25.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseAffixModel.h"
@interface ImgViewCell : UICollectionViewCell


@property(retain,nonatomic)UIImageView *imgView;



-(void)setProperty:(CourseAffixModel *)model;

@end
