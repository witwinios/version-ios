//
//  SDepartRecordModel.m
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SDepartRecordModel.h"

@implementation SDepartRecordModel
- (void)setSDepartRecordPendNum:(NSNumber *)SDepartRecordPendNum{
    if ([SDepartRecordPendNum isKindOfClass:[NSNull class]] || [SDepartRecordPendNum isEqual:@"(null)"]) {
        _SDepartRecordPendNum = @0;
    }else{
        _SDepartRecordPendNum = SDepartRecordPendNum;
    }
}
- (void)setSDepartRecordPassNum:(NSNumber *)SDepartRecordPassNum{
    if ([SDepartRecordPassNum isKindOfClass:[NSNull class]]) {
        _SDepartRecordPassNum = @0;
    }else{
        _SDepartRecordPassNum = SDepartRecordPassNum;
    }
}
- (void)setSDepartRecordReferNum:(NSNumber *)SDepartRecordReferNum{
    if ([SDepartRecordReferNum isKindOfClass:[NSNull class]]) {
        _SDepartRecordReferNum = @0;
    }else{
        _SDepartRecordReferNum = SDepartRecordReferNum;
    }
}
@end

