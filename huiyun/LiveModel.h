//
//  LiveModel.h
//  iOSZhongYe
//
//  Created by 郑敏捷 on 2017/5/21.
//  Copyright © 2017年 郑敏捷. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface LiveModel : NSObject

@property (copy, nonatomic) NSString *domain;

@property (copy, nonatomic) NSString *webcastID;

@property (copy, nonatomic) NSString *UserName;

@property (copy, nonatomic) NSString *LiveClassName;

@property (copy, nonatomic) NSString *watchPassword;

@property (copy, nonatomic) NSString *ServiceType;

@property (weak, nonatomic) UIView   *fatherView;

@end
