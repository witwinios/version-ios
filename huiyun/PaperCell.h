//
//  PaperCell.h
//  xiaoyun
//
//  Created by MacAir on 17/2/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaperCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *paperStatus;
@property (weak, nonatomic) IBOutlet UILabel *totalScore;
@property (weak, nonatomic) IBOutlet UITextView *paperDes;
@property (weak, nonatomic) IBOutlet UILabel *paperCellCategory;
@property (weak, nonatomic) IBOutlet UILabel *paperCellName;
@property (weak, nonatomic) IBOutlet UILabel *paperCellSubject;
@property (weak, nonatomic) IBOutlet UILabel *getScoreLab;

@end
