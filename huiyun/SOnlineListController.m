//
//  SOnlineListController.m
//  huiyun
//
//  Created by MacAir on 2017/11/27.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SOnlineListController.h"

@interface SOnlineListController ()
{
    NSMutableArray *_dataArray;
    NSMutableArray *_heightArray;
    int currentPage;
}
@end

@implementation SOnlineListController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPage = 1;
    self.titles = @"在线考试";
    _dataArray = [NSMutableArray new];
    _heightArray = [NSMutableArray new];
    //table
    [self.table addHeaderWithTarget:self action:@selector(downRefresh)];
    [self.table addFooterWithTarget:self action:@selector(loadRefresh)];
    //刷新
    [self.table headerBeginRefreshing];
    
}
- (void)downRefresh{
    currentPage = 1;
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@/testSchedules?pageStart=1&pageSize=15&studentId=%@",LocalIP,LocalUserId];

    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        [self.table headerEndRefreshing];
        
        NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
        
        if (array.count ==0) {
            [MBProgressHUD showToastAndMessage:@"暂无在线考试~" places:0 toView:nil];
        }else{
            [_dataArray removeAllObjects];
            for (int i = 0; i<array.count; i++) {
                onlineModel *model = [onlineModel new];
                NSDictionary *objectDic = array[i];
                model.onlineId = [objectDic objectForKey:@"scheduleId"];
                model.onlineName = [objectDic objectForKey:@"testName"];
                model.onlineRoom = [objectDic objectForKey:@"testRoomName"];
                model.onlineInternal = [objectDic objectForKey:@"testDuration"];
                model.onlineTime = [objectDic objectForKey:@"testTime"];
                model.type = @"online_test";
                model.onlinePages = [objectDic objectForKey:@"testPapersNum"];
                model.onlineDes = [objectDic objectForKey:@"description"];
                model.onlineStatus = [objectDic objectForKey:@"testStatus"];
                model.createTime = [objectDic objectForKey:@"createdTime"];
                model.createName = [objectDic objectForKey:@"createdBy"];
                model.difficulty = [objectDic objectForKey:@"difficulty"];
                CGFloat row_height = [Maneger getPonentH:model.onlineName andFont:[UIFont systemFontOfSize:17] andWidth:Swidth-180]+104;
                [_heightArray addObject:[NSNumber numberWithFloat:row_height]];
                [_dataArray addObject:model];
            }
            [self.table reloadData];
        }

    } failed:^(NSString *result) {
        [self.table headerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"获取失败~" places:0 toView:nil];
    }];
}
- (void)loadRefresh{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/testSchedules?pageStart=%d&pageSize=15&studentId=%@",LocalIP,++currentPage,LocalUserId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        [self.table headerEndRefreshing];
        
        NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
        
        if (array.count ==0) {
            [MBProgressHUD showToastAndMessage:@"无更多~" places:0 toView:nil];
        }else{
            NSMutableArray *muArr = [NSMutableArray new];
            for (int i = 0; i<array.count; i++) {
                onlineModel *model = [onlineModel new];
                NSDictionary *objectDic = array[i];
                model.onlineId = [objectDic objectForKey:@"scheduleId"];
                model.onlineName = [objectDic objectForKey:@"testName"];
                model.onlineRoom = [objectDic objectForKey:@"testRoomName"];
                model.onlineInternal = [objectDic objectForKey:@"testDuration"];
                model.onlineTime = [objectDic objectForKey:@"testTime"];
                model.type = @"online_test";
                model.onlinePages = [objectDic objectForKey:@"testPapersNum"];
                model.onlineDes = [objectDic objectForKey:@"description"];
                model.onlineStatus = [objectDic objectForKey:@"testStatus"];
                model.createTime = [objectDic objectForKey:@"createdTime"];
                model.createName = [objectDic objectForKey:@"createdBy"];
                model.difficulty = [objectDic objectForKey:@"difficulty"];
                [muArr addObject:model];
            }
            NSRange range = NSMakeRange(_dataArray.count, muArr.count);
            NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
            [_dataArray insertObjects:muArr atIndexes:set];
            self.table.contentOffset = CGPointMake(self.table.contentOffset.x, self.table.contentOffset.y+35);
            [self.table reloadData];
        }
        
    } failed:^(NSString *result) {
        currentPage --;
        [self.table headerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"获取失败~" places:0 toView:nil];
    }];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [_heightArray[indexPath.row] floatValue];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    onlineModel *model = _dataArray[indexPath.row];
    if ([model.onlineStatus isEqualToString:@"已开始"]) {
        SDOExamController *examVC = [SDOExamController new];
        examVC.model = _dataArray[indexPath.row];
        [self.navigationController pushViewController:examVC animated:YES];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OsceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"osceCell"];
    [cell set:_dataArray[indexPath.row]];
    return cell;
}

@end
