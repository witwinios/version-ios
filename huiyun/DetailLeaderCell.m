//
//  DetailLeaderCell.m
//  huiyun
//
//  Created by MacAir on 2018/1/17.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "DetailLeaderCell.h"

@implementation DetailLeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentText.layer.cornerRadius = 5;
    self.contentText.layer.masksToBounds = YES;
    self.contentText.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.contentText.layer.borderWidth = 1;
}

@end
