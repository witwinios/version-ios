//
//  AffixViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImgViewCell.h"
#import "CourseAffixModel.h"
@interface AffixViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>


@property(nonatomic,strong) NSString *scheduleIdString;



@property(nonatomic,strong)UICollectionView *MyCollectionView;
@end
