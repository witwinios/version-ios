//
//  CourseQuestionViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "CourseQuestionViewController.h"
#import "CourseQuestionModel.h"
#import "CourseQuestionCell.h"
#import "CourseQuestionAddViewController.h"
#import "CourseQuestionDetailsView.h"
@interface CourseQuestionViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *courseTable;
    NSMutableArray *dataArray;
    int currentPage;
    
    NSInteger line;
    
    NSString *PlanId;
    NSMutableArray *AllArr;
    
    NSMutableArray *QuestionIdArr;
    
    NSMutableArray *TempArray;
}

@end

@implementation CourseQuestionViewController


-(void)viewWillAppear:(BOOL)animated{

    if ([_lastStr isEqualToString:@"教案"]) {
         [self loadData];
    }

   
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self setNav];
    [self setUpRefresh];
    
    QuestionIdArr=[NSMutableArray new];
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    if ([_str isEqualToString:@"课前"]) {
        titleLab.text = @"课前课程题目";
    }else  if ([_str isEqualToString:@"课后"]) {
        titleLab.text = @"课后课程题目";
    }

    [backNavigation addSubview:titleLab];
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (_PlanModel.PlanOwnerId.intValue == persion.userID.intValue ) {
     if([_PlanModel.PlanCourseStatus isEqualToString:@"in_design"]){
    if ([_courseStr isEqualToString:@"教案"]) {
        rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
        [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
        [rightBtn addTarget:self action:@selector(Choice:) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:rightBtn];
        
        SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
        [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
        [SaveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:SaveBtn];
        SaveBtn.hidden=YES;
    }
     }
    }
    
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)Choice:(id)sender{
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"课程题目管理" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *AddAction = [UIAlertAction actionWithTitle:@"新增" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([_str isEqualToString:@"课前"]) {
            CourseQuestionAddViewController *vc=[[CourseQuestionAddViewController alloc]init];
            vc.PlanModel=_PlanModel;
           
            vc.TempArr=AllArr;
            vc.str=@"课前";
            vc.TempArray=TempArray;
            vc.backArr=^(NSArray *addArr ,NSString *addStr){
                dataArray = [NSMutableArray arrayWithArray:addArr];
                [courseTable reloadData];
                _lastStr=addStr;
            };
            
            
            [self.navigationController pushViewController:vc animated:YES];
        }else  if ([_str isEqualToString:@"课后"]) {
            CourseQuestionAddViewController *vc=[[CourseQuestionAddViewController alloc]init];
            vc.PlanModel=_PlanModel;
            vc.TempArr=AllArr;
            vc.str=@"课后";
            
            vc.backArr=^(NSArray *addArr,NSString *addStr){
                dataArray = [NSMutableArray arrayWithArray:addArr];
                [courseTable reloadData];
                _lastStr=addStr;
            };
            
            [self.navigationController pushViewController:vc animated:YES];
        }

    }];
    UIAlertAction *EditAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [courseTable setEditing:YES animated:YES];
        rightBtn.hidden=YES;
        SaveBtn.hidden=NO;
        _editing=1;
        
    }];
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
    [AlertView addAction:BackAction];
    [AlertView addAction:AddAction];
    [AlertView addAction:EditAction];
    
    [self presentViewController:AlertView animated:YES completion:nil];
}
-(void)save{
    [courseTable setEditing:NO animated:YES];
    rightBtn.hidden=NO;
    SaveBtn.hidden=YES;
}
-(void)setUI{
    currentPage = 1;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    
    [courseTable registerNib:[UINib nibWithNibName:@"CourseQuestionCell" bundle:nil] forCellReuseIdentifier:@"CourseQuestion"];
    [self.view addSubview:courseTable];
    _editing=0;
    dataArray=[NSMutableArray new];
    AllArr=[NSMutableArray new];
    if ([_courseStr isEqualToString:@"教案"]) {
          PlanId=[NSString stringWithFormat:@"%@",_PlanModel.PlanNameId];
    }else if([_courseStr isEqualToString:@"课程"]){
          PlanId=[NSString stringWithFormat:@"%@",_model.courseId];
    }
  
   
}
-(void)setUpRefresh{
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
//    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
//    courseTable.footerPullToRefreshText = @"上拉加载";
//    courseTable.footerReleaseToRefreshText = @"松开进行加载";
//    courseTable.footerRefreshingText = @"加载中。。。";
}
//http://www.hzwitwin.cn:81/witwin-ctts-web/courses/536/previewQuestions?showAnswers=true&pageSize=999&topLevelOnly=true 课前
//http://www.hzwitwin.cn:81/witwin-ctts-web/courses/535/homeworkQuestions?showAnswers=true&pageSize=999&topLevelOnly=true 课后
-(void)loadData{
    currentPage=1;
 
    NSString *Url;
    
    if ([_str isEqualToString:@"课前"]) {
        Url=[NSString stringWithFormat:@"%@/courses/%@/previewQuestions?pageStart=%d&showAnswers=true&pageSize=999&topLevelOnly=true",LocalIP,PlanId,currentPage];
    }else  if ([_str isEqualToString:@"课后"]) {
        Url=[NSString stringWithFormat:@"%@/courses/%@/homeworkQuestions?pageStart=%d&showAnswers=true&pageSize=999&topLevelOnly=true",LocalIP,PlanId,currentPage];
    }
    NSLog(@"%@",Url);
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        [AllArr removeAllObjects];
        [TempArray removeAllObjects];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                
                if ([_str isEqualToString:@"课前"]) {
                     [Maneger showAlert:@"无课前答题!" andCurentVC:self];
                }else  if ([_str isEqualToString:@"课后"]) {
                     [Maneger showAlert:@"无课后答题!" andCurentVC:self];
                }
                
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                   
                    CourseQuestionModel *model=[CourseQuestionModel new];
                    model.QuestionId=[dictionary objectForKey:@"questionId"];
                    model.QuestionType=[dictionary objectForKey:@"questionType"];
                    model.Difficulty=[dictionary objectForKey:@"score"];
                    model.ChildrenQuestionsNum=[dictionary objectForKey:@"childrenQuestionsNum"];
                    model.QuestionTitle=[dictionary objectForKey:@"questionTitle"];
                    model.ScoreNum=[NSString stringWithFormat:@"%@",model.Difficulty];
        
    
                    [dataArray addObject:model];
                
        
                    NSString *scroce=[NSString stringWithFormat:@"%lf",[model.ScoreNum doubleValue]];
                    NSMutableDictionary *Params=[NSMutableDictionary dictionary];
                    if ([_courseStr isEqualToString:@"教案"]) {
                         [Params setObject:_PlanModel.PlanNameId forKey:@"courseId"];
                    }else if([_courseStr isEqualToString:@"课程"]){
                         [Params setObject:_model.courseId forKey:@"courseId"];
                    }
                   
                    [Params setObject:model.QuestionId forKey:@"questionId"];
                    [Params setObject:scroce forKey:@"score"];
                    [AllArr addObject:Params];
                    
                    [TempArray addObject:model.QuestionId];
                    
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
              
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
                
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
    
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
    
    
}

-(void)loadRefresh{
    currentPage++;
    
    NSString *Url;
    
    if ([_str isEqualToString:@"课前"]) {
        Url=[NSString stringWithFormat:@"%@/courses/%@/previewQuestions?pageStart=%d&showAnswers=true&pageSize=15&topLevelOnly=true",LocalIP,PlanId,currentPage];
    }else  if ([_str isEqualToString:@"课后"]) {
        Url=[NSString stringWithFormat:@"%@/courses/%@/homeworkQuestions?pageStart=%d&showAnswers=true&pageSize=15&topLevelOnly=true",LocalIP,PlanId,currentPage];
    }
    NSLog(@"%@",Url);
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                
                if ([_str isEqualToString:@"课前"]) {
                    [Maneger showAlert:@"无课前答题!" andCurentVC:self];
                }else  if ([_str isEqualToString:@"课后"]) {
                    [Maneger showAlert:@"无课后答题!" andCurentVC:self];
                }
                
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    CourseQuestionModel *model=[CourseQuestionModel new];
                    model.QuestionId=[dictionary objectForKey:@"questionId"];
                    model.QuestionType=[dictionary objectForKey:@"questionType"];
                    model.Difficulty=[dictionary objectForKey:@"score"];
                    model.ChildrenQuestionsNum=[dictionary objectForKey:@"childrenQuestionsNum"];
                    model.QuestionTitle=[dictionary objectForKey:@"questionTitle"];
                    model.ScoreNum=[NSString stringWithFormat:@"%@",model.Difficulty];
                
                    
                    

                    
                    

                    [newData addObject:model];
                    
                    NSMutableDictionary *Params=[NSMutableDictionary dictionary];
                    [Params setObject:_PlanModel.PlanNameId forKey:@"courseId"];
                    [Params setObject:model.QuestionId forKey:@"questionId"];
                    [Params setObject:model.ScoreNum forKey:@"score"];
                    
                    [AllArr addObject:Params];
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                
                
                [courseTable reloadData];
                
                
            }
        }
        else{
            currentPage--;
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}


#pragma mark---TableView
//行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
     NSLog(@"count==%lu",dataArray.count);
    return dataArray.count;
}

//点击Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (!_editing) {
        CourseQuestionModel *model=dataArray[indexPath.row];
        if ([model.QuestionType isEqualToString:@"A1:单句型题"]||[model.QuestionType isEqualToString:@"A2:病例摘要题"] ||[model.QuestionType isEqualToString:@"X型多选题"]) {
            CourseQuestionDetailsView *vc =[[CourseQuestionDetailsView alloc]init];
            vc.AfterModel=model;
            [self.navigationController pushViewController:vc animated:YES];
        }else if([model.QuestionType isEqualToString:@"A3:病例组型题"] || [model.QuestionType isEqualToString:@"A4:病例串型题"] || [model.QuestionType isEqualToString:@"B1:标准配伍题型"]){
            CourseQuestionDetailsController *vc=[CourseQuestionDetailsController new];
             vc.AfterModel=model;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
    
}

//加载的Cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
CourseQuestionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CourseQuestion"];

    CourseQuestionModel *model=dataArray[indexPath.row];
    [cell setProperty:model];
   // cell.CourseQuestionStem.userInteractionEnabled=NO;
    cell.CourseQuestionStem.editable=NO;
    cell.TwoLable.text=@"题目分数:";
    cell.ThreeLable.text=@"子题数:";
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 188.f;
}
#pragma 编辑：
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return   UITableViewCellEditingStyleDelete;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"你确定删除该题目？" preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            CourseQuestionModel *model=dataArray[indexPath.row];
            
            [dataArray removeObjectAtIndex:indexPath.row];
            [AllArr removeObjectAtIndex:indexPath.row];
            [courseTable beginUpdates];
            [courseTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            
            [self DeleteDate:model QuestionId:[NSString stringWithFormat:@"%@",model.QuestionId]];
            [courseTable endUpdates];
            
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
        }]];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

//设置进入编辑状态时，Cell不会缩进

- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return NO;
    
}
// /courses/{courseId}/previewQuestions/{questionId}
-(void)DeleteDate:(CourseQuestionModel *)model QuestionId:(NSString *)QuestionId{
    
    NSString *Url;
    
    if ([_str isEqualToString:@"课前"]) {
        Url=[NSString stringWithFormat:@"%@/courses/%@/previewQuestions/%@",LocalIP,PlanId,QuestionId];
    }else  if ([_str isEqualToString:@"课后"]) {
        Url=[NSString stringWithFormat:@"%@/courses/%@/homeworkQuestions/%@",LocalIP,PlanId,QuestionId];
    }
    
    NSLog(@"%@",Url);
    
    
    [RequestTools RequestWithURL:Url Method:@"DELETE" Params:nil Success:^(NSDictionary *result) {
        
        
        
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            
            [MBProgressHUD showToastAndMessage:@"题目成功删除!" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}
@end
