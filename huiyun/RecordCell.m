//
//  RecordCell.m
//  huiyun
//
//  Created by MacAir on 2017/9/15.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "RecordCell.h"

@implementation RecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.status.layer.cornerRadius = 5;
    self.status.layer.masksToBounds = YES;
    if ([self.status.text isEqualToString:@"待审核"]) {
        self.status.backgroundColor = [UIColor redColor];
    }else{
        self.status.backgroundColor = [UIColor greenColor];
    }
    self.twolineLab.adjustsFontSizeToFitWidth = YES;
    self.dateContent.adjustsFontSizeToFitWidth = YES;
}
@end
