//
//  TeacherPlanController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/30.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TeacherPlanController.h"
#import "TeacherPlanCell.h"
#import "TCourseAddModel.h"
#import "TCaddNewPlanViewController.h"
@interface TeacherPlanController (){
    UITableView *courseTable;
    
    NSMutableArray *dataArray;
    NSMutableArray *resultArray;
    NSString *PlanId;
    NSString *PlanName;
    UIButton *rightBtn;
    UIButton *MyPlan;
    NSInteger line;
    
    int currentPage;
    NSMutableArray  *searchArray;
    UISearchBar *searchBar;
    NSMutableArray *tempArray;
    NSMutableArray *selectArray;
    
    BOOL isSearch;
    
    NSInteger IndexNum;
    UIButton *SaveBtn;
    

    UIView *SearchView;
    UITextField *SearchField;
    UIButton *SearchBtn;
    
    UIImageView *backNavigation;
    
}

@end

@implementation TeacherPlanController


-(void)viewWillAppear:(BOOL)animated{
    [self setUpRefresh];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setsearchBar];
    [self setNav];
    dataArray = [NSMutableArray new];
    resultArray = [NSMutableArray new];
    
    [self loadData];
    [self setUI];
   
   
    //添加搜索框
    
    //添加上拉加载刷新
    [self setUpRefresh];
    
    
}
- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    MyPlan=[UIButton buttonWithType:UIButtonTypeCustom];
    MyPlan.frame=CGRectMake(0, 25, 200, 25);
    MyPlan.center=CGPointMake(backNavigation.center.x, leftBtn.center.y);
    
    
    if ([_TempStr isEqualToString:@"1"]) {
         [MyPlan setTitle:@"全部的教案" forState:UIControlStateNormal];
    }else{
         [MyPlan setTitle:@"全部的教案" forState:UIControlStateNormal];
    }
    
   
    
    MyPlan.titleLabel.textAlignment=1;
    [MyPlan setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [MyPlan addTarget:self action:@selector(MyPlanAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:MyPlan];
    
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-100, 29.5, 100, 25);
    [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(AddBtn) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;
    
    
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)AddBtn{
     TCourseAddModel *model = [TCourseAddModel new];
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"教案管理" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *AddAction = [UIAlertAction actionWithTitle:@"新增" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        TCaddNewPlanViewController *vc=[[TCaddNewPlanViewController alloc]init];
        vc.Condition=@"YES";
        [self.navigationController pushViewController:vc animated:YES];
        
        
    }];
    UIAlertAction *EditAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [courseTable setEditing:YES animated:YES];
        rightBtn.hidden=YES;
        SaveBtn.hidden=NO;
        _editing=1;
        
    }];
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
    [AlertView addAction:BackAction];
    [AlertView addAction:AddAction];
    [AlertView addAction:EditAction];
    
 
    
    [self presentViewController:AlertView animated:YES completion:nil];
    
}

-(void)save{
    [courseTable setEditing:NO animated:YES];
    rightBtn.hidden=NO;
    SaveBtn.hidden=YES;
    isSearch=NO;
    _editing=0;
}

- (void)setUpRefresh{
    
    
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中。。。";

}

// NSString *Url=[NSString stringWithFormat:@"%@/courses?courseName=%@",LocalIP,str];
-(void)loadData{
     currentPage=1;
     NSString *URL;
    
    if (isSearch == YES) {
         NSString *str=SearchField.text;
        if ([_TempStr isEqualToString:@"1"]) {
            URL=[NSString stringWithFormat:@"%@/courses?pageSize=999&pageStart=%d&courseStatus=released&courseName=%@",LocalIP,currentPage,str];
            URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }else{
            switch (IndexNum) {
                case 0:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=999&pageStart=%d&courseName=%@",LocalIP,currentPage,str];
                    URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    break;
                case 1:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=999&pageStart=%d&courseStatus=in_design&courseName=%@",LocalIP,currentPage,str];
                    URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    break;
                case 2:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=999&pageStart=%d&courseStatus=released&courseName=%@",LocalIP,currentPage,str];
                    URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    break;
                default:
                    break;
            }
        }
    }else if(isSearch == NO){
        if ([_TempStr isEqualToString:@"1"]) {
            URL=[NSString stringWithFormat:@"%@/courses?pageSize=20&pageStart=%d&courseStatus=released",LocalIP,currentPage];
        }else{
            switch (IndexNum) {
                case 0:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=20&pageStart=%d",LocalIP,currentPage];
                    break;
                case 1:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=20&pageStart=%d&courseStatus=in_design",LocalIP,currentPage];
                    break;
                case 2:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=20&pageStart=%d&courseStatus=released",LocalIP,currentPage];
                    break;
                default:
                    break;
            }
        }
    }
    
    
   
    

  
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {

        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        [resultArray removeAllObjects];
        
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无教案!" andCurentVC:self];
                
                if (isSearch) {
                    isSearch=NO;
                }
                
                
                switch (IndexNum) {
                    case 0:
                        [MyPlan setTitle:@"全部的教案▼" forState:UIControlStateNormal];
                        break;
                    case 1:
                        [MyPlan setTitle:@"计划中的教案▼" forState:UIControlStateNormal];
                        break;
                    case 2:
                        [MyPlan setTitle:@"已发布的教案▼" forState:UIControlStateNormal];
                        break;
                    default:
                        break;
                }
                
                
                [courseTable reloadData];
                
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    TCourseAddModel *model = [TCourseAddModel new];
                    
                    
                    model.PlanName=[dictionary objectForKey:@"courseName"];
                    model.PlanClassify=[dictionary objectForKey:@"categoryName"];
                    model.PlanClassifyId=[dictionary objectForKey:@"categoryId"];
                    model.PlanCourse=[dictionary objectForKey:@"subjectName"];
                    model.PlanCourseId=[dictionary objectForKey:@"subjectId"];
                    model.PlanOwner=[dictionary objectForKey:@"ownerName"];
                    model.PlanOwnerId=[dictionary objectForKey:@"ownerId"];
                    model.PlanNameId=[dictionary objectForKey:@"courseId"];
                    model.PlanGenre=[dictionary objectForKey:@"courseType"];
                    model.planGenreId=[dictionary objectForKey:@"courseType"];
                    model.PlanDescription=[dictionary objectForKey:@"description"];
                    model.PlanPublic=[dictionary objectForKey:@"isPublic"];
                    model.PlanNameId=[dictionary objectForKey:@"courseId"];
                    model.PlanCourseStatus=[dictionary objectForKey:@"courseStatus"];
                    model.CourseStatus=[dictionary objectForKey:@"courseStatus"];
                    
                    [dataArray addObject:model];
                    [resultArray addObject:model];
                    
                    
                    
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
                
                switch (IndexNum) {
                    case 0:
                        [MyPlan setTitle:@"全部的教案▼" forState:UIControlStateNormal];
                        break;
                    case 1:
                        [MyPlan setTitle:@"计划中的教案▼" forState:UIControlStateNormal];
                        break;
                    case 2:
                        [MyPlan setTitle:@"已发布的教案▼" forState:UIControlStateNormal];
                        break;
                    default:
                        break;
                }

                
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }

    } failed:^(NSString *result) {
        if (![result isEqualToString:@"200"]) {
            [courseTable headerEndRefreshing];
        }
        return;

    }];

}


-(void)loadRefresh{
    currentPage++;
    NSString *URL;
    
    if (isSearch == YES) {
        NSString *str=SearchField.text;
        if ([_TempStr isEqualToString:@"1"]) {
            URL=[NSString stringWithFormat:@"%@/courses?pageSize=999&pageStart=%d&courseStatus=released&courseName=%@",LocalIP,currentPage,str];
            URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }else{
            switch (IndexNum) {
                case 0:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=999&pageStart=%d&courseName=%@",LocalIP,currentPage,str];
                    URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    break;
                case 1:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=999&pageStart=%d&courseStatus=in_design&courseName=%@",LocalIP,currentPage,str];
                    URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    break;
                case 2:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=999&pageStart=%d&courseStatus=released&courseName=%@",LocalIP,currentPage,str];
                    URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    break;
                default:
                    break;
            }
        }
    }else if(isSearch == NO){
        if ([_TempStr isEqualToString:@"1"]) {
            URL=[NSString stringWithFormat:@"%@/courses?pageSize=20&pageStart=%d&courseStatus=released",LocalIP,currentPage];
        }else{
            switch (IndexNum) {
                case 0:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=20&pageStart=%d",LocalIP,currentPage];
                    break;
                case 1:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=20&pageStart=%d&courseStatus=in_design",LocalIP,currentPage];
                    break;
                case 2:
                    URL=[NSString stringWithFormat:@"%@/courses?pageSize=20&pageStart=%d&courseStatus=released",LocalIP,currentPage];
                    break;
                default:
                    break;
            }
        }
    }
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];

        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无教案!" andCurentVC:self];
                
                
                if (isSearch) {
                    isSearch=NO;
                }
                
                switch (IndexNum) {
                    case 0:
                        [MyPlan setTitle:@"全部的教案▼" forState:UIControlStateNormal];
                        break;
                    case 1:
                        [MyPlan setTitle:@"计划中的教案▼" forState:UIControlStateNormal];
                        break;
                    case 2:
                        [MyPlan setTitle:@"已发布的教案▼" forState:UIControlStateNormal];
                        break;
                    default:
                        break;
                }
                
                
                
                [courseTable reloadData];
                
                [courseTable footerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    TCourseAddModel *model = [TCourseAddModel new];
                   
                    
                    model.PlanName=[dictionary objectForKey:@"courseName"];
                    model.PlanClassify=[dictionary objectForKey:@"categoryName"];
                    model.PlanClassifyId=[dictionary objectForKey:@"categoryId"];
                    model.PlanCourse=[dictionary objectForKey:@"subjectName"];
                    model.PlanCourseId=[dictionary objectForKey:@"subjectId"];
                    model.PlanOwner=[dictionary objectForKey:@"ownerName"];
                    model.PlanOwnerId=[dictionary objectForKey:@"ownerId"];
                    model.PlanNameId=[dictionary objectForKey:@"courseId"];
                    model.PlanGenre=[dictionary objectForKey:@"courseType"];
                    model.planGenreId=[dictionary objectForKey:@"courseType"];
                    model.PlanDescription=[dictionary objectForKey:@"description"];
                    model.PlanPublic=[dictionary objectForKey:@"isPublic"];
                    model.PlanNameId=[dictionary objectForKey:@"courseId"];
                    model.PlanCourseStatus=[dictionary objectForKey:@"courseStatus"];
                    
                    [newData addObject:model];

                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                [courseTable reloadData];
            }
        }
        else{
            currentPage--;
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [courseTable headerEndRefreshing];
        }
        return;
        
    }];
    
}


-(void)setUI{
    currentPage = 1;
    CGFloat courseTable_Y=backNavigation.wwy_height+SearchView.wwy_height;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, courseTable_Y,Swidth, Sheight-courseTable_Y) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    [courseTable registerNib:[UINib nibWithNibName:@"TeacherPlanCell" bundle:nil] forCellReuseIdentifier:@"PlanCell"];
    [self.view addSubview:courseTable];
    IndexNum =0;
    isSearch=NO;
    _editing=0;
}


-(void)setsearchBar{
    
    
    tempArray=[NSMutableArray array];
    selectArray=[NSMutableArray array];
    isSearch=NO;
    
    SearchView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 44)];
    SearchView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    UIImageView *SearchImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sousuo"]];
    SearchImg.backgroundColor=[UIColor clearColor];
    SearchImg.frame=CGRectMake(10, 6, 32, 32);
    
    
    SearchField=[[UITextField alloc]initWithFrame:CGRectMake(44, 6, Swidth-88, 32)];
    SearchField.backgroundColor=[UIColor clearColor];
    SearchField.placeholder=@"请输入教案名称";
    SearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    SearchField.clearsOnBeginEditing = YES;
    SearchField.keyboardType=UIKeyboardTypeDefault;
    SearchField.returnKeyType=UIReturnKeySearch;
    SearchField.delegate = self;
    [SearchField addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    SearchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SearchBtn.frame=CGRectMake((SearchField.wwy_x+SearchField.wwy_width)+2, 6, 40, 32);
    [SearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [SearchBtn addTarget:self action:@selector(setUpRefresh) forControlEvents:UIControlEventTouchUpInside];
    SearchBtn.backgroundColor=UIColorFromHex(0x20B2AA);
    SearchBtn.layer.cornerRadius=5;
    SearchBtn.hidden=YES;
    [SearchView addSubview:SearchBtn];
    [SearchView addSubview:SearchField];
    [SearchView addSubview:SearchImg];
    [self.view addSubview:SearchView];
 
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [SearchField  resignFirstResponder];
    [self setUpRefresh ];
    NSLog(@"点击了搜索");
    
    return YES;
    
}


-(BOOL)ApplyTextDidChange:(UITextField *)theTextField{
    
    NSLog( @"text changed: %@", theTextField.text);
    
    
    if (![theTextField.text isEqualToString:@""]) {
        SearchBtn.hidden=NO;
        isSearch=YES;
    }else{
        isSearch=NO;
        [self setUpRefresh];
        SearchBtn.hidden=YES;
        [SearchField  resignFirstResponder];
    }
    
    return YES;
    
}



//状态筛选：
-(void)MyPlanAction:(UIButton *)sender{
    
    CGPoint point = CGPointMake(sender.center.x, sender.center.y+16);
    CustomPopview *popView = [[CustomPopview alloc]initWithOrigin:point Width:130 Height:120 Type:XTTypeOfUpCenter Color:[UIColor whiteColor]];
    popView.dataArray = @[@"全部",@"计划中",@"已发布"];
    popView.fontSize = 18;
    popView.row_height = 40;
    popView.delegate = self;
    [popView popView];
    
}

-(void)selectIndexPathRow:(NSInteger)index{
    NSArray *indexArray = @[@"all",@"in_design",@"released"];
    [self searchAction:indexArray[index]];
}

- (void)searchAction:(NSString *)status{
    

    if ([status isEqualToString:@"in_design"]) {
        
        IndexNum = 1;
        [self setUpRefresh];
        
    }else  if ([status isEqualToString:@"released"]) {
        
        IndexNum = 2;
        
        [self setUpRefresh];
        
    }else  if ([status isEqualToString:@"all"]) {
      
        IndexNum = 0;
        [self setUpRefresh];
       
    }
}
#pragma -tableView协议


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return dataArray.count;

}


//点击Cell 跳转页面
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (!_editing) {
      
            
            if ([_TempStr isEqualToString:@"1"]) {
                TCourseAddModel *model = dataArray[indexPath.row];
                
                PlanName=[NSString stringWithFormat:@"%@",model.PlanName];
                PlanId=[NSString stringWithFormat:@"%@",model.PlanNameId];
                
                NSLog(@"%@",PlanName);
                NSLog(@"%@",PlanId);
                
                [self.delegate PlanName:PlanName PlanID:PlanId];
                [self.navigationController popViewControllerAnimated:YES];
                
            }else if([_TempStr isEqualToString:@"0"]){
                
                TCaddNewPlanViewController *vc=[[TCaddNewPlanViewController alloc]init];
                vc.Condition=@"NO";
                vc.PlanModel=dataArray[indexPath.row];
                [self.navigationController pushViewController:vc animated:YES];
                
            }
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TeacherPlanCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlanCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    

    TCourseAddModel *model = dataArray[indexPath.row];
   
    [cell setProperty:model];
    
    
    
    NSString *content=model.PlanName;
    CGFloat test_H=[Maneger getPonentH:content andFont:[UIFont systemFontOfSize:17] andWidth:Swidth-220]+65;
    CGRect frame=cell.frame;
    frame.size.height=test_H+73;
    cell.frame=frame;
    
//////
    cell.PlanStautebtn.hidden=YES;
//////
    
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (model.PlanOwnerId.intValue != persion.userID.intValue ){
        cell.PlanStautebtn.hidden=YES;
    }else{
        cell.PlanStautebtn.hidden=NO;
    }
    
      if ([model.PlanCourseStatus isEqualToString:@"in_design"]) {
        
        [cell.PlanStautebtn setTitle:@"发布" forState:UIControlStateNormal];
        cell.PlanStautebtn.tag=999+indexPath.row;
        [cell.PlanStautebtn addTarget:self action:@selector(ChooseStauteAction:) forControlEvents:UIControlEventTouchUpInside];
        
      }else {
        [cell.PlanStautebtn setTitle:@"已发布" forState:UIControlStateNormal];
        
      }
   
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    TeacherPlanCell *cell =(TeacherPlanCell *)[self tableView:tableView  cellForRowAtIndexPath:indexPath];
    
    return cell.frame.size.height;
}



-(void)ChooseStauteAction:(UIButton *)sender{
    line=sender.tag-999;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem: line inSection:0];
    TeacherPlanCell *cell=[courseTable cellForRowAtIndexPath:indexPath];
    
    
    TCourseAddModel *Model=dataArray[line];

    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"发布确认" message:@"发布后，其他教师可查看该教案，且教案信息将不能再修改，是否确认发布？" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/releaseCourse",LocalIP,Model.PlanNameId];
        
        [RequestTools RequestWithURL:Url Method:@"post" Params:nil Success:^(NSDictionary *result) {
            
            [Maneger showAlert:@"教案发布成功!" andCurentVC:self];
            
            [cell.PlanStautebtn setTitle:@"已发布" forState:UIControlStateNormal];
            
            
        } failed:^(NSString *result) {
            
            
            if (![result isEqualToString:@"200"]) {
                [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            }else{
                [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
            }
            
        }];
        
    }];
    
    UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:noAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark 编辑：
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return   UITableViewCellEditingStyleDelete;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"你确定删除该教案？" preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
             TCourseAddModel *model=dataArray[indexPath.row];
            if([model.CourseStatus isEqualToString:@"in_design"]){
               
                [dataArray removeObjectAtIndex:indexPath.row];
                [courseTable beginUpdates];
                [courseTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                
                [self DeleteDate:model CourseWareId:[NSString stringWithFormat:@"%@",model.PlanNameId]];
                
                [courseTable endUpdates];
            }else{
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"只有计划中的教案才能被删除！！" preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
       
                }]];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    
                }]];
                
                
                [self presentViewController:alertController animated:YES completion:nil];
                
                
            }
            
            
            
            
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
        }]];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}
//设置进入编辑状态时，Cell不会缩进

- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return NO;
    
}

// %@/courses/%@
-(void)DeleteDate:(TCourseAddModel *)model CourseWareId:(NSString *)CourseWareId{
    
    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@",LocalIP,CourseWareId];
    NSLog(@"%@",Url);
    
    
    [RequestTools RequestWithURL:Url Method:@"DELETE" Params:nil Success:^(NSDictionary *result) {
        
        
        
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            
            [MBProgressHUD showToastAndMessage:@"教案成功删除!" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}



@end
