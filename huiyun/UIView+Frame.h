//
//  UIView+Frame.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/29.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)
@property CGFloat wwy_width;
@property CGFloat wwy_height;
@property CGFloat wwy_x;
@property CGFloat wwy_y;
@property CGFloat wwy_centerX;
@property CGFloat wwy_centerY;
@end
