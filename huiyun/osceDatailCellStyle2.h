//
//  osceDatailCellStyle2.h
//  xiaoyun
//
//  Created by MacAir on 17/2/7.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface osceDatailCellStyle2 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *job;
@property (weak, nonatomic) IBOutlet UILabel *work;

@end
