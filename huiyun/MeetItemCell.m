//
//  MeetItemCell.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/23.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "MeetItemCell.h"

@implementation MeetItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    self.lab.adjustsFontSizeToFitWidth = YES;
}
@end
