//
//  TExamListController.h
//  huiyun
//
//  Created by MacAir on 2018/1/15.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "BaseController.h"
#import "SearchUI.h"
#import "TExamListCell.h"
@interface TExamListController : BaseController<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>


@property (strong, nonatomic) UISearchBar *seachBar;
@property (strong, nonatomic) UITableView *tableView;

@end
