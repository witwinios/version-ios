//
//  TeacherModel.m
//  huiyun
//
//  Created by Bad on 2018/3/22.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TeacherModel.h"

@implementation TeacherModel


-(void)setTeacherId:(NSNumber *)TeacherId{
    if ([TeacherId isKindOfClass:[NSNull class]]) {
        _TeacherId = @0;
    }else{
        _TeacherId = TeacherId;
    }
}

-(void)setTeacherName:(NSString *)TeacherName{
    if ([TeacherName isKindOfClass:[NSNull class]]) {
        _TeacherName = @"暂无";
    }else{
        _TeacherName = TeacherName;
    }
}

@end
