//
//  TimeBlockCell.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/9.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeBlockCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *blockIndex;
@property (weak, nonatomic) IBOutlet UILabel *date;

@end
