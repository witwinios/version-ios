//
//  SDepartModel.h
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDepartRecordModel.h"
@interface SDepartModel : NSObject
@property (strong, nonatomic) NSString *SdepartPhaseScheduleName;
@property (strong, nonatomic) NSNumber *SdepartStudentId;
@property (strong, nonatomic) NSString *SdepartStudentName;
@property (strong, nonatomic) NSNumber *SDepartRecordId;
@property (strong, nonatomic) NSString *SDepartName;
@property (strong, nonatomic) NSString *SDepartStatus;
@property (strong, nonatomic) NSNumber *SDepartStartTime;
@property (strong, nonatomic) NSNumber *SDepartEndTime;
@property (strong, nonatomic) NSString *SDepartPlace;
@property (strong, nonatomic) NSString *SDepartRecordName;
@property (strong, nonatomic) NSArray *SDepartRecordArray;
//
@property (strong, nonatomic) NSString *studentSummary;
@property (strong, nonatomic) NSNumber *isSubmitStudentSummary;

@property (strong, nonatomic) NSNumber *teacherId;
@property (strong, nonatomic) NSString *teacherName;
@property (strong, nonatomic) NSString *teacherSummary;
@property (strong, nonatomic) NSNumber *isSubmitTeacherSummary;

@property (strong, nonatomic) NSNumber *supervisorId;
@property (strong, nonatomic) NSString *supervisorName;
@property (strong, nonatomic) NSString *supervisorSummary;
@property (strong, nonatomic) NSNumber *isSubmitSupervisorSummary;
//
@end
