//
//  AddPaperController.m
//  xiaoyun
//
//  Created by MacAir on 17/3/2.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "AddPaperController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
@interface AddPaperController ()
{
    UIButton *currentBtn;
    NSNumber *booleanType;
    NSNumber *categoryType;
    NSNumber *knowType;
}
@end

@implementation AddPaperController

- (void)viewDidLoad {
    [super viewDidLoad];
    _selectKnowId = [NSString new];
    _selectSubjectId = [NSString new];
    _selectCategoryId = [NSString new];
    booleanType = [NSNumber numberWithInt:0];
    categoryType = [NSNumber numberWithInt:0];
    knowType = [NSNumber numberWithInt:0];
    
    [self setObj];
    [self setNav];
    [self loadType];
}
- (void)loadType{
    [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
    [RequestTools RequestWithURL:[NSString stringWithFormat:@"%@/questionCategories?pageSize=999",LocalIP] Method:@"get" Params:nil Success:^(NSDictionary *response) {
        NSMutableArray *array = [NSMutableArray new];
        NSArray *responseArray = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
        for (int i =0; i<responseArray.count; i++) {
            NSMutableDictionary *dic = [NSMutableDictionary new];
            NSDictionary *responseDic = responseArray[i];
            [dic setObject:[responseDic objectForKey:@"categoryId"] forKey:@"id"];
            [dic setObject:[responseDic objectForKey:@"categoryName"] forKey:@"content"];
            [array addObject:dic];
        }
        NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
        [defaults setObject:array forKey:@"category"];
        [defaults synchronize];
        
        //加载科目
        [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
        [RequestTools RequestWithURL:[NSString stringWithFormat:@"%@/subjects?pageSize=999",LocalIP] Method:@"get" Params:nil Success:^(NSDictionary *response) {
            NSMutableArray *array = [NSMutableArray new];
            NSArray *responseArray = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
            for (int i =0; i<responseArray.count; i++) {
                NSMutableDictionary *dic = [NSMutableDictionary new];
                NSDictionary *responseDic = responseArray[i];
                [dic setObject:[responseDic objectForKey:@"subjectId"] forKey:@"id"];
                [dic setObject:[responseDic objectForKey:@"subjectName"] forKey:@"content"];
                [array addObject:dic];
            }
            NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
            [defaults setObject:array forKey:@"subject"];
            [defaults synchronize];
            //加载知识点
            [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
            NSLog(@"--%@",[NSString stringWithFormat:@"%@/knowledgePoints?pageSize=999",LocalIP]);
            
            [RequestTools RequestWithURL:[NSString stringWithFormat:@"%@/knowledgePoints?pageSize=999",LocalIP] Method:@"get" Params:nil Success:^(NSDictionary *response) {
                NSMutableArray *array = [NSMutableArray new];
                NSArray *responseArray = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
                for (int i =0; i<responseArray.count; i++) {
                    NSMutableDictionary *dic = [NSMutableDictionary new];
                    NSDictionary *responseDic = responseArray[i];
                    [dic setObject:[responseDic objectForKey:@"knowledgePointId"] forKey:@"id"];
                    [dic setObject:[responseDic objectForKey:@"knowledgePointName"] forKey:@"content"];
                    [array addObject:dic];
                }
                NSUserDefaults *defaults =  [NSUserDefaults standardUserDefaults];
                [defaults setObject:array forKey:@"knowledge"];
                [defaults synchronize];
                //刷新表格
                
                
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"知识点加载失败~" places:0 toView:nil];
            }];
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"科目加载失败~" places:0 toView:nil];
        }];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"类型加载失败~" places:0 toView:nil];
    }];
}
- (void)setObj{
    self.view.backgroundColor = [UIColor whiteColor];
    NSArray *array = @[@"题目类型",@"题目科目",@"知识点",@"题目数*",@"至少选择一个条件进行出卷"];
    for (int i=0;i<5;i++){
        if (i == 4) {
            UIView *view = [UIView new];
            view.frame = CGRectMake(0, 270, WIDTH, HEIGHT-270);
            view.backgroundColor = [UIColor lightGrayColor];
            [self.view addSubview:view];
            UILabel *label = [UILabel new];
            label.frame = CGRectMake(5, 20, WIDTH-10, 50);
            label.text = array[i];
            label.textAlignment = 1;
            
            UIButton *originBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            originBtn.frame = CGRectMake(0, 75, 80, 40);
            [originBtn setTitle:@"清空" forState:0];
            originBtn.center = CGPointMake(WIDTH/2, 75);
            [originBtn addTarget:self action:@selector(initAction) forControlEvents:UIControlEventTouchUpInside];
            originBtn.layer.cornerRadius = 5;
            originBtn.layer.masksToBounds = YES;
            originBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            originBtn.layer.borderWidth = 1;
            
            [view addSubview:originBtn];
            [view addSubview:label];
            
        }else if (i == 3){
            UILabel *label = [UILabel new];
            label.frame = CGRectMake(5, 50*i+70, 80, 40);
            label.adjustsFontSizeToFitWidth = YES;
            label.textAlignment = 1;
            
            NSMutableAttributedString *threeStr = [[NSMutableAttributedString alloc] initWithString:@"题目数*:"];
            [threeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(3,1)];
            label.attributedText = threeStr;
            UITextField *field = [UITextField new];
            field.keyboardType = UIKeyboardTypeNumberPad;
            field.tag = 300;
            field.layer.cornerRadius = 5;
            field.layer.masksToBounds = YES;
            field.layer.borderColor = [UIColor grayColor].CGColor;
            field.layer.borderWidth = 1;
            field.borderStyle = UITextBorderStyleBezel;
            field.frame = CGRectMake(86, 50*i+70, WIDTH-91, 40);
            [self.view addSubview:label];
            [self.view addSubview:field];
        }else{
            UILabel *label = [UILabel new];
            label.frame = CGRectMake(5, 50*i+70, 80, 40);
            label.adjustsFontSizeToFitWidth = YES;
            label.textAlignment = 1;
            label.text = array[i];
            UIButton *field = [UIButton new];
            field.tag = 200+i;
            field.layer.cornerRadius = 5;
            field.layer.masksToBounds = YES;
            field.layer.borderColor = [UIColor grayColor].CGColor;
            field.layer.borderWidth = 1;
            [field setTitle:@"" forState:0];
            field.backgroundColor = [UIColor whiteColor];
            [field setTitleColor:[UIColor grayColor] forState:0];
            field.frame = CGRectMake(86, 50*i+70, WIDTH-91, 40);
            [field addTarget:self action:@selector(chooseAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:label];
            [self.view addSubview:field];
        }
    }
}
- (void)initAction{
    UIButton *typeBtn = [self.view viewWithTag:200];
    [typeBtn setTitle:@"" forState:0];
    UIButton *subjectBtn = [self.view viewWithTag:201];
    [subjectBtn setTitle:@"" forState:0];
    UIButton *knowBtn = [self.view viewWithTag:202];
    [knowBtn setTitle:@"" forState:0];
    
    _selectKnowId = @"";
    booleanType = [NSNumber numberWithInt:0];
    categoryType = [NSNumber numberWithInt:0];
    knowType = [NSNumber numberWithInt:0];
    
    _selectSubjectId = @"";
    _selectCategoryId = @"";
    UITextField *numField = [self.view viewWithTag:300];
    numField.text = @"";
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-80, 22, 80, 40);
    [rightBtn setTitle:@"出卷" forState:UIControlStateNormal];
    rightBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    rightBtn.layer.cornerRadius = 5;
    rightBtn.layer.masksToBounds = YES;
    rightBtn.layer.borderWidth = 1;
    rightBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [rightBtn addTarget:self action:@selector(paperSubmit:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"自测练习";
    [backNavigation addSubview:titleLab];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITextField *field = [self.view viewWithTag:300];
    [field resignFirstResponder];
}
#pragma -Action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)paperSubmit:(UIButton *)btn{
    UITextField *numberField = [self.view viewWithTag:300];
    UIButton *typeBtn = [self.view viewWithTag:200];
    typeBtn.titleLabel.text = @"";
    
    if (booleanType == [NSNumber numberWithInt:0] &&categoryType == [NSNumber numberWithInt:0] &&knowType == [NSNumber numberWithInt:0]) {
        [Maneger showAlert:@"至少选择一个条件出卷!" andCurentVC:self];
        return;
    }
    
    if ([[NSString stringWithFormat:@"%@",numberField.text] isEqualToString:@""]) {
        [Maneger showAlert:@"题目数不能为空!" andCurentVC:self];
        return;
    }
    
    NSDictionary *params = @{@"knowledgePointId":_selectKnowId,@"questionAmount":numberField.text,@"questionCategoryId":_selectCategoryId,@"subjectId":_selectSubjectId,@"useFor":@"FOR_PRIVATE"};
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@/selfTests/%@/simpleAutoCreateTestPaper",LocalIP,self.testID];
    
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Message:@"新增中" Success:^(NSDictionary *response) {
        
        if ([[response objectForKey:@"errorCode"] isKindOfClass:[NSNull class]]) {
            if ([self.superTab isEqualToString:@"addTestVC"]) {
                [self.navigationController popToViewController:[[MainViewController shareObject] selftestVC]animated:YES];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else{
            [MBProgressHUD showToastAndMessage:@"题库不足!" places:0 toView:nil];
        }
        
    } failed:^(NSString *result) {
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
}
- (void)chooseAction:(UIButton *)btn{
    if (currentBtn != btn) {
        currentBtn.selected = false;
        currentBtn = btn;
        [_menuView removeFromSuperview];
    }
    btn.selected = !btn.selected;
    if (btn.selected) {
        _menuView = [[MenuView alloc]initWithFrame:CGRectMake(btn.frame.origin.x, btn.frame.origin.y+1+btn.frame.size.height, btn.frame.size.width, 250)];
        if (btn.tag-200 == 0) {
            _menuView.listArray = LocalType;
        }else if (btn.tag-200 ==1){
            _menuView.listArray = LocalSubject;
        }else{
            _menuView.listArray = LocalKnow;
        }
        _menuView.selectBlock = ^(NSString *str,NSString *index){
            btn.selected = !btn.selected;
            [btn setTitle:str forState:UIControlStateNormal];
            if (btn.tag-200 == 0) {
                booleanType = [NSNumber numberWithInt:1];
                _selectCategoryId = index;
            }else if (btn.tag-200 ==1){
                categoryType = [NSNumber numberWithInt:1];
                _selectSubjectId = index;
            }else{
                knowType = [NSNumber numberWithInt:1];
                _selectKnowId = index;
            }
        };
        [self.view addSubview:_menuView];
    }else{
        [_menuView removeFromSuperview];
    }
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initAction];
}
@end

