//
//  ResultController.h
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDepartModel.h"
@interface ResultController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *studentSummary;

@property (strong, nonatomic) SDepartModel *departModel;


- (IBAction)tempSave:(id)sender;
- (IBAction)submitSave:(id)sender;
@end
