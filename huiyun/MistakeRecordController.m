//
//  MistakeRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "MistakeRecordController.h"
#import "THDatePickerView.h"
@interface MistakeRecordController ()
{
    NSString *current_date;
    UIView *btnBack;

    UIView *toolbar;
    
    NSMutableArray *departArr;
    NSMutableArray *departNameArr;
    
    NSNumber *has_image;
    NSNumber *currentDepartmentId;
    NSString *currentDepartmentName;
}
@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) WXPPickerView *pickView;
@property (strong, nonatomic) UIButton *btn;

@end

@implementation MistakeRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    has_image = @0;
    current_date = @"";
    currentDepartmentId = @0;
    currentDepartmentName = @"";
    //
    if ([SubjectTool share].subjectType == subjectTypeUpdate) {
        self.oneField.text = self.myModel.caseMistakeModelCategory;
        self.twoField.text = self.myModel.caseMistakeModelDegree;
        self.contentTextview.text = self.myModel.caseMistakeModelViaDescription;
        self.contentTextview1.text = self.myModel.caseMistakeModelReason;
        self.contentTextview2.text = self.myModel.caseMistakeModelLession;
        current_date = [NSString stringWithFormat:@"%@",self.myModel.caseMistakeModelDate];
        if (self.myModel.caseMistakeModelDepartmentId.intValue == 0) {
            currentDepartmentId = @0;
            currentDepartmentName = @"暂无";
            [self.departmentBtn setTitle:currentDepartmentName forState:0];
        }else{
            currentDepartmentId = self.myModel.caseMistakeModelDepartmentId;
            currentDepartmentName = self.myModel.caseMistakeModelDepartmentName;
            [self.departmentBtn setTitle:currentDepartmentName forState:0];
        }
        //
        if (self.myModel.caseMistakeModelDate.intValue == 0) {
            current_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseMistakeModelDate];
            [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:self.myModel.caseMistakeModelDate.stringValue] forState:0];
        }
    }

    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonHide:) name:UIKeyboardWillHideNotification object:nil];
    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];

    self.dateBtn.titleLabel.textAlignment = 0;
    THDatePickerView *dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:longDate] forState:0];
        current_date = longDate;
    } andCancelBlock:^{
        self.btn.hidden = YES;
    } dateType:DateTypeDay];
    [self.view addSubview:dateView];
    self.dateView = dateView;
    [self initStyle];
    [self loadDeaprt];
    [self setUI];
}
- (void)loadDeaprt {
    departArr = [NSMutableArray new];
    departNameArr = [NSMutableArray new];
    
    NSString *departUrl = [NSString stringWithFormat:@"%@/userGroups?groupType=department_type&pageSize=9999",LocalIP];
    [RequestTools RequestWithURL:departUrl Method:@"get" Params:nil Message:@"" Success:^(NSDictionary *result) {
        NSArray *arr = result[@"responseBody"][@"result"];
        for (int i=0; i<arr.count; i++) {
            NSDictionary *dic = arr[i];
            DepartEntity *entity = [DepartEntity new];
            NSNumber *departId = dic[@"groupId"];
            NSString *departName = dic[@"groupName"];
            entity.DepartEntityId = departId;
            entity.DepartEntityName = departName;
            
            [departNameArr addObject:departName];
            [departArr addObject:entity];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"获取失败~" places:0 toView:nil];
    }];
}
- (void)initStyle{
    self.dateBtn.layer.cornerRadius = 4;
    self.dateBtn.layer.masksToBounds = YES;
    self.departmentBtn.layer.cornerRadius = 4;
    self.departmentBtn.layer.masksToBounds = YES;
    
    self.saveBtn.layer.cornerRadius = 4;
    self.saveBtn.layer.masksToBounds = YES;
    
    self.contentTextview.layer.cornerRadius = 4;
    self.contentTextview.layer.masksToBounds = YES;
    
    self.contentTextview1.layer.cornerRadius = 4;
    self.contentTextview1.layer.masksToBounds = YES;
    
    self.contentTextview2.layer.cornerRadius = 4;
    self.contentTextview2.layer.masksToBounds = YES;
    
    self.fileBtn.layer.cornerRadius = 4;
    self.fileBtn.layer.masksToBounds = YES;
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"差错记录";
    [backNavigation addSubview:titleLab];
}
- (void)resignFirstRespond{
    [self.oneField resignFirstResponder];
    [self.twoField resignFirstResponder];
    [self.contentTextview resignFirstResponder];
    [self.contentTextview1 resignFirstResponder];
    [self.contentTextview2 resignFirstResponder];
}
- (void)hideKb:(UIButton *)btn{
    [self resignFirstRespond];
}
-(void)doneButtonshow: (NSNotification *)notification {
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
}
- (void)doneButtonHide:(NSNotification *)notification{
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
    }];
}
- (void)saveAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"类别不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"等级不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"发生时间必填~" places:0 toView:nil];
        return;
    }
    if (has_image.intValue == 0) {
        SdepartRecordController *departVC = [SdepartRecordController shareObject];
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentMalpracticeRecords",SimpleIp];
        NSDictionary *params;
        if (currentDepartmentId.intValue == 0) {
            params = @{@"accidentType":_oneField.text,@"accidentLevel":_twoField.text,@"accidentTime":current_date,@"description":_contentTextview.text,@"rootCause":_contentTextview1.text,@"lesson":_contentTextview2.text,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        }else{
            params = @{@"accidentDepartmentId":currentDepartmentId,@"accidentType":_oneField.text,@"accidentLevel":_twoField.text,@"accidentTime":current_date,@"description":_contentTextview.text,@"rootCause":_contentTextview1.text,@"lesson":_contentTextview2.text,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        }
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        [RequestTools RequestWithFile:_imageView.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            has_image = @0;
            //
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentMalpracticeRecords",SimpleIp];
            NSDictionary *params;
            if (currentDepartmentId.intValue == 0) {
                params = @{@"fileId":fileId,@"accidentType":_oneField.text,@"accidentLevel":_twoField.text,@"accidentTime":current_date,@"description":_contentTextview.text,@"rootCause":_contentTextview1.text,@"lesson":_contentTextview2.text,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};

            }else{
                params = @{@"accidentDepartmentId":currentDepartmentId,@"fileId":fileId,@"accidentType":_oneField.text,@"accidentLevel":_twoField.text,@"accidentTime":current_date,@"description":_contentTextview.text,@"rootCause":_contentTextview1.text,@"lesson":_contentTextview2.text,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
            }
            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
                }else{
                    [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];

        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }
}
- (void)updateAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"类别不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"等级不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"发生时间必填~" places:0 toView:nil];
        return;
    }
    if (has_image.intValue == 0) {
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        SdepartRecordController *departVC = [SdepartRecordController shareObject];
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentMalpracticeRecords/%@",SimpleIp,self.myModel.recordId];
        NSDictionary *params;
        if (currentDepartmentId.intValue == 0) {
            params = @{@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,@"accidentType":_oneField.text,@"accidentLevel":_twoField.text,@"accidentTime":current_date,@"description":_contentTextview.text,@"rootCause":_contentTextview1.text,@"lesson":_contentTextview2.text};
        }else{
            params = @{@"accidentDepartmentId":currentDepartmentId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,@"accidentType":_oneField.text,@"accidentLevel":_twoField.text,@"accidentTime":current_date,@"description":_contentTextview.text,@"rootCause":_contentTextview1.text,@"lesson":_contentTextview2.text};
        }
        [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        [RequestTools RequestWithFile:_imageView.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            has_image = @0;
            //
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentMalpracticeRecords/%@",SimpleIp,self.myModel.recordId];
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            NSDictionary *params;
            if (currentDepartmentId.intValue == 0) {
                params = @{@"fileId":fileId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,@"accidentType":_oneField.text,@"accidentLevel":_twoField.text,@"accidentTime":current_date,@"description":_contentTextview.text,@"rootCause":_contentTextview1.text,@"lesson":_contentTextview2.text};
            }else{
                params = @{@"fileId":fileId,@"accidentDepartmentId":currentDepartmentId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,@"accidentType":_oneField.text,@"accidentLevel":_twoField.text,@"accidentTime":current_date,@"description":_contentTextview.text,@"rootCause":_contentTextview1.text,@"lesson":_contentTextview2.text};
            }

            [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
                }else{
                    [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }

}
#pragma -action
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}
- (void)popLast{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)dateAction:(id)sender {
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];
}

- (IBAction)saveAction:(id)sender {
    if ([SubjectTool share].subjectType == subjectTypeAdd) {
        [self saveAction];
    }else{
        [self updateAction];
    }
}

- (IBAction)departmentAction:(id)sender {
    WXPPickerView *pickView = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:departNameArr]];
    pickView.delegate = self;
    pickView.rightBtnTitle = @"确认";
    pickView.backgroundColor = [UIColor whiteColor];
    pickView.index = ^(int index){
        [sender setTitle:departNameArr[index] forState:0];
        DepartEntity *entity = departArr[index];
        currentDepartmentId = entity.DepartEntityId;
        currentDepartmentName = entity.DepartEntityName;
    };
    [self.view addSubview:pickView];
    [pickView show];
    self.pickView = pickView;
}
- (void)PickerViewRightButtonOncleck:(NSInteger)index{
    [self.pickView close];
}
#pragma UIImagePickerControllerDelegate
- (IBAction)fileAction:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相册",@"拍照", nil];
    [sheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self getAlbumAction];
            break;
        case 1:
            [self photoAction];
            break;
        default:
            break;
    }
}
- (void)photoAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePickController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)getAlbumAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    [picker dismissViewControllerAnimated:YES completion:nil];
    has_image = @1;
    _imageView.image = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
}

@end
