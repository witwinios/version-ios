//
//  SubjectTool.m
//  huiyun
//
//  Created by MacAir on 2018/1/18.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "SubjectTool.h"

@implementation SubjectTool
+ (void)showWithfileUrl:(NSString *)fileUrl{
    NSString *imgStr = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",LocalIP,fileUrl,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
    NSString *imageStr =  [imgStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    UIColor *color = [UIColor lightGrayColor];
    view.backgroundColor = [color colorWithAlphaComponent:0.5];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, Swidth-40, Sheight-40)];
    imgView.layer.cornerRadius = 5;
    imgView.layer.masksToBounds = YES;
    imgView.center = view.center;
    [view addSubview:imgView];
    __block UIProgressView *pv;
    __weak UIImageView *weakImageView = imgView;
    [imgView sd_setImageWithURL:[NSURL URLWithString:imageStr]
               placeholderImage:[UIImage imageNamed:@"holdimage"]
                        options:SDWebImageCacheMemoryOnly
                       progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                           if (!pv) {
                               [weakImageView addSubview:pv = [UIProgressView.alloc initWithProgressViewStyle:UIProgressViewStyleDefault]];
                               pv.frame = CGRectMake(0, 0, Swidth/2, 40);
                           }
                           CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 8.0f);
                           pv.transform = transform;
                           float showProgress = (float)receivedSize/(float)expectedSize;
                           [pv setProgress:showProgress];
                       }
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                          [pv removeFromSuperview];
                          pv = nil;
                      }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeAction:)];
    tap.numberOfTapsRequired = 1;
    view.userInteractionEnabled = YES;
    imgView.userInteractionEnabled = YES;
    [view addGestureRecognizer:tap];
    [imgView addGestureRecognizer:tap];
    [[UIApplication sharedApplication].keyWindow addSubview:view];
}
- (void)removeAction:(UITapGestureRecognizer *)tap{
//    [[UIApplication sharedApplication].keyWindow setRootViewController:nil];
////    UIView *view = tap.view;
////    [view removeFromSuperview];
    [[UIApplication sharedApplication].keyWindow removeFromSuperview];
}

+ (instancetype)share{
    static SubjectTool *tools = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (tools == nil) {
            tools = [SubjectTool new];
        }
    });
    return tools;
}

- (SubjectType)getType{
    return self.subjectType;
}
@end
