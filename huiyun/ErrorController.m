//
//  ErrorController.m
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "ErrorController.h"

@interface ErrorController ()
{
    UITableView *errorTable;
    NSMutableArray *dataArray;
    //页数
    int currentPage;
    //
}
@end

@implementation ErrorController
- (void)viewWillAppear:(BOOL)animated{
    currentPage = 1;
    [errorTable headerBeginRefreshing];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setObj];
    [self setUpRefresh];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-27, 29.5, 25, 25);
    [rightBtn addTarget:self action:@selector(addAction:) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
    [backNavigation addSubview:rightBtn];

    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"错题集";
    [backNavigation addSubview:titleLab];
}
- (void)setObj{
    currentPage = 1;
    
    dataArray = [NSMutableArray new];
    errorTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStyleGrouped];
    errorTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    errorTable.delegate = self;
    errorTable.dataSource = self;
    [errorTable registerNib:[UINib nibWithNibName:@"SelfListCell" bundle:nil] forCellReuseIdentifier:@"selfCell"];
    [self.view addSubview:errorTable];
    //
}
- (void)setUpRefresh{
    [errorTable addHeaderWithTarget:self action:@selector(downRefresh)];
    [errorTable addFooterWithTarget:self action:@selector(loadMoreRefresh)];
    //设置文字
    errorTable.headerPullToRefreshText = @"下拉刷新";
    errorTable.headerReleaseToRefreshText = @"松开进行刷新";
    errorTable.headerRefreshingText = @"刷新中。。。";
    
    errorTable.footerPullToRefreshText = @"上拉加载";
    errorTable.footerReleaseToRefreshText = @"松开进行加载";
    errorTable.footerRefreshingText = @"加载中。。。";
}
- (void)downRefresh{
    currentPage = 1;
    NSString *URL = [NSString stringWithFormat:@"%@/selfTests?useFor=for_private&studentId=%@&selfType=MISTAKE_TEST&pageStart=1&pageSize=15",LocalIP,LocalUserId];
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *response) {
        [errorTable headerEndRefreshing];
        //移除所有元素
        [dataArray removeAllObjects];
        //
        NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
        if (array.count ==0) {
            [MBProgressHUD showToastAndMessage:@"暂无错题!" places:0 toView:nil];
        }else{
            for (int i=0; i<array.count; i++) {
                NSDictionary *dic = array[i];
                ErrorModel *model = [ErrorModel new];
                model.testId = [dic objectForKey:@"testId"];
                model.testPaper = [dic objectForKey:@"testPapersNum"];
                model.testSubject =  [dic objectForKey:@"subjectName"];
                model.testCategory =  [dic objectForKey:@"categoryName"];
                model.testStatus =  [dic objectForKey:@"testStatus"];
                model.testDescription = [dic objectForKey:@"description"];
                model.testName =  [dic objectForKey:@"testName"];
                [dataArray addObject:model];
            }
            [errorTable reloadData];
        }

    } failed:^(NSString *result) {
        [errorTable headerEndRefreshing];
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}
- (void)loadMoreRefresh{
    currentPage++;
 
    NSString *URL = [NSString stringWithFormat:@"%@/selfTests?useFor=for_private&studentId=%@&selfType=MISTAKE_TEST&pageStart=%@&pageSize=15",LocalIP,LocalUserId,[NSString stringWithFormat:@"%d",currentPage]];
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *response) {
        [errorTable footerEndRefreshing];
        NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
        if (array.count ==0) {
            [MBProgressHUD showToastAndMessage:@"暂无更多!" places:0 toView:nil];
            
        }else{
            NSMutableArray *newData = [NSMutableArray new];
            for (int i=0; i<array.count; i++) {
                NSDictionary *dic = array[i];
                ErrorModel *model = [ErrorModel new];
                model.testId = [dic objectForKey:@"testId"];
                model.testPaper = [dic objectForKey:@"testPapersNum"];
                model.testSubject =  [dic objectForKey:@"subjectName"];
                model.testCategory =  [dic objectForKey:@"categoryName"];
                model.testStatus =  [dic objectForKey:@"testStatus"];
                model.testDescription = [dic objectForKey:@"description"];
                model.testName =  [dic objectForKey:@"testName"];
                [newData addObject:model];
            }
            NSRange range = NSMakeRange(dataArray.count,newData.count);
            NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
            [dataArray insertObjects:newData atIndexes:set];
            [errorTable reloadData];
        }

    } failed:^(NSString *code) {
        currentPage--;
        [errorTable footerEndRefreshing];
        if ([code isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}
#pragma -Action
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)addAction:(UIButton *)btn{
    AddErrorController *addVC = [AddErrorController new];
    [self.navigationController pushViewController:addVC animated:YES];
}
#pragma -table协议
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 115.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SelfPaperController *selfVC = [SelfPaperController new];
    selfVC.fromVC = @"error";
    selfVC.model = [dataArray objectAtIndex:indexPath.section];
    [self.navigationController pushViewController:selfVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SelfListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"selfCell"];
    ErrorModel *model = dataArray[indexPath.section];
    cell.testName.text = model.testName;
    cell.pageNum.text = model.testPaper.stringValue;
    cell.paperDes.text = model.testDescription;
    return cell;
}
#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
