//
//  RecordCell.h
//  huiyun
//
//  Created by MacAir on 2017/9/15.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *onelineLab;
@property (weak, nonatomic) IBOutlet UILabel *onelineContent;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *twolineLab;
@property (weak, nonatomic) IBOutlet UILabel *twolineContent;
@property (weak, nonatomic) IBOutlet UILabel *threelineLab;
@property (weak, nonatomic) IBOutlet UILabel *threelineContent;
@property (weak, nonatomic) IBOutlet UILabel *dateLab;
@property (weak, nonatomic) IBOutlet UILabel *dateContent;


@end
