//
//  DepartmentModel.h
//  huiyun
//
//  Created by Bad on 2018/3/15.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DepartmentModel : NSObject
@property(nonatomic,strong)NSString *GroupId;
@property(nonatomic,strong)NSString *GroupName;
@property(nonatomic,strong)NSString *GroupType;
@end
