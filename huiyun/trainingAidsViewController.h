//
//  trainingAidsViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeachingAidsCell.h"
#import "TeachingAidsModel.h"
#import "TrainingAidsAddViewController.h"
#import "TrainingAidsDetailsViewController.h"
@interface trainingAidsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic)TCourseAddModel *PlanModel;
@property(nonatomic,getter=isEditing) BOOL editing;

-(void)setEditing:(BOOL)editing animated:(BOOL)animated;
@end
