//
//  QuestionModel.h
//  xiaoyun
//
//  Created by MacAir on 17/2/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionModel : NSObject
@property (strong, nonatomic) NSNumber *questionId;
@property (strong, nonatomic) NSString *questionType;
@property (strong, nonatomic) NSString *questTitle;
@property (strong, nonatomic) NSArray *choiceOptions;
@property (strong, nonatomic) NSArray *correctAnswer;
@property (strong, nonatomic) NSArray *responseAnswer;
@property (strong, nonatomic) NSString *questionDes;
@property (strong, nonatomic) NSString *questionFile;
@property (strong, nonatomic) NSNumber *parrentId;
@property (strong, nonatomic) NSNumber *childNum;

@property(strong,nonatomic)NSNumber *answer;

@property (strong, nonatomic) NSMutableArray *answerArray;
@property (strong, nonatomic) NSMutableArray *answerFieldArr;

@property(strong,nonatomic)NSNumber *seleQuestion;

//判断是否为父题 1 是 0 不是
@property (strong, nonatomic) NSNumber *isFather;
//如果为父题,字体加入进来
@property (strong, nonatomic) NSMutableArray *childArray;

- (NSComparisonResult)compareWithQuestion:(QuestionModel *)model;


@end

