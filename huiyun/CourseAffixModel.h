//
//  CourseAffixModel.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/25.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CourseAffixModel : NSObject

/*
 fileId: Long//文件Id
 fileName: String//文件名称
 description: String//描述
 fileType: FileType//文件类型
 fileUrl: String//文件路径
 fileFormat: String//文件格式
 fileSize: long//文件大小
 isPublic: boolean//是否公开
 createdTime: Date//创建时间
 createdBy: String//创建人
 */
@property(strong,nonatomic) NSNumber *FileId;
@property(strong,nonatomic) NSString *FileName;
@property(strong,nonatomic) NSString *Description;
@property(strong,nonatomic) NSString *FileType;
@property(strong,nonatomic) NSString *FileUrl;
@property(strong,nonatomic) NSString *FileFormat;
@property(strong,nonatomic) NSNumber *FileSize;
@property(strong,nonatomic) NSNumber *IsPublic;
@property(strong,nonatomic) NSString *CreatedBy;
@property(strong,nonatomic) NSString *CreatedTime;

@end
