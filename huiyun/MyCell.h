//
//  MyCell.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/10.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseModel.h"
@interface MyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *CourseName;
@property (weak, nonatomic) IBOutlet UILabel *CourseState;
@property (weak, nonatomic) IBOutlet UILabel *CourseTime;
@property (weak, nonatomic) IBOutlet UILabel *CourseLocation;
@property (weak, nonatomic) IBOutlet UIButton *CourseBtn;
@property (weak, nonatomic) IBOutlet UIButton *RQCodebtn;


- (void)setProperty:(TCourseModel *)model;
@end
