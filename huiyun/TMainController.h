//
//  TMainController.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/4.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseController.h"
#import "TExamController.h"
#import "TLeaveController.h"
#import "TMarkController.h"
#import "TeacherAccountController.h"
#import "TeacherPlanController.h"
#import "DepartController.h"
#import "EvaluateViewController.h"
@interface TMainController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *headBackView;
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *headName;
@property (weak, nonatomic) IBOutlet UITableView *mainTable;

@property(strong,nonatomic) TCourseController *tcourseVC;
@property(strong,nonatomic) TExamController *texamVC;
@property(strong,nonatomic) TLeaveController *tleaveVC;
@property(strong,nonatomic) TMarkController *tmarkVC;
@property(strong, nonatomic) DepartController *departVC;
@property(strong, nonatomic) TeacherPlanController *teachplanVC;

@property(strong,nonatomic)EvaluateViewController *EvaluateVc;

@property (strong, nonatomic) AccountEntity *entity;
+ (id)shareObject;
@end
