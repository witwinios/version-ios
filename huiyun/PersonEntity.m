//
//  PersonEntity.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "PersonEntity.h"
@implementation PersonEntity
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.roleId forKey:@"roleId"];
    [aCoder encodeObject:self.userAccountIM forKey:@"userAccountIM"];
    [aCoder encodeObject:self.userPasswordIM forKey:@"userPasswordIM"];
    [aCoder encodeObject:self.userAccount forKey:@"userAccount"];
    [aCoder encodeObject:self.userPassword forKey:@"userPassword"];
    [aCoder encodeObject:self.userID forKey:@"userID"];
    [aCoder encodeObject:self.userName forKey:@"userName"];
    [aCoder encodeObject:self.userFullName forKey:@"userFullName"];
    [aCoder encodeObject:self.userEmail forKey:@"userEmail"];
    [aCoder encodeObject:self.fileUrl forKey:@"fileUrl"];
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.roleId = [aDecoder decodeObjectForKey:@"roleId"];
        self.userAccountIM = [aDecoder decodeObjectForKey:@"userAccountIM"];
        self.userPasswordIM = [aDecoder decodeObjectForKey:@"userPasswordIM"];
        self.userAccount = [aDecoder decodeObjectForKey:@"userAccount"];
        self.userPassword = [aDecoder decodeObjectForKey:@"userPassword"];
        self.userID = [aDecoder decodeObjectForKey:@"userID"];
        self.userName = [aDecoder decodeObjectForKey:@"userName"];
        self.userFullName = [aDecoder decodeObjectForKey:@"userFullName"];
        self.userEmail = [aDecoder decodeObjectForKey:@"userEmail"];
        self.fileUrl = [aDecoder decodeObjectForKey:@"fileUrl"];
    }
    return self;
}
@end

