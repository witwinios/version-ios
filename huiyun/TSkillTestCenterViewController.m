//
//  TSkillTestCenterViewController.m
//  huiyun
//
//  Created by Bad on 2018/3/23.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TSkillTestCenterViewController.h"

@interface TSkillTestCenterViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *TableVc;
    NSMutableArray *dataArray;
    
    int currentPage;
}
@end

@implementation TSkillTestCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNav];
    dataArray=[NSMutableArray new];
    
   
    
    [self setUI];
    
    [self setUpRefresh];
    
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"站点信息";
    
    
    [backNavigation addSubview:titleLab];
    
    
//    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
//    [rightBtn setTitle:@"批量选取" forState:UIControlStateNormal];
//    [rightBtn addTarget:self action:@selector(Choice:) forControlEvents:UIControlEventTouchUpInside];
//    [backNavigation addSubview:rightBtn];
//
//    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
//    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
//    [SaveBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//    [SaveBtn addTarget:self action:@selector(setCourseWareChoice) forControlEvents:UIControlEventTouchUpInside];
//    [backNavigation addSubview:SaveBtn];
//    SaveBtn.hidden=YES;
    
    
}
- (void)back :(UIButton *)button{
    

        [self.navigationController popViewControllerAnimated:YES];
   
}

-(void)setUI{
   // currentPage = 1;CGFloat courseTable_Y=backNavigation.wwy_height+SearchView.wwy_height;
    TableVc = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    TableVc.delegate = self;
    TableVc.dataSource = self;
    TableVc.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    TableVc.showsVerticalScrollIndicator=NO;
    TableVc.estimatedRowHeight = 0;
    [TableVc registerNib:[UINib nibWithNibName:@"TableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [self.view addSubview:TableVc];
 
}



- (void)setUpRefresh{
    
    
    [TableVc addHeaderWithTarget:self action:@selector(downRefresh)];
    //设置文字
    TableVc.headerPullToRefreshText = @"下拉刷新";
    TableVc.headerReleaseToRefreshText = @"松开进行刷新";
    TableVc.headerRefreshingText = @"刷新中。。。";
    [TableVc headerBeginRefreshing];
    
    [TableVc addFooterWithTarget:self action:@selector(loadRefresh)];
    TableVc.footerPullToRefreshText = @"上拉加载";
    TableVc.footerReleaseToRefreshText = @"松开进行加载";
    TableVc.footerRefreshingText = @"加载中。。。";
    
}

-(void)downRefresh{
    currentPage=1;
    
    NSString *Url=[NSString stringWithFormat:@"%@/skillTests/%@/selectedStations?pageStart=%d&pageSize=20",LocalIP,_testId,currentPage];
    
    NSLog(@"%@",Url);
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [TableVc headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [MBProgressHUD showToastAndMessage:@"无站点信息" places:0 toView:nil];
                [TableVc headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dic= [array objectAtIndex:i];
                    TSkillTestCenterModel *model=[TSkillTestCenterModel new];
                    model.RoomName=[dic objectForKey:@"roomName"];
                    model.SelectedStationName=[dic objectForKey:@"selectedStationName"];
                    model.OperationScoreValue=[dic objectForKey:@"operationScoreValue"];
                    
                    NSMutableArray *tempArr=[dic objectForKey:@"teachers"];
                    
                    if (tempArr.count >0) {
                        for (int i=0; i<tempArr.count; i++) {
                            NSDictionary *dic=[tempArr objectAtIndex:i];
                            model.TeacherName=[dic objectForKey:@"fullName"];
                            [model.TeacherArray addObject:model.TeacherName];
                            
                        }
                    }
                    [dataArray addObject:model];
                    [TableVc headerEndRefreshing];
                }
                [TableVc reloadData];
               
        }
        }else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
            [TableVc headerEndRefreshing];
        }
 
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        [TableVc headerEndRefreshing];
    }];
    
    
}

-(void)loadRefresh{
    currentPage++;
    
    NSString *Url=[NSString stringWithFormat:@"%@/skillTests/%@/selectedStations?pageStart=%d&pageSize=20",LocalIP,_testId,currentPage];
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [TableVc headerEndRefreshing];
            return;
        }
       [TableVc footerEndRefreshing];
        NSMutableArray *NewData=[NSMutableArray new];
        
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [MBProgressHUD showToastAndMessage:@"无更多站点信息" places:0 toView:nil];
                 [TableVc footerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dic= [array objectAtIndex:i];
                    TSkillTestCenterModel *model=[TSkillTestCenterModel new];
                    model.RoomName=[dic objectForKey:@"roomName"];
                    model.SelectedStationName=[dic objectForKey:@"selectedStationName"];
                    model.OperationScoreValue=[dic objectForKey:@"operationScoreValue"];
                    
                    NSMutableArray *tempArr=[dic objectForKey:@"teachers"];
                    model.TeacherArray=[NSMutableArray new];
                    if (tempArr.count >0) {
                        for (int i=0; i<tempArr.count; i++) {
                            NSDictionary *dic=[tempArr objectAtIndex:i];
                            TSkillTestCenterModel *Model=[TSkillTestCenterModel new];
                            Model.TeacherName=[dic objectForKey:@"fullName"];
                            [model.TeacherArray addObject:Model.TeacherName];
                            
                        }
                    }
                    [NewData addObject:model];
                    
                }
                NSRange range = NSMakeRange(dataArray.count,NewData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:NewData atIndexes:set];
                
                
            }
        }else{
             currentPage--;
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
         currentPage--;
        NSLog(@"%@",result);
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TableViewCell *cell=(TableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    TSkillTestCenterModel *model=dataArray[indexPath.row];
    
    NSLog(@"teacher===%@",model.TeacherArray);
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell setTeacherAddModel:model];
    
    return cell;
}
//
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 122;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
