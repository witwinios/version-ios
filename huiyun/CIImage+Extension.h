//
//  CIImage+Extension.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/29.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <CoreImage/CoreImage.h>
#import <UIKit/UIKit.h>
@interface CIImage (Extension)
- (UIImage *)createNonInterpolatedWithSize:(CGFloat)size;
@end
