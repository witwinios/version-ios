//
//  TSkillTestCenterModel.m
//  huiyun
//
//  Created by Bad on 2018/3/23.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TSkillTestCenterModel.h"

@implementation TSkillTestCenterModel

-(void)setRoomName:(NSString *)RoomName{
    if ([RoomName isKindOfClass:[NSNull class]]) {
        _RoomName = @"暂无";
    }else{
        _RoomName = RoomName;
    }
}

-(void)setTeacherName:(NSString *)TeacherName{
    if ([TeacherName isKindOfClass:[NSNull class]]) {
        _TeacherName = @"暂无";
    }else{
        _TeacherName = TeacherName;
    }
}

-(void)setOperationScoreValue:(NSNumber *)OperationScoreValue{
    if ([OperationScoreValue isKindOfClass:[NSNull class]]) {
        _OperationScoreValue = @0;
    }else{
        _OperationScoreValue = OperationScoreValue;
    }
}

-(void)setSelectedStationName:(NSString *)SelectedStationName{
    if ([SelectedStationName isKindOfClass:[NSNull class]]) {
        _SelectedStationName = @"暂无";
    }else{
        _SelectedStationName = SelectedStationName;
    }
}

-(void)setTSkillTimeFrom:(NSNumber *)TSkillTimeFrom{
    if ([TSkillTimeFrom isKindOfClass:[NSNull class]]) {
        _TSkillTimeFrom = @0;
    }else{
        _TSkillTimeFrom = TSkillTimeFrom;
    }
}

-(void)setTSKillTimeEnd:(NSNumber *)TSKillTimeEnd{
    if ([TSKillTimeEnd isKindOfClass:[NSNull class]]) {
        _TSKillTimeEnd = @0;
    }else{
        _TSKillTimeEnd = TSKillTimeEnd;
    }
}

@end
