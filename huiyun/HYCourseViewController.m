//
//  CourseViewController.m
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import "HYCourseViewController.h"



#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
@interface HYCourseViewController ()
{
    UITableView *courseTable;
    
    NSMutableArray *dataArray;
    NSMutableArray *completeArray;

    int currentPage;
    
    UIButton *searchBtn;
    NSMutableArray *resultArray;
    
    NSString *TempStr;
   
    
    NSString *downURL;
    NSString *loadURL;
    
    NSInteger IndexNums;
    
    NSMutableArray *CourseSrate;
    NSMutableArray *CourseType;
    NSString *CourseSrateStr;
    NSString *CourseTypeStr;
    
    
    UIView *BackVc;
    UIPickerView *Picker;
    UIButton *okBtn;
    UIButton *noBtn;
    
    
}
@end

@implementation HYCourseViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setObj];
    
    //添加上拉加载刷新
    [self setUpRefresh];
    
    
}
- (void)setObj{
    currentPage = 1;
    //
    dataArray = [NSMutableArray new];
    completeArray = [NSMutableArray new];
    resultArray=[NSMutableArray new];
    
    
    //
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,WIDTH, HEIGHT-64) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    [courseTable registerNib:[UINib nibWithNibName:@"HYCourseCell" bundle:nil] forCellReuseIdentifier:@"courseCell"];
    [self.view addSubview:courseTable];
     downURL=[NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@",LocalIP,LocalUserId];
    //
    IndexNums = 0;
}


-(void)setUpRefresh{
    
    
    [courseTable addHeaderWithTarget:self action:@selector(downRefresh)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中。。。";
    
    
}

- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"筛选"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(Search:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"课程列表";
    [backNavigation addSubview:titleLab];
    
    
    TempStr=@"已报名";
   
   
}

-(void)Search:(id)sender{

    CourseSrate=[NSMutableArray arrayWithObjects:@"课程状态",@"已报名",@"未报名",@"已完成",@"未完成", nil];
    CourseType=[NSMutableArray arrayWithObjects:@"课程类型",@"全部课程",@"理论课程",@"技能训练",@"网络课程",@"病例讨论",@"教学查房", nil];
    
    CourseTypeStr=CourseType[0];
    CourseSrateStr=CourseSrate[0];

    if(BackVc !=nil){
        BackVc.hidden=YES;
    }
    
    //蒙版
    CGFloat BackVc_Y=Sheight/3;
    BackVc=[[UIView  alloc]initWithFrame:self.view.bounds];
    BackVc.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
    
    //工具栏
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0,Sheight-BackVc_Y , Swidth, BackVc_Y)];
    UIView *ToolView=[[UIView alloc]initWithFrame:CGRectMake(0, Picker.wwy_y-30, Swidth, 30)];
    ToolView.backgroundColor=UIColorFromHex(0x20B2AA);
    [BackVc addSubview:ToolView];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:noBtn];
    
    Picker.backgroundColor=[UIColor whiteColor];

    Picker.dataSource=self;
    Picker.delegate=self;

    
    [BackVc addSubview:Picker];
    
    [[UIApplication sharedApplication].keyWindow addSubview:BackVc];
    
    
    
    
    self.picker.delegate=self;
    self.picker.dataSource=self;

}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setOkBtn{
    NSLog(@"课程状态==%@",CourseSrateStr);
    NSLog(@"课程类型==%@",CourseTypeStr);
    

    
    if ([CourseTypeStr isEqualToString:@"理论课程"]) {
        CourseTypeStr=@"classroom_course";
    }else  if ([CourseTypeStr isEqualToString:@"技能训练"]) {
        CourseTypeStr=@"operating_course";
    }else  if ([CourseTypeStr isEqualToString:@"网络课程"]) {
        CourseTypeStr=@"online_course";
    }else  if ([CourseTypeStr isEqualToString:@"病例讨论"]) {
        CourseTypeStr=@"case_discussion";
    }else  if ([CourseTypeStr isEqualToString:@"教学查房"]) {
        CourseTypeStr=@"teaching_rounds";
    }
        
    
    if ([CourseSrateStr isEqualToString:@"课程状态"] || [CourseTypeStr isEqualToString:@"课程类型"]) {
        [MBProgressHUD showToastAndMessage:@"请重新选择" places:0 toView:nil];
    }else{
        if([CourseSrateStr isEqualToString:@"已报名"]){

            if ([CourseTypeStr isEqualToString:@"全部课程"]) {
                
                 TempStr=@"已报名";
                
                IndexNums=0;
                [self setUpRefresh];
            }else{
                IndexNums=4;
                [self setUpRefresh];
            }
            
            
        }else if([CourseSrateStr isEqualToString:@"未报名"]){
            
             TempStr=@"未报名";
            
            if ([CourseTypeStr isEqualToString:@"全部课程"]) {
                IndexNums=1;
                [self setUpRefresh];
            }else{
                IndexNums=5;
                [self setUpRefresh];
            }
        }else if([CourseSrateStr isEqualToString:@"已完成"]){
            
             TempStr=@"已报名";
            
            if ([CourseTypeStr isEqualToString:@"全部课程"]) {
                IndexNums=2;
                [self setUpRefresh];
            }else{
                IndexNums=6;
                [self setUpRefresh];
            }
        }else if([CourseSrateStr isEqualToString:@"未完成"]){
            
            TempStr=@"已报名";
            
            if ([CourseTypeStr isEqualToString:@"全部课程"]) {
                IndexNums=3;
                [self setUpRefresh];
            }else{
                IndexNums=7;
                [self setUpRefresh];
            }
        }
        
        BackVc.hidden=YES;
        
//        IndexNums=1;
//      //
//    NSString *requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&scheduleStatus=%@&scheduleType=%@&pageStart=%d",LocalIP,LocalUserId,CourseSrateStr,CourseTypeStr,currentPage];
//        NSLog(@"%@",requestUrl);
    }
    
}

-(void)setNoBtn{
    BackVc.hidden=YES;
}




- (void)downRefresh{
    currentPage = 1;
    NSString *requestUrl;
    
     switch (IndexNums) {
     case 0:
           requestUrl = [NSString stringWithFormat:@"%@&pageStart=%d",downURL,currentPage];
           break;
     case 1:
            requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&studentIn=false&isLive=true&pageStart=%d",LocalIP,LocalUserId,currentPage];
           break;
     case 2:
            requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&scheduleStatus=completed&pageStart=%d",LocalIP,LocalUserId,currentPage];
           break;
     case 3:
            requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&studentIn=false&pageStart=%d&scheduleStatus=completed",LocalIP,LocalUserId,currentPage];
           break;
     case 4:
            requestUrl = [NSString stringWithFormat:@"%@&pageStart=%d&scheduleType=%@",downURL,currentPage,CourseTypeStr];
             break;
     case 5:
             requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&studentIn=false&isLive=true&pageStart=%d&scheduleType=%@",LocalIP,LocalUserId,currentPage,CourseTypeStr];
             break;
     case 6:
             requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&scheduleStatus=completed&pageStart=%d&scheduleType=%@",LocalIP,LocalUserId,currentPage,CourseTypeStr];
             break;
     case 7:
             requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&studentIn=false&pageStart=%d&scheduleStatus=completed&scheduleType=%@",LocalIP,LocalUserId,currentPage,CourseSrateStr];
             break;
     default:
     break;
     }
    

    NSLog(@"URL==%@",requestUrl);
    
        [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *responseDic) {
            [dataArray removeAllObjects];
            [resultArray removeAllObjects];
            //获取数据源
            if ([responseDic[@"responseStatus"] isEqualToString:@"succeed"]) {
                NSArray *array = [[responseDic objectForKey:@"responseBody"] objectForKey:@"result"];
                if (array.count ==0) {
                    [Maneger showAlert:@"无课程!" andCurentVC:self];
                    [courseTable headerEndRefreshing];
                }else{
                    [searchBtn setTitle:@"已报名课程↓" forState:UIControlStateNormal];
                    for (int i=0; i<array.count; i++) {
                        NSDictionary *dictionary = [array objectAtIndex:i];
                        HYCourseModel *model = [HYCourseModel new];
                        model.courseId=[dictionary objectForKey:@"courseId"];
                        model.courseName = [dictionary objectForKey:@"courseName"];
                        model.coursePlace = [dictionary objectForKey:@"classroomName"];
                        model.courseStatus = [dictionary objectForKey:@"scheduleStatus"];
                        model.courseTime = [dictionary objectForKey:@"startTime"];
                        model.courseType = [dictionary objectForKey:@"scheduleType"];
                        model.courseCategory = [dictionary objectForKey:@"courseCategoryName"];
                        model.courseTeahcher = [dictionary objectForKey:@"teacherName"];
                        model.courseDes = [dictionary objectForKey:@"courseDescription"];
                        model.subjectName = [dictionary objectForKey:@"subjectName"];
                        model.scheduleID=[dictionary objectForKey:@"scheduleId"];
                    
                            [dataArray addObject:model];
                            [resultArray addObject:model];
                     
                    }
                    NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                    NSLog(@"resultArrayCount=%lu",(unsigned long)resultArray.count);
                    [courseTable reloadData];
                    [courseTable headerEndRefreshing];
                    
                
                    
                    
                }
            }
            else{
                [MBProgressHUD showToastAndMessage:@"请求数据错误!" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [courseTable headerEndRefreshing];
            if (![result isEqualToString:@"200"]) {
                [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
                NSLog(@"%@",result);
            }else{
                [MBProgressHUD showToastAndMessage:@"请求失败!" places:0 toView:nil];
            }
        }];
}
- (void)loadRefresh{
    currentPage++;
     NSString *requestUrl;
   
    switch (IndexNums) {
        case 0:
            requestUrl = [NSString stringWithFormat:@"%@&pageStart=%d",downURL,currentPage];
            break;
        case 1:
            requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&studentIn=false&isLive=true&pageStart=%d",LocalIP,LocalUserId,currentPage];
            break;
        case 2:
            requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&scheduleStatus=completed&pageStart=%d",LocalIP,LocalUserId,currentPage];
            break;
        case 3:
            requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&studentIn=false&pageStart=%d&scheduleStatus=completed",LocalIP,LocalUserId,currentPage];
            break;
        case 4:
            requestUrl = [NSString stringWithFormat:@"%@&pageStart=%d&scheduleType=%@",downURL,currentPage,CourseTypeStr];
            break;
        case 5:
           requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&studentIn=false&isLive=true&pageStart=%d&scheduleType=%@",LocalIP,LocalUserId,currentPage,CourseTypeStr];
            break;
        case 6:
            requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&scheduleStatus=completed&pageStart=%d&scheduleType=%@",LocalIP,LocalUserId,currentPage,CourseTypeStr];
            break;
        case 7:
            requestUrl = [NSString stringWithFormat:@"%@/courseSchedules?pageSize=10&studentId=%@&studentIn=false&pageStart=%d&scheduleStatus=completed&scheduleType=%@",LocalIP,LocalUserId,currentPage,CourseSrateStr];
            break;
        default:
            break;
    }
    
        [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *responseDic) {
 
             [courseTable footerEndRefreshing];
            NSMutableArray *newData=[NSMutableArray new];
            
            //获取数据源
            if ([responseDic[@"responseStatus"] isEqualToString:@"succeed"]) {
                NSArray *array = [[responseDic objectForKey:@"responseBody"] objectForKey:@"result"];
                if (array.count ==0) {
                    [Maneger showAlert:@"无课程!" andCurentVC:self];
                    [courseTable headerEndRefreshing];
                }else{
                    
                    for (int i=0; i<array.count; i++) {
                        NSDictionary *dictionary = [array objectAtIndex:i];
                        HYCourseModel *model = [HYCourseModel new];
                        model.courseName = [dictionary objectForKey:@"courseName"];
                        model.coursePlace = [dictionary objectForKey:@"classroomName"];
                        model.courseStatus = [dictionary objectForKey:@"scheduleStatus"];
                        model.courseTime = [dictionary objectForKey:@"startTime"];
                        model.courseType = [dictionary objectForKey:@"scheduleType"];
                        model.courseCategory = [dictionary objectForKey:@"courseCategoryName"];
                        model.courseTeahcher = [dictionary objectForKey:@"teacherName"];
                        model.courseDes = [dictionary objectForKey:@"courseDescription"];
                        model.subjectName = [dictionary objectForKey:@"subjectName"];
                        model.scheduleID=[dictionary objectForKey:@"scheduleId"];
                     
                            [newData addObject:model];
                            [resultArray addObject:model];
                       
                    }
                    NSRange range = NSMakeRange(dataArray.count,newData.count );
                    NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                    [dataArray insertObjects:newData atIndexes:set];
                    [courseTable reloadData];
                }
            }
            else{
                currentPage--;
                [MBProgressHUD showToastAndMessage:@"请求数据失败!" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            currentPage--;
            [courseTable footerEndRefreshing];
            if (![result isEqualToString:@"200"]) {
                [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            }else{
                [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
            }
        }];
}
- (void)historyAction:(UIButton *)button{
    HYStudyHistoryViewController *studyVC = [HYStudyHistoryViewController new];
    studyVC.comptArray = completeArray;
    [self.navigationController pushViewController:studyVC animated:YES];
}




////筛选：
//- (void)selectAction:(UIButton *)button{
//
//
//
//    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//    UIAlertAction *ApplyOk=[UIAlertAction actionWithTitle:@"已报名" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//
//        TempStr=@"已报名";
//        IndexNums = 0;
//        [self setUpRefresh];
//
//    }];
//
//    UIAlertAction *ApplyNO=[UIAlertAction actionWithTitle:@"未报名" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//
//        TempStr=@"未报名";
//        IndexNums = 1;
//         [self setUpRefresh];
//
//    }];
//
//    UIAlertAction *Succeed=[UIAlertAction actionWithTitle:@"已完成" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//
//        TempStr=@"已报名";
//        IndexNums = 2;
//           [self setUpRefresh];
//
//    }];
//
//    UIAlertAction *Failure=[UIAlertAction actionWithTitle:@"未完成" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//
//        TempStr=@"已报名";
//        IndexNums = 3;
//        [self setUpRefresh];
//
//
//
//    }];
//
//    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//
//    }];
//
//     [AlertView addAction:BackAction];
//     [AlertView addAction:ApplyOk];
//     [AlertView addAction:ApplyNO];
//     [AlertView addAction:Succeed];
//     [AlertView addAction:Failure];
//
// [self presentViewController:AlertView animated:YES completion:nil];
//}
//

#pragma -tableView协议


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    HYCourseDetailController *detailVC = [HYCourseDetailController new];
    
     HYCourseModel *model = dataArray[indexPath.row];
    
    detailVC.model =model;
    
    [self.navigationController pushViewController:detailVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HYCourseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"courseCell"];
    
    HYCourseModel *model = dataArray[indexPath.row];
    [cell setProperty:model];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if([TempStr isEqualToString:@"已报名"]){
        [cell.courseBtn setTitle:@"已报名" forState:UIControlStateNormal];
        
    }else if([TempStr isEqualToString:@"未报名"]){
         [cell.courseBtn setTitle:@"报名" forState:UIControlStateNormal];
         cell.courseBtn.tag=100+indexPath.row;
        [cell.courseBtn addTarget:self action:@selector(aut:) forControlEvents:UIControlEventTouchUpInside];
    }else if([TempStr isEqualToString:@"已完成"]){
        [cell.courseBtn setTitle:@"查看" forState:UIControlStateNormal];
    }
    
    
    NSString *content=model.courseName;
    CGFloat test_h=[Maneger getPonentH:content andFont:[UIFont systemFontOfSize:15] andWidth:Swidth-300]+60;
    CGRect frame=cell.frame;
    frame.size.height=test_h+60;
    cell.frame=frame;

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    HYCourseCell *cell =(HYCourseCell *)[self tableView:tableView  cellForRowAtIndexPath:indexPath];
    
    return cell.frame.size.height;
}


-(void)aut:(UIButton *)sender{
    
  NSInteger line = sender.tag-100;
    NSLog(@"line1:%ld",line);
  //  NSIndexPath *indexPath = [NSIndexPath indexPathForItem: line inSection:0];
    
  //  HYCourseCell *cell = [courseTable cellForRowAtIndexPath:indexPath];
    HYCourseModel *model = dataArray[line];
    
    
    NSString *Url=[NSString stringWithFormat:@"%@/courseSchedules/%@/registeredStudents/%@",LocalIP,model.scheduleID,LocalUserId];
    
    NSLog(@"scheduleID:%@",model.scheduleID);
    
    [RequestTools RequestWithURL:Url Method:@"post" Params:nil Success:^(NSDictionary *result) {
       
        NSLog(@"%@",result);
        
        NSString *resultStr=[result objectForKey:@"errorCode"];
        NSLog(@"resultStr:%@",resultStr);
        
        if([resultStr isKindOfClass:[NSNull class]]){
            NSLog(@"报名成功");
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"报名成功！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [courseTable reloadData];
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else if([resultStr isEqualToString:@"exceed_deadline"]){
            
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"已超过报名时间，无法正常报名！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
               
            }];

            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];

            
        }else if([resultStr isEqualToString:@"registration_not_start"]){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"未到报名时间，无法正常报名！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    } failed:^(NSString *result) {
        NSLog(@"报名失败");
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
   
}

#pragma mark UIPickerView

//该方法返回控件该控件包含多少列
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}
//该方法返回值决定该控件的制定列包含多少个列表项
-(NSInteger)pickerView:(UIPickerView *)pickerView                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return CourseSrate.count;
    }
    return CourseType.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //若果是第一列，则返回authors中的元素的个数
    //即authors有多少个元素，那么第一列就包含多少个列表项
    if (component == 0) {
        return [CourseSrate objectAtIndex:row];
    }
    //否则其他列返回的个数是books中的键值对中值对应的数组地元素的个数
    return [CourseType objectAtIndex:row];
}
-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    //如果是第一列，则宽度是90
    if (component == 0  ) {
        return 120;
    }
    return 150;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (component == 0) {
        //改变选中的作者
        NSLog(@"课程状态:%@",CourseSrateStr);
        CourseSrateStr=[CourseSrate objectAtIndex:row];
    }else{
        NSLog(@"课程类型:%@",CourseTypeStr);
        CourseTypeStr=[CourseType objectAtIndex:row];
        
    }
    //控制重写加载第二个列表，根据选中的作者来加载第二个列表项
    
    
}





#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end

