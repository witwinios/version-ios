//
//  SelfPaperController.h
//  xiaoyun
//
//  Created by MacAir on 17/2/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelfTestModel.h"
#import "LoadingView.h"
#import "PaperCell.h"
#import "PaperCellModel.h"
#import "PaperDeatailController.h"
#import "RequestTools.h"
#import "MakePaperController.h"
@interface SelfPaperController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (strong,nonatomic) SelfTestModel *model;
@property (strong, nonatomic) NSString *fromVC;
@end
