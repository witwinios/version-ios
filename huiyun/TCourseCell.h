//
//  TCourseCell.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseModel.h"
@interface TCourseCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *courseName;
@property (weak, nonatomic) IBOutlet UILabel *courseStatus;
@property (weak, nonatomic) IBOutlet UILabel *courseType;
@property (weak, nonatomic) IBOutlet UILabel *coursePlace;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
- (void)setProperty:(TCourseModel *)model;
@end
