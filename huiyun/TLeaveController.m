//
//  LeaveHistoryController.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/10.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TLeaveController.h"

@interface TLeaveController ()
{
    UITableView *leaveTable;
    NSMutableArray *dataArray;
    LoadingView *loadView;
    int currentPage;
    
    int Status;
    
    NSMutableArray *newArray;
    
    UIView *SearchView;
    UITextField *SearchField;
    UIButton *SearchBtn;
    
     BOOL isSearch;
}
@end

@implementation TLeaveController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    currentPage = 1;
    [leaveTable headerBeginRefreshing];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setsearchBar];
    [self setObj];
    [self setUpRefresh];
}
- (void)setUpRefresh{
    [leaveTable addHeaderWithTarget:self action:@selector(downRefresh)];
    //设置文字
    leaveTable.headerPullToRefreshText = @"下拉刷新";
    leaveTable.headerReleaseToRefreshText = @"松开进行刷新";
    leaveTable.headerRefreshingText = @"刷新中。。。";
    [leaveTable headerBeginRefreshing];
    
//    [leaveTable addFooterWithTarget:self action:@selector(loadMoreRefresh)];
//    leaveTable.footerPullToRefreshText = @"上拉加载";
//    leaveTable.footerReleaseToRefreshText = @"松开进行加载";
//    leaveTable.footerRefreshingText = @"加载中。。。";
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];

    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"请假列表";
    [backNavigation addSubview:titleLab];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-100, 29.5, 100, 25);
    [rightBtn setImage:[UIImage imageNamed:@"筛选"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(AddBtn) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    Status=0;
    
}

-(void)setsearchBar{
    
   
    isSearch=NO;
    
    SearchView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 44)];
    SearchView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    UIImageView *SearchImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sousuo"]];
    SearchImg.backgroundColor=[UIColor clearColor];
    SearchImg.frame=CGRectMake(10, 6, 32, 32);
    
    
    SearchField=[[UITextField alloc]initWithFrame:CGRectMake(44, 6, Swidth-88, 32)];
    SearchField.backgroundColor=[UIColor clearColor];
    SearchField.placeholder=@"请输入搜索的学生姓名";
    SearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    SearchField.clearsOnBeginEditing = YES;
    SearchField.keyboardType=UIKeyboardTypeDefault;
    SearchField.returnKeyType=UIReturnKeySearch;
    SearchField.delegate = self;
    [SearchField addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    SearchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SearchBtn.frame=CGRectMake((SearchField.wwy_x+SearchField.wwy_width)+2, 6, 40, 32);
    [SearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [SearchBtn addTarget:self action:@selector(setUpRefresh) forControlEvents:UIControlEventTouchUpInside];
    SearchBtn.backgroundColor=UIColorFromHex(0x20B2AA);
    SearchBtn.layer.cornerRadius=5;
    SearchBtn.hidden=YES;
    [SearchView addSubview:SearchBtn];
    [SearchView addSubview:SearchField];
    [SearchView addSubview:SearchImg];
    [self.view addSubview:SearchView];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [SearchField  resignFirstResponder];
    [self setUpRefresh ];
    NSLog(@"点击了搜索");
    
    return YES;
    
}


-(BOOL)ApplyTextDidChange:(UITextField *)theTextField{
    
    NSLog( @"text changed: %@", theTextField.text);
    
    
    if (![theTextField.text isEqualToString:@""]) {
        SearchBtn.hidden=NO;
        isSearch=YES;
    }else{
        isSearch=NO;
        [self setUpRefresh];
        SearchBtn.hidden=YES;
        [SearchField  resignFirstResponder];
    }
    
    return YES;
    
}


- (void)setObj{
    newArray=[NSMutableArray array];
    dataArray = [NSMutableArray array];
    leaveTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 108, Swidth, Sheight-108) style:UITableViewStylePlain];
    leaveTable.delegate = self;
    leaveTable.dataSource = self;
    [leaveTable registerNib:[UINib nibWithNibName:@"HistoryCell" bundle:nil] forCellReuseIdentifier:@"historyCell"];
    leaveTable.tableFooterView.frame = CGRectZero;
    [self.view addSubview:leaveTable];
    //
}

-(void)AddBtn{
    
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
        UIAlertAction *ApplyOk=[UIAlertAction actionWithTitle:@"已通过" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    
            Status=0;
            [self setUpRefresh];
            NSLog(@"%d",Status);
        }];
    
        UIAlertAction *ApplyNO=[UIAlertAction actionWithTitle:@"未通过" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    
    
            Status=1;
            [self setUpRefresh];
            NSLog(@"%d",Status);
        }];
    
        UIAlertAction *Succeed=[UIAlertAction actionWithTitle:@"待审批" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    
         Status=2;
           [self setUpRefresh];
         NSLog(@"%d",Status);
        }];
    
    
        UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    
        }];
    
         [AlertView addAction:BackAction];
         [AlertView addAction:ApplyOk];
         [AlertView addAction:ApplyNO];
         [AlertView addAction:Succeed];
    
     [self presentViewController:AlertView animated:YES completion:nil];
    
    
    
}

- (void)downRefresh{
    currentPage =1;

    NSString *url;
    
    if (isSearch == NO) {
        switch (Status) {
            case 0:
                url=[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=999&approvalStatus=approved",LocalIP,currentPage];
                break;
            case 1:
                url=[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=999&approvalStatus=rejected",LocalIP,currentPage];
                break;
            case 2:
                url=[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=999&approvalStatus=waiting_approval",LocalIP,currentPage];
                break;
                
            default:
                break;
        }
    }else if(isSearch == YES){
         NSString *str=SearchField.text;
        switch (Status) {
            case 0:
                url=[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=999&approvalStatus=approved&fullName=%@",LocalIP,currentPage,str];
                url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            case 1:
                url=[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=999&approvalStatus=rejected&fullName=%@",LocalIP,currentPage,str];
                  url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            case 2:
                url=[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=999&approvalStatus=waiting_approval&fullName=%@",LocalIP,currentPage,str];
                  url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
                
            default:
                break;
        }
    }

    
    NSLog(@"%@",url);

        [RequestTools RequestWithURL:url Method:@"get" Params:nil Success:^(NSDictionary *result) {
            
            if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
                [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
                [leaveTable headerEndRefreshing];
                return;
            }

            [dataArray removeAllObjects];
            if ([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
                NSArray *array= [[result objectForKey:@"responseBody"] objectForKey:@"result"];
                if (array.count == 0) {
                    [MBProgressHUD showToastAndMessage:@"无请假记录~" places:0 toView:nil];
                    
                }else{
                    for (int i=0; i<array.count; i++) {
                        HistoryLeaveModel *model = [HistoryLeaveModel new];
                        NSDictionary *responseDic = array[i];
                        if ([[responseDic objectForKey:@"reviewer"]  isKindOfClass:[NSNull class]] || [responseDic objectForKey:@"reviewer"] == nil) {
                            model.approver = @"暂无";
                        }else{
                            model.approver = [[responseDic objectForKey:@"reviewer"] objectForKey:@"fullName"];
                        }
                        if ([[responseDic objectForKey:@"file"] isKindOfClass:[NSNull class]]|| [responseDic objectForKey:@"file"] == nil) {
                            model.fileUrl = @"";
                        }else{
                            model.fileUrl = [[responseDic objectForKey:@"file"] objectForKey:@"fileUrl"];
                        }
                        model.leaveRequestId = [responseDic objectForKey:@"leaveRequestId"];
                        model.leaveRequestUserName=[[responseDic objectForKey:@"user"]objectForKey:@"userName"];
                        model.reviewComments = [responseDic objectForKey:@"reviewerComments"];
                        model.approveStatus = [responseDic objectForKey:@"approvalStatus"];
                        model.leaveStartTime = [responseDic objectForKey:@"leaveTimeStart"];
                        model.leaveEndTime = [responseDic objectForKey:@"leaveTimeEnd"];
                        model.leaveType = [responseDic objectForKey:@"leaveType"];
                        model.leaveReason = [responseDic objectForKey:@"leaveReason"];
                        model.fileId = [responseDic objectForKey:@"file"];
                        model.requestName = [[responseDic objectForKey:@"user"] objectForKey:@"fullName"];
                        
                        NSMutableArray *attendanceItemArr=[NSMutableArray array];
                        attendanceItemArr=[responseDic objectForKey:@"leaveRecords"];
                        
                        NSLog(@"attendanceItemArr=%lu",(unsigned long)attendanceItemArr.count);
                        
                        if (![attendanceItemArr isKindOfClass:[NSNull class]]) {
                            for (int i=0; i<attendanceItemArr.count; i++) {
                                NSDictionary *dic=[attendanceItemArr objectAtIndex:i];
                                HistoryLeaveModel *Model = [HistoryLeaveModel new];
                                Model.AttendanceItem=[dic objectForKey:@"attendanceItem"];
                                [model.AttendanceItemArr addObject:Model];
                            }
                        }
                        
                        
                        
                        PersonEntity *entity = [[NSuserDefaultManager share] readCurrentUser];
    
                        NSLog(@"userName==%@",model.leaveRequestUserName);
    
    
                        if ([model.approver isEqualToString:entity.userFullName]) {
                            [dataArray addObject:model];
                            
                            if(dataArray.count == 0){
                                [MBProgressHUD showToastAndMessage:@"当前教师无请假信息" places:0 toView:nil];
                                [leaveTable headerEndRefreshing];
                            }
                           
                        }
    
                    }
                    [leaveTable reloadData];
                    [leaveTable headerEndRefreshing];
                }
               
            }else{
                [MBProgressHUD showToastAndMessage:@"加载错误~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
             [MBProgressHUD showToastAndMessage:@"加载错误~" places:0 toView:nil];
            NSLog(@"%@",result);
        }];
}
//- (void)loadMoreRefresh{
//    currentPage ++;
//
//    NSString *url;
//
//    switch (Status) {
//        case 0:
//            url=[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=20&approvalStatus=approved",LocalIP,currentPage];
//            break;
//        case 1:
//            url=[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=20&approvalStatus=rejected",LocalIP,currentPage];
//            break;
//        case 2:
//            url=[NSString stringWithFormat:@"%@/attendance/leaveRequests?pageStart=%d&pageSize=20&approvalStatus=waiting_approval",LocalIP,currentPage];
//            break;
//
//        default:
//            break;
//    }
//    NSLog(@"%@",url);
//
//    [RequestTools RequestWithURL:url Method:@"get" Params:nil Success:^(NSDictionary *result) {
//
//        [dataArray removeAllObjects];
//        NSMutableArray *newData=[NSMutableArray new];
//        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
//            NSArray *array= [[result objectForKey:@"responseBody"] objectForKey:@"result"];
//            if (array.count == 0) {
//                [MBProgressHUD showToastAndMessage:@"无请假记录~" places:0 toView:nil];
//                [leaveTable footerEndRefreshing];
//            }else{
//                for (int i=0; i<array.count; i++) {
//                    HistoryLeaveModel *model = [HistoryLeaveModel new];
//                    NSDictionary *responseDic = array[i];
//                    if ([[responseDic objectForKey:@"reviewer"]  isKindOfClass:[NSNull class]] || [responseDic objectForKey:@"reviewer"] == nil) {
//                        model.approver = @"暂无";
//                    }else{
//                        model.approver = [[responseDic objectForKey:@"reviewer"] objectForKey:@"fullName"];
//                    }
//                    if ([[responseDic objectForKey:@"file"] isKindOfClass:[NSNull class]]|| [responseDic objectForKey:@"file"] == nil) {
//                        model.fileUrl = @"";
//                    }else{
//                        model.fileUrl = [[responseDic objectForKey:@"file"] objectForKey:@"fileUrl"];
//                    }
//                    model.leaveRequestId = [responseDic objectForKey:@"leaveRequestId"];
//                    model.leaveRequestUserName=[[responseDic objectForKey:@"user"]objectForKey:@"userName"];
//                    model.reviewComments = [responseDic objectForKey:@"reviewerComments"];
//                    model.approveStatus = [responseDic objectForKey:@"approvalStatus"];
//                    model.leaveStartTime = [responseDic objectForKey:@"leaveTimeStart"];
//                    model.leaveEndTime = [responseDic objectForKey:@"leaveTimeEnd"];
//                    model.leaveType = [responseDic objectForKey:@"leaveType"];
//                    model.leaveReason = [responseDic objectForKey:@"leaveReason"];
//                    model.fileId = [responseDic objectForKey:@"file"];
//                    model.requestName = [[responseDic objectForKey:@"user"] objectForKey:@"userName"];
//                    PersonEntity *entity = [[NSuserDefaultManager share] readCurrentUser];
//
//                    NSLog(@"userName==%@",model.leaveRequestUserName);
//
//
//                    if ([model.approver isEqualToString:entity.userFullName]) {
//                        [newData addObject:model];
//
//                    }
//
//                }
//
//                NSRange range = NSMakeRange(dataArray.count,newData.count );
//                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
//                [dataArray insertObjects:newData atIndexes:set];
//
//                [leaveTable reloadData];
//
//            }
//
//        }else{
//             currentPage--;
//            [MBProgressHUD showToastAndMessage:@"加载错误~" places:0 toView:nil];
//        }
//    } failed:^(NSString *result) {
//         currentPage--;
//        [MBProgressHUD showToastAndMessage:@"加载错误~" places:0 toView:nil];
//        NSLog(@"%@",result);
//    }];
//}
- (void)selectAction:(NSNumber *)requestId{
    
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)deleteAction:(UIButton *)btn{
    leaveTable.editing = !leaveTable.editing;
    if (leaveTable.editing) {
        [btn setTitle:@"完成" forState:UIControlStateNormal];
    }else{
        [btn setTitle:@"编辑" forState:UIControlStateNormal];
    }
}
#pragma -协议
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 109.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.tleaveDetailVC = [TLeaveDetailController new];
    self.tleaveDetailVC.model = dataArray[indexPath.row];
   
    [self.navigationController pushViewController:self.tleaveDetailVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"historyCell"];
    [cell setPropertys:dataArray[indexPath.row]];
    cell.OneLabel.text=@"学生姓名:";
    return cell;
}
#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
