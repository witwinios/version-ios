//
//  TCourseAddViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/10.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TCourseAddViewController.h"
#import "WPhotoViewController.h"
#import "TCourseMoldViewController.h"
#import "TCourseAddModel.h"
#import "Maneger.h"
#import "RequestTools.h"
#import "THDatePickerView.h"
#define phoneScale [UIScreen mainScreen].bounds.size.width/720.0
@interface TCourseAddViewController ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,PassingValueDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    
    UIView *BackView;
    UIButton *BackBtn;
    
    UIButton *okBtn;
    UIButton *noBtn;
    
    UIDatePicker *DatePick;
    UIPickerView *Picker;
    
    NSMutableArray *PickDateLocation;
    NSMutableArray *PickDateSubject;
    
    NSString *DateString;
    
    UITableView *_tableView;
    NSMutableArray *_photosArr;
    
    NSMutableArray *LocationID;
    NSString *LocationString;
    
    NSMutableArray *SubjectID;
    NSString *SubjectString;
    

    NSString *StrateTimeLong;
    NSString *EndTimeLong;
    
    NSString *MoldString;
    
    NSMutableArray *FileID;
    NSMutableArray *FileString;
    NSNumber *has_image;
    
    NSString *currentTimeString;
    
    long EndTimeNum;
    long StartTimeNunm;
    long CurrentTimeNunm;
    
    THDatePickerView *dateView;
    
    NSMutableArray *FileArray;
    NSString *TextViewStr;
    
    
    UIAlertController *alert;
}

@end

@implementation TCourseAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    has_image = @0;
    
    //加载数据
    [self loadData];
    //获取当前时间
    [self SetNewTime];
    //加载UI控件：
    [self setUI];
    
   

    
    
  
    
}

#pragma  mark 加载数据
-(void)loadData{
    
    //获取上课地点：
    NSString *UrlLocation=[NSString stringWithFormat:@"%@/rooms?roomName=&pageSize=999",LocalIP];
    [RequestTools RequestWithURL:UrlLocation Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array=[[result objectForKey:@"responseBody"]objectForKey:@"result"];
            if (array.count == 0) {
                [Maneger showAlert:@"暂无课程地址" andCurentVC:self];
            }else{
                
                PickDateLocation=[NSMutableArray new];
                LocationID=[NSMutableArray new];
                
                for(int i=0;i<array.count;i++){
                    NSDictionary *dic=[array objectAtIndex:i];
                    [PickDateLocation addObject:[dic objectForKey:@"roomName"]];
                    
                    [LocationID addObject:[dic objectForKey:@"roomId"]];
                
                    
                    
                    
                    
                    [Picker reloadAllComponents];
                }
           
            };
        }
    } failed:^(NSString *result) {
        NSLog(@"请求错误");
    }];
    
    //获取课程分类:
    NSString *UrlMold=[NSString stringWithFormat:@"%@/courseCategories?pageSize=200",LocalIP];
    [RequestTools RequestWithURL:UrlMold Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array=[[result objectForKey:@"responseBody"]objectForKey:@"result"];
            
            if (array.count == 0) {
                [Maneger showAlert:@"暂无课程分类" andCurentVC:self];
                
            }else{
                
                PickDateSubject=[NSMutableArray new];
                SubjectID=[NSMutableArray new];
                for(int i=0;i<array.count;i++){
                    NSDictionary *dic=[array objectAtIndex:i];
                    
                    [PickDateSubject addObject:[dic objectForKey:@"categoryName"]];
                    
                     [SubjectID addObject:[dic objectForKey:@"categoryId"]];
 
                    [Picker reloadAllComponents];
                }
              //  NSLog(@"SubjectID:%@",SubjectID);
            };
        }
    } failed:^(NSString *result) {
        NSLog(@"请求错误");
    }];
}




-(void)setUI{
    
    //课程名称；
    
    NSMutableAttributedString *threeStr = [[NSMutableAttributedString alloc] initWithString:@"*课程名称:"];
    [threeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.CourseNameLabel.attributedText = threeStr;

    
    self.CourseName.borderStyle=UITextBorderStyleRoundedRect; //边框样式
    self.CourseName.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.CourseName.clearsOnBeginEditing = YES;
    self.CourseName.keyboardType=UIKeyboardTypeDefault;
    self.CourseName.returnKeyType=UIReturnKeyDefault;
    self.CourseName.delegate = self;
    self.CourseName.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseName.layer.borderWidth=1;
    self.CourseName.layer.cornerRadius=5;
    
    
    
    
    //课程科目：
    NSMutableAttributedString *threeStr5 = [[NSMutableAttributedString alloc] initWithString:@"*课程科目:"];
    [threeStr5 addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.CourseSubjectLabel.attributedText = threeStr5;
    
    self.CourseSubject.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseSubject.layer.borderWidth=1;
    self.CourseSubject.layer.cornerRadius=5;
    
    //课程分类；
    NSMutableAttributedString *threeStr4 = [[NSMutableAttributedString alloc] initWithString:@"*课程分类:"];
    [threeStr4 addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.CourseMoldLabel.attributedText = threeStr4;
   
    
    self.CourseMold.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseMold.layer.borderWidth=1;
    self.CourseMold.layer.cornerRadius=5;
    
    //课程描述：
    self.CourExplain.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourExplain.layer.borderWidth=1;
    self.CourExplain.delegate=self;
    self.CourExplain.layer.cornerRadius=5;
    
    //上课时间：
    
    BackBtn=[[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    BackBtn.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
    BackBtn.hidden=YES;
    [self.view addSubview:BackBtn];
    
    NSMutableAttributedString *threeStr2 = [[NSMutableAttributedString alloc] initWithString:@"*上课时间:"];
    [threeStr2 addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.StartTimeLabel.attributedText = threeStr2;
    
    self.StartTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.StartTime.layer.borderWidth=1;
    self.StartTime.layer.cornerRadius=5;
    self.StartTime.titleLabel.font=[UIFont systemFontOfSize:11];
    
  
    self.EndTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.EndTime.layer.borderWidth=1;
    self.EndTime.layer.cornerRadius=5;
    self.EndTime.titleLabel.font=[UIFont systemFontOfSize:11];
    
    
    //上课地址：
    NSMutableAttributedString *threeStr3 = [[NSMutableAttributedString alloc] initWithString:@"*上课地点:"];
    [threeStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.LocationLabel.attributedText = threeStr3;
    
    self.CourseLocation.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseLocation.layer.borderWidth=1;
    self.CourseLocation.layer.cornerRadius=5;
    

    //附件：
    
    
    //保存：
    self.SaveBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.SaveBtn.layer.borderWidth=1;
    self.SaveBtn.layer.cornerRadius=5;
    
     FileArray=[NSMutableArray new];

}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.CourseName resignFirstResponder];
    [self.CourExplain resignFirstResponder];
}


#pragma mark 回退
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark 课程名称：
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.CourseName  resignFirstResponder];    //主要是[receiver resignFirstResponder]在哪调用就能把receiver对应的键盘往下收
    return YES;
}



#pragma mark 课程分类：
- (IBAction)AddMoldAction:(id)sender {
       [self resignFirstRespond];
    if(BackView !=nil){
        BackView.hidden=YES;
    }
    
    
    CGFloat BackView_Y=Sheight/3;
    BackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    BackView.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.4];
    
    //工具栏：
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0,Sheight-BackView_Y , Swidth, BackView_Y)];
    UIView *ToolView=[[UIView alloc]initWithFrame:CGRectMake(0, Picker.wwy_y-30, Swidth, 30)];
    ToolView.backgroundColor=UIColorFromHex(0x20B2AA);
    [BackView addSubview:ToolView];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn4:) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:noBtn];
    

     
    Picker.backgroundColor=[UIColor whiteColor];
    Picker.tag=0;
    Picker.dataSource=self;
    Picker.delegate=self;
    if (Picker.tag == 0) {
        DateString = PickDateSubject[0];
        SubjectString=SubjectID[0];
    }
    
    [BackView addSubview:Picker];
    
     [[UIApplication sharedApplication].keyWindow addSubview:BackView];
    
    
}

-(void)setOkBtn4:(id)sender{
    
    if(DateString == nil){

        [self.CourseMold setTitle:@" " forState:UIControlStateNormal];
        
        
    BackView.hidden=YES;
    }else{
    
  [self.CourseMold setTitle:DateString forState:UIControlStateNormal];
    BackView.hidden=YES;
    }
}


#pragma mark 课程科目：


- (IBAction)AddSubjectAction:(id)sender {
    [self resignFirstRespond];
    TCourseMoldViewController *Mold=[[TCourseMoldViewController alloc]init];
    Mold.delegate=self;
    [self.navigationController pushViewController:Mold animated:YES];
}


-(void)text:(NSString *)str dateID:(NSString *)dateID{
    NSLog(@"%@",str);
    [self.CourseSubject setTitle:str forState:UIControlStateNormal];
    MoldString=dateID;
    

}

#pragma mark 课程描述：
-(void)textViewDidChange:(UITextView *)textView{
    self.stirngLenghLabel.text = [NSString stringWithFormat:@"%lu/255", (unsigned long)textView.text.length];
    
    TextViewStr=textView.text;
    NSLog(@"%@",TextViewStr);
    
    //字数限制操作
    if (textView.text.length >= 256) {
        textView.text = [textView.text substringToIndex:256];
        self.stirngLenghLabel.text = @"255/255";
    }
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark 上课时间:


-(void)ChooseStartTime{

     CGFloat BackView_Y=Sheight/3+10;
    dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight-BackView_Y, Swidth, BackView_Y) andSureBlock:^(NSString *date, NSString *longDate) {
        BackBtn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
            [self.StartTime setTitle:date forState:0];
            StrateTimeLong = longDate;
       StartTimeNunm=[longDate longLongValue];
        
        
        if (StartTimeNunm<CurrentTimeNunm) {
            UIAlertController *alert2=[UIAlertController alertControllerWithTitle:@"提示" message:@"上课时间早于当前时间，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction2=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                self.StartTime.layer.borderColor=[UIColor redColor].CGColor;
                
            }];
            
            [alert2 addAction:okAction2];
            [self presentViewController:alert2 animated:true completion:nil];
            
        }else{
            [self.StartTime setTitle:date forState:0];
            self.StartTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
        }
    } andCancelBlock:^{
        
        BackBtn.hidden = YES;

        
    }];
    [self.view addSubview:dateView];

}
-(void)SetNewTime{
    //获取当前时间
    NSDate *datenow = [NSDate date];
    
    NSString* currentTime=[NSString stringWithFormat:@"%0.f",[datenow timeIntervalSince1970]*1000];
    
    currentTimeString =currentTime;
    NSLog(@"currentTimeString:%@",currentTimeString);
    
    CurrentTimeNunm =[currentTimeString longLongValue];
}

- (IBAction)StartAction:(id)sender {
    //选择时间控件：
    

        [self ChooseStartTime];
        BackBtn.hidden = NO;
        [self resignFirstRespond];
        [dateView show];

    
  
}

//隐藏键盘
- (void)resignFirstRespond{
    [self.CourseName resignFirstResponder];
    [self.CourExplain resignFirstResponder];
}

#pragma mark 结束时间：

- (IBAction)EndAction:(id)sender {
    
    //选择时间控件：
    [self ChooseEndTime];
    BackBtn.hidden = NO;
    [self resignFirstRespond];
    [dateView show];
    
}

-(void)ChooseEndTime{

      CGFloat BackView_Y=Sheight/3+10;
    dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight-BackView_Y, Swidth, BackView_Y) andSureBlock:^(NSString *date, NSString *longDate) {
        BackBtn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
         EndTimeNum=[longDate longLongValue];
        EndTimeLong = longDate;
        if (EndTimeNum<StartTimeNunm) {
            UIAlertController *alert2=[UIAlertController alertControllerWithTitle:@"提示" message:@"结束时间早于上课时间，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction2=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
                            self.EndTime.layer.borderColor=[UIColor redColor].CGColor;
            
                        }];
            
                        [alert2 addAction:okAction2];
                        [self presentViewController:alert2 animated:true completion:nil];
            
        }else{
            [self.EndTime setTitle:date forState:0];
             self.EndTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
        }

    } andCancelBlock:^{
        BackBtn.hidden = YES;
    }];
    [self.view addSubview:dateView];

}



#pragma mark 上课地址：

- (IBAction)AddLocationAction:(id)sender {
    [self resignFirstRespond];
    if(BackView !=nil){
        BackView.hidden=YES;
    }
    
    CGFloat BackView_Y=Sheight/3;
    BackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    BackView.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.4];
    
    //工具栏：
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0,Sheight-BackView_Y , Swidth, BackView_Y)];
    UIView *ToolView=[[UIView alloc]initWithFrame:CGRectMake(0, Picker.wwy_y-30, Swidth, 30)];
    ToolView.backgroundColor=UIColorFromHex(0x20B2AA);
    [BackView addSubview:ToolView];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn3:) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:noBtn];
    
    
  
    Picker.backgroundColor=[UIColor whiteColor];
    Picker.tag=1;
    Picker.dataSource=self;
    Picker.delegate=self;
    if (Picker.tag == 1) {
        LocationString = PickDateLocation[0];
        SubjectString=SubjectID[0];
    }
    
    [BackView addSubview:Picker];
    
    [[UIApplication sharedApplication].keyWindow addSubview:BackView];
}

-(void)setNoBtn{
    BackView.hidden=YES;
}

//返回数据列数：
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    
    if(Picker.tag==0){
        return [PickDateSubject count];
    }else{
    
    return [PickDateLocation count];
    }
}

//获取数据
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
     if(Picker.tag==0){
         return [PickDateSubject objectAtIndex:row];
     }else{
        return [PickDateLocation objectAtIndex:row];
     }
}
//返回值：
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    
    if(Picker.tag==0){
        NSLog(@"%@",[PickDateSubject objectAtIndex:row]);
        DateString = [PickDateSubject objectAtIndex:row];
        
        SubjectString=[SubjectID objectAtIndex:row];
        NSLog(@"%@",SubjectString);
    }else{
        NSLog(@"%@",[PickDateLocation objectAtIndex:row]);
        DateString = [PickDateLocation objectAtIndex:row];
        
        LocationString=[LocationID objectAtIndex:row];
        NSLog(@"%@",LocationString);
    }
}


-(void)setOkBtn3:(id)sender{

    [self.CourseLocation setTitle:DateString forState:UIControlStateNormal];
    
    BackView.hidden=YES;
}


#pragma mark 添加附件；
- (IBAction)CourseOtherBtnAction:(id)sender {

    
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"添加附件" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *AddAction = [UIAlertAction actionWithTitle:@"图片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        ZCAssetsPickerViewController *vc=[[ZCAssetsPickerViewController alloc]init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        vc.type = ChooseTypePhoto;
        vc.maximumNumbernMedia = 9;
        vc.delegate = self;
        
        vc.backArr=^(NSArray *arr){
            NSLog(@"arr：%@",arr);
            [FileArray addObjectsFromArray:arr];
            
        };
        
        
        vc.CourseStr=@"新建";
        
        [self presentViewController:nav animated:YES completion:nil];
       
        
        
    }];
    UIAlertAction *EditAction = [UIAlertAction actionWithTitle:@"视频" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
        ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//sourcetype有三种分别是camera，photoLibrary和photoAlbum
        NSArray *availableMedia = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];//Camera所支持的Media格式都有哪些,共有两个分别是@"public.image",@"public.movie"
        ipc.mediaTypes = [NSArray arrayWithObject:availableMedia[1]];//设置媒体类型为public.movie
        [self presentViewController:ipc animated:YES completion:nil];
        ipc.delegate = self;//设置委托
        
        
    }];
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
    [AlertView addAction:BackAction];
    [AlertView addAction:AddAction];
    [AlertView addAction:EditAction];
    
    [self presentViewController:AlertView animated:YES completion:nil];

}



#pragma mark 保存(发布课程)
- (IBAction)SaveAction:(id)sender {
    
  

    if([self.CourseName.text isEqualToString:@"" ]|| [self.CourseName.text length]==0){
        NSLog(@"课程名称为空");
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"课程名称为空，请填写。" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        }];

        [alert addAction:okAction];
        [self presentViewController:alert animated:true completion:nil];

    }else if([self.StartTime.titleLabel.text isEqualToString:@"上课时间"]){
        NSLog(@"开始时间选择为空");

        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"开始时间选择为空，请选择。" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        }];

        [alert addAction:okAction];
        [self presentViewController:alert animated:true completion:nil];

    }else if([self.EndTime.titleLabel.text isEqualToString:@"下课时间"]){
        NSLog(@"结束时间选择为空");

        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"结束时间选择为空，请选择。" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        }];

        [alert addAction:okAction];
        [self presentViewController:alert animated:true completion:nil];


    }else if([self.CourseLocation.titleLabel.text isEqualToString:@"请选择上课地点"]){
        NSLog(@"上课地点为空");

        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"上课地点为空，请选择。" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        }];

        [alert addAction:okAction];
        [self presentViewController:alert animated:true completion:nil];

    }else  if([EndTimeLong doubleValue]<[StrateTimeLong doubleValue]){
       
       
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"上课结束时间早于上课开始时间，请选择。" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:true completion:nil];
        
        
        
        
    }else if([StrateTimeLong doubleValue]<[currentTimeString doubleValue]){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"上课时间早于当前时间，请选择。" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:true completion:nil];
    }else if([self.StartTime.titleLabel.text isEqualToString:self.EndTime.titleLabel.text]){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"开始时间与结束时间相同，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:true completion:nil];
 
    }else {

            NSLog(@"开始时间:%@",StrateTimeLong);
            NSLog(@"结束时间:%@",EndTimeLong);
            NSLog(@"上课地址:%@",LocationID);
            NSLog(@"课程名称:%@",self.CourseName.text);
            NSLog(@"课程类别:%@",MoldString);
            NSLog(@"课程科目:%@",SubjectID);
            NSLog(@"课程描述:%@",self.CourExplain.text);

            if(MoldString.length == 0 || TextViewStr.length == 0){
                MoldString=@"";
                self.CourExplain.text=@"暂无";
            }


            //发布课程
            NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules/saveCourseAndSchedule",LocalIP];

        NSDictionary *params;
        if(FileArray.count>0){
            params=@{
                     @"startTime":StrateTimeLong,
                     @"endTime":EndTimeLong,
                     @"classroomId":LocationString,
                     @"courseName":self.CourseName.text,
                     @"categoryId":SubjectString,
                     @"subjectId":MoldString,
                     @"description":self.CourExplain.text,
                     @"fileIds":FileArray
                     };
        }else{
            params=@{
                     @"startTime":StrateTimeLong,
                     @"endTime":EndTimeLong,
                     @"classroomId":LocationString,
                     @"courseName":self.CourseName.text,
                     @"categoryId":SubjectString,
                     @"subjectId":MoldString,
                     @"description":self.CourExplain.text
                     };
        }

            NSLog(@"%@",params);

            [RequestTools RequestWithURL:URL Method:@"post" Params:params Success:^(NSDictionary *result) {
                NSLog(@"%@",result);
                NSString *resultStr=[result objectForKey:@"errorCode"];
                NSLog(@"resultStr:%@",resultStr);
                
                if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
                    [MBProgressHUD showToastAndMessage:@"创建课程成功!" places:0 toView:nil];
                    [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }else if ([resultStr isEqualToString:@"duplicate_room_in_course_schedule"]) {
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"同一时间该教室已被选中，无法正常创建课程！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }else if([resultStr isEqualToString:@"invalid_scheduled_time"]){
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"填写时间有误，请核对后重新保存。温馨提示：此类状况多出现于报名时间于当前是时间相同。（报名时间应晚于当前时间并早于上课开始时间）" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }else  if([resultStr isEqualToString:@"duplicate_course_info"]){
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"重复的课程名称，请重新创建！！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }

            } failed:^(NSString *result) {
                if ([result isEqualToString:@"200"]) {
                    [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
                    NSLog(@"result:%@",result);
                }else{
                    [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
                }
            }];

    }

    
}

#pragma mark 视频：

- (NSString *)getCurrentTime{

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    //    NSString *str = [NSString stringWithFormat:@"%@mdxx",dateTime];
    //    NSString *tokenStr = [str stringToMD5:str];
    return dateTime;
    
}
- (NSURL *)condenseVideoNewUrl: (NSURL *)url{
    // 沙盒目录
    NSString *docuPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *destFilePath = [docuPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.MOV",[self getCurrentTime]]];
    NSURL *destUrl = [NSURL fileURLWithPath:destFilePath];
    //将视频文件copy到沙盒目录中
    NSFileManager *manager = [NSFileManager defaultManager];
    NSError *error = nil;
    [manager copyItemAtURL:url toURL:destUrl error:&error];
    NSLog(@"压缩前--%.2fk",[self getFileSize:destFilePath]);
    // 播放视频
    /*
     NSURL *videoURL = [NSURL fileURLWithPath:destFilePath];
     AVPlayer *player = [AVPlayer playerWithURL:videoURL];
     AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
     playerLayer.frame = self.view.bounds;
     [self.view.layer addSublayer:playerLayer];
     [player play];
     */
    // 进行压缩
    AVAsset *asset = [AVAsset assetWithURL:destUrl];
    //创建视频资源导出会话
    /**
     NSString *const AVAssetExportPresetLowQuality; // 低质量
     NSString *const AVAssetExportPresetMediumQuality;
     NSString *const AVAssetExportPresetHighestQuality; //高质量
     */
    
    AVAssetExportSession *session = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
    // 创建导出的url
    NSString *resultPath = [docuPath stringByAppendingPathComponent:[NSString stringWithFormat:@"lyhg%@.MOV",[self getCurrentTime]]];
    session.outputURL = [NSURL fileURLWithPath:resultPath];
    // 必须配置输出属性
    session.outputFileType = @"com.apple.quicktime-movie";
    // 导出视频
    [session exportAsynchronouslyWithCompletionHandler:^{
        NSLog(@"压缩后---%.2fk",[self getFileSize:resultPath]);
        NSLog(@"视频导出完成");
        
    }];
    
    return session.outputURL;
}
// 获取视频的大小
- (CGFloat) getFileSize:(NSString *)path
{
    NSFileManager *fileManager = [[NSFileManager alloc] init] ;
    float filesize = -1.0;
    if ([fileManager fileExistsAtPath:path]) {
        NSDictionary *fileDic = [fileManager attributesOfItemAtPath:path error:nil];//获取文件的属性
        unsigned long long size = [[fileDic objectForKey:NSFileSize] longLongValue];
        filesize = 1.0*size/1024;
    }
    return filesize;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    alert=[UIAlertController alertControllerWithTitle:@"文件上传" message:@"您选择的文件正在上传，请等待...." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString*)kUTTypeMovie]) {
        // 如果是视频
        NSURL *url = info[UIImagePickerControllerMediaURL];
        // 获取视频总时长
        CGFloat lengthTime = [self getVideoLength:url];
        NSLog(@"%f",lengthTime);
        // 保存视频至相册 (异步线程)
        //        NSString *urlStr = [url path];
        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //
        //            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(urlStr)) {
        //
        //                UISaveVideoAtPathToSavedPhotosAlbum(urlStr, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
        //            }
        //
        //        });
        
        //压缩视频
        //     NSData *videoData = [NSData dataWithContentsOfURL:[self condenseVideoNewUrl:url]];
        NSData *videoData = [NSData dataWithContentsOfURL:url];
        
        //视频上传
        
        
        NSDictionary *dict = @{@"username":@"syl"};
        
        
        
        NSString *FileUrl=[NSString stringWithFormat:@"%@/files",LocalIP];
        
        [RequestTools RequestUpLoadVideoWithParams:dict URL:FileUrl data:videoData Success:^(NSDictionary *result) {
            
            NSLog(@"%@",result);
            if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
                CourseAffixModel *model=[CourseAffixModel new];
                model.CreatedBy=[[result objectForKey:@"responseBody"]objectForKey:@"createdBy"];
                model.FileFormat=[[result objectForKey:@"responseBody"]objectForKey:@"fileFormat"];
                model.FileId=[[result objectForKey:@"responseBody"]objectForKey:@"fileId"];
                model.FileName=[[result objectForKey:@"responseBody"]objectForKey:@"fileName"];
                model.FileSize=[[result objectForKey:@"responseBody"]objectForKey:@"fileSize"];
                model.FileType=[[result objectForKey:@"responseBody"]objectForKey:@"fileType"];
                model.FileUrl=[[result objectForKey:@"responseBody"]objectForKey:@"fileUrl"];
                model.CreatedTime=[[result objectForKey:@"responseBody"]objectForKey:@"createdTime"];
                model.Description=[[result objectForKey:@"responseBody"]objectForKey:@"description"];
                model.IsPublic=[[result objectForKey:@"responseBody"]objectForKey:@"isPublic"];
                
                [FileArray addObject:model.FileId];
                
                
            }else{
                [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
            }
            NSLog(@"dataArrayCount=%lu",(unsigned long)FileArray.count);
            
        } faild:^(NSString *result) {
            NSLog(@"请求错误");
            NSLog(@"%@",result);
        } progress:^(NSProgress *progress) {
            NSLog(@"%f",progress.fractionCompleted);
            
            
            if (progress.fractionCompleted == 1) {
                alert.view.hidden=YES;
            }
            
            
        }];
    }
    

    

    [self dismissViewControllerAnimated:YES completion:nil];
}
// 获取视频时间
- (CGFloat) getVideoLength:(NSURL *)URL
{
    AVURLAsset *avUrl = [AVURLAsset assetWithURL:URL];
    CMTime time = [avUrl duration];
    int second = ceil(time.value/time.timescale);
    return second;
}
#pragma mark 图片保存完毕的回调
- (void) image: (UIImage *) image didFinishSavingWithError:(NSError *) error contextInfo: (void *)contextIn {
    NSLog(@"照片保存成功");
}

#pragma mark 视频保存完毕的回调
- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextIn {
    if (error) {
        NSLog(@"保存视频过程中发生错误，错误信息:%@",error.localizedDescription);
    }else{
        NSLog(@"视频保存成功.");
    }
}

@end
