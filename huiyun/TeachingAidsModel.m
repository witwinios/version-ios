//
//  TeachingAidsModel.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TeachingAidsModel.h"

@implementation TeachingAidsModel


-(void)setAidsName:(NSString *)AidsName{
    if ([AidsName isKindOfClass:[NSNull class]]) {
        _AidsName = @"暂无";
    }else{
        _AidsName = AidsName;
    }
}


-(void)setRepertory:(NSNumber *)Repertory{
    if ([Repertory isKindOfClass:[NSNull class]]) {
        _Repertory = [NSNumber numberWithInt:0];
    }else{
        _Repertory = Repertory;
    }
}


-(void)setSubscribe:(NSNumber *)Subscribe{
    if ([Subscribe isKindOfClass:[NSNull class]]) {
        _Subscribe = [NSNumber numberWithInt:0];
    }else{
        _Subscribe = Subscribe;
    }
}

-(void)setInventory:(NSNumber *)Inventory{
    if ([Inventory isKindOfClass:[NSNull class]]) {
        _Inventory = [NSNumber numberWithInt:0];
    }else{
        _Inventory = Inventory;
    }
}

-(void)setPurpose:(NSString *)Purpose{
    
    if ([Purpose isKindOfClass:[NSNull class]]) {
        _Purpose = @"暂无";
    }else if([Purpose isEqualToString:@"for_teach"]){
        _Purpose=@"示教"; 
    }else  if([Purpose isEqualToString:@"for_student"]){
        _Purpose=@"学生练习";
    }else if([Purpose isEqualToString:@"for_all"]){
        _Purpose=@"示教&学生练习";
    }else{
        _Purpose=Purpose;
    }
    
}

-(void)setModelId:(NSNumber *)ModelId{
    
    if ([ModelId isKindOfClass:[NSNull class]]) {
        _ModelId = [NSNumber numberWithInt:0];
    }else{
        _ModelId = ModelId;
    }

}

-(void)setModelName:(NSString *)ModelName{
    if ([ModelName isKindOfClass:[NSNull class]]) {
        _ModelName = @"暂无";
    }else{
        _ModelName = ModelName;
    }
}

-(void)setModelCategoryName:(NSString *)ModelCategoryName{
    if ([ModelCategoryName isKindOfClass:[NSNull class]]) {
        _ModelCategoryName = @"暂无";
    }else{
        _ModelCategoryName = ModelCategoryName;
    }
}

-(void)setModelNo:(NSString *)ModelNo{
    if ([ModelNo isKindOfClass:[NSNull class]]) {
        _ModelNo = @"暂无";
    }else{
        _ModelNo = ModelNo;
    }
}

-(void)setModelItemsNum:(NSNumber *)ModelItemsNum{
    if ([ModelItemsNum isKindOfClass:[NSNull class]]) {
        _ModelItemsNum = [NSNumber numberWithInt:0];
    }else{
        _ModelItemsNum = ModelItemsNum;
    }
}

-(void)setModelAmount:(NSNumber *)ModelAmount{
    if ([ModelAmount isKindOfClass:[NSNull class]]) {
        _ModelAmount = [NSNumber numberWithInt:0];
    }else{
        _ModelAmount = ModelAmount;
    }
}

-(void)setModelComments:(NSString *)ModelComments{
    if ([ModelComments isKindOfClass:[NSNull class]]) {
        _ModelComments = @"暂无";
    }else{
        _ModelComments = ModelComments;
    }
}

-(void)setModeluseFor:(NSString *)ModeluseFor{
    if ([ModeluseFor isKindOfClass:[NSNull class]]) {
        _ModeluseFor = @"暂无";
    }else{
        _ModeluseFor = ModeluseFor;
    }
}

@end
