//
//  CourseViewController.m
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import "TCourseController.h"
#import "PopViewController.h"
#import "MyCell.h"
#import "TCourseAddViewController.h"
#import "PagingViewController.h"
#import <CoreImage/CoreImage.h>
#import "CIImage+Extension.h"


#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
@interface TCourseController ()
{
    //
    UITableView *courseTable;
    //
    NSMutableArray *dataArray;
    NSMutableArray *resultArray;
    NSMutableArray *NewArray;
    //
    NSMutableArray *historyArray;
    //
    int currentPage;
    PopViewController *_popVC;
    
    NSString *StatusString;
    
    
    UIView *footer;
    UILabel *footerLabel;
    
    UIButton *searchBtn;
    
    NSString *SturtBtnString;
    NSInteger line;
    
    NSString *currentTimeString;

    UIView * bgView;
    
    double *CellHight;
    
    CIFilter *filter;
    NSTimer *timer;
    
    
    NSMutableArray  *searchArray;
    UISearchBar *searchBar;
    NSMutableArray *tempArray;
    NSMutableArray *selectArray;
    
    BOOL isSearch;
    
    NSInteger IndexNum;
    UIImageView *backNavigation;
    
    UIView *SearchView;
    UITextField *SearchField;
    UIButton *SearchBtn;
    
    UILabel *lable;
    UIImageView *imageView;
    
     UIButton *rightBtn;
     UIButton *SaveBtn;
    
    NSString *NewDayTime;
    
    NSMutableArray *newArray;
}
@property(nonatomic,assign,getter=isFirstResponder)BOOL footerfrefrshing;

@property(nonatomic,strong)UISearchController *searchController;

@property(nonatomic,strong)NSMutableArray *listFilterList;

@end

@implementation TCourseController




- (void)viewWillAppear:(BOOL)animated
{
    [self setUpRefresh];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self test];
    [self setsearchBar];
    [self setNav];
    [self setObj];
    
    [self SetNewTime];
    
    //添加上拉加载刷新
    [self setUpRefresh];
    
   
    
    
    
}
- (void)setObj{
    currentPage = 1;
    //
    dataArray = [NSMutableArray new];
    resultArray = [NSMutableArray new];
    NewArray=[NSMutableArray new];
    //
    CGFloat courseTable_Y=backNavigation.wwy_height+SearchView.wwy_height;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, courseTable_Y,Swidth, Sheight-courseTable_Y) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    [courseTable registerNib:[UINib nibWithNibName:@"MyCell" bundle:nil] forCellReuseIdentifier:@"tcourseCell"];
    [self.view addSubview:courseTable];
    
    _editing=0;
    IndexNum = 0;
    
}

-(void)setsearchBar{
    
    tempArray=[NSMutableArray array];
    selectArray=[NSMutableArray array];
    isSearch=NO;
    
    SearchView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 44)];
    SearchView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    UIImageView *SearchImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sousuo"]];
    SearchImg.backgroundColor=[UIColor clearColor];
    SearchImg.frame=CGRectMake(10, 6, 32, 32);
    
    
    SearchField=[[UITextField alloc]initWithFrame:CGRectMake(44, 6, Swidth-88, 32)];
    SearchField.backgroundColor=[UIColor clearColor];
    SearchField.placeholder=@"请输入搜索的课程名称";
    SearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    SearchField.clearsOnBeginEditing = YES;
    SearchField.keyboardType=UIKeyboardTypeDefault;
    SearchField.returnKeyType=UIReturnKeySearch;
    SearchField.delegate = self;
    [SearchField addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    SearchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SearchBtn.frame=CGRectMake((SearchField.wwy_x+SearchField.wwy_width)+2, 6, 40, 32);
    [SearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [SearchBtn addTarget:self action:@selector(setUpRefresh) forControlEvents:UIControlEventTouchUpInside];
    SearchBtn.backgroundColor=UIColorFromHex(0x20B2AA);
    SearchBtn.layer.cornerRadius=5;
    SearchBtn.hidden=YES;
    [SearchView addSubview:SearchBtn];
    [SearchView addSubview:SearchField];
    [SearchView addSubview:SearchImg];
    [self.view addSubview:SearchView];
  
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField

{
     [SearchField  resignFirstResponder];
    [self setUpRefresh ];
    NSLog(@"点击了搜索");
    
    return YES;
    
}


-(BOOL)ApplyTextDidChange:(UITextField *)theTextField{
    
    NSLog( @"text changed: %@", theTextField.text);

    
    if (![theTextField.text isEqualToString:@""]) {
        SearchBtn.hidden=NO;
                isSearch=YES;
            }else{
                isSearch=NO;
                [self setUpRefresh];
                SearchBtn.hidden=YES;
                [SearchField  resignFirstResponder];
            }
    
    return YES;

}

- (void)setUpRefresh{
    
    
    [courseTable addHeaderWithTarget:self action:@selector(downRefresh)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中。。。";

}




- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-100, 29.5, 100, 25);
    [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(AddBtn) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;

    
    
    searchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame=CGRectMake(0, 25, 220, 25);
    searchBtn.center=CGPointMake(backNavigation.center.x, leftBtn.center.y);
    
    [searchBtn setTitle:@"课程类型▼" forState:UIControlStateNormal];
    searchBtn.titleLabel.textAlignment=1;
    searchBtn.titleLabel.font= [UIFont systemFontOfSize:20];
    searchBtn.titleLabel.textColor=[UIColor whiteColor];
    [searchBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:searchBtn];
    
    
    
}

-(void)SetNewTime{
    //获取当前时间
    NSDate *datenow = [NSDate date];
    NSLog(@"---%@",datenow);
    double abs=fabs((double)[datenow timeIntervalSince1970]*1000);
    
    currentTimeString = [NSString stringWithFormat:@"%.0f",abs];
    NSLog(@"%@--%lf",currentTimeString,abs);
}

-(void)test{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    NSDate *startDate = [calendar dateFromComponents:components];
    NSDate *endDate = [calendar dateByAddingUnit:NSCalendarUnitDay value:1 toDate:startDate options:0];
    
    NSLog(@"startDate==%@",startDate);
    
    NewDayTime=[NSString stringWithFormat:@"%@",startDate];
    
}




#pragma mark 加载数据：
- (void)downRefresh{
      currentPage=1;
    NSString *URL;
    if(isSearch == NO){
    switch (IndexNum) {
        case 0:
        URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&startTimeFrom=%@",LocalIP,LocalUserId,currentPage,[[Maneger shareObject] zeroTime]];
     //   URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=",LocalIP,LocalUserId,currentPage];
            break;
         case 1:
            URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=planning",LocalIP,LocalUserId,currentPage];
            break;
        case 2:
            URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=released",LocalIP,LocalUserId,currentPage];
            break;
        case 3:
            URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=started",LocalIP,LocalUserId,currentPage];
            break;
        case 4:
            URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=completed",LocalIP,LocalUserId,currentPage];
            break;
        default:
            break;
    }
    }else if(isSearch == YES){
        
        NSString *str=SearchField.text;
        
        switch (IndexNum) {
            case 0:
               URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&startTimeFrom=%@&courseName=%@",LocalIP,LocalUserId,currentPage,[[Maneger shareObject] zeroTime],str];
                URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            case 1:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=planning&courseName=%@",LocalIP,LocalUserId,currentPage,str];
                URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            case 2:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=released&courseName=%@",LocalIP,LocalUserId,currentPage,str];
               URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            case 3:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=started&courseName=%@",LocalIP,LocalUserId,currentPage,str];
               URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            case 4:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=completed&courseName=%@",LocalIP,LocalUserId,currentPage,str];
                URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            default:
                break;
        }
        
    }

    NSLog(@"URL:%@",URL);
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        [resultArray removeAllObjects];
        
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无课程!" andCurentVC:self];
                if (isSearch) {
                    isSearch=NO;
                    [courseTable reloadData];
                }
                 [courseTable reloadData];
                
                switch (IndexNum) {
                    case 0:
                        [searchBtn setTitle:@"今后的课程▼" forState:UIControlStateNormal];
                        break;
                    case 1:
                        [searchBtn setTitle:@"计划中的课程▼" forState:UIControlStateNormal];
                        break;
                    case 2:
                        [searchBtn setTitle:@"已发布的课程▼" forState:UIControlStateNormal];
                        break;
                    case 3:
                        [searchBtn setTitle:@"正在上课的课程▼" forState:UIControlStateNormal];
                        break;
                    case 4:
                        [searchBtn setTitle:@"已完成的课程▼" forState:UIControlStateNormal];
                        break;
                    default:
                        break;
                }
                
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    TCourseModel *model = [TCourseModel new];
                    model.scheduleID = [dictionary objectForKey:@"scheduleId"];
                    model.courseDes = [dictionary objectForKey:@"courseDescription"];
                    model.courseSubjec = [dictionary objectForKey:@"subjectName"];
                    model.startTime = [dictionary objectForKey:@"startTime"];
                    model.endTime = [dictionary objectForKey:@"endTime"];
                    model.scheduleTyoe = [dictionary objectForKey:@"scheduleType"];
                    model.scheduleStatus = [dictionary objectForKey:@"scheduleStatus"];
                    model.registNum = [dictionary objectForKey:@"registeredStudentsNum"];
                    model.courseName = [dictionary objectForKey:@"courseName"];
                    model.coursePlace = [dictionary objectForKey:@"classroomName"];
                    model.teacherName = [dictionary objectForKey:@"teacherName"];
                    model.teacherId=[dictionary objectForKey:@"teacherId"];
                    model.roomID=[dictionary objectForKey:@"classroomId"];
                    model.roomName=[dictionary objectForKey:@"classroomName"];
                    model.courseCategoryName=[dictionary objectForKey:@"courseCategoryName"];
                    model.courseId=[dictionary objectForKey:@"courseId"];
                    model.signstartTime=[dictionary objectForKey:@"registerStartTime"];
                    model.signendTime=[dictionary objectForKey:@"registerEndTime"];
                    model.capacity=[dictionary objectForKey:@"capacity"];
               
                    model.live=[dictionary objectForKey:@"live"];
                    model.credit=[dictionary objectForKey:@"credit"];
                    model.required=[dictionary objectForKey:@"required"];
                    
                    
                        [dataArray addObject:model];
                        [resultArray addObject:model];
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
           
                switch (IndexNum) {
                    case 0:
                        [searchBtn setTitle:@"今后的课程▼" forState:UIControlStateNormal];
                        break;
                    case 1:
                         [searchBtn setTitle:@"计划中的课程▼" forState:UIControlStateNormal];
                        break;
                    case 2:
                        [searchBtn setTitle:@"已发布的课程▼" forState:UIControlStateNormal];
                        break;
                    case 3:
                        [searchBtn setTitle:@"正在上课的课程▼" forState:UIControlStateNormal];
                        break;
                    case 4:
                         [searchBtn setTitle:@"已完成的课程▼" forState:UIControlStateNormal];
                        break;
                    default:
                        break;
                }
                
                
                
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
        
    } failed:^(NSString *result) {
        
        if (![result isEqualToString:@"200"]) {
            [courseTable headerEndRefreshing];
        }
        return;
    }];
    
}

#pragma mark 刷新数据
- (void)loadRefresh{
    
    currentPage++;
    NSString *URL;
    if(isSearch == NO){
        switch (IndexNum) {
            case 0:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&startTimeFrom=%@",LocalIP,LocalUserId,currentPage,[[Maneger shareObject] zeroTime]];
                break;
            case 1:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=planning",LocalIP,LocalUserId,currentPage];
                break;
            case 2:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=released",LocalIP,LocalUserId,currentPage];
                break;
            case 3:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=started",LocalIP,LocalUserId,currentPage];
                break;
            case 4:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=completed",LocalIP,LocalUserId,currentPage];
                break;
            default:
                break;
        }

        
    }else if(isSearch == YES){
        
        NSString *str=SearchField.text;
        
        switch (IndexNum) {
            case 0:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&startTimeFrom=%@&courseName=%@",LocalIP,LocalUserId,currentPage,[[Maneger shareObject] zeroTime],str];
               URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            case 1:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=planning&courseName=%@",LocalIP,LocalUserId,currentPage,str];
                URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            case 2:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=released&courseName=%@",LocalIP,LocalUserId,currentPage,str];
                URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            case 3:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=started&courseName=%@",LocalIP,LocalUserId,currentPage,str];
               URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            case 4:
                URL=[NSString stringWithFormat:@"%@/courseSchedules?teacher=%@&pageSize=50&pageStart=%d&scheduleStatus=completed&courseName=%@",LocalIP,LocalUserId,currentPage,str];
             URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                break;
            default:
                break;
        }
        
    }
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无课程!" andCurentVC:self];
                
                if (isSearch) {
                    isSearch=NO;
                }
                 [courseTable reloadData];
                
                switch (IndexNum) {
                    case 0:
                        [searchBtn setTitle:@"今后的课程▼" forState:UIControlStateNormal];
                        break;
                    case 1:
                        [searchBtn setTitle:@"计划中的课程▼" forState:UIControlStateNormal];
                        break;
                    case 2:
                        [searchBtn setTitle:@"已发布的课程▼" forState:UIControlStateNormal];
                        break;
                    case 3:
                        [searchBtn setTitle:@"正在上课的课程▼" forState:UIControlStateNormal];
                        break;
                    case 4:
                        [searchBtn setTitle:@"已完成的课程▼" forState:UIControlStateNormal];
                        break;
                    default:
                        break;
                }
                
                
                [courseTable footerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    TCourseModel *model = [TCourseModel new];
                    model.scheduleID = [dictionary objectForKey:@"scheduleId"];
                    model.courseDes = [dictionary objectForKey:@"courseDescription"];
                    model.courseSubjec = [dictionary objectForKey:@"subjectName"];
                    model.startTime = [dictionary objectForKey:@"startTime"];
                    model.endTime = [dictionary objectForKey:@"endTime"];
                    model.scheduleTyoe = [dictionary objectForKey:@"scheduleType"];
                    model.scheduleStatus = [dictionary objectForKey:@"scheduleStatus"];
                    model.registNum = [dictionary objectForKey:@"registeredStudentsNum"];
                    model.courseName = [dictionary objectForKey:@"courseName"];
                    model.coursePlace = [dictionary objectForKey:@"classroomName"];
                    model.teacherName = [dictionary objectForKey:@"teacherName"];
                    model.teacherId=[dictionary objectForKey:@"teacherId"];
                    model.roomID=[dictionary objectForKey:@"classroomId"];
                    model.roomName=[dictionary objectForKey:@"classroomName"];
                    model.courseCategoryName=[dictionary objectForKey:@"courseCategoryName"];
                    model.courseId=[dictionary objectForKey:@"courseId"];
                    model.signstartTime=[dictionary objectForKey:@"registerStartTime"];
                    model.signendTime=[dictionary objectForKey:@"registerEndTime"];
                    model.capacity=[dictionary objectForKey:@"capacity"];
                    
                    model.live=[dictionary objectForKey:@"live"];
                    model.credit=[dictionary objectForKey:@"credit"];
                    model.required=[dictionary objectForKey:@"required"];
                    [newData addObject:model];
                }
             
                    NSRange range = NSMakeRange(dataArray.count,newData.count );
                    NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                    [dataArray insertObjects:newData atIndexes:set];
                
                
                [courseTable reloadData];
                
                
            }
        }
        else{
            currentPage--;
            [MBProgressHUD showToastAndMessage:@"请求数据失败!" places:0 toView:nil];
        }
        
        
    } failed:^(NSString *result) {
        
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}

//筛选：

- (void)selectAction:(UIButton *)button{
    
    CGPoint point = CGPointMake(button.center.x, button.center.y+16);
    CustomPopview *popView = [[CustomPopview alloc]initWithOrigin:point Width:130 Height:200 Type:XTTypeOfUpCenter Color:[UIColor whiteColor]];
    popView.dataArray = @[@"今后",@"计划中",@"已发布",@"正在上课",@"已完成"];
    popView.fontSize = 15;
    popView.row_height = 40;
    popView.delegate = self;
    [popView popView];
  
}
-(void)selectIndexPathRow:(NSInteger)index{
    NSArray *indexArray = @[@"all",@"计划中",@"已发布",@"正在上课",@"已完成"];
    [self searchAction:indexArray[index]];
}

- (void)searchAction:(NSString *)status{
    
    
    if ([status isEqualToString:@"计划中"]) {
        IndexNum = 1;
        [self setUpRefresh];
    }else if ([status isEqualToString:@"已发布"]) {
        IndexNum = 2;
        [self setUpRefresh];
    }else if ([status isEqualToString:@"正在上课"]) {
        IndexNum = 3;
       [self setUpRefresh];
    }else if ([status isEqualToString:@"已完成"]) {
        IndexNum = 4;
      [self setUpRefresh];
    }else if ([status isEqualToString:@"all"]) {
        IndexNum = 0;
       [self setUpRefresh];
    }
 
}






-(void)AddBtn{
    
    TCourseModel *model = [TCourseModel new];
    
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"课程管理" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *AddAction = [UIAlertAction actionWithTitle:@"新增" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
          [self.navigationController pushViewController:[PagingViewController new] animated:YES];
       
        
    }];
    UIAlertAction *EditAction = [UIAlertAction actionWithTitle:@"修改" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [courseTable setEditing:YES animated:YES];
        rightBtn.hidden=YES;
        SaveBtn.hidden=NO;
        _editing=1;
        
    }];
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
    [AlertView addAction:BackAction];
    [AlertView addAction:AddAction];
     [AlertView addAction:EditAction];

    [self presentViewController:AlertView animated:YES completion:nil];
    

}

-(void)save{
    [courseTable setEditing:NO animated:YES];
    rightBtn.hidden=NO;
    SaveBtn.hidden=YES;
    isSearch=NO;
    _editing=0;
}


#pragma -action

- (void)back :(UIButton *)button
{
    
    [self.navigationController popViewControllerAnimated:YES];

    
}
#pragma -tableView协议

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
        return dataArray.count;
   
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
  //  if (!_editing) {
        TCourseDetailController *detailVC = [TCourseDetailController new];
        detailVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:detailVC animated:YES];
 //   }
    
        
   
 
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tcourseCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    TCourseModel *model;
   
    

         model = dataArray[indexPath.row];
        [cell setProperty:model];

  

    if([model.scheduleStatus isEqualToString:@"计划中"]){
         [cell.CourseBtn setTitle:@"发布" forState:UIControlStateNormal];
          cell.RQCodebtn.hidden=YES;
    }else if([model.scheduleStatus isEqualToString:@"已发布"]){
        [cell.CourseBtn setTitle:@"开始上课" forState:UIControlStateNormal];
        cell.RQCodebtn.hidden=NO;
    }else if([model.scheduleStatus isEqualToString:@"正在上课"]){
        [cell.CourseBtn setTitle:@"结束课程" forState:UIControlStateNormal];
        cell.RQCodebtn.hidden=NO;
    }else if([model.scheduleStatus isEqualToString:@"已完成"]){
        [cell.CourseBtn setTitle:@"查看" forState:UIControlStateNormal];
        cell.RQCodebtn.hidden=NO;
    }
    
    
    NSString *content=model.courseName;
    CGFloat test_H=[Maneger getPonentH:content andFont:[UIFont systemFontOfSize:17] andWidth:Swidth-220]+65;
    CGRect frame=cell.frame;
    frame.size.height=test_H+73;
    cell.frame=frame;
    
    cell.CourseBtn.tag=100+indexPath.row;
    [cell.CourseBtn addTarget:self action:@selector(aut:) forControlEvents:UIControlEventTouchUpInside];
    [cell.RQCodebtn addTarget:self action:@selector(QRcode:) forControlEvents:UIControlEventTouchUpInside];

    return cell;
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyCell *cell =(MyCell *)[self tableView:tableView  cellForRowAtIndexPath:indexPath];
    
    return cell.frame.size.height;
}


-(void)aut:(UIButton *)sender{
    line = sender.tag-100;
    NSLog(@"line1:%ld",line);
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem: line inSection:0];
    
    MyCell *cell = [courseTable cellForRowAtIndexPath:indexPath];
    TCourseModel *model;

       model = dataArray[line];

    if ([model.scheduleStatus isEqualToString:@"计划中"]) {
        
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"发布确认" message:@"发布后，学员可查看该课程，且课程信息将不能再修改，是否确认发布？" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
              [self SetRelease:sender andModel:model andCell:cell];
        }];
        
        UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alert addAction:noAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
      
    }else if([model.scheduleStatus isEqualToString:@"已发布"]){
 
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"开始上课" message:@"是否确认开始上课" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
             [self SetStarted:sender andModel:model andCell:cell];
        }];
        
        UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alert addAction:noAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
        
    }else if([model.scheduleStatus isEqualToString:@"正在上课"]){
       
        
        
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"结束课程" message:@"是否确认结束课程" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
             [self SetCompleted:sender andModel:model andCell:cell];
        }];
        
        UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alert addAction:noAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
    }if([model.scheduleStatus isEqualToString:@"已完成"]){
        TCourseDetailController *detailVC = [TCourseDetailController new];
       
        
        if (isSearch == NO) {
            detailVC.model = dataArray[indexPath.row];
        }else{
            detailVC.model = tempArray[indexPath.row];
        }
        
        
        [self.navigationController pushViewController:detailVC animated:YES];
    }
    
    SturtBtnString=cell.CourseBtn.titleLabel.text;

}
-(void)SetRelease:(UIButton *)btn andModel:(TCourseModel *)model andCell:(MyCell *)cell{
    NSString *Url=[NSString stringWithFormat:@"%@/courseSchedules/%@/release",LocalIP,model.scheduleID];
    
    [RequestTools RequestWithURL:Url Method:@"post" Params:nil Success:^(NSDictionary *result) {
        
        [cell.CourseBtn setTitle:@"开始上课" forState:UIControlStateNormal];
        cell.CourseState.text = @"已发布";
        
        model.scheduleStatus = @"已发布";
        cell.CourseState.text = @"已发布";
        cell.RQCodebtn.hidden=NO;

        [dataArray replaceObjectAtIndex:btn.tag-100 withObject:model];
    
        
    } failed:^(NSString *result){
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"result:%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
}


-(void)SetStarted:(UIButton *)btn andModel:(TCourseModel *)model andCell:(MyCell *)cell{
    NSString *Url=[NSString stringWithFormat:@"%@/courseSchedules/%@/start",LocalIP,model.scheduleID];
    
    [RequestTools RequestWithURL:Url Method:@"post" Params:nil Success:^(NSDictionary *result) {
        
        [cell.CourseBtn setTitle:@"结束课程" forState:UIControlStateNormal];
        cell.CourseState.text = @"正在上课";
        
        model.scheduleStatus = @"正在上课";
        cell.CourseState.text = @"正在上课";
        cell.RQCodebtn.hidden=NO;
            [dataArray replaceObjectAtIndex:btn.tag-100 withObject:model];

    } failed:^(NSString *result){
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"result:%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}

-(void)SetCompleted:(UIButton *)btn andModel:(TCourseModel *)model andCell:(MyCell *)cell{
    NSString *Url=[NSString stringWithFormat:@"%@/courseSchedules/%@/complete",LocalIP,model.scheduleID];

    [RequestTools RequestWithURL:Url Method:@"post" Params:nil Success:^(NSDictionary *result) {

        [cell.CourseBtn setTitle:@"查看" forState:UIControlStateNormal];
        cell.CourseState.text = @"已完成";

        model.scheduleStatus = @"已完成";
        cell.CourseState.text = @"已完成";
         cell.RQCodebtn.hidden=NO;
            [dataArray replaceObjectAtIndex:btn.tag-100 withObject:model];
       


    } failed:^(NSString *result){
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"result:%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];

}

-(void)QRcode:(id)sender{

    MyCell *cell=(MyCell *)[[sender superview]superview].superview;
    NSInteger line = cell.CourseBtn.tag-100;

    TCourseModel *model = dataArray[line];

 
    
    NSMutableDictionary *QRcode= [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                 @"attendanceItem":@"COURSE",
                                                                                 @"itemId":model.scheduleID,
                                                                                 @"itemName":model.courseName,
                                                                                 @"roomId":model.roomID,
                                                                                 @"roomName":model.roomName,
                                                                                 @"departmentId":@"",
                                                                                 @"startTime":model.startTime,
                                                                                 @"endTime":model.endTime,
                                                                                 }];
    
    [self RQcodeWithInfo:QRcode];

    bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    bgView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];

    UIButton *backViewBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backViewBtn.frame=CGRectMake(0, 64, Swidth, Sheight);
    [backViewBtn addTarget:self action:@selector(HideBackView) forControlEvents:UIControlEventTouchUpInside];
    
    [bgView addSubview:backViewBtn];
 
    [[UIApplication sharedApplication].keyWindow addSubview:bgView];
    
    [timer setFireDate:[NSDate distantPast]];

}


-(void)HideBackView{
    bgView.hidden=YES;
    [timer setFireDate:[NSDate distantFuture]];
}


//二维码层：
-(void)RQcodeWithInfo:(NSMutableDictionary *)infoArray{
    
 
    
    timer=[NSTimer scheduledTimerWithTimeInterval:10.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        [self SetNewTime];
        // 1.创建滤镜对象
        filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
        // 2.恢复默认设置
        [filter setDefaults];
        //3.设置数据
        [infoArray setObject:currentTimeString forKey:@"createTime"];
        NSLog(@"currentTimeString=====%@",currentTimeString);
        NSData *infoData = [NSJSONSerialization dataWithJSONObject:infoArray options:NSJSONWritingPrettyPrinted error:nil];
        
        [filter setValue:infoData forKeyPath:@"inputMessage"];
        // 4.生成二维码
        CIImage *outputImage = [filter outputImage];

        imageView=[[UIImageView alloc]init];
        imageView.frame=CGRectMake(Swidth/2-150, Sheight/2-200, 300, 300);
        UIImageView *view=[[UIImageView alloc]initWithFrame:CGRectMake((imageView.wwy_x+imageView.wwy_width)*0.4-20,(imageView.wwy_y+imageView.wwy_height)*0.3-24, 48, 48)];
        view.image=[UIImage imageNamed:@"logo"];
        [imageView addSubview:view];
        
        imageView.image = [outputImage createNonInterpolatedWithSize:200];
        [bgView addSubview:imageView];
        
    }];
    
   


}


#pragma -UIalertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    return UIModalPresentationNone;
}


#pragma mark  搜索栏：
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [searchBar resignFirstResponder];
}

#pragma mark 编辑：
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return   UITableViewCellEditingStyleDelete;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"你确定删除该课程？" preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
             TCourseModel *model=dataArray[indexPath.row];
            
            
            
            if([model.scheduleStatus isEqualToString:@"计划中"]){
               
               
                
                [dataArray removeObjectAtIndex:indexPath.row];
                [courseTable beginUpdates];
                [courseTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                
                [self DeleteDate:model CourseWareId:[NSString stringWithFormat:@"%@",model.scheduleID]];
                
                [courseTable endUpdates];
            }else{
                
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"只有计划中的课程才能被删除！！！" preferredStyle:UIAlertControllerStyleAlert];
                
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
      
                }]];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    
                }]];
                
                
                [self presentViewController:alertController animated:YES completion:nil];
                
                
                
                
            }
            
            
            
            
           
            
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
        }]];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}
//设置进入编辑状态时，Cell不会缩进

- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return NO;
    
}

// %@/courses/%@
-(void)DeleteDate:(TCourseModel *)model CourseWareId:(NSString *)CourseWareId{
    
    
    NSLog(@"CourseWareId:%@",CourseWareId);
    
    NSString *Url=[NSString stringWithFormat:@"%@/courseSchedules/%@",LocalIP,CourseWareId];
    NSLog(@"%@",Url);
    
    
    [RequestTools RequestWithURL:Url Method:@"DELETE" Params:nil Success:^(NSDictionary *result) {
        
        
        
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            
            [MBProgressHUD showToastAndMessage:@"教案成功删除!" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}

@end

