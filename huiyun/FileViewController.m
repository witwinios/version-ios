//
//  FileViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/25.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "FileViewController.h"
#import "ApplyStudentCell.h"
#import "AddFileViewController.h"
#import "ZCAssetsPickerViewController.h"
#import "CourseQuestionViewController.h"
@interface FileViewController ()<ZCAssetsPickerViewControllerDelegate>
{
    UITableView *courseTable;
    
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *EditBtn;
    UIButton *SaveBtn;
    NSString *courseId;
    int currentPage;
    NSMutableArray *dataArray;
    NSMutableArray *resultArray;
    NSMutableArray *NewArray;
    UILabel *titleLab;
    
    NSMutableArray *FileArray;
    UIAlertController *alert;
}

@property(nonatomic,strong)PHCachingImageManager *imageManager;


@end

@implementation FileViewController

-(void)viewWillAppear:(BOOL)animated{
    [self setUpRefresh];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    
    
    if (![_Str isEqualToString:@"教具"]) {
        [self setUpRefresh];
    }else{
        
    }
    
    
    [self setNav];
    
    self.imageManager = [[PHCachingImageManager alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
 
        titleLab.text = @"附件列表";
  
    
   
    [backNavigation addSubview:titleLab];
    
    
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (_PlanModel.PlanOwnerId.intValue == persion.userID.intValue ) {
    if([_PlanModel.PlanCourseStatus isEqualToString:@"in_design"]){
        EditBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        EditBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
        [EditBtn setTitle:@"编辑" forState:UIControlStateNormal];
        [EditBtn addTarget:self action:@selector(EditPlan:) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:EditBtn];
        
        SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
        [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
        [SaveBtn addTarget:self action:@selector(EditSavePlan:) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:SaveBtn];
        SaveBtn.hidden=YES;
    }
    }
    //教具页面隐藏修改功能
    if ([_Str isEqualToString:@"教具"]) {
        EditBtn.hidden=YES;
    }
    
    
}

-(void)EditPlan:(UIButton *)sender{
    
    
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"附件管理" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *AddAction = [UIAlertAction actionWithTitle:@"添加图片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        ZCAssetsPickerViewController *picker = [[ZCAssetsPickerViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:picker];
        picker.type = ChooseTypeMedia;
        picker.maximumNumbernMedia = 9;
        picker.delegate = self;
    
        
        if ([_Str isEqualToString:@"教案"]) {
            picker.CourseId=[_PlanModel.PlanNameId stringValue];
        }else if([_Str isEqualToString:@"课程"]){
             picker.CourseId=[_CourseModel.courseId stringValue];
        }
        
        picker.TempArray=dataArray;
        [self presentViewController:nav animated:YES completion:nil];
   
    }];
    UIAlertAction *VideoAction = [UIAlertAction actionWithTitle:@"添加视频" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
        ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//sourcetype有三种分别是camera，photoLibrary和photoAlbum
        NSArray *availableMedia = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];//Camera所支持的Media格式都有哪些,共有两个分别是@"public.image",@"public.movie"
        ipc.mediaTypes = [NSArray arrayWithObject:availableMedia[1]];//设置媒体类型为public.movie
        [self presentViewController:ipc animated:YES completion:nil];
        ipc.delegate = self;//设置委托  
        
        
    }];
    
    
    UIAlertAction *EditAction = [UIAlertAction actionWithTitle:@"删除附件" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            [courseTable setEditing:YES animated:YES];
            EditBtn.hidden=YES;
            SaveBtn.hidden=NO;

        
    }];
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
    [AlertView addAction:BackAction];
    [AlertView addAction:AddAction];
    [AlertView addAction:VideoAction];
    [AlertView addAction:EditAction];
    
    [self presentViewController:AlertView animated:YES completion:nil];
    
}

-(void)EditSavePlan:(UIButton *)sender{
    [courseTable setEditing:NO animated:YES];
    EditBtn.hidden=NO;
    SaveBtn.hidden=YES;
}

- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUI{
    currentPage = 1;
    //
    dataArray = [NSMutableArray new];
    resultArray = [NSMutableArray new];
    FileArray=[NSMutableArray new];
    //
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    [courseTable registerNib:[UINib nibWithNibName:@"ApplyStudentCell" bundle:nil] forCellReuseIdentifier:@"fileCell"];
    [self.view addSubview:courseTable];
    
    if ([_Str isEqualToString:@"教案"]) {
          courseId=[_PlanModel.PlanNameId stringValue];
    }else if([_Str isEqualToString:@"课程"]){
         courseId=[_CourseModel.courseId stringValue];
    }
  
}

#pragma  mark 从后台加载数据:

- (void)setUpRefresh{
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中。。。";
    
}

-(void)loadData{
    currentPage=1;
    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/files?pageStart=%d&pageSize=15",LocalIP,courseId,currentPage];
  
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        
        if([result[@"responseStatus"] isEqualToString:@"succeed"]){
            
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无附件!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
         
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    CourseAffixModel *model=[CourseAffixModel new];
                    
                    model.FileUrl=[dictionary objectForKey:@"fileUrl"];
                    model.FileName=[dictionary objectForKey:@"fileName"];
                    model.FileId=[dictionary objectForKey:@"fileId"];
                    model.FileFormat=[dictionary objectForKey:@"fileFormat"];
                    NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.FileUrl];
                    model.FileUrl=url;
                    [dataArray addObject:model];
                      NSLog(@"%@",model.FileUrl);
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
            }
        }else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            NSLog(@"%@",result);
            [courseTable headerEndRefreshing];
        }
        return;
        
    }];

}

-(void)loadRefresh{
    currentPage++;
    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/files?pageStart=%d&pageSize=15",LocalIP,courseId,currentPage];
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {

        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [courseTable footerEndRefreshing];
        if([result[@"responseStatus"] isEqualToString:@"succeed"]){
            
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无更多附件!" andCurentVC:self];
                [courseTable footerEndRefreshing];
            }else{
                
                 NewArray=[NSMutableArray new];
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    CourseAffixModel *model=[CourseAffixModel new];
                    
                    
                    model.FileId=[dictionary objectForKey:@"fileId"];
                    model.FileName=[dictionary objectForKey:@"fileName"];
                    model.Description=[dictionary objectForKey:@"description"];
                    model.FileType=[dictionary objectForKey:@"fileType"];
                    model.FileUrl=[dictionary objectForKey:@"fileUrl"];
                    model.FileFormat=[dictionary objectForKey:@"fileFormat"];
                    model.FileSize=[dictionary objectForKey:@"fileSize"];
                    model.IsPublic=[dictionary objectForKey:@"isPublic"];
                    model.CreatedTime=[dictionary objectForKey:@"createdTime"];
                    model.CreatedBy=[dictionary objectForKey:@"createdBy"];
                    
                    
                    
                    if ([model.FileFormat isEqualToString:@"png"] || [model.FileFormat isEqualToString:@"jpg"]) {
                        NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.FileUrl];
                        
                        model.FileUrl=url;
                    }
                  
                    [NewArray addObject:model];
                    NSLog(@"%@",model.FileUrl);
                }
                NSLog(@"NewArrayCount=%lu",(unsigned long)NewArray.count);
                
                NSRange range = NSMakeRange(dataArray.count,NewArray.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:NewArray atIndexes:set];
                [courseTable reloadData];
            }
        }else{
            currentPage--;
            [MBProgressHUD showToastAndMessage:@"请求数据失败!" places:0 toView:nil];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            NSLog(@"%@",result);
            [courseTable headerEndRefreshing];
        }
        return;
        
    }];
}

#pragma mark 从之前页面获取数据：


//返回行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
//返回行高：
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 95;
}

//点击行事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"该功能正在开发" message:@"请持续关注！" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
    [AlertView addAction:BackAction];
    [self presentViewController:AlertView animated:YES completion:nil];
}

//加载cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ApplyStudentCell *cell=[tableView dequeueReusableCellWithIdentifier:@"fileCell"];

    CourseAffixModel *model=dataArray[indexPath.row];
    [cell setCourseAffix:model];
    return cell;
}


#pragma 编辑：
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return   UITableViewCellEditingStyleDelete;
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"你确定删除该附件？" preferredStyle:UIAlertControllerStyleAlert];
        

        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            CourseAffixModel *model=dataArray[indexPath.row];
         
            [dataArray removeObjectAtIndex:indexPath.row];
            [courseTable beginUpdates];
            [courseTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            
            [self DeleteDate:model NString:[NSString stringWithFormat:@"%@",model.FileId]];
            [courseTable endUpdates];
   
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
           
        }]];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}
//设置进入编辑状态时，Cell不会缩进

- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return NO;
    
}

-(void)DeleteDate:(CourseAffixModel *)model NString:(NSString *)fileId{
   
    NSString *url=[NSString stringWithFormat:@"%@/courses/%@/files/%@",LocalIP,courseId,model.FileId];
    
    NSLog(@"%@",url);
    
    [RequestTools RequestWithURL:url Method:@"DELETE" Params:nil Success:^(NSDictionary *result) {
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            
            [MBProgressHUD showToastAndMessage:@"附件成功删除!" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}

#pragma mark 视频：

- (NSString *)getCurrentTime{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    //    NSString *str = [NSString stringWithFormat:@"%@mdxx",dateTime];
    //    NSString *tokenStr = [str stringToMD5:str];
    return dateTime;
    
}
- (NSURL *)condenseVideoNewUrl: (NSURL *)url{
    // 沙盒目录
    NSString *docuPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *destFilePath = [docuPath stringByAppendingPathComponent:[NSString stringWithFormat:@"lyh%@.MOV",[self getCurrentTime]]];
    NSURL *destUrl = [NSURL fileURLWithPath:destFilePath];
    //将视频文件copy到沙盒目录中
    NSFileManager *manager = [NSFileManager defaultManager];
    NSError *error = nil;
    [manager copyItemAtURL:url toURL:destUrl error:&error];
    NSLog(@"压缩前--%.2fk",[self getFileSize:destFilePath]);
    // 播放视频
    /*
     NSURL *videoURL = [NSURL fileURLWithPath:destFilePath];
     AVPlayer *player = [AVPlayer playerWithURL:videoURL];
     AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
     playerLayer.frame = self.view.bounds;
     [self.view.layer addSublayer:playerLayer];
     [player play];
     */
    // 进行压缩
    AVAsset *asset = [AVAsset assetWithURL:destUrl];
    //创建视频资源导出会话
    /**
     NSString *const AVAssetExportPresetLowQuality; // 低质量
     NSString *const AVAssetExportPresetMediumQuality;
     NSString *const AVAssetExportPresetHighestQuality; //高质量
     */
    
    AVAssetExportSession *session = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
    // 创建导出的url
    NSString *resultPath = [docuPath stringByAppendingPathComponent:[NSString stringWithFormat:@"lyhg%@.MOV",[self getCurrentTime]]];
    session.outputURL = [NSURL fileURLWithPath:resultPath];
    // 必须配置输出属性
    session.outputFileType = @"com.apple.quicktime-movie";
    // 导出视频
    [session exportAsynchronouslyWithCompletionHandler:^{
        NSLog(@"压缩后---%.2fk",[self getFileSize:resultPath]);
        NSLog(@"视频导出完成");
        
    }];
    
    return session.outputURL;
}
// 获取视频的大小
- (CGFloat) getFileSize:(NSString *)path
{
    NSFileManager *fileManager = [[NSFileManager alloc] init] ;
    float filesize = -1.0;
    if ([fileManager fileExistsAtPath:path]) {
        NSDictionary *fileDic = [fileManager attributesOfItemAtPath:path error:nil];//获取文件的属性
        unsigned long long size = [[fileDic objectForKey:NSFileSize] longLongValue];
        filesize = 1.0*size/1024;
    }
    return filesize;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    
    alert=[UIAlertController alertControllerWithTitle:@"文件上传" message:@"您选择的文件正在上传，请等待...." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    // 如果是视频
    NSURL *url = info[UIImagePickerControllerMediaURL];
    // 获取视频总时长
    CGFloat lengthTime = [self getVideoLength:url];
    NSLog(@"%f",lengthTime);
    // 保存视频至相册 (异步线程)
    //        NSString *urlStr = [url path];
    //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //
    //            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(urlStr)) {
    //
    //                UISaveVideoAtPathToSavedPhotosAlbum(urlStr, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
    //            }
    //
    //        });
    
    //压缩视频
    //  NSData *videoData = [NSData dataWithContentsOfURL:[self condenseVideoNewUrl:url]];
    NSData *videoData = [NSData dataWithContentsOfURL:url];
    
    //视频上传
    
    
    NSDictionary *dict = @{@"username":@"syl"};
    
    
    
    NSString *FileUrl=[NSString stringWithFormat:@"%@/files",LocalIP];
    
    [RequestTools RequestUpLoadVideoWithParams:dict URL:FileUrl data:videoData Success:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            CourseAffixModel *model=[CourseAffixModel new];
            model.CreatedBy=[[result objectForKey:@"responseBody"]objectForKey:@"createdBy"];
            model.FileFormat=[[result objectForKey:@"responseBody"]objectForKey:@"fileFormat"];
            model.FileId=[[result objectForKey:@"responseBody"]objectForKey:@"fileId"];
            model.FileName=[[result objectForKey:@"responseBody"]objectForKey:@"fileName"];
            model.FileSize=[[result objectForKey:@"responseBody"]objectForKey:@"fileSize"];
            model.FileType=[[result objectForKey:@"responseBody"]objectForKey:@"fileType"];
            model.FileUrl=[[result objectForKey:@"responseBody"]objectForKey:@"fileUrl"];
            model.CreatedTime=[[result objectForKey:@"responseBody"]objectForKey:@"createdTime"];
            model.Description=[[result objectForKey:@"responseBody"]objectForKey:@"description"];
            model.IsPublic=[[result objectForKey:@"responseBody"]objectForKey:@"isPublic"];
            
            [FileArray addObject:model.FileId];
            
            [self loadVideo];
            
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
        NSLog(@"dataArrayCount=%lu",(unsigned long)FileArray.count);
        
    } faild:^(NSString *result) {
        NSLog(@"请求错误");
        NSLog(@"%@",result);
    } progress:^(NSProgress *progress) {
        NSLog(@"%f",progress.fractionCompleted);
    }];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
// 获取视频时间
- (CGFloat) getVideoLength:(NSURL *)URL
{
    AVURLAsset *avUrl = [AVURLAsset assetWithURL:URL];
    CMTime time = [avUrl duration];
    int second = ceil(time.value/time.timescale);
    return second;
}
#pragma mark 图片保存完毕的回调
- (void) image: (UIImage *) image didFinishSavingWithError:(NSError *) error contextInfo: (void *)contextIn {
    NSLog(@"照片保存成功");
}

#pragma mark 视频保存完毕的回调
- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextIn {
    if (error) {
        NSLog(@"保存视频过程中发生错误，错误信息:%@",error.localizedDescription);
    }else{
        NSLog(@"视频保存成功.");
    }
}

-(void)loadVideo{
    NSMutableArray *IDArr=[NSMutableArray new];
    for (CourseAffixModel *model in dataArray) {
        [IDArr addObject:model.FileId];
    }
    [FileArray addObjectsFromArray:IDArr];
    NSString *URL=[NSString stringWithFormat:@"%@/courses/%@/files",LocalIP,_PlanModel.PlanNameId];
    [RequestTools RequestWithURL:URL Method:@"post" Params:FileArray Success:^(NSDictionary *result) {
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            
            [MBProgressHUD showToastAndMessage:@"添加视频成功!" places:0 toView:nil];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            alert.view.hidden=YES;
            [FileArray removeAllObjects];
            
        }
    } failed:^(NSString *result) {
        NSLog(@"result:%@",result);
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}


@end
