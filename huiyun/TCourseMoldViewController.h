//
//  TCourseMoldViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MoldCell.h"
#import "CourseMoldModel.h"
#import "NewMoldViewController.h"
@class TCourseMoldViewController;

@protocol PassingValueDelegate<NSObject>

@optional

-(void)text:(NSString *)str dateID:(NSString *)dateID;

@end
@interface TCourseMoldViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,assign) id<PassingValueDelegate>delegate;

@end
