//
//  TSkillTimeCell.m
//  huiyun
//
//  Created by Bad on 2018/3/27.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TSkillTimeCell.h"

@implementation TSkillTimeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setAddModel:(TSkillTestCenterModel *)model{
    
    self.OneLabel.text=@"时间段:";
    self.TwoLabel.text=@"开始时间:";
    self.ThreeLabel.text=@"结束时间:";
    
   
    
    // iOS 生成的时间戳是10位
    NSTimeInterval interval    =[model.TSkillTimeFrom doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString       = [formatter stringFromDate: date];
    
    
    
    self.FourLabel.text=dateString;
    
    NSTimeInterval interval2    =[model.TSKillTimeEnd doubleValue] / 1000.0;
    NSDate *date2               = [NSDate dateWithTimeIntervalSince1970:interval2];
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString2      = [formatter2 stringFromDate: date2];
    
    
    self.FiveLabel.text=dateString2;
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
