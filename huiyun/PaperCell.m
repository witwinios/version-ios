//
//  PaperCell.m
//  xiaoyun
//
//  Created by MacAir on 17/2/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "PaperCell.h"

@implementation PaperCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.paperDes.editable = NO;
    self.paperStatus.layer.cornerRadius = 5;
    self.paperStatus.layer.masksToBounds = YES;
    if ([self.paperStatus.text isEqualToString:@"完成"]) {
        self.getScoreLab.alpha = 1;
    }else{
        self.getScoreLab.alpha = 0;
    }
}
@end
