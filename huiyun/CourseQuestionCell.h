//
//  CourseQuestionCell.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseQuestionModel.h"
@interface CourseQuestionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *CourseQuestionName;
@property (weak, nonatomic) IBOutlet UILabel *CourseQuestionNum;
@property (weak, nonatomic) IBOutlet UILabel *Crosshead;
@property (weak, nonatomic) IBOutlet UITextView *CourseQuestionStem;
@property (weak, nonatomic) IBOutlet UIView *bgVc;


@property (weak, nonatomic) IBOutlet UILabel *TwoLable;
@property (weak, nonatomic) IBOutlet UILabel *ThreeLable;


-(void)setProperty:(CourseQuestionModel *)model;

-(void)AddCourseQuestion:(CourseQuestionModel *)model;

@end
