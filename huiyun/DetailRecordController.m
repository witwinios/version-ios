//
//  DetailRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/15.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "DetailRecordController.h"

@interface DetailRecordController ()
{
    UILabel *statusLab;
    UILabel *patientNameLab;
    UILabel *caseNoLab;
    UILabel *caseDateLab;
    UILabel *caseTeacherLab;
    UITextView *mainText;
    UITextView *secondText;
    UITextView *adviceText;
    
    UIButton *approveBtn;
    UIButton *rejectBtn;
    NSString *reviewStatus;
    
    UITextView *currentTextview;
    CGRect _activedTextFieldRect;
    CGPoint currentPoint;
    UIView *toolbar;
    
    UIButton *rightBtn;
}
@end

@implementation DetailRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    //注册键盘出现的通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    //注册键盘出现的通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
   
        [toolbar addSubview:doneBtn];
   
    
    
    [self setUI];
}
- (void)hideKb:(UIButton *)btn{
    [currentTextview resignFirstResponder];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [currentTextview resignFirstResponder];
}
- (void)keyboardWillHide:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
    }];
    
    UIScrollView *scroll = [self.view viewWithTag:205];
    [UIView animateWithDuration:duration animations:^{
        scroll.contentOffset = currentPoint;
    }];

}
- (void)keyboardWillShow:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];

    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
    //将视图上移计算好的偏移
    if ((_activedTextFieldRect.origin.y + _activedTextFieldRect.size.height) + 40 >  ([UIScreen mainScreen].bounds.size.height - rect.size.height)) {
        
        UIScrollView *scroll = [self.view viewWithTag:205];
        //获得当前的偏移量
        currentPoint = scroll.contentOffset;
        [UIView animateWithDuration:duration animations:^{
            scroll.contentOffset = CGPointMake(0, 40+64 + _activedTextFieldRect.origin.y + _activedTextFieldRect.size.height - ([UIScreen mainScreen].bounds.size.height - rect.size.height));
        }];
    }
}
- (void)setUI{
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    //
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    if (persion.roleId.intValue == 2) {
        //老师
        if ([_model.caseTwo isEqualToString:@"待审核"]) {
            rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            rightBtn.frame = CGRectMake(Swidth - 70, 29.5, 70, 25);
            [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
            [rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
            [rightBtn setTitle:@"保存" forState:0];
            [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
            [backNavigation addSubview:rightBtn];
        }
    }else{
        if ([_model.caseTwo isEqualToString:@"待审核"]) {
            rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            rightBtn.frame = CGRectMake(Swidth - 70, 29.5, 70, 25);
            [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
            [rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
            [rightBtn setTitle:@"修改" forState:0];
            
           
            [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
               [backNavigation addSubview:rightBtn];
            if(![_departModel.SDepartStatus isEqualToString:@"出科"]){
                rightBtn.hidden=YES;
            }
           
        }else{
            if(![_departModel.SDepartStatus isEqualToString:@"出科"]){
                 rightBtn.hidden=YES;
            }
              rightBtn.hidden=YES;
        }
    }
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"病例记录";
    [backNavigation addSubview:titleLab];
    //
    UIScrollView *scrollView =[[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    scrollView.tag = 205;
    scrollView.showsVerticalScrollIndicator = NO;
    //拖动隐藏
//    scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [self.view addSubview:scrollView];
    //
    UILabel *status = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 80, 20)];
    status.text = @"审核状态:";
    status.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:status];
    statusLab = [[UILabel alloc]initWithFrame:CGRectMake(90, 5, Swidth-95, 20)];
    statusLab.font = [UIFont systemFontOfSize:17];
    statusLab.text = _model.caseTwo;
    [scrollView addSubview:statusLab];
    //
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(5, 30, 80, 20)];
    name.font = [UIFont systemFontOfSize:17];
    name.text = @"病人姓名";
    [scrollView addSubview:name];
    patientNameLab = [[UILabel alloc]initWithFrame:CGRectMake(90, 30, Swidth-95, 20)];
    patientNameLab.font = [UIFont systemFontOfSize:17];
    patientNameLab.text = _model.caseOne;
    [scrollView addSubview:patientNameLab];
    //
    UILabel *caseNo = [[UILabel alloc]initWithFrame:CGRectMake(5, 55, 80, 20)];
    caseNo.text = @"病案号:";
    caseNo.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:caseNo];
    caseNoLab = [[UILabel alloc]initWithFrame:CGRectMake(90, 55, Swidth-95, 20)];
    caseNoLab.text = _model.caseThree;
    caseNoLab.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:caseNoLab];
    //
    UILabel *dates = [[UILabel alloc]initWithFrame:CGRectMake(5, 80, 80, 20)];
    dates.font = [UIFont systemFontOfSize:17];
    dates.text = @"病例时间:";
    [scrollView addSubview:dates];
    caseDateLab = [[UILabel alloc]initWithFrame:CGRectMake(90, 80, Swidth-95, 20)];
    caseDateLab.text = [[Maneger shareObject] timeFormatter1:_model.caseFive.stringValue];
    caseDateLab.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:caseDateLab];
    //
    UILabel *teacher = [[UILabel alloc]initWithFrame:CGRectMake(5, 105, 80, 20)];
    teacher.font = [UIFont systemFontOfSize:17];
    teacher.text = @"带教老师:";
    [scrollView addSubview:teacher];
    caseTeacherLab = [[UILabel alloc]initWithFrame:CGRectMake(90, 105, Swidth-95, 20)];
    caseTeacherLab.text = _model.doctor;
    caseTeacherLab.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:caseTeacherLab];
    
    UILabel *docLab = [[UILabel alloc]initWithFrame:CGRectMake(5, 130, 80, 20)];
    docLab.font = [UIFont systemFontOfSize:17];
    docLab.text = @"上级医师:";

    [scrollView addSubview:docLab];
    
    UILabel *docContent = [[UILabel alloc]initWithFrame:CGRectMake(90, 130, Swidth-95, 20)];
    docContent.text = _model.doctor;
    docContent.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:docContent];
    
    //
    UILabel *main = [[UILabel alloc]initWithFrame:CGRectMake(5, 155, 80, 20)];
    main.text = @"主要诊断:";
    main.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:main];
    //
    
    mainText = [[UITextView alloc]initWithFrame:CGRectMake(5, 180, Swidth-10, 60)];
    mainText.layer.borderColor = [UIColor grayColor].CGColor;
    mainText.layer.borderWidth = 1;
    mainText.delegate = self;
    [mainText setEditable:NO];
    mainText.font = [UIFont systemFontOfSize:15];
    mainText.text =[NSString stringWithFormat:@"    %@",_model.mainIos];
    [scrollView addSubview:mainText];
    //
    UILabel *second = [[UILabel alloc]initWithFrame:CGRectMake(5, 245, 80, 20)];
    second.text = @"次要诊断";
    second.font = [UIFont systemFontOfSize:17];
    [scrollView addSubview:second];
    secondText = [[UITextView alloc]initWithFrame:CGRectMake(5, 270, Swidth-10, 60)];
    secondText.layer.borderColor = [UIColor grayColor].CGColor;
    secondText.layer.borderWidth = 1;
    secondText.delegate = self;
    [secondText setEditable:NO];
    secondText.font = [UIFont systemFontOfSize:15];
    secondText.text = [NSString stringWithFormat:@"    %@",_model.secondIos];
    [scrollView addSubview:secondText];
    
    if (persion.roleId.intValue == 2) {
        UILabel *reviewLab = [[UILabel alloc]initWithFrame:CGRectMake(5, 335, 80, 30)];
        reviewLab.text = @"审核:";
        reviewLab.font = [UIFont systemFontOfSize:17];
        [scrollView addSubview:reviewLab];
        
        approveBtn = [[UIButton alloc]initWithFrame:CGRectMake(90, 337.5, 25, 25)];
        approveBtn.layer.cornerRadius = 5;
        [approveBtn setImage:[UIImage imageNamed:@"normal"] forState:0];
        [approveBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
        [approveBtn addTarget:self action:@selector(reviewAction:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:approveBtn];
        
        UILabel *approveLab = [[UILabel alloc]initWithFrame:CGRectMake(125, 335, 80, 30)];
        approveLab.font = [UIFont systemFontOfSize:15];
        approveLab.text = @"通过";
        [scrollView addSubview:approveLab];
        
        rejectBtn = [[UIButton alloc]initWithFrame:CGRectMake(210, 337.5, 25, 25)];
        rejectBtn.layer.cornerRadius = 5;
        [rejectBtn addTarget:self action:@selector(reviewAction:) forControlEvents:UIControlEventTouchUpInside];
        [rejectBtn setImage:[UIImage imageNamed:@"normal"] forState:0];
        [rejectBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
        [scrollView addSubview:rejectBtn];
        
        if ([_model.caseTwo isEqualToString:@"已通过"]) {
            reviewStatus = @"approve";
            approveBtn.selected = YES;
            rejectBtn.selected = NO;
        }else if ([_model.caseTwo isEqualToString:@"未通过"]){
            reviewStatus = @"rejected";
            rejectBtn.selected = YES;
            approveBtn.selected = NO;
        }else{
            reviewStatus = @"waiting_approval";
            approveBtn.selected = NO;
            rejectBtn.selected = NO;
        }
        UILabel *rejectLab = [[UILabel alloc]initWithFrame:CGRectMake(245, 337.5, 80, 30)];
        rejectLab.text = @"未通过";
        rejectLab.font = [UIFont systemFontOfSize:15];
        [scrollView addSubview:rejectLab];
        //
        scrollView.contentSize = CGSizeMake(Swidth, 340);
    }
    
    if([_model.caseTwo isEqualToString:@"已通过"]||[_model.caseTwo isEqualToString:@"未通过"]){
        
    }
    
    //
    if (persion.roleId.intValue == 2) {
        [mainText setEditable:NO];
        [secondText setEditable:NO];
        
        UILabel *advice = [[UILabel alloc]initWithFrame:CGRectMake(5, 372.5, 80, 20)];
        advice.text = @"审核意见:";
        advice.font = [UIFont systemFontOfSize:17];
        [scrollView addSubview:advice];
        adviceText = [[UITextView alloc]initWithFrame:CGRectMake(5, 397.5, Swidth-10, 60)];
        adviceText.layer.borderColor = [UIColor grayColor].CGColor;
        adviceText.layer.borderWidth = 1;
        adviceText.text = _model.advice;
        
        adviceText.font = [UIFont systemFontOfSize:17];
        adviceText.delegate = self;
        [scrollView addSubview:adviceText];
        //
        if (![_model.caseTwo isEqualToString:@"待审核"]) {
            [adviceText setEditable:NO];
        }
        //
        scrollView.contentSize = CGSizeMake(Swidth, 430);
    }
    
}
#pragma -action
- (void)reviewAction:(UIButton *)btn{
    btn.selected = !btn.selected;
    if (btn == approveBtn && btn.selected) {
        reviewStatus = @"approved";
        rejectBtn.selected = NO;
    }
    if (btn == rejectBtn &&  btn.selected) {
        reviewStatus = @"rejected";
        approveBtn.selected = NO;
    }
}
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)choose:(UIButton *)btn{
    btn.selected = !btn.selected;
    UIButton *agree = [self.view viewWithTag:100];
    UIButton *disagree = [self.view viewWithTag:101];
    if (btn.selected) {
        if (btn == agree) {
            disagree.selected = NO;
        }else{
            agree.selected = NO;
        }
    }
}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    currentTextview = textView;
    _activedTextFieldRect = textView.frame;
    return YES;
}
//修改
- (void)selectAction:(UIButton *)btn{
    [SubjectTool share].subjectType = subjectTypeUpdate;
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (persion.roleId.intValue == 2) {
        //
        if ([reviewStatus isEqualToString:@"waiting_approval"]) {
            [MBProgressHUD showToastAndMessage:@"请选择审批状态~" places:0 toView:nil];
            return;
        }
        NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentMedicalCaseRecords/%@/review",SimpleIp,_model.recordId];
        
        [RequestTools RequestWithURL:request Method:@"PUT" Params:@{@"reviewerId":persion.userID,@"reviewerComments":adviceText.text,@"reviewStatus":reviewStatus} Message:@"审批中..." Success:^(NSDictionary *result) {
            
            [MBProgressHUD showToastAndMessage:@"审批成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"审批失败~" places:0 toView:nil];
        }];
        return;
    }
    [SubjectTool share].subjectType = subjectTypeUpdate;
    SCaseRecordController *recordVC = [SCaseRecordController new];
    recordVC.myModel = self.model;
    [self.navigationController pushViewController:recordVC animated:YES];
}
@end
