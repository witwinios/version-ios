//
//  TypicalCasesAddViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/11.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseAddModel.h"
#import "TypicalCasesCell.h"
#import "TypicalCasesModel.h"
typedef void (^BackTypicalCases) (NSArray *arr);
@interface TypicalCasesAddViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchResultsUpdating,UITextFieldDelegate>
@property(strong,nonatomic)TCourseAddModel *PlanModel;
@property(nonatomic,getter=isEditing) BOOL editing;
@property(nonatomic,strong)NSMutableArray *TempArray;
-(void)setEditing:(BOOL)editing animated:(BOOL)animated;
@property(strong,nonatomic)BackTypicalCases backArr;
@end
