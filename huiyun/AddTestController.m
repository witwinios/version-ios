//
//  AddTestController.m
//  xiaoyun
//
//  Created by MacAir on 17/3/1.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "AddTestController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
@interface AddTestController ()
{
    UITextField *nameField;
    UITextView *desView;
    //保存的testId
    NSString *testId;
}
@end

@implementation AddTestController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setUI];
}
- (void)setUI{
    UILabel *nameLab = [UILabel new];
    nameLab.frame = CGRectMake(0, 69, 80, 30);
    nameLab.text = @"测试名称:";
    nameLab.textAlignment = 1;
    nameLab.font = [UIFont systemFontOfSize:15];
    nameLab.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:nameLab];
    nameField = [UITextField new];
    nameField.frame = CGRectMake(82, 69, WIDTH-84, 30);
    nameField.borderStyle = UITextBorderStyleBezel;
    nameField.layer.borderColor = [[UIColor colorWithRed:128.0/255.0 green:128.0/255.0 blue:128.0/255.0 alpha:1.0]CGColor];
    nameField.layer.borderWidth = 1.0;
    nameLab.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:nameField];
    //
    UILabel *desLab = [UILabel new];
    desLab.frame = CGRectMake(0, 105, 80, 30);
    desLab.text = @"测试说明:";
    desLab.textAlignment = 1;
    desLab.font = [UIFont systemFontOfSize:15];
    desLab.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:desLab];
    desView = [UITextView new];
    desView.frame = CGRectMake(82, 105, WIDTH-84, 120);
    desView.layer.borderColor = [[UIColor colorWithRed:128.0/255.0 green:128.0/255.0 blue:128.0/255.0 alpha:1.0]CGColor];
    desView.layer.borderWidth = 1.0;
    [desView.layer setMasksToBounds:YES];
    desView.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:desView];
    //新增按钮
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addButton.frame = CGRectMake(self.view.center.x-40, 231, 80, 40);
    [addButton setTitle:@"新增试卷" forState:UIControlStateNormal];
    addButton.layer.borderColor = [[UIColor colorWithRed:128.0/255.0 green:128.0/255.0 blue:128.0/255.0 alpha:1.0]CGColor];
    addButton.layer.borderWidth = 1.0;
    addButton.layer.cornerRadius = 5.0;
    [addButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [addButton addTarget:self action:@selector(addTest:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addButton];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"自测练习";
    [backNavigation addSubview:titleLab];
}
- (void)loadTest{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/selfTests",LocalIP];
    
    NSDictionary *params = @{@"testName":nameField.text,@"description":desView.text,@"selfType":@"SELF_TEST"};
    
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Message:@"新增中" Success:^(NSDictionary *responseDic) {
        
        if ([[responseDic objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
            testId = [[responseDic objectForKey:@"responseBody"] objectForKey:@"testId"];
            AddPaperController *paperVC = [AddPaperController new];
            paperVC.superTab = @"addTestVC";
            paperVC.testID = testId;
            [self.navigationController pushViewController:paperVC animated:YES];
        }else {
            [MBProgressHUD showToastAndMessage:@"新增失败,名称重复!" places:0 toView:nil];
        }
        
    } failed:^(NSString *result) {
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [nameField resignFirstResponder];
    [desView resignFirstResponder];
}
#pragma -Action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addTest:(UIButton *)btn{
    if ([nameField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"测试名称不能为空!" places:0 toView:nil];
    }else {
        //添加测试
        [self loadTest];
    }
}
@end
