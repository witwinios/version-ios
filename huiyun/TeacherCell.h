//
//  TeacherCell.h
//  yun
//
//  Created by MacAir on 2017/5/27.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeacherCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *teacherName;
@property (weak, nonatomic) IBOutlet UILabel *teacherEmail;
@property (weak, nonatomic) IBOutlet UILabel *teacherPhone;
@property (copy, nonatomic) NSString *phoneNum;
- (IBAction)emailAction:(id)sender;
- (IBAction)phoneAction:(id)sender;

@end
