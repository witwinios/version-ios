//
//  TeacherModel.h
//  huiyun
//
//  Created by Bad on 2018/3/22.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeacherModel : NSObject

@property(nonatomic,strong)NSNumber *TeacherId;
@property(nonatomic,strong)NSString *TeacherName;

@end
