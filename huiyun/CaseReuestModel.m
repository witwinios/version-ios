//
//  CaseReuestModel.m
//  huiyun
//
//  Created by MacAir on 2017/10/11.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseReuestModel.h"

@implementation CaseReuestModel
//@property (strong, nonatomic) NSNumber *caseReuestModelRecordId;
//@property (strong, nonatomic) NSNumber *CaseReuestModelRequirementId;
//@property (strong, nonatomic) NSString *CaseReuestModelName;
- (void)setCaseReuestModelType:(NSString *)CaseReuestModelType{
    if ([CaseReuestModelType isKindOfClass:[NSNull class]]) {
        _CaseReuestModelType = @"暂无";
    }else if ([CaseReuestModelType isEqualToString:@"new_case"]){
        _CaseReuestModelType = @"新收病人";
    }else if ([CaseReuestModelType isEqualToString:@"standard_case"]){
        _CaseReuestModelType = @"标准病人";
    }else if ([CaseReuestModelType isEqualToString:@"case_discussion"]){
        _CaseReuestModelType = @"病例讨论";
    }else if ([CaseReuestModelType isEqualToString:@"academic_event"]){
        _CaseReuestModelType = @"学术活动";
    }else if ([CaseReuestModelType isEqualToString:@"director_inspection"]){
        _CaseReuestModelType = @"主任查房";
    }else if ([CaseReuestModelType isEqualToString:@"others"]){
        _CaseReuestModelType = @"其他学习";
    }else if ([CaseReuestModelType isEqualToString:@"pathological_discussion"]){
        _CaseReuestModelType = @"病例讨论";
    }else if ([CaseReuestModelType isEqualToString:@"reading"]){
        _CaseReuestModelType = @"阅读参考书刊";
    }else{
        
    }
}
- (void)setCaseReuestModelName:(NSString *)CaseReuestModelName{
    if ([CaseReuestModelName isKindOfClass:[NSNull class]]) {
        _CaseReuestModelName = @"";
    }else{
        _CaseReuestModelName = CaseReuestModelName;
    }
}
//@property (strong, nonatomic) NSString *CaseReuestModelType;
//@property (strong, nonatomic) NSString *CaseReuestModelLevel;
- (void)setCaseReuestModelLevel:(NSString *)CaseReuestModelLevel{
    if ([CaseReuestModelLevel isKindOfClass:[NSNull class]]) {
        _CaseReuestModelLevel = @"暂无";
    }else if ([CaseReuestModelLevel isEqualToString:@"required"]){
        _CaseReuestModelLevel = @"必须";
    }else if ([CaseReuestModelLevel isEqualToString:@"optional"]){
        _CaseReuestModelLevel = @"可选";
    }
}
//@property (strong, nonatomic) NSNumber  *CaseReuestModelNums;
- (void)setCaseReuestModelNums:(NSNumber *)CaseReuestModelNums{
    if ([CaseReuestModelNums isKindOfClass:[NSNull class]]) {
        _CaseReuestModelNums = @0;
    }else{
        _CaseReuestModelNums = CaseReuestModelNums;
    }
}
//@property (strong, nonatomic) NSNumber *total;
- (void)setTotal:(NSNumber *)total{
    if ([total isKindOfClass:[NSNull class]]) {
        _total = @0;
    }else{
        _total = total;
    }
}
//@property (strong, nonatomic) NSNumber *pass;
- (void)setPass:(NSNumber *)pass{
    if ([pass isKindOfClass:[NSNull class]]) {
        _pass = @0;
    }else{
        _pass = pass;
    }
}
//@property (strong, nonatomic) NSNumber *refer;
- (void)setRefer:(NSNumber *)refer{
    if ([refer isKindOfClass:[NSNull class]]) {
        _refer = @0;
    }else{
        _refer = refer;
    }
}
//@property (strong, nonatomic) NSNumber *pend;
- (void)setPend:(NSNumber *)pend{
    if ([pend isKindOfClass:[NSNull class]]) {
        _pend = @0;
    }else{
        _pend = pend;
    }
}
@end
