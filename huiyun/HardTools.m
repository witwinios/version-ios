//
//  HardTools.m
//  huiyun
//
//  Created by MacAir on 2017/11/6.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "HardTools.h"

@implementation HardTools
+ (BOOL)captureEnable{
    __block BOOL boolCapture = NO;
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (device) {
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (status == AVAuthorizationStatusNotDetermined) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                boolCapture = granted;
            }];
        } else if (status == AVAuthorizationStatusAuthorized) {
            // 用户允许当前应用访问相机
            boolCapture = YES;
        } else if (status == AVAuthorizationStatusDenied) {
            // 用户拒绝当前应用访问相机
            boolCapture = NO;
        } else if (status == AVAuthorizationStatusRestricted) {
            boolCapture = NO;
        }
    } else {
        boolCapture = NO;
    }
    return boolCapture;
}
/*
 AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
 if (device) {
 AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
 if (status == AVAuthorizationStatusNotDetermined) {
 [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
 if (granted) {
 dispatch_sync(dispatch_get_main_queue(), ^{
 ScanController *scanVC = [ScanController new];
 scanVC.btnIndex = [NSNumber numberWithLong:200];
 [self.navigationController pushViewController:scanVC animated:YES];
 });
 NSLog(@"用户第一次同意了访问相机权限 - - %@", [NSThread currentThread]);
 } else {
 NSLog(@"用户第一次拒绝了访问相机权限 - - %@", [NSThread currentThread]);
 }
 }];
 } else if (status == AVAuthorizationStatusAuthorized) { // 用户允许当前应用访问相机
 ScanController *scanVC = [ScanController new];
 scanVC.btnIndex = [NSNumber numberWithLong:200];
 [self.navigationController pushViewController:scanVC animated:YES];
 } else if (status == AVAuthorizationStatusDenied) { // 用户拒绝当前应用访问相机
 [[[UIAlertView alloc]initWithTitle:@"提示" message:@"请去-> [设置 - 隐私 - 相机 - 慧云医疗] 打开访问开关" delegate:self cancelButtonTitle:@"" otherButtonTitles:@"确定", nil] show];
 } else if (status == AVAuthorizationStatusRestricted) {
 NSLog(@"因为系统原因, 无法访问相册");
 }
 } else {
 [MBProgressHUD showToastAndMessage:@"未检测到你的摄像头!" places:0 toView:nil];
 }
 */
@end

