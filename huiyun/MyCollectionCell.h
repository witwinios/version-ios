//
//  MyCollectionCell.h
//  huiyun
//
//  Created by 王慕铁 on 2018/2/26.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionModel.h"
#import "RadioButton.h"
@interface MyCollectionCell : UICollectionViewCell
@property(nonatomic,strong)UILabel *QuestionTypes;
@property(nonatomic,strong)UILabel *QusetionTitle;
@property(nonatomic,strong)UILabel *ChoiceOptionsLabel;
@property(nonatomic,strong)RadioButton *ChoiceOptionsBtn;
@property(nonatomic,strong)UIButton *AnswersBtn;
@property(nonatomic,strong)UILabel *AnswersLabel;
@property(nonatomic,strong)NSMutableArray *OptionArray;//A~Z

@property(nonatomic) CGFloat ChoiceOptionsLabel_h;
@property(nonatomic) CGFloat ChoiceOptionsBtn_h;
@property(nonatomic) CGFloat AnswersLabel_h;



-(void)setQuestion:(QuestionModel *)model;


@end
