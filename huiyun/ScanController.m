//
//  SGQRCodeScanningVC.m
//  SGQRCodeExample
//
//  Created by kingsic on 17/3/20.
//  Copyright © 2017年 kingsic. All rights reserved.
//

#import "ScanController.h"
#import "SGQRCode.h"


@interface ScanController () <SGQRCodeScanManagerDelegate, SGQRCodeAlbumManagerDelegate>
{
    UILabel  *titleLb;
}
@property (nonatomic, strong) SGQRCodeScanManager *manager;
@property (nonatomic, strong) SGQRCodeScanningView *scanningView;
@property (nonatomic, strong) UIButton *flashlightBtn;
@property (nonatomic, strong) UILabel *promptLabel;
@property (nonatomic, assign) BOOL isSelectedFlashlightBtn;
@property (nonatomic, strong) UIView *bottomView;
@end

@implementation ScanController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.scanningView addTimer];
    [_manager resetSampleBufferDelegate];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeScanningView];
    [self.scanningView removeTimer];
    [self removeFlashlightBtn];
    [_manager cancelSampleBufferDelegate];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view addSubview:self.scanningView];
    [self setupNavigationBar];
    [self setupQRCodeScanning];
    [self.view addSubview:self.promptLabel];
    [self.view addSubview:self.bottomView];
}

- (void)setupNavigationBar {
    UIView *myview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    myview.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:myview];
    
    titleLb = [[UILabel alloc]initWithFrame:CGRectMake(50, 20, Swidth-100, 44)];
    if (_btnIndex.intValue == 0) {
        titleLb.text = @"签入扫描";
    }else if (_btnIndex.intValue == 200){
        titleLb.text = @"设置地址";
    }else{
        titleLb.text = @"签出扫描";
    }
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.textColor = [UIColor whiteColor];
    [myview addSubview:titleLb];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    
    [myview addSubview:leftBtn];
}

- (SGQRCodeScanningView *)scanningView {
    if (!_scanningView) {
        _scanningView = [[SGQRCodeScanningView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0.9 * self.view.frame.size.height)];
    }
    return _scanningView;
}
- (void)removeScanningView {
    [self.scanningView removeTimer];
    [self.scanningView removeFromSuperview];
    self.scanningView = nil;
}

- (void)rightBarButtonItenAction {
    SGQRCodeAlbumManager *manager = [SGQRCodeAlbumManager sharedManager];
    [manager readQRCodeFromAlbumWithCurrentController:self];
    manager.delegate = self;
    
    if (manager.isPHAuthorization == YES) {
        [self.scanningView removeTimer];
    }
}

- (void)setupQRCodeScanning {
    self.manager = [SGQRCodeScanManager sharedManager];
    NSArray *arr = @[AVMetadataObjectTypeQRCode, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code];
    // AVCaptureSessionPreset1920x1080 推荐使用，对于小型的二维码读取率较高
    [_manager setupSessionPreset:AVCaptureSessionPreset1920x1080 metadataObjectTypes:arr currentController:self];
    //    [manager cancelSampleBufferDelegate];
    _manager.delegate = self;
}
-(NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString{
    NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
    return responseJSON;
}
#pragma mark - - - SGQRCodeAlbumManagerDelegate
- (void)QRCodeAlbumManagerDidCancelWithImagePickerController:(SGQRCodeAlbumManager *)albumManager {
    [self.view addSubview:self.scanningView];
}
- (void)QRCodeAlbumManager:(SGQRCodeAlbumManager *)albumManager didFinishPickingMediaWithResult:(NSString *)result {
    if ([result hasPrefix:@"http"]) {
        
    } else {
        
    }
}

#pragma mark - - - SGQRCodeScanManagerDelegate
- (void)QRCodeScanManager:(SGQRCodeScanManager *)scanManager didOutputMetadataObjects:(NSArray *)metadataObjects {
    if (metadataObjects != nil && metadataObjects.count > 0) {
        [scanManager stopRunning];
        //播放声音
        static SystemSoundID shake_sound_male_id = 0;
        NSString * path = [[NSBundle mainBundle]pathForResource:@"5383" ofType:@"wav"];
        if (path) {
            AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:path]),&shake_sound_male_id);
            AudioServicesPlaySystemSound(shake_sound_male_id);
        }
        AudioServicesPlaySystemSound(shake_sound_male_id);
        AVMetadataMachineReadableCodeObject *metadataObject = metadataObjects[0];
        
        NSDictionary *itemInfo = [self parseJSONStringToNSDictionary:metadataObject.stringValue];
        
        if ([itemInfo count] ==0) {
            [MBProgressHUD showToastAndMessage:@"二维码错误,请重新扫描~" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dissMiss) userInfo:nil repeats:NO];
        }else{
            if (_btnIndex.intValue == 200) {
                [self setDefaults:metadataObject];
            }else{
                if ([[itemInfo allKeys] containsObject:@"createTime"]) {
                    NSNumber *createTime = itemInfo[@"createTime"];
                    double createTimes = [createTime doubleValue];
                    double currentTime = [[NSDate date] timeIntervalSince1970];
                    double inter = currentTime - createTimes;
                    
                    if (inter > 10) {
                        [MBProgressHUD showToastAndMessage:@"二维码失效,请重新扫描~" places:0 toView:nil];
                        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dissMiss) userInfo:nil repeats:NO];
                        return;
                    }
                }
                if (_btnIndex.intValue == 0) {
                    [self signInAction:itemInfo];
                }else{
                    [self signOutAction:itemInfo];
                }
            }
        }
    } else {
        [MBProgressHUD showToastAndMessage:@"未识别的二维码~" places:0 toView:nil];
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dissMiss) userInfo:nil repeats:NO];
    }
}
- (void)setDefaults:(AVMetadataMachineReadableCodeObject *)metadataObject{
    NSString *hosName = [[Maneger parseJSONStringToNSDictionary:metadataObject.stringValue] objectForKey:@"organizationName"];
    HospitalEntity *hosEntity = [HospitalEntity new];
    hosEntity.hosName = hosName;
    [[NSuserDefaultManager share] saveHospital:hosEntity];
    //字符串分割
    NSString *ipStr = [[Maneger parseJSONStringToNSDictionary:metadataObject.stringValue] objectForKey:@"url"];
    NSArray *ipArray = [ipStr componentsSeparatedByString:@"?code="];
    //判断扫描是否正确
    if (ipArray.count == 2) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        //拼接URL
        [defaults setObject:[NSString stringWithFormat:@"%@/witwin-ctts-web",ipArray[0] ]forKey:@"scanIP"];
        
        [defaults setObject:ipArray[0] forKey:@"simpleIp"];
        [defaults setObject:ipArray[1] forKey:@"scanCODE"];
        [defaults synchronize];
        [MBProgressHUD showToastAndMessage:@"设置成功~" places:0 toView:nil];
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dissMiss) userInfo:nil repeats:NO];
    }else{
        [MBProgressHUD showToastAndMessage:@"设置失败,请重新扫描!" places:0 toView:nil];
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dissMiss) userInfo:nil repeats:NO];
    }
}
- (void)signInAction:(NSDictionary *)itemInfo{
    PersonEntity *entity = [[NSuserDefaultManager share] readCurrentUser];
    NSString *localAddresses = [[NSUserDefaults standardUserDefaults] objectForKey:@"localAddress"];
    if ([localAddresses isKindOfClass:[NSNull class]] || localAddresses == nil || localAddresses == NULL) {
        localAddresses = @"";
    }
    NSDictionary *pamas = @{@"itemInfo":itemInfo,@"userId":entity.userID,@"signLocation":localAddresses,@"signTime":[NSString stringWithFormat:@"%.0f",[[NSDate date] timeIntervalSince1970]]};
    NSLog(@"params=%@",pamas);
    NSString *requestUrl = [NSString stringWithFormat:@"%@/attendance/signIn",LocalIP];
    //
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:pamas Message:@"" Success:^(NSDictionary *responseDic) {
        
        if ([[responseDic objectForKey:@"responseStatus"] isEqualToString:@"failed"]) {
            NSString *errorCode = [responseDic objectForKey:@"errorCode"];
            if ([errorCode isEqualToString:@"invalid_user_to_this_item"]) {
                [MBProgressHUD showToastAndMessage:@"您不在这场考试中~" places:0 toView:nil];
            }else if ([errorCode isEqualToString:@"repeat_sign_in"]){
                [MBProgressHUD showToastAndMessage:@"您已经签到~" places:0 toView:nil];
            }else if ([errorCode isEqualToString:@"course_schedule_not_found"]){
                [MBProgressHUD showToastAndMessage:@"签到失败,课程没找到~" places:0 toView:nil];
            }else{
                [MBProgressHUD showToastAndMessage:@"签到失败,请重新扫描~" places:0 toView:nil];
            }
            [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dissMiss) userInfo:nil repeats:NO];
            return ;
        }
        [MBProgressHUD showToastAndMessage:@"签到成功~" places:0 toView:nil];
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dissMiss) userInfo:nil repeats:NO];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"签到失败,请重新扫描~" places:0 toView:nil];
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dissMiss) userInfo:nil repeats:NO];
    }];
}
- (void)dissMiss{
    [_manager videoPreviewLayerRemoveFromSuperlayer];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)signOutAction:(NSDictionary *)itemInfo{
    PersonEntity *entity = [[NSuserDefaultManager share] readCurrentUser];
    NSString *localAddresses = [[NSUserDefaults standardUserDefaults] objectForKey:@"localAddress"];
    if ([localAddresses isKindOfClass:[NSNull class]]) {
        localAddresses = @"";
    }
    
    NSDictionary *pamas = @{@"itemInfo":itemInfo,@"userId":entity.userID,@"signLocation":localAddresses,@"signTime":[[Maneger shareObject] currentLong]};
    NSString *requestUrl = [NSString stringWithFormat:@"%@/attendance/signOut",LocalIP];
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:pamas Message:@"" Success:^(NSDictionary *responseDic) {
        NSLog(@"%@",responseDic);
        if ([[responseDic objectForKey:@"responseStatus"] isEqualToString:@"succeccd"]) {
            [MBProgressHUD showToastAndMessage:@"签出成功~" places:0 toView:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            if ([[responseDic objectForKey:@"responseStatus"] isEqualToString:@"failed"]) {
            
            if ([[responseDic objectForKey:@"errorCode"] isEqualToString:@"invalid_user_to_this_item"]) {
                [MBProgressHUD showToastAndMessage:@"您不在这场考试中~" places:0 toView:nil];
                return ;
            }
            if ([[responseDic objectForKey:@"errorCode"] isEqualToString:@"repeat_sign_in"]) {
                [MBProgressHUD showToastAndMessage:@"重复签出~" places:0 toView:nil];
                NSLog(@"0");
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
            }
        }
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dissMiss) userInfo:nil repeats:NO];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"签出失败~" places:0 toView:nil];
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dissMiss) userInfo:nil repeats:NO];
    }];
}
- (void)QRCodeScanManager:(SGQRCodeScanManager *)scanManager brightnessValue:(CGFloat)brightnessValue {
    if (brightnessValue < - 1) {
        [self.view addSubview:self.flashlightBtn];
    } else {
        if (self.isSelectedFlashlightBtn == NO) {
            [self removeFlashlightBtn];
        }
    }
}

- (UILabel *)promptLabel {
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc] init];
        _promptLabel.backgroundColor = [UIColor clearColor];
        CGFloat promptLabelX = 0;
        CGFloat promptLabelY = 0.73 * self.view.frame.size.height;
        CGFloat promptLabelW = self.view.frame.size.width;
        CGFloat promptLabelH = 25;
        _promptLabel.frame = CGRectMake(promptLabelX, promptLabelY, promptLabelW, promptLabelH);
        _promptLabel.textAlignment = NSTextAlignmentCenter;
        _promptLabel.font = [UIFont boldSystemFontOfSize:13.0];
        _promptLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        _promptLabel.text = @"将二维码放入框内";
    }
    return _promptLabel;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.scanningView.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(self.scanningView.frame))];
        _bottomView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    }
    return _bottomView;
}

#pragma mark - - - 闪光灯按钮
- (UIButton *)flashlightBtn {
    if (!_flashlightBtn) {
        // 添加闪光灯按钮
        _flashlightBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        CGFloat flashlightBtnW = 30;
        CGFloat flashlightBtnH = 30;
        CGFloat flashlightBtnX = 0.5 * (self.view.frame.size.width - flashlightBtnW);
        CGFloat flashlightBtnY = 0.55 * self.view.frame.size.height;
        _flashlightBtn.frame = CGRectMake(flashlightBtnX, flashlightBtnY, flashlightBtnW, flashlightBtnH);
        [_flashlightBtn setBackgroundImage:[UIImage imageNamed:@"SGQRCodeFlashlightOpenImage"] forState:(UIControlStateNormal)];
        [_flashlightBtn setBackgroundImage:[UIImage imageNamed:@"SGQRCodeFlashlightCloseImage"] forState:(UIControlStateSelected)];
        [_flashlightBtn addTarget:self action:@selector(flashlightBtn_action:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _flashlightBtn;
}

- (void)flashlightBtn_action:(UIButton *)button {
    if (button.selected == NO) {
        [SGQRCodeHelperTool SG_openFlashlight];
        self.isSelectedFlashlightBtn = YES;
        button.selected = YES;
    } else {
        [self removeFlashlightBtn];
    }
}

- (void)removeFlashlightBtn {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SGQRCodeHelperTool SG_CloseFlashlight];
        self.isSelectedFlashlightBtn = NO;
        self.flashlightBtn.selected = NO;
        [self.flashlightBtn removeFromSuperview];
    });
}
#pragma -Action
- (void)leftBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
@end

