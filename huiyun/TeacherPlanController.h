//
//  TeacherPlanController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/30.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPopview.h"
@protocol TeacherPlanControllerDelegate<NSObject>

@optional

-(void)PlanName:(NSString *)Nametr PlanID:(NSString *)planId;

@end

@interface TeacherPlanController : UIViewController<UITableViewDelegate,UITableViewDataSource,selectIndexPathDelegate,UISearchBarDelegate,UITextFieldDelegate>

@property(nonatomic,assign)id<TeacherPlanControllerDelegate> delegate;

@property(strong,nonatomic) NSString *TempStr;

@property(nonatomic,getter=isEditing) BOOL editing;
-(void)setEditing:(BOOL)editing animated:(BOOL)animated;

@property(nonatomic,strong)NSString *CourseStr;

@end
