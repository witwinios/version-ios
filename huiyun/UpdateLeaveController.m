//
//  UpdateLeaveController.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/11.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "UpdateLeaveController.h"
#define UILABEL_LINE_SPACE 13
@interface UpdateLeaveController ()
{
    NSMutableArray *absenceItem;
    UITableView *tabView;
}
@end

@implementation UpdateLeaveController

- (void)viewDidLoad {
    [super viewDidLoad];
    absenceItem = [NSMutableArray new];
    //创建视图
    [self setNav];
    [self loadAbsenceItem];

}
- (void)loadAbsenceItem{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/attendance/items?",LocalIP];
    NSDictionary *params = @{@"userId":LocalUserId,@"leaveTimeStart":[[Maneger shareObject] timeLineFormatter:self.model.leaveStartTime.stringValue],@"leaveTimeEnd":[[Maneger shareObject] timeLineFormatter:self.model.leaveEndTime.stringValue]};
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:params Message:@"获取中..." Success:^(NSDictionary *result) {
        NSArray *array = [result objectForKey:@"responseBody"];
        
        for (int i=0; i<array.count; i++) {
            NSDictionary *responseDic = array[i];
            AbsenceItem *item = [AbsenceItem new];
            item.itemType = [responseDic objectForKey:@"attendanceItem"];
            item.itemId = [responseDic objectForKey:@"itemId"];
            item.itemName = [responseDic objectForKey:@"itemName"];
            item.itemEndTime = [responseDic objectForKey:@"endTime"];
            item.itemStartTime = [responseDic objectForKey:@"startTime"];
            [absenceItem addObject:item];
        }
        
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"加载错误" places:0 toView:nil];
    }];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    if (![_model.approveStatus isEqualToString:@"同意"]) {
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame = CGRectMake(self.view.frame.size.width-70, 29.5, 70, 25);
        rightBtn.layer.cornerRadius = 5;
        rightBtn.layer.masksToBounds = YES;
        rightBtn.layer.borderColor = [UIColor whiteColor].CGColor;
        rightBtn.layer.borderWidth = 1;
        [rightBtn setTitle:@"修改" forState:UIControlStateNormal];
        [rightBtn addTarget:self action:@selector(updateAction:) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:rightBtn];
    }
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"详情";
    [backNavigation addSubview:titleLab];
    //
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.bounces = NO;
    tabView.backgroundColor = UIColorFromHex(0xF0F0F0);
    tabView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"CellStyle1" bundle:nil] forCellReuseIdentifier:@"styleone"];
    [tabView registerNib:[UINib nibWithNibName:@"CellStyle2" bundle:nil] forCellReuseIdentifier:@"styletwo"];
    [tabView registerNib:[UINib nibWithNibName:@"CellStyle3" bundle:nil] forCellReuseIdentifier:@"stylethree"];
    [self.view addSubview:tabView];
}
#pragma -aciton
//点击查看图片
- (void)imgAction:(UIButton *)btn{
    NSString *imgStr = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",SimpleIp,self.model.fileUrl,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
    NSLog(@"%@",imgStr);
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    UIColor *color = [UIColor lightGrayColor];
    view.backgroundColor = [color colorWithAlphaComponent:0.5];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, Swidth/2, Sheight/2)];
    imgView.layer.cornerRadius = 5;
    imgView.layer.masksToBounds = YES;
    imgView.center = view.center;
    [view addSubview:imgView];
    __block UIProgressView *pv;
    __weak UIImageView *weakImageView = imgView;
    [imgView sd_setImageWithURL:[NSURL URLWithString:imgStr]
                placeholderImage:[UIImage imageNamed:@"holdimage"]
                         options:SDWebImageCacheMemoryOnly
                        progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                            if (!pv) {
                                [weakImageView addSubview:pv = [UIProgressView.alloc initWithProgressViewStyle:UIProgressViewStyleDefault]];
                                pv.frame = CGRectMake(0, 0, Swidth/2, 40);
                            }
                            CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 8.0f);
                            pv.transform = transform;
                            float showProgress = (float)receivedSize/(float)expectedSize;
                            NSLog(@"进度:%f",showProgress);
                            [pv setProgress:showProgress];
                        }
                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) { 
                           [pv removeFromSuperview]; 
                           pv = nil; 
                       }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeAction:)];
    tap.numberOfTapsRequired = 1;
    [view addGestureRecognizer:tap];
    
    [self.view addSubview:view];
}
- (void)removeAction:(UITapGestureRecognizer *)tap{
    UIView *view = tap.view;
    [view removeFromSuperview];
}
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
//修改请假
- (void)updateAction:(UIButton *)btn{
    UpdateLeaveDetailController *detailVC = [UpdateLeaveDetailController new];
    detailVC.historyModel = self.model;
    [self.navigationController pushViewController:detailVC animated:YES];
}
#pragma -协议
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //计算请假原因
    CGFloat reasonH = [Maneger getPonentH:self.model.leaveReason andFont:[UIFont systemFontOfSize:18] andWidth:Swidth-116];
    //计算缺勤项目
    NSMutableString *absenStr = [NSMutableString new];
    for (int s=0; s<absenceItem.count; s++) {
        AbsenceItem *item = absenceItem[s];
        if (s!=absenceItem.count-1) {
            [absenStr appendString:[NSString stringWithFormat:@"%@(%@),",item.itemName,item.itemType]];
        }else{
            [absenStr appendString:[NSString stringWithFormat:@"%@(%@)。",item.itemName,item.itemType]];
        }
    }
    CGFloat absenceH =[Maneger getPonentH:absenStr andFont:[UIFont systemFontOfSize:18] andWidth:Swidth-116];
    //计算审批意见
    CGFloat commentH = [Maneger getPonentH:self.model.reviewComments andFont:[UIFont systemFontOfSize:18] andWidth:Swidth-116];
    
    if (indexPath.row == 3) {
        return reasonH+25;
    }else if (indexPath.row == 5){
        return absenceH+25;
    }else if (indexPath.row == 6){
        return commentH+25;
    }else{
        return 40.f;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellStyle1 *cell1 = [tabView dequeueReusableCellWithIdentifier:@"styleone"];
    cell1.selectionStyle = UITableViewCellSelectionStyleNone;
    CellStyle2 *cell2 = [tabView dequeueReusableCellWithIdentifier:@"styletwo"];
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    CellStyle3 *cell3 = [tabView dequeueReusableCellWithIdentifier:@"stylethree"];
    cell3.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row==0) {
        cell1.leaveTime.text = [[Maneger shareObject] timeFormatter:self.model.leaveStartTime.stringValue];
        cell1.timeLabel.text = @"开始时间:";
        cell1.leaveStatus.text = self.model.approveStatus;
        return cell1;
    }else if (indexPath.row == 1){
        cell1.timeLabel.text = @"结束时间:";
        cell1.leaveTime.text = [[Maneger shareObject] timeFormatter:self.model.leaveEndTime.stringValue];
        cell1.leaveStatus.alpha = 0;
        return cell1;
    }else if (indexPath.row == 2){
        cell2.title.text = @"请假类型:";
        cell2.content.text = [self.model leaveType];
        return cell2;
    }else if (indexPath.row == 3){
        cell2.title.text = @"请假人:";
        cell2.content.text = [self.model requestName];
        return cell2;
    }else if (indexPath.row == 4){
        cell2.title.text = @"请假原因:";
        cell2.content.text = self.model.leaveReason;
        return cell2;
    }else if (indexPath.row == 5){
        if ([self.model.fileUrl isEqualToString:@""]) {
            [cell3.imgBtn setTitle:@"暂无" forState:0];
        }else{
            [cell3.imgBtn setTitle:@"查看" forState:0];
            [cell3.imgBtn addTarget:self action:@selector(imgAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        return cell3;
    }else if (indexPath.row == 6){
        NSMutableString *absenStr = [NSMutableString new];
        for (int s=0; s<absenceItem.count; s++) {
            AbsenceItem *item = absenceItem[s];
            if (s!=absenceItem.count-1) {
                [absenStr appendString:[NSString stringWithFormat:@"%@(%@),",item.itemName,item.itemType]];
            }else{
                [absenStr appendString:[NSString stringWithFormat:@"%@(%@)。",item.itemName,item.itemType]];
            }
        }
        cell2.title.text = @"缺勤项目:";
        if ([absenStr isEqualToString:@""]) {
            cell2.content.text = @"无缺勤项目";
        }else{
            cell2.content.text = absenStr;
        }
        return cell2;
    }else{
        cell2.title.text = @"审批意见:";
        cell2.content.text = self.model.reviewComments;
        return cell2;
    }
}
@end
