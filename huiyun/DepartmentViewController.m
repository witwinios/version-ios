//
//  DepartmentViewController.m
//  huiyun
//
//  Created by Bad on 2018/3/14.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "DepartmentViewController.h"

@interface DepartmentViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    UITableView *tableVc;
    NSMutableArray *DepartmentDataArray; //部门数据
    NSMutableArray *DepartmentIdArray;   //部门Id
    NSMutableArray *DataArray;
    
    NSString *DepStr;
    NSString *DepId;
    
    NSArray *TitleArray;
    
    
    UIPickerView *Picker;
    UIView *BackVc;
    UIButton *okBtn;
    UIButton *noBtn;
    
}

@end

@implementation DepartmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    DepartmentDataArray=[NSMutableArray array];
    DepartmentIdArray=[NSMutableArray array];
    DataArray=[NSMutableArray array];
    
    [DataArray removeAllObjects];
    [DataArray addObjectsFromArray:_TempArray];
    
    [self setNav];
    
    [self setUI];
    
    [self LoadData];
    
    [self setUpRefresh];
    
    
}
- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    if([_str isEqualToString:@"科室"]){
        titleLab.text = @"科室";
    }else if([_str isEqualToString:@"班级"]){
        titleLab.text = @"班级";
    }
    
    
    
    [backNavigation addSubview:titleLab];
    
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-100, 29.5, 100, 25);
    [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(AddBtn) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;
    
    
    
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)AddBtn{
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"科室管理" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *AddAction = [UIAlertAction actionWithTitle:@"新增" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        [self setPickerVc];
        
    }];
    UIAlertAction *EditAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [Maneger showAlert:@"请左滑删除" andCurentVC:self];
        
        
        
    }];
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
    [AlertView addAction:BackAction];
    [AlertView addAction:AddAction];
    [AlertView addAction:EditAction];
    
    [self presentViewController:AlertView animated:YES completion:nil];
    
}

- (void)setUpRefresh{
    
    
    [tableVc addHeaderWithTarget:self action:@selector(downRefresh)];
    //设置文字
    tableVc.headerPullToRefreshText = @"下拉刷新";
    tableVc.headerReleaseToRefreshText = @"松开进行刷新";
    tableVc.headerRefreshingText = @"刷新中。。。";
    [tableVc headerBeginRefreshing];
    
    
    
}


-(void)LoadData{
    
    NSString *Url;
    
    if ([_str isEqualToString:@"科室"]) {
        Url=[NSString stringWithFormat:@"%@/userGroups?groupType=department_type&pageSize=999",LocalIP];
    }else if([_str isEqualToString:@"班级"]){
        Url=[NSString stringWithFormat:@"%@/userGroups?groupType=class_type&pageSize=999",LocalIP];
    }
    
    
    NSLog(@"%@",Url);
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            return;
        }
        [DepartmentDataArray removeAllObjects];
        [DepartmentIdArray removeAllObjects];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            
            if (array.count ==0) {
                NSLog(@"无数据");
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dic = [array objectAtIndex:i];
                    //                    DepartmentModel *model=[DepartmentModel new];
                    //                    model.GroupId=;
                    //                    model.GroupName=;
                    //                    model.GroupType=[dic objectForKey:@"groupType"];
                    
                    [DepartmentDataArray addObject:[dic objectForKey:@"groupName"]];
                    [DepartmentIdArray addObject:[dic objectForKey:@"groupId"]];
                }
                NSLog(@"DepartmentDataArray=%lu",(unsigned long)DepartmentDataArray.count);
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
        
    } failed:^(NSString *result) {
        NSLog(@"result==%@",result);
    }];
    
}

//新建 UIPickerView：
-(void)setPickerVc{
    if(BackVc !=nil){
        BackVc.hidden=YES;
    }
    
    //蒙版
    CGFloat BackVc_Y=Sheight/3;
    BackVc=[[UIView  alloc]initWithFrame:self.view.bounds];
    BackVc.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
    
    //工具栏
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0,Sheight-BackVc_Y , Swidth, BackVc_Y)];
    UIView *ToolView=[[UIView alloc]initWithFrame:CGRectMake(0, Picker.wwy_y-30, Swidth, 30)];
    ToolView.backgroundColor=UIColorFromHex(0x20B2AA);
    [BackVc addSubview:ToolView];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:noBtn];
    
    Picker.backgroundColor=[UIColor whiteColor];
    
    
    TitleArray=[NSArray arrayWithObject:@"科室"];
    
    Picker.dataSource=self;
    Picker.delegate=self;
    
    DepStr=DepartmentDataArray[0];
    DepId=DepartmentIdArray[0];
    
    [BackVc addSubview:Picker];
    
    [[UIApplication sharedApplication].keyWindow addSubview:BackVc];
    
}

-(void)downRefresh{
    
    if (DataArray.count==0) {
        [tableVc headerEndRefreshing];
    }
    
    
    [tableVc headerEndRefreshing];
}

-(void)setOkBtn{
    
    DepartmentModel *model=[DepartmentModel new];
    model.GroupId=DepId;
    model.GroupName=DepStr;
    model.GroupType=@"department_type";
    [DataArray addObject:model];
    
    
    [self setAdd:model];
    NSLog(@"DepId==%@",DepId);
    NSLog(@"DataArray1=%lu",(unsigned long)DataArray.count);
    BackVc.hidden=YES;
    
    
}

-(void)setNoBtn{
    BackVc.hidden=YES;
}

-(void)setAdd:(DepartmentModel *)model{
    
    NSString *url=[NSString stringWithFormat:@"%@/userGroups/%@/members/%@",LocalIP,model.GroupId,_UserId];
    
    [RequestTools RequestWithURL:url Method:@"post" Params:nil Success:^(NSDictionary *result) {
        
        NSLog(@"result==%@",result);
        
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            [tableVc reloadData];
            [MBProgressHUD showToastAndMessage:@"成功添加!" places:0 toView:nil];
        }
        
        
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
    
    
}

-(void)setUI{
    
    
    self.view.backgroundColor=UIColorFromHex(0xF4F4F4);
    
    tableVc=[[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    tableVc.delegate=self;
    tableVc.dataSource=self;
    tableVc.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableVc.showsVerticalScrollIndicator=NO;
    tableVc.tableFooterView = [UIView new];
    [tableVc registerNib:[UINib nibWithNibName:@"MyDepartmentCell" bundle:nil] forCellReuseIdentifier:@"MyDepartment"];
    
    
    
    [self.view addSubview:tableVc];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return DataArray.count;
}

//点击Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MyDepartmentCell *cell=(MyDepartmentCell *)[tableView dequeueReusableCellWithIdentifier:@"MyDepartment"];
    
    NSLog(@"DataArray3=%lu",(unsigned long)DataArray.count);
    
    DepartmentModel *model=DataArray[indexPath.row];
    
    [cell setModel:model];
    
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}


#pragma mark Picker
// UIPickerViewDataSource中定义的方法，该方法返回值决定该控件包含多少列
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView
{
    // 返回2表明该控件只包含2列
    return 1;
}

// UIPickerViewDataSource中定义的方法，该方法返回值决定该控件指定列包含多少个列表项
- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    
    return DepartmentDataArray.count;
}


// UIPickerViewDelegate中定义的方法，该方法返回的NSString将作为
// UIPickerView中指定列、指定列表项上显示的标题
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:
(NSInteger)row forComponent:(NSInteger)component
{
    
    return [DepartmentDataArray objectAtIndex:row];
}

// 当用户选中UIPickerViewDataSource中指定列、指定列表项时激发该方法
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"科室/班级===%@",[DepartmentDataArray objectAtIndex:row]);
    DepStr=[DepartmentDataArray objectAtIndex:row];
    DepId=[DepartmentIdArray objectAtIndex:row];
    
}

// UIPickerViewDelegate中定义的方法，该方法返回的NSString将作为
// UIPickerView中指定列的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView
    widthForComponent:(NSInteger)component
{
    return Swidth;
}

#pragma mark 编辑
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return   UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"你确定删除该科室？" preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            DepartmentModel *model=DataArray[indexPath.row];
            
            [DataArray removeObjectAtIndex:indexPath.row];
            [tableVc beginUpdates];
            [tableVc deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [self DeleteWithGroupId:model.GroupId];
            
            [tableVc endUpdates];
            
            NSLog(@"DepId==%@",DepId);
            
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
        }]];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

//设置进入编辑状态时，Cell不会缩进

- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return NO;
    
}

-(void)DeleteWithGroupId:(NSString *)groupId{
    
    NSString *url=[NSString stringWithFormat:@"%@/userGroups/%@/members/%@",LocalIP,groupId,_UserId];
    
    NSLog(@"%@",url);
    
    [RequestTools RequestWithURL:url Method:@"DELETE" Params:nil Success:^(NSDictionary *result) {
        
        NSString *resultStr=[result objectForKey:@"errorCode"];
        NSLog(@"resultStr:%@",resultStr);
        
        if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){
            if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
                
                [MBProgressHUD showToastAndMessage:@"科室成功删除!" places:0 toView:nil];
            }
            NSLog(@"result==%@",result);
        }else{
            
        }
        
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

