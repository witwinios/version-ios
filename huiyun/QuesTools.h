//
//  QuesTools.h
//  huiyun
//
//  Created by MacAir on 2017/12/28.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuestionModel.h"
/*
 控件开始坐标
 */
#define begin_X 5;
/*
 选项之间间隔
 */
#define choice_height 5;
/*
 大题干与选项之间
 */
#define BQues_choice_height 10;
/*
 小题干与选项之间
 */
#define SQues_choice_height 10;
/*
 选项和图片之间
 */
#define choice_image_height 10;
/*
 大标题大小
 */
#define BTitle_size 30;
/*
 小标题大小
 */
#define STitle_size 25.0;
/*
 选项字体大小
 */
#define choice_size 20;
@interface QuesTools : NSObject

+ (instancetype)shareInstance;

@property (strong, nonatomic) UIScrollView *currentScroll;
@property (strong, nonatomic) NSMutableArray *answerArr;

- (UIScrollView *)createUI:(QuestionModel *)model;
+ (NSMutableAttributedString *)getStr:(NSString *)string Color:(UIColor *)color;
@end
