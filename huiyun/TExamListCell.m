//
//  TExamListCell.m
//  huiyun
//
//  Created by MacAir on 2018/1/16.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TExamListCell.h"

@implementation TExamListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}
- (void)setModel:(id)models{
    if ([models isKindOfClass:[SkillModel class]]) {
        SkillModel *model = (SkillModel *)models;
        self.examNameLabel.text = model.skillName;
        self.examStatus.text = model.skillStatus;
        
    }else if ([models isKindOfClass:[onlineModel class]]){
        onlineModel *model = (onlineModel *)model;
        
    }else{
        OSCEModel *model = (OSCEModel *)model;
        
    }
}
@end
