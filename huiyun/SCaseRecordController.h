//
//  SCaseRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CaseReuestModel.h"
#import "WXPPickerView.h"
@interface SCaseRecordController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PickerViewOneDelegate>
@property (weak, nonatomic) IBOutlet UITextField *diseaseNameField;
@property (weak, nonatomic) IBOutlet UITextField *diseaseNumField;
@property (weak, nonatomic) IBOutlet UITextView *mainText;
@property (weak, nonatomic) IBOutlet UIButton *doctorBtn;
@property (weak, nonatomic) IBOutlet UITextView *secodText;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)saveAction:(id)sender;
- (IBAction)dateAction:(id)sender;
- (IBAction)doctorAction:(id)sender;
@property (strong, nonatomic)UIImagePickerController *imagePickController;

@property (strong, nonatomic) CaseReuestModel *model;
- (IBAction)fileAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) WXPPickerView *pickView;
@property (strong, nonatomic) CaseMedicalModel *myModel;

@property (weak, nonatomic) IBOutlet UILabel *Name;
@property (weak, nonatomic) IBOutlet UILabel *TwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *Threelabel;
@property (weak, nonatomic) IBOutlet UILabel *Fourlabel;

@end
