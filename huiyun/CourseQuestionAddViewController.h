//
//  CourseQuestionAddViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/5.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseAddModel.h"
#import "MyScoreField.h"
#import "CourseQuestionModel.h"

typedef void (^BackCourseQuestion) (NSArray *arr ,NSString *str);
@interface CourseQuestionAddViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchResultsUpdating,UITextFieldDelegate>
@property(strong,nonatomic)TCourseAddModel *PlanModel;
@property(nonatomic,getter=isEditing) BOOL editing;
@property(nonatomic,strong) NSString *str;
@property(nonatomic,strong)NSMutableArray *TempArr;
-(void)setEditing:(BOOL)editing animated:(BOOL)animated;
@property(strong,nonatomic)BackCourseQuestion backArr;

@property(strong,nonatomic)CourseQuestionModel *model;
@property(strong,nonatomic)NSMutableArray *TempArray;

@end
