//
//  TalkController.m
//  yun
//
//  Created by MacAir on 2017/5/23.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TalkController.h"

@interface TalkController ()

@end

@implementation TalkController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.image = [UIImage imageNamed:@"navigationbar"];
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(10, 34.5, 15, 15);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"图标"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"问题讨论";
    [backNavigation addSubview:titleLab];
    //设置描述
    UITextView *quesView = [UITextView new];
    quesView.frame = CGRectMake(0, 64, Swidth, 60);
    quesView.text = [NSString stringWithFormat:@"问题描述: %@",self.quesDes];
    quesView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    quesView.layer.borderWidth = 1;
    quesView.editable = NO;
    [self.view addSubview:quesView];
    //
}

#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)rightItemAction:(UIButton *)btn{
    
}
@end
