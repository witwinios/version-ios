//
//  RoundViewCell.m
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "RoundViewCell.h"

@implementation RoundViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setPro:(NSInteger)index Content:(NSArray *)arr{
    NSArray *recordArr = @[@"病例记录",@"操作记录",@"活动记录",@"教学记录",@"科研记录",@"获奖记录",@"论文记录",@"抢救记录",@"出诊记录",@"差错记录"];
    self.numLabel.text = [NSString stringWithFormat:@"%@",arr[index]];
    self.recordName.text = [NSString stringWithFormat:@"%@",recordArr[index]];
}
@end

