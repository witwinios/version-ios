//
//  AccountCell2.h
//  yun
//
//  Created by MacAir on 2017/8/24.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountCell2 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewName;
@property (weak, nonatomic) IBOutlet UILabel *titleName;
@end
