//
//  TExamListController.m
//  huiyun
//
//  Created by MacAir on 2018/1/15.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TExamListController.h"

@interface TExamListController ()

@end

@implementation TExamListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self navibarViewInit];
}
- (void)navibarViewInit{
    __weak typeof(self)weakSelf = self;
    self.rightBtn.frame = CGRectMake(self.view.frame.size.width-40, SafeAreaTopHeight - 5 -25, 25, 25);
    self.barBC = ^(int index){
        if (index == 0) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else if (index == 1){
            
        }else{
            
        }
    };
    //
    UISegmentedControl *segContrller = [[UISegmentedControl alloc]initWithItems:@[@"在线考试",@"技能考试",@"OSCE考试"]];
    segContrller.selectedSegmentIndex = 0;
    segContrller.tintColor = [UIColor whiteColor];
    segContrller.frame = self.titleLab.bounds;
    [segContrller addTarget:self action:@selector(segmentItemAction:) forControlEvents:UIControlEventValueChanged];
    [self.titleLab addSubview:segContrller];
    
    //添加search
    UISearchBar *searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, Swidth, 44)];
    searchBar.placeholder = @"请输入考试名称";
    searchBar.delegate = self;
    searchBar.barStyle = UIBarStyleDefault;
    self.seachBar = searchBar;
    [self.screenView addSubview:self.seachBar];
    //
    UITableView *table = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, Swidth,self.screenView.bounds.size.height - 44) style:UITableViewStylePlain];
    table.delegate = self;
    table.dataSource = self;
    table.backgroundColor = UIColorFromHex(0xF0F0F0);
    [table registerNib:[UINib nibWithNibName:@"TExamListCell" bundle:nil] forCellReuseIdentifier:@"examListCell"];
    [self.screenView addSubview:table];
}
#pragma -action
- (void)segmentItemAction:(UISegmentedControl *)sender{
    NSInteger selecIndex = sender.selectedSegmentIndex;
    switch (selecIndex) {
        case 0:
            
            break;
        case 1:
            
            break;
        case 2:
            
            break;
        default:
            break;
    }
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 157.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TExamListCell *listCell = [tableView dequeueReusableCellWithIdentifier:@"examListCell"];
    listCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return listCell;
}
@end
