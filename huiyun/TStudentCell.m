//
//  TStudentCell.m
//  yun
//
//  Created by MacAir on 2017/7/18.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TStudentCell.h"

@implementation TStudentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.TSImage.layer.cornerRadius = 5;
    self.TSImage.layer.masksToBounds = YES;
    self.TSImage.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
    self.TSImage.layer.borderWidth = 1;
}
@end
