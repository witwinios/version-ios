//
//  DepartController.m
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "DepartController.h"

@interface DepartController ()
{
    UITableView *table;
    NSMutableArray *dataArray;
    
    NSInteger currentPage;
    
    int IndexNum;
}
@end

@implementation DepartController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    currentPage = 1;
    IndexNum=0;
    [self setUI];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"shaixuan"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"轮转记录";
    [backNavigation addSubview:titleLab];
    //
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    table.delegate = self;
    table.dataSource = self;
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self setShuaXin];
    [table registerNib:[UINib nibWithNibName:@"DepartCell" bundle:nil] forCellReuseIdentifier:@"departCell"];
    [self.view addSubview:table];
   
}


-(void)setShuaXin{
    [table addHeaderWithTarget:self action:@selector(fresh)];
    [table addFooterWithTarget:self action:@selector(moreFresh)];
    //开始刷新
    [table headerBeginRefreshing];
}

- (void)fresh{
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    
    NSString *requestUrl;
    switch (IndexNum) {
        case 0:
            requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageStart=1&pageSize=15&mentorId=%@",SimpleIp,persion.userID];
            break;
        case 1:
            requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageStart=1&pageSize=15&mentorId=%@&rotationStatus=not_start",SimpleIp,persion.userID];
            break;
        case 2:
            requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageStart=1&pageSize=15&mentorId=%@&rotationStatus=in_department",SimpleIp,persion.userID];
            break;
        case 3:
            requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageStart=1&pageSize=15&mentorId=%@&rotationStatus=off_department",SimpleIp,persion.userID];
            break;
        case 4:
            requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageStart=1&pageSize=15&mentorId=%@&rotationStatus=canceled",SimpleIp,persion.userID];
            break;
            
        default:
            break;
    }

    NSLog(@"%@",requestUrl);
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result){
        [table headerEndRefreshing];
        [dataArray removeAllObjects];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (arr.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂无数据~" places:0 toView:nil];
            [table reloadData];
            return ;
        }
        for (int i=0; i<arr.count; i++) {
            NSDictionary *dic = arr[i];
            SDepartModel *model = [SDepartModel new];
            model.SdepartStudentId = dic[@"studentId"];
            model.SdepartStudentName = dic[@"studentFullName"];
            model.SDepartRecordId = dic[@"studentClinicalRotationRecordId"];
            model.SDepartName = dic[@"userGroupName"];
            model.SDepartStartTime = dic[@"checkInTime"];
            model.SDepartEndTime = dic[@"checkOutTime"];
            model.SDepartStatus = dic[@"rotationStatus"];
            
            model.SDepartPlace = dic[@""];
            //出科小结
            model.studentSummary = dic[@"studentSummary"];
            if ([dic[@"submitStudentSummary"] boolValue] == false) {
                model.isSubmitStudentSummary = @0;
            }else{
                model.isSubmitStudentSummary = @1;
            }
            //带教意见
            model.teacherSummary = dic[@"mentorFeedback"];
            model.teacherId = dic[@"mentorId"];
            model.teacherName = dic[@"mentorFullName"];
            if ([dic[@"submitMentorFeedback"] boolValue] == false) {
                model.isSubmitTeacherSummary = @0;
            }else{
                model.isSubmitTeacherSummary = @1;
            }
            //主管意见
            model.supervisorId = dic[@"supervisorId"];
            model.supervisorName = dic[@"supervisorFullName"];
            model.supervisorSummary = dic[@"supervisorFeedback"];
            if ([dic[@"submitSupervisorFeedback"] boolValue] == false) {
                model.isSubmitSupervisorSummary = @0;
            }else{
                model.isSubmitSupervisorSummary = @1;
            }

            
            NSMutableArray *recordArray = [NSMutableArray new];
            //
            //病例记录
            NSNumber *medicalNum = dic[@"studentMedicalCaseRecordsNum"];
            [recordArray addObject:medicalNum];
            //操作记录
            NSNumber *operateNum = dic[@"studentOperationRecordsNum"];
            [recordArray addObject:operateNum];
            //活动记录
            NSNumber *activityNum = dic[@"studentActivityRecordsNum"];
            [recordArray addObject:activityNum];
            //教学记录
            NSNumber *tutringNum = dic[@"studentTutoringRecordsNum"];
            [recordArray addObject:tutringNum];
            //科研记录
            NSNumber *researchNum = dic[@"studentResearchRecordsNum"];
            [recordArray addObject:researchNum];
            //获奖记录
            NSNumber *awardNum = dic[@"studentAwardRecordsNum"];
            [recordArray addObject:awardNum];
            //论文记录
            NSNumber *essayNum = dic[@"studentEssayPaperRecordsNum"];
            [recordArray addObject:essayNum];
            //抢救记录
            NSNumber *rescueNum = dic[@"studentRescueRecordsNum"];
            [recordArray addObject:rescueNum];
            //出诊记录
            NSNumber *treatNum = dic[@"studentTreatmentRecordsNum"];
            [recordArray addObject:treatNum];
            //差错记录
            NSNumber *malpracticeNum = dic[@"studentMalpracticeRecordsNum"];
            [recordArray addObject:malpracticeNum];
            //
            model.SDepartRecordArray = recordArray;
            
            
            
            
            
            
            [dataArray addObject:model];
        }
        [table headerEndRefreshing];
        [table reloadData];
    }failed:^(NSString *result) {
        [table headerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"获取失败~" places:0 toView:nil];
    }];
}
- (void)moreFresh{
    currentPage ++;
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    NSString *requestUrl;
    switch (IndexNum) {
        case 0:
            requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageStart=1&pageSize=15&mentorId=%@",SimpleIp,persion.userID];
            break;
        case 1:
            requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageStart=1&pageSize=15&mentorId=%@&rotationStatus=not_start",SimpleIp,persion.userID];
            break;
        case 2:
            requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageStart=1&pageSize=15&mentorId=%@&rotationStatusin_department",SimpleIp,persion.userID];
            break;
        case 3:
            requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageStart=1&pageSize=15&mentorId=%@&rotationStatus=off_department",SimpleIp,persion.userID];
            break;
        case 4:
            requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageStart=1&pageSize=15&mentorId=%@&rotationStatus=canceled",SimpleIp,persion.userID];
            break;
            
        default:
            break;
    }
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result){
        [table footerEndRefreshing];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (arr.count == 0) {
            currentPage --;
            [MBProgressHUD showToastAndMessage:@"暂无更多~" places:0 toView:nil];
            return ;
        }
        NSMutableArray *newArray = [NSMutableArray new];
        for (int i=0; i<arr.count; i++) {
            NSDictionary *dic = arr[i];
            SDepartModel *model = [SDepartModel new];
            model.SdepartStudentId = dic[@"studentId"];
            model.SdepartStudentName = dic[@"studentFullName"];
            model.SDepartRecordId = dic[@"studentClinicalRotationRecordId"];
            model.SDepartName = dic[@"userGroupName"];
            model.SDepartStartTime = dic[@"checkInTime"];
            model.SDepartEndTime = dic[@"checkOutTime"];
            model.SDepartStatus = dic[@"rotationStatus"];
            model.SDepartPlace = dic[@""];
            
            //出科小结
            model.studentSummary = dic[@"studentSummary"];
            if ([dic[@"submitStudentSummary"] boolValue] == false) {
                model.isSubmitStudentSummary = @0;
            }else{
                model.isSubmitStudentSummary = @1;
            }
            //带教意见
            model.teacherSummary = dic[@"mentorFeedback"];
            model.teacherId = dic[@"mentorId"];
            model.teacherName = dic[@"mentorFullName"];
            if ([dic[@"submitMentorFeedback"] boolValue] == false) {
                model.isSubmitTeacherSummary = @0;
            }else{
                model.isSubmitTeacherSummary = @1;
            }
            //主管意见
            model.supervisorId = dic[@"supervisorId"];
            model.supervisorName = dic[@"supervisorFullName"];
            model.supervisorSummary = dic[@"supervisorFeedback"];
            if ([dic[@"submitSupervisorFeedback"] boolValue] == false) {
                model.isSubmitSupervisorSummary = @0;
            }else{
                model.isSubmitSupervisorSummary = @1;
            }

            
            NSMutableArray *recordArray = [NSMutableArray new];
            //
            //病例记录
            NSNumber *medicalNum = dic[@"studentMedicalCaseRecordsNum"];
            [recordArray addObject:medicalNum];
            //操作记录
            NSNumber *operateNum = dic[@"studentOperationRecordsNum"];
            [recordArray addObject:operateNum];
            //活动记录
            NSNumber *activityNum = dic[@"studentActivityRecordsNum"];
            [recordArray addObject:activityNum];
            //教学记录
            NSNumber *tutringNum = dic[@"studentTutoringRecordsNum"];
            [recordArray addObject:tutringNum];
            //科研记录
            NSNumber *researchNum = dic[@"studentResearchRecordsNum"];
            [recordArray addObject:researchNum];
            //获奖记录
            NSNumber *awardNum = dic[@"studentAwardRecordsNum"];
            [recordArray addObject:awardNum];
            //论文记录
            NSNumber *essayNum = dic[@"studentEssayPaperRecordsNum"];
            [recordArray addObject:essayNum];
            //抢救记录
            NSNumber *rescueNum = dic[@"studentRescueRecordsNum"];
            [recordArray addObject:rescueNum];
            //出诊记录
            NSNumber *treatNum = dic[@"studentTreatmentRecordsNum"];
            [recordArray addObject:treatNum];
            //差错记录
            NSNumber *malpracticeNum = dic[@"studentMalpracticeRecordsNum"];
            [recordArray addObject:malpracticeNum];
            //赋值
            model.SDepartRecordArray = recordArray;
            [newArray addObject:model];
        }
        NSRange range = NSMakeRange(dataArray.count,newArray.count );
        NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
        [dataArray insertObjects:newArray atIndexes:set];
        table.contentOffset = CGPointMake(table.contentOffset.x, table.contentOffset.y+40);
        [table reloadData];
    }failed:^(NSString *result) {
        currentPage -- ;
        [table footerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"获取失败~" places:0 toView:nil];
    }];
    
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)selectAction:(UIButton *)button{
    CGPoint point = CGPointMake(button.center.x, button.center.y+16);
    CustomPopview *popView = [[CustomPopview alloc]initWithOrigin:point Width:130 Height:200 Type:XTTypeOfUpRight Color:[UIColor whiteColor]];
    popView.dataArray = @[@"全部",@"未入科",@"在科",@"出科",@"取消"];
    popView.fontSize = 18;
    popView.row_height = 40;
    popView.delegate = self;
    [popView popView];
    
    
}
#pragma -delegate
- (void)selectIndexPathRow:(NSInteger)index{
    NSArray *indexArray =  @[@"全部",@"未入科",@"在科",@"出科",@"取消"];
    [self searchAction:indexArray[index]];
 
}

- (void)searchAction:(NSString *)status{
    if ([status isEqualToString:@"未入科"]) {
        IndexNum = 1;
        NSLog(@"IndexNum==%d",IndexNum);
        [self setShuaXin];
    }else if ([status isEqualToString:@"在科"]) {
        IndexNum = 2;
        NSLog(@"IndexNum==%d",IndexNum);
         [self setShuaXin];
    }else if ([status isEqualToString:@"出科"]) {
        IndexNum = 3;
         NSLog(@"IndexNum==%d",IndexNum);
         [self setShuaXin];
    }else if ([status isEqualToString:@"全部"]) {
        IndexNum = 0;
         NSLog(@"IndexNum==%d",IndexNum);
         [self setShuaXin];
    }else if ([status isEqualToString:@"取消"]) {
        IndexNum = 4;
        NSLog(@"IndexNum==%d",IndexNum);
        [self setShuaXin];
    }
}


- (void)statusAction:(UIButton *)btn{
    NSInteger line = btn.tag - 200;
    SDepartModel *departModdel = dataArray[line];
    if ([btn.titleLabel.text isEqualToString:@"出科"]) {
        NSString *str=[NSString stringWithFormat:@"是否要为 %@ 进行出科",departModdel.SdepartStudentName];
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:str preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *req = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/completeStudentClinicalRotation",SimpleIp,departModdel.SDepartRecordId];
            [RequestTools RequestWithURL:req Method:@"put" Params:nil Message:@"" Success:^(NSDictionary *result) {
                NSLog(@"出科===%@",result);
                NSString *status = result[@"responseStatus"];
                if ([status isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"已出科~" places:0 toView:nil];
                    [self setShuaXin];
                }else{
                    if ([result[@"errorCode"] isEqualToString:@"invalid_student_clinical_rotation_record_status"]) {
                        [MBProgressHUD showToastAndMessage:@"出科失败,不合法的轮转状态~" places:0 toView:nil];
                    }
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"出科失败~" places:0 toView:nil];
            }];
        }];
        
        UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        
        [alert addAction:noAction];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
   
        return;
    }
    NSMutableArray *arrayId = [NSMutableArray new];
    NSMutableArray *arrayContent = [NSMutableArray new];
    
    
    if (departModdel.teacherId !=nil) {
        
        
        NSString *str=[NSString stringWithFormat:@"带教老师为%@",departModdel.teacherName];
        
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:str preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"继续入科" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
            NSString *req = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/startStudentClinicalRotation",SimpleIp,departModdel.SDepartRecordId];
            [RequestTools RequestWithURL:req Method:@"put" Params:nil Message:@"入科中~" Success:^(NSDictionary *result) {
                NSLog(@"入科==%@",result);
                
                NSString *status = result[@"responseStatus"];
                if ([status isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"已入科~" places:0 toView:nil];
                    [self setShuaXin];
                    [btn setTitle:@"出科" forState:0];
                    departModdel.SDepartStatus = @"在科";
                }else {
                    if ([result[@"errorCode"] isEqualToString:@"invalid_check_in_time_or_check_out_time"]){
                        [MBProgressHUD showToastAndMessage:@"请验证入科时间~" places:0 toView:nil];
                    }else if([result[@"errorCode"] isEqualToString:@"invalid_student_clinical_rotation_record_status"]){
                        [MBProgressHUD showToastAndMessage:@"无效的轮转记录状态" places:0 toView:nil];
                    }
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"入科失败~" places:0 toView:nil];
            }];
        }];
        
        UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alert addAction:noAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
        
        
        
    }else{
        //没有带教老师，添加带教
        NSString *requestTeacher = [NSString stringWithFormat:@"%@/webappv2/teacherAssignments/dropdown?teachingDepartmentId=%@&pageSize=999",SimpleIp,departModdel.SDepartRecordId];
        NSLog(@"%@",requestTeacher);
        [RequestTools RequestWithURL:requestTeacher Method:@"get" Params:nil Message:@"获取带教老师~" Success:^(NSDictionary *result) {
            
            
            NSLog(@"添加带教==%@",result);
            
            
            NSArray *array = result[@"responseBody"][@"result"];
            for (int s=0; s<array.count; s++) {
                NSDictionary *dic = array[s];
                [arrayId addObject:dic[@"userId"]];
                [arrayContent addObject:dic[@"fullName"]];
            }
            if (array.count == 0) {
                
                [MBProgressHUD showToastAndMessage:@"抱歉, 未找到匹配的带教老师，请联系管理员"places:0 toView:nil];
                return ;
            }
            self.pickView = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:arrayContent]];
            self.pickView.delegate = self;
            self.pickView.rightBtnTitle = @"确认";
            self.pickView.backgroundColor = [UIColor whiteColor];
            self.pickView.index = ^(int index){
                //分配带教老师
                NSString *fenpeiTeacher = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/massUpdate",SimpleIp];
                
                NSDictionary *params = @{@"checkInTime":departModdel.SDepartStartTime,@"checkOutTime":departModdel.SDepartEndTime,@"mentorId":arrayId[index],@"studentClinicalRotationRecordId":departModdel.SDepartRecordId};
                NSLog(@"fenpei=%@ params=%@",fenpeiTeacher,params);
                [RequestTools RequestWithURL:fenpeiTeacher Method:@"put" Params:@[params] Message:@"分配老师中~" Success:^(NSDictionary *result) {
                    if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                        //
                        //入科
                        NSString *req = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/startStudentClinicalRotation",SimpleIp,departModdel.SDepartRecordId];
                        [RequestTools RequestWithURL:req Method:@"put" Params:nil Message:@"入科中~" Success:^(NSDictionary *result) {
                            NSLog(@"入科==%@",result);
                            
                            NSString *status = result[@"responseStatus"];
                            if ([status isEqualToString:@"succeed"]) {
                                [MBProgressHUD showToastAndMessage:@"已入科~" places:0 toView:nil];
                                
                                [btn setTitle:@"出科" forState:0];
                                departModdel.SDepartStatus = @"在科";
                            }else {
                                if ([result[@"errorCode"] isEqualToString:@"invalid_check_in_time_or_check_out_time"]){
                                    [MBProgressHUD showToastAndMessage:@"请验证入科时间~" places:0 toView:nil];
                                }
                            }
                        } failed:^(NSString *result) {
                            [MBProgressHUD showToastAndMessage:@"入科失败~" places:0 toView:nil];
                        }];
                    }else{
                        [MBProgressHUD showToastAndMessage:@"带教老师分配失败~" places:0 toView:nil];
                    }
                } failed:^(NSString *result) {
                    [MBProgressHUD showToastAndMessage:@"带教老师分配失败~" places:0 toView:nil];
                }];
                
            };
            [self.view addSubview:self.pickView];
            [self.pickView show];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"获取失败~" places:0 toView:nil];
        }];
    }
    
    
    
//    if (array.count == 0) {
//        [MBProgressHUD showToastAndMessage:@"无带教老师~" places:0 toView:nil];
//        return;
//    }


    

    
}
//- (void)sureAction:(UIButton *)btn{
//    [[self.view viewWithTag:150] removeFromSuperview];
//}
//- (void)cancelAction:(UIButton *)btn{
//    [[self.view viewWithTag:150] removeFromSuperview];
//}
//- (void)PickerViewRightButtonOncleck:(NSInteger)index{
//    [self.pickView close];
//}

#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RoundRecordController *recordVC = [RoundRecordController shareObject];
    recordVC.departModel = dataArray[indexPath.row];
    [self.navigationController pushViewController:recordVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DepartCell *cell = [tableView dequeueReusableCellWithIdentifier:@"departCell"];
    [cell.statusBtn addTarget:self action:@selector(statusAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.statusBtn.tag = indexPath.row + 200;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    SDepartModel *model = dataArray[indexPath.row];
    cell.studentName.text = model.SdepartStudentName;
    cell.departStatus.text = model.SDepartStatus;
    cell.depart.text = model.SDepartName;
    if ([model.SDepartStatus isEqualToString:@"在科"]) {
        cell.statusBtn.hidden = NO;
        [cell.statusBtn setTitle:@"出科" forState:0];
    }else if ([model.SDepartStatus isEqualToString:@"未入科"]) {
        cell.statusBtn.hidden = NO;
        [cell.statusBtn setTitle:@"入科" forState:0];
    }else{
        cell.statusBtn.hidden = YES;
    }
  
    cell.inTime.text = model.SDepartStartTime.intValue == 0 ? @"入科时间:暂无" : [NSString stringWithFormat:@"入科时间:%@",[[Maneger shareObject] timeFormatter1:model.SDepartStartTime.stringValue]];

    cell.outTime.text = model.SDepartEndTime.intValue == 0 ? @"出科时间:暂无" : [NSString stringWithFormat:@"出科时间:%@",[[Maneger shareObject] timeFormatter1:model.SDepartEndTime.stringValue]];
    
    return cell;
}
@end

