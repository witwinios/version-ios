//
//  ErrorController.h
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ErrorCell.h"
#import "ErrorModel.h"
#import "SelfPaperController.h"
#import "RequestTools.h"
#import "LoadingView.h"
#import "AddErrorController.h"
#import "SelfListCell.h"
@interface ErrorController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@end
