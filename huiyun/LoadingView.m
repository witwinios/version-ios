//
//  LoadingView.m
//  UIActivityIndicatorViewDemo
//
//  Created by qianfeng on 15/8/19.
//  Copyright (c) 2015年 LiuYaNan. All rights reserved.
//

#import "LoadingView.h"

#define ACTIVITYVIEW_TAG 10

#define TITLELABEL_TAG 11

@implementation LoadingView

- (instancetype)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self) {
    
        self.backgroundColor = [UIColor darkGrayColor];
        // 设置圆角
        self.layer.cornerRadius = 5;
        
        self.hidden = YES;
        
        UIActivityIndicatorView * activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height - 20);
        activityView.hidesWhenStopped = YES;
        activityView.tag = ACTIVITYVIEW_TAG;
        [self addSubview:activityView];
        
        UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height - 20, frame.size.width, 20)];
        titleLabel.font = [UIFont boldSystemFontOfSize:14];
        titleLabel.adjustsFontSizeToFitWidth = YES;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.tag = TITLELABEL_TAG;
        [self addSubview:titleLabel];
        
        self.loadTitle = @"";
    }
    return self;
}

- (void)showLoadingView
{
    self.hidden = NO;
    UIActivityIndicatorView * activityView = (id)[self viewWithTag:ACTIVITYVIEW_TAG];
    [activityView startAnimating];
    
    UILabel * titleLabel = (id)[self viewWithTag:TITLELABEL_TAG];
    titleLabel.text = self.loadTitle;
}

- (void)hiddenLoadingView
{
    self.hidden = YES;
    UIActivityIndicatorView * activityView = (id)[self viewWithTag:ACTIVITYVIEW_TAG];
    [activityView stopAnimating];
}
@end
