//
//  MarkTableController.m
//  yun
//
//  Created by MacAir on 2017/7/21.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "MarkTableController.h"

@interface MarkTableController ()
{
    UITableView *tabView;
    NSMutableArray *dataArray;
    NSMutableArray *submitArray;
    NSNumber *isHasSign;
    //记录每项的扣分数
    
    ExamPointController *pointVC;
    
    UIImage *signImage;
    
    CGRect _activedTextFieldRect;
    UIView *toolbar;
    CGPoint currentPoint;
    NSString *ThisStr;
}
@end

@implementation MarkTableController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    submitArray = [NSMutableArray new];
    isHasSign = [NSNumber new];
    pointVC = [[ControlManeger share] PointVC];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyBoardHide:) name: UIKeyboardWillHideNotification object:nil];
    ThisStr=@"未签名";
    [self setUI];
}
- (void)getSignName{
    [MBProgressHUD showHUDAndMessage:@"获取评分表" toView:nil];
    NSString *Url = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/getSignName/%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,LocalUserId];
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        if ([result[@"responseBody"] isKindOfClass:[NSNull class]]) {
            //
            isHasSign = [NSNumber numberWithInt:0];
            
            UIButton *signBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            signBtn.frame = CGRectMake(0, Sheight-30, Swidth/2, 30);
            [signBtn setTitle:@"签名" forState:0];
            [signBtn setTitleColor:[UIColor whiteColor] forState:0];
            signBtn.backgroundColor = [UIColor lightGrayColor];
            [self.view addSubview:signBtn];
            [signBtn addTarget:self action:@selector(signAction:) forControlEvents:UIControlEventTouchUpInside];
            UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            submitBtn.frame = CGRectMake(Swidth/2, Sheight-30, Swidth/2, 30);
            [submitBtn setTitleColor:[UIColor whiteColor] forState:0];
            [submitBtn setTitle:@"提交" forState:0];
            [submitBtn addTarget:self action:@selector(submitTable:) forControlEvents:UIControlEventTouchUpInside];
            submitBtn.backgroundColor = UIColorFromHex(0x20B2AA);
            [self.view addSubview:submitBtn];
            [tabView reloadData];
            return ;
        }
        //nsdata转image
        UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        submitBtn.frame = CGRectMake(0, Sheight-30, Swidth, 30);
        [submitBtn setTitleColor:[UIColor whiteColor] forState:0];
        [submitBtn setTitle:@"提交" forState:0];
        submitBtn.backgroundColor = UIColorFromHex(0x20B2AA);
        [submitBtn addTarget:self action:@selector(submitTable:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:submitBtn];
        
        isHasSign = [NSNumber numberWithInt:1];
        
        NSString *imageStr = result[@"responseBody"];
        NSData *decodedImageData = [[NSData alloc]initWithBase64EncodedString:[imageStr substringFromIndex:22] options:NSDataBase64DecodingIgnoreUnknownCharacters];
        signImage = [UIImage imageWithData:decodedImageData];
        
        [tabView reloadData];
    }];
}
- (void)setUI{
    //背景色
    self.view.backgroundColor = [UIColor whiteColor];
    //导航栏
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.text = @"评分表";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    [navigationView addSubview:titleLab];
    
    UIButton *contentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    contentBtn.frame = CGRectMake(Swidth-100, 29.5, 95, 25);
    contentBtn.layer.cornerRadius = 5;
    contentBtn.layer.borderWidth = 1;
    contentBtn.layer.masksToBounds = YES;
    contentBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [contentBtn addTarget:self action:@selector(contentAction:) forControlEvents:UIControlEventTouchUpInside];
    [contentBtn setTitle:@"考试内容" forState:0];
    contentBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [navigationView addSubview:contentBtn];
    
    [self.view addSubview:navigationView];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64-30) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerClass:[MarkCell class] forCellReuseIdentifier:@"markTableCell"];
    tabView.bounces = NO;
    tabView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self.view addSubview:tabView];
    
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];

}
- (void)keyBoardHide:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
        tabView.contentOffset = currentPoint;
    }];
}
- (void)hideKb:(UIButton *)btn{
    [[UIApplication sharedApplication].keyWindow endEditing:YES];
}
- (void)doneButtonshow:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
    currentPoint = tabView.contentOffset;
    if ((_activedTextFieldRect.origin.y + _activedTextFieldRect.size.height) >  ([UIScreen mainScreen].bounds.size.height - rect.size.height - 40)) {//键盘的高度 高于textView的高度 需要滚动
        [UIView animateWithDuration:duration animations:^{
        tabView.contentOffset = CGPointMake(0, tabView.contentOffset.y + _activedTextFieldRect.origin.y+_activedTextFieldRect.size.height - Sheight +rect.size.height+40);
        }];
    }}
- (void)loadData{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/operationRecords?pageStart=1&pageSize=999&teacherId=%@&studentId=%@&osceStationId=%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,LocalUserId,self.callModel.callModelUserId,self.pointModel.ExamPointModelStationId];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"获取评分表" Success:^(NSDictionary *result) {
        NSArray *array = result[@"responseBody"][@"result"];
        [dataArray removeAllObjects];
        NSLog(@"markArrayCount=%ld",(unsigned long)array.count);
        for (NSDictionary *dic in array) {
            MarkTableModel *markModel = [MarkTableModel new];
            markModel.MarkTableRecordId = dic[@"recordId"];
            markModel.MarkTableOsceStationId = dic[@"osceStationId"];
            markModel.MarkTableKeyType = dic[@"operationKey"][@"keyType"];
            markModel.MarkTableKey = dic[@"operationKey"][@"key"];
            markModel.MarkTableAnswer = dic[@"operationKey"][@"answers"];
            markModel.MarkTableTotalScore = dic[@"scoreValue"];
            markModel.MarkTableTeacherId = dic[@"teacher"][@"userId"];
            markModel.MarkTableTeacherName = dic[@"teacher"][@"fullName"];
            markModel.MarkTableDescription = dic[@"operationKey"][@"description"];
            markModel.scoreReason = @"";
            markModel.getScore = @0;
            markModel.dockScore = @0;
            [dataArray addObject:markModel];
        }
        dataArray = (NSMutableArray *)[[dataArray reverseObjectEnumerator] allObjects];
        for (int i=0; i<dataArray.count; i++) {
            MarkTableModel *tableModel = dataArray[i];
            NSLog(@"osceStationId=%@",tableModel.MarkTableOsceStationId);
            NSMutableDictionary *muDic = [NSMutableDictionary new];
            [muDic setValue:tableModel.MarkTableRecordId forKey:@"osceStationId"];
            [submitArray addObject:muDic];
        }
        [self getSignName];
    } failed:^(NSString *result) {
        
    }];
}
#pragma -action
- (void)back{
   // [self.navigationController popViewControllerAnimated:YES];
    
    NSArray *vcArray = self.navigationController.viewControllers;
    
    
    for(UIViewController *vc in vcArray)
    {
        if ([vc isKindOfClass:[ListStudentController class]])
        {
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
    
    
}
- (void)signAction:(UIButton *)btn{
    DrawController *drawVC = [DrawController new];
    drawVC.strblock=^(NSString *str){
        ThisStr=str;
    };

    [self.navigationController pushViewController:drawVC animated:YES];
}
- (void)contentAction:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)submitTable:(UIButton *)btn{
    
    if ([ThisStr isEqualToString:@"未签名"]) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"您尚未签名，是否需要签名?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
             [self submitAction];
        }];
        UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            DrawController *drawVC = [DrawController new];
            drawVC.strblock=^(NSString *str){
                ThisStr=str;
            };
            
            [self.navigationController pushViewController:drawVC animated:YES];
        }];
        [alertVC addAction:cancelAction];
        [alertVC addAction:sureAction];
        [self presentViewController:alertVC animated:YES completion:nil];
    }else if([ThisStr isEqualToString:@"已签名"]){
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"提交后将无法修改,确定提交?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self submitAction];
        }];
        [alertVC addAction:cancelAction];
        [alertVC addAction:sureAction];
        [self presentViewController:alertVC animated:YES completion:nil];
    }
}
//
- (void)submitAction{
    [MBProgressHUD showToastAndMessage:@"提交评分中。。。" places:0 toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/submitOperationGrades/%@/osceStation/%@/student/%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,LocalUserId,self.pointModel.ExamPointModelStationId,self.callModel.callModelUserId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:submitArray Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            //获取所有考点
            int isMark = 0;
            for (int s=0; s<pointVC.dataArray.count; s++) {
                ExamPointModel *pointModel = pointVC.dataArray[s];
                if (pointModel.ExamPointModelStationId.integerValue == self.pointModel.ExamPointModelStationId.integerValue) {
                    pointModel.ExamPointModelStatus = @"已评分";
                }
                if ([pointModel.ExamPointModelStatus isEqualToString:@"未评"]) {
                    isMark = 1;
                }
            }
            if (isMark == 1) {
                [MBProgressHUD showToastAndMessage:@"评分成功!" places:0 toView:nil];
                //还有没评分考点
                [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    [self.navigationController popToViewController:pointVC animated:YES];
                }];
            }else{
                [self isMarkEnd];
            }
        }else{
            [MBProgressHUD hideHUDForView:nil];
            [MBProgressHUD showToastAndMessage:@"打分失败~" places:0 toView:nil];
        }
        [tabView reloadData];
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"打分失败~" places:0 toView:nil];
    }];
}
- (void)isMarkEnd{
    [MBProgressHUD showHUDAndMessage:@"检查中。。。" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD hideHUDForView:nil];
        if ([result[@"responseBody"][@"currentStudent"] isKindOfClass:[NSNull class]]) {
            //评分成功
            [MBProgressHUD showToastAndMessage:@"评分成功!" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer * _Nonnull timer) {
                //返回学生界面
                ControlManeger *VCManeger = [ControlManeger share];
                [self.navigationController popToViewController:VCManeger.ListSVC animated:YES];
            }];
            
        }else{
            NSNumber *userId = result[@"responseBody"][@"currentStudent"][@"userId"];
            if (self.callModel.callModelUserId.intValue != userId.intValue) {
                [MBProgressHUD showToastAndMessage:@"评分成功!" places:0 toView:nil];
                [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    //返回学生界面
                    ControlManeger *VCManeger = [ControlManeger share];
                    [self.navigationController popToViewController:VCManeger.ListSVC animated:YES];
                }];
            }else{
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"您已完成评分,请等待其他老师完成评分!" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    [self isMarkEnd];
                }];
                [alertVC addAction:sureAction];
                [self presentViewController:alertVC animated:YES completion:nil];
            }
        }
        
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"检查失败~" places:0 toView:nil];
    }];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (isHasSign.integerValue == 1) {
        return 50.f;
    }
    return 0.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (isHasSign.integerValue == 1) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 50)];
        view.backgroundColor = [UIColor lightGrayColor];
        UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-100, 0, 100, 50)];
        lab.text = @"签名:";
        lab.textAlignment = 1;
        lab.font = [UIFont systemFontOfSize:18];
        [view addSubview:lab];
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2, 5, 150, 40)];
        imageView.image = signImage;
        imageView.backgroundColor = [UIColor whiteColor];
        [view addSubview:imageView];
        return view;
    }
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 85)];
    headView.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *stuLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Swidth/2, 25)];
    stuLab.text = [NSString stringWithFormat:@"考生: %@",self.callModel.callModelUserName];
    stuLab.font = [UIFont systemFontOfSize:15];
    stuLab.textColor = [UIColor whiteColor];
    [headView addSubview:stuLab];
    //时间
    UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2, 0, Swidth/2-5, 25)];
    
    timeLab.text = [NSString stringWithFormat:@"日期: %@",[[Maneger shareObject] currentTimeF]];
    timeLab.font = [UIFont systemFontOfSize:15];
    timeLab.textAlignment = 2;
    timeLab.textColor = [UIColor whiteColor];
    [headView addSubview:timeLab];
    //考官
    UILabel *teacherLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, Swidth, 25)];
    teacherLab.text = [NSString stringWithFormat:@"考官: %@",self.callModel.callModelTeacher];
    teacherLab.textColor = [UIColor whiteColor];
    teacherLab.font = [UIFont systemFontOfSize:15];
    [headView addSubview:teacherLab];
    //
    return headView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MarkCell *cell = (MarkCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.cellHeight.floatValue;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MarkCell *markCell = [tableView dequeueReusableCellWithIdentifier:@"markTableCell"];
    markCell.selectionStyle = UITableViewCellSeparatorStyleNone;
    MarkTableModel *markModel = dataArray[indexPath.row];
    
    [markCell setModel:markModel andIndexPath:indexPath];
    markCell.model = markModel;
    
    markCell.selectView = ^(UIView *select) {
        _activedTextFieldRect = [self.view convertRect:select.frame fromView:select.superview.superview];
        NSLog(@"%f,%f",_activedTextFieldRect.origin.y,_activedTextFieldRect.size.height);
    };
    markCell.fieldAction = ^(NSNumber *row, NSString *str) {
        NSMutableDictionary *muDic = submitArray[[row integerValue]];
        [muDic setValue:str forKey:@"score"];
    };
    markCell.textAction = ^(NSNumber *row, NSString *str) {
        NSMutableDictionary *muDic = submitArray[[row integerValue]];
        [muDic setValue:str forKey:@"comments"];
    };
    return markCell;
}
#pragma -扣分delegate

@end

