//
//  TeachingAidsModel.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeachingAidsModel : NSObject
/*
 AidsName 名称
 Repertory 可用库存
 Subscribe 已预约数
 Purpose 用途
 Inventory 库存量
 
 ModelId 教具类ID
 ModelName 教具类名称
 ModelCategoryName 教具类分类
 ModelNo 教具类类别
 ModelItemsNum 教具类数量
 ModelAmount 教具类数目
 ModelComments 教具类备注
 ModeluseFor 教具类用途
*/
@property(nonatomic,strong) NSString *AidsName;
@property(nonatomic,strong) NSNumber *Repertory;
@property(nonatomic,strong) NSNumber *Subscribe;
@property(nonatomic,strong) NSString *Purpose;
@property(nonatomic,strong) NSNumber *Inventory;

@property(nonatomic,strong) NSNumber *ModelId;
@property(nonatomic,strong) NSString *ModelName;
@property(nonatomic,strong) NSString *ModelCategoryName;
@property(nonatomic,strong) NSString *ModelNo;
@property(nonatomic,strong) NSNumber *ModelItemsNum;
@property(nonatomic,strong) NSNumber *ModelAmount;
@property(nonatomic,strong) NSString *ModelComments;
@property(nonatomic,strong) NSString *ModeluseFor;

@property (nonatomic, assign) BOOL isCheck;

@end
