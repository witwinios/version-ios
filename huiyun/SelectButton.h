//
//  SelectButton.h
//  huiyun
//
//  Created by MacAir on 2017/12/3.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectButton : UIButton
@property (strong, nonatomic) QuestionModel *model;
@property (strong, nonatomic) NSNumber *currentOption;
@property (strong, nonatomic) NSNumber *quesIndex;
@property (strong, nonatomic) NSNumber *currentIndex;
@end

