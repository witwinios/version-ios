//
//  OrderAidsViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderAidsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>


@property(nonatomic,strong)NSString *AidsName;
@property(nonatomic,strong)NSNumber *Repertory;
@property(nonatomic,strong)NSNumber *Inventory;
@property(nonatomic,strong)NSNumber *Subscribe;
@property(nonatomic,strong)NSString *scheduleIdString;
@property(nonatomic,strong)NSNumber *ModelId;


@end
