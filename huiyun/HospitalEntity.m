//
//  HospitalEntity.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/27.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "HospitalEntity.h"
@implementation HospitalEntity
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.hosIP forKey:@"hosIP"];
    [aCoder encodeObject:self.hosName forKey:@"hosName"];
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    if (self == [super init]) {
        self.hosIP = [coder decodeObjectForKey:@"hosIP"];
        self.hosName = [coder decodeObjectForKey:@"hosName"];
    }
    return self;
}
@end
