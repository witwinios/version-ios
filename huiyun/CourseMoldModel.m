//
//  CourseMoldModel.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/27.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "CourseMoldModel.h"

@implementation CourseMoldModel

/*

 @property(strong,nonatomic)NSString *MoldName;
 @property(strong,nonatomic)NSNumber *MoldId;
 
 @property(strong,nonatomic)NSMutableArray *ChildMoldArray;
 @property(strong,nonatomic)NSString *ChildMoldName;
 @property(strong,nonatomic)NSNumber *ChildMoldId;
*/

-(void)setMoldId:(NSNumber *)MoldId{
    if ([MoldId isKindOfClass:[NSNull class]]) {
        _MoldId = [NSNumber numberWithInt:0];
    }else{
        _MoldId = MoldId;
    }
}

-(void)setMoldName:(NSString *)MoldName{
    if ([MoldName isKindOfClass:[NSNull class]]) {
        _MoldName = @"暂无";
    }else{
        _MoldName = MoldName;
    }
}


-(void)setChildMoldId:(NSNumber *)ChildMoldId{
    if ([ChildMoldId isKindOfClass:[NSNull class]]) {
        _ChildMoldId = [NSNumber numberWithInt:0];
    }else{
        _ChildMoldId = ChildMoldId;
    }
}

-(void)setChildMoldName:(NSString *)ChildMoldName{
    if ([ChildMoldName isKindOfClass:[NSNull class]]) {
        _ChildMoldName = @"暂无";
    }else{
        _ChildMoldName = ChildMoldName;
    }
}

-(void)setChildrenNum:(NSNumber *)ChildrenNum{
    if ([ChildrenNum isKindOfClass:[NSNull class]]) {
        _ChildrenNum = [NSNumber numberWithInt:0];
    }else{
        _ChildrenNum = ChildrenNum;
    }
}

-(void)setParentId:(NSNumber *)ParentId{
    if ([ParentId isKindOfClass:[NSNull class]]) {
        _ParentId = [NSNumber numberWithInt:0];
    }else{
        _ParentId = ParentId;
    }
}

@end
