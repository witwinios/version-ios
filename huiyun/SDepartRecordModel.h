//
//  SDepartRecordModel.h
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDepartRecordModel : NSObject
@property (strong, nonatomic) NSString *SDepartRecordName;
@property (strong, nonatomic) NSNumber *SDepartRecordPassNum;
@property (strong, nonatomic) NSNumber *SDepartRecordReferNum;
@property (strong, nonatomic) NSNumber *SDepartRecordPendNum;
@end
