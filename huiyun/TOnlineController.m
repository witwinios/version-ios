//
//  OnlineDetailController.m
//  xiaoyun
//
//  Created by MacAir on 17/1/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TOnlineController.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
@interface TOnlineController ()
{
    UITableView *onlineTable;
}
@end

@implementation TOnlineController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self createUI];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"在线详情";
    [backNavigation addSubview:titleLab];
}
- (void)createUI{
    //
    onlineTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT-64) style:UITableViewStyleGrouped];
    onlineTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    onlineTable.delegate = self;
    onlineTable.dataSource = self;
    onlineTable.backgroundColor = UIColorFromHex(0xf5f5f5);
    
    [self.view addSubview:onlineTable];
    
  
    //注册cell
    [onlineTable registerNib:[UINib nibWithNibName:@"osceDetailCellStyle1" bundle:nil] forCellReuseIdentifier:@"style1"];
    [onlineTable registerNib:[UINib nibWithNibName:@"osceDatailCellStyle2" bundle:nil] forCellReuseIdentifier:@"style2"];
}
#pragma -protocol
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 20)];
    view.backgroundColor = UIColorFromHex(0xf5f5f5);
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 8;
    }else{
        return 2;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            if (self.model.onlinePages.intValue == 0) {
                [MBProgressHUD showToastAndMessage:@"暂无试卷~" places:0 toView:nil];
                return;
            }
            OnlinePagesController *pageVC = [OnlinePagesController new];
            pageVC.model = self.model;
            [self.navigationController pushViewController:pageVC animated:YES];

        }else {
            if (self.model.selectNums.intValue == 0) {
                [MBProgressHUD showToastAndMessage:@"暂无人数~" places:0 toView:nil];
                return;
            }
            OnlineStudentController *stuVC = [OnlineStudentController new];
            stuVC.model = self.model;
            [self.navigationController pushViewController:stuVC animated:YES];
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    osceDetailCellStyle1 *cell = [tableView dequeueReusableCellWithIdentifier:@"style1"];
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                cell.biaoti.text = @"安排状态:";
                cell.content.text = [self.model onlineStatus];
                cell.imgShow.hidden=YES;
                break;
            case 1:
                cell.biaoti.text = @"考试名称:";
                cell.content.text = [self.model onlineName];
                cell.imgShow.hidden=YES;
                break;
            case 2:
                cell.biaoti.text = @"考试时间:";
                cell.content.text = [[Maneger shareObject] timeFormatter:[self.model onlineTime].stringValue];
                cell.imgShow.hidden=YES;
                break;
            case 3:
                cell.biaoti.text = @"窗口时间:";
                cell.content.text = [NSString stringWithFormat:@"%@分钟",self.model.onlineClearance];
                cell.imgShow.hidden=YES;
                break;
            case 4:
                cell.biaoti.text = @"考试时长:";
                cell.content.text = [NSString stringWithFormat:@"%@分钟",[self.model onlineInternal].stringValue];
                cell.imgShow.hidden=YES;

                break;
            case 5:
                cell.biaoti.text = @"考试地点";
                cell.content.text = [self.model onlineRoom];
                cell.imgShow.hidden=YES;
                break;
            case 6:
                cell.biaoti.text = @"监考老师:";
                cell.content.text = [self.model teacher];
                cell.imgShow.hidden=YES;
                break;
            case 7:
                cell.biaoti.text = @"考试说明:";
                cell.content.text = [self.model onlineDes];
                cell.imgShow.hidden=YES;
                break;
            default:
                break;
        }
    }else{
        switch (indexPath.row) {
            case 0:
                cell.biaoti.text = @"试卷数:";
                cell.content.text = [self.model onlinePages].stringValue;
                cell.imgShow.alpha = 1;

                break;
            case 1:
                cell.biaoti.text = @"参考人数:";
                cell.imgShow.alpha = 1;
                cell.content.text = [self.model selectNums].stringValue;
                break;
            default:
                break;
        }
    }
    
    
    CGRect frame=cell.frame;
    frame.size.height=cell.content.frame.size.height+44;
    cell.frame=frame;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    osceDetailCellStyle1 *cell =(osceDetailCellStyle1 *)[self tableView:tableView  cellForRowAtIndexPath:indexPath];
    
    return cell.frame.size.height;
}

- (void)back:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
