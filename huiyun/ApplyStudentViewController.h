//
//  ApplyStudentViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/5.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseModel.h"
@interface ApplyStudentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>


@property(nonatomic,strong) NSString *scheduleIdString;

@property(nonatomic,strong) NSString *scheduleStatusString;

@property(nonatomic,getter=isEditing) BOOL editing;

@property (strong, nonatomic) TCourseModel *model;

-(void)setEditing:(BOOL)editing animated:(BOOL)animated;

@end
