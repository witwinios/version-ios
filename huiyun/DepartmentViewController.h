//
//  DepartmentViewController.h
//  huiyun
//
//  Created by Bad on 2018/3/14.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDepartmentCell.h"
#import "DepartmentModel.h"
@interface DepartmentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIPickerViewDataSource,UIPickerViewDelegate>


@property(nonatomic,getter=isEditing) BOOL editing;

-(void)setEditing:(BOOL)editing animated:(BOOL)animated;
@property(nonatomic,strong)NSString *UserId;
@property(nonatomic,strong)NSMutableArray *TempArray;
@property(nonatomic,strong)NSString *str;
@end

