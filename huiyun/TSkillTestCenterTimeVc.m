//
//  TSkillTestCenterTimeVc.m
//  huiyun
//
//  Created by Bad on 2018/3/27.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TSkillTestCenterTimeVc.h"

@interface TSkillTestCenterTimeVc ()<UITableViewDelegate,UITableViewDataSource>
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *TableVc;
    NSMutableArray *dataArray;
    
}

@end

@implementation TSkillTestCenterTimeVc

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setUI];
    [self loadData];
   
}



- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"时间段";
    
    
    [backNavigation addSubview:titleLab];
    
    
    //    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    //    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    //    [rightBtn setTitle:@"批量选取" forState:UIControlStateNormal];
    //    [rightBtn addTarget:self action:@selector(Choice:) forControlEvents:UIControlEventTouchUpInside];
    //    [backNavigation addSubview:rightBtn];
    //
    //    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    //    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    //    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    //    [SaveBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    //    [SaveBtn addTarget:self action:@selector(setCourseWareChoice) forControlEvents:UIControlEventTouchUpInside];
    //    [backNavigation addSubview:SaveBtn];
    //    SaveBtn.hidden=YES;
    
    
}
- (void)back :(UIButton *)button{
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)loadData{
    dataArray=[NSMutableArray new];
    NSString *testID=[NSString stringWithFormat:@"%@",self.model.skillID];
    NSString *Url=[NSString stringWithFormat:@"%@/skillTests/%@/timeBlocks",LocalIP,testID];
    NSLog(@"%@",Url);
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
          NSLog(@"%@",result);

        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];

            return;
        }
        [dataArray removeAllObjects];


        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [result objectForKey:@"responseBody"];
            if (array.count ==0) {
                [MBProgressHUD showToastAndMessage:@"无时间段信息" places:0 toView:nil];
                
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dic= [array objectAtIndex:i];
                    TSkillTestCenterModel *model=[TSkillTestCenterModel new];

                    model.TSkillTimeFrom=[dic objectForKey:@"startTime"];
                    model.TSKillTimeEnd=[dic objectForKey:@"endTime"];


                    [dataArray addObject:model];

                }
                [TableVc reloadData];

            }
        }else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];

        }


    } failed:^(NSString *result) {
        NSLog(@"%@",result);
    }];
    
    
}

-(void)setUI{
    
    TableVc = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    TableVc.delegate = self;
    TableVc.dataSource = self;
    TableVc.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    TableVc.showsVerticalScrollIndicator=NO;
    TableVc.estimatedRowHeight = 0;
    [TableVc registerNib:[UINib nibWithNibName:@"TSkillTimeCell" bundle:nil] forCellReuseIdentifier:@"TSkillTimeCell"];
    [self.view addSubview:TableVc];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 92;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TSkillTimeCell *cell=(TSkillTimeCell *)[tableView dequeueReusableCellWithIdentifier:@"TSkillTimeCell"];
    TSkillTestCenterModel *model=dataArray[indexPath.row];

    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell setAddModel:model];
    
    return cell;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
