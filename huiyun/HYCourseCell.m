//
//  HYCourseCell.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "HYCourseCell.h"

@implementation HYCourseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor=self.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.3];
    
    self.courseBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.courseBtn.layer.borderWidth=1;
    self.courseBtn.layer.cornerRadius=10;
    self.courseName.font=[UIFont systemFontOfSize:15];
    [self.courseName sizeToFit];
    self.courseName.numberOfLines = 0;
    
}
- (void)setProperty:(HYCourseModel *)model{
    self.courseName.text = model.courseName;
    self.coursePlace.text = model.coursePlace;
    self.courseStatus.text = model.courseStatus;
    self.courseTime.text = [[Maneger shareObject] timeFormatter:model.courseTime.stringValue];
    self.courseType.text = model.courseType;
}
@end
