//
//  TeachingAidsViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TeachingAidsViewController.h"
#import "TeachingAidsCell.h"
#import "TeachingAidsModel.h"
#import "OrderAidsViewController.h"
@interface TeachingAidsViewController ()
{
    UITableView *courseTable;
    
    NSMutableArray *dataArray;
}

@end

@implementation TeachingAidsViewController


-(void)viewWillAppear:(BOOL)animated{
     [self setUpRefresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self setNav];
    
    [self setUI];
    
    [self setUpRefresh];
    
    
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"所需教具";
    [backNavigation addSubview:titleLab];
}
- (void)setUpRefresh{
    
    
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    
    
}
#pragma mark 确定：




#pragma mark 回退
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)loadData{
    NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules/%@/TrainingAidModels/idle",LocalIP,_scheduleIdString];
    NSLog(@"%@",URL);
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array = [result objectForKey:@"responseBody"] ;
            if (array.count ==0) {
                [Maneger showAlert:@"无教具!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    TeachingAidsModel *model=[TeachingAidsModel new];
         
                    if (![[dictionary objectForKey:@"modelBean"]isKindOfClass:[NSNull class]]) {
                         model.AidsName=[[dictionary objectForKey:@"modelBean"]objectForKey:@"modelName"];
                         model.ModelId=[[dictionary objectForKey:@"modelBean"]objectForKey:@"modelId"];
                    }
                    model.Repertory=[dictionary objectForKey:@"idleItemsNum"];
                    model.Subscribe=[dictionary objectForKey:@"trainingAidItemsNum"];
                    model.Purpose=[dictionary objectForKey:@"useFor"];
                    model.Inventory=[dictionary objectForKey:@"amount"];
                    
                    [dataArray addObject:model];
                    
                    NSLog(@"%@",model.ModelId);
                }
                
                
             
                
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
            }
        }else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        

    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            [courseTable headerEndRefreshing];
        }
        return;
    }];
    
    
}




-(void)setUI{
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [courseTable registerNib:[UINib nibWithNibName:@"TeachingAidsCell" bundle:nil] forCellReuseIdentifier:@"AidsCell"];
    [self.view addSubview:courseTable];
    dataArray=[NSMutableArray array];
    
    
}







//返回行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

//返回行高：
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
}

//点击行事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TeachingAidsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"AidsCell"];
     cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    if(cell == nil){
        cell=[[TeachingAidsCell alloc]initWithStyle:UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert reuseIdentifier:@"applyCell" ];
    }
    
    TeachingAidsModel *model=dataArray[indexPath.row];
    [cell setProperty:model];
    
    
    if([cell.Repertory.text isEqualToString:@"0"] ){
        cell.StatusBtn.backgroundColor=[UIColor lightGrayColor];
        [cell.StatusBtn setTitle:@"预约教具" forState:UIControlStateNormal];
        
    }else{
        cell.StatusBtn.backgroundColor= UIColorFromHex(0x20B2AA);
        [cell.StatusBtn setTitle:@"预约教具" forState:UIControlStateNormal];
        [cell.StatusBtn addTarget:self action:@selector(StatusBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.StatusBtn.tag=999+indexPath.row;
    
    
    return cell;
    
    
}

-(void)StatusBtnAction:(UIButton *)sender{
    
    NSInteger line=sender.tag-999;
    NSLog(@"line:%ld",line);
    TeachingAidsModel *model = dataArray[line];
    
    OrderAidsViewController *vc=[[OrderAidsViewController alloc]init];
    vc.AidsName=model.AidsName;
    vc.Repertory=model.Repertory;
    vc.Inventory=model.Inventory;
    vc.Subscribe=model.Subscribe;
    vc.scheduleIdString=_scheduleIdString;
    vc.ModelId=model.ModelId;
    [self.navigationController pushViewController:vc animated:YES];
    
    
}


@end
