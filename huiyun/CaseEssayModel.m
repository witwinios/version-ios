//
//  CaseEssayModel.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseEssayModel.h"

@implementation CaseEssayModel
//@property (strong, nonatomic) NSString *caseEssayModelStatus;
- (void)setCaseEssayModelStatus:(NSString *)caseEssayModelStatus{
    if ([caseEssayModelStatus isKindOfClass:[NSNull class]]) {
        _caseEssayModelStatus = @"";
    }else if([caseEssayModelStatus isEqualToString:@"waiting_approval"]){
        _caseEssayModelStatus = @"待审核";
    }else if ([caseEssayModelStatus isEqualToString:@"approved"]){
        _caseEssayModelStatus = @"已通过";
    }else{
        _caseEssayModelStatus = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelTitle;
- (void)setCaseEssayModelTitle:(NSString *)caseEssayModelTitle{
    if ([caseEssayModelTitle isKindOfClass:[NSNull class]]) {
        _caseEssayModelTitle = @"暂无";
    }else{
        _caseEssayModelTitle = caseEssayModelTitle;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelDegree;
- (void)setCaseEssayModelDegree:(NSString *)caseEssayModelDegree{
    if ([caseEssayModelDegree isKindOfClass:[NSNull class]]) {
        _caseEssayModelDegree = @"暂无";
    }else if ([caseEssayModelDegree isEqualToString:@"CORE_JOURNAL"]){
        _caseEssayModelDegree = @"核心期刊";
    }else if ([caseEssayModelDegree isEqualToString:@"NON_CORE_JOURNAL"]){
        _caseEssayModelDegree = @"非核心期刊";
    }else if ([caseEssayModelDegree isEqualToString:@"NATIONAL_CONFERENCE"]){
        _caseEssayModelDegree = @"全国(级)会议交流";
    }else if ([caseEssayModelDegree isEqualToString:@"PROVINCIAL_CONFERENCE"]){
        _caseEssayModelDegree = @"省市(级)会议交流";
    }else if ([caseEssayModelDegree isEqualToString:@"HOSPITAL_CONFERENCE"]){
        _caseEssayModelDegree = @"院(级)会议交流";
    }else if ([caseEssayModelDegree isEqualToString:@"UNPUBLISHED"]){
        _caseEssayModelDegree = @"未发表";
    }else if ([caseEssayModelDegree isEqualToString:@"SCI"]){
        _caseEssayModelDegree = @"SCI期刊";
    }else{
        _caseEssayModelDegree = @"未知级别";
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelCategory;
- (void)setCaseEssayModelCategory:(NSString *)caseEssayModelCategory{
    if ([caseEssayModelCategory isKindOfClass:[NSNull class]]) {
        _caseEssayModelCategory = @"暂无";
    }else if ([caseEssayModelCategory isEqualToString:@"TRANSLATION"]){
        _caseEssayModelCategory = @"译文";
    }else if ([caseEssayModelCategory isEqualToString:@"MEDICAL_CASE"]){
        _caseEssayModelCategory = @"病例";
    }else if ([caseEssayModelCategory isEqualToString:@"REVIEWS"]){
        _caseEssayModelCategory = @"综述";
    }else if ([caseEssayModelCategory isEqualToString:@"THESIS"]){
        _caseEssayModelCategory = @"论文";
    }else{
        _caseEssayModelCategory = @"未知类别";
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelAuthor;
- (void)setCaseEssayModelAuthor:(NSString *)caseEssayModelAuthor{
    if ([caseEssayModelAuthor isKindOfClass:[NSNull class]]) {
        _caseEssayModelAuthor = @"暂无";
    }else{
        _caseEssayModelAuthor = caseEssayModelAuthor;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelEdtion;
- (void)setCaseEssayModelEdtion:(NSString *)caseEssayModelEdtion{
    if ([caseEssayModelEdtion isKindOfClass:[NSNull class]]) {
        _caseEssayModelEdtion = @"暂无";
    }else{
        _caseEssayModelEdtion = caseEssayModelEdtion;
    }
}
//@property (strong, nonatomic) NSNumber *caseEssayModelDate;
- (void)setCaseEssayModelDate:(NSNumber *)caseEssayModelDate{
    if ([caseEssayModelDate isKindOfClass:[NSNull class]]) {
        _caseEssayModelDate = @0;
    }else{
        _caseEssayModelDate = caseEssayModelDate;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelTeacher;
- (void)setCaseEssayModelTeacher:(NSString *)caseEssayModelTeacher{
    if ([caseEssayModelTeacher isKindOfClass:[NSNull class]]) {
        _caseEssayModelTeacher = @"暂无";
    }else{
        _caseEssayModelTeacher = caseEssayModelTeacher;
    }
}
//@property (strong, nonatomic) NSNumber *caseEssayModelTeacherId;
- (void)setCaseEssayModelTeacherId:(NSNumber *)caseEssayModelTeacherId{
    if ([caseEssayModelTeacherId isKindOfClass:[NSNull class]]) {
        _caseEssayModelTeacherId = @0;
    }else{
        _caseEssayModelTeacherId = caseEssayModelTeacherId;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelDescription;
- (void)setCaseEssayModelDescription:(NSString *)caseEssayModelDescription{
    if ([caseEssayModelDescription isKindOfClass:[NSNull class]]) {
        _caseEssayModelDescription = @"暂无";
    }else{
        _caseEssayModelDescription = caseEssayModelDescription;
    }
}
//@property (strong, nonatomic) NSString *caseEssayModelAdvice;
- (void)setCaseEssayModelAdvice:(NSString *)caseEssayModelAdvice{
    if ([caseEssayModelAdvice isKindOfClass:[NSNull class]]) {
        _caseEssayModelAdvice = @"暂无";
    }else{
        _caseEssayModelAdvice = caseEssayModelAdvice;
    }
}
//@property (strong, nonatomic) NSString *fileUrl;
- (void)setFileUrl:(NSString *)fileUrl{
    if ([fileUrl isKindOfClass:[NSNull class]]) {
        _fileUrl = @"暂无";
    }else{
        _fileUrl = fileUrl;
    }
}
@end
