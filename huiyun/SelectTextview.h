//
//  SelectTextview.h
//  huiyun
//
//  Created by MacAir on 2017/12/5.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionModel.h"
@interface SelectTextview : UITextView
@property (strong, nonatomic) QuestionModel *model;
@property (strong, nonatomic) NSNumber *currentIndex;

@property (strong, nonatomic) NSNumber *quesIndex;
@end

