//
//  CourseDetailController.m
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TCourseDetailController.h"
#import "TeachingAidsViewController.h"
#import "THDatePickerView.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]
@interface TCourseDetailController ()
{
    UITableView *teachTable;
    
    //第一部分Cell
    NSArray *titleArray;
    NSArray *contentArray;
    //第二部分cell
    NSArray *TitleCellArray;
    NSArray *CellcontentArray;
    
    UIButton *rightBtn;
    UIButton *editBtn;
    EditStatus editStatus;
    TCourseDatailCell *cell1;
    TimeCell *time;
    
    UIView *bgView;
    UIView *ToolsView;
    UIPickerView *Picker;
    NSMutableArray *PickDateLocation;
    NSMutableArray *LocationID;
    
    NSString *DateString;
    NSString *LocationString;
    
    NSString *scheduleIdString;
    NSString *scheduleStatusString;
    
    NSString *StartTime;
    
    NSString *EndTime;
    UIButton *BackBtn;
    NSString *currentTimeString;
    THDatePickerView *dateView;
    long EndTimeNum;
    long StartTimeNunm;
    long CurrentTimeNunm;
    UIImageView *backNavigation;
    
    
    NSString *DataStartTime;
    NSString *DataEndTime;
    NSString *DataLocation;
    
    
}
@end

@implementation TCourseDetailController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //默认非编辑
    editStatus = EditStatusNo;
    rightBtn.selected = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    [self setUI];
    [self SetNewTime];
}
- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"课程详情";
    [backNavigation addSubview:titleLab];
    

    if([self.model.scheduleStatus isEqualToString:@"计划中"]){
        rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame = CGRectMake(self.view.frame.size.width-80, 29.5, 100, 25);
        [rightBtn setTitle:@"修改" forState:UIControlStateNormal];
        [rightBtn addTarget:self action:@selector(CourseEdit:) forControlEvents:UIControlEventTouchUpInside];
        
        
        editBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        editBtn.frame = CGRectMake(self.view.frame.size.width-80, 29.5, 100, 25);
        [editBtn setTitle:@"完成" forState:UIControlStateNormal];
        [editBtn addTarget:self action:@selector(Edit:) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:editBtn];
        editBtn.hidden=YES;
        
        [backNavigation addSubview:rightBtn];
    }
 
}

-(void)CourseEdit:(UIButton *)sender{
    
    
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"进入编辑状态您可以修改课程上课时间以及上课地点(必须同时修改)" preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [MBProgressHUD showToastAndMessage:@"进入编辑模式~" places:0 toView:nil];
        rightBtn.hidden=YES;
        editBtn.hidden=NO;
        editStatus = EditStatusYes;
    }];
    
    UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    
    }];
    [alert addAction:noAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}
//   PUT /courseSchedules/{scheduleId}
-(void)Edit:(id)sender{
    
    NSLog(@"DateString:%@",DateString);
    
    
    
    NSString *scheduleTyoe;
    
    if ([_model.scheduleTyoe isKindOfClass:[NSNull class]]) {
      
        scheduleTyoe=@"";
    }else if ([_model.scheduleTyoe isEqualToString:@"课堂课程"]){
        scheduleTyoe=@"classroom_course";
    }else if ([scheduleTyoe isEqualToString:@"在线课程"]){
        scheduleTyoe=@"online_course";
    }else if ([scheduleTyoe isEqualToString:@"实操课程"]){
        scheduleTyoe=@"operating_course";
    }else if([scheduleTyoe isEqualToString:@"教学查房"]){
       scheduleTyoe=@"teaching_rounds";
    }else if([scheduleTyoe isEqualToString:@"病例讨论"]){
        scheduleTyoe=@"case_discussion";
    }

    NSString *Url=[NSString stringWithFormat:@"%@/courseSchedules/%@",LocalIP,self.model.scheduleID];
    NSLog(@"%@",Url);
    
    
    
    NSLog(@"StartTime==%@",StartTime);
    NSLog(@"EndTime==%@",EndTime);
    
    NSDictionary *Params;
    if (_model.required) {
        Params=@{
                 @"courseId":_model.courseId,
                 @"teacherId":_model.teacherId,
                 @"startTime":StartTime,
                 @"endTime":EndTime,
                 @"scheduleType":scheduleTyoe,
                 @"capacity":_model.capacity,
                 @"classroomId":LocationString,
//                 @"registerStartTime":_model.signstartTime,
//                 @"registerEndTime":_model.signendTime,
                 @"live":_model.live,
                 @"credit":_model.credit,
                 @"required":_model.required
                 };
    }else{
        Params=@{
                 @"courseId":_model.courseId,
                 @"teacherId":_model.teacherId,
                 @"startTime":StartTime,
                 @"endTime":EndTime,
                 @"scheduleType":_model.scheduleTyoe,
                 @"capacity":_model.capacity,
                 @"classroomId":LocationString,
                 @"registerStartTime":_model.signstartTime,
                 @"registerEndTime":_model.signendTime,
                 @"live":_model.live,
                 @"credit":_model.credit,
                 @"required":_model.required
                 };
    }

    NSLog(@"Params:%@",Params);
    
    [RequestTools RequestWithURL:Url Method:@"put" Params:Params Success:^(NSDictionary *result) {
        NSLog(@"%@",result);
        NSString *resultStr=[result objectForKey:@"errorCode"];
        NSLog(@"resultStr:%@",resultStr);
        
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            [MBProgressHUD showToastAndMessage:@"修改课程成功!" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            rightBtn.hidden=NO;
            editBtn.hidden=YES;
            editStatus = EditStatusNo;
        }else if ([resultStr isEqualToString:@"duplicate_room_in_course_schedule"]) {
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"同一时间该教室已被选中，无法正常创建课程！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }else if([resultStr isEqualToString:@"invalid_scheduled_time"]){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"填写时间有误，请核对后重新保存。温馨提示：此类状况多出现于报名时间于当前是时间相同。（报名时间应晚于当前时间并早于上课开始时间）" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else  if([resultStr isEqualToString:@"duplicate_course_info"]){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"重复的课程名称，请重新创建！！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else if([resultStr isEqualToString:@"course_scheduled_register_time_is_null"]){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"上课时间修改后，报名时间为零。请重新修改！！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    } failed:^(NSString *result) {
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"result:%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];

   
}


-(void)setUI{
    teachTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-100) style:UITableViewStylePlain];
    teachTable.showsVerticalScrollIndicator = NO;
    teachTable.delegate = self;
    teachTable.dataSource = self;
    teachTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [teachTable registerNib:[UINib nibWithNibName:@"TCourseDatailCell" bundle:nil] forCellReuseIdentifier:@"cellStyle1"];
    [teachTable registerNib:[UINib nibWithNibName:@"DesCell" bundle:nil] forCellReuseIdentifier:@"cellStyle2"];
     [teachTable registerNib:[UINib nibWithNibName:@"TimeCell" bundle:nil] forCellReuseIdentifier:@"timeCell"];
    [self.view addSubview:teachTable];
    
    if ([self.model.scheduleStatus isEqualToString:@"已发布"] || [self.model.scheduleStatus isEqualToString:@"计划中"]) {
        NSLog(@"startTime=%@ current=%@",self.model.startTime,[[Maneger shareObject] currentLong]);
        if (self.model.startTime.longValue > [[[Maneger shareObject] currentLong] longLongValue]) {
            UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            submitBtn.frame = CGRectMake(0, Sheight-44, Swidth, 44);
            submitBtn.backgroundColor = UIColorFromHex(0x20B2AA);
            [submitBtn setTitle:@"开始上课" forState:UIControlStateNormal];
            [submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
            [submitBtn addTarget:self action:@selector(beginAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:submitBtn];
        }
    }
    
    NSLog(@"timeFomatter=%@",[[Maneger shareObject] timeFormatter:self.model.startTime.stringValue]);

    titleArray = @[@"课程状态:",@"课程名称:",@"课程科目:",@"课程分类:",@"上课时间:",@"下课时间",@"上课地点:",@"课程简介:"];
    
    TitleCellArray=@[@"课程附件:",@"上课学生:",@"课前答题:",@"课后答题:",@"所需教具:"];
    
   
    contentArray = @[self.model.scheduleStatus,self.model.courseName,self.model.courseSubjec,self.model.courseCategoryName,[[Maneger shareObject] timeFormatter:self.model.startTime.stringValue],[[Maneger shareObject] timeFormatter:self.model.endTime.stringValue],self.model.coursePlace,self.model.courseDes];
    
        //待补充
   // CellcontentArray=@[];
    
    scheduleIdString=[NSString stringWithFormat:@"%@",self.model.scheduleID];
    scheduleStatusString=[NSString stringWithFormat:@"%@",self.model.scheduleStatus];
    NSLog(@"%@",scheduleIdString);
    
//    contentArray = @[self.model.courseName,self.model.courseSubjec,self.model.scheduleTyoe,start,end,self.model.teacherName,self.model.registNum.stringValue,self.model.courseDes];

    BackBtn=[[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    BackBtn.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
    BackBtn.hidden=YES;
    [self.view addSubview:BackBtn];
    
    
    StartTime=[NSString stringWithFormat:@"%@",_model.startTime];
    EndTime=[NSString stringWithFormat:@"%@",_model.endTime];
    LocationString=[NSString stringWithFormat:@"%@",_model.roomID];


}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
//开始上课
- (void)beginAction:(UIButton *)btn{
    [MBProgressHUD showHUDAndMessage:@"上课中..." toView:nil];
    if (btn.enabled == YES) {
        RequestTools *tool = [RequestTools new];
        [tool postRequestPrams:nil andURL:[NSString stringWithFormat:@"%@/courseSchedules/%@/start",LocalIP,self.model.scheduleID]];
        NSLog(@"beginAPI=%@",[NSString stringWithFormat:@"%@/courseSchedules/%@/start",LocalIP,self.model.scheduleID]);
        tool.errorBlock = ^(NSString *code){
            [MBProgressHUD hideHUDForView:nil];
            if (![code isEqualToString:@"200"]) {
                [MBProgressHUD showToastAndMessage:@"加载失败!" places:0 toView:nil];
            }
        };
        tool.responseBlock = ^(NSDictionary *result){
            [MBProgressHUD hideHUDForView:nil];
            if ([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
                [btn setTitle:@"上课中" forState:0];
                btn.enabled=NO;
                [MBProgressHUD showToastAndMessage:@"上课成功!" places:0 toView:nil];
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem: 0 inSection:0];
               cell1 = [teachTable cellForRowAtIndexPath:indexPath];
                cell1.content.text=@"正在上课";
                
                
            }else{
                [MBProgressHUD showToastAndMessage:@"课程未发布，暂不能上课!" places:0 toView:nil];
            }
        };
        
    }else{
        [Maneger showAlert:@"不可用" andCurentVC:self];
    }
}
#pragma -delegate&dataSourse

//返回分组数：
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

//返回每组行数：
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section ==0) {
        return 8;
    }else {
        return 5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row ==7) {
            return 120.f;
        }
        return 44.f;
    }
    return 44.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return 20.f;
    }
    return 20.f;
}

- ( NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0){
        NSString *str=@"课程基本信息:";
        return str;
    }else{
        NSString *str=@"课程其他信息:";
        return str;
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        if(editStatus == EditStatusYes){
            if(indexPath.row==4 || indexPath.row == 5){
            
                if(indexPath.row == 4){
                    [self ChooseStartTime];
                }else if(indexPath.row == 5){
                    [self ChooseEndTime];
                }
                BackBtn.hidden = NO;
                [dateView show];
            }else if(indexPath.row == 6){

                NSString *UrlLocation=[NSString stringWithFormat:@"%@/rooms?roomName=&pageSize=999",LocalIP];
                [RequestTools RequestWithURL:UrlLocation Method:@"get" Params:nil Success:^(NSDictionary *result) {
                    if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){
                        NSArray *array=[[result objectForKey:@"responseBody"]objectForKey:@"result"];
                        if (array.count == 0) {
                            [Maneger showAlert:@"暂无课程地址" andCurentVC:self];
                        }else{
                            
                            PickDateLocation=[NSMutableArray new];
                            LocationID=[NSMutableArray new];
                            
                            for(int i=0;i<array.count;i++){
                                NSDictionary *dic=[array objectAtIndex:i];
                                [PickDateLocation addObject:[dic objectForKey:@"roomName"]];
                                
                                [LocationID addObject:[dic objectForKey:@"roomId"]];
                                
                                [Picker reloadAllComponents];
                            }
                            [self SetBackView];
                        };
                    }
                    
                } failed:^(NSString *result) {
                    NSLog(@"请求错误");
                }];
             
            }
        }
    }else if(indexPath.section ==1){
        
        
        if(indexPath.row == 0){
                FileViewController *AfVc=[[FileViewController alloc]init];
                AfVc.CourseModel=_model;
                AfVc.Str=@"课程";
                [self.navigationController pushViewController:AfVc animated:YES];

        }else if(indexPath.row == 1){
            
            ApplyStudentViewController *vc=[[ApplyStudentViewController alloc]init];
            vc.scheduleIdString=scheduleIdString;
            vc.scheduleStatusString=scheduleStatusString;
            vc.model=_model;
            [self.navigationController pushViewController:vc animated:YES];
           
        }else if(indexPath.row==2 || indexPath.row == 3){
            CourseQuestionViewController *vc=[[CourseQuestionViewController alloc]init];
            if (indexPath.row == 2) {
                vc.str=@"课前";
            }else if(indexPath.row == 3){
                vc.str=@"课后";
            }
            vc.courseStr=@"课程";
            vc.model=_model;
            [self.navigationController pushViewController:vc animated:YES];
        }else if(indexPath.row == 4){
            
                TeachingAidsViewController *vc=[[TeachingAidsViewController alloc]init];
                vc.scheduleIdString=scheduleIdString;
                [self.navigationController pushViewController:vc animated:YES];

            
        }else{
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"此功能仍处于开发完善状态，请持续关注本产品。" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }

    }
         
    
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 4 || indexPath.row == 5) {
            time=[tableView dequeueReusableCellWithIdentifier:@"timeCell"];
            time.onetext.text=contentArray[indexPath.row];
            if (indexPath.row == 4) {
                time.OneLabel.text=@"上课时间";
                DataStartTime= time.onetext.text;
            }else{
                time.OneLabel.text=@"下课时间";
                DataEndTime=time.onetext.text;
            }
            [time setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            return time;
            
        }else if(indexPath.row == 6){
            
            cell1 = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
            [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            cell1.title.text = titleArray[indexPath.row];
            
            cell1.content.text = contentArray[indexPath.row];
            cell1.content.hidden=NO;
            cell1.RightImg.hidden=YES;
    
            DataLocation=cell1.content.text;
            NSLog(@"DataLocation:%@",DataLocation);
                 return cell1;
            
            
            
        }else if (indexPath.row == 7) {
            
            //课程简介：
            DesCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"cellStyle2"];
            cell2.detailDes.text = contentArray[indexPath.row];
            return cell2;
            
        }else {
            //平常cell:

            cell1 = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
            [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];

            cell1.title.text = titleArray[indexPath.row];
  
            cell1.content.text = contentArray[indexPath.row];
        
            cell1.content.hidden=NO;
            cell1.RightImg.hidden=YES;

            return cell1;
 
        }
        
    }else {
        
        
//        if (indexPath.section == 1) {
//            if (indexPath.row == 4) {
//                TCourseDatailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
//                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//                cell.title.text=TitleCellArray[indexPath.row];
//                cell.content.hidden=YES;
//                return cell;
//            }
//        }
        
        TCourseDatailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.title.text=TitleCellArray[indexPath.row];
        cell.content.hidden=YES;
        cell.RightImg.hidden=NO;
        return cell;
    }
}


-(void)SetBackView{
    //设置背景蒙版
    
    if(bgView != nil){
        bgView.hidden=YES;
    }
    
    
    
    bgView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    bgView.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
    
    //创建返回按钮：
    UIButton *backViewbtn=[UIButton buttonWithType:UIButtonTypeCustom];
    backViewbtn.frame=CGRectMake(0, 64, Swidth, Sheight);
    [backViewbtn addTarget:self action:@selector(HideBackView) forControlEvents:UIControlEventTouchUpInside];
 //   [bgView addSubview:backViewbtn];
    
    
    //创建工具栏：
    ToolsView=[[UIView alloc]initWithFrame:CGRectMake(0, Sheight-290, Swidth, 40)];
    ToolsView.backgroundColor=UIColorFromHex(0x20B2AA);
    
    UIButton *saveBtn = [[UIButton alloc] init];
    saveBtn.frame = CGRectMake(Swidth - 50, 2, 40, 40);
    [saveBtn setImage:[UIImage imageNamed:@"icon_select1"] forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:0];
    [saveBtn setTitle:@"确定" forState:0];
    [ToolsView addSubview:saveBtn];
    
    UIButton *cancelBtn = [[UIButton alloc] init];
    cancelBtn.frame = CGRectMake(10, 2, 40, 40);
    [cancelBtn setImage:[UIImage imageNamed:@"icon_revocation1"] forState:UIControlStateNormal];
    [cancelBtn setTitle:@"取消" forState:0];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:0];
    [cancelBtn addTarget:self action:@selector(HideBackView) forControlEvents:UIControlEventTouchUpInside];
    [ToolsView addSubview:cancelBtn];
    
  
    //创建选择器:
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0, Sheight-250, Swidth, 250)];
    Picker.backgroundColor=[UIColor whiteColor];
    Picker.delegate=self;
    Picker.dataSource=self;
    DateString =PickDateLocation[0];
    LocationString=LocationID[0];
    [bgView addSubview:ToolsView];
    [bgView addSubview:Picker];
    [[UIApplication sharedApplication].keyWindow addSubview:bgView];
    
}

-(void)saveBtnClick:(UIButton *)btn{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem: 6 inSection:0];
    
    cell1 = [teachTable cellForRowAtIndexPath:indexPath];
    cell1.content.text=DateString;
    bgView.hidden=YES;
}

-(void)HideBackView{
    bgView.hidden=YES;
  
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    
 return [PickDateLocation count];
    
    
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [PickDateLocation objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"%@",[PickDateLocation objectAtIndex:row]);
    DateString = [PickDateLocation objectAtIndex:row];
    
    LocationString=[LocationID objectAtIndex:row];
    NSLog(@"%@",LocationString);
}



#pragma mark 修改时间：

-(void)ChooseStartTime{
    
     NSIndexPath *indexPath = [NSIndexPath indexPathForItem: 4 inSection:0];
    CGFloat BackView_Y=Sheight/3+10;
    dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight-BackView_Y, Swidth, BackView_Y) andSureBlock:^(NSString *date, NSString *longDate) {
        BackBtn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
        time=[teachTable cellForRowAtIndexPath:indexPath];
        time.onetext.text=date;
        StartTime = longDate;
        StartTimeNunm=[longDate longLongValue];
        
        
        if (StartTimeNunm<CurrentTimeNunm) {
            UIAlertController *alert2=[UIAlertController alertControllerWithTitle:@"提示" message:@"上课时间早于当前时间，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction2=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                time.onetext.textColor=[UIColor redColor];
                
            }];
            
            [alert2 addAction:okAction2];
            [self presentViewController:alert2 animated:true completion:nil];
            
        }else{
            time.onetext.text=date;
            time.onetext.textColor=[UIColor blackColor];
        }
    } andCancelBlock:^{
        
        BackBtn.hidden = YES;
        
        
    }];
    [self.view addSubview:dateView];
    
}
-(void)SetNewTime{
    //获取当前时间
    NSDate *datenow = [NSDate date];
    
    NSString* currentTime=[NSString stringWithFormat:@"%0.f",[datenow timeIntervalSince1970]*1000];
    
    currentTimeString =currentTime;
    NSLog(@"currentTimeString:%@",currentTimeString);
    
    CurrentTimeNunm =[currentTimeString longLongValue];
}

-(void)ChooseEndTime{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem: 5 inSection:0];
    time=[teachTable cellForRowAtIndexPath:indexPath];
    CGFloat BackView_Y=Sheight/3+10;
    dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight-BackView_Y, Swidth, BackView_Y) andSureBlock:^(NSString *date, NSString *longDate) {
        BackBtn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
        EndTimeNum=[longDate longLongValue];
        EndTime = longDate;
        if (EndTimeNum<StartTimeNunm) {
            UIAlertController *alert2=[UIAlertController alertControllerWithTitle:@"提示" message:@"结束时间早于上课时间，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction2=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                time.onetext.layer.borderColor=[UIColor redColor].CGColor;
                
            }];
            
            [alert2 addAction:okAction2];
            [self presentViewController:alert2 animated:true completion:nil];
            
        }else if(EndTimeNum == StartTimeNunm){
            UIAlertController *alert2=[UIAlertController alertControllerWithTitle:@"提示" message:@"结束时间与开始时间相同，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction2=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                time.onetext.textColor=[UIColor redColor];
                
            }];
            
            [alert2 addAction:okAction2];
            [self presentViewController:alert2 animated:true completion:nil];
        }else{
            time.onetext.text=date;
            time.onetext.textColor=[UIColor blackColor];
        }
        
    } andCancelBlock:^{
        BackBtn.hidden = YES;
    }];
    [self.view addSubview:dateView];
    
}





@end

