//
//  MoldCell.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/27.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CourseMoldModel.h"

@interface MoldCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *Label;
@property (weak, nonatomic) IBOutlet UIButton *RightBtn;


- (void)setProperty:(CourseMoldModel *)model;


- (void)setChilder:(CourseMoldModel *)model;
@end
