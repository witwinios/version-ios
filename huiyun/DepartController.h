//
//  DepartController.h
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPopview.h"
#import "DepartCell.h"
#import "DepartModel.h"
#import "DepartMarkController.h"
#import "RoundRecordController.h"
#import "WXPPickerView.h"
@interface DepartController : UIViewController<UITableViewDelegate,UITableViewDataSource,selectIndexPathDelegate,PickerViewOneDelegate>
@property (strong, nonatomic) WXPPickerView *pickView;
@end

