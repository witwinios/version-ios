//
//  MenuView.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/31.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "DiseaseView.h"

@implementation DiseaseView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _listArray = [NSMutableArray new];
        self.backgroundColor = [UIColor lightGrayColor];
        _menuTable = [[UITableView alloc]initWithFrame:CGRectMake(1, 1, frame.size.width-2, frame.size.height-2)];
        _menuTable.delegate = self;
        _menuTable.dataSource = self;
        [_menuTable addHeaderWithTarget:self action:@selector(loadData)];
        [_menuTable headerBeginRefreshing];
        [self addSubview:_menuTable];
    }
    return self;
}
- (void)loadData{
    NSLog(@"url=%@",_url);
    [RequestTools RequestWithURL:_url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [_listArray removeAllObjects];
        [_menuTable headerEndRefreshing];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (arr.count == 0) {
            [MBProgressHUD showToastAndMessage:@"无分类~" places:0 toView:nil];
            return ;
        }
        for (int i=0; i<arr.count; i++) {
            NSDictionary *responseDic = arr[i];
            NSMutableDictionary *myDic = [NSMutableDictionary new];
            [myDic setObject:responseDic[@"diseaseICDName"] forKey:@"diseaseName"];
            [myDic setObject:responseDic[@"diseaseICDId"] forKey:@"diseaseId"];
            [_listArray addObject:myDic];
        }
        NSLog(@"--%ld",_listArray.count);
        [_menuTable reloadData];
    }failed:^(NSString *result) {
        
    }];
}
#pragma -table协议
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
     NSLog(@"--%ld",_listArray.count);
    return _listArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self removeFromSuperview];
    //传值
    _selectBlock(_listArray[indexPath.row][@"diseaseName"],_listArray[indexPath.row][@"diseaseId"]);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"menuCell";
    UITableViewCell *cell;
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.textLabel.text = _listArray[indexPath.row][@"diseaseName"];
        NSLog(@"===%@",_listArray[indexPath.row]);
        cell.textLabel.textAlignment = 1;
    }
    return cell;
}
@end


