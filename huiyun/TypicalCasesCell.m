//
//  TypicalCasesCell.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/10.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TypicalCasesCell.h"

@implementation TypicalCasesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.4];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setProperty:(TypicalCasesModel *)model{
    
    self.TypicalCasesName.text=model.TypicalCasesName;
    self.TypicalCasesBreed.text=model.TypicalCasesDiseaseICDName;
    self.TypicalCasesRecord.text=[NSString stringWithFormat:@"%@", model.TypicalCasesMedicalRecordsNum];
    
    if (model.isCheck) {
        self.BgVc.backgroundColor = [UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.2];
    } else {
        self.BgVc.backgroundColor = [UIColor whiteColor];
    }
}



@end
