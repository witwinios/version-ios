//
//  MeetItemCell.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/23.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeetItemCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lab;

@end
