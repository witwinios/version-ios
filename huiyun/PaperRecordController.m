//
//  PaperRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "PaperRecordController.h"
#import "THDatePickerView.h"

@interface PaperRecordController ()
{
    NSString *current_date;
    UIView *btnBack;
    NSString *current_degree;
    NSString *current_category;
    
    NSNumber *has_image;
    
    UIView *toolbar;
}
@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) UIButton *btn;

@end

@implementation PaperRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    current_date = @"";
    current_degree = @"";
    current_category = @"";
    has_image = @0;
    if ([SubjectTool share].subjectType == subjectTypeUpdate) {
        self.oneField.text = self.myModel.caseEssayModelTitle;
//        NSDictionary *two = @{@"CORE_JOURNA":@"核心期刊",@"NON_CORE_JOURNAL":@"非核心期刊",@"NATIONAL_CONFERENCE":@"全国(级)会议交流",@"PROVINCIAL_CONFERENCE":@"省市(级)会议交流",@"HOSPITAL_CONFERENCE":@"院(级)会议交流",@"UNPUBLISHED":@"未发表",@"SCI":@"SCI"};
//        NSDictionary *three = @{@"TRANSLATION":@"译文",@"MEDICAL_CASE":@"个案",@"REVIEWS":@"综述",@"THESIS":@"论文"};
        
        
        NSLog(@"2===%@",self.myModel.caseEssayModelDegree);
        NSLog(@"3===%@",self.myModel.caseEssayModelCategory);
        
        [self.twoBtn setTitle:self.myModel.caseEssayModelDegree forState:0];
        [self.threeBtn setTitle:self.myModel.caseEssayModelCategory forState:0];
        
        if ([self.myModel.caseEssayModelDegree isEqualToString:@"核心期刊"]) {
            current_degree =@"CORE_JOURNAL";
        }else if ([self.myModel.caseEssayModelDegree isEqualToString:@"非核心期刊"]) {
            current_degree =@"NON_CORE_JOURNAL";
        }else if ([self.myModel.caseEssayModelDegree isEqualToString:@"全国(级)会议交流"]) {
            current_degree =@"NATIONAL_CONFERENCE";
        }else if ([self.myModel.caseEssayModelDegree isEqualToString:@"省市(级)会议交流"]) {
            current_degree =@"PROVINCIAL_CONFERENCE";
        }else if ([self.myModel.caseEssayModelDegree isEqualToString:@"院(级)会议交流"]) {
            current_degree =@"HOSPITAL_CONFERENCE";
        }else if ([self.myModel.caseEssayModelDegree isEqualToString:@"未发表"]) {
            current_degree =@"UNPUBLISHED";
        }else if ([self.myModel.caseEssayModelDegree isEqualToString:@"SCI"]) {
            current_degree =@"SCI";
        }
        
        if ([self.myModel.caseEssayModelCategory isEqualToString:@"译文"]) {
            current_category=@"TRANSLATION";
        }else if ([self.myModel.caseEssayModelCategory isEqualToString:@"个案"]) {
            current_category=@"MEDICAL_CASE";
        }else if ([self.myModel.caseEssayModelCategory isEqualToString:@"综述"]) {
            current_category=@"REVIEWS";
        }else if ([self.myModel.caseEssayModelCategory isEqualToString:@"论文"]) {
            current_category=@"THESIS";
        }
            
        
//        current_degree = self.myModel.caseEssayModelDegree;
  //      current_category = self.myModel.caseEssayModelCategory;
        
        self.fourField.text = self.myModel.caseEssayModelAuthor;
        self.fiveField.text = self.myModel.caseEssayModelEdtion;
        self.contentTextview.text = self.myModel.caseEssayModelDescription;
        if (self.myModel.caseEssayModelDate.intValue == 0) {
            current_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseEssayModelDate];
            [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:self.myModel.caseEssayModelDate.stringValue] forState:0];
        }
    }

    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonHide:) name:UIKeyboardWillHideNotification object:nil];
    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];
    self.dateBtn.layer.cornerRadius = 4;
    self.dateBtn.layer.masksToBounds = YES;
    self.dateBtn.titleLabel.textAlignment = 0;
    self.dateBtn.layer.cornerRadius = 4;
    THDatePickerView *dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
        [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:longDate] forState:0];
        current_date = longDate;
    } andCancelBlock:^{
        self.btn.hidden = YES;
    } dateType:DateTypeDay];
    [self.view addSubview:dateView];
    self.dateView = dateView;
    

    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"论文记录";
    [backNavigation addSubview:titleLab];

}
- (void)resignFirstRespond{
    [self.oneField resignFirstResponder];
    [self.fourField resignFirstResponder];
    [self.fiveField resignFirstResponder];
    [self.contentTextview resignFirstResponder];
}
- (void)hideKb:(UIButton *)btn{
    [self resignFirstRespond];
}
-(void)doneButtonshow: (NSNotification *)notification {
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
}
- (void)doneButtonHide:(NSNotification *)notification{
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
    }];
}


#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)saveAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"题目不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_degree isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"级别不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_category isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"类别不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间不能为空~" places:0 toView:nil];
        return;
    }
 
    if ([_fiveField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"刊物不能为空~" places:0 toView:nil];
        return;
    }
    if (has_image.intValue == 0) {
        [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];

        SdepartRecordController *departVC = [SdepartRecordController shareObject];
    
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentEssayPaperRecords",SimpleIp];
        NSLog(@"%@",requestUrl);
        
        NSDictionary *params = @{@"paperTitle":_oneField.text,@"paperType":current_degree,@"articleType":current_category,@"publishedTime":current_date,@"authorRank":_fourField.text,@"publishedJournal":_fiveField.text,@"description":_contentTextview.text,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        
        NSLog(@"%@",params);
        
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
        [RequestTools RequestWithFile:_currentImage.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentEssayPaperRecords",SimpleIp];

            NSDictionary *params = @{@"paperTitle":_oneField.text,@"paperType":current_degree,@"articleType":current_category,@"publishedTime":current_date,@"authorRank":_fourField.text,@"publishedJournal":_fiveField.text,@"description":_contentTextview.text,@"fileId":fileId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
            
            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];

                }else{
                    [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];

        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }

}
- (void)updateAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"题目不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_degree isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"级别不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_category isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"类别不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间不能为空~" places:0 toView:nil];
        return;
    }
 
    if ([_fiveField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"刊物不能为空~" places:0 toView:nil];
        return;
    }
    if (has_image.intValue == 0){
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];

        SdepartRecordController *departVC = [SdepartRecordController shareObject];
        
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentEssayPaperRecords/%@",SimpleIp,self.myModel.recordId];
        
        NSLog(@"%@",requestUrl);
        
        NSDictionary *params = @{@"paperTitle":_oneField.text,
                                 @"paperType":current_degree,
                                 @"articleType":current_category,
                                 @"publishedTime":current_date,
                                 @"authorRank":_fourField.text,
                                 @"publishedJournal":_fiveField.text,
                                 @"description":_contentTextview.text,
                                 @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        
        NSLog(@"%@",params);
        
        [RequestTools RequestWithURL:requestUrl Method:@"PUT" Params:params Success:^(NSDictionary *result) {
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
        [RequestTools RequestWithFile:_currentImage.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentEssayPaperRecords/%@",SimpleIp,self.myModel.recordId];

            NSDictionary *params = @{@"paperTitle":_oneField.text,@"paperType":current_degree,@"articleType":current_category,@"publishedTime":current_date,@"authorRank":_fourField.text,@"publishedJournal":_fiveField.text,@"description":_contentTextview.text,@"fileId":fileId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
            
            [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
                }else{
                    [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];

        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }

}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}
- (void)popLast{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveAction:(id)sender {
    if ([SubjectTool share].subjectType == subjectTypeAdd) {
        [self saveAction];
    }else{
        [self updateAction];
    }
}

- (IBAction)dateAction:(id)sender {
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];
}
- (IBAction)threeAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    if (btn.selected) {
        _menuView = [[ListView alloc]initWithFrame:CGRectMake(btn.frame.origin.x, btn.frame.origin.y+2+btn.frame.size.height+64, btn.frame.size.width, 250)];
        
        _menuView.listArray = @[@"译文",@"个案",@"综述",@"论文"];
        _menuView.selectBlock = ^(NSString *str,NSInteger index){
            btn.selected = !btn.selected;
            NSArray *content = @[@"TRANSLATION",@"MEDICAL_CASE",@"REVIEWS",@"THESIS"];
            current_category = content[index];
            [btn setTitle:str forState:UIControlStateNormal];
            
        };
        [self.view addSubview:_menuView];
    }else{
        [_menuView removeFromSuperview];
    }

}

- (IBAction)twoAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    if (btn.selected) {
        _menuView = [[ListView alloc]initWithFrame:CGRectMake(btn.frame.origin.x, btn.frame.origin.y+2+btn.frame.size.height+64, btn.frame.size.width, 250)];
        _menuView.listArray = @[@"核心期刊",@"非核心期刊",@"全国(级)会议交流",@"省市(级)会议交流",@"院(级)会议交流",@"未发表",@"SCI"];
        _menuView.selectBlock = ^(NSString *str,NSInteger index){
            NSArray *content = @[@"CORE_JOURNAL",@"NON_CORE_JOURNAL",@"NATIONAL_CONFERENCE",@"PROVINCIAL_CONFERENCE",@"HOSPITAL_CONFERENCE",@"UNPUBLISHED",@"SCI"];
            current_degree = content[index];
            btn.selected = !btn.selected;
            [btn setTitle:str forState:UIControlStateNormal];
            
        };
        [self.view addSubview:_menuView];
    }else{
        [_menuView removeFromSuperview];
    }
}
- (IBAction)fileAction:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相册",@"拍照", nil];
    [sheet showInView:self.view];

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self getAlbumAction];
            break;
        case 1:
            [self photoAction];
            break;
        default:
            break;
    }
}
- (void)photoAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePickController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)getAlbumAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    [picker dismissViewControllerAnimated:YES completion:nil];
    has_image = @1;
    _currentImage.image = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
}


@end
