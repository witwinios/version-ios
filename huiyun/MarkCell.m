//
//  MarkCell.m
//  yun
//
//  Created by MacAir on 2017/7/21.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "MarkCell.h"

@implementation MarkCell
- (UILabel *)indexLab{
    if (!_indexLab) {
        _indexLab = [UILabel new];
        _indexLab.backgroundColor = UIColorFromHex(0x20B2AA);
        _indexLab.textColor = [UIColor whiteColor];
        _indexLab.textAlignment = 1;
        _indexLab.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_indexLab];
    }
    return _indexLab;
}
- (UILabel *)categoryLab{
    if (!_categoryLab) {
        _categoryLab = [UILabel new];
        _categoryLab.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_categoryLab];
    }
    return _categoryLab;
}
- (UILabel *)categoryContentLab{
    if (!_categoryContentLab) {
        _categoryContentLab = [UILabel new];
        _categoryContentLab.numberOfLines = 0;
        _categoryContentLab.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:_categoryContentLab];
    }
    return _categoryContentLab;
}
- (UILabel *)markLab{
    if (!_markLab) {
        _markLab = [UILabel new];
        _markLab.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_markLab];
    }
    return _markLab;
}
- (UILabel *)markContentLab{
    if (!_markContentLab) {
        _markContentLab = [UILabel new];
        _markContentLab.numberOfLines = 0;
        _markContentLab.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_markContentLab];
    }
    return _markContentLab;
}
- (UILabel *)operateLab{
    if (!_operateLab) {
        _operateLab = [UILabel new];
        _operateLab.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_operateLab];
    }
    return _operateLab;
}
- (UILabel *)operateContentLab{
    if (!_operateContentLab) {
        _operateContentLab = [UILabel new];
        _operateContentLab.numberOfLines = 0;
        _operateContentLab.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_operateContentLab];
    }
    return _operateContentLab;
}
- (UILabel *)keyLab{
    if (!_keyLab) {
        _keyLab = [UILabel new];
        _keyLab.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_keyLab];
    }
    return _keyLab;
}
- (UILabel *)keyContentLab{
    if (!_keyContentLab) {
        _keyContentLab = [UILabel new];
        _keyContentLab.numberOfLines = 0;
        _keyContentLab.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_keyContentLab];
    }
    return _keyContentLab;
}
- (UILabel *)totalScore{
    if (!_totalScore) {
        _totalScore = [UILabel new];
        _totalScore.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_totalScore];
    }
    return _totalScore;
}
- (UILabel *)totalScoreContent{
    if (!_totalScoreContent) {
        _totalScoreContent = [UILabel new];
        _totalScoreContent.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_totalScoreContent];
    }
    return _totalScoreContent;
}
- (UILabel *)getscoreValue{
    if (!_getscoreValue) {
        _getscoreValue = [UILabel new];
        _getscoreValue.textColor = [UIColor redColor];
        _getscoreValue.font = [UIFont systemFontOfSize:18];
        [self.contentView addSubview:_getscoreValue];
    }
    return _getscoreValue;
}
- (UILabel *)dockscoreValue{
    if (!_dockscoreValue) {
        _dockscoreValue = [UILabel new];
        _dockscoreValue.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_dockscoreValue];
    }
    return _dockscoreValue;
}
- (UITextField *)dockField{
    if (!_dockField) {
        _dockField = [UITextField new];
        _dockField.keyboardType = UIKeyboardTypeDecimalPad;
        _dockField.layer.cornerRadius = 3;
        _dockField.layer.masksToBounds = YES;
        _dockField.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _dockField.layer.borderWidth = 1;
        [self.contentView addSubview:_dockField];
    }
    return _dockField;
}
- (UILabel *)dockreasonLab{
    if (!_dockreasonLab) {
        _dockreasonLab = [UILabel new];
        _dockreasonLab.font = [UIFont systemFontOfSize:20];
        [self.contentView addSubview:_dockreasonLab];
    }
    return _dockreasonLab;
}
- (UITextView *)dockreasonContent{
    if (!_dockreasonContent) {
        _dockreasonContent = [UITextView new];
        _dockreasonContent.layer.cornerRadius = 3;
        _dockreasonContent.layer.masksToBounds = YES;
        _dockreasonContent.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _dockreasonContent.layer.borderWidth = 1;
        [self.contentView addSubview:_dockreasonContent];
    }
    return _dockreasonContent;
}
- (void)awakeFromNib {
    [super awakeFromNib];
}
- (void)setModel:(MarkTableModel *)model andIndexPath:(NSIndexPath *)indexPath{
    _model = model;
    _indexRow =@(indexPath.row);
    CGFloat space = 5.0f;
    self.indexLab.text = [NSString stringWithFormat:@"第%ld项",indexPath.row+1];
    _indexLab.frame = CGRectMake(0, 0, Swidth, 40);
    
    self.categoryLab.frame = CGRectMake(10, 40+space, 100, 20);
    self.categoryLab.text = @"评分类别:";

    self.categoryContentLab.text = model.MarkTableKeyType;
    
    CGFloat categoryContentLab_H=[self.categoryContentLab getSpaceLabelHeight:self.categoryContentLab.text withWidh:Swidth-110];
    
    self.categoryContentLab.frame = CGRectMake(110, 40+space, Swidth-110, categoryContentLab_H);
    
    self.markLab.text = @"评分项目:";
    self.markLab.frame = CGRectMake(10, CGRectGetMaxY(self.categoryContentLab.frame)+space, 100, 20);
    
    self.markContentLab.text = self.model.MarkTableKey;
    self.markContentLab.frame = CGRectMake(110, CGRectGetMaxY(self.categoryContentLab.frame)+space, Swidth-110, [Maneger autoCalculateWidth:self.model.MarkTableKey andFont:[UIFont systemFontOfSize:20] Width:Swidth-110]);
    
    self.operateLab.text = @"操作内容:";
    self.operateLab.frame = CGRectMake(10, CGRectGetMaxY(self.markContentLab.frame)+space, 100, 20);
    
    self.operateContentLab.text = self.model.MarkTableDescription;
    self.operateContentLab.frame = CGRectMake(110, CGRectGetMaxY(self.markContentLab.frame)+space, Swidth-110, [Maneger autoCalculateWidth:self.model.MarkTableDescription andFont:[UIFont systemFontOfSize:20] Width:Swidth-110]);
    
    self.keyLab.text = @"操作要点:";
    self.keyLab.frame = CGRectMake(10, CGRectGetMaxY(self.operateContentLab.frame)+space, 100, 20);

    self.keyContentLab.text = self.model.MarkTableAnswer;
    self.keyContentLab.frame = CGRectMake(110, CGRectGetMaxY(self.operateContentLab.frame)+space, Swidth-110, [Maneger autoCalculateWidth:self.model.MarkTableAnswer andFont:[UIFont systemFontOfSize:20] Width:Swidth-110]);

    self.totalScore.text = @"总分:";
    self.totalScore.frame = CGRectMake(10, CGRectGetMaxY(self.keyContentLab.frame)+space, 100, 20);
    
    
    NSString *str= [NSString stringWithFormat:@"%@",self.model.MarkTableTotalScore];
    self.totalScoreContent.text =[NSString stringWithFormat:@"%0.1f",[str doubleValue]];
   
    self.totalScoreContent.frame = CGRectMake(110, CGRectGetMaxY(self.keyContentLab.frame)+space, 50, 20);
    self.totalScoreContent.adjustsFontSizeToFitWidth = YES;
    
    self.getscoreValue.frame = CGRectMake(170, CGRectGetMaxY(self.keyContentLab.frame)+space,150, 20);
    NSLog(@"getscore=%@",
          _model.getScore);
    self.getscoreValue.text = _model.getScore.intValue == 0?@"":[NSString stringWithFormat:@"得分:%@",_model.getScore];

    self.dockscoreValue.text = @"扣分:";
    self.dockscoreValue.frame = CGRectMake(10, CGRectGetMaxY(self.getscoreValue.frame)+space, 100, 20);

    self.dockField.frame = CGRectMake(110, CGRectGetMaxY(self.getscoreValue.frame)+space, Swidth-120, 20);
    self.dockField.delegate = self;
    self.dockField.text = _model.dockScore.intValue == 0 ? @"":[NSString stringWithFormat:@"%@",_model.dockScore];
 
    self.dockreasonLab.text = @"扣分原因:";
    self.dockreasonLab.frame = CGRectMake(10, CGRectGetMaxY(self.dockField.frame)+space, 100, 20);
    self.dockreasonContent.delegate = self;
    self.dockreasonContent.text = _model.scoreReason;

    self.dockreasonContent.frame = CGRectMake(10, CGRectGetMaxY(self.dockreasonLab.frame)+space, Swidth-20, 50);

    _cellHeight = @(CGRectGetMaxY(self.dockreasonContent.frame)+space);
}
#pragma -delegate
//- (void)textViewDidBeginEditing:(UITextView *)textView{
//    _selectView(textView);
//}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{

    _selectView(textView);
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
        _selectView(textField);
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    //删除处理
    if ([string isEqualToString:@""]) {
        NSRange range = NSMakeRange(0, textField.text.length-1);
        NSString *fomatStr = [textField.text substringWithRange:range];
        _getscoreValue.text = [NSString stringWithFormat:@"得分:%.2f", _model.MarkTableTotalScore.floatValue - [fomatStr floatValue]];
        
        return YES;
    }
    //首位不能为.号
    if (range.location == 0 && [string isEqualToString:@"."]) {
        [MBProgressHUD showToastAndMessage:@"首字符不能为小数~" places:0 toView:nil];
        return NO;
    }
 
    return [self isRightInPutOfString:textField.text withInputString:string range:range];
}
- (BOOL)isRightInPutOfString:(NSString *) string withInputString:(NSString *) inputString range:(NSRange) range{
    //判断只输出数字和.号
    NSString *passWordRegex = @"[0-9\\.]";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    if (![passWordPredicate evaluateWithObject:inputString]) {
        return NO;
    }

    //逻辑处理
    if ([string containsString:@"."]) {
        if ([inputString isEqualToString:@"."]) {
            return NO;
        }
        NSRange subRange = [string rangeOfString:@"."];
        if (range.location - subRange.location > 2) {
            return NO;
        }
    }
    NSString *fomatStr = [NSString stringWithFormat:@"%@%@",string,inputString];
    if ([fomatStr floatValue] > _model.MarkTableTotalScore.floatValue) {
        [MBProgressHUD showToastAndMessage:@"扣分项应小于总分~" places:0 toView:nil];
        return NO;
    }
    _getscoreValue.text = [NSString stringWithFormat:@"得分:%.2f",_model.MarkTableTotalScore.floatValue - [fomatStr floatValue]];
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    _textAction(_indexRow,textView.text);
    _model.scoreReason = textView.text;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    _model.dockScore = @(textField.text.floatValue);
    _model.getScore = @(_model.MarkTableTotalScore.floatValue - textField.text.floatValue);
    _fieldAction(_indexRow,[NSString stringWithFormat:@"%@",_model.getScore]);
}
@end
