//
//  MyTwoCollectionCell.m
//  huiyun
//
//  Created by 王慕铁 on 2018/2/26.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "MyTwoCollectionCell.h"

@implementation MyTwoCollectionCell

//-(instancetype)initWithFrame:(CGRect)frame{
//    self=[super initWithFrame:frame];
//
//    if (self ) {
//        [self addSubview:_QuestionTypes];
//    }
//    return self;
//}
- (UILabel *)QuestionTypes{
    if (!_QuestionTypes) {
        //题型
        _QuestionTypes=[[UILabel alloc]init];
        [_QuestionTypes setFont:[UIFont systemFontOfSize:15]];
        [_QuestionTypes setNumberOfLines:0];
    }
    return _QuestionTypes;
}
-(void)setQuestions:(QuestionModel *)model{

    [self.contentView addSubview:self.QuestionTypes];
    
    //题型：
    if ([model.questionType isEqualToString:@"A3题型"]) {
        self.QuestionTypes.text=@"A3:病例组型题";
    }if([model.questionType isEqualToString:@"A4题型"]){
        self.QuestionTypes.text=@"A4:病例串型题";
    }else if([model.questionType isEqualToString:@"B1题型"]){
        self.QuestionTypes.text=@"B1:标准配伍题型";
    }
    
    self.QuestionTypes.textAlignment=NSTextAlignmentCenter;
    self.QuestionTypes.textColor=UIColorFromHex(0x20B2AA);
    self.QuestionTypes.layer.borderWidth=2;
    self.QuestionTypes.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.QuestionTypes.layer.cornerRadius=5;
    
    //  CGFloat QuestionTypes_H=[self.QuestionTypes getSpaceLabelHeight:self.QuestionTypes.text withWidh:Swidth-40];
    [self.QuestionTypes setFrame:CGRectMake(10,10, 200, 20)];
    
}

@end
