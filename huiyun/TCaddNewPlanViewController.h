//
//  TCaddNewPlanViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/14.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseAddModel.h"
#import "TCourseDatailCell.h"
#import "TCourseMoldViewController.h"
#import "AffixViewController.h"
#import "FileViewController.h"
#import "StandardCourseWareViewController.h"
#import "CourseWareViewController.h"
#import "trainingAidsViewController.h"
#import "CourseQuestionViewController.h"
#import "TypicalCasesViewController.h"
@interface TCaddNewPlanViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextViewDelegate>

//教案名称：
@property (weak, nonatomic) IBOutlet UILabel *NewPlanLabel;
@property (weak, nonatomic) IBOutlet UITextField *NewPlanText;
//教案类型：
@property (weak, nonatomic) IBOutlet UILabel *NewPlanGenreLabel;
@property (weak, nonatomic) IBOutlet UIButton *NewPlanGenreBtn;
//教案科目：
@property (weak, nonatomic) IBOutlet UILabel *NewPlanSubjectLabel;
@property (weak, nonatomic) IBOutlet UIButton *NewPlanSubjectBtn;
//教案分类：
@property (weak, nonatomic) IBOutlet UILabel *NewPlanClassifyLabel;
@property (weak, nonatomic) IBOutlet UIButton *NewPlanClassifyBtn;
//教案简介：
@property (weak, nonatomic) IBOutlet UIView *NewPlanIntroView;
@property (weak, nonatomic) IBOutlet UITextView *NewPlanIntroTextView;
@property (weak, nonatomic) IBOutlet UILabel *stirngLenghLabel;
//教案状态：
@property (weak, nonatomic) IBOutlet UISegmentedControl *NewPlanStateSegmented;
//
@property(strong,nonatomic)NSString *Condition;

@property(strong,nonatomic)TCourseAddModel *PlanModel;
@property(strong,nonatomic)NSString *NewStr;
@property(nonatomic,getter=isEditing) BOOL editing;
@end
