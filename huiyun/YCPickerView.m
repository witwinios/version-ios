//
//  YCPickerView.m
//  xiaoyun
//
//  Created by MacAir on 17/2/24.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#define SCREEN_WIDTH                [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT               [[UIScreen mainScreen] bounds].size.height
#define UIColorFromHex(s)  [UIColor colorWithRed:(((s & 0xFF0000) >> 16))/255.0 green:(((s &0xFF00) >>8))/255.0 blue:((s &0xFF))/255.0 alpha:1.0]

#import "YCPickerView.h"

@interface YCPickerView () <UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSInteger selectRow;
}

@property (retain, nonatomic) UIView *baseView;
@property (retain, nonatomic) UILabel *showLab;
@end

@implementation YCPickerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        selectRow = 0;
        
        _baseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 250)];
        _baseView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_baseView];
        //
        _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 210)];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        [_baseView addSubview:_pickerView];
        //显示
        UIView *backView = [UIView new];
        backView.frame = CGRectMake(0, 170, SCREEN_WIDTH, 40);
//        backView.backgroundColor = [UIColor blueColor];
        [_baseView addSubview:backView];
        _showLab = [[UILabel alloc]initWithFrame:CGRectMake(30, 5,SCREEN_WIDTH-150, 30)];
        _showLab.backgroundColor = UIColorFromHex(0x46bec8);
        _showLab.textAlignment = 1;
        _showLab.textColor = [UIColor whiteColor];
        _showLab.layer.cornerRadius = 5;
        [backView addSubview:_showLab];
//        //确定按钮
        UIButton *btnOK = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-100, 5, 70, 30)];
        [btnOK setTitle:@"确定" forState:UIControlStateNormal];
        [btnOK setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnOK addTarget:self action:@selector(pickerViewBtnOK:) forControlEvents:UIControlEventTouchUpInside];
        btnOK.backgroundColor = UIColorFromHex(0x46bec8);
        btnOK.layer.cornerRadius = 5;
        [backView addSubview:btnOK];
        //
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPickerView)];
//        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

#pragma mark - UIPickerViewDataSource

//返回多少列
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

//每列对应多少行
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _arrPickerData.count;
}

//每行显示的数据
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return _arrPickerData[row];
}


#pragma mark - UIPickerViewDelegate

//选中pickerView的某一行
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _showLab.text = _arrPickerData[row];
    selectRow = row;
}

#pragma mark - Private Menthods

//弹出pickerView
- (void)popPickerView
{
    //获取当前正在显示的行
    NSInteger row =[_pickerView selectedRowInComponent:0];
    _showLab.text = _arrPickerData[row];
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.frame = CGRectMake(0, SCREEN_HEIGHT-210, SCREEN_WIDTH, 210);
                     }];
}

//取消pickerView
- (void)dismissPickerView
{
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 210);
                     }];
}

//确定
- (void)pickerViewBtnOK:(id)sender
{
    if (self.selectBlock) {
        self.selectBlock(_arrPickerData[selectRow]);
    }
    [self dismissPickerView];
}

//取消
//- (void)pickerViewBtnCancel:(id)sender
//{
//    if (self.selectBlock) {
 //       self.selectBlock(nil);
 //   }
 //   [self dismissPickerView];
//}
@end

