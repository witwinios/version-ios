//
//  AlertView.h
//  huiyun
//
//  Created by MacAir on 2017/9/11.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^BackBlock) (NSString *str);
@interface AlertView : UIView<UITextFieldDelegate>
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *inType;
@property (strong, nonatomic) UITextField *contentField;
@property (strong, nonatomic) UILabel *msgLab;
@property (copy, nonatomic) BackBlock backBlock;
@end
