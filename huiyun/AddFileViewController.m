//
//  AddFileViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/27.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "AddFileViewController.h"

@interface AddFileViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *EditBtn;
    UIButton *SaveBtn;
}

@end

@implementation AddFileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNav];
    
    
    [self setUI];
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"新增附件";
    [backNavigation addSubview:titleLab];
    
    
    EditBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    EditBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [EditBtn setTitle:@"添加" forState:UIControlStateNormal];
    [EditBtn addTarget:self action:@selector(EditPlan:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:EditBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(EditSavePlan:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;
    
    
}

-(void)EditPlan:(UIButton *)sender{
    
}

-(void)EditSavePlan:(UIButton *)sender{
    
}


- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUI{
    self.view.backgroundColor=[UIColor whiteColor];
    
}


@end
