//
//  TitleButton.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/29.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TitleButton.h"

@implementation TitleButton


-(instancetype)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        self.titleLabel.font=[UIFont systemFontOfSize:15];
        [self setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    }
    return self;
}


-(void)setHighlighted:(BOOL)highlighted{
    highlighted=NO;
}

@end
