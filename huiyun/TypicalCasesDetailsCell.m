//
//  TypicalCasesDetailsCell.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/12.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TypicalCasesDetailsCell.h"

@implementation TypicalCasesDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
     self.backgroundColor=[[UIColor darkGrayColor]colorWithAlphaComponent:0.3];
     self.TextView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.2];
}


-(void)setProperty:(TypicalCasesDetailsModel *)model{
    self.OneLabel.text=@"类别:";
    self.TwoLabel.text=@"日期:";
    self.ThreeLabel.text=@"描述:";
    
    self.oneText.text=model.RecordType;
    NSString *time = [NSString stringWithFormat:@"%@",[[Maneger shareObject] timeFormatter:model.RecordTime.stringValue]];
    self.TwoText.text=time;
    self.TextView.text=model.RecordDescription;
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
