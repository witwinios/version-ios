//
//  CourseViewController.h
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HYCourseCell.h"
#import "LoadingView.h"
#import "RequestTools.h"
#import "TCourseModel.h"
#import "TCourseDetailController.h"
#import "CustomPopview.h"
@interface TCourseController: UIViewController<UITableViewDataSource,UIPopoverPresentationControllerDelegate,UITableViewDelegate,UIAlertViewDelegate,UISearchBarDelegate,UISearchResultsUpdating,UISearchBarDelegate,selectIndexPathDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>


-(void)FilterContentForSearchText:(NSString *)searchText scope:(NSUInteger)scope;

@property(nonatomic,getter=isEditing) BOOL editing;
-(void)setEditing:(BOOL)editing animated:(BOOL)animated;

@end

