//
//  CellStyle1.h
//  yun
//
//  Created by MacAir on 2017/6/22.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellStyle1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leaveTime;
@property (weak, nonatomic) IBOutlet UILabel *leaveStatus;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
