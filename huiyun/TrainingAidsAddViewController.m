//
//  TrainingAidsAddViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/11.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TrainingAidsAddViewController.h"

@interface TrainingAidsAddViewController ()<UITextFieldDelegate>
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *courseTable;
    NSMutableArray *dataArray;
    int currentPage;
    
    NSInteger line;
    
    NSMutableArray *seleArr;
    NSMutableArray *NewArr;
    
    NSString *CommentsStr;
    NSString *UseForStr;
    NSString *StrForUser;
    
    NSIndexPath * IndexPath;
    UITextField *Comments;
    UIButton *Tybtn;
    
    UIView *FirstView;
    UIView *ChildrenView;
    UIView *ToolView;
    
    UIPickerView *picker;
    UIButton *okBtn;
    UIButton *noBtn;
    
    NSMutableArray *PickDate;
    NSMutableArray *PickDateID;
    
    UIView *SearchView;
    UITextField *SearchField;
    UIButton *SearchBtn;
    
    BOOL isSearch;
}


@end

@implementation TrainingAidsAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [self setsearchBar];
    [self setNav];
    [self setUI];
    [self setUpRefresh];
    
    
    
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"教案教具列表";
    
    
    [backNavigation addSubview:titleLab];
    
    
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [rightBtn setTitle:@"添加" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(Choice:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;
    
}
- (void)back :(UIButton *)button{
    if(seleArr.count >0){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您有尚未保存的教具，是否保存？" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self save];
        }];
        
        UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert addAction:noAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    };
}
-(void)Choice:(id)sender{
   // [courseTable setEditing:YES animated:YES];
    
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您现在可以选择教案教具." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _editing=YES;
        rightBtn.hidden=YES;
        SaveBtn.hidden=NO;
    }];
    
    UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:noAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];

}
-(void)setsearchBar{
    
    
    isSearch=NO;
    
    SearchView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 44)];
    SearchView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    UIImageView *SearchImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sousuo"]];
    SearchImg.backgroundColor=[UIColor clearColor];
    SearchImg.frame=CGRectMake(10, 6, 32, 32);
    
    
    SearchField=[[UITextField alloc]initWithFrame:CGRectMake(44, 6, Swidth-88, 32)];
    SearchField.backgroundColor=[UIColor clearColor];
    SearchField.placeholder=@"请输入搜索的教具名称";
    SearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    SearchField.clearsOnBeginEditing = YES;
    SearchField.keyboardType=UIKeyboardTypeDefault;
    SearchField.returnKeyType=UIReturnKeySearch;
    SearchField.delegate = self;
    [SearchField addTarget:self action:@selector(ApplyTextDidChanges:) forControlEvents:UIControlEventEditingChanged];
    
    SearchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SearchBtn.frame=CGRectMake((SearchField.wwy_x+SearchField.wwy_width)+2, 6, 40, 32);
    [SearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [SearchBtn addTarget:self action:@selector(setUpRefresh) forControlEvents:UIControlEventTouchUpInside];
    SearchBtn.backgroundColor=UIColorFromHex(0x20B2AA);
    SearchBtn.layer.cornerRadius=5;
    SearchBtn.hidden=YES;
    [SearchView addSubview:SearchBtn];
    [SearchView addSubview:SearchField];
    [SearchView addSubview:SearchImg];
    [self.view addSubview:SearchView];
    
}


-(BOOL)ApplyTextDidChanges:(UITextField *)theTextField{
    
    NSLog( @"text changed: %@", theTextField.text);
    
    
    if (![theTextField.text isEqualToString:@""]) {
        SearchBtn.hidden=NO;
        isSearch=YES;
    }else{
        isSearch=NO;
        [self setUpRefresh];
        SearchBtn.hidden=YES;
        [SearchField  resignFirstResponder];
    }
    
    return YES;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [SearchField  resignFirstResponder];
    
    
    [self setUpRefresh ];
    
    
    
    NSLog(@"点击了搜索");
    
    return YES;
    
}


-(void)setUI{
    currentPage = 1;
    CGFloat courseTable_Y=backNavigation.wwy_height+SearchView.wwy_height;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, courseTable_Y,Swidth, Sheight-courseTable_Y) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    courseTable.estimatedRowHeight = 0;
    [courseTable registerNib:[UINib nibWithNibName:@"TeachingAidsCell" bundle:nil] forCellReuseIdentifier:@"AidsCell"];
    [self.view addSubview:courseTable];
    _editing=NO;
    dataArray=[NSMutableArray new];
    seleArr=[NSMutableArray new];
    NewArr=[NSMutableArray  new];
    
}
-(void)setUpRefresh{
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中。。。";
}

//http://www.hzwitwin.cn:81/witwin-ctts-web/trainingAids/models?pageStart=1&pageSize=10&currentPage=1
-(void)loadData{
    if (_editing) {
        [seleArr removeAllObjects];
    }
    currentPage=1;
    
    NSString *Url;
    if(isSearch == NO){
        Url =[NSString stringWithFormat:@"%@/trainingAids/models?pageStart=%d&pageSize=10",LocalIP,currentPage];
    }else{
         NSString *str=SearchField.text;
        Url =[NSString stringWithFormat:@"%@/trainingAids/models?pageStart=%d&pageSize=10&modelName=%@",LocalIP,currentPage,str];
        Url=[Url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
   
    
    NSLog(@"%@",Url);
    
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                
                [Maneger showAlert:@"当前无教具信息!" andCurentVC:self];
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    TeachingAidsModel *model=[TeachingAidsModel new];
                    model.ModelId=[dictionary objectForKey:@"modelId"];
                    model.ModelName=[dictionary objectForKey:@"modelName"];
                    model.ModelCategoryName=[dictionary objectForKey:@"categoryName"];
                    model.ModelNo=[dictionary objectForKey:@"modelNo"];
                    model.ModelItemsNum=[dictionary objectForKey:@"itemsNum"];
                    model.ModelAmount=[dictionary objectForKey:@"amount"];
                    
           
                    
                    [dataArray addObject:model];
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];

            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
        
        
        
        
    } failed:^(NSString *result) {
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
    
}

-(void)loadRefresh{
    currentPage++;
    
    
    NSString *Url;
    if(isSearch == NO){
        Url =[NSString stringWithFormat:@"%@/trainingAids/models?pageStart=%d&pageSize=10",LocalIP,currentPage];
    }else{
        NSString *str=SearchField.text;
        Url =[NSString stringWithFormat:@"%@/trainingAids/models?pageStart=%d&pageSize=10&modelName=%@",LocalIP,currentPage,str];
        Url=[Url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    NSLog(@"%@",Url);
    
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                
                [Maneger showAlert:@"当前无教具信息!" andCurentVC:self];
                
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    TeachingAidsModel *model=[TeachingAidsModel new];
                    model.ModelId=[dictionary objectForKey:@"modelId"];
                    model.ModelName=[dictionary objectForKey:@"modelName"];
                    model.ModelCategoryName=[dictionary objectForKey:@"categoryName"];
                    model.ModelNo=[dictionary objectForKey:@"modelNo"];
                    model.ModelItemsNum=[dictionary objectForKey:@"itemsNum"];
                    model.ModelAmount=[dictionary objectForKey:@"amount"];
          

                    [newData addObject:model];
                    
                }
                NSLog(@"newData=%lu",(unsigned long)newData.count);
                
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                
                
                [courseTable reloadData];
                
                
            }
        }
        else{
            currentPage--;
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}


#pragma mark---TableView
//行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

//点击Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
      [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_editing) {
        TeachingAidsModel *model=dataArray[indexPath.row];
        
        model.isCheck=!model.isCheck;
        
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        
        if (model.isCheck == 0) {
            [NewArr removeObject:model];
            
            for (int s=0; s<seleArr.count; s++) {
                NSMutableDictionary *doc = seleArr[s];
                if ([doc[@"modelId"] intValue] == model.ModelId.intValue) {
                        [seleArr removeObjectAtIndex:s];
                    }
                }
            NSLog(@"seleArr 取消:%@",seleArr);
            NSLog(@"NewArr  取消:%@",NewArr);
            
            
        }else if(model.isCheck == 1){
            IndexPath =indexPath;
            [self setAddView:model IndexPath:indexPath];
        }
        
        
       
    }
    
}

-(void)setAddView:(TeachingAidsModel *)model IndexPath:(NSIndexPath *)indexPath{
    
   ChildrenView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
   ChildrenView.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.4];

    FirstView=[[UIView alloc]initWithFrame:CGRectMake(Swidth/2-150, Sheight/2-150, 300, 200)];
    FirstView.backgroundColor=[UIColor colorWithRed:220 green:220 blue:220 alpha:0.4];
    FirstView.layer.cornerRadius=15;
    
    UILabel *TitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(FirstView.wwy_width/2-60, 20, 120, 20)];
    TitleLabel.text=@"添加教具详情";
    
   
    
    Tybtn=[UIButton buttonWithType:UIButtonTypeCustom];
    Tybtn.frame=CGRectMake(0, 60, FirstView.wwy_width, 45);
    Tybtn.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    [Tybtn setTitle:@"请选择教具使用类型" forState:UIControlStateNormal];
    [Tybtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    Tybtn.layer.cornerRadius=15;
    [Tybtn addTarget:self action:@selector(ChooseType:) forControlEvents:UIControlEventTouchUpInside];
    
    Comments=[[UITextField alloc]initWithFrame:CGRectMake(0, 112, FirstView.wwy_width, 45)];
    Comments.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.2];
    Comments.borderStyle=UITextBorderStyleRoundedRect; //边框样式
    Comments.clearButtonMode = UITextFieldViewModeWhileEditing;
    Comments.keyboardType=UIKeyboardTypeDefault;
    Comments.returnKeyType=UIReturnKeyDefault;
    Comments.layer.cornerRadius=20;
    Comments.delegate=self;
    Comments.placeholder=@"请输入备注";
   [Comments addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
    UIView *Av=[[UIView alloc]initWithFrame:CGRectMake(0, Tybtn.wwy_y-3, FirstView.wwy_width, 1)];
    Av.backgroundColor=[UIColor lightGrayColor];
    
    UIView *Bv=[[UIView alloc]initWithFrame:CGRectMake(0, Comments.wwy_y+Comments.wwy_height+3, FirstView.wwy_width, 1)];
    Bv.backgroundColor=[UIColor lightGrayColor];
    
    UIView *Cv=[[UIView alloc]initWithFrame:CGRectMake(0, Tybtn.wwy_y+Tybtn.wwy_height+3, FirstView.wwy_width, 1)];
    Cv.backgroundColor=[UIColor lightGrayColor];
    
    
    UIButton *CanelBtn=[UIButton buttonWithType:UIButtonTypeSystem];
    CanelBtn.frame=CGRectMake(FirstView.wwy_width/2, Bv.wwy_y+Bv.wwy_height+3,FirstView.wwy_width/2, 40);
    [CanelBtn setTitle:@"确定" forState:UIControlStateNormal];
    [CanelBtn addTarget:self action:@selector(ChildrenViewAction) forControlEvents:UIControlEventTouchUpInside];
   
    UIButton *NoBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    NoBtn.frame=CGRectMake(0, Bv.wwy_y+Bv.wwy_height+3,FirstView.wwy_width/2, 40);
    [NoBtn setTitle:@"取消" forState:UIControlStateNormal];
    [NoBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [NoBtn addTarget:self action:@selector(NotAction) forControlEvents:UIControlEventTouchUpInside];
    
    [FirstView addSubview:TitleLabel];
    [FirstView addSubview:Comments];
    [FirstView addSubview:Tybtn];
    
    
    [FirstView addSubview:Cv];
    [FirstView addSubview:Bv];
    [FirstView addSubview:Av];
    [FirstView addSubview:CanelBtn];
    [FirstView addSubview:NoBtn];
    [ChildrenView addSubview:FirstView];
   [[UIApplication sharedApplication].keyWindow addSubview:ChildrenView];
}

-(void)NotAction{
    ChildrenView.hidden=YES;
    FirstView.hidden=YES;
    [Comments resignFirstResponder];
}

-(void)ChildrenViewAction{
    
    if ([CommentsStr isKindOfClass:[NSNull class]] || CommentsStr.length == 0 ) {
        CommentsStr=@"";
    }

    if(UseForStr.length == 0 ){
             ChildrenView.hidden=YES;
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"教具使用类型未被填写，无法正常创建添加教具！！！" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            ChildrenView.hidden=NO;
        }];

        [alert addAction:okAction];
        [self presentViewController:alert animated:true completion:nil];
        
    }else{
        ChildrenView.hidden=YES;
        FirstView.hidden=YES;
        [Comments resignFirstResponder];
    TeachingAidsModel *model=dataArray[IndexPath.row];
    
    [NewArr addObject:model];
    
    NSMutableDictionary *Params=[NSMutableDictionary dictionary];
    [Params setObject:_PlanModel.PlanNameId forKey:@"courseId"];
    [Params setObject:model.ModelId forKey:@"modelId"];
    [Params setObject:model.ModelItemsNum forKey:@"amount"];
    
    
    [Params setObject:CommentsStr forKey:@"comments"];
    [Params setObject:UseForStr forKey:@"useFor"];
    
    
    [seleArr addObject:Params];
    
    
    NSLog(@"selectorPatnArray 点击:%@",seleArr);
    NSLog(@"Params 点击:%@",Params);
    
    }
    
    
}


-(void)ChooseType:(id)sender{
     [Comments resignFirstResponder];

    picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0,Sheight-Sheight/3, Swidth, Sheight/3)];
    ToolView=[[UIView alloc]initWithFrame:CGRectMake(0,picker.wwy_y-30, Swidth, 30)];
    ToolView.backgroundColor=UIColorFromHex(0x20B2AA);
    [ChildrenView addSubview:ToolView];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn:) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:noBtn];
    
    
    
    picker.backgroundColor=[UIColor whiteColor];
    picker.dataSource=self;
    picker.delegate=self;
    
    PickDate=[[NSMutableArray alloc]initWithObjects:@"请选择教具用途",@"示教",@"学生练习" ,nil];
    PickDateID=[[NSMutableArray alloc]initWithObjects:@"请输入教具用途",@"for_teach",@"for_student" ,nil];
    
    UseForStr=PickDateID[1];
    
    [ChildrenView addSubview:picker];
    

}

-(void)setOkBtn:(id)sender{
    [Tybtn setTitle:StrForUser forState:UIControlStateNormal];
    picker.hidden=YES;
    ToolView.hidden=YES;
}

-(void)setNoBtn{
    picker.hidden=YES;
    ToolView.hidden=YES;
}


-(void)ApplyTextDidChange:(UITextField *)theTextField{
    
    
    
    NSLog(@"%@",theTextField.text);
    CommentsStr=theTextField.text;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [Comments resignFirstResponder];
}


//加载的Cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TeachingAidsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"AidsCell"];
    TeachingAidsModel *model=dataArray[indexPath.row];
    [cell setModelAids:model];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130.f;
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
//}
//
//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    //从选中中取消
//    TeachingAidsModel *model=dataArray[indexPath.row];
//
//    [NewArr removeObject:model];
//
//    for (int s=0; s<seleArr.count; s++) {
//        NSMutableDictionary *doc = seleArr[s];
//        if ([doc[@"modelId"] intValue] == model.ModelId.intValue) {
//            [seleArr removeObjectAtIndex:s];
//        }
//    }
//
//    NSLog(@"seleArr 取消:%@",seleArr);
//    NSLog(@"NewArr  取消:%@",NewArr);
//
//}
/*
 [courseTable setEditing:NO animated:YES];
 rightBtn.hidden=NO;
 SaveBtn.hidden=YES;
 */


// /courses/{courseId}/requiredTrainingAids
-(void)save{
    
  NSString *PlanId=[NSString stringWithFormat:@"%@",_PlanModel.PlanNameId];

    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/requiredTrainingAids",LocalIP,PlanId];
    
    
    NSLog(@"%@",Url);
    NSLog(@"seleArr:%@",seleArr);
    
    [seleArr addObjectsFromArray:_TempArr];
    
    [RequestTools RequestWithURL:Url Method:@"post" Params:seleArr Success:^(NSDictionary *result) {
        NSLog(@"result:%@",result);
        
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            [MBProgressHUD showToastAndMessage:@"添加教具成功!" places:0 toView:nil];
            _backArr(NewArr);
            [seleArr removeAllObjects];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        
    } failed:^(NSString *result) {
        NSLog(@"result:%@",result);
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
   
    
}

#pragma mark PickerView
//返回的列数：
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

//数据个数：
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [PickDate count];
}

//获取数据
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [PickDate objectAtIndex:row];
}

//返回值：
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"==:%@",[PickDate objectAtIndex:row]);
    StrForUser=[PickDate objectAtIndex:row];
    UseForStr=[PickDateID objectAtIndex:row];
    NSLog(@"UseForStr:%@",UseForStr);
   
}


@end
