//
//  GroupController.h
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupCell.h"
typedef void (^GroupBlock)(NSArray *resultArray);
@interface GroupController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)GroupBlock groupBlock;
@end
