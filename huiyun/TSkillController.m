//
//  SkillDetailController.m
//  xiaoyun
//
//  Created by MacAir on 17/2/28.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TSkillController.h"

@interface TSkillController ()
{
    UITableView *skillTable;
    NSArray *Time;
    NSArray *Location;
   
}
@end

@implementation TSkillController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    [self setNav];
    [self createUI];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"技能详情";
    [backNavigation addSubview:titleLab];
}
- (void)createUI{
    //
    skillTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight) style:UITableViewStylePlain];
    skillTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    skillTable.delegate = self;
    skillTable.dataSource = self;
    skillTable.backgroundColor = UIColorFromHex(0xf5f5f5);
    [self.view addSubview:skillTable];
    //注册cell
    [skillTable registerNib:[UINib nibWithNibName:@"osceDetailCellStyle1" bundle:nil] forCellReuseIdentifier:@"style1"];
}

-(void)loadData{

    NSString *url=[NSString stringWithFormat:@"%@/skillTests/%@/selectedStations",LocalIP,_model.skillID];
    
    NSString *TimeUrl=[NSString stringWithFormat:@"%@/skillTests/%@/timeBlocks",LocalIP,_model.skillID];
    
    
    [RequestTools RequestWithURL:url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        Location = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
        [skillTable reloadData];
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
    }];
    
    [RequestTools RequestWithURL:TimeUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
       Time = [result objectForKey:@"responseBody"];
        [skillTable reloadData];
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
    }];
}

#pragma -action
- (void)back:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -protocol
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 40)];
    view.backgroundColor = UIColorFromHex(0xf5f5f5);
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 5;
    }else{
        return 2;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                if (self.model.timeBlock.intValue == 0) {
                    [MBProgressHUD showToastAndMessage:@"暂无时间段~" places:0 toView:nil];
                    return;
                }
                
                break;
            case 1:
                
                break;
            default:
                break;
        }
    }else if(indexPath.section == 1){
        if (indexPath.row == 0) {
            [self TimeAction];
        }else if(indexPath.row == 1){
            [self Nextaction];
        }
    }
}

-(void)TimeAction{
    TSkillTestCenterTimeVc *vc=[TSkillTestCenterTimeVc new];
    vc.model=_model;
    [self.navigationController pushViewController:vc animated:YES];
}



-(void)Nextaction{
    TSkillTestCenterViewController *vc=[TSkillTestCenterViewController new];
    vc.testId=[NSString stringWithFormat:@"%@", self.model.skillID];
    [self.navigationController pushViewController:vc animated:YES];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    osceDetailCellStyle1 *cell;
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"style1"];
        if (indexPath.row == 0) {
            cell.biaoti.text = @"考试名称:";
            cell.content.text = [self.model skillName];
        }else if (indexPath.row == 1){
            cell.biaoti.text = @"考试时间:";
            cell.content.text = [[Maneger shareObject] timeFormatter:[self.model skillStartTime].stringValue];
        }else if (indexPath.row == 2){
            cell.biaoti.text = @"考试分类:";
            cell.content.text = [self.model skillCategory];
        }else if (indexPath.row == 3){
            cell.biaoti.text = @"考试科目:";
            cell.content.text = [self.model skillSubject];
        }else if (indexPath.row == 4){
            cell.biaoti.text = @"考试说明:";
            cell.content.text = [self.model skillDes];
        }
        return cell;
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"style1"];
        if (indexPath.row == 0) {
            cell.biaoti.text = @"时间段:";
            cell.content.text = [NSString stringWithFormat:@"%lu",Time.count];
            cell.imgShow.image=[UIImage imageNamed:@"Right"];
            cell.imgShow.hidden=NO;
        }else if (indexPath.row == 1){
            cell.biaoti.text = @"站点数:";
            cell.content.text = [NSString stringWithFormat:@"%lu",Location.count];
            cell.imgShow.image=[UIImage imageNamed:@"Right"];
             cell.imgShow.hidden=NO;
        }
        return cell;
    }
}

@end
