//
//  ExamViewController.m
//  huiyun
//
//  Created by 王慕铁 on 2018/2/25.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "ExamViewController.h"

@interface ExamViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UICollectionView *MainCollectionVc;
    
    NSMutableArray *dataArray;
}

@end

@implementation ExamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self setNav];
    [self setLoadData];
    [self setUI];
    
   
    
    
}

-(void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    if ([_str isEqualToString:@"课前"]) {
        titleLab.text = @"课前作业";
    }else  if ([_str isEqualToString:@"课后"]) {
        titleLab.text = @"课后作业";
    }
    
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [rightBtn setTitle:@"查看" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(Answerboard:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    [backNavigation addSubview:titleLab];
    
    dataArray=[NSMutableArray new];
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)Answerboard:(UIButton *)sender{
    
}

//http://www.hzwitwin.cn:81/witwin-ctts-web/courses/ courseId /questions/?courseQuestionType=preview_question&pageSize=999  课前
//http://www.hzwitwin.cn:81/witwin-ctts-web/courses/ courseId /questions/?courseQuestionType=homework_question&pageSize=999 课后
-(void)setLoadData{
    NSString *Url;
    if ([_str isEqualToString:@"课前"]) {
         Url=[NSString stringWithFormat:@"%@/courses/%@/questions/?courseQuestionType=preview_question&pageSize=999&showAnswers=true",LocalIP,_courseId];
    }else if([_str isEqualToString:@"课后"]){
        Url=[NSString stringWithFormat:@"%@/courses/%@/questions/?courseQuestionType=homework_question&pageSize=999&showAnswers=true",LocalIP,_courseId];
    }
    
    NSLog(@"URL==%@",Url);
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
    
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            
            return;
        }
        [dataArray removeAllObjects];
        
        if([result[@"responseStatus"] isEqualToString:@"succeed"]){
            
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无题目!" andCurentVC:self];
                
            }else{
                
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    QuestionModel *model=[QuestionModel new];
                    model.questionType=[dictionary objectForKey:@"questionType"];
                    model.questionId=[dictionary objectForKey:@"questionId"];
                    model.questTitle=[dictionary objectForKey:@"questionTitle"];
                    model.choiceOptions=[dictionary objectForKey:@"choiceOptions"];
                    model.answerArray=[NSMutableArray new];
                    
                    NSArray *tempArr=[dictionary objectForKey:@"answersBean"];

                    if (tempArr.count > 0) {
                        for (int j=0; j<tempArr.count; j++) {
                            NSDictionary *dic=[tempArr objectAtIndex:j];
                            model.answer=[dic objectForKey:@"answer"];
                            
                            [model.answerArray addObject: model.answer];
                        }
                    }
                   
                    
                    
                 //   NSLog(@"tempArr=%lu",(unsigned long)tempArr.count);
                    [dataArray addObject:model];
                    
                   
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [MainCollectionVc reloadData];
               
            }
        }else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
        
        
        
    } failed:^(NSString *result) {
        
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
           
        }
        return;
        
    }];
    
   
}



-(void)setUI{
    self.view.backgroundColor=[UIColor whiteColor];
    
    
    //1.初始化：
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc]init];
    layout.scrollDirection=UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    //设置iteamSize:
    layout.itemSize=CGSizeMake(Swidth, Sheight-64);
    
    //4.初始化 CollectionView
    MainCollectionVc=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) collectionViewLayout:layout];
    MainCollectionVc.pagingEnabled=YES;
    MainCollectionVc.scrollsToTop=NO;
    MainCollectionVc.showsHorizontalScrollIndicator=NO;
    
    MainCollectionVc.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:MainCollectionVc];
    
    //5.注册Cell

    
    [MainCollectionVc registerClass:[MyCollectionCell class] forCellWithReuseIdentifier:@"cellId"];

    [MainCollectionVc registerClass:[MyTwoCollectionCell class] forCellWithReuseIdentifier:@"cellIds"];


    MainCollectionVc.delegate=self;
    MainCollectionVc.dataSource=self;
    
    
}

//每个section 的iteam 个数：
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return dataArray.count;
}

//if([model.questionType isEqualToString:@"a3_choice_question"] || [model.questionType isEqualToString:@"a4_choice_question"] || [model.questionType isEqualToString:@"b1_choice_question"])

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    QuestionModel *model=dataArray[indexPath.row];
     NSLog(@"tempArr=%@",model.answerArray);
    
    NSLog(@"%@-%ld",model.questionType,indexPath.row);
     MyCollectionCell *cell = (MyCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellId" forIndexPath:indexPath];
    
     MyTwoCollectionCell *cells = (MyTwoCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIds" forIndexPath:indexPath];
    
    
    if ([model.questionType isEqualToString:@"A1选择题"] ||[model.questionType isEqualToString:@"A2题型"] || [model.questionType isEqualToString:@"X: 多选题"]) {

        if (cell == nil) {
            cell=[MyCollectionCell new];
        }
    
        [cell setQuestion:model];
        
       
        
        return cell;
    }else {

        if (cells == nil) {
           cells=[MyTwoCollectionCell new];
        }
        
        [cells setQuestions:model];
        
        return cells;
    }
    
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(Swidth, Sheight-64);
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
