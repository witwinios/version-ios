//
//  MenuView.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/31.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^myBackBlock)(id result,NSString *dieaseId);
@interface DiseaseView : UIView<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)UITableView *menuTable;
@property (strong, nonatomic)NSMutableArray *listArray;
@property (strong, nonatomic)NSString *url;
@property (strong, nonatomic)myBackBlock selectBlock;
@end


