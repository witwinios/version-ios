//
//  PopViewController.h
//  UIPresentationVCtest
//
//  Created by Aotu on 16/4/11.
//  Copyright © 2016年 Aotu. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^xBlock)(NSString *result);
@interface PopView : UIViewController
@property(copy,nonatomic) xBlock  indexBlock;
@end
