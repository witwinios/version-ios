//
//  TypicalCasesDetailsModel.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/12.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TypicalCasesDetailsModel : NSObject

@property(strong,nonatomic)NSString *RecordType;
@property(strong,nonatomic)NSNumber *RecordTime;
@property(strong,nonatomic)NSString *RecordDescription;

@property(strong,nonatomic)NSMutableArray *RecordFile;
@property(strong,nonatomic)NSString *RecordFileFormat;
@property(strong,nonatomic)NSString *RecordFileName;
@property(strong,nonatomic)NSString *RecordUrl;

@end
