//
//  TCourseAddModel.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCourseAddModel : NSObject


@property(strong,nonatomic)NSString *PlanName; //教案名称
@property(strong,nonatomic)NSNumber *PlanNameId;   //课程Id

@property(nonatomic,strong)NSString *PlanClassify; //教案分类
@property(nonatomic,strong)NSNumber *PlanClassifyId;  //课程科目Id

@property(strong,nonatomic)NSString *PlanCourse; //教案科目
@property(strong,nonatomic)NSNumber *PlanCourseId; //教案科目Id

@property (strong,nonatomic)NSString *PlanOwner; //教案所有者
@property(strong,nonatomic)NSNumber *PlanOwnerId; //教案所有者id

@property(nonatomic,strong) NSString *PlanGenre; //教案类型
@property(nonatomic,strong) NSString *planGenreId; //教案类型ID

@property(nonatomic,strong) NSString *PlanDescription; //教案简介
@property(nonatomic,strong) NSNumber *PlanPublic; //教案是否公开

@property(nonatomic,strong) NSString *CourseStatus; //教案状态

@property(nonatomic,strong)NSString *PlanCourseStatus;


@end
