//
//  MyButton.h
//  yun
//
//  Created by MacAir on 2017/6/13.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyButton : UIButton
@property (strong, nonatomic) NSString *questionIndex;
@property (strong, nonatomic) NSString *answerIndex;
@end
