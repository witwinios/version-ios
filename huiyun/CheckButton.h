//
//  CheckButton.h
//  yun
//
//  Created by MacAir on 2017/8/23.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckButton : UIButton

@property (strong, nonatomic) NSString *answerStr;
@property (strong, nonatomic) QuestionModel *quesModel;
@end
