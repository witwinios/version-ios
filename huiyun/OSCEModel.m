//
//  OSCEModel.m
//  xiaoyun
//
//  Created by MacAir on 17/1/3.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "OSCEModel.h"

@implementation OSCEModel
- (void)setOsceName:(NSString *)osceName{
    if ([osceName isKindOfClass:[NSNull class]]) {
        _osceName = @"暂无";
    }else{
        _osceName = osceName;
    }
}
- (void)setOsceStatus:(NSString *)osceStatus{
    if ([osceStatus isEqualToString:@"planning"]||[osceStatus isEqualToString:@"PLANNING"]) {
            _osceStatus = @"计划中";
    }else if ([osceStatus isEqualToString:@"started"]||[osceStatus isEqualToString:@"STARTED"]){
            _osceStatus = @"已开始";
    }else if ([osceStatus isEqualToString:@"completed"]||[osceStatus isEqualToString:@"COMPLETED"]){
            _osceStatus = @"已完成";
    }else if ([osceStatus isEqualToString:@"released"]||[osceStatus isEqualToString:@"RELEASED"]){
            _osceStatus = @"已发布";
    }else if ([osceStatus isEqualToString:@"preordered"]||[osceStatus isEqualToString:@"PREORDERED"]){
            _osceStatus = @"预排";
    }else if ([osceStatus isEqualToString:@"arranged"]||[osceStatus isEqualToString:@"ARRANGED"]){
            _osceStatus = @"已安排";
    }
}
- (void)setOsceTime:(NSNumber *)osceTime{
    if ([osceTime isKindOfClass:[NSNull class]]) {
        _osceTime = [NSNumber numberWithInt:1];
    }else{
        _osceTime = osceTime;
    }
}
- (void)setOsceCategory:(NSString *)osceCategory{
    if ([osceCategory isKindOfClass:[NSNull class]]) {
        _osceCategory = @"暂无";
    }else{
        _osceCategory = osceCategory;
    }
}
- (void)setOsceContent:(NSString *)osceContent{
    if ([osceContent isKindOfClass:[NSNull class]]) {
        _osceContent = @"暂无";
    }else{
        _osceContent = osceContent;
    }
}
- (void)setStationName:(NSString *)stationName{
    if ([stationName isKindOfClass:[NSNull class]]) {
        _stationName = @"暂无";
    }else{
        _stationName = stationName;
    }
}
- (void)setTotalScore:(NSNumber *)totalScore{
    if ([totalScore isKindOfClass:[NSNull class]]) {
        _totalScore = [NSNumber numberWithInt:0];
    }else{
        _totalScore = totalScore;
    }
}
- (void)setOsceStationNums:(NSNumber *)osceStationNums{
    if ([osceStationNums isKindOfClass:[NSNull class]]) {
        _osceStationNums = [NSNumber numberWithInt:0];
    }else{
        _osceStationNums = osceStationNums;
    }
}
- (void)setCreateTime:(NSNumber *)createTime{
    if ([createTime isKindOfClass:[NSNull class]]) {
        _createTime = 0;
    }else{
        _createTime = createTime;
    }
}
- (void)setSubjectName:(NSString *)subjectName{
    if ([subjectName isKindOfClass:[NSNull class]]) {
        _subjectName = @"暂无";
    }else{
        _subjectName = subjectName;
    }
}
- (void)setCreateName:(NSString *)createName{
    if ([createName isKindOfClass:[NSNumber class]]) {
        _createName = @"暂无";
    }else{
        _createName = createName;
    }
}
- (void)setDifficulty:(NSNumber *)difficulty{
    if ([difficulty isKindOfClass:[NSNull class]]||[_difficulty isEqual:@"null"]) {
        _difficulty = [NSNumber numberWithInt:-1];
    }else{
        _difficulty = difficulty;
    }
}
- (void)setDes:(NSString *)des{
    if ([des isKindOfClass:[NSNull class]]) {
        _des = @"暂无";
    }else{
        _des = des;
    }
}
#pragma -mark
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.osceName forKey:@"osceName"];
    [aCoder encodeObject:self.osceContent forKey:@"osceContent"];
    [aCoder encodeObject:self.osceTime forKey:@"osceTime"];
    [aCoder encodeObject:self.stationName forKey:@"stationName"];
    [aCoder encodeObject:self.type forKey:@"type"];
    [aCoder encodeObject:self.difficulty forKey:@"difficulty"];
    [aCoder encodeObject:self.subjectName forKey:@"subjectName"];
    [aCoder encodeObject:self.createTime forKey:@"createTime"];
    [aCoder encodeObject:self.createName forKey:@"createName"];
    [aCoder encodeObject:self.des forKey:@"des"];
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.osceName = [aDecoder decodeObjectForKey:@"osceName"];
        self.osceContent = [aDecoder decodeObjectForKey:@"osceContent"];
        self.osceTime = [aDecoder decodeObjectForKey:@"osceTime"];
        self.stationName = [aDecoder decodeObjectForKey:@"stationName"];
        self.type = [aDecoder decodeObjectForKey:@"type"];
        self.difficulty = [aDecoder decodeObjectForKey:@"difficulty"];
        self.subjectName = [aDecoder decodeObjectForKey:@"subjectName"];
        self.createTime = [aDecoder decodeObjectForKey:@"createTime"];
        self.createName = [aDecoder decodeObjectForKey:@"createName"];
        self.des = [aDecoder decodeObjectForKey:@"des"];
    }
    return self;
}
@end
