//
//  CellStyle1.m
//  yun
//
//  Created by MacAir on 2017/6/22.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "CellStyle1.h"

@implementation CellStyle1

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leaveStatus.layer.cornerRadius = 5;
    self.leaveStatus.layer.masksToBounds = YES;
    self.leaveStatus.layer.borderColor = [UIColor grayColor].CGColor;
    self.leaveStatus.layer.borderWidth = 1;
    if ([self.leaveStatus.text isEqualToString:@"同意"]) {
        self.leaveStatus.backgroundColor = [UIColor greenColor];
    }else if ([self.leaveStatus.text isEqualToString:@"待审批"]){
        self.leaveStatus.backgroundColor = [UIColor orangeColor];
    }else{
        self.leaveStatus.backgroundColor = [UIColor redColor];
    }
    //
    self.leaveTime.adjustsFontSizeToFitWidth = YES;
}
@end
