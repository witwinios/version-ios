//
//  AbsenceItem.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/14.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AbsenceItem : NSObject
@property (strong, nonatomic) NSString *itemType;
@property (strong, nonatomic) NSNumber *itemId;
@property (strong, nonatomic) NSString *itemName;
@property (strong, nonatomic) NSNumber *itemStartTime;
@property (strong, nonatomic) NSNumber *itemEndTime;
@end
