//
//  ApplyStudentViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/5.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "ApplyStudentViewController.h"
#import "ApplyStudentCell.h"
#import "TCourseDetailController.h"
#import "ApplyAddStudentViewController.h"
@interface ApplyStudentViewController ()
{
    UITableView *courseTable;
    NSMutableArray *dataArray;
    NSMutableArray *selectorPatnArray;//存放选中数据
    
    UIAlertController *AlertView;
    UIButton *rightBtn;
    
    UIButton *Delebtn;
    
    NSString *StudentId;
    
    UIButton *DoubleDelete;
    UIButton *AllDelete;
    UIView *DeleView;
    
    NSString *row;
    
    NSMutableArray *rowIdArray;
    NSInteger *rowId;
    
    NSIndexPath *Data;
    NSIndexPath *indexData;
    NSArray *arr;
    
    double CurrentTimeNunm;
    
    
    UIView *SearchView;
    UITextField *SearchField;
    UIButton *SearchBtn;
    UIImageView *backNavigation;
    NSMutableArray *selectArray;
    BOOL isSearch;
}

@end

@implementation ApplyStudentViewController


-(void)viewWillAppear:(BOOL)animated{
    [self setUpRefresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setsearchBar];
    [self setNav];
    
    [self setUI];
    
    [self setUpRefresh];
    [self loadData];
    [self SetNewTime];
}

-(void)setsearchBar{
    
    
    isSearch=NO;
    
    SearchView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 44)];
    SearchView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    UIImageView *SearchImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sousuo"]];
    SearchImg.backgroundColor=[UIColor clearColor];
    SearchImg.frame=CGRectMake(10, 6, 32, 32);
    
    
    SearchField=[[UITextField alloc]initWithFrame:CGRectMake(44, 6, Swidth-88, 32)];
    SearchField.backgroundColor=[UIColor clearColor];
    SearchField.placeholder=@"请输入搜索的学生名字";
    SearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    SearchField.clearsOnBeginEditing = YES;
    SearchField.keyboardType=UIKeyboardTypeDefault;
    SearchField.returnKeyType=UIReturnKeySearch;
    SearchField.delegate = self;
    [SearchField addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    SearchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SearchBtn.frame=CGRectMake((SearchField.wwy_x+SearchField.wwy_width)+2, 6, 40, 32);
    [SearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [SearchBtn addTarget:self action:@selector(setUpRefresh) forControlEvents:UIControlEventTouchUpInside];
    SearchBtn.backgroundColor=UIColorFromHex(0x20B2AA);
    SearchBtn.layer.cornerRadius=5;
    SearchBtn.hidden=YES;
    [SearchView addSubview:SearchBtn];
    [SearchView addSubview:SearchField];
    [SearchView addSubview:SearchImg];
    [self.view addSubview:SearchView];
    
}

-(BOOL)ApplyTextDidChange:(UITextField *)theTextField{
    
    NSLog( @"text changed: %@", theTextField.text);
    
    
    if (![theTextField.text isEqualToString:@""]) {
        SearchBtn.hidden=NO;
        isSearch=YES;
    }else{
        isSearch=NO;
        [self setUpRefresh];
        SearchBtn.hidden=YES;
        [SearchField  resignFirstResponder];
    }
    
    return YES;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [SearchField  resignFirstResponder];
    
    
    [self setUpRefresh ];
    
    
    
    NSLog(@"点击了搜索");
    
    return YES;
    
}



-(void)SetNewTime{
    //获取当前时间
    NSDate *datenow = [NSDate date];
    
    NSString* currentTime=[NSString stringWithFormat:@"%0.f",[datenow timeIntervalSince1970]*1000];
    
 
   
    
    CurrentTimeNunm =[currentTime doubleValue];
}





#pragma mark 导航栏：
- (void)setNav{
   backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"报名学生";
    [backNavigation addSubview:titleLab];
    
    
    
    
    if([_scheduleStatusString isEqualToString:@"已发布"]){
        rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame = CGRectMake(self.view.frame.size.width-80, 29.5, 100, 25);
        [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];

        [rightBtn addTarget:self action:@selector(Edit:) forControlEvents:UIControlEventTouchUpInside];
        [backNavigation addSubview:rightBtn];
    
    
         Delebtn=[UIButton buttonWithType:UIButtonTypeCustom];
         Delebtn.frame=CGRectMake(self.view.frame.size.width-80, 29.5, 100, 25);
         [Delebtn setTitle:@"返回" forState:UIControlStateNormal];
         [Delebtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         Delebtn.hidden=YES;
         [Delebtn addTarget:self action:@selector(DeleteStudent) forControlEvents:UIControlEventTouchUpInside];
         [backNavigation addSubview:Delebtn];

    }
    
   
    
    dataArray=[NSMutableArray new];

}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)Edit:(UIButton *)sender{
    double Num=[_model.startTime doubleValue];
    double sign=[_model.signendTime doubleValue];
 
    
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"学生管理" message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *AddAction = [UIAlertAction actionWithTitle:@"新增" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        NSLog(@"signstartTime：%@",_model.signstartTime);
        NSLog(@"startTime：%@",_model.startTime);
        NSLog(@"CurrentTimeNunm：%0.f",CurrentTimeNunm);
        

        
        if(sign == 0){
            if (CurrentTimeNunm > Num) {
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"已超过报名时间，无法报名！！" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                ApplyAddStudentViewController *vc =[ApplyAddStudentViewController new];
                vc.scheduleIdString=_scheduleIdString;
                vc.scheduleStatusString=_scheduleStatusString;
                
//                vc.backArr = ^(NSArray *addArr){
//                    dataArray = [NSMutableArray arrayWithArray:addArr];
//                    NSLog(@"%@",dataArray);
//                    for (ApplyStudent *models in addArr) {
//                        NSLog(@"姓名=%@",models.ApplyStudentName);
//                    }
//                    [courseTable reloadData];
  //              };
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else if(CurrentTimeNunm > sign){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"已超过报名时间，无法报名！！" preferredStyle:UIAlertControllerStyleAlert];
            
                            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
                            }];
            
                            [alert addAction:okAction];
                            [self presentViewController:alert animated:YES completion:nil];
        }else {
            ApplyAddStudentViewController *vc =[ApplyAddStudentViewController new];
            vc.scheduleIdString=_scheduleIdString;
            vc.scheduleStatusString=_scheduleStatusString;
            
            vc.backArr = ^(NSArray *addArr){
                dataArray = [NSMutableArray arrayWithArray:addArr];
                NSLog(@"%@",dataArray);
                for (ApplyStudent *models in addArr) {
                    NSLog(@"姓名=%@",models.ApplyStudentName);
                }
                [courseTable reloadData];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }

    }];
    
    UIAlertAction *EditAction = [UIAlertAction actionWithTitle:@"修改" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([rightBtn.titleLabel.text isEqualToString:@"编辑"]) {
            
            //
            
            Delebtn.hidden=NO;
            rightBtn.hidden=YES;
            
            [courseTable setEditing:YES animated:YES];
            
            DeleView=[[UIView alloc]initWithFrame:CGRectMake(0, Sheight-44, Swidth, 44)];
            //  DeleView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.5];
            DeleView.backgroundColor=UIColorFromHex(0x20B2AA);
            
            
            AllDelete=[UIButton buttonWithType:UIButtonTypeCustom];
            AllDelete.frame=CGRectMake(0, 0, Swidth/2, 44);
            [AllDelete setTitle:@"全选" forState:UIControlStateNormal];
            [AllDelete addTarget:self action:@selector(AllDelete:) forControlEvents:UIControlEventTouchUpInside];
            AllDelete.layer.borderColor=[UIColor whiteColor].CGColor;
            AllDelete.layer.borderWidth=1;
            
            
            DoubleDelete=[UIButton buttonWithType:UIButtonTypeCustom];
            DoubleDelete.frame=CGRectMake(Swidth/2, 0, Swidth/2, 44);
            [DoubleDelete setTitle:@"删除" forState:UIControlStateNormal];
            [DoubleDelete addTarget:self action:@selector(DoubleDelete:) forControlEvents:UIControlEventTouchUpInside];
            DoubleDelete.layer.borderColor=[UIColor whiteColor].CGColor;
            DoubleDelete.layer.borderWidth=1;
            
            [DeleView addSubview:AllDelete];
            [DeleView addSubview:DoubleDelete];
            [self.view addSubview:DeleView];
            
            [courseTable setEditing:YES animated:YES];
            
            
        }
        
    }];
    
    
   
    

    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

    }];

    [AlertView addAction:AddAction];
    [AlertView addAction:EditAction];
 
    [AlertView addAction:BackAction];
   
    

    [self presentViewController:AlertView animated:YES completion:nil];
}

-(void)DeleteStudent{
    Delebtn.hidden=YES;
    rightBtn.hidden=NO;
    DeleView.hidden=YES;
    [courseTable setEditing:NO animated:YES];

}



- (void)setUpRefresh{
    
    
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
   
 
}


-(void)loadData{
    NSLog(@"scheduleIdString:%@",_scheduleIdString);
    NSString *URL;
    
    if (isSearch == NO) {
         URL =[NSString stringWithFormat:@"%@/courseSchedules/%@/registeredStudents?pageSize=999",LocalIP,_scheduleIdString];
    }else if(isSearch == YES){
        NSString *str=SearchField.text;
         URL =[NSString stringWithFormat:@"%@/courseSchedules/%@/registeredStudents?pageSize=999&fullName=%@",LocalIP,_scheduleIdString,str];
        URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    
  
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无学生!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    ApplyStudent *model=[ApplyStudent new];
                    model.row = [NSNumber numberWithInt:i];
                    model.ApplyStudentName=[dictionary objectForKey:@"fullName"];
                    model.ApplyStudentID=[dictionary objectForKey:@"userId"];
                    
                    
                    if(![[dictionary objectForKey:@"picture"] isKindOfClass:[NSNull class]]){
                        
                        
                        model.ApplyStudentImg=[[dictionary objectForKey:@"picture"] objectForKey:@"fileUrl"];
                        
                        NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.ApplyStudentImg];
                        
                        model.ApplyStudentImg=url;
                        
                    }
                    [dataArray addObject:model];
              
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
           
            }
        }else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        if (![result isEqualToString:@"200"]) {
            [courseTable headerEndRefreshing];
        }
        return;
    }];
 
    
}


-(void)setUI{
    CGFloat courseTable_Y=backNavigation.wwy_height+SearchView.wwy_height;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, courseTable_Y,Swidth, Sheight-courseTable_Y) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleNone;

    
    [courseTable registerNib:[UINib nibWithNibName:@"ApplyStudentCell" bundle:nil] forCellReuseIdentifier:@"applyCell"];
    [self.view addSubview:courseTable];
    selectorPatnArray=[NSMutableArray array];
    rowIdArray=[NSMutableArray array];
     isSearch = NO;
}

//返回行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

//返回行高：
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 95;
}

//点击行事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ApplyStudent *model=dataArray[indexPath.row];
    [selectorPatnArray addObject:model];
    [rowIdArray addObject:[NSNumber numberWithInteger:indexPath.row]];
}

//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ApplyStudentCell *cell=[tableView dequeueReusableCellWithIdentifier:@"applyCell"];
   // cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    if(cell == nil){
        cell=[[ApplyStudentCell alloc]initWithStyle:UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert reuseIdentifier:@"applyCell" ];
    }
    ApplyStudent *model=dataArray[indexPath.row];
    [cell setProperty:model];
   
    return cell;
    
    
}

//编辑模式：
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;


}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    return UITableViewCellEditingStyleDelete ;
//
//
//}



- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    //从选中中取消
    if (selectorPatnArray.count > 0) {
        ApplyStudent *model=dataArray[indexPath.row];
        
        [selectorPatnArray removeObject:model];
        [rowIdArray removeObject:indexPath];
        
        
        
        NSLog(@"selectorPatnArray 取消:%@",selectorPatnArray);
        NSLog(@"rowIdArray 取消:%@",rowIdArray);
    }
    
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
//
////    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        ApplyStudent *model=dataArray[indexPath.row];
//        StudentId=[NSString stringWithFormat:@"%@",model.ApplyStudentID];
//
//        [dataArray removeObjectAtIndex:indexPath.row];
//        NSLog(@"indexPath.row:%ld",indexPath.row);
//
//       [courseTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
//
////    }
//
//
//
//}


//多删：
-(void)DoubleDelete:(UIButton *)sender{
    
   NSLog(@"选中个数是 : %lu   内容为 : %@",(unsigned long)selectorPatnArray.count,selectorPatnArray);

    NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules/%@/registeredStudents",LocalIP,_scheduleIdString];
    NSMutableArray *IDArr = [NSMutableArray new];
    NSMutableArray *tempArr = [NSMutableArray new];
    for (ApplyStudent *model in selectorPatnArray) {
        [IDArr addObject:model.ApplyStudentID];
    }
    for (NSNumber *row in rowIdArray) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:row.integerValue inSection:0];
        
        [tempArr addObject:path];
    }
    [RequestTools RequestWithURL:URL Method:@"DELETE" Params:IDArr Success:^(NSDictionary *result) {
        //删数据yuan

        
        if (selectorPatnArray.count == dataArray.count) {
            [dataArray removeAllObjects];
        }else{
            for (ApplyStudent *model in selectorPatnArray) {
                if ([dataArray containsObject:model]) {
                    [dataArray removeObject:model];
                }
            }
        }
        
      
       [courseTable deleteRowsAtIndexPaths:[NSArray arrayWithArray:tempArr] withRowAnimation:UITableViewRowAnimationRight];
        [courseTable reloadData];
        [selectorPatnArray removeAllObjects];
        [rowIdArray removeAllObjects];
        [Maneger showAlert:@"学生删除成功!" andCurentVC:self];
        
        
        
        } failed:^(NSString *result) {
            
             NSLog(@"%@",result);
            if (![result isEqualToString:@"200"]) {
                [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
                 NSLog(@"%@",result);
            }else{
                [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
            }
        }];

    DeleView.hidden=YES;
    Delebtn.hidden=YES;
    rightBtn.hidden=NO;
    [courseTable setEditing:NO animated:YES];

}

//全删
-(void)AllDelete:(UIButton *)sender{
    
//    courseTable.editing = !courseTable.editing;
    for (int i=0; i<dataArray.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        [courseTable selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
        ApplyStudent *model=dataArray[i];
        [rowIdArray addObject:[NSNumber numberWithInt:i]];
       [selectorPatnArray addObject:model];
    
    }

}

//单删
//-(void)Delete{
//
//    NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules/%@/registeredStudents/%@",LocalIP,_scheduleIdString,StudentId];
//
//    NSLog(@"%@",URL);
//
//    [RequestTools RequestWithURL:URL Method:@"delete" Params:nil Success:^(NSDictionary *result) {
//
//
//          [Maneger showAlert:@"学生删除成功!" andCurentVC:self];
//
//
//
//    } failed:^(NSString *result) {
//        if (![result isEqualToString:@"200"]) {
//            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
//        }else{
//            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
//        }
//    }];
//
//
//}







@end
