//
//  AddTeachingPlanViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/29.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddTeachingPlanViewController : UIViewController

//课程教案
@property (weak, nonatomic) IBOutlet UILabel *CoursePlanLabel;
@property (weak, nonatomic) IBOutlet UIButton *CoursePlan;

//上课时间：
@property (weak, nonatomic) IBOutlet UILabel *CourseTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *CourseStartTime;
@property (weak, nonatomic) IBOutlet UIButton *CourceEndTime;

//上课地点：
@property (weak, nonatomic) IBOutlet UILabel *CourseLocationLabel;
@property (weak, nonatomic) IBOutlet UIButton *CourseLocation;

//上课类型：
@property (weak, nonatomic) IBOutlet UIButton *CourseGenreBtn;

//课程状态:
@property (weak, nonatomic) IBOutlet UISegmentedControl *CourseStatusSegmented;

//必修课状态：
@property (weak, nonatomic) IBOutlet UIView *RequiredView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *RequiredSegmented;

//报名上限：
@property (weak, nonatomic) IBOutlet UITextField *ApplyTextField;

//报名时间：
@property (weak, nonatomic) IBOutlet UIButton *ApplyStartTime;
@property (weak, nonatomic) IBOutlet UIButton *ApplyEndTime;


@property (weak, nonatomic) IBOutlet UIButton *SaveBtn;

- (IBAction)SaveAction:(id)sender;


@end
