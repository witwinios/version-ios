//
//  ImageChangeController.m
//  yun
//
//  Created by MacAir on 2017/9/4.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ImageChangeController.h"

@interface ImageChangeController ()
{
    UIImageView *headImage;
    LoadingView *loadView;
}
@end

@implementation ImageChangeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-80, 29.5, 80, 25);
    [rightBtn setTitle:@"更换" forState:UIControlStateNormal];
    rightBtn.titleLabel.textAlignment = 1;
    [rightBtn setImage:nil forState:0];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(imageAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"个人中心";
    [backNavigation addSubview:titleLab];
    //
    headImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    headImage.image = self.img;
    [self.view addSubview:headImage];
    
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)imageAction:(UIButton *)btn{
    //从底部弹出(iOS 8 的用法)
    UIAlertController *sheetController = [UIAlertController alertControllerWithTitle:@"请选择图片" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        //选中拍照之后的操作
        
        //资源类型为照相机
        UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
        //判断是否有相机
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            
            picker.delegate =self;
            //设置拍照后的图片可被编辑
            picker.allowsEditing = YES;
            //资源类型为照相机
            picker.sourceType = sourceType;
            [self presentViewController:picker animated:YES completion:nil];
            
        }else {
            [MBProgressHUD showToastAndMessage:@"该设备无摄像头" places:0 toView:nil];
        }
    }];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        //选中相册之后的操作
        NSLog(@"相册");
        //从相册选择
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.navigationBar.backgroundColor = UIColorFromHex(0x20B2AA);
        //资源类型为图片库
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        //设置选择后的图片可被编辑
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    [sheetController addAction:saveAction];
    [sheetController addAction:deleteAction];
    [sheetController addAction:cancelAction];
    
    [self presentViewController:sheetController animated:YES completion:^{
    }];
}
#pragma -action
- (void)changeImage:(UIImage *)img{
    [MBProgressHUD showHUDAndMessage:@"上传中~" toView:nil];
    NSDictionary *dic = @{@"fileName":@"headimage",@"description":@"testimage",@"isPublic":@"true"};
    NSString *request = [NSString stringWithFormat:@"%@/files",LocalIP];
    PersonEntity *person = [[NSuserDefaultManager share] readCurrentUser];
    [RequestTools RequestFile:img andParams:dic andUrl:request Progress:^(NSProgress *progress) {
//         [progress addObserver:self forKeyPath:@"fractionCompleted" options:NSKeyValueObservingOptionNew context:nil];
    } Success:^(NSDictionary *result) {
        //获取fileId
        NSNumber *fileId = result[@"responseBody"][@"fileId"];
        NSString *requestUrl = [NSString stringWithFormat:@"%@/users/%@/updateUserPicture/%@",LocalIP,self.entity.userID,fileId];
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:@{@"userId":person.userID,@"fileId":fileId} Success:^(NSDictionary *result) {
            [MBProgressHUD hideHUDForView:nil];
            [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
            headImage.image = img;
            //传到上个界面
            _imageBlock(img);
            //保存
            [NSuserDefaultManager saveHeadImage:img byAccount:person.userID];
            if (self.entity.roleId.integerValue == 2) {
                //教师
                TMainController *mainVC = [TMainController shareObject];
                mainVC.headImage.image = img;
            }else{
                MainViewController *mainVC = [MainViewController new];
                mainVC.headImage.image = img;
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
    }];
}
#pragma -delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // 销毁控制器
    [picker dismissViewControllerAnimated:YES completion:nil];
    // 获得图片
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    //
    
    [self changeImage:image];
}
//
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
//    if ([keyPath isEqualToString:@"fractionCompleted"] && [object isKindOfClass:[NSProgress class]]) {
//
//    }
//}
@end
