//
//  CourseAffixModel.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/25.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CourseAffixModel.h"

@implementation CourseAffixModel

-(void)setFileId:(NSNumber *)FileId{
    if ([FileId isKindOfClass:[NSNull class]]) {
        _FileId = [NSNumber numberWithInt:0];
    }else{
        _FileId = FileId;
    }
}

-(void)setFileName:(NSString *)FileName{
    if ([FileName isKindOfClass:[NSNull class]]) {
        _FileName = @"暂无";
    }else{
        _FileName = FileName;
    }
}


-(void)setDescription:(NSString *)Description{
    if ([Description isKindOfClass:[NSNull class]]) {
        _Description = @"";
    }else{
        _Description = Description;
    }
}


-(void)setFileType:(NSString *)FileType{
    if ([FileType isKindOfClass:[NSNull class]]) {
        _FileType = @"暂无";
    }else{
        _FileType = FileType;
    }
}

-(void)setFileUrl:(NSString *)FileUrl{
    if ([FileUrl isKindOfClass:[NSNull class]]) {
        _FileUrl = @"暂无";
    }else{
        _FileUrl = FileUrl;
    }
}


-(void)setFileFormat:(NSString *)FileFormat{
    if ([FileFormat isKindOfClass:[NSNull class]]) {
        _FileFormat = @"暂无";
    }else{
        _FileFormat = FileFormat;
    }
}

-(void)setFileSize:(NSNumber *)FileSize{
    if ([FileSize isKindOfClass:[NSNull class]]) {
        _FileSize = [NSNumber numberWithInt:0];
    }else{
        _FileSize = FileSize;
    }
}


-(void)setIsPublic:(NSNumber *)IsPublic{
    if ([IsPublic isKindOfClass:[NSNull class]]) {
        _IsPublic = [NSNumber numberWithInt:0];
    }else{
        _IsPublic = IsPublic;
    }
}

-(void)setCreatedBy:(NSString *)CreatedBy{
    if ([CreatedBy isKindOfClass:[NSNull class]]) {
        _CreatedBy = @"暂无";
    }else{
        _CreatedBy = CreatedBy;
    }
}

-(void)setCreatedTime:(NSString *)CreatedTime{
    if ([CreatedTime isKindOfClass:[NSNull class]]) {
        _CreatedTime = @"暂无";
    }else{
        _CreatedTime = CreatedTime;
    }
}

@end
