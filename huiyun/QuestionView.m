//
//  QuestionView.m
//  yun
//
//  Created by MacAir on 2017/6/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "QuestionView.h"

@implementation QuestionView
#define SCREEN_W [UIScreen mainScreen].bounds.size.width

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self laySubview];
    }
    return self;
}
- (void)laySubview{
    CGFloat title_h = [Maneger getPonentH:self.quesModel.questTitle andFont:[UIFont systemFontOfSize:12] andWidth:SCREEN_W];
    self.quesTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_W, title_h)];
    self.quesTitle.numberOfLines = 0;
    self.quesTitle.text = self.quesModel.questTitle;
    [self addSubview:self.quesTitle];
}
@end
