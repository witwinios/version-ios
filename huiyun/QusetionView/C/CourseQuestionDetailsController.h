//
//  CourseQuestionDetailsController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/2/1.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseQuestionModel.h"
#import "AAQusetionTableViewCell.h"
#import "AQusetionCell.h"
#import "CourseQuestionBView.h"
#import "CourseQuestionViewController.h"

@interface CourseQuestionDetailsController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
@property(strong,nonatomic)CourseQuestionModel *AfterModel;

@property(nonatomic) CGFloat ChoiceOptionsLabel_h;
@property(nonatomic,strong)NSMutableArray *QuestionArr;

@end
