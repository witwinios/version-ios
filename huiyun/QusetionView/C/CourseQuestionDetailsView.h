//
//  CourseQuestionDetailsView.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/16.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseQuestionModel.h"
#import "CourseQuestionViewController.h"
//A1 题型:
#import "AQusetionCell.h"

@interface CourseQuestionDetailsView : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic)CourseQuestionModel *AfterModel;


@end
