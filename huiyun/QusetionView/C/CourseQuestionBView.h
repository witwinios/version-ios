//
//  CourseQuestionBView.h
//  huiyun
//
//  Created by Mr.Wang on 2018/2/1.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyGestureRecognizer.h"
@interface CourseQuestionBView : UIViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate>
@property(nonatomic,strong)NSString *ImgUrl;

@property(nonatomic,strong)UIImage *QuestionImg;

@property (strong, nonatomic) MyGestureRecognizer *customGestureRecognizer;
@property (strong, nonatomic) UIImageView *imgV;
@property (strong, nonatomic) UIImageView *imgV2;
- (void)handlePan:(UIPanGestureRecognizer *)recognizer;
- (void)handlePinch:(UIPinchGestureRecognizer *)recognizer;
- (void)handleRotation:(UIRotationGestureRecognizer *)recognizer;
- (void)handleTap:(UITapGestureRecognizer *)recognizer;
- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer;
- (void)handleSwipe:(UISwipeGestureRecognizer *)recognizer;
- (void)handleCustomGestureRecognizer:(MyGestureRecognizer *)recognizer;

- (void)bindPan:(UIImageView *)imgVCustom;
- (void)bindPinch:(UIImageView *)imgVCustom;
- (void)bindRotation:(UIImageView *)imgVCustom;
- (void)bindTap:(UIImageView *)imgVCustom;
- (void)bindLongPress:(UIImageView *)imgVCustom;
- (void)bindSwipe;
- (void)bingCustomGestureRecognizer;

@property(nonatomic,strong)NSString *error;


@end
