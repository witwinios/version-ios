//
//  CourseQuestionDetailsController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/2/1.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "CourseQuestionDetailsController.h"
#define Cell @"AAQusetionCell"
@interface CourseQuestionDetailsController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *TableView;
    NSMutableArray *DataArray;
    NSMutableArray *AnswersArray;
    
    UIView *QuestionTitleView;
    UILabel *QuestionTypes;
    
    NSMutableArray *NewArray;
    
    UILabel *Qusetion;
    
    NSMutableArray *ChildQArray;
    
    NSString *Qusetion_Str;
    NSString *QuestionTypes_Str;
    
    NSMutableArray *ChoiceOptionsArray;
    
    NSMutableArray *OptionArray;
    
    UIScrollView *MyScrollView;
    
    NSString *FileImgStr;
    
    NSString *FileStrs;
    
}

@end

@implementation CourseQuestionDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    DataArray=[NSMutableArray new];
    AnswersArray=[NSMutableArray new];
    NewArray=[NSMutableArray new];
    ChildQArray=[NSMutableArray new];
    ChoiceOptionsArray=[NSMutableArray new];
    OptionArray=[NSMutableArray new];
    
    FileStrs=@"有图";
    FileImgStr=@"图片";
    
    [self loadData];
    [self setNav];
    
    
}


- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"题目详情";
    
    
    [backNavigation addSubview:titleLab];
    
}

- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)setUI{
    
    
    for (int i=0; i<26; i++) {
        [OptionArray addObject:[NSString stringWithFormat:@"%c",'A'+i]];
    }
    
    
    MyScrollView=[[UIScrollView alloc]init];
    MyScrollView.contentInset=UIEdgeInsetsMake(1, 1, 1, 1);
    MyScrollView.directionalLockEnabled=YES;
    MyScrollView.alwaysBounceVertical=YES;
    MyScrollView.alwaysBounceHorizontal=NO;
    MyScrollView.pagingEnabled=YES;
    MyScrollView.showsHorizontalScrollIndicator = NO;
    MyScrollView.scrollIndicatorInsets=UIEdgeInsetsMake(0, 50, 0, 0);
    MyScrollView.indicatorStyle=UIScrollViewIndicatorStyleBlack;
    MyScrollView.scrollsToTop=NO;
    MyScrollView.delegate = self;
    MyScrollView.backgroundColor=[UIColorFromHex(0xF0F0F0)colorWithAlphaComponent:0.5];
    
    
    
    
    QuestionTypes=[[UILabel alloc]initWithFrame:CGRectMake(10,10, 200, 20)];
    if ([QuestionTypes_Str isEqualToString:@"A3:病例组型题"]) {
        QuestionTypes.text=@"A3:病例组型题";
    }else if([QuestionTypes_Str isEqualToString:@"A4:病例串型题"]){
        QuestionTypes.text=@"A4:病例串型题";
    }else{
        QuestionTypes.text=@"B1:标准配伍题型";
    }
    
    QuestionTypes.textAlignment=NSTextAlignmentCenter;
    QuestionTypes.textColor=UIColorFromHex(0x20B2AA);
    QuestionTypes.font=[UIFont systemFontOfSize:13];
    QuestionTypes.layer.borderWidth=2;
    QuestionTypes.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    QuestionTypes.layer.cornerRadius=5;
    [MyScrollView addSubview:QuestionTypes];
    
    CGFloat ChoiceOptions_X=50;
    CGFloat Test_h;
    CGFloat Question_Y=QuestionTypes.wwy_y+QuestionTypes.wwy_height+10;
     NSLog(@"FileImgStr==%@",FileImgStr);
    if ([QuestionTypes_Str isEqualToString:@"B1:标准配伍题型"]) {
        for (int i=0; i<ChoiceOptionsArray.count; i++) {
            
            Qusetion=[[UILabel alloc]init];
            [Qusetion setFont:[UIFont systemFontOfSize:15]];
            [Qusetion setNumberOfLines:0];
            
            Qusetion.text=[NSString stringWithFormat:@"%@. %@",OptionArray[i],ChoiceOptionsArray[i]];
            CGFloat Qusetion_H=[Qusetion getSpaceLabelHeight:Qusetion.text withWidh:Swidth-60];
            CGFloat Qusetion_Y=(Qusetion_H+10)*i+(QuestionTypes.wwy_height+20);
            [Qusetion setFrame:CGRectMake(ChoiceOptions_X, Qusetion_Y,Swidth-60, Qusetion_H)];
            [MyScrollView addSubview:Qusetion];
            if (i == ChoiceOptionsArray.count-1) {
                _ChoiceOptionsLabel_h=Qusetion_H+Qusetion_Y;
                Test_h=Qusetion_H+ChoiceOptions_X;
            }
        }
        
        NSString *url=[NSString stringWithFormat:@"%@?CTTS-Token=%@",FileImgStr,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
        UIImageView *FileImg=[[UIImageView alloc]init];
        FileImg.frame=CGRectMake(20, _ChoiceOptionsLabel_h+20, Swidth-40, Swidth-40);
        [FileImg sd_setImageWithURL:[NSURL URLWithString:url]];
        
        CGFloat FileImg_H=20+Swidth-40;
        
        [MyScrollView addSubview:FileImg];
        
       
        
        if ([FileImgStr isEqualToString:@"图片"]) {
            MyScrollView.frame=CGRectMake(0, 64, Swidth, _ChoiceOptionsLabel_h+25);
        }else{
            MyScrollView.frame=CGRectMake(0, 64, Swidth, Sheight/3);
            MyScrollView.contentSize=CGSizeMake(0, FileImg_H+10);
        }
        NSLog(@"_ChoiceOptionsLabel_h==%f",_ChoiceOptionsLabel_h);
        
        
        [self.view addSubview:MyScrollView];
        
        
    }else{
        
        Qusetion=[[UILabel alloc]init];
        [Qusetion setFont:[UIFont systemFontOfSize:15]];
        [Qusetion setNumberOfLines:0];
        Qusetion.text=Qusetion_Str;
        CGFloat Qusetion_H=[Qusetion getSpaceLabelHeight:Qusetion.text withWidh:Swidth-20];
        [Qusetion setFrame:CGRectMake(20, Question_Y, Swidth-20, Qusetion_H)];
        
        CGFloat Qusetion_h=Question_Y+Qusetion_H;
        
        [MyScrollView addSubview:Qusetion];
    
        
       
        
        
        if ([FileImgStr isEqualToString:@"图片"]) {
            MyScrollView.frame=CGRectMake(0, 64, Swidth, Qusetion.wwy_height+QuestionTypes.wwy_height+25);
        }else{
            NSString *url=[NSString stringWithFormat:@"%@?CTTS-Token=%@",FileImgStr,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
            UIImageView *FileImg=[[UIImageView alloc]init];
            FileImg.frame=CGRectMake(20, Qusetion_h+20, 100, 100);
            [FileImg sd_setImageWithURL:[NSURL URLWithString:url]];
            [FileImg setUserInteractionEnabled:YES];
            
            [FileImg addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setShowImg)]];
            
            [MyScrollView addSubview:FileImg];
            MyScrollView.frame=CGRectMake(0, 64, Swidth, Sheight/3);
           // MyScrollView.contentSize=CGSizeMake(0,FileImg_H);
        }
        
        [self.view addSubview:MyScrollView];
    }
    
    
    CGFloat TableView_Y;
    
    if ([QuestionTypes_Str isEqualToString:@"B1:标准配伍题型"]) {
        TableView_Y=MyScrollView.wwy_y+MyScrollView.wwy_height+2;
        NSLog(@"TableView_Y==%f",TableView_Y);
    }else{
        TableView_Y=MyScrollView.wwy_y+MyScrollView.wwy_height+2;
    }
    
    TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, TableView_Y,Swidth, Sheight-TableView_Y) style:UITableViewStylePlain];
    TableView.delegate = self;
    TableView.dataSource = self;
    TableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    TableView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:TableView];
    
    
}


-(void)setShowImg{
    CourseQuestionBView *vc=[CourseQuestionBView new];
    vc.ImgUrl=FileImgStr;
    
    [self.navigationController pushViewController:vc animated:YES];
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return scrollView.subviews.firstObject;
}


-(void)setData{
    /*
     1.获取教案数据：
     2.shi y
     
     
     
     */
}

-(void)loadData{
    NSString *Url=[NSString stringWithFormat:@"%@/questions/%@",LocalIP,_AfterModel.QuestionId];
    NSLog(@"%@",Url);
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            
            return;
        }
        [DataArray removeAllObjects];
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            CourseQuestionModel *model=[CourseQuestionModel new];
            model.QuestionId=[result[@"responseBody"] objectForKey:@"questionId"];
            model.QuestionTitle=[result[@"responseBody"] objectForKey:@"questionTitle"];
            model.ChildrenQuestionsNum=[result[@"responseBody"] objectForKey:@"childrenQuestionsNum"];
            model.childArray=[NSMutableArray new];
            model.ChoiceOptions=[result[@"responseBody"] objectForKey:@"choiceOptions"];
            model.QuestionType=[result[@"responseBody"] objectForKey:@"questionType"];
            model.Analysis=[result[@"responseBody"] objectForKey:@"analysis"];
            
            if(![[result[@"responseBody"] objectForKey:@"questionFile"]isKindOfClass:[NSNull class]]){
                
                model.QuestionFile=[[result[@"responseBody"] objectForKey:@"questionFile"]objectForKey:@"fileUrl"];
                NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.QuestionFile];
                model.QuestionFile=url;
                FileImgStr=url;
                
            }

            int Num=[model.ChildrenQuestionsNum intValue];
            
            NSArray *childer=[result[@"responseBody"] objectForKey:@"childrenQuestions"];
            for (int i=0; i<Num; i++) {
                NSDictionary *childDictionary=[childer objectAtIndex:i];
                CourseQuestionModel *childModel = [CourseQuestionModel new];
                childModel.ChildrenQuestionId=[childDictionary objectForKey:@"questionId"];
                childModel.ChildrenQuestionTitle=[childDictionary objectForKey:@"questionTitle"];
                childModel.ChildrensChoiceOptions=[childDictionary objectForKey:@"choiceOptions"];
                childModel.ChildrenQuestionType=[result[@"responseBody"] objectForKey:@"questionType"];
                childModel.Analysis=[childDictionary objectForKey:@"analysis"];
                
                
                if(![[result[@"responseBody"] objectForKey:@"questionFile"]isKindOfClass:[NSNull class]]){
                    model.ChildrensQuestionFile=[[result[@"responseBody"] objectForKey:@"questionFile"]objectForKey:@"fileUrl"];
                    NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.ChildrensQuestionFile];
                    model.ChildrensQuestionFile=url;
                    FileStrs=url;
                    
                }
                
                [self AnswersWithQuestionId:[NSString stringWithFormat:@"%@",childModel.ChildrenQuestionId]];
                [ChildQArray addObject:childModel];
                [model.childArray addObject:childModel];
                NSLog(@"Analysis----%@",childModel.Analysis);
                
            //    if (i == Num-1) {
                
              //  }
                
            }
            QuestionTypes_Str=model.QuestionType;
            
            if ([QuestionTypes_Str isEqualToString:@"B1:标准配伍题型"]) {
                [ChoiceOptionsArray addObjectsFromArray:model.ChoiceOptions];
                [self setUI];
            }else{
                Qusetion_Str=model.QuestionTitle;
                [self setUI];
            }
            [DataArray addObject:model];
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        //获取答案：
        //  [self QuestionAddAnswers];
        
        
    } failed:^(NSString *result) {
        NSLog(@"result==%@",result);
        if (![result isEqualToString:@"200"]) {
            
        }
        return;
    }];
    
}

-(void)AnswersWithQuestionId:(NSString *)questionId{
    
    NSString *url=[NSString stringWithFormat:@"%@/questions/%@/answers",LocalIP,questionId];
    NSLog(@"url==%@",url);
    [RequestTools RequestWithURL:url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            return;
        }
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [result objectForKey:@"responseBody"];
            if (array.count ==0) {
                [Maneger showAlert:@"无题目!" andCurentVC:self];
                
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    CourseQuestionModel *model=[CourseQuestionModel new];
                    
                    model.QuestionId=[dictionary objectForKey:@"questionId"];
                    model.Answers=[dictionary objectForKey:@"answer"];
                    
                    [AnswersArray addObject:model];
                    NSLog(@"Answers---%@",[NSString stringWithFormat:@"%@",model.Answers]);
                }
              //  [TableView reloadData];
                NSLog(@"AnswersArray==%@",AnswersArray);
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
         [TableView reloadData];
    } failed:^(NSString *result) {
        NSLog(@"result==%@",result);
        if (![result isEqualToString:@"200"]) {
            
        }
        return;
    }];
    
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return AnswersArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CourseQuestionModel *ChildrenModel=ChildQArray[indexPath.row];
    CourseQuestionModel *AnswersModel=AnswersArray[indexPath.row];

    
    
    AAQusetionTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:Cell];
    
    if (cell == nil) {
        cell=[AAQusetionTableViewCell new];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
 //   ChoiceOptionsArray
 //   [cell setProperty:ChildrenModel Type:@"教师" AnswersArray:[NSString stringWithFormat:@"%@",AnswersModel.Answers] ];
    [cell setProperty:ChildrenModel Type:@"教师" AnswersArray:[NSString stringWithFormat:@"%@",AnswersModel.Answers] ChoiceOptions:ChoiceOptionsArray];
    
    NSLog(@"Cell--%@",[NSString stringWithFormat:@"%@",AnswersModel.Answers]);
    
    CGRect frame=cell.frame;
    
    
    if ([FileStrs isEqualToString:@"有图"]) {
        frame.size.height=cell.AnswersView.wwy_y+cell.AnswersView.wwy_height+10;
    }else{
        frame.size.height=cell.FileImg.wwy_y+cell.FileImg.wwy_height+10;
    }

    frame.size.width=Swidth;
    cell.frame=frame;
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    AAQusetionTableViewCell *cell=(AAQusetionTableViewCell *)[self tableView:tableView  cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end

