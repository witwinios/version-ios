//
//  CourseQuestionDetailsView.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/16.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "CourseQuestionDetailsView.h"

#define Cell @"QusetionCell"

@interface CourseQuestionDetailsView ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *TableView;
    NSMutableArray *DataArray;
    NSMutableArray *AnswersArray;
    
    NSMutableArray *NewArray;
    
    NSString *FileStr;
    
}

@end

@implementation CourseQuestionDetailsView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    FileStr=@"有图";
   
    [self setNav];
    
    [self setUI];
    
    [self loadData];
}
- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"题目详情";
    
    
    [backNavigation addSubview:titleLab];
    
}

- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUI{
    TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    TableView.delegate = self;
    TableView.dataSource = self;
    TableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    TableView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:TableView];
    
    DataArray=[NSMutableArray new];
    AnswersArray=[NSMutableArray new];
    NewArray=[NSMutableArray new];
}

//http://www.hzwitwin.cn:81/witwin-ctts-web/questions/253768 题目
// http://www.hzwitwin.cn:81/witwin-ctts-web/questions/4372/answers 答案
-(void)loadData{
    //获取题目:
    NSString *Url=[NSString stringWithFormat:@"%@/questions/%@",LocalIP,_AfterModel.QuestionId];
    NSLog(@"%@",Url);
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];

            return;
        }
        [DataArray removeAllObjects];
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            CourseQuestionModel *model=[CourseQuestionModel new];
            model.QuestionId=[result[@"responseBody"] objectForKey:@"questionId"];
            model.QuestionTitle=[result[@"responseBody"] objectForKey:@"questionTitle"];
            model.ChoiceOptions=[result[@"responseBody"] objectForKey:@"choiceOptions"];
            model.QuestionType=[result[@"responseBody"] objectForKey:@"questionType"];
            model.Analysis=[result[@"responseBody"] objectForKey:@"analysis"];
            
            
            if(![[result[@"responseBody"] objectForKey:@"questionFile"]isKindOfClass:[NSNull class]]){
                model.QuestionFile=[[result[@"responseBody"] objectForKey:@"questionFile"]objectForKey:@"fileUrl"];
                NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.QuestionFile];
                model.QuestionFile=url;
                FileStr=url;
            }
            [DataArray addObject:model];
            
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        //获取答案:
        NSString *url=[NSString stringWithFormat:@"%@/questions/%@/answers",LocalIP,_AfterModel.QuestionId];
        
        [RequestTools RequestWithURL:url Method:@"get" Params:nil Success:^(NSDictionary *result) {
            
            if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
                [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
                return;
            }
            
            [AnswersArray removeAllObjects];
            
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                NSArray *array = [result objectForKey:@"responseBody"];
                if (array.count ==0) {
                    [Maneger showAlert:@"无题目!" andCurentVC:self];
                    
                }else{
                    for (int i=0; i<array.count; i++) {
                        NSDictionary *dictionary = [array objectAtIndex:i];
                        CourseQuestionModel *model=[CourseQuestionModel new];
                        
                        model.QuestionId=[dictionary objectForKey:@"questionId"];
                        model.Answers=[dictionary objectForKey:@"answer"];
                        model.AnswersArray=[NSMutableArray new];
                        [model.AnswersArray addObject: model.Answers];
                        [NewArray addObject:model.Answers];
                        [AnswersArray addObject:model];
                        NSLog(@"model.Answers==%@",[NSString stringWithFormat:@"%@",NewArray]);
                        NSLog(@"AnswersArray==%@",NewArray);
                    }
                    [TableView reloadData];
                    
                }
            }
            else{
                [Maneger showAlert:@"请重新登录!" andCurentVC:self];
            }
            
        } failed:^(NSString *result) {
            NSLog(@"result==%@",result);
            if (![result isEqualToString:@"200"]) {
                
            }
            return;
        }];
        
        
    } failed:^(NSString *result) {
        NSLog(@"result==%@",result);
        if (![result isEqualToString:@"200"]) {
            
        }
        return;
    }];
    
   
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return DataArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        CourseQuestionModel *model=DataArray[indexPath.row];
        AQusetionCell *cell=[tableView dequeueReusableCellWithIdentifier:Cell];
        if (cell == nil) {
            cell=[AQusetionCell new];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setProperty:model Type:@"教师" AnswersArray:NewArray];
        CGRect frame=cell.frame;
    
    
    NSLog(@"QuestionFile==%@",model.QuestionFile);
    
    if ([FileStr isEqualToString:@"有图"]) {
        frame.size.height=cell.AnswersView.wwy_y+cell.AnswersView.wwy_height+10;
    }else{
        frame.size.height=cell.FileImg.wwy_y+cell.FileImg.wwy_height+10;
    }
    
    
    
        frame.size.width=Swidth;
        cell.frame=frame;
        
         return cell;
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CourseQuestionModel *model=DataArray[indexPath.row];
    
    if ([model.QuestionType isEqualToString:@"A1:单句型题"]||[model.QuestionType isEqualToString:@"A2:病例摘要题"] ||[model.QuestionType isEqualToString:@"X型多选题"]) {
        AQusetionCell *cell=(AQusetionCell *)[self tableView:tableView  cellForRowAtIndexPath:indexPath];
        return cell.frame.size.height;
    }else{
        return 100;
    }
    
   
}

@end
