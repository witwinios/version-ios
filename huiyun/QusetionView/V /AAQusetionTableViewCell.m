//
//  AAQusetionTableViewCell.m
//  huiyun
//
//  Created by Mr.Wang on 2018/2/1.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "AAQusetionTableViewCell.h"

@implementation AAQusetionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        //题标:
        
        
        //题目:
        self.QusetionTitle=[[UILabel alloc]init];
        [self.QusetionTitle setFont:[UIFont systemFontOfSize:15]];
        [self.QusetionTitle setNumberOfLines:0];
        [self addSubview:self.QusetionTitle];
        
        //选项:
        
        
        
       
        //按钮：
       
        
       
        
        //答案View：
         self.AnswersView=[[UIView alloc]init];
         [self addSubview:self.AnswersView];
        //答案label：
       
        self.AnswersLabel=[[UILabel alloc]init];
        [self.AnswersLabel setFont:[UIFont systemFontOfSize:15]];
        [self.AnswersLabel setNumberOfLines:0];
         [self addSubview:self.AnswersLabel];
        //解析：
        self.AnalysisLabel=[[UILabel alloc]init];
        [self.AnalysisLabel setFont:[UIFont systemFontOfSize:15]];
        [self.AnalysisLabel setNumberOfLines:0];
        [self addSubview:self.AnalysisLabel];
        
        //图片：
        self.FileImg=[[UIImageView alloc]init];
        
        [self addSubview:self.FileImg];
        
    }
    
    
    return self;
}

-(void)setProperty:(CourseQuestionModel *)model Type:(NSString *)str AnswersArray:(NSString *)Answers ChoiceOptions:(NSMutableArray *)ChoiceOptions{
    
    _OptionArray=[NSMutableArray new];
    //标题:

    
    //选项A～Z:
    for (int i=0; i<26; i++) {
        [_OptionArray addObject:[NSString stringWithFormat:@"%c",'A'+i]];
    }
    
    //题目：
   
  
    
    self.QusetionTitle.text=[NSString stringWithFormat:@"    %@",model.ChildrenQuestionTitle];
    CGFloat QuesetionTitle_H=[self.QusetionTitle getSpaceLabelHeight:self.QusetionTitle.text withWidh:Swidth-20];
    [self.QusetionTitle setFrame:CGRectMake(20, 15, Swidth-20, QuesetionTitle_H)];
    
  
    
    //选项
    CGFloat ChoiceOptions_X=95;
    for (int i=0; i<model.ChildrensChoiceOptions.count; i++) {
        self.ChoiceOptionsLabel=[[UILabel alloc]init];
        [self.ChoiceOptionsLabel setFont:[UIFont systemFontOfSize:15]];
        [self.ChoiceOptionsLabel setNumberOfLines:0];
        self.ChoiceOptionsLabel.text=[NSString stringWithFormat:@"%@.%@",_OptionArray[i],model.ChildrensChoiceOptions[i]];
        
        
        
        CGFloat ChoiceOptions_H=[self.ChoiceOptionsLabel getSpaceLabelHeight:self.ChoiceOptionsLabel.text withWidh:self.frame.size.width-115];
        
        
        CGFloat ChoiceOptions_Y;
        
        if(i == 0){
            ChoiceOptions_Y=(QuesetionTitle_H+35);
        }else{
            ChoiceOptions_Y=(_ChoiceOptionsLabel_h+20);
        }
        
        
        [self.ChoiceOptionsLabel setFrame:CGRectMake(ChoiceOptions_X, ChoiceOptions_Y, self.frame.size.width-115, ChoiceOptions_H)];
        _ChoiceOptionsLabel_h=ChoiceOptions_Y+ChoiceOptions_H;
        
        self.ChoiceOptionsBtn=[RadioButton buttonWithType:UIButtonTypeCustom];
        [self.ChoiceOptionsBtn setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
        [self.ChoiceOptionsBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateSelected];
        [self.ChoiceOptionsBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.ChoiceOptionsBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateSelected];
        if ([str isEqualToString:@"教师"]) {
            self.ChoiceOptionsBtn.userInteractionEnabled=NO;
        }
        [self.ChoiceOptionsBtn setFrame:CGRectMake(ChoiceOptions_X-38, ChoiceOptions_Y-5, 40, 30)];
        _ChoiceOptionsBtn_h=ChoiceOptions_Y+30;
        self.ChoiceOptionsBtn.tag=99+i;
//
        
        if ([model.ChildrenQuestionType isEqualToString:@"B1:标准配伍题型"]) {
            
        }else{
            [self addSubview:self.ChoiceOptionsLabel];
            [self addSubview:self.ChoiceOptionsBtn];
        }
//        [self addSubview:self.ChoiceOptionsLabel];
//        [self addSubview:self.ChoiceOptionsBtn];
        
        
    }
    
    
    
   
    
    
    //答案:
    

     CGFloat AnswersLabel_Y=_ChoiceOptionsLabel_h+20;
    self.AnswersLabel.text=[NSString stringWithFormat:@"答案:%c",'A'+ [Answers intValue]];
    self.AnswersLabel.textColor=[UIColor redColor];
    CGFloat AnswersLabel_H=[self.AnswersLabel getSpaceLabelHeight:self.AnswersLabel.text withWidh:Swidth-20];
    [self.AnswersLabel setFrame:CGRectMake(20, AnswersLabel_Y, Swidth-20, AnswersLabel_H)];
    _AnswersLabel_h=AnswersLabel_Y+AnswersLabel_H;
   
    
    
    //解析：

    CGFloat AnalysisLabel_Y=_AnswersLabel_h+10;
    self.AnalysisLabel.text=[NSString stringWithFormat:@"解析:  %@",model.Analysis];
    CGFloat AnalysisLabel_H=[self.AnalysisLabel getSpaceLabelHeight:self.AnalysisLabel.text withWidh:Swidth-20];
    [self.AnalysisLabel setFrame:CGRectMake(20, AnalysisLabel_Y, Swidth-20, AnalysisLabel_H)];
    _AnalysisLabel_h=AnalysisLabel_H;
   
   
    
   
    self.AnswersView.backgroundColor=[UIColorFromHex(0xF0F0F0)colorWithAlphaComponent:0.2];
    [self.AnswersView setFrame:CGRectMake(20, AnswersLabel_Y-5, Swidth,AnalysisLabel_H+AnswersLabel_H+20)];
    
    
    NSString *url=[NSString stringWithFormat:@"%@?CTTS-Token=%@",model.ChildrensQuestionFile,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
    self.FileImg.frame=CGRectMake(20, self.AnswersView.wwy_y+self.AnswersView.wwy_height+20, 100, 100);
    [self.FileImg sd_setImageWithURL:[NSURL URLWithString:url]];
    
    
   
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
