//
//  AAQusetionTableViewCell.h
//  huiyun
//
//  Created by Mr.Wang on 2018/2/1.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioButton.h"
#import "CourseQuestionModel.h"
@interface AAQusetionTableViewCell : UITableViewCell
@property(nonatomic,strong)UILabel *QusetionTitle;
@property(nonatomic,strong)UILabel *ChoiceOptionsLabel;
@property(nonatomic,strong)RadioButton *ChoiceOptionsBtn;
@property(nonatomic,strong)UIView *AnswersView;
@property(nonatomic,strong)UILabel *AnswersLabel;
@property(nonatomic,strong)UILabel *AnalysisLabel;
@property(nonatomic,strong)NSMutableArray *OptionArray;

@property(nonatomic) CGFloat ChoiceOptionsLabel_h;
@property(nonatomic) CGFloat ChoiceOptionsBtn_h;
@property(nonatomic) CGFloat AnswersLabel_h;

@property(nonatomic) CGFloat AnalysisLabel_Y;
@property(nonatomic) CGFloat AnalysisLabel_h;

@property(nonatomic,strong)NSString *Title;

-(void)setProperty:(CourseQuestionModel *)model Type:(NSString *)str AnswersArray:(NSString *)Answers ChoiceOptions:(NSMutableArray *)ChoiceOptions;

@property(nonatomic,strong)NSString *StrNum;

@property(nonatomic,strong)NSMutableArray *ChoiceOptionArr;

@property(nonatomic,strong)NSString *ChildrenQuestionTitle;

@property(nonatomic,strong)NSString *Answers;

@property(nonatomic,strong)NSString *Analysis;

@property(nonatomic,strong)UIImageView *FileImg;

@end
