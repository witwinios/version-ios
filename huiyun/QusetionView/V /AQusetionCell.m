//
//  AQusetionCell.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/16.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "AQusetionCell.h"

@implementation AQusetionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        //题标:
        self.QuestionTypes=[[UILabel alloc]init];
        [self.QuestionTypes setFont:[UIFont systemFontOfSize:15]];
        [self.QuestionTypes setNumberOfLines:0];
        [self addSubview:self.QuestionTypes];
        
        //题目:
        self.QusetionTitle=[[UILabel alloc]init];
        [self.QusetionTitle setFont:[UIFont systemFontOfSize:15]];
        [self.QusetionTitle setNumberOfLines:0];
        [self addSubview:self.QusetionTitle];
        
        //选项:
        
        //按钮：
        
        //答案View：
        
        
        //答案label：
       
        //解析：
        self.AnalysisLabel=[[UILabel alloc]init];
        [self.AnalysisLabel setFont:[UIFont systemFontOfSize:15]];
        [self.AnalysisLabel setNumberOfLines:0];
        [self addSubview:self.AnalysisLabel];
        
        //图片：
        self.FileImg=[[UIImageView alloc]init];
        
        [self addSubview:self.FileImg];
        
        
    }
    
    
    return self;
}



-(void)setProperty:(CourseQuestionModel *)model Type:(NSString *)str AnswersArray:(NSMutableArray *)Answers{
    _OptionArray=[NSMutableArray new];
    
    //标题:
    
    if ([model.QuestionType isEqualToString:@"A1:单句型题"]) {
        self.QuestionTypes.text=@"A1:单句型题";
    }else if([model.QuestionType isEqualToString:@"A2:病例摘要题"]){
       self.QuestionTypes.text=@"A2:病例摘要题";
    }else{
        self.QuestionTypes.text=@"X型题型";
    }
    self.QuestionTypes.textAlignment=NSTextAlignmentCenter;
    self.QuestionTypes.textColor=UIColorFromHex(0x20B2AA);
    self.QuestionTypes.layer.borderWidth=2;
    self.QuestionTypes.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.QuestionTypes.layer.cornerRadius=5;

    CGFloat QuestionTypes_H=[self.QuestionTypes getSpaceLabelHeight:self.QuestionTypes.text withWidh:Swidth-40];
    [self.QuestionTypes setFrame:CGRectMake(10,10, 200, 20)];
    
    //选项A～Z:
    for (int i=0; i<26; i++) {
        [_OptionArray addObject:[NSString stringWithFormat:@"%c",'A'+i]];
    }
    
    //题目：
    self.QusetionTitle.text=[NSString stringWithFormat:@"    %@",model.QuestionTitle];
    CGFloat QuesetionTitle_H=[self.QusetionTitle getSpaceLabelHeight:self.QusetionTitle.text withWidh:Swidth-20];
    [self.QusetionTitle setFrame:CGRectMake(20, QuestionTypes_H+20, Swidth-20, QuesetionTitle_H)];
    
    
    //选项
    CGFloat ChoiceOptions_X=50;
    for (int i=0; i<model.ChoiceOptions.count; i++) {
        self.ChoiceOptionsLabel=[[UILabel alloc]init];
        [self.ChoiceOptionsLabel setFont:[UIFont systemFontOfSize:15]];
        [self.ChoiceOptionsLabel setNumberOfLines:0];
        self.ChoiceOptionsLabel.text=[NSString stringWithFormat:@"%@. %@",_OptionArray[i],model.ChoiceOptions[i]];
        CGFloat ChoiceOptions_H=[self.ChoiceOptionsLabel getSpaceLabelHeight:self.ChoiceOptionsLabel.text withWidh:self.frame.size.width-60];
        
        CGFloat ChoiceOptions_Y;
        
        if (i==0) {
             ChoiceOptions_Y=(QuesetionTitle_H+60);
        }else{
             ChoiceOptions_Y=(_ChoiceOptionsLabel_h+20);
        }
        
        
       
        
        [self.ChoiceOptionsLabel setFrame:CGRectMake(ChoiceOptions_X, ChoiceOptions_Y,self.frame.size.width-60, ChoiceOptions_H)];
        _ChoiceOptionsLabel_h=ChoiceOptions_Y+ChoiceOptions_H;
        [self addSubview:self.ChoiceOptionsLabel];
        
        self.ChoiceOptionsBtn=[RadioButton buttonWithType:UIButtonTypeCustom];
        
        if ([model.QuestionType isEqualToString:@"A1:单句型题"] || [model.QuestionType isEqualToString:@"A2:病例摘要题"]) {
            [self.ChoiceOptionsBtn setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            [self.ChoiceOptionsBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateSelected];
        }else{
            [self.ChoiceOptionsBtn setImage:[UIImage imageNamed:@"kong"] forState:UIControlStateNormal];
            [self.ChoiceOptionsBtn setImage:[UIImage imageNamed:@"fill"] forState:UIControlStateSelected];
        }
        [self.ChoiceOptionsBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.ChoiceOptionsBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateSelected];
        if ([str isEqualToString:@"教师"]) {
              self.ChoiceOptionsBtn.userInteractionEnabled=NO;
        }
        [self.ChoiceOptionsBtn setFrame:CGRectMake(ChoiceOptions_X-38, ChoiceOptions_Y, 40, 30)];
        _ChoiceOptionsBtn_h=ChoiceOptions_Y+30;
        self.ChoiceOptionsBtn.tag=99+i;
        [self addSubview:self.ChoiceOptionsBtn];
        
    }
    
    //答案:
    
    CGFloat AnswersLabel_Y=_ChoiceOptionsLabel_h+20;
    self.AnswersLabel=[[UILabel alloc]init];
    [self.AnswersLabel setFont:[UIFont systemFontOfSize:15]];
    [self.AnswersLabel setNumberOfLines:0];
    
    NSString *AnswersStr;
    NSString *AnswersStrr=@"答案是：";
    NSString *Answer;
    for (int i=0; i<Answers.count; i++) {
        AnswersStr=[NSString stringWithFormat:@"%c",'A'+ [Answers[i] intValue]];
        Answer=[AnswersStrr stringByAppendingString:AnswersStr];
        AnswersStrr=Answer;
        NSLog(@"Answers==%@",Answers[i]);
    }
    
    
    self.AnswersLabel.text=[NSString stringWithFormat:@"%@",Answer];
    self.AnswersLabel.textColor=[UIColor redColor];
    CGFloat AnswersLabel_H=[self.AnswersLabel getSpaceLabelHeight:self.AnswersLabel.text withWidh:Swidth-20];
    [self.AnswersLabel setFrame:CGRectMake(20, AnswersLabel_Y, Swidth-20, AnswersLabel_H)];
    _AnswersLabel_h=AnswersLabel_Y+AnswersLabel_H;
    [self addSubview:self.AnswersLabel];
    

   //解析：
    CGFloat AnalysisLabel_Y=_AnswersLabel_h+10;
    self.AnalysisLabel.text=[NSString stringWithFormat:@"解析:  %@",model.Analysis];
    CGFloat AnalysisLabel_H=[self.AnalysisLabel getSpaceLabelHeight:self.AnalysisLabel.text withWidh:Swidth-20];
    [self.AnalysisLabel setFrame:CGRectMake(20, AnalysisLabel_Y, Swidth-20, AnalysisLabel_H)];
    _AnalysisLabel_h=AnalysisLabel_H;

    self.AnswersView=[[UIView alloc]init];
    self.AnswersView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.2];
    [self.AnswersView setFrame:CGRectMake(20, AnswersLabel_Y-5, Swidth,AnalysisLabel_H+AnswersLabel_H+20)];
    [self addSubview:self.AnswersView];
    
    //图片：
    NSString *url=[NSString stringWithFormat:@"%@?CTTS-Token=%@",model.QuestionFile,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
    self.FileImg.frame=CGRectMake(20, self.AnswersView.wwy_y+self.AnswersView.wwy_height+20, 100, 100);
    [self.FileImg sd_setImageWithURL:[NSURL URLWithString:url]];

}





- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
