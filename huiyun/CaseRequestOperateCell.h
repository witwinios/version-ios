//
//  CaseRequestOperateCell.h
//  huiyun
//
//  Created by MacAir on 2017/10/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CaseRequestOperateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *indexLab;
@property (weak, nonatomic) IBOutlet UILabel *leastNum;

@property (weak, nonatomic) IBOutlet UILabel *resultLab;
@property (weak, nonatomic) IBOutlet UILabel *operateName;
@property (weak, nonatomic) IBOutlet UILabel *operateLevel;

@end
