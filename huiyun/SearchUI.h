//
//  SearchUI.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/10.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^basicBlock)(NSString *startTime,NSString *endTime);
@interface SearchUI : UIView
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIButton *endBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UIButton *dateCancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *dateSureBtn;
@property (weak, nonatomic) IBOutlet UILabel *showLab;
@property (weak, nonatomic) IBOutlet UIView *pickView;
@property (weak, nonatomic) IBOutlet UIView *SCView1;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePick;
@property (weak, nonatomic) IBOutlet UIView *SCView;
@property (strong, nonatomic) basicBlock responseBlock;
//控制视图的显示隐藏
@property (strong, nonatomic) NSNumber *isShowSelf;
@property (strong, nonatomic) NSNumber *isShowPick;
@property (strong,nonatomic) NSNumber *currentBtn;
@property (strong, nonatomic) UIViewController *currentController;
- (instancetype)initWithCurrentvc:(UIViewController *)currentVC;
#pragma -action
- (void)showInVC;
- (void)hideInVC;
- (void)showDatePick;
- (void)hideDatePick;

- (IBAction)cancelAction:(id)sender;
- (IBAction)sureAction:(id)sender;
- (IBAction)dateCancelAction:(id)sender;
- (IBAction)dateSureAction:(id)sender;
- (IBAction)startAction:(id)sender;
- (IBAction)endAction:(id)sender;
@end
