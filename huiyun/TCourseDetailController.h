//
//  CourseDetailController.h
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseModel.h"
#import "TCourseDatailCell.h"
#import "DesCell.h"
#import "BeforeCourseController.h"
#import "ApplyStudentViewController.h"
#import "FileViewController.h"
#import "CourseQuestionViewController.h"
#import "TimeCell.h"
typedef void (^NextBlock)(NSString *scheduleId);



@interface TCourseDetailController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
@property (strong, nonatomic) TCourseModel *model;
@property (strong, nonatomic) BeforeCourseController *beforeVC;
@property(nonatomic,copy)NextBlock Block;

-(void)NextBlock:(NextBlock)block;

@end

