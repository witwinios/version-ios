//
//  DepartmentModel.m
//  huiyun
//
//  Created by Bad on 2018/3/15.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "DepartmentModel.h"

@implementation DepartmentModel

-(void)setGroupId:(NSString *)GroupId{
    if ([GroupId isKindOfClass:[NSNull class]]) {
        _GroupId= @"暂无";
    }else{
        _GroupId = GroupId;
    }
}

-(void)setGroupName:(NSString *)GroupName{
    if ([GroupName isKindOfClass:[NSNull class]]) {
        _GroupName= @"暂无";
    }else{
        _GroupName = GroupName;
    }
}

-(void)setGroupType:(NSString *)GroupType{
    if ([GroupType isKindOfClass:[NSNull class]]) {
        _GroupType= @"暂无";
    }else{
        _GroupType = GroupType;
    }
}

@end
