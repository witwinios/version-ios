//
//  CourseWareCell.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/2.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseWareModel.h"
@interface CourseWareCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *CourseWareName;
@property (weak, nonatomic) IBOutlet UILabel *CourseWarePublic;
@property (weak, nonatomic) IBOutlet UILabel *CourseWareSubject;
@property (weak, nonatomic) IBOutlet UILabel *CourseWareClass;
@property (weak, nonatomic) IBOutlet UIButton *CourseWareChoice;

@property (weak, nonatomic) IBOutlet UIView *BgVc;
-(void)setProperty:(CourseWareModel *)model;

@end
