//
//  OnlinePagesController.m
//  huiyun
//
//  Created by MacAir on 2018/2/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "OnlinePagesController.h"

@interface OnlinePagesController ()
{
    UIView *toolbar;
    NSMutableArray *_heightArr;
}

@end

@implementation OnlinePagesController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArr = [NSMutableArray new];
    _resultArr = [NSMutableArray new];
    _heightArr = [NSMutableArray new];
    __weak UIViewController *weakSelf = self;
    self.barBC = ^(int s){
        if (s == 0) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    };
    self.rightBtn.hidden = YES;
    [self.titleLab setTitle:@"试卷列表" forState:0];
    //
    [self setUI];
    [self setupRefresh];
    //
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"确定" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonHide:) name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)setUI{
    _seachBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, Swidth-50, 35)];
    _seachBar.delegate = self;
    _seachBar.placeholder = @"请输入试卷名称";
    [self.screenView addSubview:_seachBar];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(Swidth-45, 0, 40, 35);
    [cancelBtn setTitle:@"取消" forState:0];
    cancelBtn.layer.cornerRadius = 5;
    [cancelBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateHighlighted];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:0];
    cancelBtn.layer.masksToBounds = YES;
    [cancelBtn addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.screenView addSubview:cancelBtn];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 35, Swidth, self.screenView.frame.size.height-35) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [_tableView registerNib:[UINib nibWithNibName:@"OnlineStudentCell" bundle:nil] forCellReuseIdentifier:@"stuCell"];
    [self.screenView addSubview:_tableView];
}
- (void)setupRefresh {
    [_tableView addHeaderWithTarget:self action:@selector(downRefresh)];
    //    [_tableView addFooterWithTarget:self action:@selector(moreRefresh)];
    [_tableView headerBeginRefreshing];
}
- (void)downRefresh{
    [_dataArr removeAllObjects];
    _seachBar.text = @"";
    
    NSString *str = [NSString stringWithFormat:@"%@/testSchedules/%@/testPapers?pageSize=999",LocalIP,self.model.onlineId];
    [RequestTools RequestWithURL:str Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [_tableView headerEndRefreshing];
        
        NSArray *arr = result[@"responseBody"][@"result"];
        for (int s=0; s<arr.count; s++) {
            NSDictionary *dic = arr[s];
            OnlinePagesModel *model = [OnlinePagesModel new];
            model.pageId = dic[@"paperId"];
            model.pageScore = dic[@"totalScore"];
            model.pageSubject = dic[@"subjectName"];
            model.pageCategory = dic[@"categoryName"];
            model.pageName = dic[@"paperName"];
            [_heightArr addObject:[NSString stringWithFormat:@"%f",[Maneger getPonentH:model.pageName andFont:[UIFont systemFontOfSize:20] andWidth:Swidth-218]]];
            
            [_dataArr addObject:model];
        }
        _resultArr = _dataArr;
        [_tableView reloadData];
    }failed:^(NSString *result) {
        [_tableView headerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"失败,尝试重新刷新~" places:0 toView:nil];
    }];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [_heightArr[indexPath.row] floatValue] + 93;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OnlinePagesModel *model = _dataArr[indexPath.row];
    OnlineStudentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"stuCell"];
    cell.nameLabel.text = @"试卷名称:";
    cell.oneLabel.text = @"试卷分类:";
    cell.twoLabel.text = @"试卷科目:";
    cell.threeLabel.text = @"满分:";
    cell.stuStatus.hidden = YES;
    cell.nameLab.text = model.pageName;
    cell.oneContent.text = model.pageCategory;
    cell.twoContent.text = model.pageCategory;
    cell.rightImg.hidden=YES;
    cell.threeContent.text = [NSString stringWithFormat:@"%@",model.pageScore];
    return cell;
}
//
- (void)hideKb:(UIButton *)btn{
    [_seachBar resignFirstResponder];
    NSMutableArray *arr = [NSMutableArray new];
    for (int s=0; s<_resultArr.count; s++) {
        OnlinePagesModel *model = _resultArr[s];
        if ([model.pageName containsString:_seachBar.text]) {
            [arr addObject:model];
        }
    }
    _dataArr = arr;
    [_tableView reloadData];
}
-(void)doneButtonshow: (NSNotification *)notification {
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
}
- (void)doneButtonHide:(NSNotification *)notification{
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
    }];
}
- (void)cancelAction:(UIButton *)btn{
    _seachBar.text = @"";
    _dataArr = _resultArr;
    [_tableView reloadData];
    [_seachBar resignFirstResponder];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText isEqualToString:@""]) {
        _dataArr = _resultArr;
        [_tableView reloadData];
    }
}
@end
