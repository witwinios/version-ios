//
//  SelectField.h
//  huiyun
//
//  Created by MacAir on 2017/12/5.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionModel.h"
@interface SelectField : UITextField
@property (strong, nonatomic) QuestionModel *model;
@property (strong, nonatomic) NSNumber *currentIndex;

@property (strong, nonatomic) NSNumber *quesIndex;

//当前第几个填空
@property (strong, nonatomic) NSNumber *fieldNum;
@end

