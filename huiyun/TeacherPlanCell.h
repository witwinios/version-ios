//
//  TeacherPlanCell.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/30.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseAddModel.h"
@interface TeacherPlanCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *PlanName;
@property (weak, nonatomic) IBOutlet UILabel *PlanClassify;
@property (weak, nonatomic) IBOutlet UILabel *PlanCourse;
@property (weak, nonatomic) IBOutlet UILabel *PlanOwner;
@property (weak, nonatomic) IBOutlet UIButton *PlanStautebtn;

- (void)setProperty:(TCourseAddModel *)model;


@end
