//
//  TeacherAdviceController.h
//  huiyun
//
//  Created by MacAir on 2018/1/23.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDepartModel.h"
@interface TeacherAdviceController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *mainText;
- (IBAction)tempSave:(id)sender;
- (IBAction)submitAction:(id)sender;

@property (strong, nonatomic) SDepartModel *model;
@end
