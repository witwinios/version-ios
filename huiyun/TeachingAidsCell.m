//
//  TeachingAidsCell.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TeachingAidsCell.h"

@implementation TeachingAidsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
   self.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.3];
   
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setProperty:(TeachingAidsModel *)model{
    
    self.Onelabel.text=@"教具名称：";
    self.TwoLabel.text=@"现有可用库存：";
    self.ThreeLabel.text=@"已预约数量：";
    self.FourLabel.text=@"用途：";
    
    
    
    self.AidsName.text=model.AidsName;
    self.Repertory.text=[NSString stringWithFormat:@"%@",model.Repertory];
    self.Subscribe.text=[NSString stringWithFormat:@"%@",model.Subscribe];
    self.Purpose.text=model.Purpose;

}

-(void)setModelAids:(TeachingAidsModel *)model{
    
    self.Onelabel.text=@"教具品名：";
    self.TwoLabel.text=@"分类：      ";
    self.ThreeLabel.text=@"规格型号：";
    self.FourLabel.text=@"库存数量：";
    self.StatusBtn.hidden=YES;
    
    
    
    self.AidsName.text=model.ModelName;
    self.Repertory.text=model.ModelCategoryName;
    self.Subscribe.text=model.ModelNo;
    self.Purpose.text=[NSString stringWithFormat:@"%@", model.ModelItemsNum];
    
    if (model.isCheck) {
        self.StatusView.backgroundColor = [UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.4];
    } else {
        self.StatusView.backgroundColor = [UIColor whiteColor];
    }
    
}

@end
