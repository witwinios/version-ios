//
//  TSkillTestCenterModel.h
//  huiyun
//
//  Created by Bad on 2018/3/23.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSkillTestCenterModel : NSObject

@property(nonatomic,strong)NSString *RoomName;//考场地点

@property(nonatomic,strong)NSString *TeacherName;//考官名称

@property(nonatomic,strong)NSNumber *OperationScoreValue;//技能满分

@property(nonatomic,strong)NSString *SelectedStationName; //站点名称

@property(nonatomic,strong)NSMutableArray *TeacherArray;

@property(nonatomic,strong)NSNumber *TSkillTimeFrom;
@property(nonatomic,strong)NSNumber *TSKillTimeEnd;

@end
