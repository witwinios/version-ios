//
//  MyQuesModel.h
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface MyQuesModel : NSObject
@property (strong, nonatomic) NSNumber *indexId;
@property (strong, nonatomic) NSNumber *qaGroupId;
@property (strong, nonatomic) NSNumber *tribeId;
@property (copy, nonatomic) NSString *quesTitle;
@property (copy, nonatomic) NSString *quesDes;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSArray *memberArray;
@end
