//
//  StandardCourseWareViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/2.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "StandardCourseWareViewController.h"
#import "CourseWareCell.h"
#import "CourseWareModel.h"

@interface StandardCourseWareViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *courseTable;
    NSMutableArray *dataArray;
     int currentPage;
    
     NSInteger line;
    NSMutableArray *seleArr;
    
    
    UIView *SearchView;
    UITextField *SearchField;
    UIButton *SearchBtn;
    
    NSMutableArray *tempArray;
    
    NSMutableArray *selectArray;
     BOOL isSearch;
}

@end

@implementation StandardCourseWareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setsearchBar];
    [self setNav];
    
    [self setUI];
    [self setUpRefresh];
   
    
 
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"课件选取";
    
    
    [backNavigation addSubview:titleLab];
    
    
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [rightBtn setTitle:@"批量选取" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(Choice:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(setCourseWareChoice) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;

    
}
- (void)back :(UIButton *)button{
    
    if(seleArr.count >0){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您有尚未保存的课件，是否保存？" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self setCourseWareChoice];
        }];
        
        UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert addAction:noAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)Choice:(id)sender{
   // [courseTable setEditing:YES animated:YES];
    
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您现在可以选择教案课件." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        _editing =YES;
        rightBtn.hidden=YES;
        SaveBtn.hidden=NO;
    }];
    
    UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:noAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
 
}


-(void)setUI{
    currentPage = 1;
    CGFloat courseTable_Y=backNavigation.wwy_height+SearchView.wwy_height;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, courseTable_Y,Swidth, Sheight-courseTable_Y) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    courseTable.estimatedRowHeight = 0;
    [courseTable registerNib:[UINib nibWithNibName:@"CourseWareCell" bundle:nil] forCellReuseIdentifier:@"courseware"];
    [self.view addSubview:courseTable];
    
    _editing = NO;
    dataArray=[NSMutableArray new];
    seleArr=[NSMutableArray new];
    
}

#pragma mark 搜索框：
-(void)setsearchBar{
    tempArray=[NSMutableArray array];
    selectArray=[NSMutableArray array];
    isSearch=NO;
    
    SearchView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 44)];
    SearchView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    UIImageView *SearchImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sousuo"]];
    SearchImg.backgroundColor=[UIColor clearColor];
    SearchImg.frame=CGRectMake(10, 6, 32, 32);
    
    
    SearchField=[[UITextField alloc]initWithFrame:CGRectMake(44, 6, Swidth-88, 32)];
    SearchField.backgroundColor=[UIColor clearColor];
    SearchField.placeholder=@"请输入课件名称";
    SearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    SearchField.clearsOnBeginEditing = YES;
    SearchField.keyboardType=UIKeyboardTypeDefault;
    SearchField.returnKeyType=UIReturnKeySearch;
    SearchField.delegate = self;
    [SearchField addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    SearchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SearchBtn.frame=CGRectMake((SearchField.wwy_x+SearchField.wwy_width)+2, 6, 40, 32);
    [SearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [SearchBtn addTarget:self action:@selector(setUpRefresh) forControlEvents:UIControlEventTouchUpInside];
    SearchBtn.backgroundColor=UIColorFromHex(0x20B2AA);
    SearchBtn.layer.cornerRadius=5;
    SearchBtn.hidden=YES;
    [SearchView addSubview:SearchBtn];
    [SearchView addSubview:SearchField];
    [SearchView addSubview:SearchImg];
    [self.view addSubview:SearchView];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [SearchField  resignFirstResponder];
    [self setUpRefresh ];
    NSLog(@"点击了搜索");
    
    return YES;
    
}


-(BOOL)ApplyTextDidChange:(UITextField *)theTextField{
    
    NSLog( @"text changed: %@", theTextField.text);
    
    
    if (![theTextField.text isEqualToString:@""]) {
        SearchBtn.hidden=NO;
        isSearch=YES;
    }else{
        isSearch=NO;
        [self setUpRefresh];
        SearchBtn.hidden=YES;
        [SearchField  resignFirstResponder];
    }
    
    return YES;
    
}


#pragma mark 筛选框：
-(void)screen{
    
}

#pragma mark 数据加载：
-(void)setUpRefresh{
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中。。。";
}

//http://www.hzwitwin.cn:81/witwin-ctts-web/standardCourses?pageStart=1&pageSize=10&currentPage=1&active=true
-(void)loadData{
    
    if (_editing) {
        [seleArr removeAllObjects];
    }
    
    
     currentPage = 1;
    [seleArr removeAllObjects];
    NSString *Url;
    
    if (isSearch == YES) {
        NSString *str=SearchField.text;
        Url=[NSString stringWithFormat:@"%@/standardCourses?pageStart=%d&pageSize=20&active=true&courseName=%@",LocalIP,currentPage,str];
        Url=[Url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }else{
        Url=[NSString stringWithFormat:@"%@/standardCourses?pageStart=%d&pageSize=20&active=true",LocalIP,currentPage];
    }
    
    
    
    NSLog(@"%@",Url);
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        NSLog(@"%@",result);
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                [Maneger showAlert:@"无课件!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    CourseWareModel *model=[CourseWareModel new];
                    model.CourseWareId=[dictionary objectForKey:@"courseId"];
                    model.CourseWareName=[dictionary objectForKey:@"courseName"];
                    model.CourseWarePublic=[dictionary objectForKey:@"isPublic"];
                    model.CourseWareSubject=[dictionary objectForKey:@"subjectName"];
                    model.CourseWareClass=[dictionary objectForKey:@"categoryName"];
                    [dataArray addObject:model];
                    
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
                
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
     
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}

-(void)loadRefresh{
   currentPage++;
    NSString *Url;
    
    if (isSearch == YES) {
        NSString *str=SearchField.text;
        Url=[NSString stringWithFormat:@"%@/standardCourses?pageStart=%d&pageSize=20&active=true&courseName=%@",LocalIP,currentPage,str];
        Url=[Url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }else{
        Url=[NSString stringWithFormat:@"%@/standardCourses?pageStart=%d&pageSize=20&active=true",LocalIP,currentPage];
    }
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable footerEndRefreshing];
            return;
        }
        [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];
        
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                [Maneger showAlert:@"无课件!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    CourseWareModel *model=[CourseWareModel new];
                    
                    model.CourseWareId=[dictionary objectForKey:@"courseId"];
                    model.CourseWareName=[dictionary objectForKey:@"courseName"];
                    model.CourseWarePublic=[dictionary objectForKey:@"isPublic"];
                    model.CourseWareSubject=[dictionary objectForKey:@"subjectName"];
                    model.CourseWareClass=[dictionary objectForKey:@"categoryName"];
                 
                     [newData addObject:model];
                    
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                
                
              [courseTable reloadData];
                
                
            }
        }
        else{
            currentPage--;
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

//点击Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(_editing){
        CourseWareModel *model=dataArray[indexPath.row];
        
        model.isCheck=!model.isCheck;
         [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        if (_editing) {
            
            if (model.isCheck == 0) {
                [seleArr removeObject:model];
                NSLog(@"Remove_selectArray:%@",seleArr);
            }else if(model.isCheck == 1){
                [seleArr addObject:model];
                NSLog(@"Add_selectArray:%@",seleArr);
                NSLog(@"CourseWareId=%@---select=%d",model.CourseWareId,model.isSelect.intValue);
                NSLog(@"seleArr:%@",seleArr);
            }
       
        }
 
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CourseWareCell *cell=[tableView dequeueReusableCellWithIdentifier:@"courseware"];
    CourseWareModel *model=dataArray[indexPath.row];
    [cell setProperty:model];
    
    NSLog(@"CourseWareId=%@---select=%d",model.CourseWareId,model.isSelect.intValue);
    
    
    NSString *content=model.CourseWareClass;
    CGFloat test_H=[Maneger getPonentH:content andFont:[UIFont systemFontOfSize:15] andWidth:Swidth-200]+60;
    
    
    CGRect frame=cell.frame;
  
    frame.size.height=test_H+76;
    
    
    cell.frame=frame;
    
    [cell.CourseWareChoice addTarget:self action:@selector(CourseWareChoice:) forControlEvents:UIControlEventTouchUpInside];
    cell.CourseWareChoice.tag=100+indexPath.row;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

        CourseWareCell *cell =(CourseWareCell *)[self tableView:tableView  cellForRowAtIndexPath:indexPath];
        return cell.frame.size.height;
  
}
-(void)CourseWareChoice:(UIButton *)sender{
    line = sender.tag-100;
    NSLog(@"line1:%ld",line);
    
    CourseWareModel *model=dataArray[line];
    [seleArr addObject:model];
    [self setCourseWareChoice];
    
}



#pragma mark 编辑
//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
//}
//
//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    //从选中中取消
//    if (seleArr.count > 0 && _editing) {
//            CourseWareModel *model=dataArray[indexPath.row];
//            model.isSelect = @0;
//            [dataArray replaceObjectAtIndex:indexPath.row withObject:model];
//
//        [seleArr removeObject:model];
//        NSLog(@"CourseWareId=%@---select=%d",model.CourseWareId,model.isSelect.intValue);
//        NSLog(@"selectorPatnArray 取消:%@",seleArr);
//    }
//
//}



//http://www.hzwitwin.cn:81/witwin-ctts-web/courses/531/standardCourses
-(void)setCourseWareChoice{
    
    NSString *CourseWareChoiceId=[NSString stringWithFormat:@"%@",_PlanModel.PlanNameId];
    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/standardCourses",LocalIP,CourseWareChoiceId];
    NSLog(@"setCourseWareChoice==%@",Url);

    [seleArr addObjectsFromArray:_TempArray];
    
    NSMutableArray *IDArr = [NSMutableArray new];
    for (CourseWareModel *model in seleArr) {
        [IDArr addObject:model.CourseWareId];
    }
    
    NSLog(@"%@",IDArr);
    
   NSLog(@"IDArr=%lu",(unsigned long)IDArr.count);
    
    [RequestTools RequestWithURL:Url Method:@"post" Params:IDArr Success:^(NSDictionary *result) {
        
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
             [MBProgressHUD showToastAndMessage:@"添加课件成功!" places:0 toView:nil];
           // _backArr(seleArr);
            [seleArr removeAllObjects];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } failed:^(NSString *result) {
        NSLog(@"result:%@",result);
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
    
}


@end
