//
//  RoundCell.h
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoundCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *departName;
@property (weak, nonatomic) IBOutlet UILabel *departTile;

@end
