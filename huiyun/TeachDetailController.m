//
//  TeachDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/10/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TeachDetailController.h"

@interface TeachDetailController ()
{
    NSArray *titleArray;
    NSArray *heightArr;
    
    PersonEntity *persion;
    
    UITextView *currentTextview;
    CGRect _activedTextFieldRect;
    CGPoint currentPoint;
    UIView *toolbar;
    
    UIView *view;
}
@end

@implementation TeachDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    persion = [[NSuserDefaultManager share] readCurrentUser];
    if (persion.roleId.intValue != 3) {
        self.rightBtn.hidden = YES;
    }else{
        NSLog(@"SDepartStatus==%@",_SDepartModel.SDepartStatus);
        if ([self.model.caseStatus isEqualToString:@"待审核"]) {
            
            if ([_SDepartModel.SDepartStatus isEqualToString:@"出科"]) {
                self.rightBtn.hidden=YES;
            }else{
                self.rightBtn.hidden = NO;
            }
            
            [self.rightBtn setTitleColor:[UIColor whiteColor] forState:0];
            TeachDetailController *weakSelf = self;
            self.rightBlock = ^(){
                [SubjectTool share].subjectType = subjectTypeUpdate;
                TeachRecordController *teachVC = [TeachRecordController new];
                teachVC.myModel = weakSelf.model;
                [weakSelf.navigationController pushViewController:teachVC animated:YES];
            };
        }else {
            if ([_SDepartModel.SDepartStatus isEqualToString:@"出科"]) {
                self.rightBtn.hidden=YES;
            }else{
                self.rightBtn.hidden = YES;
            }
            
        }
        
        
    }
    __weak UIViewController *weakSelf = self;
    self.leftBlock = ^(){
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    self.rightTitle = @"修改";
    self.titles = @"教学记录";
    titleArray = @[@"审核状态:",@"教学项目:",@"带教对象:",@"人数:",@"开始时间:",@"结束时间:",@"带教老师:",@"教学内容:",@"指导意见:",@"附件:"];
    self.dataArray = @[_model.caseStatus,_model.caseProject,_model.caseTeachObject,[NSString stringWithFormat:@"%@",_model.caseNums],    [[Maneger shareObject] timeFormatter1:[NSString stringWithFormat:@"%@",_model.caseStartTime]],    [[Maneger shareObject] timeFormatter1:[NSString stringWithFormat:@"%@",_model.caseEndTime]],_model.caseTeacher,_model.caseTeachContent,_model.caseAdvice,_model.fileUrl];
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"44",@"44",@"103",@"103",@"44"];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    //注册键盘出现的通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];

}
#pragma -action
- (void)keyboardWillHide:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
        self.table.contentOffset = currentPoint;
    }];
}
- (void)hideKb:(UIButton *)btn{
    [currentTextview resignFirstResponder];
}
- (void)keyboardWillShow:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
    if ((_activedTextFieldRect.origin.y + _activedTextFieldRect.size.height) + 40>  ([UIScreen mainScreen].bounds.size.height - rect.size.height)) {//键盘的高度 高于textView的高度 需要滚动
        currentPoint = self.table.contentOffset;
        [UIView animateWithDuration:duration animations:^{
            self.table.contentOffset = CGPointMake(0, 64 + _activedTextFieldRect.origin.y + _activedTextFieldRect.size.height - ([UIScreen mainScreen].bounds.size.height - rect.size.height));
        }];
    }
}
- (void)approveAction:(UIButton *)appBtn{
    
    if ([currentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"请输入指导意见~" places:0 toView:nil];
        return;
    }
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认通过?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentTutoringRecords/%@/review",SimpleIp,_model.recordId];
        
        [RequestTools RequestWithURL:request Method:@"PUT" Params:@{@"reviewerId":persion.userID,@"reviewerComments":currentTextview.text,@"reviewStatus":@"approved"} Message:@"审批中..." Success:^(NSDictionary *result) {
            
            [MBProgressHUD showToastAndMessage:@"审批成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"审批失败~" places:0 toView:nil];
        }];
        
    }];
    [alertVC addAction:cancel];
    [alertVC addAction:sure];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    _activedTextFieldRect = [textView convertRect:textView.frame toView:self.table];
    currentTextview = textView;
    return YES;
}
- (void)rejectAction:(UIButton *)rejBtn{
    if ([currentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"请输入指导意见~" places:0 toView:nil];
        return;
    }

    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认未通过?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentTutoringRecords/%@/review",SimpleIp,_model.recordId];
        
        [RequestTools RequestWithURL:request Method:@"PUT" Params:@{@"reviewerId":persion.userID,@"reviewerComments":currentTextview.text,@"reviewStatus":@"rejected"} Message:@"审批中..." Success:^(NSDictionary *result) {
            
            [MBProgressHUD showToastAndMessage:@"审批成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"审批失败~" places:0 toView:nil];
        }];
    }];
    [alertVC addAction:cancel];
    [alertVC addAction:sure];
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (persion.roleId.intValue == 2 && [_model.caseStatus isEqualToString:@"待审核"]) {
        return 40.f;
    }
    return 0.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *foot = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 40)];
    foot.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIButton *approveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    approveBtn.frame = CGRectMake(0, 0, Swidth/2, 40);
    approveBtn.backgroundColor = UIColorFromHex(0x20B2AA);
    [approveBtn setTitle:@"通过" forState:0];
    [approveBtn addTarget:self action:@selector(approveAction:) forControlEvents:UIControlEventTouchUpInside];
    [foot addSubview:approveBtn];
    
    UIButton *rejectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rejectBtn.frame = CGRectMake(Swidth/2, 0, Swidth/2, 40);
    rejectBtn.backgroundColor = [UIColor redColor];
    [rejectBtn setTitle:@"未通过" forState:0];
    [rejectBtn addTarget:self action:@selector(rejectAction:) forControlEvents:UIControlEventTouchUpInside];
    [foot addSubview:rejectBtn];
    return foot;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (persion.roleId.intValue == 2) {
        return heightArr.count - 1;
    }
    return heightArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *one = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *two = [tableView dequeueReusableCellWithIdentifier:@"two"];
    DetailCellThree *three = [tableView dequeueReusableCellWithIdentifier:@"three"];
    if (indexPath.row == 9) {
        three.titleLab.text = titleArray[indexPath.row];
        [three.contentBtn addTarget:self action:@selector(imageAction) forControlEvents:UIControlEventTouchUpInside];
        return three;
    }else if (indexPath.row == 8 || indexPath.row == 7){
        if (indexPath.row == 8) {
            if (persion.roleId.intValue == 2 && [_model.caseStatus isEqualToString:@"待审核"]) {[SubjectTool share].subjectType = subjectTypeUpdate;
                [two.contentText setEditable:YES];
                currentTextview = two.contentText;
                currentTextview.delegate =self;
            }else{
                [two.contentText setEditable:NO];
            }
        }else{
            [two.contentText setEditable:NO];
        }
        two.titleText.text = titleArray[indexPath.row];
        two.contentText.text = self.dataArray[indexPath.row];
        return two;
    }else{
        one.titleLab.text = titleArray[indexPath.row];
        one.contentLab.text = self.dataArray[indexPath.row];
        return one;
    }
}
- (void)imageAction{
    NSString *imgStr = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",SimpleIp,self.model.fileUrl,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
    
    NSString *imageStr =  [imgStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@",imgStr);
    
    view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    UIColor *color = [UIColor lightGrayColor];
    view.backgroundColor = [color colorWithAlphaComponent:0.5];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, Swidth-40, Sheight-40)];
    imgView.layer.cornerRadius = 5;
    imgView.layer.masksToBounds = YES;
    imgView.center = view.center;
    [view addSubview:imgView];
    __block UIProgressView *pv;
    __weak UIImageView *weakImageView = imgView;
    [imgView sd_setImageWithURL:[NSURL URLWithString:imageStr]
               placeholderImage:[UIImage imageNamed:@"holdimage"]
                        options:SDWebImageCacheMemoryOnly
                       progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                           if (!pv) {
                               [weakImageView addSubview:pv = [UIProgressView.alloc initWithProgressViewStyle:UIProgressViewStyleDefault]];
                               pv.frame = CGRectMake(0, 0, Swidth/2, 40);
                           }
                           CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 8.0f);
                           pv.transform = transform;
                           float showProgress = (float)receivedSize/(float)expectedSize;
                           [pv setProgress:showProgress];
                       }
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                          [pv removeFromSuperview];
                          pv = nil;
                      }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeAction:)];
    tap.numberOfTapsRequired = 1;
    [view addGestureRecognizer:tap];
    
    [self.view addSubview:view];
}

-(void)removeAction:(id)sender{
    view.hidden=YES;
}
@end
