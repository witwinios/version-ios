//
//  BeforeCellModel.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/8.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeforeCellModel : NSObject
@property(strong,nonatomic) NSString *title;
@property(strong,nonatomic) NSNumber *correctNum;
@property(strong,nonatomic) NSNumber *inCorrectNum;
@property(strong,nonatomic) NSNumber *weiNum;
@end
