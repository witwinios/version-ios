//
//  CourseQuestionCell.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "CourseQuestionCell.h"

@implementation CourseQuestionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.3];
  //  self.CourseQuestionStem.editable=NO;
    
    self.CourseQuestionStem.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.CourseQuestionStem.layer.borderWidth=0.2;
    self.CourseQuestionStem.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.2];
    
    

}



-(void)setProperty:(CourseQuestionModel *)model{
    
    self.CourseQuestionName.text=model.QuestionType;
    self.CourseQuestionNum.text=[NSString stringWithFormat:@"%@", model.Difficulty];
    self.Crosshead.text=[NSString stringWithFormat:@"%@", model.ChildrenQuestionsNum ];
    self.CourseQuestionStem.text=model.QuestionTitle;

}

-(void)AddCourseQuestion:(CourseQuestionModel *)model{
    self.CourseQuestionName.text=model.QuestionType;
    self.CourseQuestionNum.text=model.CategoryName;
    self.Crosshead.text=model.SubjectName;
    self.CourseQuestionStem.text=model.QuestionTitle;
    
    if (model.isCheck) {
        self.bgVc.backgroundColor = [UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.4];
    } else {
        self.bgVc.backgroundColor = [UIColor whiteColor];
    }
    
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
