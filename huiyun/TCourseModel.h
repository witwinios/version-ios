//
//  TCourseModel.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 scheduleID 课程ID
 scheduleStatus 课程状态
 coursePlace  上课地点
 courseSubjec 课程科目
 courseName   课程名称
 scheduleTyoe 课程类型
 startTime   上课时间
 endTime     下课时间
 teacherId    教师ID
 teacherName  教师名称
 registNum    报名学生个数
 courseDes   课程备注
 roomID;     上课地点ID
 roomName    上课地点名称
 courseCategoryName 课程类别名称
 courseId;     教案ID
 signstartTime; 报名开始
 signendTime;   报名结束
 capacity 报名上限
 
 live 是否公开
 credit 学分
 required 是否必修

*/
@interface TCourseModel : NSObject
@property(strong,nonatomic)NSNumber *scheduleID;
@property(strong,nonatomic)NSString *scheduleStatus;
@property (strong,nonatomic)NSString *coursePlace;
@property(strong,nonatomic)NSString *courseSubjec;
@property(strong,nonatomic)NSString *courseName;
@property(strong,nonatomic)NSString *scheduleTyoe;
@property(strong,nonatomic)NSNumber *startTime;
@property (strong, nonatomic) NSNumber *endTime;
@property(strong,nonatomic)NSNumber *teacherId;
@property(strong,nonatomic)NSString *teacherName;
@property(strong,nonatomic)NSNumber *registNum;
@property(strong,nonatomic)NSString *courseDes;
@property(strong,nonatomic)NSNumber *roomID;
@property(strong,nonatomic)NSString *roomName;
@property(strong,nonatomic)NSString *courseCategoryName;
@property(strong,nonatomic)NSNumber *courseId;

@property(strong,nonatomic)NSNumber *signstartTime;
@property (strong, nonatomic) NSNumber *signendTime;
@property(strong,nonatomic)NSNumber *capacity;


@property(strong,nonatomic)NSNumber *live;
@property(strong,nonatomic)NSNumber *credit;
@property(strong,nonatomic)NSNumber *required;
@end
