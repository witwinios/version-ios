//
//  ActivityDetailController.h
//  huiyun
//
//  Created by MacAir on 2017/10/16.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailCellOne.h"
#import "DetailCellTwo.h"
#import "CaseActivityModel.h"
#import "SDepartModel.h"
@interface ActivityDetailController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
@property (strong, nonatomic) CaseActivityModel *model;

@property(strong,nonatomic)SDepartModel *SDepartModel;

@end
