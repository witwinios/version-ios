//
//  AddTeachingPlanViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/29.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "AddTeachingPlanViewController.h"
#import "TeacherPlanController.h"
#define NUMBERS @"0123456789\n"
#import "THDatePickerView.h"
@interface AddTeachingPlanViewController ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,TeacherPlanControllerDelegate>
{
    UIView *BackView;
    UIButton *BackBtn;
    
    UIButton *okBtn;
    UIButton *noBtn;
    
    UIDatePicker *DatePick;
    UIPickerView *Picker;
    
    NSMutableArray *PickDateLocation;
    NSMutableArray*PickDateSubject;
    
    NSString *DateString;
    
    UITableView *_tableView;
    NSMutableArray *_photosArr;
    
    NSMutableArray *LocationID;
    NSString *LocationString;
    
    NSMutableArray *SubjectID;
    NSString *SubjectString;
    
    
    NSString *StrateTimeLong;
    NSString *EndTimeLong;
    NSString *ApplyStrateTimeLong;
    NSString *ApplyEndTimeLong;
    
    NSString *MoldString;
    
    NSMutableArray *FileID;
    NSMutableArray *FileString;
    
    
    NSString *currentTimeString;
    
    double EndTimeNum;
    double StartTimeNunm;
    
    double CurrentTimeNunm;
    
    double ApplyEndTimeNum;
    double ApplyStartTimeNunm;
    THDatePickerView *dateView;
    
    NSNumber *BXKC;
    
    
    NSString *KCZT;
    
    NSNumber *BXGK;
    
    NSString *SFGK;
    
    
    NSString *PlanID;
    NSString *ApplyNum;
    
    NSString *KTLX;
 
    NSString *PlanName;
    
    
}

@end

@implementation AddTeachingPlanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //获取当前时间
    [self SetNewTime];
    
    //获取数据
    [self loadData];
    
    //UI创建
    [self setUI];
    
    
    
}

-(void)loadData{
    
    //获取上课地点：
    NSString *UrlLocation=[NSString stringWithFormat:@"%@/rooms?roomName=&pageSize=999",LocalIP];
    [RequestTools RequestWithURL:UrlLocation Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array=[[result objectForKey:@"responseBody"]objectForKey:@"result"];
            if (array.count == 0) {
                [Maneger showAlert:@"暂无课程地址" andCurentVC:self];
            }else{
                
                PickDateLocation=[NSMutableArray new];
                LocationID=[NSMutableArray new];
                
                for(int i=0;i<array.count;i++){
                    NSDictionary *dic=[array objectAtIndex:i];
                    [PickDateLocation addObject:[dic objectForKey:@"roomName"]];
                    
                    [LocationID addObject:[dic objectForKey:@"roomId"]];

                    [Picker reloadAllComponents];
                }
                
            };
        }
    } failed:^(NSString *result) {
        NSLog(@"请求错误");
    }];
    

    
}


-(void)setUI{
    
    //教案
    NSMutableAttributedString *threeStr = [[NSMutableAttributedString alloc] initWithString:@"*课程教案:"];
    [threeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.CoursePlanLabel.attributedText = threeStr;
    
    self.CoursePlan.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CoursePlan.layer.borderWidth=1;
    self.CoursePlan.layer.cornerRadius=5;
    [self.CoursePlan addTarget:self action:@selector(AddCoursePlan:) forControlEvents:UIControlEventTouchUpInside];
    //上课时间：
    NSMutableAttributedString *threeStr2 = [[NSMutableAttributedString alloc] initWithString:@"*上课时间:"];
    [threeStr2 addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.CourseTimeLabel.attributedText = threeStr2;
    
    BackBtn=[[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    BackBtn.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
    BackBtn.hidden=YES;
    [self.view addSubview:BackBtn];
    
    self.CourseStartTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseStartTime.layer.borderWidth=1;
    self.CourseStartTime.layer.cornerRadius=5;
    [self.CourseStartTime addTarget:self action:@selector(CourseStartTime:) forControlEvents:UIControlEventTouchUpInside];
    self.CourseStartTime.titleLabel.font=[UIFont systemFontOfSize:11];
    self.CourceEndTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourceEndTime.layer.borderWidth=1;
    self.CourceEndTime.layer.cornerRadius=5;
    [self.CourceEndTime addTarget:self action:@selector(CourseEndTime:) forControlEvents:UIControlEventTouchUpInside];
    self.CourceEndTime.titleLabel.font=[UIFont systemFontOfSize:11];
    
    //上课地点：
    NSMutableAttributedString *threeStr3 = [[NSMutableAttributedString alloc] initWithString:@"*上课地点:"];
    [threeStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.CourseLocationLabel.attributedText = threeStr3;
    
    [self.CourseLocation addTarget:self action:@selector(CourcseLocation:) forControlEvents:UIControlEventTouchUpInside];
    
    self.CourseLocation.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseLocation.layer.borderWidth=1;
    self.CourseLocation.layer.cornerRadius=5;
    
    //上课类型：
    self.CourseGenreBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseGenreBtn.layer.borderWidth=1;
    self.CourseGenreBtn.layer.cornerRadius=5;
    
    [self.CourseGenreBtn addTarget:self action:@selector(CourseGenre:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //课程状态：
    self.CourseStatusSegmented.selectedSegmentIndex=0;

    BXKC=@(NO);
    KCZT=@"选修";
    [self.CourseStatusSegmented addTarget:self action:@selector(CourseChange:) forControlEvents:UIControlEventValueChanged];
    
    //必修课状态：
    self.RequiredView.hidden=YES;
    self.RequiredSegmented.selectedSegmentIndex=0;
    BXGK=@(YES);
    SFGK=@"公开";
    [self.RequiredSegmented addTarget:self action:@selector(RequiredChange:) forControlEvents:UIControlEventValueChanged];
    
    //报名上限:
    self.ApplyTextField.borderStyle=UITextBorderStyleRoundedRect; //边框样式
    self.ApplyTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.ApplyTextField.clearsOnBeginEditing = YES;
    self.ApplyTextField.keyboardType=UIKeyboardTypeNumberPad;
    self.ApplyTextField.returnKeyType=UIReturnKeyDefault;
    self.ApplyTextField.delegate = self;
    self.ApplyTextField.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.ApplyTextField.layer.borderWidth=1;
    self.ApplyTextField.layer.cornerRadius=5;

    [self.ApplyTextField addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    //报名时间：
    self.ApplyStartTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.ApplyStartTime.layer.borderWidth=1;
    self.ApplyStartTime.layer.cornerRadius=5;
    self.ApplyStartTime.titleLabel.font=[UIFont systemFontOfSize:11];
    [self.ApplyStartTime addTarget:self action:@selector(ApplyStratTime:) forControlEvents:UIControlEventTouchUpInside];
    

    self.ApplyEndTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.ApplyEndTime.layer.borderWidth=1;
    self.ApplyEndTime.layer.cornerRadius=5;
    self.ApplyEndTime.titleLabel.font=[UIFont systemFontOfSize:11];
      [self.ApplyEndTime addTarget:self action:@selector(ApplyEndTime:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.SaveBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.SaveBtn.layer.borderWidth=1;
    self.SaveBtn.layer.cornerRadius=5;
}




#pragma mark 报名上限——键盘回收：
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.ApplyTextField  resignFirstResponder];
}

-(void)ApplyTextDidChange:(UITextField *)theTextField{
    
    NSLog( @"text changed: %@", theTextField.text);
    ApplyNum=theTextField.text;
    
    int Num=[theTextField.text intValue];
    
    if (Num<1) {

        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"报名数小于最小报名数，请重新输入" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            theTextField.layer.borderColor=[UIColor redColor].CGColor;
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    
    
    }else if(Num>10000){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"报名数大于最大报名数，请重新输入!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            theTextField.layer.borderColor=[UIColor redColor].CGColor;
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        theTextField.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    }
  
}




#pragma mark 课程教案
-(void)AddCoursePlan:(id)sender{
    
     [self resignFirstRespond];
    TeacherPlanController *vc=[[TeacherPlanController alloc]init];
    
    vc.delegate=self;
    vc.TempStr=@"1";
    
    
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark 上课时间

-(void)SetNewTime{
    
    NSDate *datenow = [NSDate date];
    
    NSString* currentTime=[NSString stringWithFormat:@"%0.f",[datenow timeIntervalSince1970]*1000];
    
    currentTimeString =currentTime;
    NSLog(@"currentTimeString:%@",currentTimeString);
    
    CurrentTimeNunm =[currentTimeString longLongValue];
}




-(void)CourseStartTime:(UIButton *)sender{
    //选择时间控件：
    [self resignFirstRespond];
    [self ChooseStartTime];
    BackBtn.hidden = NO;
    [self resignFirstRespond];
    [dateView show];
}
//隐藏键盘
- (void)resignFirstRespond{
    [self.ApplyTextField resignFirstResponder];
}

-(void)ChooseStartTime{
       CGFloat BackView_Y=Sheight/3;
    dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight-BackView_Y, Swidth, BackView_Y) andSureBlock:^(NSString *date, NSString *longDate) {
        BackBtn.hidden = YES;
        
        
        
        NSLog(@"%@-%@",date,longDate);
        [self.CourseStartTime setTitle:date forState:0];
        StrateTimeLong = longDate;
        StartTimeNunm=[longDate longLongValue];
        
        
        if (StartTimeNunm<CurrentTimeNunm) {
            UIAlertController *alert2=[UIAlertController alertControllerWithTitle:@"提示" message:@"上课时间早于当前时间，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction2=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                self.CourseStartTime.layer.borderColor=[UIColor redColor].CGColor;
                
            }];
            
            [alert2 addAction:okAction2];
            [self presentViewController:alert2 animated:true completion:nil];
            
        }else{
            [self.CourseStartTime setTitle:date forState:0];
            self.CourseStartTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
        }
        
        
        
        
    } andCancelBlock:^{
        BackBtn.hidden = YES;
    }];
    [self.view addSubview:dateView];
    
}


-(void)setNoBtn{
    BackView.hidden=YES;
}
//结束时间:
-(void)CourseEndTime:(UIButton *)sender{
    //选择时间控件：
    [self ChooseEndTime];
    BackBtn.hidden = NO;
    [self resignFirstRespond];
    [dateView show];
}
-(void)ChooseEndTime{
    
    CGFloat BackView_Y=Sheight/3;
    dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight-BackView_Y, Swidth, BackView_Y) andSureBlock:^(NSString *date, NSString *longDate) {
        BackBtn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
        EndTimeNum=[longDate longLongValue];
        EndTimeLong = longDate;
        if (EndTimeNum<StartTimeNunm) {
            UIAlertController *alert2=[UIAlertController alertControllerWithTitle:@"提示" message:@"结束时间早于上课时间，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction2=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                self.CourceEndTime.layer.borderColor=[UIColor redColor].CGColor;
                
            }];
            
            [alert2 addAction:okAction2];
            [self presentViewController:alert2 animated:true completion:nil];
            
        }else{
            [self.CourceEndTime setTitle:date forState:0];
            self.CourceEndTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
        }
        
    } andCancelBlock:^{
        BackBtn.hidden = YES;
    }];
    [self.view addSubview:dateView];
    
}

#pragma mark 课程类型：
-(void)CourseGenre:(UIButton *)sender{
     [self resignFirstRespond];
    if(BackView !=nil){
        BackView.hidden=YES;
    }
    
    CGFloat BackView_Y=Sheight/3;
    
    BackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    BackView.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.4];
    
    //工具栏：
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0,Sheight-BackView_Y , Swidth, BackView_Y)];
    UIView *ToolView=[[UIView alloc]initWithFrame:CGRectMake(0,Picker.wwy_y-30, Swidth, 30)];
    ToolView.backgroundColor=UIColorFromHex(0x20B2AA);
    [BackView addSubview:ToolView];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn4:) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:noBtn];
    
    
    
    Picker.tag=0;
    Picker.backgroundColor=[UIColor whiteColor];
    Picker.dataSource=self;
    Picker.delegate=self;
    PickDateSubject=[[NSMutableArray alloc]initWithObjects:         @"请选择课程类型",
                                                                    @"理论课程",
                                                                    @"网络课程",
                                                                    @"实训课程",
                                                                    @"教学查房",
                                                                    @"病例讨论",
                                                                        nil
                                                                    ];
    
    
    
    SubjectID=[[NSMutableArray alloc]initWithObjects:@"请选择教案类型",@"classroom_course",@"online_course",@"operating_course",@"teaching_rounds",@"case_discussion", nil];
    
    
    
    if (Picker.tag == 0) {
        DateString = PickDateSubject[1];
        SubjectString=SubjectID[1];
    }
    
    [BackView addSubview:Picker];
    
    [[UIApplication sharedApplication].keyWindow addSubview:BackView];
}

-(void)setOkBtn4:(id)sender{
    
    if(DateString == nil){
        
        [self.CourseGenreBtn setTitle:@" " forState:UIControlStateNormal];

        BackView.hidden=YES;
    }else{
        [self.CourseGenreBtn setTitle:DateString forState:UIControlStateNormal];
        BackView.hidden=YES;
    }
}

#pragma mark 上课地点：
-(void)CourcseLocation:(UIButton *)sender{
     [self resignFirstRespond];
    if(BackView !=nil){
        BackView.hidden=YES;
    }
    
    CGFloat BackView_Y=Sheight/3;
    
    BackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    BackView.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.4];
    
    //工具栏：
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0,Sheight-BackView_Y , Swidth, BackView_Y)];
    UIView *ToolView=[[UIView alloc]initWithFrame:CGRectMake(0, Picker.wwy_y-30, Swidth, 30)];
    ToolView.backgroundColor=UIColorFromHex(0x20B2AA);
    [BackView addSubview:ToolView];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn3:) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:noBtn];
    
    
    
   Picker.backgroundColor=[UIColor whiteColor];
    Picker.tag=1;
    if (Picker.tag==1) {
        DateString = PickDateLocation[0];
        LocationString=LocationID[0];
    }
    Picker.delegate=self;
    Picker.dataSource=self;
    
    
    [BackView addSubview:Picker];
    
    [[UIApplication sharedApplication].keyWindow addSubview:BackView];
}

//返回数据列数：
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    
    if(pickerView.tag == 0){
        return [PickDateSubject count];
    }else{
        
        return [PickDateLocation count];
    }
}
//获取数据
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(pickerView.tag == 0){
        return [PickDateSubject objectAtIndex:row];
    }else{
        return [PickDateLocation objectAtIndex:row];
    }
}
//返回值：
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    
    if(pickerView.tag == 0){
        NSLog(@"%@",[PickDateSubject objectAtIndex:row]);
        DateString = [PickDateSubject objectAtIndex:row];
        
        SubjectString=[SubjectID objectAtIndex:row];
        NSLog(@"%@",SubjectString);
    }else{
        NSLog(@"%@",[PickDateLocation objectAtIndex:row]);
        DateString = [PickDateLocation objectAtIndex:row];
        
        LocationString=[LocationID objectAtIndex:row];
        NSLog(@"%@",LocationString);
    }
}


-(void)setOkBtn3:(id)sender{
    
    [self.CourseLocation setTitle:DateString forState:UIControlStateNormal];
    
    BackView.hidden=YES;
}





#pragma mark 课程状态：
-(void)CourseChange:(UISegmentedControl *)sender{
    
    
    if(sender.selectedSegmentIndex == 0){
        
        NSLog(@"选修课状态");
        if(self.RequiredView !=nil){
            self.RequiredView.hidden=YES;
        }
        BXKC=@(NO);
        KCZT=@"选修";
    }else if(sender.selectedSegmentIndex == 1){
        NSLog(@"必修课状态");
        self.RequiredView.hidden=NO;
        BXKC=@(YES);
        KCZT=@"必修";
    }
}

#pragma mark 必修课程状态
-(void)RequiredChange:(UISegmentedControl *)sender{
    
    if(sender.selectedSegmentIndex == 0){
        
        NSLog(@"选修课状态--公开");
        BXGK=@(YES);
        SFGK=@"公开";
        
    }else if(sender.selectedSegmentIndex == 1){
        NSLog(@"必修课状态--不公开");
        
        BXGK=@(NO);
        SFGK=@"不公开";
        
    }
}




#pragma mark 报名时间：
-(void)ApplyStratTime:(UIButton *)sender{

    //选择时间控件：
    [self ChooseApplyStratTime];
    BackBtn.hidden = NO;
    [self resignFirstRespond];
    [dateView show];
    
    
}

-(void)ChooseApplyStratTime{
CGFloat BackView_Y=Sheight/3;
    dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight-BackView_Y, Swidth, BackView_Y) andSureBlock:^(NSString *date, NSString *longDate) {
        BackBtn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
         ApplyStartTimeNunm=[longDate longLongValue];
        
        
        
        if(StartTimeNunm == 0 || EndTimeNum == 0){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"请先选择上课时间！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:true completion:nil];
        }else  if (ApplyStartTimeNunm < CurrentTimeNunm) {
            
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"报名开始时间早于当前时间，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                self.ApplyStartTime.layer.borderColor=[UIColor redColor].CGColor;
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            
            
            
        }else{
            self.ApplyStartTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
            [self.ApplyStartTime setTitle:date forState:0];
            ApplyStrateTimeLong = longDate;
        }

    } andCancelBlock:^{
        BackBtn.hidden = YES;
    }];
    [self.view addSubview:dateView];
 
}

//报名结束时间：
-(void)ApplyEndTime:(UIButton *)sender{
    //选择时间控件：
    [self ChooseApplyEndTime];
    BackBtn.hidden = NO;
    [self resignFirstRespond];
    [dateView show];
}


-(void)ChooseApplyEndTime{
    
    CGFloat BackView_Y=Sheight/3;
    dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight-BackView_Y, Swidth, BackView_Y) andSureBlock:^(NSString *date, NSString *longDate) {
        BackBtn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
        
        ApplyEndTimeLong = longDate;
        
        ApplyEndTimeNum=[longDate longLongValue];
        
        
        if(StartTimeNunm == 0 || EndTimeNum == 0){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"请先选择上课时间！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:true completion:nil];
        }else if (ApplyEndTimeNum > StartTimeNunm) {
            
            UIAlertController *alert2=[UIAlertController alertControllerWithTitle:@"提示" message:@"报名结束时间晚于上课时间，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction2=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                self.ApplyEndTime.layer.borderColor=[UIColor redColor].CGColor;
                
            }];
            
            [alert2 addAction:okAction2];
            [self presentViewController:alert2 animated:true completion:nil];
            
        }else{
            self.ApplyEndTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
             [self.ApplyEndTime setTitle:date forState:0];
        }

        
    } andCancelBlock:^{
        BackBtn.hidden = YES;
    }];
    [self.view addSubview:dateView];

}


-(void)setOkBtn6:(id)sender{
    
    NSDate *theDate=DatePick.date;
    ApplyEndTimeLong=[NSString stringWithFormat:@"%ld",(long)[theDate timeIntervalSince1970] * 1000];
    ApplyEndTimeNum= (long)[theDate timeIntervalSince1970] * 1000;
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    dateFormatter.dateFormat=@"MM/dd HH:mm";
    [self.ApplyEndTime setTitle:[dateFormatter stringFromDate:theDate] forState:UIControlStateNormal];
    
    if(StartTimeNunm == 0 || EndTimeNum == 0){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"请先选择上课时间！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:true completion:nil];
    }else if (ApplyEndTimeNum > StartTimeNunm) {
        
        UIAlertController *alert2=[UIAlertController alertControllerWithTitle:@"提示" message:@"报名结束时间晚于上课时间，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction2=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            self.ApplyEndTime.layer.borderColor=[UIColor redColor].CGColor;
            
        }];
        
        [alert2 addAction:okAction2];
        [self presentViewController:alert2 animated:true completion:nil];
        
    }else{
        self.ApplyEndTime.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
        BackView.hidden=YES;
    }
}


-(void)PlanName:(NSString *)Nametr PlanID:(NSString *)planId{
    
    [self.CoursePlan setTitle:Nametr forState:UIControlStateNormal];
    PlanName=Nametr;
    PlanID=planId;
    NSLog(@"%@",PlanID);
    
    
    
}

- (IBAction)SaveAction:(id)sender {
    
    
    if (PlanName.length == 0 ) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您未选择课程教案，无法正常创建课程！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else if (self.CoursePlan.titleLabel.text.length == 0) {
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您的教案ID为空，无法正常创建课程！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
    }else if ( StrateTimeLong.length == 0 || EndTimeLong.length == 0 ) {
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您未选择课程时间，无法正常创建课程！！" preferredStyle:UIAlertControllerStyleAlert];
        
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
        
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
    }else if([StrateTimeLong isEqualToString:EndTimeLong]){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"开始时间与结束时间相同，请重新选择。" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:true completion:nil];
        
    }else if (DateString.length == 0 ) {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您未选择上课地点，无法正常创建课程！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
    

    if (SubjectString.length == 0) {
        SubjectString=@"classroom_course";
    }
    
    if (ApplyNum.length==0) {
            ApplyNum=@"100000";
        }

    if ([KCZT isEqualToString:@"必修"]) {
            
        ApplyStrateTimeLong=[NSString stringWithFormat:@"%.0f", CurrentTimeNunm];
        NSLog(@"%@",ApplyStrateTimeLong);
        ApplyEndTimeLong=[NSString stringWithFormat:@"%.0f",StartTimeNunm-600000];
        NSLog(@"%@",ApplyEndTimeLong);
           
    }else{
        if (ApplyStrateTimeLong.length == 0) {
            ApplyStrateTimeLong=[NSString stringWithFormat:@"%.0f", CurrentTimeNunm];
            NSLog(@"%@",ApplyStrateTimeLong);
        }
            
        if (ApplyEndTimeLong.length == 0) {
            ApplyEndTimeLong=[NSString stringWithFormat:@"%.0f",StartTimeNunm-600000];
            NSLog(@"%@",ApplyEndTimeLong);
        }
    }
        
        
    
    
    NSLog(@"课程教案名称：%@  课程ID:%@ ",self.CoursePlan.titleLabel.text,PlanID);
    NSLog(@"上课时间:%@   下课时间：%@",StrateTimeLong,EndTimeLong);
    NSLog(@"上课地点:%@   课程类型：%@",LocationString,KTLX);
    NSLog(@"报名上限:%@   课程状态：%@ ",ApplyNum,KCZT);
    NSLog(@"报名开始时间:%@   报名结束时间：%@",ApplyStrateTimeLong,ApplyEndTimeLong);
    NSLog(@" 必修课状态：%@",SFGK);
    
    NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules",LocalIP];
        
        
        
        NSDictionary *params;
        if ([KCZT isEqualToString:@"必修"]) {
            params=@{
                     @"courseId":PlanID,
                     @"courseName":self.CoursePlan.titleLabel.text,
                     @"teacherId":LocalUserId,
                     @"startTime":StrateTimeLong,
                     @"endTime":EndTimeLong,
                     @"scheduleType":SubjectString,
                     @"capacity":ApplyNum,
                     @"classroomId":LocationString,
                     @"classroomName":DateString,
                     @"courseDescription":@"",
//                     @"registerStartTime":ApplyStrateTimeLong,
//                     @"registerEndTime":ApplyEndTimeLong,
                     @"live":BXKC,
                     @"required":BXGK
                     };
        }else{
            params=@{
                     @"courseId":PlanID,
                     @"courseName":self.CoursePlan.titleLabel.text,
                     @"teacherId":LocalUserId,
                     @"startTime":StrateTimeLong,
                     @"endTime":EndTimeLong,
                     @"scheduleType":SubjectString,
                     @"capacity":ApplyNum,
                     @"classroomId":LocationString,
                     @"classroomName":DateString,
                     @"courseDescription":@"",
                     @"registerStartTime":ApplyStrateTimeLong,
                     @"registerEndTime":ApplyEndTimeLong,
                     @"live":BXGK,
                     @"required":BXKC
                     };
        }
        
        
        
        
        
    
    NSLog(@"%@",params);
    
    [RequestTools RequestWithURL:URL Method:@"post" Params:params Success:^(NSDictionary *result) {
        NSLog(@"%@",result);

            NSString *resultStr=[result objectForKey:@"errorCode"];
            NSLog(@"resultStr:%@",resultStr);


            if([resultStr isKindOfClass:[NSNull class]]){
                [MBProgressHUD showToastAndMessage:@"创建课程成功!" places:0 toView:nil];
                [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            
        }else if ([resultStr isEqualToString:@"duplicate_room_in_course_schedule"]) {
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"同一时间该教室已被选中，无法正常创建课程！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }else if([resultStr isEqualToString:@"invalid_scheduled_time"]){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"填写时间有误，请核对后重新保存。温馨提示：此类状况多出现于报名时间于当前是时间相同。（报名时间应晚于当前时间并早于上课开始时间）" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else if([resultStr isEqualToString:@"duplicate_course_info"]){
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"重复的课程名称，请重新创建！！！" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } failed:^(NSString *result) {
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"result:%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];

    }
    
}
@end
