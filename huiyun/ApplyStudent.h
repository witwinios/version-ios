//
//  ApplyStudent.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/5.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplyStudent : NSObject

@property (nonatomic, strong) NSNumber *row;
@property(nonatomic,strong) NSString *ApplyStudentImg;
@property(nonatomic,strong) NSString *ApplyStudentName;
@property(nonatomic,strong) NSString *ApplyStudentpicture;

@property(nonatomic,strong) NSNumber *ApplyStudentID;

@property(nonatomic,assign)BOOL isCheck;

@end
