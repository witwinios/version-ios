//
//  CourseWareModel.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/2.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "CourseWareModel.h"

@implementation CourseWareModel

-(void)setCourseWareId:(NSNumber *)CourseWareId{
    if ([CourseWareId isKindOfClass:[NSNull class]]) {
        _CourseWareId = [NSNumber numberWithInt:0];
    }else{
        _CourseWareId = CourseWareId;
    }
}

-(void)setCourseWareName:(NSString *)CourseWareName{
    if ([CourseWareName isKindOfClass:[NSNull class]]) {
        _CourseWareName = @"暂无";
    }else{
        _CourseWareName = CourseWareName;
    }
}

-(void)setCourseWarePublic:(NSNumber *)CourseWarePublic{
    if ([CourseWarePublic isKindOfClass:[NSNull class]]) {
        _CourseWarePublic = [NSNumber numberWithInt:0];
    }else if([CourseWarePublic isEqual:@(true)]){
        _CourseWarePublic=@(YES);
    }else if([CourseWarePublic isEqual:@(false)]){
        _CourseWarePublic=@(NO);
    }
}

-(void)setCourseWareSubject:(NSString *)CourseWareSubject{
    if ([CourseWareSubject isKindOfClass:[NSNull class]]) {
        _CourseWareSubject = @"暂无";
    }else{
        _CourseWareSubject = CourseWareSubject;
    }
}

-(void)setCourseWareClass:(NSString *)CourseWareClass{
    if ([CourseWareClass isKindOfClass:[NSNull class]]) {
        _CourseWareClass = @"暂无";
    }else{
        _CourseWareClass = CourseWareClass;
    }
}

-(void)setFileFormat:(NSString *)FileFormat{
    if ([FileFormat isKindOfClass:[NSNull class]]) {
        _FileFormat = @"暂无";
    }else{
        _FileFormat = FileFormat;
    }
}

-(void)setFileUrl:(NSString *)FileUrl{
    if ([FileUrl isKindOfClass:[NSNull class]]) {
        _FileUrl = @"暂无";
    }else{
        _FileUrl = FileUrl;
    }
}


@end
