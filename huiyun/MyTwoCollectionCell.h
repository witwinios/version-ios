//
//  MyTwoCollectionCell.h
//  huiyun
//
//  Created by 王慕铁 on 2018/2/26.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionModel.h"
@interface MyTwoCollectionCell : UICollectionViewCell
@property(nonatomic,strong)UILabel *QuestionTypes;


-(void)setQuestions:(QuestionModel *)model;

@end
