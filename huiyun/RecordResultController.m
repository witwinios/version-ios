//
//  RecordResultController.m
//  huiyun
//
//  Created by MacAir on 2017/9/15.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "RecordResultController.h"

@interface RecordResultController ()

@end

@implementation RecordResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"出科意见";
    [backNavigation addSubview:titleLab];
    //
    self.studentSummary.layer.cornerRadius = 4;
    self.studentSummary.layer.masksToBounds = YES;
    self.teacherSummary.layer.cornerRadius = 4;
    self.teacherSummary.layer.masksToBounds = YES;
    self.supervisorSummary.layer.cornerRadius = 4;
    self.supervisorSummary.layer.masksToBounds = YES;
    self.tempBtn.layer.cornerRadius = 4;
    self.tempBtn.layer.masksToBounds = YES;
    self.saveBtn.layer.cornerRadius = 4;
    self.saveBtn.layer.masksToBounds = YES;
    //
    self.studentSummary.text = self.model.studentSummary;
    self.teacherSummary.text = self.model.teacherSummary;
    self.supervisorSummary.text = self.model.supervisorSummary;
}
#pragma -action
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.studentSummary resignFirstResponder];
    [self.teacherSummary resignFirstResponder];
    [self.supervisorSummary resignFirstResponder];
}
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)tempAction:(id)sender {
    NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/saveMentorFeedback",SimpleIp,self.model.SDepartRecordId];
    [RequestTools RequestWithURL:request Method:@"post" Params:@{@"mentorId":LocalUserId,@"mentorFeedback":self.teacherSummary.text} Message:@"暂存中..." Success:^(NSDictionary *result) {
        NSString *errorCode = [result[@"errorCode"] isKindOfClass:[NSNull class]]?@"":result[@"errorCode"];
        if ([errorCode isEqualToString:@""]) {
            self.model.teacherSummary = self.teacherSummary.text;
            //保存主管意见
            NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/saveSupervisorFeedback",SimpleIp,self.model.SDepartRecordId];
            [RequestTools RequestWithURL:request Method:@"post" Params:@{@"supervisorId":LocalUserId,@"supervisorFeedback":self.supervisorSummary.text} Message:@"暂存中..." Success:^(NSDictionary *result) {
                [MBProgressHUD showToastAndMessage:@"已暂存~" places:0 toView:nil];
                self.model.supervisorSummary = self.supervisorSummary.text;
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"暂存失败~" places:0 toView:nil];
            }];
        }else{
            [MBProgressHUD showToastAndMessage:@"主管意见保存失败~" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"带教意见保存失败~" places:0 toView:nil];
    }];
}
// 111 110 100 101 010 011 001 000

/*
 studentSummary:出科小结
 teacherSummary:带教意见
 supervisorSummary:主管意见
*/
- (IBAction)saveAction:(id)sender {
    
    NSString *url_Student=[NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/submitCheckOutSummary",SimpleIp,self.model.SDepartRecordId];
    NSString *url_Teacher=[NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/submitMentorFeedback",SimpleIp,self.model.SDepartRecordId];
    NSString *url_Supervisor=[NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/submitSupervisorFeedback",SimpleIp,self.model.SDepartRecordId];
    
    NSDictionary *dic_student=@{
                                @"studentSummary":self.studentSummary.text
                                };
    NSDictionary *dic_teacher=@{
                                @"directorId":LocalUserId,
                                @"directorFeedback":self.teacherSummary.text
                                };
    NSDictionary *dic_supervisor=@{
                                @"supervisorId":LocalUserId,
                                @"supervisorFeedback":self.supervisorSummary.text
                                };
    
    
    
    // 出科小结，带教意见，主管意见 全部存在
    if (self.studentSummary.text.length >0 && self.teacherSummary.text.length >0 && self.supervisorSummary.text.length >0) {
        NSLog(@"CK:%@-----DJ:%@----ZG:%@",self.studentSummary.text,self.teacherSummary.text,self.supervisorSummary.text);
           NSLog(@"0");
        
        
     //出科
        [RequestTools RequestWithURL:url_Student Method:@"post" Params:dic_student Success:^(NSDictionary *result) {
                                                                             
                NSLog(@"result_Student:%@",result);
                                                                             
            NSString *errorCode = [result[@"errorCode"] isKindOfClass:[NSNull class]]?@"":result[@"errorCode"];
            if ([errorCode isEqualToString:@""]) {
    //带教
                [RequestTools RequestWithURL:url_Teacher Method:@"post" Params:dic_teacher Success:^(NSDictionary *result) {
                    
                    NSLog(@"result_Student:%@",result);
                    
                    NSString *errorCode = [result[@"errorCode"] isKindOfClass:[NSNull class]]?@"":result[@"errorCode"];
                    if ([errorCode isEqualToString:@""]){
   //主管
                        [RequestTools RequestWithURL:url_Supervisor Method:@"post" Params:dic_supervisor Success:^(NSDictionary *result) {
                            
                            NSLog(@"result_Student:%@",result);
                            
                            NSString *errorCode = [result[@"errorCode"] isKindOfClass:[NSNull class]]?@"":result[@"errorCode"];
                            if ([errorCode isEqualToString:@""]){
                                 [MBProgressHUD showToastAndMessage:@"提交成功" places:0 toView:nil];
                            }else if([errorCode isEqualToString:@"supervisor_feedback_is_submit"]){
                                 [MBProgressHUD showToastAndMessage:@"教学秘书意见已提交" places:0 toView:nil];
                            }

                        } failed:^(NSString *result) {
                        [MBProgressHUD showToastAndMessage:@"主管意见，提交失败" places:0 toView:nil];
                        }];
 
                    }else if([errorCode isEqualToString:@"director_feedback_is_submit"]){
                        [MBProgressHUD showToastAndMessage:@"科主任意见已提交" places:0 toView:nil];
                    }
 
                } failed:^(NSString *result) {
                     [MBProgressHUD showToastAndMessage:@"带教意见，提交失败" places:0 toView:nil];
                }];
            }else if([errorCode isEqualToString:@"student_check_out_summary_is_submit"]){
                [MBProgressHUD showToastAndMessage:@"出科小结已经提交" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"出科小结，提交失败" places:0 toView:nil];
        }];
        

        
    // 出科小结，带教意见 存在 ，主管意见 不存在
    }else if (self.studentSummary.text.length >0 && self.teacherSummary.text.length >0 && self.supervisorSummary.text.length ==0) {
        NSLog(@"CK:%@-----DJ:%@----ZG:%@",self.studentSummary.text,self.teacherSummary.text,self.supervisorSummary.text);
           NSLog(@"1");
        [MBProgressHUD showToastAndMessage:@"主管意见是必填的呢~" places:0 toView:nil];
        
    // 出科小结 存在 ，主管意见，带教意见 不存在
    }else if (self.studentSummary.text.length >0 && self.teacherSummary.text.length == 0 && self.supervisorSummary.text.length == 0) {
        NSLog(@"CK:%@-----DJ:%@----ZG:%@",self.studentSummary.text,self.teacherSummary.text,self.supervisorSummary.text);
          NSLog(@"2");
         [MBProgressHUD showToastAndMessage:@"带教意见和主管意见都是必填的呢~" places:0 toView:nil];
        
    // 出科小结，主管意见 存在 ，带教意见 不存在
    }else if (self.studentSummary.text.length >0 && self.teacherSummary.text.length == 0 && self.supervisorSummary.text.length >0) {
        NSLog(@"CK:%@-----DJ:%@----ZG:%@",self.studentSummary.text,self.teacherSummary.text,self.supervisorSummary.text);
          NSLog(@"3");
        [MBProgressHUD showToastAndMessage:@"带教意见是必填的呢~" places:0 toView:nil];
        
    // 带教意见 存在，出科小结 ，主管意见 不存在
    }else if (self.studentSummary.text.length == 0 && self.teacherSummary.text.length >0 && self.supervisorSummary.text.length == 0) {
        NSLog(@"CK:%@-----DJ:%@----ZG:%@",self.studentSummary.text,self.teacherSummary.text,self.supervisorSummary.text);
          NSLog(@"4");
        [MBProgressHUD showToastAndMessage:@"主管意见是必填的呢~" places:0 toView:nil];
        
    //带教意见，主观意见 存在 ，出科小结 不存在
    }else if (self.studentSummary.text.length == 0 && self.teacherSummary.text.length >0 && self.supervisorSummary.text.length >0) {
        NSLog(@"CK:%@-----DJ:%@----ZG:%@",self.studentSummary.text,self.teacherSummary.text,self.supervisorSummary.text);
          NSLog(@"5");
        
        //带教
        [RequestTools RequestWithURL:url_Teacher Method:@"post" Params:dic_teacher Success:^(NSDictionary *result) {
            
            NSLog(@"result_Student:%@",result);
            
            NSString *errorCode = [result[@"errorCode"] isKindOfClass:[NSNull class]]?@"":result[@"errorCode"];
            if ([errorCode isEqualToString:@""]){
                //主管
                [RequestTools RequestWithURL:url_Supervisor Method:@"post" Params:dic_supervisor Success:^(NSDictionary *result) {
                    
                    NSLog(@"result_Student:%@",result);
                    
                    NSString *errorCode = [result[@"errorCode"] isKindOfClass:[NSNull class]]?@"":result[@"errorCode"];
                    if ([errorCode isEqualToString:@""]){
                        [MBProgressHUD showToastAndMessage:@"提交成功" places:0 toView:nil];
                    }else if([errorCode isEqualToString:@"supervisor_feedback_is_submit"]){
                        [MBProgressHUD showToastAndMessage:@"教学秘书意见已提交" places:0 toView:nil];
                    }
                    
                } failed:^(NSString *result) {
                    [MBProgressHUD showToastAndMessage:@"主管意见，提交失败" places:0 toView:nil];
                }];
                
            }else if([errorCode isEqualToString:@"director_feedback_is_submit"]){
                [MBProgressHUD showToastAndMessage:@"科主任意见已提交" places:0 toView:nil];
            }
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"带教意见，提交失败" places:0 toView:nil];
        }];

    //主管意见 存在，出科小结，带教意见 不存在
    }else if (self.studentSummary.text.length == 0 && self.teacherSummary.text.length == 0 && self.supervisorSummary.text.length >0) {
        NSLog(@"CK:%@-----DJ:%@----ZG:%@",self.studentSummary.text,self.teacherSummary.text,self.supervisorSummary.text);
          NSLog(@"6");
        [MBProgressHUD showToastAndMessage:@"带教意见是必填的呢~" places:0 toView:nil];
        
    //出科小结，带教意见，主管意见 都不存在
    }else if (self.studentSummary.text.length == 0 && self.teacherSummary.text.length == 0 && self.supervisorSummary.text.length == 0) {
        NSLog(@"CK:%@-----DJ:%@----ZG:%@",self.studentSummary.text,self.teacherSummary.text,self.supervisorSummary.text);
        NSLog(@"7");
        
       [MBProgressHUD showToastAndMessage:@"你什么都不填，我们怎么提交呀~" places:0 toView:nil];
        
    }
   
}
@end
