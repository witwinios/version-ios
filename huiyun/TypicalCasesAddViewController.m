//
//  TypicalCasesAddViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/11.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TypicalCasesAddViewController.h"

@interface TypicalCasesAddViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *courseTable;
    NSMutableArray *dataArray;
    int currentPage;
    
    NSInteger line;
    
    NSMutableArray *seleArr;
    
    UIView *SearchView;
    UITextField *SearchField;
    UIButton *SearchBtn;
     BOOL isSearch;
}

@end

@implementation TypicalCasesAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setsearchBar];
    [self setNav];
    [self setUI];
    [self setUpRefresh];
    
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"典型病案列表";
    
    
    [backNavigation addSubview:titleLab];
    
    
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [rightBtn setTitle:@"添加" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(Choice:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;
    
}
- (void)back :(UIButton *)button{
    if(seleArr.count >0){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您有尚未保存的病例，是否保存？" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self save];
        }];
        
        UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert addAction:noAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    };
}
-(void)Choice:(id)sender{
  //  [courseTable setEditing:YES animated:YES];
    
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您现在可以选择教案病例." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _editing=1;
        rightBtn.hidden=YES;
        SaveBtn.hidden=NO;
    }];
    
    UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:noAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
  
}
-(void)setsearchBar{
    
    
    isSearch=NO;
    
    SearchView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 44)];
    SearchView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    UIImageView *SearchImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sousuo"]];
    SearchImg.backgroundColor=[UIColor clearColor];
    SearchImg.frame=CGRectMake(10, 6, 32, 32);
    
    
    SearchField=[[UITextField alloc]initWithFrame:CGRectMake(44, 6, Swidth-88, 32)];
    SearchField.backgroundColor=[UIColor clearColor];
    SearchField.placeholder=@"请输入搜索的病案名称";
    SearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    SearchField.clearsOnBeginEditing = YES;
    SearchField.keyboardType=UIKeyboardTypeDefault;
    SearchField.returnKeyType=UIReturnKeySearch;
    SearchField.delegate = self;
    [SearchField addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    SearchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SearchBtn.frame=CGRectMake((SearchField.wwy_x+SearchField.wwy_width)+2, 6, 40, 32);
    [SearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [SearchBtn addTarget:self action:@selector(setUpRefresh) forControlEvents:UIControlEventTouchUpInside];
    SearchBtn.backgroundColor=UIColorFromHex(0x20B2AA);
    SearchBtn.layer.cornerRadius=5;
    SearchBtn.hidden=YES;
    [SearchView addSubview:SearchBtn];
    [SearchView addSubview:SearchField];
    [SearchView addSubview:SearchImg];
    [self.view addSubview:SearchView];
    
}


-(BOOL)ApplyTextDidChange:(UITextField *)theTextField{
    
    NSLog( @"text changed: %@", theTextField.text);
    
    
    if (![theTextField.text isEqualToString:@""]) {
        SearchBtn.hidden=NO;
        isSearch=YES;
    }else{
        isSearch=NO;
        [self setUpRefresh];
        SearchBtn.hidden=YES;
        [SearchField  resignFirstResponder];
    }
    
    return YES;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [SearchField  resignFirstResponder];
    
    
    [self setUpRefresh ];
    
    
    
    NSLog(@"点击了搜索");
    
    return YES;
    
}


-(void)setUI{
    currentPage = 1;
    CGFloat courseTable_Y=backNavigation.wwy_height+SearchView.wwy_height;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, courseTable_Y,Swidth, Sheight-courseTable_Y) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    courseTable.estimatedRowHeight = 0;
    [courseTable registerNib:[UINib nibWithNibName:@"TypicalCasesCell" bundle:nil] forCellReuseIdentifier:@"TypicalCases"];
    [self.view addSubview:courseTable];
    _editing=0;
    dataArray=[NSMutableArray new];
    seleArr=[NSMutableArray new];
    
}
-(void)setUpRefresh{
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中。。。";
}

//http://www.hzwitwin.cn:81/witwin-ctts-web/typicalCases?pageStart=1&pageSize=10&currentPage=1
-(void)loadData{
    currentPage=1;

    if (_editing) {
        [seleArr removeAllObjects];
    }
    NSString *Url;
    if(isSearch == NO){
       Url=[NSString stringWithFormat:@"%@/typicalCases?pageStart=%d&pageSize=10",LocalIP,currentPage];
        
    }else{
        NSString *str=SearchField.text;
        Url=[NSString stringWithFormat:@"%@/typicalCases?pageStart=%d&pageSize=10&caseName=%@",LocalIP,currentPage,str];
         Url=[Url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
    }
    
    
   
    NSLog(@"%@",Url);
    
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                
                [Maneger showAlert:@"无典型病例!" andCurentVC:self];
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    TypicalCasesModel *model=[TypicalCasesModel new];
                    model.TypicalCasesId=[dictionary objectForKey:@"caseId"];
                    model.TypicalCasesName=[dictionary objectForKey:@"caseName"];
                    model.TypicalCasesDiseaseICDName=[[dictionary objectForKey:@"diseaseICD"]objectForKey:@"diseaseICDName"];
                    model.TypicalCasesMedicalRecordsNum=[dictionary objectForKey:@"medicalRecordsNum"];
                    
                    
                    [dataArray addObject:model];
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
                
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
        
        
        
        
    } failed:^(NSString *result) {
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
    
}

-(void)loadRefresh{
    currentPage++;
   
    
    NSString *Url;
    if(isSearch == NO){
        Url=[NSString stringWithFormat:@"%@/typicalCases?pageStart=%d&pageSize=10",LocalIP,currentPage];
        
    }else{
        NSString *str=SearchField.text;
        Url=[NSString stringWithFormat:@"%@/typicalCases?pageStart=%d&pageSize=10&caseName=%@",LocalIP,currentPage,str];
        Url=[Url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
    }
    NSLog(@"%@",Url);
    
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                
                [Maneger showAlert:@"无典型病例!" andCurentVC:self];
                
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    TypicalCasesModel *model=[TypicalCasesModel new];
                    
                    model.TypicalCasesId=[dictionary objectForKey:@"caseId"];
                    model.TypicalCasesName=[dictionary objectForKey:@"caseName"];
                    model.TypicalCasesDiseaseICDName=[[dictionary objectForKey:@"diseaseICD"]objectForKey:@"diseaseICDName"];
                    model.TypicalCasesMedicalRecordsNum=[dictionary objectForKey:@"medicalRecordsNum"];
                    
                    [newData addObject:model];
                    
                }
                NSLog(@"newData=%lu",(unsigned long)newData.count);
                
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                
                
                [courseTable reloadData];
                
                
            }
        }
        else{
            currentPage--;
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}


#pragma mark---TableView
//行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

//点击Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_editing) {
        TypicalCasesModel *model=dataArray[indexPath.row];
        model.isCheck=!model.isCheck;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        model.TypicalCasesBool=@(YES);
        if (model.isCheck == 0) {
            [seleArr removeObject:model];
            NSLog(@"selectorPatnArray 取消:%@",seleArr);
        }else if(model.isCheck == 1){
             [seleArr addObject:model];
        }
        
       
    }
    
}




//加载的Cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TypicalCasesCell *cell=[tableView dequeueReusableCellWithIdentifier:@"TypicalCases"];
    
    TypicalCasesModel *model=dataArray[indexPath.row];
    cell.TypicalCasesBtn.hidden=YES;
    [cell setProperty:model];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 102.f;
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
//}
//
//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    //从选中中取消
//    if (seleArr.count > 0) {
//        TypicalCasesModel *model=dataArray[indexPath.row];
//
//        [seleArr removeObject:model];
//
//        NSLog(@"selectorPatnArray 取消:%@",seleArr);
//    }
//
//}
/*
 [courseTable setEditing:NO animated:YES];
 rightBtn.hidden=NO;
 SaveBtn.hidden=YES;
*/


// /courses/{courseId}/typicalCases
-(void)save{
    
    NSString *PlanId=[NSString stringWithFormat:@"%@",_PlanModel.PlanNameId];
    
    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/typicalCases",LocalIP,PlanId];
    
    [seleArr addObjectsFromArray:_TempArray];
    
    NSMutableArray *IDArr = [NSMutableArray new];
    for (TypicalCasesModel *model in seleArr) {
        [IDArr addObject:model.TypicalCasesId];
    }
    
    [RequestTools RequestWithURL:Url Method:@"post" Params:IDArr Success:^(NSDictionary *result) {
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            [MBProgressHUD showToastAndMessage:@"添加病案成功!" places:0 toView:nil];
            _backArr(seleArr);
            [seleArr removeAllObjects];
            [courseTable setEditing:NO animated:YES];
            rightBtn.hidden=NO;
            SaveBtn.hidden=YES;
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failed:^(NSString *result) {
        NSLog(@"result:%@",result);
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
}

@end
