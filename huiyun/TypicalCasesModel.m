//
//  TypicalCasesModel.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/10.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TypicalCasesModel.h"

@implementation TypicalCasesModel

-(void)setTypicalCasesId:(NSNumber *)TypicalCasesId{
    
    if ([TypicalCasesId isKindOfClass:[NSNull class]]) {
        _TypicalCasesId = [NSNumber numberWithInt:0];
    }else{
        _TypicalCasesId = TypicalCasesId;
    }
    
    
}

-(void)setTypicalCasesName:(NSString *)TypicalCasesName{
    if ([TypicalCasesName isKindOfClass:[NSNull class]]) {
        _TypicalCasesName = @"暂无";
    }else{
        _TypicalCasesName = TypicalCasesName;
    }
}

-(void)setTypicalCasesDiseaseICDName:(NSString *)TypicalCasesDiseaseICDName{
    if ([TypicalCasesDiseaseICDName isKindOfClass:[NSNull class]]) {
        _TypicalCasesDiseaseICDName = @"暂无";
    }else{
        _TypicalCasesDiseaseICDName = TypicalCasesDiseaseICDName;
    }
}

-(void)setTypicalCasesMedicalRecordsNum:(NSNumber *)TypicalCasesMedicalRecordsNum{
    if ([TypicalCasesMedicalRecordsNum isKindOfClass:[NSNull class]]) {
        _TypicalCasesMedicalRecordsNum = [NSNumber numberWithInt:0];
    }else{
        _TypicalCasesMedicalRecordsNum = TypicalCasesMedicalRecordsNum;
    }
}



@end
