//
//  BaseController.m
//  TestParrent
//
//  Created by MacAir on 2018/1/2.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "BaseController.h"
@interface BaseController ()

@end

@implementation BaseController

- (void)loadView{
    [super loadView];
    
    self.navibarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, SafeAreaTopHeight)];
    self.navibarView.backgroundColor = UIColorFromHex(0x20B2AA);
    self.navibarView.userInteractionEnabled = YES;
    [self.view addSubview:self.navibarView];
    
    self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftBtn.frame = CGRectMake(0, SafeAreaTopHeight - 5 - 25, 80, 25);
    self.leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [self.leftBtn setTitle:@"返回" forState:0];
    [self.leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [self.leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [self.navibarView addSubview:self.leftBtn];
    
    self.titleLab = [[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 50, SafeAreaTopHeight - 5 - 25, 100, 25)];
    [self.titleLab setTitleColor:[UIColor whiteColor] forState:0];;
    self.titleLab.titleLabel.font = [UIFont systemFontOfSize:20];
    [self.titleLab setTitle:@"我的标题" forState:0];
    [self.titleLab addTarget:self action:@selector(titleAction:) forControlEvents:UIControlEventTouchUpInside];
    self.titleLab.userInteractionEnabled = NO;
    [self.navibarView addSubview:self.titleLab];
    
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightBtn.frame = CGRectMake(self.view.frame.size.width-80, SafeAreaTopHeight - 5 -25, 80, 25);
    [self.rightBtn setTitle:@"待定" forState:0];
    [self.rightBtn setImage:[UIImage imageNamed:@"shaixuan"] forState:UIControlStateNormal];
    [self.rightBtn addTarget:self action:@selector(rightAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.navibarView addSubview:self.rightBtn];
    //主视图
    self.screenView = [[UIView alloc]initWithFrame:CGRectMake(0, SafeAreaTopHeight, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - SafeAreaTopHeight)];
    self.screenView.backgroundColor = [UIColor lightGrayColor];
    self.screenView.userInteractionEnabled = YES;
    [self.view addSubview:self.screenView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
}
#pragma -action
- (void)back:(UIButton *)btn{
    _barBC(0);
}
- (void)titleAction:(UIButton *)btn{
    _barBC(1);
}
- (void)rightAction:(UIButton *)btn{
    _barBC(2);
}
@end

