//
//  ExamPickView.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/4.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ExamPickView.h"
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@implementation ExamPickView
- (instancetype)initWithCurrentvc:(UIViewController *)currentVC
{
    self = [super init];
    if (self) {
        //实例化UI
        self = [[[NSBundle mainBundle] loadNibNamed:@"ExamPickView" owner:self options:nil] lastObject];
        self.frame = CGRectMake(0, HEIGHT, WIDTH, 250);
        [currentVC.view addSubview:self];
        //初始化控件
        [self.datePick  setTimeZone:[NSTimeZone localTimeZone]];
        [self.datePick setDate:[NSDate date]];
        self.datePick.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_ch"];
        self.datePick.timeZone = [NSTimeZone timeZoneWithName:@"GTM+8"];
        [self.datePick setDatePickerMode:UIDatePickerModeDateAndTime];

    }
    _isShow = [NSNumber numberWithInt:0];
    return self;
}
- (IBAction)cancelAction:(id)sender {
    [self hidePick];
}

- (IBAction)sureAction:(id)sender {
    [self hidePick];
    self.dateBlock(self.datePick.date);
}
- (void)showPick{
    _isShow = [NSNumber numberWithInt:1];
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, HEIGHT-250, WIDTH, self.frame.size.height);
        [self.superview bringSubviewToFront:self];
    }];
}
- (void)hidePick{
    _isShow = [NSNumber numberWithInt:0];
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, HEIGHT, WIDTH, self.frame.size.height);
    }];
}
@end
