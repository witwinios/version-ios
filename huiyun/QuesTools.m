//
//  QuesTools.m
//  huiyun
//
//  Created by MacAir on 2017/12/28.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "QuesTools.h"

@implementation QuesTools
+ (instancetype)shareInstance{
    static QuesTools *tools = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        tools = [QuesTools new];
    });
    
    return tools;
}
+ (NSMutableAttributedString *)getStr:(NSString *)string Color:(UIColor *)color{
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:string];
    [attributeStr addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(5, string.length - 5)];
    return attributeStr;
}
- (UIScrollView *)createUI:(QuestionModel *)model{
    UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight - 64 - 40 - 40)];
    self.currentScroll = scroll;
    //
    
    
    //
    return self.currentScroll;
}
@end
