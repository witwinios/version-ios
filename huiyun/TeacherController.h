//
//  TeacherController.h
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TribeMem.h"
#import "TeacherCell.h"
@interface TeacherController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) NSArray *tribeArray;
@end
