//
//  CourseQuestionModel.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CourseQuestionModel : NSObject

/*
 题型：questionType
 题目分数：difficulty
 子题数：childrenQuestionsNum
 题目：questionTitle
 分类：categoryName
 科目：subjectName
 选项：ChoiceOptions
 答案数组: AnswersArray
 答案：Answers
 解析:Analysis
 题目文件: questionFile
*/
@property(nonatomic,strong)NSNumber *QuestionId;
@property(nonatomic,strong)NSString *QuestionType;
@property(nonatomic,strong)NSNumber *Difficulty;
@property(nonatomic,strong)NSNumber *ChildrenQuestionsNum;
@property(nonatomic,strong)NSString *QuestionTitle;
@property(nonatomic,strong)NSString *CategoryName;
@property(nonatomic,strong)NSString *SubjectName;
@property(nonatomic,strong)NSString *ScoreNum;
@property(nonatomic,strong)NSMutableArray *ChoiceOptions;
@property(nonatomic,strong)NSMutableArray *AnswersArray;
@property(nonatomic,strong)NSNumber *Answers;
@property(nonatomic,strong)NSString *Analysis;
@property(nonatomic,strong)NSString *QuestionFile;

@property(nonatomic,strong)NSNumber *ChildrenQuestionId;
@property(nonatomic,strong)NSString *ChildrenQuestionTitle;
@property(nonatomic,strong)NSString *ChildrenQuestionType;
@property(nonatomic,strong)NSNumber *ChildrenDifficulty;
@property(nonatomic,strong)NSNumber *ChildrensQuestionsNum;
@property(nonatomic,strong)NSString *ChildrensCategoryName;
@property(nonatomic,strong)NSString *ChildrensSubjectName;
@property (strong, nonatomic) NSMutableArray *childArray;
@property(nonatomic,strong)NSMutableArray *ChildrensChoiceOptions;
@property(nonatomic,strong)NSString *ChildrensQuestionFile;

@property (nonatomic, assign) BOOL isCheck;

@end
