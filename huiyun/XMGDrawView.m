//
//  XMGDrawView.m
//  XMG-画板
//
//  Created by 张强 on 16/3/22.
//  Copyright © 2016年 XMG. All rights reserved.
//

#import "XMGDrawView.h"
#import "XMGBezierPath.h"


@interface XMGDrawView () 

@property (nonatomic,strong) NSMutableArray *paths;

@property (nonatomic,strong) XMGBezierPath *path;

//用来存放path的线宽属性
@property (nonatomic,assign) CGFloat lineW;

@property (nonatomic,strong) UIColor *lineColor;

@end


@implementation XMGDrawView

//懒加载
- (NSMutableArray *)paths
{
    if (!_paths) {
        _paths = [NSMutableArray array];
    }
    return _paths;
}

/**
 *  首先实现画线的功能，再慢慢完成其他功能
 */
- (void)awakeFromNib
{
    //设置画板颜色
    self.backgroundColor = [UIColor colorWithRed:230/255.0 green:208/255.0 blue:230/255.0 alpha:1];
    //初始化操作，画线需通过手势来完成，这里用pan手势来完成
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(pan:)];
    [self addGestureRecognizer:pan];
    
    //最开始不设置的话，一进来画线宽度为0；
    self.lineW = 2;
    self.lineColor = [UIColor blackColor];
}


//监听手势
- (void)pan:(UIPanGestureRecognizer *)pan
{
    CGPoint curP = [pan locationInView:pan.view];
    //通过贝塞尔曲线来画图
    
    
    if (pan.state == UIGestureRecognizerStateBegan) {
        
        XMGBezierPath *path = [XMGBezierPath bezierPath];

        //path创建好后，就可以设置其线宽，颜色等属性
        [path moveToPoint:curP];
        path.lineCapStyle = kCGLineCapRound;
        path.lineJoinStyle = kCGLineJoinRound;
        path.color = self.lineColor;
        path.lineWidth = self.lineW;
        
        
        self.path = path;
        
        [self.paths addObject:self.path];
    }
   
    else if (pan.state == UIGestureRecognizerStateChanged )
    {
        
        [self.path addLineToPoint:curP];
        
        //每次拖动都需要重绘
        [self setNeedsDisplay];
        
    }
    
}

//清除
- (void)clear
{
    [self.paths removeAllObjects];
    [self setNeedsDisplay];
}

//撤销
- (void)deleteLast
{
    [self.paths removeLastObject];
    [self setNeedsDisplay];
}

//设置线宽
- (void)lineWidthWithFloat:(CGFloat)width
{
    self.lineW = width;
}

//设置颜色
- (void)lineColorWithColor:(UIColor *)color
{
    self.lineColor = color;
}

//橡皮擦
- (void)erase
{
    [self lineColorWithColor:[UIColor colorWithRed:230/255.0 green:208/255.0 blue:230/255.0 alpha:1]];
}

- (void)drawRect:(CGRect)rect
{
   
    
    for (XMGBezierPath *path in self.paths) {
        //注意，设置颜色只能在drawRect方法里面进行
        //注意，取出每条线的颜色，而不是用self.color去绘制
        if ([path isKindOfClass:[UIImage class]]) {
            UIImage *image = (UIImage *)path;
            [image drawInRect:rect];
        }
        else{
        [path.color set];
        [path stroke];
        }
    
    }
}


//重写newImage方法进行传值
- (void)setImage:(UIImage *)image
{
    _image = image;
    
    [self.paths addObject:image];
    
    [self setNeedsDisplay];
}
@end
// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com