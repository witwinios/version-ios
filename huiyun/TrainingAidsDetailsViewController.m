//
//  TrainingAidsDetailsViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/11.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TrainingAidsDetailsViewController.h"

@interface TrainingAidsDetailsViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;

    UITableView *courseTable;
    
    //第一部分Cell
    NSArray *titleArray;
    NSArray *contentArray;
    //第二部分cell
    NSArray *TitleCellArray;
    NSArray *CellcontentArray;
    
    TCourseDatailCell *cell1;
}

@end

@implementation TrainingAidsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNav];
    [self setUI];
}

-(void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"教具详情";
    [backNavigation addSubview:titleLab];
    
    
   
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUI{
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-100) style:UITableViewStylePlain];
    courseTable.showsVerticalScrollIndicator = NO;
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [courseTable registerNib:[UINib nibWithNibName:@"TCourseDatailCell" bundle:nil] forCellReuseIdentifier:@"cellStyle1"];
    [courseTable registerNib:[UINib nibWithNibName:@"DesCell" bundle:nil] forCellReuseIdentifier:@"cellStyle2"];
    [self.view addSubview:courseTable];
    
    
    //第一部分：
    titleArray=@[@"教具品名:",@"教具分类:",@"规格型号:",@"库存数量:",@"说明:"];
    
    NSString *ModelItemsNum=[NSString stringWithFormat:@"%@",self.Model.ModelItemsNum];
    
contentArray=@[self.Model.ModelName,self.Model.ModelCategoryName,self.Model.ModelNo,ModelItemsNum,self.Model.ModelComments];
    
    NSLog(@"ModelComments===%@",self.Model.ModelComments);
    //第二部分：
    TitleCellArray=@[@"使用说明"];
    
    
}

//返回分组数：
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

//返回每组行数：
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section ==0) {
        return 5;
    }else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 4) {
            return 120.f;
        }
        return 44.f;
    }
    return 44.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return 20.f;
    }
    return 20.f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        UIView *TitleView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 30)];
        TitleView.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.5];
        UILabel *TitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, 200, 10)];
        TitleLabel.textColor=[UIColor whiteColor];
        TitleLabel.text=@"教具基本信息:";
        TitleLabel.font=[UIFont systemFontOfSize:18];
        [TitleView addSubview:TitleLabel];
        return TitleView;
    }else{
        
        UIView *TitleView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 30)];
        TitleView.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.5];
        UILabel *TitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, 200, 10)];
        TitleLabel.textColor=[UIColor whiteColor];
        TitleLabel.text=@"教具其他信息:";
        TitleLabel.font=[UIFont systemFontOfSize:18];
        [TitleView addSubview:TitleLabel];
        return TitleView;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"该功能正在开发" message:@"请持续关注" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            
            [AlertView addAction:BackAction];
            [self presentViewController:AlertView animated:YES completion:nil];
        }
    }
    
    
    
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        
        if (indexPath.row == 4) {
            
            //课程简介：
            DesCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"cellStyle2"];
            cell2.detailDes.text = contentArray[indexPath.row];
            cell2.LabelText.text=@"教具简介：";
   
            return cell2;
            
        }else {
            //平常cell:

            cell1 = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
            [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            cell1.title.text = titleArray[indexPath.row];
            
            cell1.content.text = contentArray[indexPath.row];
            
            cell1.RightImg.hidden=YES;
            return cell1;
            
        }
        
    }else {
        TCourseDatailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell.title.text=TitleCellArray[indexPath.row];
        
        return cell;
    }
}


@end
