//
//  ActivityDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/10/16.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "ActivityDetailController.h"

@interface ActivityDetailController ()
{
    UITableView *table;
    NSArray *titleArray;
    NSArray *dataArray;
    NSArray *heightArr;
    
    UITextView *currentTextview;
    CGRect _activedTextFieldRect;
    CGPoint currentPoint;
    UIView *toolbar;
    
    PersonEntity *persion;
}
@end

@implementation ActivityDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    //注册键盘出现的通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];

    titleArray = @[@"活动名称:",@"组织人:",@"活动时长:",@"活动时间:",@"带教老师:",@"活动内容:",@"指导意见:"];
    dataArray = @[_model.caseActivityName,_model.casePersion,[NSString stringWithFormat:@"%@",_model.caseInternal],[[Maneger shareObject] timeFormatter1:_model.caseDate.stringValue],_model.caseTeacher,_model.caseActivityContent,_model.caseAdvice];
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"103",@"103"];
    
    persion = [[NSuserDefaultManager share] readCurrentUser];
    
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    if ([self.model.caseStatus isEqualToString:@"待审核"] && persion.roleId.intValue != 2) {
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame = CGRectMake(Swidth-70, 29.5, 70, 25);
        [rightBtn setTitle:@"修改" forState:0];
        [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
        [rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if (![_SDepartModel.SDepartStatus isEqualToString:@"出科"]) {
             [backNavigation addSubview:rightBtn];
        }
       
    }

    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"活动记录";
    [backNavigation addSubview:titleLab];
    //
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.bounces = NO;
    table.delegate = self;
    table.dataSource = self;
    table.showsVerticalScrollIndicator = NO;
    [table registerNib:[UINib nibWithNibName:@"DetailCellOne" bundle:nil] forCellReuseIdentifier:@"one"];
    [table registerNib:[UINib nibWithNibName:@"DetailCellTwo" bundle:nil] forCellReuseIdentifier:@"two"];
    [self.view addSubview:table];
    
}
#pragma -action
- (void)keyboardWillHide:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
        table.contentOffset = currentPoint;
    }];
}
- (void)hideKb:(UIButton *)btn{
    [currentTextview resignFirstResponder];
}
- (void)keyboardWillShow:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
    if ((_activedTextFieldRect.origin.y + _activedTextFieldRect.size.height) + 40 >  ([UIScreen mainScreen].bounds.size.height - rect.size.height)) {//键盘的高度 高于textView的高度 需要滚动
        currentPoint = table.contentOffset;
        [UIView animateWithDuration:duration animations:^{
            table.contentOffset = CGPointMake(0, 64 + _activedTextFieldRect.origin.y + _activedTextFieldRect.size.height - ([UIScreen mainScreen].bounds.size.height - rect.size.height));
        }];
    }
}
- (void)approveAction:(UIButton *)appBtn{
    
    if ([currentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"请输入指导意见~" places:0 toView:nil];
        return;
    }
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认通过?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentActivityRecords/%@/review",SimpleIp,_model.recordId];
        
        [RequestTools RequestWithURL:request Method:@"PUT" Params:@{@"reviewerId":persion.userID,@"reviewerComments":currentTextview.text,@"reviewStatus":@"approved"} Message:@"审批中..." Success:^(NSDictionary *result) {
            
            [MBProgressHUD showToastAndMessage:@"审批成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"审批失败~" places:0 toView:nil];
        }];
        
    }];
    [alertVC addAction:cancel];
    [alertVC addAction:sure];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    _activedTextFieldRect = [textView convertRect:textView.frame toView:table];
    currentTextview = textView;
    return YES;
}
- (void)rejectAction:(UIButton *)rejBtn{
    if ([currentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"请输入指导意见~" places:0 toView:nil];
        return;
    }

    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认未通过?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentActivityRecords/%@/review",SimpleIp,_model.recordId];
        
        [RequestTools RequestWithURL:request Method:@"PUT" Params:@{@"reviewerId":persion.userID,@"reviewerComments":currentTextview.text,@"reviewStatus":@"rejected"} Message:@"审批中..." Success:^(NSDictionary *result) {
            
            [MBProgressHUD showToastAndMessage:@"审批成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"审批失败~" places:0 toView:nil];
        }];
    }];
    [alertVC addAction:cancel];
    [alertVC addAction:sure];
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)selectAction:(UIButton *)btn{
    [SubjectTool share].subjectType = subjectTypeUpdate;
    ActivityRecordController *recordVC = [ActivityRecordController new];
    recordVC.myModel = self.model;
    [self.navigationController pushViewController:recordVC animated:YES];
}

#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (persion.roleId.intValue == 2 && [_model.caseStatus isEqualToString:@"待审核"]) {
        return 40.f;
    }
    return 0.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *foot = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 40)];
    foot.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIButton *approveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    approveBtn.frame = CGRectMake(0, 0, Swidth/2, 40);
    approveBtn.backgroundColor = UIColorFromHex(0x20B2AA);
    [approveBtn setTitle:@"通过" forState:0];
    [approveBtn addTarget:self action:@selector(approveAction:) forControlEvents:UIControlEventTouchUpInside];
    [foot addSubview:approveBtn];
    
    UIButton *rejectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rejectBtn.frame = CGRectMake(Swidth/2, 0, Swidth/2, 40);
    rejectBtn.backgroundColor = [UIColor redColor];
    [rejectBtn setTitle:@"未通过" forState:0];
    [rejectBtn addTarget:self action:@selector(rejectAction:) forControlEvents:UIControlEventTouchUpInside];
    [foot addSubview:rejectBtn];
    return foot;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *oneCell = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *twoCell = [tableView dequeueReusableCellWithIdentifier:@"two"];
    if (indexPath.row == 6 || indexPath.row == 5){
        if (persion.roleId.intValue == 2 && indexPath.row == 6 && [_model.caseStatus isEqualToString:@"待审核"]) {
            currentTextview = twoCell.contentText;
            currentTextview.delegate = self;
            [twoCell.contentText setEditable:YES];
        }else{
            [twoCell.contentText setEditable:NO];
        }
        twoCell.titleText.text = titleArray[indexPath.row];
        twoCell.contentText.text = dataArray[indexPath.row];
        return twoCell;
    }else{
        oneCell.titleLab.text = titleArray[indexPath.row];
        oneCell.contentLab.text = dataArray[indexPath.row];
        return oneCell;
    }
}
@end
