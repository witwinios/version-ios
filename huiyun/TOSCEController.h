//
//  OSCEDetailController.h
//  xiaoyun
//
//  Created by MacAir on 17/1/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OSCEModel.h"
#import "Maneger.h"
#import "osceDatailCellStyle2.h"
#import "osceDetailCellStyle1.h"
#import "TimeBlockController.h"
#import "StationsController.h"
@interface TOSCEController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) OSCEModel *model;
@property (strong, nonatomic) TimeBlockController *timeVC;
@property (strong, nonatomic) StationsController *stationVC;
@end
