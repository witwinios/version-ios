//
//  TCourseModel.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TCourseModel.h"

@implementation TCourseModel
-(void)setScheduleTyoe:(NSString *)scheduleTyoe{
    if ([scheduleTyoe isKindOfClass:[NSNull class]]) {
        _scheduleTyoe = @"暂无";
    }else if ([scheduleTyoe isEqualToString:@"classroom_course"]){
        _scheduleTyoe = @"课堂课程";
    }else if ([scheduleTyoe isEqualToString:@"online_course"]){
        _scheduleTyoe = @"在线课程";
    }else if ([scheduleTyoe isEqualToString:@"operating_course"]){
        _scheduleTyoe = @"实操课程";
    }else if([scheduleTyoe isEqualToString:@"teaching_rounds"]){
        _scheduleTyoe=@"教学查房";
    }else if([scheduleTyoe isEqualToString:@"case_discussion"]){
        _scheduleTyoe=@"病例讨论";
    }
}
- (void)setScheduleStatus:(NSString *)scheduleStatus{
    if ([scheduleStatus isEqualToString:@"planning"]) {
        _scheduleStatus = @"计划中";
    }else if ([scheduleStatus isEqualToString:@"started"]){
        _scheduleStatus = @"正在上课";
    }else if ([scheduleStatus isEqualToString:@"released"]){
        _scheduleStatus = @"已发布";
    }else if([scheduleStatus isEqualToString:@"completed"]){
        _scheduleStatus  = @"已完成";
    }else{
        _scheduleStatus = scheduleStatus;
    }
}
- (void)setScheduleID:(NSNumber *)scheduleID{
    if ([scheduleID isKindOfClass:[NSNull class]]) {
        _scheduleID = [NSNumber numberWithInt:0];
    }else{
        _scheduleID = scheduleID;
    }
}
- (void)setCoursePlace:(NSString *)coursePlace{
    if ([coursePlace isKindOfClass:[NSNull class]]) {
        _coursePlace = @"暂无";
    }else{
        _coursePlace = coursePlace;
    }
}
- (void)setCourseSubjec:(NSString *)courseSubjec{
    if ([courseSubjec isKindOfClass:[NSNull class]]) {
        _courseSubjec = @"无科目";
    }else{
        _courseSubjec = courseSubjec;
    }
}
- (void)setCourseName:(NSString *)courseName{
    if ([courseName isKindOfClass:[NSNull class]]) {
        _courseName = @"暂无";
    }else{
        _courseName = courseName;
    }
}
- (void)setStartTime:(NSNumber *)startTime{
    if ([startTime isKindOfClass:[NSNull class]]) {
        _startTime = [NSNumber numberWithInt:0];
    }else{
        _startTime = startTime;
    }
}
- (void)setTeacherName:(NSString *)teacherName{
    if ([teacherName isKindOfClass:[NSNull class]]) {
        _teacherName = @"暂无";
    }else{
        _teacherName = teacherName;
    }
}
- (void)setRegistNum:(NSNumber *)registNum{
    if ([registNum isKindOfClass:[NSNull class]]) {
        _registNum = [NSNumber numberWithInt:0];
    }else{
        _registNum = registNum;
    }
}
- (void)setCourseDes:(NSString *)courseDes{
    if ([courseDes isKindOfClass:[NSNull class]]) {
        _courseDes = @"暂无";
    }else{
        _courseDes = courseDes;
    }
}
-(void)setRoomID:(NSNumber *)roomID{
    if ([roomID isKindOfClass:[NSNull class]]) {
        _roomID = [NSNumber numberWithInt:0];
    }else{
        _roomID = roomID;
    }
}

-(void)setRoomName:(NSString *)roomName{
    if ([roomName isKindOfClass:[NSNull class]]) {
        _roomName = @"暂无";
    }else{
        _roomName = roomName;
    }
}
- (void)setEndTime:(NSNumber *)endTime{
    if ([endTime isKindOfClass:[NSNull class]]) {
        _endTime = @0;
    }else{
        _endTime = endTime;
    }
}

-(void)setCourseCategoryName:(NSString *)courseCategoryName{
    if ([courseCategoryName isKindOfClass:[NSNull class]]) {
        _courseCategoryName = @"暂无";
    }else{
        _courseCategoryName = courseCategoryName;
    }
}

-(void)setCourseId:(NSNumber *)courseId{
    if ([courseId isKindOfClass:[NSNull class]]) {
        _courseId = [NSNumber numberWithInt:0];
    }else{
        _courseId = courseId;
    }
}

-(void)setSignstartTime:(NSNumber *)signstartTime{
    if ([signstartTime isKindOfClass:[NSNull class]]) {
        _signstartTime = [NSNumber numberWithInt:0];
    }else{
        _signstartTime = signstartTime;
    }
}

-(void)setSignendTime:(NSNumber *)signendTime{
    if ([signendTime isKindOfClass:[NSNull class]]) {
        _signendTime = [NSNumber numberWithInt:0];
    }else{
        _signendTime = signendTime;
    }
}

-(void)setCapacity:(NSNumber *)capacity{
    if ([capacity isKindOfClass:[NSNull class]]) {
        _capacity = [NSNumber numberWithInt:0];
    }else{
        _capacity = capacity;
    }
}

-(void)setTeacherId:(NSNumber *)teacherId{
    if ([teacherId isKindOfClass:[NSNull class]]) {
        _teacherId = [NSNumber numberWithInt:0];
    }else{
        _teacherId = teacherId;
    }
}


-(void)setLive:(NSNumber *)live{
    if ([live isKindOfClass:[NSNull class]]) {
        _live = [NSNumber numberWithInt:0];
    }else{
        _live = live;
    }
}

-(void)setCredit:(NSNumber *)credit{
    if ([credit isKindOfClass:[NSNull class]]) {
        _credit = [NSNumber numberWithInt:0];
    }else{
        _credit = credit;
    }
}

-(void)setRequired:(NSNumber *)required{
    if ([required isKindOfClass:[NSNull class]]) {
        _required = [NSNumber numberWithInt:0];
    }else{
        _required = required;
    }
}

@end
