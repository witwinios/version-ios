//
//  LeaveController.h
//  xiaoyun
//
//  Created by MacAir on 17/2/17.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeaveCellStyle1.h"
#import "LeaveCellStyle2.h"
#import "YCPickerView.h"
#import "Maneger.h"
#import "LoadingView.h"
#import "RequestTools.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "LeaveHistoryController.h"
typedef void (^MyBasicBlock)(id result);
@interface LeaveController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>
@property(retain,nonatomic)UIView *mohuView;
@property (strong, nonatomic) UIImageView *localImage;
@property (nonatomic, copy) MyBasicBlock myBlock;
@property (strong, nonatomic)UIImagePickerController *imagePickController;
@end
