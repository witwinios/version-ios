//
//  LoadingView.h
//  UIActivityIndicatorViewDemo
//
//  Created by qianfeng on 15/8/19.
//  Copyright (c) 2015年 LiuYaNan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

// 正在加载的字 有外界可配
@property (nonatomic, copy) NSString * loadTitle;
// 显示加载框
- (void)showLoadingView;
// 隐藏加载框
- (void)hiddenLoadingView;

@end
