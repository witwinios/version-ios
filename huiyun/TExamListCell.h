//
//  TExamListCell.h
//  huiyun
//
//  Created by MacAir on 2018/1/16.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SkillModel.h"
#import "OSCEModel.h"
#import "onlineModel.h"
@interface TExamListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *examNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *examStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *examStatus;
@property (weak, nonatomic) IBOutlet UILabel *examPlaceLabel;
@property (weak, nonatomic) IBOutlet UILabel *examInternalLabel;
@property (weak, nonatomic) IBOutlet UILabel *examDateLabel;

@end
