//
//  OnlineStudentController.m
//  huiyun
//
//  Created by MacAir on 2018/2/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "OnlineStudentController.h"

@interface OnlineStudentController ()
{
    UIView *toolbar;
    
}
@end

@implementation OnlineStudentController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArr = [NSMutableArray new];
    self.resultArr = [NSMutableArray new];
    __weak UIViewController *weakSelf = self;
    self.barBC = ^(int s){
        if (s == 0) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    };
    self.rightBtn.hidden = YES;
    [self.titleLab setTitle:@"考生列表" forState:0];
    //
    [self setUI];
    [self setupRefresh];
    //
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"确定" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonHide:) name:UIKeyboardWillHideNotification object:nil];

}
- (void)setUI{
    _seachBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, Swidth-50, 35)];
    _seachBar.delegate = self;
    
    _seachBar.placeholder = @"请输入学生姓名";
    [self.screenView addSubview:_seachBar];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(Swidth-45, 0, 40, 35);
    [cancelBtn setTitle:@"取消" forState:0];
    cancelBtn.layer.cornerRadius = 5;
    [cancelBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateHighlighted];
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:0];
    cancelBtn.layer.masksToBounds = YES;
    [cancelBtn addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.screenView addSubview:cancelBtn];

    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 35, Swidth, self.screenView.frame.size.height-35) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [_tableView registerNib:[UINib nibWithNibName:@"OnlineStudentCell" bundle:nil] forCellReuseIdentifier:@"stuCell"];
    [self.screenView addSubview:_tableView];
}
- (void)setupRefresh {
    [_tableView addHeaderWithTarget:self action:@selector(downRefresh)];
//    [_tableView addFooterWithTarget:self action:@selector(moreRefresh)];
    [_tableView headerBeginRefreshing];
}
- (void)downRefresh{
    [_dataArr removeAllObjects];
    _seachBar.text = @"";
    
    NSString *str = [NSString stringWithFormat:@"%@/testSchedules/%@/registeredStudents?pageSize=999",LocalIP,self.model.onlineId];
    NSLog(@"%@",str);
    [RequestTools RequestWithURL:str Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [_tableView headerEndRefreshing];
        
        NSArray *arr = result[@"responseBody"][@"result"];
        for (int s=0; s<arr.count; s++) {
            NSDictionary *dic = arr[s];
            OnlineStudentModel *stuModel = [OnlineStudentModel new];
            stuModel.stuId = dic[@"userId"];
            stuModel.name = dic[@"fullName"];
            stuModel.professin = @"暂无";
            stuModel.className = dic[@"grade"];
            stuModel.inTime = dic[@"registeredTime"];
            [_dataArr addObject:stuModel];
        }
        _resultArr = _dataArr;
        [_tableView reloadData];
    }failed:^(NSString *result) {
        [_tableView headerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"失败,尝试重新刷新~" places:0 toView:nil];
    }];
}
- (void)moreRefresh{
    
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 117.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OnlineStudentModel *model = _dataArr[indexPath.row];
    OnlineStudentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"stuCell"];
    cell.stuStatus.hidden = YES;
    cell.nameLab.text = model.name;
    cell.oneContent.text = model.professin;
    cell.twoContent.text = model.className;
    if (model.inTime.intValue == 0) {
        cell.threeContent.text = @"暂无";
    }else{
        cell.threeContent.text = [[Maneger shareObject] timeFormatter:model.inTime.stringValue];
    }
    return cell;
}
//
- (void)hideKb:(UIButton *)btn{
    [_seachBar resignFirstResponder];
    NSMutableArray *arr = [NSMutableArray new];
    for (int s=0; s<_resultArr.count; s++) {
        OnlineStudentModel *model = _resultArr[s];
        if ([model.name containsString:_seachBar.text]) {
            [arr addObject:model];
        }
    }
    _dataArr = arr;
    [_tableView reloadData];
}
-(void)doneButtonshow: (NSNotification *)notification {
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
}
- (void)doneButtonHide:(NSNotification *)notification{
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
    }];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText isEqualToString:@""]) {
        _dataArr = _resultArr;
        [_tableView reloadData];
    }
}
- (void)cancelAction:(UIButton *)btn{
    _seachBar.text = @"";
    _dataArr = _resultArr;
    [_tableView reloadData];
    [_seachBar resignFirstResponder];
}

@end
