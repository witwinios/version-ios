//
//  MarkCell.h
//  yun
//
//  Created by MacAir on 2017/7/21.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarkTableModel.h"
typedef void (^FieldAction)(NSNumber *row,NSString *str);
typedef void (^TextviewAction)(NSNumber *row,NSString *str);
typedef void (^SelectView)(UIView *select);
typedef void (^test)(NSString *str,NSNumber *row);
@interface MarkCell : UITableViewCell<UITextViewDelegate,UITextFieldDelegate>
//传值回调
@property (strong, nonatomic) NSNumber *indexRow;
@property (strong, nonatomic) SelectView selectView;
@property (strong, nonatomic) FieldAction fieldAction;
@property (strong, nonatomic) TextviewAction textAction;
@property (strong, nonatomic) NSIndexPath *indexPath;

@property (strong, nonatomic) MarkTableModel *model;
@property (strong, nonatomic) NSNumber *cellHeight;

@property (strong, nonatomic) UILabel *indexLab;

@property (strong, nonatomic) UILabel *categoryLab;
@property (strong, nonatomic) UILabel *categoryContentLab;
//评分项目
@property (strong, nonatomic) UILabel *markLab;
@property (strong, nonatomic) UILabel *markContentLab;
//操作内容
@property (strong, nonatomic) UILabel *operateLab;
@property (strong, nonatomic) UILabel *operateContentLab;
//
@property (strong, nonatomic) UILabel *keyLab;
@property (strong, nonatomic) UILabel *keyContentLab;
//
@property (strong, nonatomic) UILabel *totalScore;
@property (strong, nonatomic) UILabel *totalScoreContent;
@property (strong, nonatomic) UILabel *getscoreValue;
//
@property (strong, nonatomic) UILabel *dockscoreValue;
@property (strong, nonatomic) UITextField *dockField;
//
@property (strong, nonatomic) UILabel *dockreasonLab;
@property (strong, nonatomic) UITextView *dockreasonContent;

- (void)setModel:(MarkTableModel *)model andIndexPath:(NSIndexPath *)indexPath;
@end
