//
//  MyQuesModel.m
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "MyQuesModel.h"

@implementation MyQuesModel
- (void)setStatus:(NSString *)status{
    if ([status isEqualToString:@"SOLVED"]) {
        _status = @"已解决";
    }else{
        _status = @"未解决";
    }
}
- (void)setQuesDes:(NSString *)quesDes{
    if ([quesDes isKindOfClass:[NSNull class]]) {
        _quesDes = @"无";
    }else{
        _quesDes = quesDes;
    }
}
- (void)setQuesTitle:(NSString *)quesTitle{
    if ([quesTitle isKindOfClass:[NSNull class]]) {
        _quesTitle = @"无";
    }else{
        _quesTitle = quesTitle;
    }
}
@end
