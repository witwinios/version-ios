//
//  BlankCardController.h
//  yun
//
//  Created by MacAir on 2017/8/29.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlankCardController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) AccountEntity *entity;
@end
