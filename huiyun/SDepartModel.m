//
//  SDepartModel.m
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SDepartModel.h"

@implementation SDepartModel

- (void)setSDepartStartTime:(NSNumber *)SDepartStartTime{
    if ([SDepartStartTime isKindOfClass:[NSNull class]]) {
        _SDepartStartTime = @0;
    }else{
        _SDepartStartTime = SDepartStartTime;
    }
}
- (void)setSDepartEndTime:(NSNumber *)SDepartEndTime{
    if ([SDepartEndTime isKindOfClass:[NSNull class]]) {
        SDepartEndTime = @0;
    }else{
        _SDepartEndTime = SDepartEndTime;
    }
}
- (void)setSDepartName:(NSString *)SDepartName{
    if ([SDepartName isKindOfClass:[NSNull class]]) {
        _SDepartName = @"暂无";
    }else{
        _SDepartName = SDepartName;
    }
}
- (void)setSDepartStatus:(NSString *)SDepartStatus{
    if ([SDepartStatus isKindOfClass:[NSNull class]]) {
        _SDepartStatus = @"无";
    }else if ([SDepartStatus isEqualToString:@"not_start"]){
        _SDepartStatus = @"未入科";
    }else if ([SDepartStatus isEqualToString:@"off_department"]){
        _SDepartStatus = @"出科";
    }else if ([SDepartStatus isEqualToString:@"canceled"]){
        _SDepartStatus = @"取消";
    }else{
        _SDepartStatus = @"在科";
    }
}
- (void)setSDepartPlace:(NSString *)SDepartPlace{
    if ([SDepartPlace isKindOfClass:[NSNull class]]) {
        _SDepartPlace = @"暂无";
    }else{
        _SDepartPlace = SDepartPlace;
    }
}
- (void)setSDepartRecordName:(NSString *)SDepartRecordName{
    if ([SDepartRecordName isKindOfClass:[NSNull class]]) {
        _SDepartRecordName = @"暂无";
    }else{
        _SDepartRecordName = SDepartRecordName;
    }
}
- (void)setStudentSummary:(NSString *)studentSummary{
    if ([studentSummary isKindOfClass:[NSNull class]]) {
        _studentSummary = @"";
    }else{
        _studentSummary = studentSummary;
    }
}
- (void)setTeacherSummary:(NSString *)teacherSummary{
    if ([teacherSummary isKindOfClass:[NSNull class]]) {
        _teacherSummary = @"";
    }else{
        _teacherSummary = teacherSummary;
    }
}
- (void)setSupervisorSummary:(NSString *)supervisorSummary{
    if ([supervisorSummary isKindOfClass:[NSNull class]]) {
        _supervisorSummary = @"";
    }else{
        _supervisorSummary = supervisorSummary;
    }
}
@end

