//
//  GroupController.m
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "GroupController.h"

@interface GroupController ()
{
    UITableView *tabView;
    NSMutableArray *dataArray;
    NSMutableArray *selectArray;
    int isLoad;
}
@end

@implementation GroupController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isLoad = 0;
    [selectArray removeAllObjects];
    [tabView setEditing:YES animated:YES];
    [tabView headerBeginRefreshing];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    dataArray = [NSMutableArray new];
    selectArray = [NSMutableArray new];
    //
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-65, 29.5, 60, 25);
    [rightBtn setTitle:@"确定" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightItemAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"科室选择";
    [backNavigation addSubview:titleLab];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    [tabView setEditing:YES animated:YES];
    [tabView addHeaderWithTarget:self action:@selector(downRefresh)];
    //设置文字
    tabView.headerPullToRefreshText = @"下拉刷新";
    tabView.headerReleaseToRefreshText = @"松开进行刷新";
    tabView.headerRefreshingText = @"刷新中。。。";
    [tabView registerNib:[UINib nibWithNibName:@"GroupCell" bundle:nil] forCellReuseIdentifier:@"groupCell"];
    [self.view addSubview:tabView];
}
//下拉刷新
- (void)downRefresh{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/userGroups?pageSize=999&groupType=department_type",LocalIP];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *response) {
        [dataArray removeAllObjects];
        isLoad = 1;
        [tabView headerEndRefreshing];
        
        NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
        if (array == 0) {
            [Maneger showAlert:@"暂时没有科室" andCurentVC:self];
        }else{
            for (int i=0; i<array.count; i++) {
                NSMutableDictionary *dic = [NSMutableDictionary new];
                NSDictionary *resDic = array[i];
                [dic setObject: [resDic objectForKey:@"groupId"] forKey:@"groupID"];
                [dic setObject: [resDic objectForKey:@"groupName"] forKey:@"groupNAME"];
                [dataArray addObject:dic];
            }
            [tabView reloadData];
        }
    } failed:^(NSString *result) {
        [tabView headerEndRefreshing];
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }

    }];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)rightItemAction:(UIButton *)btn{
    if (isLoad == 0) {
        [MBProgressHUD showToastAndMessage:@"加载没有完成,请稍等。" places:0 toView:nil];
        return;
    }
    [tabView setEditing:NO animated:YES];
    _groupBlock(selectArray);
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -delegate dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.f;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
        return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //选中数据
    [selectArray addObject:dataArray[indexPath.row]];
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    //从选中中取消
    [selectArray removeObject:dataArray[indexPath.row]];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupCell *cell = [tabView dequeueReusableCellWithIdentifier:@"groupCell"];
    NSDictionary *dic = dataArray[indexPath.row];
    cell.name.text = [dic objectForKey:@"groupNAME"];
    return cell;
}

@end
