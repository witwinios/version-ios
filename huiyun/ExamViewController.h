//
//  ExamViewController.h
//  huiyun
//
//  Created by 王慕铁 on 2018/2/25.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyCollectionCell.h"
#import "MyTwoCollectionCell.h"
#import "QuestionModel.h"
@interface ExamViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>


@property(nonatomic,strong)NSString *str;
@property(nonatomic,strong)NSString *courseId;

@end
