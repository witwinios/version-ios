//
//  MoldCell.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/27.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "MoldCell.h"

@implementation MoldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.3];
    
}



-(void)setProperty:(CourseMoldModel *)model{
    
    NSLog(@"Cell_MoldName==%@",model.MoldName);
    self.Label.text=model.MoldName;
    
}


-(void)setChilder:(CourseMoldModel *)model{
    self.Label.text=model.ChildMoldName;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
