//
//  TeachingAidsViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeachingAidsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) NSString *scheduleIdString;
@end
