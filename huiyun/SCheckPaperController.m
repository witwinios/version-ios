//
//  SDOExamController.m
//  huiyun
//
//  Created by MacAir on 2017/11/29.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SCheckPaperController.h"
@interface SCheckPaperController ()
{
    CGFloat choiceLab_H;
    CGFloat questionTitle_H;
    CGFloat questionType_H;
    //页码
    int pageNum;
    //选择题角标
    int choiceNum;
    
    NSMutableArray *dataArray;
    NSMutableArray *scroll_X_Array;
    NSMutableArray *manegerArray;
    //记录是否做题
    NSMutableArray *indexArray;
    //
    NSArray *parrentB1Optons;
    //键盘高度
    CGFloat keyboard_H;
}
@end

@implementation SCheckPaperController
- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, Sheight)];
        _scrollView.alpha = 0.5;
        _scrollView.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:_scrollView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideView:)];
        tap.numberOfTapsRequired = 1;
        [_scrollView addGestureRecognizer:tap];
    }
    return _scrollView;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [dataArray removeAllObjects];
    [scroll_X_Array removeAllObjects];
    [manegerArray removeAllObjects];
    [indexArray removeAllObjects];
    pageNum = 1;
    choiceNum = 0;
}
- (void)buildNavigation{
    //
    choiceLab_H = 18;
    questionTitle_H = 20;
    questionType_H = 25;
    pageNum = 1;
    choiceNum = 0;
    scroll_X_Array = [NSMutableArray new];
    manegerArray = [NSMutableArray new];
    indexArray = [NSMutableArray new];
    dataArray = [NSMutableArray new];
    
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"答题卷";
    [backNavigation addSubview:titleLab];
    
    UILabel *indexLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth-100, 29.5, 90, 30)];
    indexLab.adjustsFontSizeToFitWidth = YES;
    indexLab.tag = 201;
    indexLab.textColor = [UIColor whiteColor];
    indexLab.textAlignment = NSTextAlignmentRight;
    [backNavigation addSubview:indexLab];
}
//
- (void)viewDidLoad {
    //关闭左滑
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [super viewDidLoad];
    [self buildNavigation];
    [self loadData];
}
- (void)loadData{
    //加载试卷
    NSString *requestUrl = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/responseAnswers/answers?pageSize=1000&order=asc",LocalIP,_model.onlineId,LocalUserId];
    NSLog(@"request=%@",requestUrl);
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"" Success:^(NSDictionary *result) {
        
        
        NSArray *array = result[@"responseBody"][@"result"];
        if (array.count == 0) {
            [MBProgressHUD showToastAndMessage:@"你这场考试缺考~" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            return ;
        }
        for (int i =0; i<array.count; i++) {
            NSDictionary *dictionary = array[i];
            QuestionModel *model = [QuestionModel new];
            model.questionId = [dictionary objectForKey:@"questionId"];
            model.questionType = [dictionary objectForKey:@"questionType"];
            model.questTitle = [dictionary objectForKey:@"questionTitle"];
            model.choiceOptions = [dictionary objectForKey:@"choiceOptions"];
            //是否有父类
            if ([[dictionary objectForKey:@"parentQuestionId"] isKindOfClass:[NSNull class]]) {
                model.parrentId = [NSNumber numberWithInt:0];
            }else{
                model.parrentId = [dictionary objectForKey:@"parentQuestionId"];
            }
            model.childNum = [dictionary objectForKey:@"childrenQuestionsNum"];
            //图片
            if (![[dictionary objectForKey:@"questionFile"] isKindOfClass:[NSNull class]]) {
                model.questionFile = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",SimpleIp,[[dictionary objectForKey:@"questionFile"] objectForKey:@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
            }else{
                model.questionFile = @"";
            }
            model.correctAnswer = [dictionary objectForKey:@"correctAnswer"];
            model.questionDes = [dictionary objectForKey:@"analysis"];
            NSArray *answerArr = [dictionary objectForKey:@"responseAnswer"];
            model.answerArray = [NSMutableArray arrayWithArray:answerArr];
            
            if (model.childNum.intValue == 0) {
                if (model.answerArray.count == 0) {
                    [indexArray addObject:@0];
                }else{
                    [indexArray addObject:@1];
                }
            }
            [dataArray addObject:model];
        }
        //制作视图
        [self createUI];
        //开始计时
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"制作失败~" places:0 toView:nil];
        [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }];
}
- (void)createUI {
    //
    UILabel *indexLab = (UILabel *)[self.view viewWithTag:201];
    indexLab.text = [NSString stringWithFormat:@"%.0f/%ld页",_mainScroll.contentOffset.x/Swidth+1,dataArray.count];
    
    _mainScroll.contentSize = CGSizeMake(Swidth * dataArray.count, _mainScroll.frame.size.height);
    _mainScroll.delegate = self;
    _mainScroll.bounces = NO;
    
    _mainScroll.showsHorizontalScrollIndicator = NO;
    for (int i = 0; i < dataArray.count; i ++) {
        UIScrollView *pageScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(i * Swidth, 0, Swidth, Sheight - 64 - 40 - 40)];
        pageScroll.bounces = NO;
        
        pageScroll.showsVerticalScrollIndicator = NO;
        pageScroll.showsHorizontalScrollIndicator = NO;
        [_mainScroll addSubview:pageScroll];
        //
        QuestionModel *model = dataArray[i];
        //创建控件的角标
        CGFloat current_y = 5;
        if ([model.questionType isEqualToString:@"A1选择题"] || [model.questionType isEqualToString:@"A1题型"] || [model.questionType isEqualToString:@"A2题型"]) {
            
            NSString *titleText = [NSString stringWithFormat:@"%@",model.questionType];
            CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
            
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
            
            current_y = current_y + height;
            
            titleLab.numberOfLines = 0;
            titleLab.text = titleText;
            titleLab.font = [UIFont systemFontOfSize:questionType_H];
            titleLab.textColor = UIColorFromHex(0x20B2AA);
            [pageScroll addSubview:titleLab];
            //说明和题干的距离
            current_y += 20;
            
            //题干
            NSString *quesString = [NSString stringWithFormat:@"%d: %@?",pageNum,model.questTitle];
            CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
            
            UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
            current_y = current_y + ques_H;
            quesTitleLab.numberOfLines = 0;
            quesTitleLab.text = quesString;
            quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
            quesTitleLab.textColor = [UIColor blackColor];
            
            [pageScroll addSubview:quesTitleLab];
            
            current_y += 10;
            //选项
            NSArray *choiceOptionArray = model.choiceOptions;
            for (int j = 0; j < choiceOptionArray.count; j ++) {
                SelectButton *choiceBtn = [SelectButton buttonWithType:UIButtonTypeCustom];
                [choiceBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                [choiceBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                if (model.answerArray.count != 0) {
                    if ([model.answerArray[0] intValue] == j) {
                        choiceBtn.selected = YES;
                    }
                }
                
                
                NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                
                CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                
                UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                
                choiceBtn.frame = CGRectMake(5, choiceLab.frame.origin.y, 25, 25);
                choiceLab.text = choiceString;
                choiceLab.numberOfLines = 0;
                
                current_y = current_y + choice_H;
                
                choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                choiceLab.textColor = [UIColor lightGrayColor];
                
                [pageScroll addSubview:choiceLab];
                [pageScroll addSubview:choiceBtn];
                //选项与选项之间的距离
                current_y += 5;
            }
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                
                [pageScroll addSubview:imgView];
                //
                current_y += 105;
            }
            current_y += 5;
            //
            if (model.childNum.intValue == 0) {
                //你的选择
                UIView *answerView = [UIView new];
                answerView.backgroundColor = [UIColor whiteColor];
                UILabel *youChoice = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, Swidth-20/2, 35)];
                youChoice.textColor = [UIColor lightGrayColor];
                youChoice.numberOfLines = 0;
                youChoice.font = [UIFont systemFontOfSize:15];
//                NSMutableAttributedString *oneStr = [[NSMutableAttributedString alloc] initWithString:@"所属科室*:"];
//                NSMutableAttributedString *threeStr = [[NSMutableAttributedString alloc] initWithString:@"问题描述*:"];
//                [oneStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4,1)];
//                [threeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4,1)];
//                self.oneBiao.attributedText = oneStr;
//                self.threeBiao.attributedText = threeStr;
                if (model.answerArray.count != 0) {
                    NSMutableString *str = [NSMutableString new];
                    //排序
                    NSArray *sortArray = [model.answerArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                        return [obj1 compare:obj2];
                    }];
                    for (int u=0; u<sortArray.count; u++) {
                        [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[sortArray[u] intValue]+65]]];
                    }
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"你的选择:%@",str]];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(5, [NSString stringWithFormat:@"你的选择:%@",str].length - 5)];
                    youChoice.attributedText = attributeStr;
//                    youChoice.text = [NSString stringWithFormat:@"你的答案:%@",str];
                }else{
                    NSString *str = @"你的选择:暂无";
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:str];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(5, str.length - 5)];
                    youChoice.attributedText = attributeStr;
                }
                [answerView addSubview:youChoice];
                //正确答案
                UILabel *correctLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2+5, 5, Swidth-20/2, 35)];
                correctLab.textColor = [UIColor lightGrayColor];
                correctLab.numberOfLines = 0;
                correctLab.font = [UIFont systemFontOfSize:15];
                if (model.correctAnswer.count !=0) {
                    NSMutableString *str = [NSMutableString new];
                    for (int u=0; u<model.correctAnswer.count; u++) {
                        [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[model.correctAnswer[u][@"answer"] intValue]+65]]];
                    }
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"正确答案:%@",str]];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(5, [NSString stringWithFormat:@"正确答案:%@",str].length - 5)];
                    youChoice.attributedText = attributeStr;

//                    correctLab.text = [NSString stringWithFormat:@"正确答案:%@",str];
                }else{
                    NSString *str = @"正确答案:暂无";
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:str];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(5, str.length - 5)];
                    youChoice.attributedText = attributeStr;
                }
                [answerView addSubview:correctLab];
                //解析
                UILabel *analysLab = [UILabel new];
                analysLab.numberOfLines = 0;
                analysLab.font = [UIFont systemFontOfSize:15];
                analysLab.text = [NSString stringWithFormat:@"解析: %@",model.questionDes];
                analysLab.textColor = [UIColor lightGrayColor];
                CGFloat analys_H = [Maneger getPonentH:[NSString stringWithFormat:@"解析: %@",model.questionDes] andFont:[UIFont systemFontOfSize:15] andWidth:Swidth-10];
                analysLab.frame = CGRectMake(5, 40, Swidth, analys_H);
                [answerView addSubview:analysLab];
                [pageScroll addSubview:answerView];
                answerView.frame = CGRectMake(0, current_y, Swidth, 40 + analys_H + 10);
                //
                current_y = current_y + 40 + analys_H + 10;
            }
            //答案保存情况
            //题目
            pageNum ++;
            choiceNum ++;
            //
            [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
            //扩大宽度
            if (current_y > pageScroll.frame.size.height) {
                pageScroll.contentSize = CGSizeMake(Swidth, current_y);
            }
        }else if ([model.questionType isEqualToString:@"A3题型"] || [model.questionType isEqualToString:@"A4题型"]){
            if (model.childNum.intValue != 0) {
                NSString *titleText = [NSString stringWithFormat:@"%@ (答题说明：以下提供若干个案例，每个案例下设若干道试题。请根据案例提供的信息，在每道试题下面的多个备选答案中选择一个最佳答案。)",model.questionType];
                CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
                current_y = current_y + height;
                
                titleLab.numberOfLines = 0;
                titleLab.text = titleText;
                titleLab.font = [UIFont systemFontOfSize:questionType_H];
                titleLab.textColor = UIColorFromHex(0x20B2AA);
                
                [pageScroll addSubview:titleLab];
                //说明和题干的距离
                current_y += 20;
            }else{
                NSString *titleText = [NSString stringWithFormat:@"(%@)",model.questionType];
                CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
                current_y = current_y + height;
                
                titleLab.numberOfLines = 0;
                titleLab.text = titleText;
                titleLab.font = [UIFont systemFontOfSize:questionType_H];
                titleLab.textColor = UIColorFromHex(0x20B2AA);
                
                [pageScroll addSubview:titleLab];
                //说明和题干的距离
                current_y += 20;
                
            }
            //题干
            if (model.childNum.intValue != 0) {
                NSString *quesString = [NSString stringWithFormat:@"题干:  %@?",model.questTitle];
                CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
                
                UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
                current_y = current_y + ques_H;
                quesTitleLab.numberOfLines = 0;
                quesTitleLab.text = quesString;
                quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
                quesTitleLab.textColor = [UIColor blackColor];
                
                [pageScroll addSubview:quesTitleLab];
                
            }else{
                NSString *quesString = [NSString stringWithFormat:@"%d: %@?",pageNum,model.questTitle];
                CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
                
                UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
                current_y = current_y + ques_H;
                quesTitleLab.numberOfLines = 0;
                quesTitleLab.text = quesString;
                quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
                quesTitleLab.textColor = [UIColor blackColor];
                
                [pageScroll addSubview:quesTitleLab];
                
                
                [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
                //题干和选项的距离
                current_y += 10;
                //选项
                NSArray *choiceOptionArray = model.choiceOptions;
                for (int j = 0; j < choiceOptionArray.count; j ++) {
                    SelectButton *choiceBtn = [SelectButton buttonWithType:UIButtonTypeCustom];
                    [choiceBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                    [choiceBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                    
                    if (model.answerArray.count != 0) {
                        if ([model.answerArray[0] intValue] == j) {
                            choiceBtn.selected = YES;
                        }
                    }
                    
                    NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                    
                    CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                    
                    UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                    choiceBtn.frame = CGRectMake(5, choiceLab.frame.origin.y, 25, 25);
                    
                    choiceLab.text = choiceString;
                    choiceLab.numberOfLines = 0;
                    
                    current_y = current_y + choice_H;
                    
                    choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                    choiceLab.textColor = [UIColor lightGrayColor];
                    
                    [pageScroll addSubview:choiceLab];
                    [pageScroll addSubview:choiceBtn];
                    //选项与选项之间的距离
                    current_y += 5;
                    
                }
                //题目坐标
                pageNum ++;
                choiceNum ++;
                
            }
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                [pageScroll addSubview:imgView];
                //
                current_y += 105;
            }
            current_y += 5;
            if (model.childNum.intValue == 0) {
                //你的选择
                UIView *answerView = [UIView new];
                answerView.backgroundColor = [UIColor whiteColor];
                UILabel *youChoice = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, Swidth-20/2, 35)];
                youChoice.numberOfLines = 0;
                youChoice.font = [UIFont systemFontOfSize:15];
                
                if (model.answerArray.count != 0) {
                    NSMutableString *str = [NSMutableString new];
                    //排序
                    NSArray *sortArray = [model.answerArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                        return [obj1 compare:obj2];
                    }];
                    for (int u=0; u<sortArray.count; u++) {
                        [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[sortArray[u] intValue]+65]]];
                    }

                    NSMutableAttributedString *attStr = [QuesTools getStr:[NSString stringWithFormat:@"你的答案:%@",str] Color:[UIColor redColor]];
                    youChoice.attributedText = attStr;
                }else{
                    NSString *str = @"你的答案:暂无";
                    NSMutableAttributedString *attStr = [QuesTools getStr:str Color:[UIColor redColor]];
                    youChoice.attributedText = attStr;
                }
                [answerView addSubview:youChoice];
                //正确答案
                UILabel *correctLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2+5, 5, Swidth-20/2, 35)];

                correctLab.numberOfLines = 0;
                correctLab.font = [UIFont systemFontOfSize:15];
                if (model.correctAnswer.count !=0) {
                    NSMutableString *str = [NSMutableString new];
                    for (int u=0; u<model.correctAnswer.count; u++) {
                        [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[model.correctAnswer[u][@"answer"] intValue]+65]]];
                    }
                    NSMutableAttributedString *attStr = [QuesTools getStr:[NSString stringWithFormat:@"正确答案:%@",str] Color:[UIColor blueColor]];
                    correctLab.attributedText = attStr;
                }else{
                    NSMutableAttributedString *attStr = [QuesTools getStr:@"正确答案:暂无" Color:[UIColor blueColor]];
                    correctLab.attributedText = attStr;
                }
                [answerView addSubview:correctLab];
                //解析
                UILabel *analysLab = [UILabel new];
                analysLab.numberOfLines = 0;
                analysLab.font = [UIFont systemFontOfSize:15];
                analysLab.text = [NSString stringWithFormat:@"解析: %@",model.questionDes];
                analysLab.textColor = [UIColor lightGrayColor];
                CGFloat analys_H = [Maneger getPonentH:[NSString stringWithFormat:@"解析: %@",model.questionDes] andFont:[UIFont systemFontOfSize:15] andWidth:Swidth-10];
                analysLab.frame = CGRectMake(5, 40, Swidth, analys_H);
                [answerView addSubview:analysLab];
                [pageScroll addSubview:answerView];
                answerView.frame = CGRectMake(0, current_y, Swidth, 40 + analys_H + 10);
                //
                current_y = current_y + 40 + analys_H + 10;
            }
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
        }else if ([model.questionType isEqualToString:@"B1题型"]){
            if (model.childNum.intValue != 0) {
                NSString *titleText = [NSString stringWithFormat:@"%@ (答题说明：以下提供若干个案例，每个案例下设若干道试题。请根据案例提供的信息，在每道试题下面的多个备选答案中选择一个最佳答案。)",model.questionType];
                CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
                current_y = current_y + height;
                
                titleLab.numberOfLines = 0;
                titleLab.text = titleText;
                titleLab.font = [UIFont systemFontOfSize:questionType_H];
                titleLab.textColor = UIColorFromHex(0x20B2AA);
                
                [pageScroll addSubview:titleLab];
                //说明和题干的距离
                current_y += 20;
            }else{
                NSString *titleText = [NSString stringWithFormat:@"%@",model.questionType];
                CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
                current_y = current_y + height;
                
                titleLab.numberOfLines = 0;
                titleLab.text = titleText;
                titleLab.font = [UIFont systemFontOfSize:questionType_H];
                titleLab.textColor = UIColorFromHex(0x20B2AA);
                
                [pageScroll addSubview:titleLab];
                //说明和题干的距离
                current_y += 20;
            }
            //题干
            if (model.childNum.intValue != 0) {
                //选项
                parrentB1Optons = model.choiceOptions;
                NSArray *choiceOptionArray = model.choiceOptions;
                for (int j = 0; j < choiceOptionArray.count; j ++) {
                    NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                    CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                    UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                    choiceLab.text = choiceString;
                    choiceLab.numberOfLines = 0;
                    current_y = current_y + choice_H;
                    choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                    choiceLab.textColor = [UIColor lightGrayColor];
                    [pageScroll addSubview:choiceLab];
                    //选项与选项之间的距离
                    current_y += 5;
                }
            }else{
                //
                NSString *quesString = [NSString stringWithFormat:@"%d: %@?",pageNum,model.questTitle];
                CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
                
                UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
                current_y = current_y + ques_H;
                quesTitleLab.numberOfLines = 0;
                quesTitleLab.text = quesString;
                quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
                quesTitleLab.textColor = [UIColor blackColor];
                
                [pageScroll addSubview:quesTitleLab];
                //题干和选项的距离
                current_y += 10;
                //选项
                NSArray *choiceOptionArray = parrentB1Optons;
                for (int j = 0; j < choiceOptionArray.count; j ++) {
                    SelectButton *choiceBtn = [SelectButton buttonWithType:UIButtonTypeCustom];
                    [choiceBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                    [choiceBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                    
                    if (model.answerArray.count != 0) {
                        if ([model.answerArray[0] intValue] == j) {
                            choiceBtn.selected = YES;
                        }
                    }
                    NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                    
                    CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                    
                    UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                    choiceBtn.frame = CGRectMake(5, choiceLab.frame.origin.y, 25, 25);
                    
                    choiceLab.text = choiceString;
                    choiceLab.numberOfLines = 0;
                    
                    current_y = current_y + choice_H;
                    
                    choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                    choiceLab.textColor = [UIColor lightGrayColor];
                    
                    [pageScroll addSubview:choiceLab];
                    if (model.childNum.intValue == 0) {
                        [pageScroll addSubview:choiceBtn];
                    }
                    //选项与选项之间的距离
                    current_y += 5;
                }
                //
                [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
                //
                pageNum ++;
                choiceNum ++;
            }
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            current_y += 5;
            //
            if (model.childNum.intValue == 0) {
                //你的选择
                UIView *answerView = [UIView new];
                answerView.backgroundColor = [UIColor whiteColor];
                UILabel *youChoice = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, Swidth-20/2, 35)];
                
                youChoice.numberOfLines = 0;
                youChoice.font = [UIFont systemFontOfSize:15];
                
                if (model.answerArray.count != 0) {
                    NSMutableString *str = [NSMutableString new];
                    //排序
                    NSArray *sortArray = [model.answerArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                        return [obj1 compare:obj2];
                    }];
                    for (int u=0; u<sortArray.count; u++) {
                        [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[sortArray[u] intValue]+65]]];
                    }
                    NSMutableAttributedString *attStr = [QuesTools getStr:[NSString stringWithFormat:@"你的选择:%@",str] Color:[UIColor redColor]];
                    youChoice.attributedText = attStr;

                    
                }else{
                    NSMutableAttributedString *attStr = [QuesTools getStr:@"你的选择:暂无" Color:[UIColor redColor]];
                    youChoice.attributedText = attStr;
                }
                [answerView addSubview:youChoice];
                //正确答案
                UILabel *correctLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2+5, 5, Swidth-20/2, 35)];
        
                correctLab.numberOfLines = 0;
                correctLab.font = [UIFont systemFontOfSize:15];
                if (model.correctAnswer.count !=0) {
                    NSMutableString *str = [NSMutableString new];
                    for (int u=0; u<model.correctAnswer.count; u++) {
                        [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[model.correctAnswer[u][@"answer"] intValue]+65]]];
                    }
                    NSMutableAttributedString *attStr = [QuesTools getStr:[NSString stringWithFormat:@"正确答案:%@",str] Color:[UIColor blueColor]];
                    correctLab.attributedText = attStr;

                    
                }else{
                    NSMutableAttributedString *attStr = [QuesTools getStr:[NSString stringWithFormat:@"正确答案:暂无"] Color:[UIColor blueColor]];
                    correctLab.attributedText = attStr;

                }
                [answerView addSubview:correctLab];
                //解析
                UILabel *analysLab = [UILabel new];
                analysLab.numberOfLines = 0;
                analysLab.font = [UIFont systemFontOfSize:15];
                analysLab.text = [NSString stringWithFormat:@"解析: %@",model.questionDes];
                analysLab.textColor = [UIColor lightGrayColor];
                CGFloat analys_H = [Maneger getPonentH:[NSString stringWithFormat:@"解析: %@",model.questionDes] andFont:[UIFont systemFontOfSize:15] andWidth:Swidth-10];
                analysLab.frame = CGRectMake(5, 40, Swidth, analys_H);
                [answerView addSubview:analysLab];
                [pageScroll addSubview:answerView];
                answerView.frame = CGRectMake(0, current_y, Swidth, 40 + analys_H + 10);
                //
                current_y = current_y + 40 + analys_H + 10;
            }
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
            
        }else if ([model.questionType isEqualToString:@"填空题"]){
            NSString *titleText = @"    填空题:";
            CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth];
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
            current_y = current_y + height;
            
            titleLab.numberOfLines = 0;
            titleLab.text = titleText;
            titleLab.font = [UIFont systemFontOfSize:questionTitle_H];
            titleLab.textColor = UIColorFromHex(0x20B2AA);
            [pageScroll addSubview:titleLab];
            //填空题和题目距离
            current_y += 10;
            //题目
            NSString *quesString = [NSString stringWithFormat:@"%d: %@?",pageNum,model.questTitle];
            CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-10];
            
            UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
            current_y = current_y + ques_H;
            quesTitleLab.numberOfLines = 0;
            quesTitleLab.text = quesString;
            quesTitleLab.font = [UIFont systemFontOfSize:16];
            quesTitleLab.textColor = [UIColor blackColor];
            
            [pageScroll addSubview:quesTitleLab];
            //
            [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
            //查询几个填空
            int fieldNum = 0;
            for (int i = 0; i < model.questTitle.length; i ++) {
                if ([model.questTitle characterAtIndex:i] == '[') {
                    fieldNum ++;
                }
            }
            //题目和field距离
            current_y += 10;
            //创建field
            for (int i = 0; i < fieldNum; i ++) {
                SelectField *field = [[SelectField alloc]initWithFrame:CGRectMake(5, current_y, Swidth-10, 30)];
                NSInteger answerCount = model.answerArray.count;
                if (i < answerCount) {
                    if (model.answerArray.count != 0) {
                        field.text = model.answerArray[i];
                    }else{
                        field.textAlignment = 1;
                        field.placeholder = [NSString stringWithFormat:@"第%d个空格",i + 1];
                    }
                }
                field.enabled = NO;
                field.borderStyle = UITextBorderStyleRoundedRect;
                if (i != fieldNum) {
                    current_y += 40;
                }
                [pageScroll addSubview:field];
            }
            //题目坐标
            pageNum ++;
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            current_y += 5;
            //
            if (model.childNum.intValue == 0) {
                //
                UIView *answerView = [UIView new];
                answerView.backgroundColor = UIColorFromHex(0x20B2AA);
                CGFloat small_y = 5;
                for (int s=0; s<model.correctAnswer.count; s++) {
                    UILabel *label = [UILabel new];
                    NSString *labelText = [NSString stringWithFormat:@"空%d答案:%@",s + 1,model.correctAnswer[s][@"answer"]];
                    CGFloat field_H = [Maneger getPonentH:labelText andFont:[UIFont systemFontOfSize:15] andWidth:Swidth - 10];
                    label.text = labelText;
                    label.numberOfLines = 0;
                    label.font = [UIFont systemFontOfSize:15];
                    label.textColor = [UIColor whiteColor];
                    label.frame = CGRectMake(5, small_y, Swidth-10, field_H);
                    [answerView addSubview:label];
                    if (s != model.correctAnswer.count) {
                        small_y = small_y + field_H + 5;
                    }
                }
                //
                small_y += 5;
                //解析
                UILabel *analysLab = [UILabel new];
                analysLab.numberOfLines = 0;
                analysLab.font = [UIFont systemFontOfSize:15];
                analysLab.text = [NSString stringWithFormat:@"解析: %@",model.questionDes];
                analysLab.textColor = [UIColor whiteColor];
                CGFloat analys_H = [Maneger getPonentH:[NSString stringWithFormat:@"解析: %@",model.questionDes] andFont:[UIFont systemFontOfSize:15] andWidth:Swidth-10];
                analysLab.frame = CGRectMake(5, small_y, Swidth, analys_H);
                //
                small_y += analys_H;
                small_y += 5;
                [answerView addSubview:analysLab];
                [pageScroll addSubview:answerView];
                answerView.frame = CGRectMake(0, current_y, Swidth, small_y);
                //
                current_y += small_y;
            }
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
        }else if ([model.questionType isEqualToString:@"解答题"]){
            NSString *titleText = @"    解答题:";
            CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:25] andWidth:Swidth];
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
            current_y = current_y + height;
            
            titleLab.numberOfLines = 0;
            titleLab.text = titleText;
            titleLab.font = [UIFont systemFontOfSize:25];
            titleLab.textColor = UIColorFromHex(0x20B2AA);
            [pageScroll addSubview:titleLab];
            //题型和题目距离
            current_y += 10;
            
            //题目
            NSString *quesString = [NSString stringWithFormat:@"%d: %@?",pageNum,model.questTitle];
            CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-10];
            
            UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
            current_y = current_y + ques_H;
            quesTitleLab.numberOfLines = 0;
            quesTitleLab.text = quesString;
            quesTitleLab.font = [UIFont systemFontOfSize:16];
            quesTitleLab.textColor = [UIColor blackColor];
            
            [pageScroll addSubview:quesTitleLab];
            //题目和答题区距离
            current_y += 10;
            SelectTextview *answerView = [[SelectTextview alloc]initWithFrame:CGRectMake(5, current_y, Swidth-10, 60)];
            answerView.model = model;
            if (model.answerArray.count != 0) {
                answerView.text = model.answerArray[0];
            }
            answerView.quesIndex = [NSNumber numberWithInt:pageNum - 1];
            answerView.backgroundColor = [UIColor whiteColor];
            answerView.layer.cornerRadius = 3;
            answerView.layer.masksToBounds = YES;
            answerView.editable = NO;
            answerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            answerView.layer.borderWidth = 1;
            [pageScroll addSubview:answerView];
            //
            current_y += 70;
            pageNum ++;
            //
            [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            current_y += 5;
            //
            CGFloat small_y = 5;
            if (model.childNum.intValue == 0) {
                UIView *answerView = [UIView new];
                answerView.backgroundColor = UIColorFromHex(0x20B2AA);
                UILabel *answerLab = [UILabel new];
                NSString *answerStr;
                if (model.correctAnswer.count != 0) {
                    answerStr = [NSString stringWithFormat:@"正确答案:%@",model.correctAnswer[0][@"answer"]];
                }else{
                    answerStr = @"正确答案:暂无";
                }
                answerLab.text = answerStr;
                answerLab.font = [UIFont systemFontOfSize:15];
                answerLab.numberOfLines = 0;
                CGFloat answer_H = [Maneger getPonentH:answerStr andFont:[UIFont systemFontOfSize:15] andWidth:Swidth - 10];
                answerLab.frame = CGRectMake(5, small_y, Swidth - 10, answer_H);
                [answerView addSubview:answerLab];
                small_y += answer_H;
                small_y += 5;
                //解析
                UILabel *analysLab = [UILabel new];
                analysLab.numberOfLines = 0;
                analysLab.font = [UIFont systemFontOfSize:15];
                analysLab.text = [NSString stringWithFormat:@"解析: %@",model.questionDes];
                analysLab.textColor = [UIColor whiteColor];
                CGFloat analys_H = [Maneger getPonentH:[NSString stringWithFormat:@"解析:%@",model.questionDes] andFont:[UIFont systemFontOfSize:15] andWidth:Swidth-10];
                analysLab.frame = CGRectMake(5, small_y, Swidth, analys_H);
                [answerView addSubview:analysLab];
                [pageScroll addSubview:answerView];
                small_y += analys_H;
                small_y += 5;
                
                answerView.frame = CGRectMake(0, current_y, Swidth, small_y);
                
                //
                current_y += small_y;
            }
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
            
        }else if ([model.questionType isEqualToString:@"X: 多选题"]){
            NSString *titleText = @"X: 多选题";
            CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
            
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
            current_y = current_y + height;
            
            titleLab.numberOfLines = 0;
            titleLab.text = titleText;
            titleLab.font = [UIFont systemFontOfSize:questionType_H];
            titleLab.textColor = UIColorFromHex(0x20B2AA);
            
            [pageScroll addSubview:titleLab];
            //说明和题干的距离
            current_y += 20;
            
            //题干
            NSString *quesString = [NSString stringWithFormat:@"%d: %@?",pageNum,model.questTitle];
            CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
            
            UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
            current_y = current_y + ques_H;
            quesTitleLab.numberOfLines = 0;
            quesTitleLab.text = quesString;
            quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
            quesTitleLab.textColor = [UIColor blackColor];
            
            [pageScroll addSubview:quesTitleLab];
            
            current_y += 10;
            //选项
            NSArray *choiceOptionArray = model.choiceOptions;
            for (int j = 0; j < choiceOptionArray.count; j ++) {
                SelectButton *choiceBtn = [SelectButton buttonWithType:UIButtonTypeCustom];
                [choiceBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                [choiceBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                if (model.answerArray.count != 0) {
                    for (int s = 0; s<model.answerArray.count; s++) {
                        if ([model.answerArray[s] intValue] == j) {
                            choiceBtn.selected = YES;
                        }
                    }
                }
                
                
                NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                
                CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                
                UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                choiceBtn.frame = CGRectMake(5, choiceLab.frame.origin.y, 25, 25);
                
                choiceLab.text = choiceString;
                choiceLab.numberOfLines = 0;
                
                current_y = current_y + choice_H;
                
                choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                choiceLab.textColor = [UIColor lightGrayColor];
                
                [pageScroll addSubview:choiceLab];
                [pageScroll addSubview:choiceBtn];
                //选项与选项之间的距离
                current_y += 5;
            }
            //题目坐标
            pageNum ++;
            //
            [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            current_y += 5;
            //解析
            if (model.childNum.intValue == 0) {
                //你的选择
                UIView *answerView = [UIView new];
                answerView.backgroundColor = [UIColor whiteColor];
                UILabel *youChoice = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, Swidth-20/2, 35)];
                
                youChoice.numberOfLines = 0;
                youChoice.font = [UIFont systemFontOfSize:15];
                
                if (model.answerArray.count != 0) {
                    NSMutableString *str = [NSMutableString new];
                    //排序
                    NSArray *sortArray = [model.answerArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                        return [obj1 compare:obj2];
                    }];
                    for (int u=0; u<sortArray.count; u++) {
                        [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[sortArray[u] intValue]+65]]];
                    }
                    NSMutableAttributedString *attStr = [QuesTools getStr:[NSString stringWithFormat:@"你的答案:%@",str] Color:[UIColor redColor]];
                    youChoice.attributedText = attStr;

                    
                }else{
                    NSMutableAttributedString *attStr = [QuesTools getStr:[NSString stringWithFormat:@"你的选择:暂无"] Color:[UIColor redColor]];
                    youChoice.attributedText = attStr;

    
                }
                [answerView addSubview:youChoice];
                //正确答案
                UILabel *correctLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2+5, 5, Swidth-20/2, 35)];
                
                correctLab.numberOfLines = 0;
                correctLab.font = [UIFont systemFontOfSize:15];
                if (model.correctAnswer.count !=0) {
                    NSMutableString *str = [NSMutableString new];
                    for (int u=0; u<model.correctAnswer.count; u++) {
                        [str appendString:[NSString stringWithFormat:@"%@ ",[NSString stringWithFormat:@"%c",[model.correctAnswer[u][@"answer"] intValue]+65]]];
                    }
                    NSMutableAttributedString *attStr = [QuesTools getStr:[NSString stringWithFormat:@"正确答案:%@",str] Color:[UIColor blueColor]];
                    correctLab.attributedText = attStr;

                }else{
                    NSMutableAttributedString *attStr = [QuesTools getStr:[NSString stringWithFormat:@"正确答案:暂无"] Color:[UIColor blueColor]];
                    correctLab.attributedText = attStr;
                }
                [answerView addSubview:correctLab];
                //解析
                UILabel *analysLab = [UILabel new];
                analysLab.numberOfLines = 0;
                analysLab.font = [UIFont systemFontOfSize:15];
                analysLab.text = [NSString stringWithFormat:@"解析: %@",model.questionDes];
                analysLab.textColor = [UIColor lightGrayColor];
                CGFloat analys_H = [Maneger getPonentH:[NSString stringWithFormat:@"解析: %@",model.questionDes] andFont:[UIFont systemFontOfSize:15] andWidth:Swidth-10];
                analysLab.frame = CGRectMake(5, 40, Swidth, analys_H);
                [answerView addSubview:analysLab];
                [pageScroll addSubview:answerView];
                answerView.frame = CGRectMake(0, current_y, Swidth, 40 + analys_H + 5);
                //
                current_y = current_y + 40 + analys_H + 5;
            }
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
        }
    }
}
#pragma -action
- (void)biggerImage:(UITapGestureRecognizer *)tap{
    UIImageView *imgView = (UIImageView *)tap.view;
    
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight - 144)];
    backView.backgroundColor = [UIColor blackColor];
    //    backView.alpha = 0.5;
    backView.userInteractionEnabled = YES;
    UITapGestureRecognizer *taps = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideImage:)];
    taps.numberOfTapsRequired = 1;
    [backView addGestureRecognizer:taps];
    
    UIImageView *bigImgView = [[UIImageView alloc]initWithFrame:backView.frame];
    bigImgView.image = imgView.image;
    [backView addSubview:bigImgView];
    [tap.view.superview addSubview:backView];
}
- (void)hideImage:(UITapGestureRecognizer *)tap{
    [tap.view removeFromSuperview];
}

- (void)hideView:(UITapGestureRecognizer *)tap{
    self.cardBtn.selected = !self.cardBtn.selected;
    [UIView animateWithDuration:0.3 animations:^{
        [self scrollView].frame = CGRectMake(0, Sheight, Swidth, Sheight);
    }];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)moveTo :(UIButton *)btn{
    self.cardBtn.selected = !self.cardBtn.selected;
    
    int tag = [btn.titleLabel.text intValue];
    NSLog(@"value = %@",indexArray[tag -1]);
    [UIView animateWithDuration:0.5 animations:^{
        self.mainScroll.contentOffset = CGPointMake([scroll_X_Array[tag - 1] floatValue], 0);
    }];
    UILabel *indexLab = [self.view viewWithTag:201];
    indexLab.text = [NSString stringWithFormat:@"%.0f/%ld页",_mainScroll.contentOffset.x/Swidth + 1,dataArray.count];
    [btn.superview.superview removeFromSuperview];
}
- (void)removeQues: (UITapGestureRecognizer *)tap{
    self.cardBtn.selected = !self.cardBtn.selected;
    [tap.view removeFromSuperview];
}

#pragma -scrollDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView == _mainScroll) {
        UILabel *indexLab = [self.view viewWithTag:201];
        indexLab.text = [NSString stringWithFormat:@"%.0f/%ld页",scrollView.contentOffset.x/Swidth + 1,dataArray.count];
    }
}
- (IBAction)cardAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    UIScrollView  *questionView;
    if (btn.selected) {
        questionView = [UIScrollView new];
        questionView.frame = CGRectMake(0, 64,Swidth, Sheight-64);
        questionView.backgroundColor = [UIColor lightGrayColor];
        questionView.alpha = 0.9;
        
        int tags = pageNum - 1;
        for (int i=0; i<tags; i++) {
            int j = i/4;
            UIView *view = [UIView new];
            CGPoint point;
            view.frame = CGRectMake(Swidth/4*(i%4), Swidth/4*j, Swidth/4, Swidth/4);
            if (i==0 && j==0) {
                point = view.center;
            }
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(0, 0, 30, 30);
            button.center = point;
            button.tag = 300+j*4+(i%4)+1;
            button.titleLabel.font = [UIFont systemFontOfSize:10];
            [button setBackgroundImage:[UIImage imageNamed:@"weizuo"] forState:UIControlStateNormal];
            [button setTitle:[NSString stringWithFormat:@"%d",i + 1] forState:0];
            //设置已做 未做颜色
            //设置已做 未做颜色
            if ([indexArray[i] intValue] == 0) {
                [button setBackgroundImage:[UIImage imageNamed:@"weizuo"] forState:0];
                [button setTitleColor:UIColorFromHex(0x20B2AA) forState:0];
            }else{
                [button setBackgroundImage:[UIImage imageNamed:@"yizuo"] forState:0];
                [button setTitleColor:[UIColor whiteColor] forState:0];
            }

            [button addTarget:self action:@selector(moveTo:) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:button];
            [questionView addSubview:view];
        }
        if (tags%4 == 0){
            questionView.contentSize = CGSizeMake(Swidth, tags/4*(Swidth/4));
        }else{
            questionView.contentSize = CGSizeMake(Swidth, (tags/4+1)*(Swidth/4));
        }
        [self.view addSubview:questionView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeQues:)];
        tap.numberOfTapsRequired = 1;
        [questionView addGestureRecognizer:tap];
        return;
    }
    [questionView removeFromSuperview];
    
}

- (IBAction)leftAction:(id)sender {
    if (self.mainScroll.contentOffset.x == 0) {
        [MBProgressHUD showToastAndMessage:@"已经是第一页了~" places:0 toView:nil];
        return;
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.mainScroll.contentOffset = CGPointMake(self.mainScroll.contentOffset.x - Swidth, self.mainScroll.contentOffset.y);
    } completion:^(BOOL finished) {
        UILabel *indexLab = [self.view viewWithTag:201];
        indexLab.text = [NSString stringWithFormat:@"%.0f/%ld页",_mainScroll.contentOffset.x/Swidth + 1,dataArray.count];
    }];
}

- (IBAction)rightAction:(id)sender {
    if (self.mainScroll.contentOffset.x/Swidth == dataArray.count - 1) {
        [MBProgressHUD showToastAndMessage:@"已经是最后一页了~" places:0 toView:nil];
        return;
    }
    [UIView animateWithDuration:0.5 animations:^{
        self.mainScroll.contentOffset = CGPointMake(self.mainScroll.contentOffset.x + Swidth, self.mainScroll.contentOffset.y);
    } completion:^(BOOL finished) {
        UILabel *indexLab = [self.view viewWithTag:201];
        indexLab.text = [NSString stringWithFormat:@"%.0f/%ld页",_mainScroll.contentOffset.x/Swidth + 1,dataArray.count];
    }];
}
@end

