//
//  ImgViewCell.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/25.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "ImgViewCell.h"

@implementation ImgViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _imgView  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetWidth(self.frame))];
        [self.contentView addSubview:_imgView];
        
    }
    
    return self;
}

-(void)setProperty:(CourseAffixModel *)model{
    
    NSString *url=[NSString stringWithFormat:@"%@?CTTS-Token=%@",model.FileUrl,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
     [self.imgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"tab"]];
    
    
    
}


@end
