//
//  QuestionController.m
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "QuestionController.h"

@interface QuestionController ()
{
    UITableView *tabView;
    NSMutableArray *dataArray;
    NSMutableArray *resultArray;
    PopViewController *_popVC;
    //当前选择的群
    YWConversationViewController *selfConversation;
    MyQuesModel *currentModel;
}
@end

@implementation QuestionController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (YWIMCore *)ywIMCore {
    return [SPKitExample sharedInstance].ywIMKit.IMCore;
}
- (id<IYWTribeService>)ywTribeService {
    return [[self ywIMCore] getTribeService];
}
- (void)setUI{
    //
    self.ywIMKit = [[YWAPI sharedInstance] fetchIMKitForOpenIM];
    self.tribe = [[YWTribe alloc]init];
    dataArray = [NSMutableArray new];
    resultArray = [NSMutableArray new];
    //
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    //
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"我的问题";
    [backNavigation addSubview:titleLab];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"shaixuan"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStyleGrouped];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    tabView.layer.borderWidth = 2;
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"QuestionCell" bundle:nil] forCellReuseIdentifier:@"quesCell"];
    [tabView addHeaderWithTarget:self action:@selector(downRefresh)];
    //设置文字
    tabView.headerPullToRefreshText = @"下拉刷新";
    tabView.headerReleaseToRefreshText = @"松开进行刷新";
    tabView.headerRefreshingText = @"刷新中。。。";
    [self.view addSubview:tabView];
    //刷新
    [tabView headerBeginRefreshing];
}
//下拉刷新
- (void)downRefresh{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/QAGroup?createdBy=%@",LocalIP,LocalUserId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *response) {
        [dataArray removeAllObjects];
        [tabView headerEndRefreshing];
        //组装数据
        NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"result"];
        if (array.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂无群组!" places:0 toView:nil];
            return ;
        }
        for (int i=0; i<array.count; i++) {
            NSDictionary *responseDic = array[i];
            
            MyQuesModel *quesModel = [MyQuesModel new];
            quesModel.qaGroupId = [responseDic objectForKey:@"qaGroupId"];
            quesModel.quesTitle = [responseDic objectForKey:@"questionTitle"];
            quesModel.quesDes  = [responseDic objectForKey:@"questionDescription"];
            quesModel.tribeId = [responseDic objectForKey:@"tribeId"];
            quesModel.status = [responseDic objectForKey:@"status"];
            
            NSArray *memArray = [responseDic objectForKey:@"tribeMembers"];
            NSMutableArray *memArrays = [NSMutableArray new];
            for (int s=0; s<memArray.count; s++) {
                NSDictionary *memDic = memArray[s];
                TribeMem *tribeMem = [TribeMem new];
                tribeMem.userId = [memDic objectForKey:@"userId"];
                tribeMem.fullName = [memDic objectForKey:@"fullName"];
                tribeMem.email = [memDic objectForKey:@"email"];
                tribeMem.phone = [memDic objectForKey:@"personalPhoneNo"];
                tribeMem.school = [memDic objectForKey:@"schoolName"];
                [memArrays addObject:tribeMem];
            }
            quesModel.memberArray = [NSArray arrayWithArray:memArrays];
            
            [dataArray addObject:quesModel];
            [resultArray addObject:quesModel];
        }
        [tabView reloadData];
        
    } failed:^(NSString *code) {
        [tabView headerEndRefreshing];
        if ([code isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}
#pragma -action
- (void)selectAction:(UIButton *)btn{
    CGPoint point = CGPointMake(btn.center.x, btn.center.y+16);
    CustomPopview *popView = [[CustomPopview alloc]initWithOrigin:point Width:130 Height:80 Type:XTTypeOfUpRight Color:[UIColor whiteColor]];
    popView.dataArray = @[@"未解决",@"已解决"];
    popView.fontSize = 18;
    popView.row_height = 40;
    popView.delegate = self;
    [popView popView];
}
//
- (void)exampleOpenConversationViewControllerWithTribe:(YWTribe *)aTribe fromNavigationController:(UINavigationController *)aNavigationController
{
    YWConversation *conversation = [YWTribeConversation fetchConversationByTribe:aTribe createIfNotExist:YES baseContext:self.ywIMKit.IMCore];
    [self exampleOpenConversationViewControllerWithConversation:conversation fromNavigationController:aNavigationController];
}
- (void)exampleOpenConversationViewControllerWithConversation:(YWConversation *)aConversation fromNavigationController:(UINavigationController *)aNavigationController
{
    __block YWConversationViewController *alreadyController = nil;
    [aNavigationController.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[YWConversationViewController class]]) {
            YWConversationViewController *c = obj;
            if (aConversation.conversationId && [c.conversation.conversationId isEqualToString:aConversation.conversationId]) {
                alreadyController = c;
                *stop = YES;
            }
        }
    }];
    
    if (alreadyController) {
        // 必须判断当前是否已有该会话，如果有，则直接显示已有会话
        [aNavigationController popToViewController:alreadyController animated:YES];
        [aNavigationController setNavigationBarHidden:NO];
        return;
    } else {
        YWConversationViewController *conversationController = [self.ywIMKit makeConversationViewControllerWithConversationId:aConversation.conversationId];
        selfConversation = conversationController;
        __weak typeof(conversationController) weakController = conversationController;
        [conversationController setViewWillAppearBlock:^(BOOL aAnimated) {
            
            weakController.view.backgroundColor = [UIColor whiteColor];
            //添加导航栏
            UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
            backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
            backNavigation.userInteractionEnabled = YES;
            [self.view addSubview:backNavigation];
            self.view.backgroundColor = [UIColor whiteColor];
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
            leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
            [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
            [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
            [backNavigation addSubview:leftBtn];
            UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
            [rightBtn setImage:[UIImage imageNamed:@"user"] forState:UIControlStateNormal];
            [rightBtn addTarget:self action:@selector(rightItemAction:) forControlEvents:UIControlEventTouchUpInside];
            [backNavigation addSubview:rightBtn];
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
            titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
            titleLab.textAlignment = 1;
            titleLab.font = [UIFont systemFontOfSize:20];
            titleLab.textColor = [UIColor whiteColor];
            titleLab.text = @"讨论组";
            [backNavigation addSubview:titleLab];
            
            UIButton *solveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            solveBtn.frame = CGRectMake(backNavigation.frame.size.width/2+60, 27, 80, 30);
            [solveBtn setTitle:@"已解决?" forState:0];
            
            solveBtn.layer.cornerRadius = 3;
            solveBtn.layer.masksToBounds=YES;
            solveBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            solveBtn.layer.borderWidth = 1;
            [solveBtn addTarget:self action:@selector(solveAction:) forControlEvents:UIControlEventTouchUpInside];
            [backNavigation addSubview:solveBtn];
            [weakController.view addSubview:backNavigation];
            
            //添加textView
            UITextView *quesView = [UITextView new];
            quesView.frame = CGRectMake(0, 64, Swidth, 60);
            quesView.text = [NSString stringWithFormat:@"问题描述:%@",currentModel.quesDes];
            quesView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            quesView.layer.borderWidth = 1;
            quesView.editable = NO;
            [weakController.view addSubview:quesView];
            //
            weakController.tableView.frame = CGRectMake(0, 124, Swidth, Sheight-124);
        }];
        
        [aNavigationController pushViewController:conversationController animated:YES];
        
    }
}
//打开聊天
- (void)openAction{
    self.tribe.tribeId = [NSString stringWithFormat:@"%@",currentModel.tribeId];
    self.tribe.tribeName = @"群聊";
    [self exampleOpenConversationViewControllerWithTribe:self.tribe fromNavigationController:self.navigationController];
}

#pragma -action
- (void)rightItemAction:(UIButton *)btn{
    TeacherController *teacherVC = [TeacherController new];
    teacherVC.tribeArray = currentModel.memberArray;
    //
    [self.navigationController pushViewController:teacherVC animated:YES];
}
- (void)solveAction:(UIButton *)btn{
    UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"是否已经解决该问题并关闭讨论组?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"已解决", nil];
    [alerView show];
}
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -delegate dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 117.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MyQuesModel *quesModel = dataArray[indexPath.section];
    currentModel = dataArray[indexPath.section];
    currentModel.indexId = [NSNumber numberWithInteger:indexPath.row];
    if ([quesModel.status isEqualToString:@"未解决"]) {
        [self openAction];
    }else{
        TribeHiostory *historyVC = [TribeHiostory new];
        historyVC.model = quesModel;
        [self.navigationController pushViewController:historyVC animated:YES];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QuestionCell *cell = [tabView dequeueReusableCellWithIdentifier:@"quesCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setModel:dataArray[indexPath.section] andCurrentVC:self];
    return cell;
}
#pragma -alertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self chageStatus];
    }
}
- (void)chageStatus{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/QAGroup/%@/solved",LocalIP,currentModel.qaGroupId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:nil Message:@"关闭中" Success:^(NSDictionary *response) {
        
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"已关闭群!" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1.2 repeats:NO block:^(NSTimer * _Nonnull timer) {
                [dataArray removeObjectAtIndex:currentModel.indexId.integerValue];
                [tabView reloadData];
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else{
            [MBProgressHUD showToastAndMessage:@"失败,请重试!" places:0 toView:nil];
        }
        
    } failed:^(NSString *code) {
        if ([code isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}
#pragma -popView delegate
- (void)selectIndexPathRow:(NSInteger)index{
    NSMutableArray *muArray = [NSMutableArray new];
    switch (index) {
        case 0:
            for (int i=0; i<resultArray.count; i++) {
                MyQuesModel *model = resultArray[i];
                if ([model.status isEqualToString:@"未解决"]) {
                    [muArray addObject:model];
                }
            }
            dataArray = muArray;
            [tabView reloadData];
            break;
        case 1:
            for (int i=0; i<resultArray.count; i++) {
                MyQuesModel *model = resultArray[i];
                if ([model.status isEqualToString:@"已解决"]) {
                    [muArray addObject:model];
                }
            }
            dataArray = muArray;
            [tabView reloadData];
            break;
        default:
            break;
    }
}
@end
