//
//  OnlineStudentController.h
//  huiyun
//
//  Created by MacAir on 2018/2/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "onlineModel.h"
#import "BaseController.h"
#import "OnlineStudentCell.h"
#import "OnlineStudentModel.h"
@interface OnlineStudentController : BaseController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (strong, nonatomic) onlineModel *model;

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UISearchBar *seachBar;

@property (strong, nonatomic) NSMutableArray *dataArr;
@property (strong, nonatomic) NSMutableArray *resultArr;
@end
