//
//  DataManager.h
//  NSURLConnection
//
//  Created by qianfeng on 15-8-6.
//  Copyright (c) 2015年 ZYK. All rights reserved.
//  DataManager类库的功能为，实现数据异步加载

#import <Foundation/Foundation.h>
//声明block
typedef void(^SuccessBlock) (id obj);

@interface DataManager : NSObject<NSURLConnectionDataDelegate>
{
    NSURLConnection *_connection;
    NSMutableData *_mutData;
    
}

//声明网络加载方法
-(void)loadWithURL:(NSString *)url;
//封装block为属性
@property(copy,nonatomic)SuccessBlock sBlock;


@end
