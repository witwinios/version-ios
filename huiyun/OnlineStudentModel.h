//
//  OnlineStudentModel.h
//  huiyun
//
//  Created by MacAir on 2018/2/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OnlineStudentModel : NSObject
@property (strong, nonatomic) NSNumber *stuId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *professin;
@property (strong, nonatomic) NSString *className;
@property (strong, nonatomic) NSNumber *inTime;
@end
