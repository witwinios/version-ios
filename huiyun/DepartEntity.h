//
//  DepartEntity.h
//  huiyun
//
//  Created by MacAir on 2018/1/30.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DepartEntity : NSObject
@property (strong, nonatomic) NSNumber *DepartEntityId;
@property (strong, nonatomic) NSString *DepartEntityName;

@end
