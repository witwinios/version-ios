//
//  ContentController.m
//  yun
//
//  Created by MacAir on 2017/7/19.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ContentController.h"
#define HEIGHT [UIScreen mainScreen].bounds.size.height
#define WIDTH [UIScreen mainScreen].bounds.size.width
@interface ContentController ()

@end

@implementation ContentController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)setUI{
    self.view.backgroundColor = [UIColor whiteColor];
    //导航栏
    self.navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [self.navigationView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(self.navigationView.frame.size.width/2, leftBtn.center.y);
    titleLab.text = @"考生确认";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    [self.navigationView addSubview:titleLab];
    //头像居中
    self.headImage.frame = CGRectMake(0, 0, 60, 60);
    self.headImage.center = CGPointMake(WIDTH/2, 38);
    self.headImage.layer.cornerRadius = 5;
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
    self.headImage.layer.borderWidth = 1;
    //
    self.name.text = [NSString stringWithFormat:@"姓名: %@",self.callModel.callModelUserName];
    self.name.adjustsFontSizeToFitWidth = YES;
    self.xuehao.adjustsFontSizeToFitWidth = YES;
    
    if (self.callModel.callModelUserNo.integerValue == 0) {
        self.xuehao.text = @"学号: 暂无";
    }else{
        self.xuehao.text = [NSString stringWithFormat:@"学号: %@",self.callModel.callModelUserNo];
    }
    //创建视图
    //考官
    UILabel *teacherLab = [[UILabel alloc]initWithFrame:CGRectMake(5, 220, 100, 20)];
    teacherLab.text = @"考官姓名:";
    teacherLab.font = [UIFont systemFontOfSize:20];
    [self.view addSubview:teacherLab];
    UILabel *teacherContent = [UILabel new];
    CGFloat t_h = [Maneger getPonentH:self.callModel.callModelTeacher andFont:[UIFont systemFontOfSize:20] andWidth:Swidth-115];
    teacherContent.numberOfLines = 0;
    teacherContent.text = self.callModel.callModelTeacher;
    teacherContent.frame = CGRectMake(110, 220, Swidth-115, t_h);
    teacherContent.numberOfLines = 0;
    teacherContent.font = [UIFont systemFontOfSize:20];
    [self.view addSubview:teacherContent];
    //站点
    UILabel *stationLab = [[UILabel alloc]initWithFrame:CGRectMake(5, 220+t_h+5, 100, 20)];
    stationLab.text = @"站点名称:";
    stationLab.font = [UIFont systemFontOfSize:20];
    [self.view addSubview:stationLab];
    UILabel *stationContent = [UILabel new];
    CGFloat s_h = [Maneger getPonentH:self.callModel.callModelStationName andFont:[UIFont systemFontOfSize:20] andWidth:Swidth-115];
    stationContent.text = self.callModel.callModelStationName;
    stationContent.frame = CGRectMake(110, 220+t_h+5, Swidth - 115, s_h);
    stationContent.font = [UIFont systemFontOfSize:20];
    stationContent.numberOfLines = 0;
    [self.view addSubview:stationContent];
    //考试地点
    UILabel *roomLab = [[UILabel alloc]initWithFrame:CGRectMake(5, 230+t_h+s_h, 100, 20)];
    roomLab.text = @"考试地点:";
    roomLab.font = [UIFont systemFontOfSize:20];
    [self.view addSubview:roomLab];
    UILabel *roomContent = [UILabel new];
    roomContent.text = self.callModel.callModelRoomName;
    roomContent.textColor = UIColorFromHex(0x20B2AA);
    CGFloat r_h = [Maneger getPonentH:self.callModel.callModelRoomName andFont:[UIFont systemFontOfSize:20] andWidth:Swidth-115];
    roomContent.font = [UIFont systemFontOfSize:20];
    roomContent.numberOfLines = 0;
    roomContent.frame = CGRectMake(110, 230+t_h+s_h, Swidth-115, r_h);
    [self.view addSubview:roomContent];

    //添加button
    if (self.callType.integerValue == 0) {
        //手动
        UIButton *absenceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [absenceBtn setTitle:@"弃考" forState:0];
        absenceBtn.frame = CGRectMake(0, HEIGHT-40, WIDTH/2, 40);
        [absenceBtn addTarget:self action:@selector(absenceAction:) forControlEvents:UIControlEventTouchUpInside];
        absenceBtn.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:absenceBtn];
        //身份确认
        UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [sureBtn setTitle:@"身份无误" forState:0];
        sureBtn.frame = CGRectMake(WIDTH/2, HEIGHT-40, WIDTH/2, 40);
        sureBtn.backgroundColor = UIColorFromHex(0x20B2AA);
        [sureBtn addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:sureBtn];
        //
    }else{
        //缺考
        UIButton *absenceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        absenceBtn.frame = CGRectMake(0, HEIGHT-40, WIDTH/3, 40);
        [absenceBtn setTitle:@"弃考" forState:0];
        [absenceBtn addTarget:self action:@selector(absenceAction:) forControlEvents:UIControlEventTouchUpInside];
        absenceBtn.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:absenceBtn];
        //下一个
//        UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        nextBtn.frame = CGRectMake(WIDTH/3, HEIGHT-40, WIDTH/3, 40);
//        nextBtn.tag = 350;
//        [nextBtn addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
//        [nextBtn setTitle:@"下一个" forState:0];
//        nextBtn.backgroundColor = [UIColor blueColor];
//        [self.view addSubview:nextBtn];
        //身份确认
        UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sureBtn.frame = CGRectMake(WIDTH/3*2, HEIGHT-40, WIDTH/3, 40);
        [sureBtn setTitle:@"身份确认" forState:0];
        [sureBtn addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
        sureBtn.backgroundColor = UIColorFromHex(0x20B2AA);
        [self.view addSubview:sureBtn];
    }
}
#pragma -action
- (void)sureAction:(UIButton *)btn {
    if ([self.callModel.callModelType isEqualToString:@"自动"]) {
        ControlManeger *VCManeger = [ControlManeger share];
        VCManeger.PointVC = [ExamPointController new];
        VCManeger.PointVC.skillModel = self.skillModel;
        VCManeger.PointVC.callModel = self.callModel;
        [self.navigationController pushViewController:VCManeger.PointVC animated:YES];
    }else{
        ControlManeger *VCManeger = [ControlManeger share];
        VCManeger.AllStationVC = [AllStationController new];
        VCManeger.AllStationVC.skillModel = self.skillModel;
        VCManeger.AllStationVC.callModel = self.callModel;
        [self.navigationController pushViewController:VCManeger.AllStationVC animated:YES];
    }
  
}
- (void)back{
    [MBProgressHUD showHUDAndMessage:@"跳过该考生。。。" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/skip/%@",LocalIP,_skillModel.skillID,_callModel.callModelStationId,_callModel.callModelUserId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"put" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        [self.navigationController popViewControllerAnimated:YES];

    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"跳过该考生失败,请重试!" places:0 toView:nil];
    }];
}
- (void)nextAction:(UIButton *)btn{
    UIButton *nextBtn = [self.view viewWithTag:350];
    nextBtn.enabled = NO;
    [MBProgressHUD showHUDAndMessage:@"叫号中" toView:nil];
    //跳过考生
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/skip/%@",LocalIP,_skillModel.skillID,_callModel.callModelStationId,_callModel.callModelUserId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"put" Params:nil Success:^(NSDictionary *result) {
        [self autoCall];
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"失败,请重试!" places:0 toView:nil];
        nextBtn.enabled = YES;
    }];
}
- (void)autoCall{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/call",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId];
    NSLog(@"%@",requestUrl);
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        
        CallModel *callModel = [CallModel new];
        NSDictionary *dic = result[@"responseBody"];
        callModel.callModelStationId = dic[@"selectedId"];
        callModel.callModelStationName = dic[@"selectedStationName"];
        callModel.callModelRoomId = dic[@"roomId"];
        callModel.callModelRoomName = dic[@"roomName"];
        callModel.callModelTeacherId = dic[@"teacherIds"];
        callModel.callModelType = dic[@"mode"];
        callModel.selectOsceStationNums = dic[@"selectedCandidateStationsNum"];
        NSArray *teacherArray = dic[@"teachers"];
        NSMutableString *teachers = [[NSMutableString alloc]init];

        for (NSDictionary *teacherObj in teacherArray) {
            [teachers appendString:[NSString stringWithFormat:@"%@  ",teacherObj[@"fullName"]]];
        }
        callModel.callModelTeacher = teachers;
        
        callModel.callModelUserId = dic[@"nextStudent"][@"userId"];
        callModel.callModelUserName = dic[@"nextStudent"][@"fullName"];
        callModel.callModelUserNo = dic[@"nextStudent"][@"userNo"];
        
        
        self.callType = [NSNumber numberWithInt:1];
        self.callModel = callModel;
        //
        self.name.text = [NSString stringWithFormat:@"姓名: %@",self.callModel.callModelUserName];
        
        if (self.callModel.callModelUserNo.integerValue == 0) {
            self.xuehao.text = @"学号: 暂无";
        }else{
            self.xuehao.text = [NSString stringWithFormat:@"学号: %@",self.callModel.callModelUserNo];
        }
        self.teacherName.text = self.callModel.callModelTeacher;
        self.stationName.text = self.callModel.callModelStationName;
        self.roomName.text = self.callModel.callModelRoomName;
        //
        UIButton *nextBtn = [self.view viewWithTag:350];
        nextBtn.enabled = YES;
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"失败,请重试!" places:0 toView:nil];
        UIButton *nextBtn = [self.view viewWithTag:350];
        nextBtn.enabled = YES;
    }];
}
//缺考
- (void)absenceAction:(UIButton *)btn{
    [MBProgressHUD showHUDtoView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/drop/%@",LocalIP,_skillModel.skillID,_callModel.callModelStationId,_callModel.callModelUserId];
    
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:nil Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"弃考成功!" places:0 toView:nil];
            [NSTimer scheduledTimerWithTimeInterval:1.2 repeats:NO block:^(NSTimer * _Nonnull timer) {
               [self.navigationController popViewControllerAnimated:YES];
            }];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
        [MBProgressHUD showToastAndMessage:@"该考生弃考失败,请重试!" places:0 toView:nil];
    }];
}

@end
