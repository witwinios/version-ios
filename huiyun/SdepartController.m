//
//  DepartController.m
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SdepartController.h"
#define STATE_SEARCH 0
#define STATE_NOT_SEARCH 1
@interface SdepartController ()
{
    UITableView *table;
    NSMutableArray *dataArray;
    NSMutableArray *copyArray;
    
    NSMutableArray *heightArr;
    
    int currentPage;
}
@end

@implementation SdepartController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    copyArray = [NSMutableArray new];
    heightArr = [NSMutableArray new];
    [self setUI];
    currentPage = 1;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"shaixuan"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"轮转科室";
    [backNavigation addSubview:titleLab];
    //
    table = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    table.delegate = self;
    table.dataSource = self;
    [table registerNib:[UINib nibWithNibName:@"SDeaprtCell" bundle:nil] forCellReuseIdentifier:@"sdepartCell"];
    [self.view addSubview:table];
    //
    table.tableFooterView.frame = CGRectZero;
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.backgroundColor = UIColorFromHex(0xF0F0F0);
    [table addHeaderWithTarget:self action:@selector(downRefresh)];
    [table addFooterWithTarget:self action:@selector(moreRefresh)];
    [table headerBeginRefreshing];
}
- (void)searchAction:(NSString *)status{
    [MBProgressHUD showHUDAndMessage:@"" toView:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageSize=999&pageStart=1&studentId=%@&rotationStatus=%@",SimpleIp,LocalUserId,status];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [dataArray removeAllObjects];
        [heightArr removeAllObjects];
        [MBProgressHUD hideHUDForView:nil];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (arr.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂无~" places:0 toView:nil];
            return ;
        }
        for (int i=0; i<arr.count; i++) {
            NSDictionary *dic = arr[i];
            SDepartModel *model = [SDepartModel new];
            model.SDepartRecordId = dic[@"studentClinicalRotationRecordId"];
            model.SDepartName = dic[@"userGroupName"];
            model.SDepartStartTime = dic[@"checkInTime"];
            model.SDepartEndTime = dic[@"checkOutTime"];
            model.SDepartStatus = dic[@"rotationStatus"];
            
            model.SDepartPlace = dic[@""];
            model.SdepartPhaseScheduleName = dic[@"phaseScheduleName"];

            //出科小结
            model.studentSummary = dic[@"studentSummary"];
            if ([dic[@"submitStudentSummary"] boolValue] == false) {
                model.isSubmitStudentSummary = @0;
            }else{
                model.isSubmitStudentSummary = @1;
            }
            //带教意见
            model.teacherSummary = dic[@"mentorFeedback"];
            model.teacherId = dic[@"mentorId"];
            model.teacherName = dic[@"mentorFullName"];
            if ([dic[@"submitMentorFeedback"] boolValue] == false) {
                model.isSubmitTeacherSummary = @0;
            }else{
                model.isSubmitTeacherSummary = @1;
            }
            //主管意见
            model.supervisorId = dic[@"supervisorId"];
            model.supervisorName = dic[@"supervisorFullName"];
            model.supervisorSummary = dic[@"supervisorFeedback"];
            if (![dic[@"submitSupervisorFeedback"] isKindOfClass:[NSNull class]]) {
                if ([dic[@"submitSupervisorFeedback"] boolValue] == false) {
                    model.isSubmitSupervisorSummary = @0;
                }else{
                    model.isSubmitSupervisorSummary = @1;
                }
            }
            
            NSMutableArray *recordArray = [NSMutableArray new];
            //病例记录
            NSNumber *medicalNum = dic[@"studentMedicalCaseRecordsNum"];
            [recordArray addObject:medicalNum];
            //操作记录
            NSNumber *operateNum = dic[@"studentOperationRecordsNum"];
            [recordArray addObject:operateNum];
            //活动记录
            NSNumber *activityNum = dic[@"studentActivityRecordsNum"];
            [recordArray addObject:activityNum];
            //教学记录
            NSNumber *tutringNum = dic[@"studentTutoringRecordsNum"];
            [recordArray addObject:tutringNum];
            //科研记录
            NSNumber *researchNum = dic[@"studentResearchRecordsNum"];
            [recordArray addObject:researchNum];
            //获奖记录
            NSNumber *awardNum = dic[@"studentAwardRecordsNum"];
            [recordArray addObject:awardNum];
            //论文记录
            NSNumber *essayNum = dic[@"studentEssayPaperRecordsNum"];
            [recordArray addObject:essayNum];
            //抢救记录
            NSNumber *rescueNum = dic[@"studentRescueRecordsNum"];
            [recordArray addObject:rescueNum];
            //出诊记录
            NSNumber *treatNum = dic[@"studentTreatmentRecordsNum"];
            [recordArray addObject:treatNum];
            //差错记录
            NSNumber *malpracticeNum = dic[@"studentMalpracticeRecordsNum"];
            [recordArray addObject:malpracticeNum];
            //赋值
            model.SDepartRecordArray = recordArray;
            //添加高度
            CGFloat lineH = [Maneger getPonentH:model.SDepartName andFont:[UIFont systemFontOfSize:18] andWidth:Swidth-136];
            [heightArr addObject:[NSString stringWithFormat:@"%.0f",lineH]];
            [dataArray addObject:model];
        }
        copyArray = dataArray;
        [table headerEndRefreshing];
        [table reloadData];
    }];
}
- (void)downRefresh{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords?pageSize=15&pageStart=1&studentId=%@",SimpleIp,LocalUserId];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [dataArray removeAllObjects];
        [heightArr removeAllObjects];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (arr.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂无~" places:0 toView:nil];
            [table headerEndRefreshing];
            return ;
        }
        for (int i=0; i<arr.count; i++) {
            NSDictionary *dic = arr[i];
            SDepartModel *model = [SDepartModel new];
            model.SDepartRecordId = dic[@"studentClinicalRotationRecordId"];
            model.SDepartName = dic[@"userGroupName"];
            model.SDepartStartTime = dic[@"checkInTime"];
            model.SDepartEndTime = dic[@"checkOutTime"];
            model.SDepartStatus = dic[@"rotationStatus"];
            model.SDepartPlace = dic[@""];
            model.SdepartPhaseScheduleName = dic[@"phaseScheduleName"];
            //出科小结
            model.studentSummary = dic[@"studentSummary"];
            if ([dic[@"submitStudentSummary"] boolValue] == false) {
                model.isSubmitStudentSummary = @0;
            }else{
                model.isSubmitStudentSummary = @1;
            }
            //带教意见
            model.teacherSummary = dic[@"mentorFeedback"];
            model.teacherId = dic[@"mentorId"];
            model.teacherName = dic[@"mentorFullName"];
            if ([dic[@"submitMentorFeedback"] boolValue] == false) {
                model.isSubmitTeacherSummary = @0;
            }else{
                model.isSubmitTeacherSummary = @1;
            }
            //主管意见
            model.supervisorId = dic[@"supervisorId"];
            model.supervisorName = dic[@"supervisorFullName"];
            model.supervisorSummary = dic[@"supervisorFeedback"];
            if (![dic[@"submitSupervisorFeedback"] isKindOfClass:[NSNull class]]) {
                if ([dic[@"submitSupervisorFeedback"] boolValue] == false) {
                    model.isSubmitSupervisorSummary = @0;
                }else{
                    model.isSubmitSupervisorSummary = @1;
                }
            }
           
            NSMutableArray *recordArray = [NSMutableArray new];
            //病例记录
            NSNumber *medicalNum = dic[@"studentMedicalCaseRecordsNum"];
            [recordArray addObject:medicalNum];
            //操作记录
            NSNumber *operateNum = dic[@"studentOperationRecordsNum"];
            [recordArray addObject:operateNum];
            //活动记录
            NSNumber *activityNum = dic[@"studentActivityRecordsNum"];
            [recordArray addObject:activityNum];
            //教学记录
            NSNumber *tutringNum = dic[@"studentTutoringRecordsNum"];
            [recordArray addObject:tutringNum];
            //科研记录
            NSNumber *researchNum = dic[@"studentResearchRecordsNum"];
            [recordArray addObject:researchNum];
            //获奖记录
            NSNumber *awardNum = dic[@"studentAwardRecordsNum"];
            [recordArray addObject:awardNum];
            //论文记录
            NSNumber *essayNum = dic[@"studentEssayPaperRecordsNum"];
            [recordArray addObject:essayNum];
            //抢救记录
            NSNumber *rescueNum = dic[@"studentRescueRecordsNum"];
            [recordArray addObject:rescueNum];
            //出诊记录
            NSNumber *treatNum = dic[@"studentTreatmentRecordsNum"];
            [recordArray addObject:treatNum];
            //差错记录
            NSNumber *malpracticeNum = dic[@"studentMalpracticeRecordsNum"];
            [recordArray addObject:malpracticeNum];
            //赋值
            model.SDepartRecordArray = recordArray;
            //
            CGFloat lineH = [Maneger getPonentH:model.SDepartName andFont:[UIFont systemFontOfSize:18] andWidth:Swidth-136];
            [heightArr addObject:[NSString stringWithFormat:@"%.0f",lineH]];
            [dataArray addObject:model];
        }
        copyArray = dataArray;
        [table headerEndRefreshing];
        [table reloadData];
    }];
}
- (void)moreRefresh{
    currentPage ++;
    NSString *requestUrl = [NSString stringWithFormat:@"%@/studentClinicalRotationRecords?pageSize=15&pageStart=%d&studentId=%@",LocalIP,currentPage,LocalUserId];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        NSMutableArray *newData = [NSMutableArray new];
        NSMutableArray *newHeight = [NSMutableArray new];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (arr.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂无更多~" places:0 toView:nil];
            [table footerEndRefreshing];
            return ;
        }
        for (int i=0; i<arr.count; i++) {
            NSDictionary *dic = arr[i];
            SDepartModel *model = [SDepartModel new];
            model.SDepartRecordId = dic[@"studentClinicalRotationRecordId"];
            model.SDepartName = dic[@"userGroupName"];
            model.SDepartStartTime = dic[@"checkInTime"];
            model.SDepartEndTime = dic[@"checkOutTime"];
            model.SDepartStatus = dic[@"rotationStatus"];
            model.SdepartPhaseScheduleName = dic[@"phaseScheduleName"];
            //出科小结
            model.studentSummary = dic[@"studentSummary"];
            if ([dic[@"submitStudentSummary"] boolValue] == false) {
                model.isSubmitStudentSummary = @0;
            }else{
                model.isSubmitStudentSummary = @1;
            }
            //带教意见
            model.teacherSummary = dic[@"mentorFeedback"];
            model.teacherId = dic[@"mentorId"];
            model.teacherName = dic[@"mentorFullName"];
            if (![dic[@"submitSupervisorFeedback"] isKindOfClass:[NSNull class]]) {
                if ([dic[@"submitSupervisorFeedback"] boolValue] == false) {
                    model.isSubmitSupervisorSummary = @0;
                }else{
                    model.isSubmitSupervisorSummary = @1;
                }
            }
            //主管意见
            model.supervisorId = dic[@"supervisorId"];
            model.supervisorName = dic[@"supervisorFullName"];
            model.supervisorSummary = dic[@"supervisorFeedback"];
            if ([dic[@"submitSupervisorFeedback"] boolValue] == false) {
                model.isSubmitSupervisorSummary = @0;
            }else{
                model.isSubmitSupervisorSummary = @1;
            }
            
            NSMutableArray *recordArray = [NSMutableArray new];
            //病例记录
            NSNumber *medicalNum = dic[@"studentMedicalCaseRecordsNum"];
            [recordArray addObject:medicalNum];
            //操作记录
            NSNumber *operateNum = dic[@"studentOperationRecordsNum"];
            [recordArray addObject:operateNum];
            //活动记录
            NSNumber *activityNum = dic[@"studentActivityRecordsNum"];
            [recordArray addObject:activityNum];
            //教学记录
            NSNumber *tutringNum = dic[@"studentTutoringRecordsNum"];
            [recordArray addObject:tutringNum];
            //科研记录
            NSNumber *researchNum = dic[@"studentResearchRecordsNum"];
            [recordArray addObject:researchNum];
            //获奖记录
            NSNumber *awardNum = dic[@"studentAwardRecordsNum"];
            [recordArray addObject:awardNum];
            //论文记录
            NSNumber *essayNum = dic[@"studentEssayPaperRecordsNum"];
            [recordArray addObject:essayNum];
            //抢救记录
            NSNumber *rescueNum = dic[@"studentRescueRecordsNum"];
            [recordArray addObject:rescueNum];
            //出诊记录
            NSNumber *treatNum = dic[@"studentTreatmentRecordsNum"];
            [recordArray addObject:treatNum];
            //差错记录
            NSNumber *malpracticeNum = dic[@"studentMalpracticeRecordsNum"];
            [recordArray addObject:malpracticeNum];
            //赋值
            model.SDepartRecordArray = recordArray;
            CGFloat lineH = [Maneger getPonentH:model.SDepartName andFont:[UIFont systemFontOfSize:18] andWidth:Swidth-136];
            [newHeight addObject:[NSString stringWithFormat:@"%.0f",lineH]];
            [newData addObject:model];
        }
        [table footerEndRefreshing];
        NSRange range = NSMakeRange(dataArray.count,newData.count);
        NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
        [dataArray insertObjects:newData atIndexes:set];
        [heightArr insertObjects:newHeight atIndexes:set];
        copyArray = dataArray;
        [table reloadData];
    }];
}
#pragma -action
- (void)statusAction:(UIButton *)btn{
    
}
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)selectAction:(UIButton *)button{
    CGPoint point = CGPointMake(button.center.x, button.center.y+16);
    CustomPopview *popView = [[CustomPopview alloc]initWithOrigin:point Width:130 Height:160 Type:XTTypeOfUpRight Color:[UIColor whiteColor]];
    popView.dataArray = @[@"未开始",@"在科",@"出科",@"取消"];
    popView.fontSize = 18;
    popView.row_height = 40;
    popView.delegate = self;
    [popView popView];
}
#pragma -delegate
- (void)selectIndexPathRow:(NSInteger)index{
    NSArray *indexArray = @[@"not_start",@"in_department",@"off_department",@"canceled"];
    [self searchAction:indexArray[index]];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue] + 94;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    recordVC.departModel = dataArray[indexPath.row];

    
    
    [self.navigationController pushViewController:recordVC animated:YES];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SDeaprtCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sdepartCell"];
    SDepartModel *model = dataArray[indexPath.row];
    cell.departName.text = model.SDepartName;
    cell.departStatus.text = model.SDepartStatus;
    cell.phaseScheduleName.text = model.SdepartPhaseScheduleName;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *start = [[Maneger shareObject] timeFormatter1:model.SDepartStartTime.stringValue];
    NSString *end = [[Maneger shareObject] timeFormatter1:model.SDepartEndTime.stringValue];
    cell.departTime.text = [NSString stringWithFormat:@"%@ ~ %@",start,end];
    return cell;
}
@end

