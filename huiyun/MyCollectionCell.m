//
//  MyCollectionCell.m
//  huiyun
//
//  Created by 王文远 on 2018/2/26.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "MyCollectionCell.h"

@implementation MyCollectionCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    self=[super initWithFrame:frame];
    
    if (self) {
        
        //提标：
        _QuestionTypes=[[UILabel alloc]init];
        [self.QuestionTypes setFont:[UIFont systemFontOfSize:15]];
        [self.QuestionTypes setNumberOfLines:0];
        [self.contentView addSubview:_QuestionTypes];
        
        //题目:
        self.QusetionTitle=[[UILabel alloc]init];
        [self.QusetionTitle setFont:[UIFont systemFontOfSize:15]];
        [self.QusetionTitle setNumberOfLines:0];
        [self.contentView addSubview:self.QusetionTitle];
        
        
        self.AnswersBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.AnswersBtn setTitle:@"查看答案" forState:UIControlStateNormal];
        self.AnswersBtn.backgroundColor=[UIColor lightGrayColor];
        [self.AnswersBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
       
        
        
    }
    return self;

}

-(void)setQuestion:(QuestionModel *)model{
    
    _OptionArray=[NSMutableArray new];

    //题型：
    if ([model.questionType isEqualToString:@"A1选择题"]) {
        _QuestionTypes.text=@"A1:单句型题";
    }if([model.questionType isEqualToString:@"A2题型"]){
        self.QuestionTypes.text=@"A2:病例摘要题";
    }else{
        self.QuestionTypes.text=@"X型题型";
    }
    
    self.QuestionTypes.textAlignment=NSTextAlignmentCenter;
    self.QuestionTypes.textColor=UIColorFromHex(0x20B2AA);
    self.QuestionTypes.layer.borderWidth=2;
    self.QuestionTypes.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.QuestionTypes.layer.cornerRadius=5;
    
    CGFloat QuestionTypes_H=[self.QuestionTypes getSpaceLabelHeight:self.QuestionTypes.text withWidh:Swidth-40];
    [self.QuestionTypes setFrame:CGRectMake(10,10, 200, 20)];
    
    //选项A～Z:
    for (int i=0; i<26; i++) {
        [_OptionArray addObject:[NSString stringWithFormat:@"%c",'A'+i]];
    }
    
    //题目：
    self.QusetionTitle.text=[NSString stringWithFormat:@"    %@",model.questTitle];
    CGFloat QuesetionTitle_H=[self.QusetionTitle getSpaceLabelHeight:self.QusetionTitle.text withWidh:Swidth-20];
    [self.QusetionTitle setFrame:CGRectMake(20, QuestionTypes_H+20, Swidth-20, QuesetionTitle_H)];
    
    CGFloat ChoiceOptions_X=50;
    for (int i=0; i<model.choiceOptions.count; i++) {
        self.ChoiceOptionsLabel=[[UILabel alloc]init];
        [self.ChoiceOptionsLabel setFont:[UIFont systemFontOfSize:15]];
        [self.ChoiceOptionsLabel setNumberOfLines:0];
        self.ChoiceOptionsLabel.text=[NSString stringWithFormat:@"%@. %@",_OptionArray[i],model.choiceOptions[i]];
        CGFloat ChoiceOptions_H=[self.ChoiceOptionsLabel getSpaceLabelHeight:self.ChoiceOptionsLabel.text withWidh:self.frame.size.width-60];
        
        CGFloat ChoiceOptions_Y;
        
        if (i==0) {
            ChoiceOptions_Y=(QuesetionTitle_H+60);
        }else{
            ChoiceOptions_Y=(_ChoiceOptionsLabel_h+20);
        }
        
        [self.ChoiceOptionsLabel setFrame:CGRectMake(ChoiceOptions_X, ChoiceOptions_Y,self.frame.size.width-60, ChoiceOptions_H)];
        _ChoiceOptionsLabel_h=ChoiceOptions_Y+ChoiceOptions_H;
        [self addSubview:self.ChoiceOptionsLabel];
        
        self.ChoiceOptionsBtn=[RadioButton buttonWithType:UIButtonTypeCustom];
        
        if ([model.questionType isEqualToString:@"A1选择题"] || [model.questionType isEqualToString:@"A2题型"]) {
            [self.ChoiceOptionsBtn setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            [self.ChoiceOptionsBtn setImage:[UIImage imageNamed:@"check"] forState:UIControlStateSelected];
        }else{
            [self.ChoiceOptionsBtn setImage:[UIImage imageNamed:@"kong"] forState:UIControlStateNormal];
            [self.ChoiceOptionsBtn setImage:[UIImage imageNamed:@"fill"] forState:UIControlStateSelected];
        }
        [self.ChoiceOptionsBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.ChoiceOptionsBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateSelected];

        [self.ChoiceOptionsBtn setFrame:CGRectMake(ChoiceOptions_X-38, ChoiceOptions_Y, 40, 30)];
        _ChoiceOptionsBtn_h=ChoiceOptions_Y+30;
        self.ChoiceOptionsBtn.tag=99+i;
        [self addSubview:self.ChoiceOptionsBtn];
        
    }

    //答案:
    
    CGFloat AnswersLabel_Y=_ChoiceOptionsLabel_h+20;
    
    [self.AnswersBtn addTarget:self action:@selector(Add) forControlEvents:UIControlEventTouchUpInside];
    
    [self.AnswersBtn setFrame:CGRectMake(20, AnswersLabel_Y, 100, 30)];
    
    
    
    CGFloat AnswersBtn_Y=AnswersLabel_Y+30;
    
    
    
    [self.contentView addSubview:self.AnswersBtn];
    
    
    
    self.AnswersLabel=[[UILabel alloc]init];
    [self.AnswersLabel setFont:[UIFont systemFontOfSize:15]];
    [self.AnswersLabel setNumberOfLines:0];

    NSString *AnswersStr;
    NSString *AnswersStrr=@"答案是：";
    NSString *Answer;
    for (int i=0; i<model.answerArray.count; i++) {
        AnswersStr=[NSString stringWithFormat:@"%c",'A'+ [model.answerArray[i] intValue]];
        Answer=[AnswersStrr stringByAppendingString:AnswersStr];
        AnswersStrr=Answer;
        NSLog(@"Answers==%@",model.answerArray[i]);
    }


    self.AnswersLabel.text=[NSString stringWithFormat:@"%@",Answer];
    self.AnswersLabel.textColor=[UIColor redColor];
    CGFloat AnswersLabel_H=[self.AnswersLabel getSpaceLabelHeight:self.AnswersLabel.text withWidh:Swidth-20];
    [self.AnswersLabel setFrame:CGRectMake(20, AnswersBtn_Y+10, Swidth-20, AnswersLabel_H)];
    _AnswersLabel_h=AnswersLabel_Y+AnswersLabel_H;
    [self.contentView addSubview:self.AnswersLabel];
    self.AnswersLabel.hidden=YES;
    
    
}

-(void)Add{
    self.AnswersBtn.selected=!self.AnswersBtn.selected;
    if(self.AnswersBtn.selected){
        self.AnswersLabel.hidden=NO;
    }else{
         self.AnswersLabel.hidden=YES;
    }
}

@end
