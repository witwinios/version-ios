//
//  MistakeDetailController.h
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "BaseWitwinController.h"
#import "CaseMistakeModel.h"
#import "DetailLeaderCell.h"
#import "SDepartModel.h"
@interface MistakeDetailController : BaseWitwinController<UITextViewDelegate>
@property (strong, nonatomic) CaseMistakeModel *model;

@property(strong,nonatomic)SDepartModel *SDepartModel;
@end
