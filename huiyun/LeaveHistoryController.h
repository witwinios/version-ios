//
//  LeaveHistoryController.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/10.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryCell.h"
#import "HistoryLeaveModel.h"
#import "RequestTools.h"
#import "LoadingView.h"
#import "UpdateLeaveController.h"
@interface LeaveHistoryController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
+ (id)shareInstance;
@end
