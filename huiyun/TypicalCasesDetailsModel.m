//
//  TypicalCasesDetailsModel.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/12.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TypicalCasesDetailsModel.h"

@implementation TypicalCasesDetailsModel


-(void)setRecordType:(NSString *)RecordType{
    if ([RecordType isKindOfClass:[NSNull class]]) {
        _RecordType = @"暂无";
    }if ([RecordType isEqualToString:@"statement"]) {
        _RecordType=@"症状";
    }else if ([RecordType isEqualToString:@"inspection"]) {
        _RecordType=@"检查";
    }else if ([RecordType isEqualToString:@"treatment"]) {
        _RecordType=@"治疗";
    }else{
        _RecordType = RecordType;
    }
}

-(void)setRecordTime:(NSNumber *)RecordTime{
    if ([RecordTime isKindOfClass:[NSNull class]]) {
        _RecordTime = [NSNumber numberWithInt:0];
    }else{
        _RecordTime = RecordTime;
    }
}

-(void)setRecordDescription:(NSString *)RecordDescription{
    if ([RecordDescription isKindOfClass:[NSNull class]]) {
        _RecordDescription = @"暂无";
    }else{
        _RecordDescription = RecordDescription;
    }
}


-(void)setRecordFileName:(NSString *)RecordFileName{
    if ([RecordFileName isKindOfClass:[NSNull class]]) {
        _RecordFileName = @"暂无";
    }else{
        _RecordFileName = RecordFileName;
    }
}


-(void)setRecordFileFormat:(NSString *)RecordFileFormat{
    if ([RecordFileFormat isKindOfClass:[NSNull class]]) {
        _RecordFileFormat = @"暂无";
    }else{
        _RecordFileFormat = RecordFileFormat;
    }
}


-(void)setRecordUrl:(NSString *)RecordUrl{
    if ([RecordUrl isKindOfClass:[NSNull class]]) {
        _RecordUrl = @"暂无";
    }else{
        _RecordUrl = RecordUrl;
    }
}

@end
