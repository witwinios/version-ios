//
//  TRoomCell.h
//  yun
//
//  Created by MacAir on 2017/7/17.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRoomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *room;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UILabel *capacity;
@end
