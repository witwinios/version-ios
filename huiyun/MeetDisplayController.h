//
//  MeetDisplayController.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/23.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MeetItemCell.h"
#import "DataManager.h"
#import "LiveRoomViewController.h"
@interface MeetDisplayController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) NSArray *meetArray;
@end
