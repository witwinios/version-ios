//
//  ResultSubmitedController.h
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDepartModel.h"
@interface ResultSubmitedController : UIViewController

@property (strong, nonatomic) SDepartModel *departModel;
@property (weak, nonatomic) IBOutlet UITextView *studentSummary;
@property (weak, nonatomic) IBOutlet UITextView *teacherSummary;
@property (weak, nonatomic) IBOutlet UITextView *supervisionSummary;

@end
