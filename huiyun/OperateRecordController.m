//
//  OperateRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "OperateRecordController.h"
#import "THDatePickerView.h"
@interface OperateRecordController ()
{
    NSString *current_date;
    NSNumber *has_image;
    
    UIView *toolbar;
    
    NSString *operateResult;
}
@property (strong, nonatomic) WXPPickerView *PickerOne;
@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) UIButton *btn;
@end

@implementation OperateRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    has_image = @0;
    current_date = @"";
    operateResult = @"";
    
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];
    if ([SubjectTool share].subjectType == subjectTypeUpdate) {
        NSDictionary *resultParam = @{@"成功":@"success",@"失败":@"failure"};
        [self.oneField setTitle:self.myModel.caseResult forState:0];
        operateResult = resultParam[self.myModel.caseResult];
        
        self.twoField.text = self.myModel.casePatientName;
        self.threeField.text = [NSString stringWithFormat:@"%@",self.myModel.caseNo];
        self.reasonView.text = self.myModel.caseReason;
        self.descriptionView.text = self.myModel.caseDescription;
        if (self.myModel.caseDate.intValue == 0) {
            current_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseDate];
            [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:self.myModel.caseDate.stringValue] forState:0];
        }
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonHide:) name:UIKeyboardWillHideNotification object:nil];

    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];
    self.dateBtn.layer.cornerRadius = 4;
    self.dateBtn.layer.masksToBounds = YES;
    self.dateBtn.titleLabel.textAlignment = 0;
    self.saveBtn.layer.cornerRadius = 4;
    self.saveBtn.layer.masksToBounds = YES;
    THDatePickerView *dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:longDate] forState:0];
        current_date = longDate;
    } andCancelBlock:^{
        self.btn.hidden = YES;
    } dateType:DateTypeDay];
    [self.view addSubview:dateView];
    self.dateView = dateView;
    
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"操作记录";
    [backNavigation addSubview:titleLab];
    //style
    self.oneField.layer.cornerRadius = 5;
    self.oneField.layer.masksToBounds = YES;
    self.oneField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.oneField.layer.borderWidth = 1;
    
    self.twoField.layer.cornerRadius = 5;
    self.twoField.layer.masksToBounds = YES;
    self.twoField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.twoField.layer.borderWidth = 1;
    
    self.threeField.layer.cornerRadius = 5;
    self.threeField.layer.masksToBounds = YES;
    self.threeField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.threeField.layer.borderWidth = 1;
}
- (void)resignFirstRespond{
    [self.oneField resignFirstResponder];
    [self.twoField resignFirstResponder];
    [self.threeField resignFirstResponder];
    [self.reasonView resignFirstResponder];
    [self.descriptionView resignFirstResponder];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)hideKb:(UIButton *)btn{
    [self resignFirstRespond];
}
-(void)doneButtonshow: (NSNotification *)notification {
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
}
- (void)doneButtonHide:(NSNotification *)notification{
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
    }];
}
- (IBAction)dateAction:(id)sender {
    self.btn.hidden = NO;
    [self resignFirstRespond];
    [self.dateView show];
}
- (void)updateAction{
    if ([operateResult isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"结果不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"姓名不能为空~" places:0 toView:nil];
        return;
    }
    if ([_threeField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"病案号不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([self.reasonView.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"原因不能为空~" places:0 toView:nil];
        return;
    }
    SdepartRecordController *departVC = [SdepartRecordController shareObject];
    
    if (has_image.intValue == 0) {
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentOperationRecords/%@",SimpleIp,self.myModel.recordId];
        
        NSDictionary *params = @{@"operationTime":current_date,@"caseNo":_threeField.text,@"patientName":_twoField.text,@"result":operateResult,@"analysis":_reasonView.text,@"description":_descriptionView.text,@"phaseScheduleOperationRequirementId":self.myModel.requestmentId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        
        NSLog(@"request=%@ params=%@",requestUrl,params);
        
        [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
            self.myModel = nil;
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        [RequestTools RequestWithFile:_currentImage.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            has_image = @0;
            
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentOperationRecords/%@",SimpleIp,self.myModel.recordId];
            
            NSDictionary *params = @{@"operationTime":current_date,@"caseNo":_threeField.text,@"patientName":_twoField.text,@"result":operateResult,@"analysis":_reasonView.text,@"description":_descriptionView.text,@"phaseScheduleOperationRequirementId":self.myModel.requestmentId,@"fileId":fileId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
            
            NSLog(@"request=%@ params=%@",requestUrl,params);
            
            [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
                self.myModel = nil;
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
                }else{
                    [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
            }];
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }
}
- (void)saveAction{
    NSLog(@"result=%@",operateResult);
    if ([operateResult isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"结果不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"姓名不能为空~" places:0 toView:nil];
        return;
    }
    if ([_threeField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"病案号不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([self.reasonView.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"原因不能为空~" places:0 toView:nil];
        return;
    }
    SdepartRecordController *departVC = [SdepartRecordController shareObject];
    
    if (has_image.intValue == 0) {
        [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
        
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentOperationRecords",SimpleIp];
        
        NSDictionary *params = @{@"operationTime":current_date,@"caseNo":_threeField.text,@"patientName":_twoField.text,@"result":operateResult,@"analysis":_reasonView.text,@"description":_descriptionView.text,@"phaseScheduleOperationRequirementId":self.model.CaseReuestModelRequirementId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        NSLog(@"request=%@ params=%@",requestUrl,params);
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
        [RequestTools RequestWithFile:_currentImage.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentOperationRecords",SimpleIp];
            
            NSDictionary *params = @{@"operationTime":current_date,@"caseNo":_threeField.text,@"patientName":_twoField.text,@"result":operateResult,@"analysis":_reasonView.text,@"description":_descriptionView.text,@"phaseScheduleOperationRequirementId":self.model.CaseReuestModelRequirementId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,@"fileId":fileId};
            
            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
                }else{
                    [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }
    
}
- (void)popLast{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}

- (IBAction)saveAction:(id)sender {
    if ([SubjectTool share].subjectType == subjectTypeAdd) {
        [self saveAction];
    }else{
        [self updateAction];
    }
    
}

#pragma UIImagePickerControllerDelegate
- (IBAction)fileAction:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相册",@"拍照", nil];
    [sheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self getAlbumAction];
            break;
        case 1:
            [self photoAction];
            break;
        default:
            break;
    }
}
- (void)photoAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePickController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)getAlbumAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    [picker dismissViewControllerAnimated:YES completion:nil];
    has_image = @1;
    _currentImage.image = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
}

- (IBAction)oneFieldAction:(id)sender {
    UIButton *button = sender;
    NSArray *array = @[@"成功",@"失败"];
    NSArray *content = @[@"success",@"failure"];
    self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:array]];
    self.PickerOne.delegate = self;
    self.PickerOne.rightBtnTitle = @"确认";
    self.PickerOne.backgroundColor = [UIColor whiteColor];
    self.PickerOne.index = ^(int index){
        [button setTitle:array[index] forState:0];
        operateResult = content[index];
    };
    [self.view addSubview:self.PickerOne];
    [self.PickerOne show];
}
- (void)PickerViewRightButtonOncleck:(NSInteger)index{
    [self.PickerOne close];
}
@end
