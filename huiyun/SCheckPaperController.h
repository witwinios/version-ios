//
//  SCheckPaperController.h
//  huiyun
//
//  Created by MacAir on 2017/12/8.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectButton.h"
#import "SelectField.h"
#import "SelectTextview.h"
@interface SCheckPaperController : UIViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *cardBtn;

- (IBAction)cardAction:(id)sender;
- (IBAction)leftAction:(id)sender;
- (IBAction)rightAction:(id)sender;

@property (strong, nonatomic) onlineModel *model;
@end

