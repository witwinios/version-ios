//
//  AddFileViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/27.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseAddModel.h"
#import "ZCAssetsPickerViewController.h"
#import <Photos/Photos.h>
@interface AddFileViewController : UIViewController
@property(strong,nonatomic)TCourseAddModel *PlanModel;
@end
