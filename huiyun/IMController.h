//
//  IMController.h
//  yun
//
//  Created by MacAir on 2017/5/22.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TalkController.h"
#import "SPKitExample.h"
#import "GroupController.h"
#import "TeacherController.h"
#import "SubjectController.h"
#import "QuestionController.h"
@interface IMController : UIViewController<UITextViewDelegate,UIAlertViewDelegate>
@property(strong, nonatomic) NSMutableArray *selectGroup;
@property (strong, nonatomic) NSMutableArray *selectSubject;

@property (weak, nonatomic) IBOutlet UIView *navigationTView;

@property (weak, nonatomic) IBOutlet UILabel *oneBiao;
@property (weak, nonatomic) IBOutlet UILabel *threeBiao;
@property (weak, nonatomic) IBOutlet UIButton *oneContent;
@property (weak, nonatomic) IBOutlet UIButton *twoContent;
@property (weak, nonatomic) IBOutlet UIButton *rightItem;
@property (weak, nonatomic) IBOutlet UITextView *quesDes;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIButton *commitBtn;
@property (weak, nonatomic) IBOutlet UILabel *biaoti;

@property (strong, nonatomic)GroupController *groupVC;
@property (strong, nonatomic)SubjectController *subjectVC;

@property (strong, nonatomic, readwrite) YWIMKit *ywIMKit;
@property (strong, nonatomic, readwrite) YWTribe *tribe;
- (IBAction)commitAction:(id)sender;
- (IBAction)groupAction:(id)sender;
- (IBAction)subjectAction:(id)sender;
- (IBAction)quesAction:(id)sender;


//YWTribe
@end
