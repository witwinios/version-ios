//
//  AppDelegate.m
//  FastSdkDemo
//
//  Created by jiangcj on 16/10/11.
//  Copyright © 2016年 gensee. All rights reserved.
//

#import "AppDelegate.h"

#import "MainViewController.h"
#define IP @"http://www.hzwitwin.cn:81/witwin-ctts-web"
#define CODE @"witwin"
@interface AppDelegate (){
    BOOL isGoHome;
    UIBackgroundTaskIdentifier backIden;
}
@property (retain,nonatomic)  NSTimer *timer;

@end

@implementation AppDelegate
- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    /// 处理运行时APNS
    /// 现在您不需要手动调用，IMSDK会自动截获
    /// [[SPKitExample sharedInstance] exampleHandleRunningAPNSWithUserInfo:userInfo];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //
    [[SPKitExample sharedInstance] callThisInDidFinishLaunching];
    //
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.loginVC = [LoginController share];
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:self.loginVC];
    self.loginVC.from = @"delegate";
    self.window.rootViewController = navVC;
    [self.window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    if ([LocalIP isEqualToString:@""] || [LocalIP isKindOfClass:[NSNull class]] || LocalIP == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:IP forKey:@"scanIP"];
        [defaults setObject:CODE forKey:@"scanCODE"];
        [defaults setObject:@"http://www.hzwitwin.cn:81" forKey:@"simpleIp"];
        [defaults synchronize];
        //保存成功
    }
    //定位代理
    [self initializeLocationService];
    //直播代理
    _playerManager = [GSPPlayerManager sharedManager];
    _playerManager.delegate = self;
    /// 向APNS注册PUSH服务，需要区分iOS SDK版本和iOS版本。
//#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
//    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
//    {
//        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge|UIUserNotificationTypeAlert|UIUserNotificationTypeSound) categories:nil];
//        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
//
//    } else
//#endif
//    {
//        /// 去除warning
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wdeprecated-declarations"
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
//#pragma clang diagnostic pop
//    }
    
    /// 处理启动时携带的APNS消息
    /// 现在您不需要手动调用，IMSDK会自动截获
    /// [[SPKitExample sharedInstance] exampleHandleAPNSWithLaunchOptions:launchOptions];
    
    return YES;
}
- (void)initializeLocationService{
    // 初始化定位管理器
    _locationManager = [[CLLocationManager alloc] init];
    // 设置代理
    _locationManager.delegate = self;
    [_locationManager requestWhenInUseAuthorization];
    // 设置定位精确度到米
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    // 设置过滤器为无
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    // 开始定位
    // 取得定位权限，有两个方法，取决于你的定位使用情况
    // 一个是requestAlwaysAuthorization，一个是requestWhenInUseAuthorization
    
    // 开始定位
    
    NSLog(@"%d",[CLLocationManager locationServicesEnabled]);
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        [_locationManager startUpdatingLocation];
    }else {
        UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"提示" message:@"打开[定位服务]来允许[慧云医疗]确定您的位置?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *sureAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if( [[UIApplication sharedApplication]canOpenURL:url] ) {
                [[UIApplication sharedApplication] openURL:url];
            }
            
        }];
        UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];

        [alertController addAction:sureAction];
        [alertController addAction:cancelAction];
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    }
}
#pragma -定位代理
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // 获取当前所在的城市名
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    //根据经纬度反向地理编译出地址信息
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *array, NSError *error){
        if (array.count > 0){
            CLPlacemark *placemark = [array objectAtIndex:0];
            //获取城市
            NSString *city = placemark.locality;
            if (!city) {
                //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                city = placemark.administrativeArea;
            }
            NSString *address = [placemark.addressDictionary objectForKey:@"FormattedAddressLines"][0];;
            [[NSUserDefaults standardUserDefaults] setObject:address forKey:@"localAddress"];
        }
        else if (error == nil && [array count] == 0)
        {
            NSLog(@"无结果");
        }
        else if (error != nil)
        {
            NSLog(@"错误:%@", error);
        }
    }];
    //系统会一直更新数据，直到选择停止更新，因为我们只需要获得一次经纬度即可，所以获取之后就停止更新
    [_locationManager stopUpdatingLocation];
}
@end
