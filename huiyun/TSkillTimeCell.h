//
//  TSkillTimeCell.h
//  huiyun
//
//  Created by Bad on 2018/3/27.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSkillTestCenterModel.h"
@interface TSkillTimeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *OneLabel;
@property (weak, nonatomic) IBOutlet UILabel *TwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *ThreeLabel;
@property (weak, nonatomic) IBOutlet UILabel *FourLabel;
@property (weak, nonatomic) IBOutlet UILabel *FiveLabel;


-(void)setAddModel:(TSkillTestCenterModel *)model;

@end
