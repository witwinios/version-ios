//
//  TypicalCasesCell.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/10.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TypicalCasesModel.h"
@interface TypicalCasesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *TypicalCasesName;
@property (weak, nonatomic) IBOutlet UILabel *TypicalCasesBreed;
@property (weak, nonatomic) IBOutlet UILabel *TypicalCasesRecord;
@property (weak, nonatomic) IBOutlet UIButton *TypicalCasesBtn;

@property (weak, nonatomic) IBOutlet UIView *BgVc;




-(void)setProperty:(TypicalCasesModel *)model;


@end
