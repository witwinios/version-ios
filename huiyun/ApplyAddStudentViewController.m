//
//  ApplyAddStudentViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/6.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "ApplyAddStudentViewController.h"
#import "ApplyStudentCell.h"

@interface ApplyAddStudentViewController (){
    UIButton *rightBtn;
    UIButton *AddBtn;
    
    
    UITableView *courseTable;
    NSMutableArray *dataArray;
    
    NSString *StudentId;
    
    int currentPage;
    NSMutableArray *tempArray;
    NSMutableArray *selectArray;
    
    NSMutableArray  *searchArray;

    UIView *SearchView;
    UITextField *SearchField;
    UIButton *SearchBtn;
    
    UIImageView *backNavigation;
    
    BOOL isSearch;
}

@end

@implementation ApplyAddStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setsearchBar];
    [self setNav];
    [self setUI];
    
    [self loadData];
    
    [self setUpRefresh];
    
    
    //获取当前学生数据:
    [self ThisStudent];
   
    
    
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"报名学生";
    [backNavigation addSubview:titleLab];
    
    
    
    
    if([_scheduleStatusString isEqualToString:@"已发布"]){
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-80, 29.5, 100, 25);
    [rightBtn setTitle:@"添加" forState:UIControlStateNormal];
    
    [rightBtn addTarget:self action:@selector(saveStudent:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    
    AddBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    AddBtn.frame = CGRectMake(self.view.frame.size.width-80, 29.5, 100, 25);
    [AddBtn setTitle:@"保存" forState:UIControlStateNormal];
    [AddBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [AddBtn addTarget:self action:@selector(AddStudent) forControlEvents:UIControlEventTouchUpInside];
    AddBtn.hidden=YES;
    [backNavigation addSubview:AddBtn];
}
    
}

- (void)back :(UIButton *)button{
    
    if(selectArray.count >0){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您有尚未保存的上课学生，是否保存？" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self AddStudent];
        }];
        
        UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert addAction:noAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
      [self.navigationController popViewControllerAnimated:YES];
    }
    
    
   
}


-(void)saveStudent:(UIButton *)button{
  //  [courseTable setEditing:YES animated:YES];
    //
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您现在可以选择上课学生." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        rightBtn.hidden=YES;
        AddBtn.hidden=NO;
        _editing=1;
    }];
    
    UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:noAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];

}

-(void)setsearchBar{
    
    tempArray=[NSMutableArray array];
    selectArray=[NSMutableArray array];
    isSearch=NO;
    
    SearchView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 44)];
    SearchView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    UIImageView *SearchImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sousuo"]];
    SearchImg.backgroundColor=[UIColor clearColor];
    SearchImg.frame=CGRectMake(10, 6, 32, 32);
    
    
    SearchField=[[UITextField alloc]initWithFrame:CGRectMake(44, 6, Swidth-88, 32)];
    SearchField.backgroundColor=[UIColor clearColor];
    SearchField.placeholder=@"请输入搜索的学生名字";
    SearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    SearchField.clearsOnBeginEditing = YES;
    SearchField.keyboardType=UIKeyboardTypeDefault;
    SearchField.returnKeyType=UIReturnKeySearch;
    SearchField.delegate = self;
    [SearchField addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    SearchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SearchBtn.frame=CGRectMake((SearchField.wwy_x+SearchField.wwy_width)+2, 6, 40, 32);
    [SearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [SearchBtn addTarget:self action:@selector(setUpRefresh) forControlEvents:UIControlEventTouchUpInside];
    SearchBtn.backgroundColor=UIColorFromHex(0x20B2AA);
    SearchBtn.layer.cornerRadius=5;
    SearchBtn.hidden=YES;
    [SearchView addSubview:SearchBtn];
    [SearchView addSubview:SearchField];
    [SearchView addSubview:SearchImg];
    [self.view addSubview:SearchView];
    
}


-(BOOL)ApplyTextDidChange:(UITextField *)theTextField{
    
    NSLog( @"text changed: %@", theTextField.text);
    
    
    if (![theTextField.text isEqualToString:@""]) {
        SearchBtn.hidden=NO;
        isSearch=YES;
    }else{
        isSearch=NO;
        [self setUpRefresh];
        SearchBtn.hidden=YES;
        [SearchField  resignFirstResponder];
    }
    
    return YES;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [SearchField  resignFirstResponder];
    
 
        [self setUpRefresh ];
 
    
    
    NSLog(@"点击了搜索");
    
    return YES;
    
}

- (void)setUpRefresh{
     [SearchField  resignFirstResponder];
    
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中。。。";
    
}


//刷新
-(void)loadData{
    
    if (_editing) {
        [selectArray removeAllObjects];
    }
    
    currentPage=1;
    NSString *URL;
    if (isSearch == NO) {
        URL=[NSString stringWithFormat:@"%@/users/students?pageStart=%d&pageSize=20&accountStatus=active",LocalIP,currentPage];
    }else if(isSearch == YES){
        NSString *str=SearchField.text;
        URL=[NSString stringWithFormat:@"%@/users/students?pageStart=1&pageSize=999&accountStatus=active&fullName=%@",LocalIP,str];
        URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }

    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        
        if([result[@"responseStatus"] isEqualToString:@"succeed"]){
            
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无学生!" andCurentVC:self];
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
            }else{
                
                for (int i=0; i<array.count; i++) {
                     NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    
                    ApplyStudent *model=[ApplyStudent new];
                    
                    model.ApplyStudentName=[dictionary objectForKey:@"fullName"];
                    model.ApplyStudentID=[dictionary objectForKey:@"userId"];
                    
                    
                    if(![[dictionary objectForKey:@"picture"] isKindOfClass:[NSNull class]]){
                        
                        
                        model.ApplyStudentImg=[[dictionary objectForKey:@"picture"] objectForKey:@"fileUrl"];
                        
                        NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.ApplyStudentImg];
                        
                        model.ApplyStudentImg=url;
                        
                    }
                    
                    
                    [dataArray addObject:model];
                    
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
                
            }
        }else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }

    } failed:^(NSString *result) {
        if (![result isEqualToString:@"200"]) {
            [courseTable headerEndRefreshing];
        }
        return;
    }];
  
}



//加载
-(void)loadRefresh{
currentPage ++ ;
    
    NSString *URL;
    if (isSearch == NO) {
        URL=[NSString stringWithFormat:@"%@/users/students?pageStart=%d&pageSize=20&accountStatus=active",LocalIP,currentPage];
    }else if(isSearch == YES){
        NSString *str=SearchField.text;
        URL=[NSString stringWithFormat:@"%@/users/students?pageStart=1&pageSize=999&accountStatus=active&fullName=%@",LocalIP,str];
        URL=[URL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {

        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            return;
        }
        [courseTable footerEndRefreshing];
        
        if([result[@"responseStatus"] isEqualToString:@"succeed"]){
            
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
                [Maneger showAlert:@"无学生!" andCurentVC:self];
                [courseTable reloadData];
                [courseTable footerEndRefreshing];
            }else{
                NSMutableArray *newArray=[NSMutableArray new];
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    
                    ApplyStudent *model=[ApplyStudent new];
                    
                    model.ApplyStudentName=[dictionary objectForKey:@"fullName"];
                    model.ApplyStudentID=[dictionary objectForKey:@"userId"];
                    
                    
                    if(![[dictionary objectForKey:@"picture"] isKindOfClass:[NSNull class]]){
                        
                        
                        model.ApplyStudentImg=[[dictionary objectForKey:@"picture"] objectForKey:@"fileUrl"];
                        
                        NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.ApplyStudentImg];
                        
                        model.ApplyStudentImg=url;
                        
                    }
                    
                    
                    [newArray addObject:model];
                    
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)newArray.count);
                
                NSRange range = NSMakeRange(dataArray.count,newArray.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newArray atIndexes:set];
                [courseTable reloadData];
            }
        }else{
            currentPage--;
            [MBProgressHUD showToastAndMessage:@"请求数据失败!" places:0 toView:nil];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];

}




-(void)ThisStudent{
    
    
    NSLog(@"scheduleIdString:%@",_scheduleIdString);
    NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules/%@/registeredStudents?pageSize=999",LocalIP,_scheduleIdString];
    
    [RequestTools RequestWithURL:URL Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            if (array.count ==0) {
    
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    ApplyStudent *model=[ApplyStudent new];
                    
                    model.ApplyStudentName=[dictionary objectForKey:@"fullName"];
                    model.ApplyStudentID=[dictionary objectForKey:@"userId"];
                    
                    
                    if(![[dictionary objectForKey:@"picture"] isKindOfClass:[NSNull class]]){
                        
                        
                        model.ApplyStudentImg=[[dictionary objectForKey:@"picture"] objectForKey:@"fileUrl"];
                        
                        NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.ApplyStudentImg];
                        
                        model.ApplyStudentImg=url;
                        
                    }

                    [_selectorPatnArray addObject:model];
 
                }
                NSLog(@"_selectorPatnArray=%lu",(unsigned long)_selectorPatnArray.count);
                
                [courseTable reloadData];
               
                
            }
        }else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        if (![result isEqualToString:@"200"]) {
            [courseTable headerEndRefreshing];
        }
        return;
    }];
    
    
    
}



-(void)setUI{
    
    currentPage = 1;
    CGFloat courseTable_Y=backNavigation.wwy_height+SearchView.wwy_height;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, courseTable_Y,Swidth, Sheight-courseTable_Y) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    courseTable.estimatedRowHeight = 0;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [courseTable registerNib:[UINib nibWithNibName:@"ApplyStudentCell" bundle:nil] forCellReuseIdentifier:@"applyCell"];
    [self.view addSubview:courseTable];
     _selectorPatnArray=[NSMutableArray array];
     dataArray=[NSMutableArray array];
    
    tempArray=[NSMutableArray array];
    selectArray=[NSMutableArray array];
    isSearch = NO;
      _editing=0;
}

//返回行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

        return  dataArray.count;
}

//返回行高：
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 95;
}

//点击行事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if(_editing){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        NSString *row=[NSString stringWithFormat:@"%ld",indexPath.row];
        NSLog(@"indexPath.row:%@",row);
        ApplyStudent *model=dataArray[indexPath.row];
        model.isCheck=!model.isCheck;
        
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        
        if (model.isCheck == 0) {
            [selectArray removeObject:model];
            NSLog(@"Remove_selectArray:%@",selectArray);
        }else if(model.isCheck == 1){

            [selectArray addObject:model];
            NSLog(@"Add_selectArray:%@",selectArray);
            
            
        }
        
    }

}

//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ApplyStudentCell *cell=[tableView dequeueReusableCellWithIdentifier:@"applyCell"];
    
    
    
    if(cell == nil){
        cell=[[ApplyStudentCell alloc]initWithStyle:UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert reuseIdentifier:@"applyCell" ];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    

        ApplyStudent *model=dataArray[indexPath.row];
        [cell setProperty:model];


    return cell;
  
}

-(void)AddStudent{
    
    AddBtn.hidden=YES;
    rightBtn.hidden=NO;
    [courseTable setEditing:NO animated:YES];
    [_selectorPatnArray addObjectsFromArray:selectArray];
    
    NSLog(@"_selectorPatnArray:%@",_selectorPatnArray);
    
    NSMutableArray *IDArr = [NSMutableArray new];
    for (ApplyStudent *model in _selectorPatnArray) {
        [IDArr addObject:model.ApplyStudentID];
    }
    //the_student_already_registered
     //http://liudeliang22.picp.io:14715/witwin-ctts-web/courseSchedules/%@/registeredStudents
    
        NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules/%@/registeredStudents",LocalIP,_scheduleIdString];
    
        [RequestTools RequestWithURL:URL Method:@"post" Params:IDArr Success:^(NSDictionary *result) {
            
            NSLog(@"%@",result);
          //  [self.navigationController popViewControllerAnimated:YES];
            if (selectArray.count == 0) {
                NSLog(@"未选择学生");
            }else{
                
                if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
                    [Maneger showAlert:@"学生添加成功!" andCurentVC:self];
                    // _backArr(_selectorPatnArray);
                    [_selectorPatnArray removeAllObjects];
                    [selectArray removeAllObjects];
                    [self.navigationController popViewControllerAnimated:YES];
                    _editing=0;
                }else{
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"报名失败。请重新选择！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
                
                
               
            }
            
        } failed:^(NSString *result) {
            if (![result isEqualToString:@"200"]) {
                [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            }else{
                [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
            }
        }];
    
}


@end
