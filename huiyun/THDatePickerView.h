

#import <UIKit/UIKit.h>


/*
 SelectTypeBefore 当前时间之前可以选择
 **/
typedef NS_ENUM(NSInteger, SelectType) {
    SelectTypeBefore = 0,
    SelectTypeNormal
};
typedef NS_ENUM(NSInteger, DateType) {
    DateTypeAll = 0,
    DateTypeDay
};

@protocol THDatePickerViewDelegate <NSObject>

/**
 保存按钮代理方法
 
 @param timer 选择的数据
 */
- (void)datePickerViewSaveBtnClickDelegate:(NSString *)timer;

/**
 取消按钮代理方法
 */
- (void)datePickerViewCancelBtnClickDelegate;

@end
//传值block
typedef void (^dateBlock)(NSString *date,NSString *longDate);

typedef void (^cancelBlock)(void);
@interface THDatePickerView : UIView

@property (copy, nonatomic) NSString *title;
@property (weak, nonatomic) id <THDatePickerViewDelegate> delegate;
@property (strong, nonatomic) dateBlock sureBlock;
@property (strong, nonatomic) cancelBlock cancelBlock;
@property (assign, nonatomic) SelectType selectType;
@property (assign, nonatomic) DateType dateType;
/// 显示
- (void)show;
- (instancetype)initWithFrame:(CGRect)frame andSureBlock:(dateBlock)sure andCancelBlock:(cancelBlock)cancel;
- (instancetype)initWithFrame:(CGRect)frame andSureBlock:(dateBlock)sure andCancelBlock:(cancelBlock)cancel dateType:(DateType)dateType;

@end

