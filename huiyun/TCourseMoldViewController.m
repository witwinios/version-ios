//
//  TCourseMoldViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TCourseMoldViewController.h"


@interface TCourseMoldViewController ()<NewMoldDelegate>

@end

@implementation TCourseMoldViewController{

    UITableView *courseTable;
    NSMutableArray *dataArray;
    int currentPage;
    
    NSInteger line;
    
    
    
    NSString *MoldId;
    NSString *MoldName;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNav];

    [self setUI];
    
    
    [self loadData];
   
}

- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"科目类型";
    [backNavigation addSubview:titleLab];
}

#pragma mark 确定：



#pragma mark 回退
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)loadData{
    //%@/subjects?pageStart=%d&pageSize=999
    currentPage=1;
    NSString *Url=[NSString stringWithFormat:@"%@/subjects?pageStart=%d&pageSize=999",LocalIP,currentPage];
    
   
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
           
            if (array.count ==0) {
                [Maneger showAlert:@"暂无科目类型!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    CourseMoldModel *model=[CourseMoldModel new];
                    model.MoldId=[dictionary objectForKey:@"subjectId"];
                    model.MoldName=[dictionary objectForKey:@"subjectName"];
                    model.ChildrenNum=[dictionary objectForKey:@"childrenSubjectsNum"];
                    
                    model.ChildMoldArray=[NSMutableArray new];
                    
                    int Num=[model.ChildrenNum intValue];
                    NSArray *childer=[dictionary objectForKey:@"childrenSubjects"];
                    if (Num >0) {
                        for (int j=0; j<Num; j++) {
                            NSDictionary *childDictionary=[childer objectAtIndex:j];
                            CourseMoldModel *Chidermodel=[CourseMoldModel new];
                            
                            Chidermodel.ChildMoldId=[childDictionary objectForKey:@"subjectId"];
                            Chidermodel.ChildMoldName=[childDictionary objectForKey:@"subjectName"];
                            
                            [model.ChildMoldArray addObject:Chidermodel];
                        }
                    }
                    
                    
                    
                    model.ParentId=[[dictionary objectForKey:@"metadata"]objectForKey:@"parentId"];
                    NSString *str=[NSString stringWithFormat:@"%@",model.ParentId];
                   
                   
                    if ([str isEqualToString:@"0"]) {
                        [dataArray addObject:model];
                    }
                    
        
                    
                    
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
                
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
    } failed:^(NSString *result) {
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
    
    
}

-(void)setUI{
    currentPage = 1;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    
    [courseTable registerNib:[UINib nibWithNibName:@"MoldCell" bundle:nil] forCellReuseIdentifier:@"cellStyle1"];
    [self.view addSubview:courseTable];
    currentPage=1;
    dataArray=[NSMutableArray new];
}
#pragma mark---TableView
//行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

//点击Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CourseMoldModel *model=dataArray[indexPath.row];
    
    int Num=[model.ChildrenNum intValue];
    
    MoldId=[NSString stringWithFormat:@"%@",model.MoldId];
    MoldName=model.MoldName;

//    if (Num > 0) {
//        CourseMoldModel *model=dataArray[indexPath.row];
//        NewMoldViewController *vc=[[NewMoldViewController alloc]init];
//        vc.DataArray=model.ChildMoldArray;
//        vc.delegate=self;
//        [self.navigationController pushViewController:vc animated:YES];
//    }else{
        [self.delegate text:MoldName dateID:MoldId];
        NSLog(@"MoldName:%@ MoldId:%@ ",model.MoldName,[NSString stringWithFormat:@"%@",model.MoldId]);
        [self.navigationController popViewControllerAnimated:YES];
//    }

}

-(void)Moldtext:(NSString *)moldstr MoldID:(NSString *)moldID{
    
    
    [self.delegate text:moldstr dateID:[NSString stringWithFormat:@"%@",moldID]];
    [self.navigationController popViewControllerAnimated:YES];
    
}



//加载的Cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MoldCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
    
    CourseMoldModel *model=dataArray[indexPath.row];
    [cell setProperty:model];
    
    int Num=[model.ChildrenNum intValue];
    if (Num>0) {
        cell.RightBtn.hidden=NO;
    }else{
        cell.RightBtn.hidden=YES;
    }
    
    cell.RightBtn.tag=999+indexPath.row;
    
    [cell.RightBtn addTarget:self action:@selector(NextMold:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return  cell;
}
//返回行高：
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}


-(void)NextMold:(UIButton *)sender{
    line=sender.tag-999;
    NSLog(@"line1:%ld",line);
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem: line inSection:0];
    

    CourseMoldModel *model=dataArray[line];
    NewMoldViewController *vc=[[NewMoldViewController alloc]init];
    vc.DataArray=model.ChildMoldArray;
    vc.delegate=self;
    [self.navigationController pushViewController:vc animated:YES];

}



@end
