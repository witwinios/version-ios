//
//  ExamPointModel.m
//  yun
//
//  Created by MacAir on 2017/7/20.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ExamPointModel.h"

@implementation ExamPointModel
//@property (strong, nonatomic) NSNumber *ExamPointModelStationId;
//@property (strong, nonatomic) NSString *ExamPointModelSubject;
//@property (strong, nonatomic) NSString *ExamPointModelCategory;
//@property (strong, nonatomic) NSString *ExamPointModelStationName;
//@property (strong, nonatomic) NSNumber *ExamPointModelUseds;
//@property (strong, nonatomic) NSNumber *ExamPointModelInternal;
- (void)setExamPointModelSubject:(NSString *)ExamPointModelSubject{
    if ([ExamPointModelSubject isKindOfClass:[NSNull class]]) {
        _ExamPointModelSubject = @"暂无";
    }else{
        _ExamPointModelSubject = ExamPointModelSubject;
    }
}
- (void)setExamPointModelCategory:(NSString *)ExamPointModelCategory{
    if ([ExamPointModelCategory isKindOfClass:[NSNull class]]) {
        _ExamPointModelCategory = @"暂无";
    }else{
        _ExamPointModelCategory = ExamPointModelCategory;
    }
}

@end
