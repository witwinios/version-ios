//
//  ExamModelCell.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OSCEModel.h"
#import "onlineModel.h"
#import "SkillModel.h"
#import "Maneger.h"
@interface ExamModelCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *testName;

@property (weak, nonatomic) IBOutlet UILabel *testStatus;

@property (weak, nonatomic) IBOutlet UILabel *testCategory;

@property (weak, nonatomic) IBOutlet UILabel *testClassRoom;
@property (weak, nonatomic) IBOutlet UILabel *testTime;

- (void)setProperty:(id)model andIndex:(NSInteger)index;
@end
