//
//  TableViewCell.h
//  huiyun
//
//  Created by Bad on 2018/3/23.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSkillTestCenterModel.h"
@interface TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *OneLabel;
@property (weak, nonatomic) IBOutlet UILabel *TwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *ThreeLabel;
@property (weak, nonatomic) IBOutlet UILabel *FourLabel;
@property (weak, nonatomic) IBOutlet UILabel *FiveLabel;
@property (weak, nonatomic) IBOutlet UILabel *SixLabel;
@property (weak, nonatomic) IBOutlet UILabel *SevenLabel;
@property (weak, nonatomic) IBOutlet UILabel *EightLabel;



-(void)setTeacherAddModel:(TSkillTestCenterModel *)model;

-(void)setStudentAddModel:(TSkillTestCenterModel *)model;


@end
