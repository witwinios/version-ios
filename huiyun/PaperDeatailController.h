//
//  PaperDeatailController.h
//  yun
//
//  Created by MacAir on 2017/8/15.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaperCellModel.h"
#import "QuestionModel.h"
@interface PaperDeatailController : UIViewController

@property (strong, nonatomic) PaperCellModel *paperModel;
@property (strong, nonatomic) SelfTestModel *testModel;

@end
