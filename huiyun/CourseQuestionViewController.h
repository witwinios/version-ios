//
//  CourseQuestionViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseModel.h"
#import "CourseQuestionDetailsController.h"
#import "CourseQuestionBView.h"

@interface CourseQuestionViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic)TCourseAddModel *PlanModel;
@property(nonatomic,getter=isEditing) BOOL editing;
@property(nonatomic,strong)NSString *str;
@property(nonatomic,strong) NSString *courseStr;
@property (strong, nonatomic) TCourseModel *model;
-(void)setEditing:(BOOL)editing animated:(BOOL)animated;

@property(nonatomic,strong)NSString *NextVc;
@property(nonatomic,strong)NSString *lastStr;

@end
