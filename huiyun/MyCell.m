//
//  MyCell.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/10.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "MyCell.h"

@implementation MyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.CourseBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseBtn.layer.borderWidth=1;
    self.CourseBtn.layer.cornerRadius=10;
    self.CourseName.font=[UIFont systemFontOfSize:17];
    [self.CourseName sizeToFit];
    self.CourseName.numberOfLines = 0;
    self.backgroundColor=[[UIColor darkGrayColor]colorWithAlphaComponent:0.4];
    
}



- (void)setProperty:(TCourseModel *)model{
    
    self.CourseName.text=model.courseName;
    
    self.CourseState.text=model.scheduleStatus;
    
    self.CourseTime.text=[[Maneger shareObject] timeFormatter:model.startTime.stringValue];
    
    self.CourseLocation.text=model.coursePlace;
    

}







@end
