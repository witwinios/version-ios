//
//  CourseQuestionModel.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "CourseQuestionModel.h"

@implementation CourseQuestionModel

-(void)setQuestionId:(NSNumber *)QuestionId{
    if ([QuestionId isKindOfClass:[NSNull class]]) {
        _QuestionId = [NSNumber numberWithInt:0];
    }else{
        _QuestionId = QuestionId;
    }
}

-(void)setQuestionType:(NSString *)QuestionType{
    if ([QuestionType isKindOfClass:[NSNull class]]) {
        _QuestionType = @"暂无";
    }else if([QuestionType isEqualToString:@"choice_question"]){
        _QuestionType=@"A1:单句型题";
    }else if([QuestionType isEqualToString:@"a2_choice_question"]){
        _QuestionType=@"A2:病例摘要题";
    }else if([QuestionType isEqualToString:@"a3_choice_question"]){
        _QuestionType=@"A3:病例组型题";
    }else if([QuestionType isEqualToString:@"a4_choice_question"]){
        _QuestionType=@"A4:病例串型题";
    }else if([QuestionType isEqualToString:@"b1_choice_question"]){
        _QuestionType=@"B1:标准配伍题型";
    }else if([QuestionType isEqualToString:@"x_choice_question"]){
        _QuestionType=@"X型多选题";
    }else if([QuestionType isEqualToString:@"completion_question"]){
        _QuestionType=@"填空题";
    }else if([QuestionType isEqualToString:@"diagnosis_question"]){
        _QuestionType=@"诊断题";
    }else if([QuestionType isEqualToString:@"essay_question"]){
        _QuestionType=@"问答题";
    }
}

-(void)setDifficulty:(NSNumber *)Difficulty{
    if ([Difficulty isKindOfClass:[NSNull class]]) {
        _Difficulty = [NSNumber numberWithInt:0];
    }else{
        _Difficulty = Difficulty;
    }
}

-(void)setChildrenQuestionsNum:(NSNumber *)ChildrenQuestionsNum{
    if ([ChildrenQuestionsNum isKindOfClass:[NSNull class]]) {
        _ChildrenQuestionsNum = [NSNumber numberWithInt:0];
    }else{
        _ChildrenQuestionsNum = ChildrenQuestionsNum;
    }
}

-(void)setQuestionTitle:(NSString *)QuestionTitle{
    if ([QuestionTitle isKindOfClass:[NSNull class]]) {
        _QuestionTitle = @"暂无";
    }else if(QuestionTitle.length == 0){
        _QuestionTitle=@"无";
    }else{
        _QuestionTitle = QuestionTitle;
    }
}

-(void)setCategoryName:(NSString *)CategoryName{
    if ([CategoryName isKindOfClass:[NSNull class]]) {
        _CategoryName = @"暂无";
    }else{
        _CategoryName = CategoryName;
    }
}

-(void)setSubjectName:(NSString *)SubjectName{
    if ([SubjectName isKindOfClass:[NSNull class]]) {
        _SubjectName = @"暂无";
    }else{
        _SubjectName = SubjectName;
    }
}

-(void)setScoreNum:(NSString *)ScoreNum{
    if ([ScoreNum isKindOfClass:[NSNull class]]) {
        _ScoreNum = @"暂无";
    }else{
        _ScoreNum = ScoreNum;
    }
}

-(void)setChildrenQuestionId:(NSNumber *)ChildrenQuestionId{
    if ([ChildrenQuestionId isKindOfClass:[NSNull class]]) {
        _ChildrenQuestionId = [NSNumber numberWithInt:0];
    }else{
        _ChildrenQuestionId = ChildrenQuestionId;
    }
}

-(void)setChildrenQuestionTitle:(NSString *)ChildrenQuestionTitle{
    if ([ChildrenQuestionTitle isKindOfClass:[NSNull class]]) {
        _ChildrenQuestionTitle = @"暂无";
    }else if(ChildrenQuestionTitle.length == 0){
        _ChildrenQuestionTitle=@"无";
    }else{
        _ChildrenQuestionTitle = ChildrenQuestionTitle;
    }
}

-(void)setChildrenQuestionType:(NSString *)ChildrenQuestionType{
    if ([ChildrenQuestionType isKindOfClass:[NSNull class]]) {
        _ChildrenQuestionType = @"暂无";
    }else if([ChildrenQuestionType isEqualToString:@"choice_question"]){
        _ChildrenQuestionType=@"A1:单句型题";
    }else if([ChildrenQuestionType isEqualToString:@"a2_choice_question"]){
        _ChildrenQuestionType=@"A2:病例摘要题";
    }else if([ChildrenQuestionType isEqualToString:@"a3_choice_question"]){
        _ChildrenQuestionType=@"A3:病例组型题";
    }else if([ChildrenQuestionType isEqualToString:@"a4_choice_question"]){
        _ChildrenQuestionType=@"A4:病例串型题";
    }else if([ChildrenQuestionType isEqualToString:@"b1_choice_question"]){
        _ChildrenQuestionType=@"B1:标准配伍题型";
    }else if([ChildrenQuestionType isEqualToString:@"x_choice_question"]){
        _ChildrenQuestionType=@"X型多选题";
    }else if([ChildrenQuestionType isEqualToString:@"completion_question"]){
        _ChildrenQuestionType=@"填空题";
    }else if([ChildrenQuestionType isEqualToString:@"diagnosis_question"]){
        _ChildrenQuestionType=@"诊断题";
    }else if([ChildrenQuestionType isEqualToString:@"essay_question"]){
        _ChildrenQuestionType=@"问答题";
    }
}

-(void)setAnalysis:(NSString *)Analysis{
    if ([Analysis isKindOfClass:[NSNull class]]) {
        _Analysis = @"暂无";
    }else{
        _Analysis = Analysis;
    }
}

-(void)setAnswers:(NSNumber *)Answers{
    if ([Answers isKindOfClass:[NSNull class]]) {
        _Answers= [NSNumber numberWithInt:0];
    }else{
        _Answers = Answers;
    }
}

-(void)setQuestionFile:(NSString *)QuestionFile{
    if ([QuestionFile isKindOfClass:[NSNull class]]) {
        _QuestionFile = @"暂无";
    }else{
        _QuestionFile = QuestionFile;
    }
}

-(void)setChildrensQuestionFile:(NSString *)ChildrensQuestionFile{
    if ([ChildrensQuestionFile isKindOfClass:[NSNull class]]) {
        _ChildrensQuestionFile = @"暂无";
    }else{
        _ChildrensQuestionFile = ChildrensQuestionFile;
    }
}

@end
