//
//  MistakeDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/11/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "MistakeDetailController.h"

@interface MistakeDetailController ()
{
    NSArray *titleArray;
    NSArray *heightArr;
    PersonEntity *persion;
    
    UITextView *currentTextview;
    CGRect _activedTextFieldRect;
    CGPoint currentPoint;
    UIView *toolbar;

    UITextView *dealAdvice;
    UITextView *leadName;
    UITextView *leadAdvice;
}
@end

@implementation MistakeDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titles = @"差错记录";
    self.rightBtn.hidden = YES;
    self.rightTitle = @"修改";
    self.table.bounces = NO;
//    dealAdvice = self.model.caseMistakeModelAdvice;
//    leadName = self.model.casemis
    persion = [[NSuserDefaultManager share] readCurrentUser];
    if (persion.roleId.intValue == 2) {
        self.rightBtn.hidden = YES;
    }else{
        if ([self.model.caseMistakeModelStatus isEqualToString:@"待审核"]) {
            MistakeDetailController *weakSelf = self;
            
            if ([_SDepartModel.SDepartStatus isEqualToString:@"出科"]) {
                 self.rightBtn.hidden = YES;
            }
            
            self.rightBtn.hidden = NO;
            self.rightBlock = ^(){
                [SubjectTool share].subjectType = subjectTypeUpdate;
                MistakeRecordController *misVC = [MistakeRecordController new];
                misVC.myModel = weakSelf.model;
                [weakSelf.navigationController pushViewController:misVC animated:YES];
            };
        }else{
            if ([_SDepartModel.SDepartStatus isEqualToString:@"出科"]) {
                self.rightBtn.hidden = YES;
            }
            
            self.rightBtn.hidden = YES;
        }

    }
    titleArray = @[@"审核状态:",@"差错类别:",@"差错等级:",@"发生时间:",@"带教老师:",@"发生部门:",@"详细经过:",@"差错原因:",@"差错教训:",@"处理意见:",@"院领导:",@"院领导意见:"];
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"44",@"103",@"103",@"103",@"103",@"44",@"103"];
    self.dataArray = @[_model.caseMistakeModelStatus,_model.caseMistakeModelCategory,_model.caseMistakeModelDegree,[[Maneger shareObject] timeFormatter1:_model.caseMistakeModelDate.stringValue],_model.caseMistakeModelTeacher,_model.caseMistakeModelDepartmentName,_model.caseMistakeModelViaDescription,_model.caseMistakeModelReason,_model.caseMistakeModelLession,_model.caseMistakeModelAdvice,_model.caseMistakeModelLeader,_model.caseMistakeModelLeaderAdvice];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    //注册键盘出现的通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];
}
- (void)keyboardWillHide:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
        self.table.contentOffset = currentPoint;
    }];
}
- (void)hideKb:(UIButton *)btn{
    [currentTextview resignFirstResponder];
}
- (void)keyboardWillShow:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
    if ((_activedTextFieldRect.origin.y + _activedTextFieldRect.size.height) + 40>  ([UIScreen mainScreen].bounds.size.height - rect.size.height)) {//键盘的高度 高于textView的高度 需要滚动
        currentPoint = self.table.contentOffset;
        [UIView animateWithDuration:duration animations:^{
            self.table.contentOffset = CGPointMake(0, 64 + _activedTextFieldRect.origin.y + _activedTextFieldRect.size.height - ([UIScreen mainScreen].bounds.size.height - rect.size.height)+44);
        }];
    }
}
- (void)approveAction:(UIButton *)appBtn{
    
    if ([dealAdvice.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"请输入处理意见~" places:0 toView:nil];
        return;
    }
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认通过?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentMalpracticeRecords/%@/review",SimpleIp,_model.recordId];
        
        [RequestTools RequestWithURL:request Method:@"PUT" Params:@{@"reviewerId":persion.userID,@"reviewerComments":dealAdvice.text,@"reviewStatus":@"approved"} Message:@"审批中..." Success:^(NSDictionary *result) {
            
            [MBProgressHUD showToastAndMessage:@"审批成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"审批失败~" places:0 toView:nil];
        }];
        
    }];
    [alertVC addAction:cancel];
    [alertVC addAction:sure];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    _activedTextFieldRect = [textView convertRect:textView.frame toView:self.table];
    currentTextview = textView;
    return YES;
}
- (void)rejectAction:(UIButton *)rejBtn{
    if ([dealAdvice.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"请输入处理意见~" places:0 toView:nil];
        return;
    }
    
    if (currentTextview.text.length == 0 ) {
        [MBProgressHUD showToastAndMessage:@"请输入主管意见~" places:0 toView:nil];
        return;
    }

    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认未通过?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentMalpracticeRecords/%@/review",SimpleIp,_model.recordId];
        
        NSDictionary *dic=@{
                            @"reviewerId":persion.userID,
                            @"reviewerComments":currentTextview.text,
                            @"reviewStatus":@"rejected"
                            
                            } ;
        
        NSLog(@"dic == %@",dic);
        
        [RequestTools RequestWithURL:request Method:@"PUT" Params:dic Message:@"审批中..." Success:^(NSDictionary *result) {
            
            [MBProgressHUD showToastAndMessage:@"审批成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"审批失败~" places:0 toView:nil];
        }];
    }];
    [alertVC addAction:cancel];
    [alertVC addAction:sure];
    [self presentViewController:alertVC animated:YES completion:nil];
}
#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (persion.roleId.intValue == 2 && [_model.caseMistakeModelStatus isEqualToString:@"待审核"]) {
        return 40.f;
    }
    return 0.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *foot = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 40)];
    foot.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIButton *approveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    approveBtn.frame = CGRectMake(0, 0, Swidth/2, 40);
    approveBtn.backgroundColor = UIColorFromHex(0x20B2AA);
    [approveBtn setTitle:@"通过" forState:0];
    [approveBtn addTarget:self action:@selector(approveAction:) forControlEvents:UIControlEventTouchUpInside];
    [foot addSubview:approveBtn];
    
    UIButton *rejectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rejectBtn.frame = CGRectMake(Swidth/2, 0, Swidth/2, 40);
    rejectBtn.backgroundColor = [UIColor redColor];
    [rejectBtn setTitle:@"未通过" forState:0];
    [rejectBtn addTarget:self action:@selector(rejectAction:) forControlEvents:UIControlEventTouchUpInside];
    [foot addSubview:rejectBtn];
    return foot;
}

#pragma -delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *one = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *two = [tableView dequeueReusableCellWithIdentifier:@"two"];
    DetailLeaderCell *three = [tableView dequeueReusableCellWithIdentifier:@"leadCell"];
    if (indexPath.row == 8 || indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 9 || indexPath.row == 11) {
        if (indexPath.row == 9 && [_model.caseMistakeModelStatus isEqualToString:@"待审核"] && persion.roleId.intValue == 2) {
            dealAdvice = two.contentText;
            [two.contentText setEditable:YES];
        }else if (indexPath.row == 11){
            leadAdvice = two.contentText;
            [two.contentText setEditable:YES];
        }else{
            [two.contentText setEditable:YES];
        }
        two.titleText.text = titleArray[indexPath.row];
        two.contentText.text = self.dataArray[indexPath.row];
        two.contentText.delegate = self;
        return two;
    }else if (indexPath.row == 10){
        leadName = three.contentText;
        [leadName setEditable:YES];
        three.contentText.delegate = self;
        return three;
    }else {
        one.titleLab.text = titleArray[indexPath.row];
        one.contentLab.text = self.dataArray[indexPath.row];
        return one;
    }
}
@end
