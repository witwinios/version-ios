//
//  SCaseRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SCaseRecordController.h"
#import "THDatePickerView.h"
@interface SCaseRecordController ()
{
    NSString *current_date;
    UIView *btnBack;
    
    UIView *toolbar;
    
    NSNumber *has_image;
    
    NSString *currentDoctor;
    NSMutableArray *idArr;
    NSMutableArray *nameArr;
    
    long current_time;
    long  CurrentTimeNunm;
    
    int  *Doctor;
    
}

@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) UIButton *btn;
@end
@implementation SCaseRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    idArr = [NSMutableArray new];
    nameArr = [NSMutableArray new];
    current_date = @"";
    has_image = @0;
    Doctor=0;
    currentDoctor = @"暂无";
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];

    if (![self.myModel isKindOfClass:[NSNull class]] || self.model != nil) {
        self.diseaseNameField.text = self.myModel.caseOne;
        self.diseaseNumField.text = self.myModel.caseThree;
        self.mainText.text = self.myModel.mainIos;
        self.secodText.text = self.myModel.secondIos;
        [self.doctorBtn setTitle:self.myModel.doctor forState:UIControlStateNormal];
        if (self.myModel.caseFive.intValue == 0) {
            current_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseFive];
            [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:self.myModel.caseFive.stringValue] forState:0];
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonHide:) name:UIKeyboardWillHideNotification object:nil];
    //
    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];
    self.dateBtn.layer.cornerRadius = 4;
    self.dateBtn.layer.masksToBounds = YES;
    self.dateBtn.titleLabel.textAlignment = 0;
    self.doctorBtn.layer.cornerRadius = 4;
    self.doctorBtn.layer.masksToBounds = YES;
    self.mainText.layer.cornerRadius = 4;
    self.mainText.layer.masksToBounds = YES;
    self.secodText.layer.cornerRadius = 4;
    self.secodText.layer.masksToBounds = YES;
    self.saveBtn.layer.cornerRadius = 4;
    self.saveBtn.layer.masksToBounds = YES;
    
    

    
    
    THDatePickerView *date = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:longDate] forState:0];
        current_date = longDate;
        //选择的时间大于当前时间 提示 时间错误
        current_time=[current_date longLongValue];
        
            //获取当前时间
            NSDate *datenow = [NSDate date];
            
            NSString* currentTime=[NSString stringWithFormat:@"%0.f",[datenow timeIntervalSince1970]*1000];
            
          NSString * currentTimeString =currentTime;
        
        
          CurrentTimeNunm =[currentTimeString longLongValue];
        
        if (current_time > CurrentTimeNunm) {
            [MBProgressHUD showToastAndMessage:@"您选中时间大于当前时间，请重新选择" places:0 toView:nil];
        }
        
        
    } andCancelBlock:^{
        self.btn.hidden = YES;
    } dateType:DateTypeDay];
    [self.view addSubview:date];
    self.dateView = date;
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"病例记录";
    [backNavigation addSubview:titleLab];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self resignResponder];
}
- (void)resignResponder{
    [self.diseaseNameField resignFirstResponder];
    [self.diseaseNumField resignFirstResponder];
    [self.mainText resignFirstResponder];
    [self.secodText resignFirstResponder];
    btnBack.hidden=YES;
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)hideKb:(UIButton *)btn{
    [self resignResponder];
}
-(void)doneButtonshow: (NSNotification *)notification {
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
}
- (void)doneButtonHide:(NSNotification *)notification{
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
    }];
}
- (void)hideKeyboard:(UIButton *)btn{
    [btn.superview removeFromSuperview];
    [self resignFirstRespond];
}
- (void)resignFirstRespond{
    [self.diseaseNameField resignFirstResponder];
    [self.diseaseNumField resignFirstResponder];
    [self.mainText resignFirstResponder];
    [self.secodText resignFirstResponder];
}
#pragma -delegate
- (void)saveAction{
    if ([_diseaseNameField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"姓名必填~" places:0 toView:nil];
        return;
    }
    if ([_diseaseNumField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"病案号必填~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间必填~" places:0 toView:nil];
        return;
    }
    
    if (current_time > CurrentTimeNunm) {
        [MBProgressHUD showToastAndMessage:@"您选中时间大于当前时间，请重新选择" places:0 toView:nil];
        return;
    }
    if([self.mainText.text isEqualToString:@""]){
        [MBProgressHUD showToastAndMessage:@"主要诊断必填~" places:0 toView:nil];
        return;
    }
    
    if([self.secodText.text isEqualToString:@""]){
        [MBProgressHUD showToastAndMessage:@"次要诊断必填~" places:0 toView:nil];
        return;
    }
    
    NSLog(@"currentDoctor==%@",currentDoctor);

    if(has_image.intValue == 0){
        SdepartRecordController *departVC = [SdepartRecordController shareObject];
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentMedicalCaseRecords",SimpleIp];
        NSDictionary *params ;
        if(Doctor == 0){
            params= @{
                      //@"doctorId":currentDoctor,
                                     @"patientName":_diseaseNameField.text,
                                     @"caseNo":_diseaseNumField.text,
                                     @"caseTime":current_date,
                                     @"principalDiagnosis":_mainText.text,
                                     @"secondaryDiagnosis":_secodText.text,
                                     @"phaseScheduleMedicalCaseRequirementId":_model.CaseReuestModelRequirementId,
                                     @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        }else{
            params= @{@"doctorId":currentDoctor,
                      @"patientName":_diseaseNameField.text,
                      @"caseNo":_diseaseNumField.text,
                      @"caseTime":current_date,
                      @"principalDiagnosis":_mainText.text,
                      @"secondaryDiagnosis":_secodText.text,
                      @"phaseScheduleMedicalCaseRequirementId":_model.CaseReuestModelRequirementId,
                      @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        }
       
        NSLog(@"params=%@", params);
        [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"创建成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                [MBProgressHUD showToastAndMessage:@"创建失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"创建失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
        [RequestTools RequestWithFile:_imageView.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            //
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentMedicalCaseRecords",SimpleIp];
            NSDictionary *params;
            if (Doctor == 0) {
                params = @{@"fileId":fileId,@"patientName":_diseaseNameField.text,@"caseNo":_diseaseNumField.text,@"caseTime":current_date,@"principalDiagnosis":_mainText.text,@"secondaryDiagnosis":_secodText.text,@"phaseScheduleMedicalCaseRequirementId":_model.CaseReuestModelRequirementId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
            }else{
               params = @{@"doctorId":currentDoctor,@"fileId":fileId,@"patientName":_diseaseNameField.text,@"caseNo":_diseaseNumField.text,@"caseTime":current_date,@"principalDiagnosis":_mainText.text,@"secondaryDiagnosis":_secodText.text,@"phaseScheduleMedicalCaseRequirementId":_model.CaseReuestModelRequirementId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
            }
            
            
            NSLog(@"request=%@, params=%@",requestUrl, params);
            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"创建成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
                }else{
                    [MBProgressHUD showToastAndMessage:@"创建失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD hideHUDForView:nil];
                [MBProgressHUD showToastAndMessage:@"创建失败~" places:0 toView:nil];
            }];

        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }
}
- (void)updateAction{
    if ([_diseaseNameField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"姓名必填~" places:0 toView:nil];
        return;
    }
    if ([_diseaseNumField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"病案号必填~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间必填~" places:0 toView:nil];
        return;
    }
    if (current_time > CurrentTimeNunm) {
        [MBProgressHUD showToastAndMessage:@"您选中时间大于当前时间，请重新选择" places:0 toView:nil];
        return;
    }
    
    
    if([self.mainText.text isEqualToString:@""]){
        [MBProgressHUD showToastAndMessage:@"主要诊断必填~" places:0 toView:nil];
        return;
    }
    
    if([self.secodText.text isEqualToString:@""]){
        [MBProgressHUD showToastAndMessage:@"次要诊断必填~" places:0 toView:nil];
        return;
    }
    
    
    if (has_image.intValue == 0) {
        SdepartRecordController *departVC = [SdepartRecordController shareObject];
        
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentMedicalCaseRecords/%@",SimpleIp,self.myModel.recordId];
        NSDictionary *params;
        if (Doctor == 0) {
             params = @{@"patientName":_diseaseNameField.text,@"caseNo":_diseaseNumField.text,@"caseTime":current_date,@"principalDiagnosis":_mainText.text,@"secondaryDiagnosis":_secodText.text,@"phaseScheduleMedicalCaseRequirementId":self.myModel.requestmentId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        }else{
             params = @{@"doctorId":currentDoctor,@"patientName":_diseaseNameField.text,@"caseNo":_diseaseNumField.text,@"caseTime":current_date,@"principalDiagnosis":_mainText.text,@"secondaryDiagnosis":_secodText.text,@"phaseScheduleMedicalCaseRequirementId":self.myModel.requestmentId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        }
       
        
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        
        [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
            
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
                self.myModel = nil;
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
        }];

    }else{
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        [RequestTools RequestWithFile:_imageView.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            //
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentMedicalCaseRecords/%@",SimpleIp,self.myModel.recordId];
            NSDictionary *params;
            if (Doctor == 0) {
                params = @{@"fileId":fileId,@"patientName":_diseaseNameField.text,@"caseNo":_diseaseNumField.text,@"caseTime":current_date,@"principalDiagnosis":_mainText.text,@"secondaryDiagnosis":_secodText.text,@"phaseScheduleMedicalCaseRequirementId":self.myModel.requestmentId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
                
            }else{
                params = @{@"doctorId":currentDoctor,@"fileId":fileId,@"patientName":_diseaseNameField.text,@"caseNo":_diseaseNumField.text,@"caseTime":current_date,@"principalDiagnosis":_mainText.text,@"secondaryDiagnosis":_secodText.text,@"phaseScheduleMedicalCaseRequirementId":self.myModel.requestmentId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
                
            }
        
            [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
                
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
                    self.myModel = nil;
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
                }else{
                    [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
            }];

            
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];

    }
    
}
- (void)popLast{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}
- (IBAction)saveAction:(id)sender {
    if ([SubjectTool share].subjectType == subjectTypeAdd) {
        [self saveAction];
    }else{
        [self updateAction];
    }
}

- (IBAction)dateAction:(id)sender {
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];
}

- (IBAction)doctorAction:(id)sender {
    
    if (idArr.count != 0) {
        WXPPickerView *pickView = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:nameArr]];
        pickView.delegate = self;
        pickView.rightBtnTitle = @"确认";
        pickView.backgroundColor = [UIColor whiteColor];
        pickView.index = ^(int index){
            [sender setTitle:nameArr[index] forState:0];
            Doctor=1;
            currentDoctor = idArr[index];
            NSLog(@"DoctorName=%@",idArr[index]);
        };
        [self.view addSubview:pickView];
        [pickView show];
        self.pickView = pickView;
        return;
    }
    NSString *requestUrl = [NSString stringWithFormat:@"%@/users/teachers?pageSize=999&accountStatus=active",LocalIP];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"获取医师中" Success:^(NSDictionary *result) {
        NSArray *teacherArray = [[result objectForKey:@"responseBody"]objectForKey:@"result"];
        if (teacherArray.count == 0) {
            [MBProgressHUD showToastAndMessage:@"没有上级医师~" places:0 toView:nil];
            return ;
        }
        for (int i=0; i<teacherArray.count; i++) {
            NSDictionary *teacherModel = [teacherArray objectAtIndex:i];
            [idArr addObject:teacherModel[@"userId"]];
            [nameArr addObject:teacherModel[@"fullName"]];
        }
        //
        WXPPickerView *pickView = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:nameArr]];
        pickView.delegate = self;
        pickView.rightBtnTitle = @"确认";
        pickView.backgroundColor = [UIColor whiteColor];
        pickView.index = ^(int index){
            [sender setTitle:nameArr[index] forState:0];
            currentDoctor = idArr[index];
            [self.doctorBtn setTitle:nameArr[index] forState:UIControlStateNormal];
            Doctor=1;
            NSLog(@"DoctorStr==%@",currentDoctor);
        };
        [self.view addSubview:pickView];
        [pickView show];
        self.pickView = pickView;
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"获取失败,请重新进入!" places:0 toView:nil];
        [NSTimer scheduledTimerWithTimeInterval:1 repeats:NO block:^(NSTimer * _Nonnull timer) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }];}
- (IBAction)fileAction:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相册",@"拍照", nil];
    [sheet showInView:self.view];
}
- (void)PickerViewRightButtonOncleck:(NSInteger)index{
    [self.pickView close];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self getAlbumAction];
            break;
        case 1:
            [self photoAction];
            break;
        default:
            break;
    }
}
- (void)photoAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePickController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)getAlbumAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    [picker dismissViewControllerAnimated:YES completion:nil];
    has_image = @1;
    _imageView.image = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
}
@end
