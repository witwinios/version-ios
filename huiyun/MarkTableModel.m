//
//  MarkTableModel.m
//  yun
//
//  Created by MacAir on 2017/7/24.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "MarkTableModel.h"

@implementation MarkTableModel
- (void)setMarkTableDescription:(NSString *)MarkTableDescription{
    if ([MarkTableDescription isKindOfClass:[NSNull class]]|| [MarkTableDescription isEqualToString:@""]) {
        _MarkTableDescription = @"暂无";
    }else{
        _MarkTableDescription = MarkTableDescription;
    }
}
- (void)setMarkTableKey:(NSString *)MarkTableKey{
    if ([MarkTableKey isKindOfClass:[NSNull class]] || [MarkTableKey isEqualToString:@""]) {
        _MarkTableKey = @"暂无";
    }else{
        _MarkTableKey = MarkTableKey;
    }
}
- (void)setMarkTableKeyType:(NSString *)MarkTableKeyType{
    if ([MarkTableKeyType isKindOfClass:[NSNull class]] ||[MarkTableKeyType isEqualToString:@""]) {
        _MarkTableKeyType = @"暂无";
    }else{
        _MarkTableKeyType = MarkTableKeyType;
    }
}
- (void)setMarkTableAnswer:(NSString *)MarkTableAnswer{
    if ([MarkTableAnswer isKindOfClass:[NSNull class]] || [MarkTableAnswer isEqualToString:@""]) {
        _MarkTableAnswer = @"暂无";
    }else{
        _MarkTableAnswer = MarkTableAnswer;
    }
}

@end
