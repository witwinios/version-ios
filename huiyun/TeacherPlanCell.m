//
//  TeacherPlanCell.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/30.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TeacherPlanCell.h"

@implementation TeacherPlanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    self.backgroundColor=[[UIColor darkGrayColor]colorWithAlphaComponent:0.3];
    
    self.PlanStautebtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.PlanStautebtn.layer.borderWidth=1;
    self.PlanStautebtn.layer.cornerRadius=10;
    
    self.PlanName.numberOfLines = 0;
    [self.PlanName sizeToFit];
    
}


-(void)setProperty:(TCourseAddModel *)model{
    
    
     self.PlanName.text=model.PlanName;
    
     self.PlanClassify.text=model.PlanClassify;
    
     self.PlanCourse.text=model.PlanCourse;
    
     self.PlanOwner.text=model.PlanOwner;
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
