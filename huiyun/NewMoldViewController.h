//
//  NewMoldViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/27.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoldCell.h"
#import "CourseMoldModel.h"

@class NewMoldViewController;


@protocol NewMoldDelegate<NSObject>

@optional

-(void)Moldtext:(NSString *)moldstr MoldID:(NSString *)moldID;

@end


@interface NewMoldViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)NSMutableArray *DataArray;
@property(nonatomic,assign) id<NewMoldDelegate>delegate;

@end
