//
//  OnlinePagesModel.m
//  huiyun
//
//  Created by MacAir on 2018/2/4.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "OnlinePagesModel.h"

@implementation OnlinePagesModel
//@property (strong, nonatomic) NSString *pageName;
- (void)setPageName:(NSString *)pageName{
    if ([pageName isKindOfClass:[NSNull class]]) {
        _pageName = @"暂无";
    }else{
        _pageName = pageName;
    }
}
//@property (strong, nonatomic) NSString *pageCategory;
- (void)setPageCategory:(NSString *)pageCategory{
    if ([pageCategory isKindOfClass:[NSNull class]]) {
        _pageCategory = @"暂无";
    }else{
        _pageCategory = pageCategory;
    }
}
//@property (strong, nonatomic) NSString *pageSubject;
- (void)setPageSubject:(NSString *)pageSubject{
    if ([pageSubject isKindOfClass:[NSNull class]]) {
        _pageSubject = @"暂无";
    }else{
        _pageSubject = pageSubject;
    }
}

//@property (strong, nonatomic) NSNumber *pageScore;
- (void)setPageScore:(NSNumber *)pageScore{
    if ([pageScore isKindOfClass:[NSNull class]]) {
        _pageScore = @0;
    }else{
        _pageScore = pageScore;
    }
}
@end
