//
//  CaseActivityModel.h
//  huiyun
//
//  Created by MacAir on 2017/10/16.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CaseActivityModel : NSObject
@property (strong, nonatomic) NSNumber *recordId;
@property (strong, nonatomic) NSNumber *requestmentId;
@property (strong, nonatomic) NSString *caseActivityName;
@property (strong, nonatomic) NSString *caseStatus;
@property (strong, nonatomic) NSString *casePersion;
@property (strong, nonatomic) NSNumber *caseInternal;
@property (strong, nonatomic) NSNumber *caseDate;
@property (strong, nonatomic) NSString *caseTeacher;
@property (strong, nonatomic) NSString *caseActivityContent;
@property (strong, nonatomic) NSString *caseAdvice;
@end
