//
//  LiveRoomViewController.m
//  iOSZhongYe
//
//  Created by 郑敏捷 on 2017/5/19.
//  Copyright © 2017年 郑敏捷. All rights reserved.
//

#import "LiveRoomViewController.h"

#import "MenuView2.h"
#import "LiveView.h"

#import "LiveModel.h"

#import "UIView+Zmj.h"

#define WeakObj(o) autoreleasepool{} __weak typeof(o) o##Weak = o;

#define StrongObj(o) autoreleasepool{} __strong typeof(o) o = o##Weak;

#define ScreenWidthRatio ScreenWidth / 320.0

#define MenuHeight  35 * ScreenWidthRatio

@interface LiveRoomViewController ()<liveViewDelegate, UIScrollViewDelegate, GSPDocViewDelegate>

@property (strong, nonatomic) UIScrollView *scrollView;

@property (strong, nonatomic) UIView     *statusView;

@property (strong, nonatomic) UIView     *fatherView;

@property (strong, nonatomic) UIView     *backView;

@property (strong, nonatomic) MenuView2   *menuView2;

@property (strong, nonatomic) LiveView   *liveView;

@property (strong, nonatomic) LiveModel  *liveModel;

@property (strong, nonatomic) NSArray    *titleArray;

@property (strong, nonatomic) GSPChatView *chatView;

@property (strong, nonatomic) GSPQaView   *qaView;

@property (strong, nonatomic) GSPDocView  *docView;

@property (strong, nonatomic) GSPInvestigationView *investigationView;

@property (strong, nonatomic) GSPChatInputToolView *chatInputToolView;

@property (strong, nonatomic) GSPChatInputToolView *qaInputToolView;

@property (assign, nonatomic) CGFloat    chatInputToolViewX;

@property (assign, nonatomic) CGFloat    qaInputToolViewX;

@end

@implementation LiveRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initData];
    
    [self initView];
    
    [self makeSubViewsConstraints];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    AppDelegate *appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate1.playerManager activateMicrophone:NO];
    [appDelegate1.playerManager leave];
}

// 返回值要必须为NO
- (BOOL)shouldAutorotate {
    
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    
    return ZFPlayerShared.isStatusBarHidden;
}

- (void)backBtnAction {

    if (_investigationView.hasForceInvestigation) {
        
        [self alertViewControllerTitleStr:NSLocalizedString(@"强制投票","") alertStr:NSLocalizedString(@"不能返回，请投票",@"") isCancel:NO];
        
    } else {
    
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)initView {
    
    [self.view addSubview:self.statusView];
    [self.view addSubview:self.fatherView];
    
    if (self.liveView) {}
    
    [self.view addSubview:self.menuView2];
    
    _menuView2.titleArray = _titleArray;
    
    [self.view addSubview:self.scrollView];
    
    [_scrollView addSubview:self.backView];
    [_backView addSubview:self.chatView];
    [_backView addSubview:self.qaView];
    [_backView addSubview:self.docView];
    [_backView addSubview:self.investigationView];

    [self.view addSubview:self.chatInputToolView];
    [self.view addSubview:self.qaInputToolView];

    [self.view bringSubviewToFront:_fatherView];
    [self.view bringSubviewToFront:_chatInputToolView];
    [self.view bringSubviewToFront:_qaInputToolView];
    
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.playerManager.chatView = _chatView;
    appDelegate.playerManager.qaView   = _qaView;
    appDelegate.playerManager.docView  = _docView;
    appDelegate.playerManager.investigationView = _investigationView;
    
    _chatInputToolViewX = _chatInputToolView.x;
    _qaInputToolViewX   = _qaInputToolView.x;
}

- (void)initData {

    _titleArray = @[@"聊天", @"问答", @"文档", @"投票"];
}

- (void)makeSubViewsConstraints {

//    [self.fatherView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.view.mas_top);
//        make.leading.trailing.equalTo(self.view);
//        // 这里宽高比16：9,可自定义宽高比
//        make.height.mas_equalTo(self.fatherView.mas_width).multipliedBy(9.0f/16.0f);
//    }];
    
    [self.menuView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.fatherView.mas_bottom);
        make.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(MenuHeight);
    }];
    
    [self.investigationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.trailing.bottom.equalTo(self.backView);
        make.width.mas_equalTo(ScreenWidth);
    }];
}

- (UIView *)statusView {
    if (!_statusView) {
        _statusView = [[UIView alloc]init];
        _statusView.frame = CGRectMake(0, 0, ScreenWidth, 20);
        _statusView.backgroundColor = [UIColor blackColor];
    }
    return _statusView;
}

- (UIView *)fatherView {
    if (!_fatherView) {
        _fatherView = [[UIView alloc]init];
        _fatherView.frame = CGRectMake(0, 20, ScreenWidth, ScreenWidth / 16 * 10);
    }
    return _fatherView;
}

- (LiveView *)liveView {
    if (!_liveView) {
        _liveView = [[LiveView alloc]init];
        _liveView.delegate = self;
        
        _liveView.liveModel = self.liveModel;
    }
    return _liveView;
}

- (LiveModel *)liveModel {
    if (!_liveModel) {
        _liveModel = [[LiveModel alloc]init];
        _liveModel.domain        = _domain;
        _liveModel.webcastID     = _webcastID;
        _liveModel.UserName      = _UserName;
        _liveModel.ServiceType   = _ServiceType;
        _liveModel.watchPassword = _watchPassword;
        _liveModel.LiveClassName = _LiveClassName;
        _liveModel.fatherView    = self.fatherView;
    }
    return _liveModel;
}

- (MenuView2 *)menuView2 {
    if (!_menuView2) {
        _menuView2 = [[MenuView2 alloc]init];
        
        @WeakObj(self);
        _menuView2.vcBtnBlock = ^(NSInteger tag) {
            @StrongObj(self);
            [self changeVC:tag - 1];
        };
    }
    return _menuView2;
}

- (void)changeVC:(NSInteger)page {

    [_scrollView setContentOffset:CGPointMake(ScreenWidth * page, 0) animated:YES];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.frame = CGRectMake(0, _fatherView.y + _fatherView.height + MenuHeight, ScreenWidth, ScreenHeight - _fatherView.y - _fatherView.height - MenuHeight);
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(ScreenWidth * _titleArray.count, ScreenHeight - _fatherView.y - _fatherView.height - MenuHeight);
        _scrollView.bounces = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if ([_scrollView isEqual:scrollView]) {
        
        CGFloat x = scrollView.contentOffset.x / _titleArray.count;
        
        _menuView2.lineView.x = x;
        
        NSInteger tag = scrollView.contentOffset.x / ScreenWidth + 0.5;
        
        UIButton *button  = (UIButton *)[_menuView2 viewWithTag:tag + 1];
        button.selected = YES;
        
        NSArray *subArray = _menuView2.subviews;
        
        for (UIButton *subbutton  in subArray) {
            
            if ([subbutton isKindOfClass:[UIButton class]]) {
                
                if (subbutton != button) {
                    
                    subbutton.selected = NO;
                }
            }
        }
        
        switch (tag + 1) {
                
            case 1: {
                
                _chatInputToolView.hidden = NO;
                _qaInputToolView.hidden = YES;
            }
                break;
                
            case 2: {
                
                _chatInputToolView.hidden = YES;
                _qaInputToolView.hidden = NO;
            }
                break;
                
            default: {
            
                _chatInputToolView.hidden = YES;
                _qaInputToolView.hidden = YES;
            }
                break;
        }
    }
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc]init];
        _backView.frame = CGRectMake(0, 0, ScreenWidth * _titleArray.count, _scrollView.height);
    }
    return _backView;
}

- (GSPChatView *)chatView {
    if (!_chatView) {
        _chatView = [[GSPChatView alloc]init];
        _chatView.frame = CGRectMake(0, 0, ScreenWidth, _scrollView.height - 52);
    }
    return _chatView;
}

- (GSPQaView *)qaView {
    if (!_qaView) {
        _qaView = [[GSPQaView alloc]init];
        _qaView.frame = CGRectMake(ScreenWidth, 0, ScreenWidth, _scrollView.height - 52);
    }
    return _qaView;
}

- (GSPChatInputToolView *)chatInputToolView {
    if (!_chatInputToolView) {
        _chatInputToolView = [[GSPChatInputToolView alloc]initWithViewController:self combinedChatView:_chatView combinedQaView:nil isChatMode:YES];
    }
    return _chatInputToolView;
}

- (GSPChatInputToolView *)qaInputToolView {
    if (!_qaInputToolView) {
        _qaInputToolView = [[GSPChatInputToolView alloc]initWithViewController:self combinedChatView:nil combinedQaView:_qaView isChatMode:NO];
        _qaInputToolView.hidden = YES;
    }
    return _qaInputToolView;
}

- (GSPDocView *)docView {
    if (!_docView) {
        _docView = [[GSPDocView alloc]init];
        _docView.frame = CGRectMake(ScreenWidth * 2, 0, ScreenWidth, _scrollView.height);
        _docView.gSDocModeType = ScaleAspectFit;
        _docView.pdocDelegate = self;
    }
    return _docView;
}

- (GSPInvestigationView *)investigationView {
    if (!_investigationView) {
        _investigationView = [[GSPInvestigationView alloc]init];
        _investigationView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _investigationView;
}

- (void)joinRoomResult:(NSString *)resultStr isCancel:(BOOL)bl {

    [self alertViewControllerTitleStr:nil alertStr:resultStr isCancel:bl];
}

- (void)alertViewControllerTitleStr:(NSString *)titleStr alertStr:(NSString *)alertStr isCancel:(BOOL)bl {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:titleStr message:alertStr preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        _liveView.isAlert = NO;
        
        if (bl) {
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)resign {

    [self.view endEditing:YES];
}

- (void)dealloc {
    _liveView = nil;
}

@end
