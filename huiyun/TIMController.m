//
//  QuestionController.m
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "QuestionController.h"

@interface TIMController ()
{
    UITableView *tabView;
    NSMutableArray *dataArray;
     YWConversationViewController *selfConversation;
    NSString *currentDes;
    NSArray *currentTribeMem;
    
    int pageNumber;
}
@end

@implementation TIMController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}
- (YWIMCore *)ywIMCore {
    return [SPKitExample sharedInstance].ywIMKit.IMCore;
}
- (id<IYWTribeService>)ywTribeService {
    return [[self ywIMCore] getTribeService];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    pageNumber = 1;
    [self setUI];
}

- (void)exampleOpenConversationViewControllerWithTribe:(YWTribe *)aTribe fromNavigationController:(UINavigationController *)aNavigationController
{
    YWConversation *conversation = [YWTribeConversation fetchConversationByTribe:aTribe createIfNotExist:YES baseContext:self.ywIMKit.IMCore];
    [self exampleOpenConversationViewControllerWithConversation:conversation fromNavigationController:aNavigationController];
}
- (void)exampleOpenConversationViewControllerWithConversation:(YWConversation *)aConversation fromNavigationController:(UINavigationController *)aNavigationController
{
    __block YWConversationViewController *alreadyController = nil;
    [aNavigationController.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[YWConversationViewController class]]) {
            YWConversationViewController *c = obj;
            if (aConversation.conversationId && [c.conversation.conversationId isEqualToString:aConversation.conversationId]) {
                alreadyController = c;
                *stop = YES;
            }
        }
    }];
    
    if (alreadyController) {
        /// 必须判断当前是否已有该会话，如果有，则直接显示已有会话
        /// @note 目前IMSDK不允许同时存在两个相同会话的Controller
        [aNavigationController popToViewController:alreadyController animated:YES];
        [aNavigationController setNavigationBarHidden:NO];
        return;
    } else {
        YWConversationViewController *conversationController = [self.ywIMKit makeConversationViewControllerWithConversationId:aConversation.conversationId];
        selfConversation = conversationController;
        __weak typeof(conversationController) weakController = conversationController;
        [conversationController setViewWillAppearBlock:^(BOOL aAnimated) {
            //            [weakController.navigationController setNavigationBarHidden:YES animated:aAnimated];
            weakController.view.backgroundColor = [UIColor whiteColor];
            //添加导航栏
            UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
            backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
            backNavigation.userInteractionEnabled = YES;
            [self.view addSubview:backNavigation];
            self.view.backgroundColor = [UIColor whiteColor];
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            leftBtn.frame = CGRectMake(10, 24.5, 20, 30);
            [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
            [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
            [backNavigation addSubview:leftBtn];
            UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
            [rightBtn setImage:[UIImage imageNamed:@"user"] forState:UIControlStateNormal];
            [rightBtn addTarget:self action:@selector(rightItemAction:) forControlEvents:UIControlEventTouchUpInside];
            [backNavigation addSubview:rightBtn];
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
            titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
            titleLab.textAlignment = 1;
            titleLab.font = [UIFont systemFontOfSize:20];
            titleLab.textColor = [UIColor whiteColor];
            titleLab.text = @"讨论组";
            [backNavigation addSubview:titleLab];
            [weakController.view addSubview:backNavigation];
            //添加textView
            UITextView *quesView = [UITextView new];
            quesView.frame = CGRectMake(0, 64, Swidth, 60);
            quesView.text = [NSString stringWithFormat:@"问题描述:%@",currentDes];
            quesView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            quesView.layer.borderWidth = 1;
            quesView.editable = NO;
            [weakController.view addSubview:quesView];
            //
            weakController.tableView.frame = CGRectMake(0, 124, Swidth, Sheight-124);
        }];
        
        [aNavigationController pushViewController:conversationController animated:YES];
        
    }
}
- (void)setUI{
    self.ywIMKit = [[YWAPI sharedInstance] fetchIMKitForOpenIM];
    self.tribe = [[YWTribe alloc]init];
    //
    dataArray = [NSMutableArray new];
    
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];

    //
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 29.5, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, 44);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"我的群组";
    [backNavigation addSubview:titleLab];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStyleGrouped];
    tabView.delegate = self;
    tabView.dataSource = self;
    tabView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    tabView.layer.borderWidth = 2;
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"QuestionCell" bundle:nil] forCellReuseIdentifier:@"quesCell"];
    [tabView addHeaderWithTarget:self action:@selector(downRefresh)];
    [tabView addFooterWithTarget:self action:@selector(moreRefresh)];
    //设置文字
    tabView.headerPullToRefreshText = @"下拉刷新";
    tabView.headerReleaseToRefreshText = @"松开进行刷新";
    tabView.headerRefreshingText = @"刷新中。。。";
    
    
    [self.view addSubview:tabView];
    //
    [tabView headerBeginRefreshing];
}
//上拉加载
- (void)moreRefresh{
    pageNumber ++;
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@/QAGroup?memberId=%@&pageStart=%d&pageSize=15",LocalIP,LocalUserId,pageNumber];
    
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        //组装数据
        NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
        if (array.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂无更多!" places:0 toView:nil];
            return ;
        }
        NSMutableArray *newData = [NSMutableArray new];
        for (int i=0; i<array.count; i++) {
            NSDictionary *responseDic = array[i];
            //
            MyQuesModel *quesModel = [MyQuesModel new];
            quesModel.qaGroupId = [responseDic objectForKey:@"qaGroupId"];
            NSLog(@"------%@", [responseDic objectForKey:@"qaGroupId"]);
            quesModel.quesTitle = [responseDic objectForKey:@"questionTitle"];
            quesModel.quesDes  = [responseDic objectForKey:@"questionDescription"];
            quesModel.tribeId = [responseDic objectForKey:@"tribeId"];
            quesModel.status = [responseDic objectForKey:@"status"];
            //
            NSArray *memArray = [responseDic objectForKey:@"tribeMembers"];
            NSMutableArray *memArrays = [NSMutableArray new];
            for (int s=0; s<memArray.count; s++) {
                NSDictionary *memDic = memArray[s];
                TribeMem *tribeMem = [TribeMem new];
                tribeMem.userId = [memDic objectForKey:@"userId"];
                tribeMem.fullName = [memDic objectForKey:@"fullName"];
                tribeMem.email = [memDic objectForKey:@"email"];
                tribeMem.phone = [memDic objectForKey:@"personalPhoneNo"];
                tribeMem.school = [memDic objectForKey:@"schoolName"];
                [memArrays addObject:tribeMem];
            }
            quesModel.memberArray = [NSArray arrayWithArray:memArrays];
            [newData addObject:quesModel];
        }
        NSRange range = NSMakeRange(dataArray.count, newData.count);
        NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
        [dataArray insertObjects:newData atIndexes:set];
        [tabView reloadData];
    } failed:^(NSString *result) {
        pageNumber --;
        [tabView footerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"加载错误,请重试!" places:0 toView:nil];
    }];
}
//下拉刷新
- (void)downRefresh{
    pageNumber = 1;
    
    [RequestTools RequestWithURL:[NSString stringWithFormat:@"%@/QAGroup?memberId=%@&pageStart=%d&pageSize=15",LocalIP,LocalUserId,pageNumber] Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [dataArray removeAllObjects];
        [tabView headerEndRefreshing];
        //组装数据
        NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
        if (array.count == 0) {
            [MBProgressHUD showToastAndMessage:@"暂无群组!" places:0 toView:nil];
            return ;
        }
        for (int i=0; i<array.count; i++) {
            NSDictionary *responseDic = array[i];
            //
            MyQuesModel *quesModel = [MyQuesModel new];
            quesModel.qaGroupId = [responseDic objectForKey:@"qaGroupId"];
            NSLog(@"------%@", [responseDic objectForKey:@"qaGroupId"]);
            quesModel.quesTitle = [responseDic objectForKey:@"questionTitle"];
            quesModel.quesDes  = [responseDic objectForKey:@"questionDescription"];
            quesModel.tribeId = [responseDic objectForKey:@"tribeId"];
            quesModel.status = [responseDic objectForKey:@"status"];
            
            //
            NSArray *memArray = [responseDic objectForKey:@"tribeMembers"];
            NSMutableArray *memArrays = [NSMutableArray new];
            for (int s=0; s<memArray.count; s++) {
                NSDictionary *memDic = memArray[s];
                TribeMem *tribeMem = [TribeMem new];
                tribeMem.userId = [memDic objectForKey:@"userId"];
                tribeMem.fullName = [memDic objectForKey:@"fullName"];
                tribeMem.email = [memDic objectForKey:@"email"];
                tribeMem.phone = [memDic objectForKey:@"personalPhoneNo"];
                tribeMem.school = [memDic objectForKey:@"schoolName"];
                [memArrays addObject:tribeMem];
            }
            quesModel.memberArray = [NSArray arrayWithArray:memArrays];
            //
            if ([quesModel.status isEqualToString:@"未解决"]) {
                [dataArray addObject:quesModel];
            }
        }
        [tabView reloadData];

    }failed:^(NSString *result) {
        [tabView headerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"加错错误,请重试!" places:0 toView:nil];
    }];
}

#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)rightItemAction:(UIButton *)btn{
    TeacherController *teacherVC = [TeacherController new];
    teacherVC.tribeArray = currentTribeMem;
    [self.navigationController pushViewController:teacherVC animated:YES];
}
#pragma -delegate dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dataArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 117.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MyQuesModel *myModel= dataArray[indexPath.section];
    //打开聊天
    //传值
    currentDes = myModel.quesDes;
    currentTribeMem = myModel.memberArray;
    self.tribe.tribeId = [NSString stringWithFormat:@"%@",myModel.tribeId];
    NSLog(@"id====%@",self.tribe.tribeId);
    self.tribe.tribeName = @"老师群聊";
    
//
    [self exampleOpenConversationViewControllerWithTribe:self.tribe fromNavigationController:self.navigationController];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QuestionCell *cell = [tabView dequeueReusableCellWithIdentifier:@"quesCell"];
    [cell setModel:dataArray[indexPath.section] andCurrentVC:self];
    return cell;
}

@end
