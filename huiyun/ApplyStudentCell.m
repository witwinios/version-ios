//
//  ApplyStudentCell.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/5.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "ApplyStudentCell.h"

@implementation ApplyStudentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
     self.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.3];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)setProperty:(ApplyStudent *)model{
    
NSString *url=[NSString stringWithFormat:@"%@?CTTS-Token=%@",model.ApplyStudentImg,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
    
    if (model.isCheck) {
        self.ApplyBgVc.backgroundColor = UIColorFromHex(0x20B2AA);
    } else {
        self.ApplyBgVc.backgroundColor = [UIColor whiteColor];
    }
    
    
    [self.ApplyStudentImg sd_setImageWithURL:[NSURL URLWithString:url]];
    [self.ApplyStudentImg sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"tab"]];
    self.ApplyStudentName.text=model.ApplyStudentName;
    
    
}


-(void)setCourseAffix:(CourseAffixModel *)model{
    
    
    if ([model.FileFormat isEqualToString:@"png"] || [model.FileFormat isEqualToString:@"jpg"]) {
        NSString *url=[NSString stringWithFormat:@"%@?CTTS-Token=%@",model.FileUrl,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
        [self.ApplyStudentImg sd_setImageWithURL:[NSURL URLWithString:url]];
        [self.ApplyStudentImg sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"tab"]];
    }else if([model.FileFormat isEqualToString:@"doc"] || [model.FileFormat isEqualToString:@"docx"]){
        self.ApplyStudentImg.image=[UIImage imageNamed:@"DOC"];
    }else if([model.FileFormat isEqualToString:@"pptx"]){
        self.ApplyStudentImg.image=[UIImage imageNamed:@"PPT"];
    }else if([model.FileFormat isEqualToString:@"xls"]){
        self.ApplyStudentImg.image=[UIImage imageNamed:@"XLS"];
    }else if([model.FileFormat isEqualToString:@"html"]){
        self.ApplyStudentImg.image=[UIImage imageNamed:@"HTML"];
    }else if([model.FileFormat isEqualToString:@"mp4"]){
         self.ApplyStudentImg.image=[UIImage imageNamed:@"MP4"];
    }else{
         self.ApplyStudentImg.image=[UIImage imageNamed:@"logo"];
    }
    
    self.ApplyStudentName.text=model.FileName;
    
    
    
    
}



@end
