//
//  EvaluateViewController.m
//  huiyun
//
//  Created by Bad on 2018/3/14.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "EvaluateViewController.h"

@interface EvaluateViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    
    UITableView *tableview;
    
    UIView *leftLine;
    UIButton *leftS;
    
    UIView *rightLine;
    UIButton *rightS;
    
    UIView *centerLine;
    UIButton *centerS;
}

@end

@implementation EvaluateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNav];
    
    [self setTabBar];
    
    [self setUI];
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"全方位评价";
    
    
    [backNavigation addSubview:titleLab];
    


}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setTabBar{
    UIView *TabBarVc=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 40)];
    TabBarVc.backgroundColor=[[UIColor whiteColor]colorWithAlphaComponent:0.5];
    leftS=[UIButton buttonWithType:UIButtonTypeCustom];
    leftS.frame=CGRectMake(0, 0, Swidth/3, 39);
    [leftS setTitle:@"评价学生" forState:UIControlStateNormal];
  
    [leftS setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    
    [leftS addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    leftLine=[[UIView alloc]initWithFrame:CGRectMake(0, 39, Swidth/3, 1)];
    leftLine.backgroundColor=UIColorFromHex(0x20B2AA);
    [TabBarVc addSubview:leftLine];
    [TabBarVc addSubview:leftS];
    
    rightS=[UIButton buttonWithType:UIButtonTypeCustom];
    rightS.frame=CGRectMake(2*Swidth/3, 0, Swidth/3, 39);
    [rightS setTitle:@"评价带教" forState:UIControlStateNormal];
    [rightS setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   
    [rightS addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    rightLine=[[UIView alloc]initWithFrame:CGRectMake(2*Swidth/3, 39, Swidth/3, 1)];
    
    [TabBarVc addSubview:rightLine];
    [TabBarVc addSubview:rightS];
    
    centerS=[UIButton buttonWithType:UIButtonTypeCustom];
    centerS.frame=CGRectMake(Swidth/3, 0, Swidth/3, 39);
    [centerS setTitle:@"评价科室" forState:UIControlStateNormal];
    [centerS setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   
    [centerS addTarget:self action:@selector(centerAction) forControlEvents:UIControlEventTouchUpInside];
    centerLine=[[UIView alloc]initWithFrame:CGRectMake(Swidth/3, 39, Swidth/3, 1)];
    
    [TabBarVc addSubview:centerLine];
    [TabBarVc addSubview:centerS];
    [self.view addSubview:TabBarVc];
}

-(void)leftAction{
    leftLine.backgroundColor=UIColorFromHex(0x20B2AA);
    rightLine.backgroundColor=[UIColor whiteColor];
    centerLine.backgroundColor=[UIColor whiteColor];
    [leftS setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [rightS setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [centerS setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

-(void)rightAction{
    leftLine.backgroundColor=[UIColor whiteColor];
    rightLine.backgroundColor=UIColorFromHex(0x20B2AA);
    centerLine.backgroundColor=[UIColor whiteColor];
    [rightS setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [leftS setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [centerS setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

}
-(void)centerAction{
    leftLine.backgroundColor=[UIColor whiteColor];
    rightLine.backgroundColor=[UIColor whiteColor];
    centerLine.backgroundColor=UIColorFromHex(0x20B2AA);
    [centerS setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [leftS setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightS setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

-(void)setUI{
//    tableview=[[UITableView alloc]initWithFrame:CGRectMake(0, 104, Swidth, Sheight-104) style:UITableViewStylePlain];
//    tableview.delegate=self;
//    tableview.dataSource=self;
//    
//    
//    [self.view addSubview:tableview];
    
    [MBProgressHUD showToastAndMessage:@"暂无数据" places:0 toView:0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
