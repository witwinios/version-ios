//
//  trainingAidsViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "trainingAidsViewController.h"

@interface trainingAidsViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *courseTable;
    NSMutableArray *dataArray;
    int currentPage;
    
    NSInteger line;
    NSMutableArray *OneArr;
}

@end

@implementation trainingAidsViewController

-(void)viewWillAppear:(BOOL)animated{
    [self setUpRefresh];
}


- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setUI];
    [self setUpRefresh];
    [self setNav];
    
    
}
// NSLog(@"%@/courses/%@/requiredTrainingAids",LocalIP,_PlanModel.PlanNameId);
//http://www.hzwitwin.cn:81/witwin-ctts-web/courses/529/requiredTrainingAids

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"教具选取";
    
    
    [backNavigation addSubview:titleLab];
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (_PlanModel.PlanOwnerId.intValue == persion.userID.intValue ) {
     if([_PlanModel.PlanCourseStatus isEqualToString:@"in_design"]){
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(Choice:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;
     }
    }
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)Choice:(id)sender{
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"教具管理" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *AddAction = [UIAlertAction actionWithTitle:@"新增" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        TrainingAidsAddViewController *vc=[[TrainingAidsAddViewController alloc]init];
        vc.PlanModel=_PlanModel;
        vc.TempArr=OneArr;
        vc.backArr=^(NSArray *addArr){
            dataArray = [NSMutableArray arrayWithArray:addArr];
            [courseTable reloadData];
        };
         [self.navigationController pushViewController:vc animated:YES];
        
    }];
    UIAlertAction *EditAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [courseTable setEditing:YES animated:YES];
        rightBtn.hidden=YES;
        SaveBtn.hidden=NO;
        
    }];
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
    [AlertView addAction:BackAction];
    [AlertView addAction:AddAction];
    [AlertView addAction:EditAction];
    
    [self presentViewController:AlertView animated:YES completion:nil];
}
-(void)save{
    [courseTable setEditing:NO animated:YES];
    rightBtn.hidden=NO;
    SaveBtn.hidden=YES;
}
-(void)setUI{
    currentPage = 1;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    
    [courseTable registerNib:[UINib nibWithNibName:@"TeachingAidsCell" bundle:nil] forCellReuseIdentifier:@"AidsCell"];
    [self.view addSubview:courseTable];
    
    dataArray=[NSMutableArray new];
    OneArr=[NSMutableArray new];
    
}
-(void)setUpRefresh{
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
//    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
//    courseTable.footerPullToRefreshText = @"上拉加载";
//    courseTable.footerReleaseToRefreshText = @"松开进行加载";
//    courseTable.footerRefreshingText = @"加载中。。。";
}
// NSLog(@"%@/courses/%@/requiredTrainingAids",LocalIP,_PlanModel.PlanNameId);
//http://www.hzwitwin.cn:81/witwin-ctts-web/courses/529/requiredTrainingAids
-(void)loadData{
    currentPage=1;
    NSString *PlanId=[NSString stringWithFormat:@"%@",_PlanModel.PlanNameId];
   
    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/requiredTrainingAids?pageStart=%d&pageSize=999",LocalIP,PlanId,currentPage];
    

    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                
                [Maneger showAlert:@"当前无教具信息!" andCurentVC:self];
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    TeachingAidsModel *model=[TeachingAidsModel new];
                    model.ModelId=[dictionary objectForKey:@"modelId"];
                    model.ModelName=[dictionary objectForKey:@"modelName"];
                    model.ModelCategoryName=[dictionary objectForKey:@"categoryName"];
                    model.ModelNo=[dictionary objectForKey:@"modelNo"];
                    model.ModelItemsNum=[dictionary objectForKey:@"itemsNum"];
                    model.ModelAmount=[dictionary objectForKey:@"amount"];
                    model.ModelComments=[dictionary objectForKey:@"comments"];
                    model.ModeluseFor=[dictionary objectForKey:@"useFor"];
                    
                    [dataArray addObject:model];
                    
                    NSMutableDictionary *Params=[NSMutableDictionary dictionary];
                    [Params setObject:_PlanModel.PlanNameId forKey:@"courseId"];
                    [Params setObject:model.ModelId forKey:@"modelId"];
                    [Params setObject:model.ModelAmount forKey:@"amount"];
                    [Params setObject:model.ModelComments forKey:@"comments"];
                    [Params setObject:model.ModeluseFor forKey:@"useFor"];
                    [OneArr addObject:Params];
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
                
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
        
        
        
        
    } failed:^(NSString *result) {
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
    
    
}

-(void)loadRefresh{
    currentPage++;
    NSString *PlanId=[NSString stringWithFormat:@"%@",_PlanModel.PlanNameId];
    
    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/requiredTrainingAids?pageStart=%d&pageSize=10",LocalIP,PlanId,currentPage];
    
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                
                [Maneger showAlert:@"当前无教具信息!" andCurentVC:self];
                
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    TeachingAidsModel *model=[TeachingAidsModel new];
                    model.ModelId=[dictionary objectForKey:@"modelId"];
                    model.ModelName=[dictionary objectForKey:@"modelName"];
                    model.ModelCategoryName=[dictionary objectForKey:@"categoryName"];
                    model.ModelNo=[dictionary objectForKey:@"modelNo"];
                    model.ModelItemsNum=[dictionary objectForKey:@"itemsNum"];
                    model.ModelAmount=[dictionary objectForKey:@"amount"];
                    model.ModelComments=[dictionary objectForKey:@"comments"];
                    model.ModeluseFor=[dictionary objectForKey:@"useFor"];
                    
                    
                    [newData addObject:model];
                    
                    NSMutableDictionary *Params=[NSMutableDictionary dictionary];
                    [Params setObject:_PlanModel.PlanNameId forKey:@"courseId"];
                    [Params setObject:model.ModelId forKey:@"modelId"];
                    [Params setObject:model.ModelAmount forKey:@"amount"];
                    [Params setObject:model.ModelComments forKey:@"comments"];
                    [Params setObject:model.ModeluseFor forKey:@"useFor"];
                    [OneArr addObject:Params];
                    
                }
                NSLog(@"newData=%lu",(unsigned long)newData.count);
                
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                
                
                [courseTable reloadData];
                
                
            }
        }
        else{
            currentPage--;
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
}


#pragma mark---TableView
//行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

//点击Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (!_editing) {
        TeachingAidsModel *model=dataArray[indexPath.row];
        TrainingAidsDetailsViewController *vc=[[TrainingAidsDetailsViewController alloc]init];
        vc.Model=model;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
}

//加载的Cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  TeachingAidsCell *cell=[tableView dequeueReusableCellWithIdentifier:@"AidsCell"];
  TeachingAidsModel *model=dataArray[indexPath.row];
NSLog(@"ModelComments===%@",model.ModelComments);
  [cell setModelAids:model];
    return  cell;
}
//返回行高：
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
}

#pragma 编辑：
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return   UITableViewCellEditingStyleDelete;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"你确定删除该教具类？" preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            TeachingAidsModel *model=dataArray[indexPath.row];
            
            [dataArray removeObjectAtIndex:indexPath.row];
            [OneArr removeObjectAtIndex:indexPath.row];
            [courseTable beginUpdates];
            [courseTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            [self DeleteDate:model ModelId:[NSString stringWithFormat:@"%@",model.ModelId]];
           
            [courseTable endUpdates];
            
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
        }]];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

//设置进入编辑状态时，Cell不会缩进

- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return NO;
    
}
// /courses/{courseId}/requiredTrainingAids/{modelId}
-(void)DeleteDate:(TeachingAidsModel *)model ModelId:(NSString *)ModelId{
    NSString *PlanId=[NSString stringWithFormat:@"%@",_PlanModel.PlanNameId];
    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/requiredTrainingAids/%@",LocalIP,PlanId,ModelId];
   
    [RequestTools RequestWithURL:Url Method:@"DELETE" Params:nil Success:^(NSDictionary *result) {
        
        
        
        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            
            [MBProgressHUD showToastAndMessage:@"教具成功删除!" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
}

@end
