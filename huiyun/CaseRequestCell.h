//
//  CaseRequestCell.h
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CaseRequestCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *CaseRequestName;

@property (weak, nonatomic) IBOutlet UILabel *CaseRequestLevel;
@property (weak, nonatomic) IBOutlet UILabel *CaseRequestNum;
@property (weak, nonatomic) IBOutlet UILabel *CaseRequestType;
@property (weak, nonatomic) IBOutlet UILabel *resultLab;

@end
