//
//  QuestionCell.h
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyQuesModel.h"
@interface QuestionCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate>

//当前选择的组
@property (strong, nonatomic)NSNumber *phoneNumber;

@property(strong, nonatomic) UIViewController *superVC;
@property(strong, nonatomic) UIView *showView;

@property (strong, nonatomic) NSArray *tribeArray;

@property (weak, nonatomic) IBOutlet UILabel *quesName;
@property (weak, nonatomic) IBOutlet UILabel *quesStatus;
@property (weak, nonatomic) IBOutlet UILabel *memNum;
@property (weak, nonatomic) IBOutlet UITextView *quesDes;
@property (weak, nonatomic) IBOutlet UITableView *teacherTab;

- (void)setModel:(MyQuesModel *)model andCurrentVC:(UIViewController *)currentVC;

@end
