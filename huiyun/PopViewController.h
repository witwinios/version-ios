//
//  PopViewController.h
//  UIPresentationVCtest
//
//  Created by Aotu on 16/4/11.
//  Copyright © 2016年 Aotu. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^xBlock)(NSString *result);
@interface PopViewController : UIViewController
+ (id)share;
@property(copy,nonatomic) xBlock  indexBlock;
@property (copy, nonatomic) NSArray *keyArray;
@end
