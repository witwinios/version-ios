//
//  SDOExamController.h
//  huiyun
//
//  Created by MacAir on 2017/11/29.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "BaseWitwinController.h"
#import "SelectButton.h"
#import "SelectField.h"
#import "SelectTextview.h"
#import "MZTimerLabel.h"
#import "QuesTools.h"
@interface SDOExamController : UIViewController<UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate>
- (IBAction)leftAction:(id)sender;
- (IBAction)rightAction:(id)sender;

@property (strong, nonatomic) UIScrollView *leftScroll;
@property (strong, nonatomic) UIScrollView *centerScroll;
@property (strong, nonatomic) UIScrollView *rightScroll;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;

@property (strong, nonatomic) UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *cardBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)submitAction:(id)sender;
- (IBAction)cardAction:(id)sender;


@property (strong, nonatomic) onlineModel *model;
//0 第一次  1 不是第一次
@property (strong, nonatomic) NSNumber *isFirst;
@end

