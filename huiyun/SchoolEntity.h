//
//  SchoolEntity.h
//  huiyun
//
//  Created by MacAir on 2017/9/26.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SchoolEntity : NSObject

@property (strong, nonatomic) NSNumber *schoolId;
@property (strong, nonatomic) NSString *schoolName;

@end
