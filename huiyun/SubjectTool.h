//
//  SubjectTool.h
//  huiyun
//
//  Created by MacAir on 2018/1/18.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubjectTool : NSObject
typedef NS_ENUM(NSInteger, SubjectType) {
    subjectTypeAdd = 0,
    subjectTypeUpdate
};

@property (assign, nonatomic) SubjectType subjectType;


+ (instancetype)share;

- (SubjectType)getType;

+ (void)showWithfileUrl:(NSString *)fileUrl;


@end
