//
//  OnlineStudentModel.m
//  huiyun
//
//  Created by MacAir on 2018/2/3.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "OnlineStudentModel.h"

@implementation OnlineStudentModel
//@property (strong, nonatomic) NSNumber *stuId;
//@property (strong, nonatomic) NSString *name;
//@property (strong, nonatomic) NSString *professin;
- (void)setProfessin:(NSString *)professin{
    if ([professin isKindOfClass:[NSNull class]]) {
        _professin = @"暂无";
    }else{
        _professin = professin;
    }
}
//@property (strong, nonatomic) NSString *className;
- (void)setClassName:(NSString *)className{
    if ([className isKindOfClass:[NSNull class]]) {
        _className = @"暂无";
    }else{
        _className = className;
    }
}
//@property (strong, nonatomic) NSNumber *inTime;
- (void)setInTime:(NSNumber *)inTime{
    if ([inTime isKindOfClass:[NSNull class]]) {
        _inTime = @0;
    }else{
        _inTime = inTime;
    }
}
@end
