//
//  CourseWareModel.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/2.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CourseWareModel : NSObject

@property(strong, nonatomic)NSNumber *isSelect;
@property(nonatomic,strong)NSNumber *CourseWareId;
@property(nonatomic,strong)NSString *CourseWareName;
@property(nonatomic,strong)NSNumber *CourseWarePublic;
@property(nonatomic,strong)NSString *CourseWareSubject;
@property(nonatomic,strong)NSString *CourseWareClass;
@property(nonatomic,strong)NSString *FileFormat;
@property(nonatomic,strong)NSString *FileUrl;
@property (nonatomic, assign) BOOL isCheck;


@end
