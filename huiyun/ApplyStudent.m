//
//  ApplyStudent.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/5.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "ApplyStudent.h"

@implementation ApplyStudent


-(void)setApplyStudentImg:(NSString *)ApplyStudentImg{
    if ([ApplyStudentImg isKindOfClass:[NSNull class]]) {
        _ApplyStudentImg = @"暂无";
    }else{
        _ApplyStudentImg = ApplyStudentImg;
    }
}

-(void)setApplyStudentName:(NSString *)ApplyStudentName{
    if ([ApplyStudentName isKindOfClass:[NSNull class]]) {
        _ApplyStudentName = @"暂无";
    }else{
        _ApplyStudentName = ApplyStudentName;
    }
}

-(void)setApplyStudentpicture:(NSString *)ApplyStudentpicture{
    if ([ApplyStudentpicture isKindOfClass:[NSNull class]]) {
        _ApplyStudentpicture = @"暂无";
    }else{
        _ApplyStudentpicture = ApplyStudentpicture;
    }
}

-(void)setApplyStudentID:(NSNumber *)ApplyStudentID{
    if ([ApplyStudentID isKindOfClass:[NSNull class]]) {
        _ApplyStudentID = [NSNumber numberWithInt:0];
    }else{
        _ApplyStudentID = ApplyStudentID;
    }
}


@end
