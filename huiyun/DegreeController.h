//
//  DegreeController.h
//  yun
//
//  Created by MacAir on 2017/8/29.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WXPPickerView.h"
@interface DegreeController : UIViewController<UITableViewDelegate,UITableViewDataSource,PickerViewOneDelegate>
@property (strong, nonatomic) AccountEntity *entity;
@end
