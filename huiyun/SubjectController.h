//
//  SubjectController.h
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupCell.h"
typedef void (^SubjectBlock)(NSArray *resultArray);
@interface SubjectController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic)SubjectBlock subjectBlock;
@end
