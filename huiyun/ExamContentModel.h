//
//  ExamContentModel.h
//  yun
//
//  Created by MacAir on 2017/7/21.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExamContentModel : NSObject
@property (strong, nonatomic) NSString *ExamContentModelStationName;
@property (strong, nonatomic) NSNumber *ExamContentModelStationId;
@property (strong, nonatomic) NSString *ExamContentModelPurpose;
@property (strong, nonatomic) NSString *ExamContentModelContent;
@property (strong, nonatomic) NSString *ExamContentModelStudentName;
@property (strong, nonatomic) NSNumber *ExamContentModelStudentId;
@property (strong, nonatomic) NSString *ExamContentModelTeacherName;
@property (strong, nonatomic) NSNumber *ExamContentModelTeacherId;
@property (strong, nonatomic) NSNumber *ExamContentModelDate;
@property (strong, nonatomic) NSString *ExamContentModelInternal;
@property (strong, nonatomic) NSNumber *ExamContentModelRecordId;
@end
