//
//  MeetingController.m
//  xiaoyun
//
//  Created by MacAir on 2017/3/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "MeetDisplayController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height
@interface MeetDisplayController ()
{
    UICollectionView *collectionView;
    DataManager *manager;
    //
    GSPJoinParam *info;
    AppDelegate *delega;
}
@end

@implementation MeetDisplayController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNav];
    [self createUI];
    [self setUpRefresh];
}
- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"所有直播";
    [backNavigation addSubview:titleLab];
    //
    delega = (AppDelegate *)[UIApplication sharedApplication].delegate;
}
- (void)loadMeet{
    manager = [[DataManager alloc]init];
    [manager loadWithURL:@"http://hzwitwin.gensee.com/integration/site/webcast/page?loginName=admin@hzwitwin.com&password=hzwitwin_123"];
    __block UICollectionView *collection = collectionView;
    manager.sBlock = ^(NSDictionary *dic){
        
        _meetArray = [dic objectForKey:@"list"];
        //
        [collection reloadData];
        [collection headerEndRefreshing];
    };
}
- (void)createUI{
    //
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.itemSize = CGSizeMake(150, 150);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, WIDTH, HEIGHT-64) collectionViewLayout:layout];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.backgroundColor = UIColorFromHex(0xC0C0C0);
    [collectionView registerNib:[UINib nibWithNibName:@"MeetItemCell" bundle:nil] forCellWithReuseIdentifier:@"item"];
    [self.view addSubview:collectionView];
}
- (void)setUpRefresh{
    [collectionView addHeaderWithTarget:self action:@selector(loadMeet)];
    [collectionView headerBeginRefreshing];
    //设置文字
    collectionView.headerPullToRefreshText = @"下拉刷新";
    collectionView.headerReleaseToRefreshText = @"松开进行刷新";
    collectionView.headerRefreshingText = @"刷新中。。。";
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -delegate
//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(120, 120);
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _meetArray.count;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(20.0f, 20.0f, 2.0f, 20.0f);
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionViewContent cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = [_meetArray objectAtIndex:indexPath.row];
    MeetItemCell *cell = (MeetItemCell *)[collectionViewContent dequeueReusableCellWithReuseIdentifier:@"item" forIndexPath:indexPath];
    cell.lab.text = [dic objectForKey:@"description"];
    return cell;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView1 didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //获取id
    NSDictionary *dic = [_meetArray objectAtIndex:indexPath.row];
    LiveRoomViewController *liveVC = [LiveRoomViewController new];
    liveVC.domain = @"hzwitwin.gensee.com";
    liveVC.watchPassword = @"000000";
    liveVC.UserName = @"student2";
    NSLog(@"--%@--",dic[@"id"]);
    liveVC.webcastID = dic[@"id"];
    liveVC.ServiceType = @"webcast";
    
    liveVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:liveVC animated:YES];
}
@end
