//
//  TeacherController.m
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TeacherController.h"

@interface TeacherController ()
{
    NSMutableArray *dataArray;
    UITableView *tabView;
}
@end

@implementation TeacherController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [tabView headerBeginRefreshing];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(10, 24.5, 20, 30);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"成员列表";
    [backNavigation addSubview:titleLab];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    [tabView registerNib:[UINib nibWithNibName:@"TeacherCell" bundle:nil] forCellReuseIdentifier:@"teacherMem"];
    [self.view addSubview:tabView];
}
#pragma -action
- (void)back:(UIButton *)btn{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)rightItemAction:(UIButton *)btn{
    
}
#pragma -delegate dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  self.tribeArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TeacherCell *cell = [tabView dequeueReusableCellWithIdentifier:@"teacherMem"];
    TribeMem *mem  = self.tribeArray[indexPath.row];
    cell.teacherName.text = mem.fullName;
    cell.teacherEmail.text = mem.email;
    cell.teacherPhone.text = [NSString stringWithFormat:@"%@",mem.phone];
    cell.phoneNum = [NSString stringWithFormat:@"%@",mem.phone];
    
    return cell;
}

@end
