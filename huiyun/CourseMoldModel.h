//
//  CourseMoldModel.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/27.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CourseMoldModel : NSObject

@property(strong,nonatomic)NSString *MoldName;
@property(strong,nonatomic)NSNumber *MoldId;

@property(nonatomic,strong)NSNumber *ChildrenNum;
@property(strong,nonatomic)NSMutableArray *ChildMoldArray;
@property(strong,nonatomic)NSString *ChildMoldName;
@property(strong,nonatomic)NSNumber *ChildMoldId;

@property(strong,nonatomic)NSDictionary *Metadata;
@property(strong,nonatomic)NSNumber *ParentId;

@end
