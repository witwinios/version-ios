//
//  TClassController.h
//  yun
//
//  Created by MacAir on 2017/7/17.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRoomCell.h"
#import "TRoomModel.h"
#import "SkillModel.h"
#import "ListStudentController.h"
@interface TClassController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (strong, nonatomic) SkillModel *skillModel;
@end
