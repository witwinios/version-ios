//
//  StandardCourseWareViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/2.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseAddModel.h"
typedef void (^BackCourseWare) (NSArray *arr);
@interface StandardCourseWareViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UITextFieldDelegate>
@property(strong,nonatomic)TCourseAddModel *PlanModel;
@property(strong,nonatomic)NSMutableArray *TempArray;

@property(strong,nonatomic)BackCourseWare backArr;

@property(nonatomic,getter=isEditing) BOOL editing;
-(void)setEditing:(BOOL)editing animated:(BOOL)animated;
@end
