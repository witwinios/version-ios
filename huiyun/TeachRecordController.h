//
//  TeachRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeachRecordController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *objectField;
@property (weak, nonatomic) IBOutlet UITextField *organizationField;
@property (weak, nonatomic) IBOutlet UIImageView *current_Image;
@property (weak, nonatomic) IBOutlet UITextField *numField;
- (IBAction)endAction:(id)sender;
@property (strong, nonatomic)UIImagePickerController *imagePickController;

@property (weak, nonatomic) IBOutlet UITextView *contentTextview;
- (IBAction)saveAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)fileAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIButton *endBtn;
- (IBAction)startAction:(id)sender;
@property (strong, nonatomic) CaseReuestModel *model;
@property (strong, nonatomic) CaseTeachModel *myModel;
@end
