//
//  ErrorModel.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ErrorModel.h"

@implementation ErrorModel
- (void)setTestName:(NSString *)testName{
    if ([testName isKindOfClass:[NSNull class]]||[testName isEqualToString:@""]) {
        _testName = @"暂无";
    }else{
        _testName = testName;
    }
}
- (void)setTestStatus:(NSString *)testStatus{
    if ([testStatus isEqualToString:@"NOT_START"]) {
        _testStatus = @"未开始";
    }else if ([testStatus isEqualToString:@"STARTED"]){
        _testStatus = @"已开始";
    }else if ([testStatus isEqualToString:@"REALEASED"]){
        _testStatus = @"已发布";
    }else if ([testStatus isEqualToString:@"PLANNING"]){
        _testStatus = @"计划中";
    }else if([testStatus isEqualToString:@"COMPLETED"]){
        _testStatus = @"已完成";
    }
}
- (void)setTestSubject:(NSString *)testSubject{
    if ([testSubject isKindOfClass:[NSNull class]] || [testSubject isEqualToString:@""]) {
        _testSubject = @"暂无";
    }else{
        _testSubject = testSubject;
    }
}
- (void)setTestPaper:(NSNumber *)testPaper{
    if ([testPaper isKindOfClass:[NSNull class]]) {
        _testPaper = 0;
    }else{
        _testPaper = testPaper;
    }
}
- (void)setTestCategory:(NSString *)testCategory{
    if ([testCategory isKindOfClass:[NSNull class]]) {
        _testCategory = @"暂无";
    }else{
        _testCategory = testCategory;
    }
}
- (void)setTestDescription:(NSString *)testDescription{
    if ([testDescription isKindOfClass:[NSNull class]]) {
        _testDescription = @"暂无描述。";
    }else{
        _testDescription = testDescription;
    }
}
@end
