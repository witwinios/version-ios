//
//  SelfListCell.h
//  xiaoyun
//
//  Created by MacAir on 16/12/29.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelfListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *statusLab;
@property (weak, nonatomic) IBOutlet UILabel *testName;
@property (weak, nonatomic) IBOutlet UILabel *pageNum;
@property (weak, nonatomic) IBOutlet UITextView *paperDes;
@end
