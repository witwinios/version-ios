//
//  RecentCell.m
//  xiaoyun
//
//  Created by MacAir on 16/12/23.
//  Copyright © 2016年 慧云医疗. All rights reserved.
//

#import "RecentOSCECell.h"

@implementation RecentOSCECell

- (void)awakeFromNib {
    NSString *str = self.recentTime.text;
    NSRange range = [str rangeOfString:@" "];
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc]initWithString:str];
    [attribute addAttribute:NSFontAttributeName value:[UIColor redColor] range:NSMakeRange(0,range.location)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (void)setProperty:(RecentOSCECell *)property{
    
}

@end
