//
//  LiveView.m
//  iOSZhongYe
//
//  Created by 郑敏捷 on 2017/5/19.
//  Copyright © 2017年 郑敏捷. All rights reserved.
//

#import "LiveView.h"

#import "Reachability.h"
#import <MediaPlayer/MediaPlayer.h>

#import "MMMaterialDesignSpinner.h"

#import "Macro.h"

#import "MBProgressHUD+Zmj.h"

@interface LiveView ()<UIGestureRecognizerDelegate>

@property (strong, nonatomic) GSPJoinParam *joinParam;
/** 判断当前网络状态 */
@property (strong, nonatomic) Reachability *reachability;

@property (strong, nonatomic) GSPVideoView  *videoView;

@property (strong, nonatomic) UIImageView  *bottomImageView;

@property (strong, nonatomic) UIImageView  *topImageView;

@property (strong, nonatomic) UIButton     *backBtn;

@property (nonatomic, strong) UIButton     *lockBtn;

@property (strong, nonatomic) UILabel      *titleLabel;

@property (strong, nonatomic) UILabel      *peopleLabel;

@property (strong, nonatomic) UIButton     *fullScreenBtn;

@property (nonatomic, strong) ZFBrightnessView  *brightnessView;

@property (nonatomic, strong) MMMaterialDesignSpinner *activity;

@property (nonatomic, strong) UISlider     *volumeViewSlider;

@property (strong, nonatomic) UITapGestureRecognizer *singleTap;

@property (strong, nonatomic) UIPanGestureRecognizer *panRecognizer;

@property (strong, nonatomic) NSMutableArray  *usersArray;

@property (assign, nonatomic) BOOL          isShowing;

@property (assign, nonatomic) BOOL          isVolume;

@property (assign, nonatomic) BOOL          isFullScreen;

@property (assign, nonatomic) BOOL          didEnterBackground;

@property (assign, nonatomic) BOOL          isPlayer;

@property (assign, nonatomic) BOOL          isBack;

@property (assign, nonatomic) BOOL          isLock;

@end

@implementation LiveView

- (instancetype)init {
    if (self = [super init]) {
        
        [self initView];

        [self initData];
        
        [self initGesture];

        [self initNotifications];

        [self makeSubViewsConstraints];
    }
    return self;
}

- (void)setLiveModel:(LiveModel *)liveModel {

    _liveModel = liveModel;
    
    [self addPlayerToFatherView:_liveModel.fatherView];
    
    _joinParam.domain = _liveModel.domain;
    
    _joinParam.webcastID = _liveModel.webcastID;
    
    _joinParam.nickName = _liveModel.UserName;
    
    _joinParam.watchPassword = _liveModel.watchPassword;
    
    if ([_liveModel.ServiceType isEqualToString:@"webcast"]) {
    
        _joinParam.serviceType = GSPServiceTypeWebcast;
    
    }else {
        
        _joinParam.serviceType = GSPServiceTypeTraining;
    }

    [appDelegate.playerManager joinWithParam:_joinParam];
    
    [_activity startAnimating];
    
    _titleLabel.text = _liveModel.LiveClassName;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _videoView.frame = self.bounds;
}

- (void)playerManager:(GSPPlayerManager *)playerManager didReceiveSelfJoinResult:(GSPJoinResult)joinResult {
    
    [_activity stopAnimating];
    
    if (joinResult != GSPJoinResultOK) {
        
        _isAlert = YES;
        
        if (_isFullScreen) {
            
            _fullScreenBtn.selected = !_fullScreenBtn.selected;
            [self interfaceOrientation:UIInterfaceOrientationPortrait];
            _lockBtn.hidden = YES;
        }
        
        if (joinResult == GSPJoinResultNetworkError) {
            
            if ([_delegate respondsToSelector:@selector(joinRoomResult:isCancel:)]) {
                [_delegate joinRoomResult:@"网络错误" isCancel:YES];
            }
            
        }else if (joinResult == GSPJoinResultParamsError) {
            
            if ([_delegate respondsToSelector:@selector(joinRoomResult:isCancel:)]) {
                [_delegate joinRoomResult:@"参数错误" isCancel:YES];
            }
            
        }else if (joinResult == GSPJoinResultTOO_EARLY) {
            
            if ([_delegate respondsToSelector:@selector(joinRoomResult:isCancel:)]) {
                [_delegate joinRoomResult:@"直播未开始" isCancel:NO];
            }
            
        }else if (joinResult == GSPJoinResultLICENSE) {
            
            if ([_delegate respondsToSelector:@selector(joinRoomResult:isCancel:)]) {
                [_delegate joinRoomResult:@"人数已满" isCancel:YES];
            }
            
        }else if (joinResult == GSPJoinResultTimeout) {
            
            if ([_delegate respondsToSelector:@selector(joinRoomResult:isCancel:)]) {
                [_delegate joinRoomResult:@"连接超时" isCancel:YES];
            }
            
        }else {
            
            if ([_delegate respondsToSelector:@selector(joinRoomResult:isCancel:)]) {
                [_delegate joinRoomResult:@"未知错误" isCancel:YES];
            }
        }
    }else {
    
        _isPlayer = YES;
        [MBProgressHUD showToastAndMessage:@"加入成功" places:0 toView:nil];
    }
}

- (void)playerManager:(GSPPlayerManager *)playerManager didSelfLeaveFor:(GSPLeaveReason)reason {
    
    NSString *reasonStr = nil;
    
    switch (reason) {
            
        case GSPLeaveReasonEjected:
            
            reasonStr = NSLocalizedString(@"被踢出直播", @"");
            
            break;
            
        case GSPLeaveReasonTimeout:
            
            reasonStr = NSLocalizedString(@"超时", @"");
            
            break;
            
        case GSPLeaveReasonClosed:
            
            reasonStr = NSLocalizedString(@"直播关闭", @"");
            
            break;
            
        case GSPLeaveReasonUnknown:
            
            reasonStr = NSLocalizedString(@"位置错误", @"");
            
            break;
            
        default:
            break;
    }
    
    if (reasonStr != nil) {
        
        [self interfaceOrientation:UIInterfaceOrientationPortrait];
        
        if ([_delegate respondsToSelector:@selector(joinRoomResult:isCancel:)]) {
            [_delegate joinRoomResult:NSLocalizedString(@"退出直播", @"") isCancel:YES];
        }
    }
}

- (void)playerManager:(GSPPlayerManager*)playerManager isPaused:(BOOL)isPaused {
    
    if (isPaused) {
        
        if ([_delegate respondsToSelector:@selector(joinRoomResult:isCancel:)]) {
            [_delegate joinRoomResult:NSLocalizedString(@"直播已暂停", @"") isCancel:NO];
        }
    }
}

- (void)playerManagerWillReconnect:(GSPPlayerManager *)playerManager {
    
    [_activity startAnimating];
}

- (void)playerManager:(GSPPlayerManager *)playerManager didUserJoin:(GSPUserInfo *)userInfo {
    
    [_activity stopAnimating];
    
    if (userInfo.userID != playerManager.selfUserInfo.userID) {
        
        [_usersArray addObject:userInfo];
    }
    _peopleLabel.text = [NSString stringWithFormat:@"有%ld人在观看", _usersArray.count];
}

- (void)playerManager:(GSPPlayerManager *)playerManager didUserLeave:(GSPUserInfo *)userInfo {
    
    [_usersArray removeObject:userInfo];
}

- (BOOL)sendChatMessage:(GSPChatMessage *)chatMessage {
    
    return [appDelegate.playerManager chatWithAll:chatMessage];
}

- (void)initView {
    
    [self joinParam];
    
    appDelegate.playerManager.delegate = self;
    [appDelegate.playerManager enableVideo:YES];
    [appDelegate.playerManager enableAudio:YES];
    
    [self addSubview:self.videoView];
    
    appDelegate.playerManager.videoView = _videoView;
    
    [self addSubview:self.topImageView];
    [self addSubview:self.bottomImageView];
    [self.topImageView addSubview:self.backBtn];
    [self.topImageView addSubview:self.titleLabel];
    [self.bottomImageView addSubview:self.peopleLabel];
    [self.bottomImageView addSubview:self.fullScreenBtn];
    [self addSubview:self.lockBtn];
    
    [self addSubview:self.activity];
    
    [self configureVolume];
    [self brightnessView];
}

- (void)initData {

    if (_usersArray.count > 0) {[_usersArray removeAllObjects];}
    
    [self usersArray];
    
//    _peopleLabel.text = [NSString stringWithFormat:@"有%ld人在观看", _usersArray.count];
}

- (void)initGesture {
    
    [self addGestureRecognizer:self.singleTap];
    [self addGestureRecognizer:self.panRecognizer];
}

#pragma mark - 观察者、通知
/**
 *  添加观察者、通知
 */
- (void)initNotifications {
    // app退到后台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground) name:UIApplicationWillResignActiveNotification object:nil];
    // app进入前台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterPlayground) name:UIApplicationDidBecomeActiveNotification object:nil];
    // 监听耳机插入和拔掉通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioRouteChangeListenerCallback:) name:AVAudioSessionRouteChangeNotification object:nil];
    // 监测设备方向
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onDeviceOrientationChange)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onStatusBarOrientationChange)
                                                 name:UIApplicationDidChangeStatusBarOrientationNotification
                                               object:nil];
    
    // 监控网络变化的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(netChanged:) name:kReachabilityChangedNotification object:nil];
    
    // 获取访问指定站点的Reachability对象
    _reachability = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    
    // 让Reachability对象开启被监听状态
    [_reachability startNotifier];
}

- (void)netChanged:(NSNotification *)notifi {
    // 通过通知对象获取被监听的Reachability对象
    Reachability *curReach = [notifi object];
    
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    
    [self updateInterfaceWithReachability:curReach];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability {
    
    if (reachability == _reachability) {
        
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        
        switch (netStatus) {
                
            case NotReachable:   {
                
                
                break;
            }
            case ReachableViaWWAN: {
                
                [appDelegate.playerManager reconnect];
         
                break;
            }
            case ReachableViaWiFi: {
                
                [appDelegate.playerManager reconnect];
            
                break;
            }
        }
    }
}

- (void)makeSubViewsConstraints {
    
    [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.top.equalTo(self);
        make.height.mas_equalTo(ZmjSize(50));
    }];
    
    [self.bottomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(self);
        make.height.mas_equalTo(ZmjSize(50));
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.topImageView.mas_leading);
        make.top.equalTo(self.topImageView.mas_top);
        make.width.height.mas_equalTo(ZmjSize(40));
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.backBtn.mas_trailing).offset(ZmjSize(5));
        make.centerY.equalTo(self.backBtn.mas_centerY);
        make.trailing.equalTo(self.mas_trailing).offset(ZmjSize(-15));
    }];
    
    [self.fullScreenBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(40);
        make.trailing.equalTo(self.bottomImageView.mas_trailing).offset(-5);
        make.bottom.equalTo(self.bottomImageView.mas_bottom);
    }];
    
    [self.peopleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.bottomImageView.mas_leading).offset(ZmjSize(15));
        make.bottom.equalTo(self.bottomImageView);
        make.trailing.equalTo(self.fullScreenBtn.mas_leading);
        make.height.mas_equalTo(ZmjSize(40));
    }];
    
    [self.lockBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.mas_leading).offset(15);
        make.centerY.equalTo(self.mas_centerY);
        make.width.height.mas_equalTo(ZmjSize(30));
    }];
    
    [self.activity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.width.with.height.mas_equalTo(45);
    }];
}

- (GSPVideoView *)videoView {
    if (!_videoView) {
        _videoView = [[GSPVideoView alloc]init];
        _videoView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _videoView;
}

- (UIImageView *)topImageView {
    if (!_topImageView) {
        _topImageView                        = [[UIImageView alloc] init];
        _topImageView.userInteractionEnabled = YES;
        _topImageView.alpha                  = 0;
        _topImageView.image                  = ZFPlayerImage(@"ZFPlayer_top_shadow");
    }
    return _topImageView;
}

- (UIImageView *)bottomImageView {
    if (!_bottomImageView) {
        _bottomImageView                        = [[UIImageView alloc] init];
        _bottomImageView.userInteractionEnabled = YES;
        _bottomImageView.alpha                  = 0;
        _bottomImageView.image                  = ZFPlayerImage(@"ZFPlayer_bottom_shadow");
    }
    return _bottomImageView;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:ZFPlayerImage(@"ZFPlayer_back_full") forState:UIControlStateNormal];
        [_backBtn addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backBtn;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.numberOfLines = 0;
        _titleLabel.font = ZmjFontSize(12.0);
    }
    return _titleLabel;
}

- (UILabel *)peopleLabel {
    if (!_peopleLabel) {
        _peopleLabel = [[UILabel alloc] init];
        _peopleLabel.textColor = [UIColor whiteColor];
        _peopleLabel.numberOfLines = 0;
        _peopleLabel.font = ZmjFontSize(12.0);
    }
    return _peopleLabel;
}

- (UIButton *)fullScreenBtn {
    if (!_fullScreenBtn) {
        _fullScreenBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_fullScreenBtn setImage:ZFPlayerImage(@"ZFPlayer_fullscreen") forState:UIControlStateNormal];
        [_fullScreenBtn setImage:ZFPlayerImage(@"ZFPlayer_shrinkscreen") forState:UIControlStateSelected];
        [_fullScreenBtn addTarget:self action:@selector(fullScreenBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _fullScreenBtn;
}

- (UIButton *)lockBtn {
    if (!_lockBtn) {
        _lockBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_lockBtn setImage:ZFPlayerImage(@"ZFPlayer_unlock-nor") forState:UIControlStateNormal];
        [_lockBtn setImage:ZFPlayerImage(@"ZFPlayer_lock-nor") forState:UIControlStateSelected];
        [_lockBtn addTarget:self action:@selector(lockScreenBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        _lockBtn.alpha = 0;
    }
    return _lockBtn;
}

- (ZFBrightnessView *)brightnessView {
    if (!_brightnessView) {
        _brightnessView = [ZFBrightnessView sharedBrightnessView];
    }
    return _brightnessView;
}

- (MMMaterialDesignSpinner *)activity {
    if (!_activity) {
        _activity = [[MMMaterialDesignSpinner alloc] init];
        _activity.lineWidth = 1;
        _activity.duration  = 1;
        _activity.tintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9];
    }
    return _activity;
}

- (NSMutableArray *)usersArray {
    if (!_usersArray) {
        _usersArray = [[NSMutableArray alloc]init];
    }
    return _usersArray;
}

- (UITapGestureRecognizer *)singleTap {
    if (!_singleTap) {
        _singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapAction:)];
        _singleTap.delegate                = self;
        _singleTap.numberOfTouchesRequired = 1;
        _singleTap.numberOfTapsRequired    = 1;
    }
    return _singleTap;
}

- (UIPanGestureRecognizer *)panRecognizer {
    if (!_panRecognizer) {
        _panRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panDirection:)];
        _panRecognizer.delegate = self;
        [_panRecognizer setMaximumNumberOfTouches:1];
        [_panRecognizer setDelaysTouchesBegan:YES];
        [_panRecognizer setDelaysTouchesEnded:YES];
        [_panRecognizer setCancelsTouchesInView:YES];
    }
    return _panRecognizer;
}

- (GSPJoinParam *)joinParam {
    if (!_joinParam) {
        _joinParam = [GSPJoinParam new];
    }
    return _joinParam;
}

- (void)singleTapAction:(UIGestureRecognizer *)gesture {
    
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        
        if ([_delegate respondsToSelector:@selector(resign)]) {
            
            [_delegate resign];
        }
        
        if (!_isShowing) {[self showControllView];}
        else             {[self hideControllView];}
        
        [self autoFadeOutControlView];
    }
}

- (void)backBtnClick:(UIButton *)button {

    if (_isFullScreen) {
        
        _fullScreenBtn.selected = !_fullScreenBtn.selected;
        [self interfaceOrientation:UIInterfaceOrientationPortrait];
    
    }else {
    
        _isBack = YES;
        
        if ([_delegate respondsToSelector:@selector(backBtnAction)]) {
            [_delegate backBtnAction];
        }
    }
    
    _lockBtn.alpha = [[NSNumber numberWithBool:_isFullScreen] floatValue];
    
    [self autoFadeOutControlView];
}

- (void)fullScreenBtnClick:(UIButton *)button {
    
    button.selected = !button.selected;
    
    if (_isFullScreen) {
        
        [self interfaceOrientation:UIInterfaceOrientationPortrait];
        
        _isFullScreen = NO;
        
    }else {
        
        UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
        
        if (orientation == UIDeviceOrientationLandscapeRight) {
            
            [self interfaceOrientation:UIInterfaceOrientationLandscapeLeft];
            
        } else {
            
            [self interfaceOrientation:UIInterfaceOrientationLandscapeRight];
        }
        _isFullScreen = YES;
    }
    
    _lockBtn.alpha = [[NSNumber numberWithBool:_isFullScreen] floatValue];
    
    [self autoFadeOutControlView];
}

- (void)lockScreenBtnClick:(UIButton *)button {

    button.selected = !button.selected;
    
    if (button.selected) {
        
        [self lockHideControllView];
    
    }else {
    
        [self showControllView];
    }
}

/**
 *  pan手势事件
 *
 *  @param pan UIPanGestureRecognizer
 */
- (void)panDirection:(UIPanGestureRecognizer *)pan {
    //根据在view上Pan的位置，确定是调音量还是亮度
    CGPoint locationPoint = [pan locationInView:self];
    
    // 我们要响应水平移动和垂直移动
    // 根据上次和本次移动的位置，算出一个速率的point
    CGPoint veloctyPoint = [pan velocityInView:self];
    
    // 判断是垂直移动还是水平移动
    switch (pan.state) {
        case UIGestureRecognizerStateBegan:{ // 开始移动
            // 使用绝对值来判断移动的方向
            CGFloat x = fabs(veloctyPoint.x);
            CGFloat y = fabs(veloctyPoint.y);
         
            if (x > y) { // 水平移动
                
                return;
            }
            else if (x < y){ // 垂直移动
                // 开始滑动的时候,状态改为正在控制音量
                if (locationPoint.x > self.bounds.size.width / 2) {
            
                    self.isVolume = YES;
                    
                }else { // 状态改为显示亮度调节
                    
                    self.isVolume = NO;
                }
            }
            break;
        }
        case UIGestureRecognizerStateChanged:{ // 正在移动

            CGFloat x = fabs(veloctyPoint.x);
            CGFloat y = fabs(veloctyPoint.y);
            
            if (x > y) { // 水平移动
                
                return;
            }
            
            if (locationPoint.y > ScreenWidth) {return;}
            
            [self verticalMoved:veloctyPoint.y]; // 垂直移动方法只要y方向的值

            break;
        }
        case UIGestureRecognizerStateEnded:{ // 移动停止

            self.isVolume = NO;
            
            break;
        }
        default:
            break;
    }
}

/**
 *  pan垂直移动的方法
 *
 *  @param value void
 */
- (void)verticalMoved:(CGFloat)value {
    
    self.isVolume ? (self.volumeViewSlider.value -= value / 10000) : ([UIScreen mainScreen].brightness -= value / 10000);
}

/**
 *  获取系统音量
 */
- (void)configureVolume {
    
    MPVolumeView *volumeView = [[MPVolumeView alloc] init];
    _volumeViewSlider = nil;
    
    for (UIView *view in [volumeView subviews]) {
        
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]) {
            
            _volumeViewSlider = (UISlider *)view;
            break;
        }
    }
    // 使用这个category的应用不会随着手机静音键打开而静音，可在手机静音下播放声音
    NSError *setCategoryError = nil;
    BOOL success = [[AVAudioSession sharedInstance]
                    setCategory: AVAudioSessionCategoryPlayback
                    error: &setCategoryError];
    
    if (!success) { /* handle the error in setCategoryError */ }
}

- (void)showControllView {
    
    [UIView animateWithDuration:0.35 animations:^{
    
        if (_lockBtn.selected) {
            
            _lockBtn.alpha = 1;
            ZFPlayerShared.isStatusBarHidden = NO;
        
        }else {
        
            ZFPlayerShared.isStatusBarHidden = NO;
            _topImageView.alpha = 1;
            _bottomImageView.alpha = 1;
            if (_isFullScreen) {_lockBtn.alpha = 1;}
        }
    } completion:^(BOOL finished) {
        
        _isShowing = !_isShowing;
    }];
}

- (void)hideControllView {
    
    [UIView animateWithDuration:0.35 animations:^{
    
        if (_isFullScreen) {ZFPlayerShared.isStatusBarHidden = YES;}
        _topImageView.alpha = 0;
        _bottomImageView.alpha = 0;
        _lockBtn.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        _isShowing = !_isShowing;
    }];
}

- (void)lockHideControllView {

    [UIView animateWithDuration:0.35 animations:^{
        
        _topImageView.alpha = 0;
        _bottomImageView.alpha = 0;
        
    } completion:nil];
}

- (void)autoFadeOutControlView {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideControllView) object:nil];
    [self performSelector:@selector(hideControllView) withObject:nil afterDelay:7.0];
}

/**
 *  屏幕转屏
 *
 *  @param orientation 屏幕方向
 */
- (void)interfaceOrientation:(UIInterfaceOrientation)orientation {
    if (orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft) {
        // 设置横屏
        [self setOrientationLandscapeConstraint:orientation];
    } else if (orientation == UIInterfaceOrientationPortrait) {
        // 设置竖屏
        [self setOrientationPortraitConstraint];
    }
}

/**
 *  设置横屏的约束
 */
- (void)setOrientationLandscapeConstraint:(UIInterfaceOrientation)orientation {
    [self toOrientation:orientation];
    self.isFullScreen = YES;
    
    [self.backBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topImageView.mas_top).offset(20);
        make.leading.equalTo(self.topImageView.mas_leading);
        make.width.height.mas_equalTo(40);
    }];
}

/**
 *  设置竖屏的约束
 */
- (void)setOrientationPortraitConstraint {

    [self addPlayerToFatherView:self.liveModel.fatherView];
    
    [self toOrientation:UIInterfaceOrientationPortrait];
    self.isFullScreen = NO;
    [self.backBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topImageView.mas_top);
        make.leading.equalTo(self.topImageView.mas_leading);
        make.width.height.mas_equalTo(40);
    }];
}

- (void)toOrientation:(UIInterfaceOrientation)orientation {
    // 获取到当前状态条的方向
    UIInterfaceOrientation currentOrientation = [UIApplication sharedApplication].statusBarOrientation;
    // 判断如果当前方向和要旋转的方向一致,那么不做任何操作
    if (currentOrientation == orientation) { return; }
    
    // 根据要旋转的方向,使用Masonry重新修改限制
    if (orientation != UIInterfaceOrientationPortrait) {//
        // 这个地方加判断是为了从全屏的一侧,直接到全屏的另一侧不用修改限制,否则会出错;
        if (currentOrientation == UIInterfaceOrientationPortrait) {
            [self removeFromSuperview];
            ZFBrightnessView *brightnessView = [ZFBrightnessView sharedBrightnessView];
            [[UIApplication sharedApplication].keyWindow insertSubview:self belowSubview:brightnessView];
            [self mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(ScreenHeight));
                make.height.equalTo(@(ScreenWidth));
                make.center.equalTo([UIApplication sharedApplication].keyWindow);
            }];
            
            [self.backBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.topImageView.mas_top).offset(20);
                make.leading.equalTo(self.topImageView.mas_leading);
                make.width.height.mas_equalTo(40);
            }];
        }
    }
    // iOS6.0之后,设置状态条的方法能使用的前提是shouldAutorotate为NO,也就是说这个视图控制器内,旋转要关掉;
    // 也就是说在实现这个方法的时候-(BOOL)shouldAutorotate返回值要为NO
    [[UIApplication sharedApplication] setStatusBarOrientation:orientation animated:NO];

    // 获取旋转状态条需要的时间:
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    // 更改了状态条的方向,但是设备方向UIInterfaceOrientation还是正方向的,这就要设置给你播放视频的视图的方向设置旋转
    // 给你的播放视频的view视图设置旋转
    self.transform = CGAffineTransformIdentity;
    self.transform = [self getTransformRotationAngle];
    // 开始旋转
    [UIView commitAnimations];
}

/**
 * 获取变换的旋转角度
 *
 * @return 角度
 */
- (CGAffineTransform)getTransformRotationAngle {
    // 状态条的方向已经设置过,所以这个就是你想要旋转的方向
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    // 根据要进行旋转的方向来计算旋转的角度
    if (orientation == UIInterfaceOrientationPortrait) {
        return CGAffineTransformIdentity;
    } else if (orientation == UIInterfaceOrientationLandscapeLeft){
        return CGAffineTransformMakeRotation(-M_PI_2);
    } else if(orientation == UIInterfaceOrientationLandscapeRight){
        return CGAffineTransformMakeRotation(M_PI_2);
    }
    return CGAffineTransformIdentity;
}

/**
 *  player添加到fatherView上
 */
- (void)addPlayerToFatherView:(UIView *)view {
    // 这里应该添加判断，因为view有可能为空，当view为空时[view addSubview:self]会crash
    if (view) {
        
        [self removeFromSuperview];
        [view addSubview:self];
        [self mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_offset(UIEdgeInsetsZero);
        }];
    }
}

/**
 *  屏幕方向发生变化会调用这里
 */
- (void)onDeviceOrientationChange {
    
    if (_isBack) {return;}
    if (_isAlert) {return;}
    if (_lockBtn.selected) {return;}
    if (_didEnterBackground) {return;};
    
    [self showControllView];
    
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = (UIInterfaceOrientation)orientation;
    if (orientation == UIDeviceOrientationFaceUp || orientation == UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationUnknown ) { return; }
    
    switch (interfaceOrientation) {
            
        case UIInterfaceOrientationPortraitUpsideDown:{
        }
            break;
        case UIInterfaceOrientationPortrait:{
            if (self.isFullScreen) {
                [self toOrientation:UIInterfaceOrientationPortrait];
                
            }
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:{
            if (self.isFullScreen == NO) {
                [self toOrientation:UIInterfaceOrientationLandscapeLeft];
                self.isFullScreen = YES;
            } else {
                [self toOrientation:UIInterfaceOrientationLandscapeLeft];
            }
            
        }
            break;
        case UIInterfaceOrientationLandscapeRight:{
            if (self.isFullScreen == NO) {
                [self toOrientation:UIInterfaceOrientationLandscapeRight];
                self.isFullScreen = YES;
            } else {
                [self toOrientation:UIInterfaceOrientationLandscapeRight];
            }
        }
            break;
        default:
            break;
    }
    
    _fullScreenBtn.selected = _isFullScreen;
    
    _lockBtn.alpha = [[NSNumber numberWithBool:_isFullScreen] floatValue];
    
    [self autoFadeOutControlView];
}

// 状态条变化通知（在前台播放才去处理）
- (void)onStatusBarOrientationChange {
    
    if (!self.didEnterBackground) {
        // 获取到当前状态条的方向
        UIInterfaceOrientation currentOrientation = [UIApplication sharedApplication].statusBarOrientation;
        if (currentOrientation == UIInterfaceOrientationPortrait) {
            
            [self setOrientationPortraitConstraint];
            [self.brightnessView removeFromSuperview];
            [[UIApplication sharedApplication].keyWindow addSubview:self.brightnessView];
            [self.brightnessView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.width.height.mas_equalTo(155);
                make.leading.mas_equalTo((ScreenWidth-155)/2);
                make.top.mas_equalTo((ScreenHeight-155)/2);
            }];
        } else {
            
            if (currentOrientation == UIInterfaceOrientationLandscapeRight) {
                [self toOrientation:UIInterfaceOrientationLandscapeRight];
            } else if (currentOrientation == UIDeviceOrientationLandscapeLeft){
                [self toOrientation:UIInterfaceOrientationLandscapeLeft];
            }
            [self.brightnessView removeFromSuperview];
            [self addSubview:self.brightnessView];
            [self.brightnessView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.center.mas_equalTo(self);
                make.width.height.mas_equalTo(155);
            }];
        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isKindOfClass:[UIImageView class]]) {
        
        return NO;
    
    }else if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
    
        if (_lockBtn.selected || !_isPlayer) {
            
            return NO;
        }
    }
    return YES;
}

/**
 *  应用退到后台
 */
- (void)appDidEnterBackground {
    
    _didEnterBackground     = YES;
    
    _isLock = _lockBtn.selected;
    // 退到后台锁定屏幕方向
    _lockBtn.selected = YES;
}

/**
 *  应用进入前台
 */
- (void)appDidEnterPlayground {
    
    _didEnterBackground     = NO;
    
    _lockBtn.selected = _isLock;
    
    _isLock = NO;
    
    [appDelegate.playerManager resetAudioHelper];
}

/**
 *  耳机插入、拔出事件
 */
- (void)audioRouteChangeListenerCallback:(NSNotification*)notification {
    NSDictionary *interuptionDict = notification.userInfo;
    
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
            
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            // 耳机插入
            break;
            
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable: {
            // 耳机拔掉
            // 拔掉耳机继续播放
            AVAudioSession *audioSession = [AVAudioSession sharedInstance];
            
            [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
            
            [audioSession setActive:YES error:nil];
        }
            break;
            
        case AVAudioSessionRouteChangeReasonCategoryChange:
            // called at start - also when other audio wants to play
            ZmjLog(@"AVAudioSessionRouteChangeReasonCategoryChange");
            break;
    }
}

- (void)dealloc {
    
    ZmjLog(@"%@释放了",self.class);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    [_reachability stopNotifier];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
