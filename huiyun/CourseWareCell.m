//
//  CourseWareCell.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/2.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "CourseWareCell.h"

@implementation CourseWareCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.CourseWareChoice.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.CourseWareChoice.layer.borderWidth=1;
    self.CourseWareChoice.layer.cornerRadius=10;
    
    self.CourseWareName.numberOfLines = 0;
    [self.CourseWareName sizeToFit];
     self.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.3];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)setProperty:(CourseWareModel *)model{
    self.CourseWareName.text=model.CourseWareName;
   
    
    if([model.CourseWarePublic isEqual:@(YES)]){
        self.CourseWarePublic.text=@"公开";
    }else if([model.CourseWarePublic isEqual:@(NO)]){
        self.CourseWarePublic.text=@"不公开";
    }
    
    if (model.isCheck) {
        self.BgVc.backgroundColor = [UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.4];
        self.CourseWareChoice.layer.borderColor=[UIColor whiteColor].CGColor;
        [self.CourseWareChoice setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
    } else {
        self.BgVc.backgroundColor = [UIColor whiteColor];
        self.CourseWareChoice.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
         [self.CourseWareChoice setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    }
    
    
    
    self.CourseWareSubject.text=model.CourseWareSubject;
    self.CourseWareClass.text=model.CourseWareClass;
}


@end
