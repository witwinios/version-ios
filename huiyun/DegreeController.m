//
//  DegreeController.m
//  yun
//
//  Created by MacAir on 2017/8/29.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "DegreeController.h"

@interface DegreeController ()
{
    NSArray *titleArray;
    NSArray *contentArray;
    Edit status;
    UIButton *rightBtn;
    NSMutableDictionary *updateDic;
}
@property (strong, nonatomic)WXPPickerView * PickerOne;
@end

@implementation DegreeController

- (void)viewDidLoad {
    [super viewDidLoad];
    titleArray = @[@"最高学历:",@"毕业院校:",@"学制类型:",@"学位类型:",@"学位:",@"在职单位:"];
    
    contentArray = @[self.entity.userHighDegree,self.entity.userSchool,self.entity.userEducationType,self.entity.userDegreeType,self.entity.userDegree,self.entity.userSource];
    
    updateDic = [NSMutableDictionary dictionaryWithDictionary:self.entity.responseDic];
    [self setUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    status = EditNo;
    rightBtn.selected = NO;
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xBEBEBE);
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-60, 29.5, 60, 25);
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [rightBtn setTitle:@"保存" forState:UIControlStateSelected];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
    [rightBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"学历信息";
    [backNavigation addSubview:titleLab];
    //
    //tableView
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-164) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [tableView registerNib:[UINib nibWithNibName:@"AccountCell1" bundle:nil] forCellReuseIdentifier:@"account1"];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.bounces = NO;
    [self.view addSubview:tableView];
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)editAction:(UIButton *)btn{
    btn.selected = !btn.selected;
    if (btn.selected) {
        [MBProgressHUD showToastAndMessage:@"进入编辑模式~" places:0 toView:nil];
        status = EditYes;
    }else{
        status = EditNo;
        //修改
        [self updateInfo:0];
    }
}
- (void)updateInfo:(int)s{
    //s = 1 返回
    NSString *requesUrl = [NSString stringWithFormat:@"%@/users/%@",LocalIP,self.entity.userID];
    [RequestTools RequestWithURL:requesUrl Method:@"put" Params:updateDic Message:@"修改中" Success:^(NSDictionary *result) {
        [MBProgressHUD showToastAndMessage:@"成功!" places:0 toView:nil];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"修改失败!" places:0 toView:nil];
    }];
}
- (void)PickerViewRightButtonOncleck:(NSInteger)index{
    [self.PickerOne close];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return titleArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (status == EditYes) {
        AccountCell1 *cell = [tableView cellForRowAtIndexPath:indexPath];
        __block NSMutableDictionary *response = updateDic;
        __block DegreeController *teachVC = self;
        if (indexPath.row == 0) {
            NSArray *array = @[@"中专",@"大专",@"本科",@"硕士研究生",@"博士研究生"];
            NSDictionary *titleDic = @{@"中专":@"technical_secondary_school",@"大专":@"junior_college",@"本科":@"undergraduate",@"硕士研究生":@"master",@"博士研究生":@"doctor"};
            self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:array]];
            self.PickerOne.delegate = self;
            self.PickerOne.rightBtnTitle = @"确认";
            self.PickerOne.backgroundColor = [UIColor whiteColor];
            self.PickerOne.index = ^(int index){
                cell.content.text = array[index];
                teachVC.entity.userDegree = array[index];
                [response setObject:titleDic[array[index]] forKey:@"educationType"];
            };
            [self.view addSubview:self.PickerOne];
            [self.PickerOne show];
        }else if (indexPath.row == 1){
            NSMutableArray *indexArray = [NSMutableArray new];
            NSMutableArray *nameArray = [NSMutableArray new];
            [RequestTools RequestWithURL:[NSString stringWithFormat:@"%@/schools?pageSize=999",LocalIP] Method:@"get" Params:nil Message:@"" Success:^(NSDictionary *result) {
                NSArray *arr = result[@"responseBody"][@"result"];
                for (int i=0; i<arr.count; i++) {
                    NSDictionary *dic = arr[i];
                    [indexArray addObject:dic[@"schoolId"]];
                    [nameArray addObject:dic[@"schoolName"]];
                }
                //弹框
                self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:nameArray]];
                self.PickerOne.delegate = self;
                self.PickerOne.rightBtnTitle = @"确认";
                self.PickerOne.backgroundColor = [UIColor whiteColor];
                self.PickerOne.index = ^(int index){
                    cell.content.text = nameArray[index];
                    teachVC.entity.userSchool = nameArray[index];
                    [response setObject:indexArray[index] forKey:@"schoolId"];
                };
                [self.view addSubview:self.PickerOne];
                [self.PickerOne show];
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];
        }else if (indexPath.row == 2){
            NSArray *array = @[@"全日制",@"非全日制"];
            NSArray *content = @[@"full_time",@"part_time"];
            self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:array]];
            self.PickerOne.delegate = self;
            self.PickerOne.rightBtnTitle = @"确认";
            self.PickerOne.backgroundColor = [UIColor whiteColor];
            self.PickerOne.index = ^(int index){
                cell.content.text = array[index];
                teachVC.entity.userTitleType = array[index];
                [response setObject:content[index] forKey:@"educationalSystem"];
            };
            [self.view addSubview:self.PickerOne];
            [self.PickerOne show];
        }else if (indexPath.row == 3){
            NSArray *array = @[@"专业型",@"科学型"];
            NSArray *content = @[@"specialty",@"sience"];
            self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:array]];
            self.PickerOne.delegate = self;
            self.PickerOne.rightBtnTitle = @"确认";
            self.PickerOne.backgroundColor = [UIColor whiteColor];
            self.PickerOne.index = ^(int index){
                cell.content.text = array[index];
                teachVC.entity.userTitleType = array[index];
                [response setObject:content[index] forKey:@"degreeType"];
            };
            [self.view addSubview:self.PickerOne];
            [self.PickerOne show];
        }else if (indexPath.row == 4){
            NSArray *array = @[@"学士",@"硕士",@"博士",@"博士后"];
            NSArray *content = @[@"bachelor",@"master",@"doctorate",@"post_doctorate"];
            self.PickerOne = [[WXPPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150) midArry:[NSMutableArray arrayWithArray:array]];
            self.PickerOne.delegate = self;
            self.PickerOne.rightBtnTitle = @"确认";
            self.PickerOne.backgroundColor = [UIColor whiteColor];
            self.PickerOne.index = ^(int index){
                cell.content.text = array[index];
                teachVC.entity.userTitleType = array[index];
                [response setObject:content[index] forKey:@"degree"];
            };
            [self.view addSubview:self.PickerOne];
            [self.PickerOne show];
        }else{
            AlertView *alert = [[AlertView alloc]init];
            alert.backBlock = ^(NSString *str){
                cell.content.text = str;
                _entity.userSource = str;
                [response setObject:str forKey:@"source"];
            };
            [self.view addSubview:alert];
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AccountCell1 *cell1 = [tableView dequeueReusableCellWithIdentifier:@"account1"];
    cell1.selectionStyle = UITableViewCellSelectionStyleNone;
    cell1.title.text = titleArray[indexPath.row];
    cell1.content.text = contentArray[indexPath.row];
    return cell1;
}
@end
