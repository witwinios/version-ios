//
//  TribeMem.h
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TribeMem : NSObject
@property (strong , nonatomic) NSNumber *userId;
@property (copy, nonatomic) NSString *fullName;
@property (copy, nonatomic) NSString *email;
@property (strong, nonatomic) NSNumber *phone;
@property (copy, nonatomic) NSString *school;
@end
