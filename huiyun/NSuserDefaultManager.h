//
//  NSuserDefaultManager.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountEntity.h"
@interface NSuserDefaultManager : NSObject
@property(strong, nonatomic) NSUserDefaults *userDefaults;


+ (void)saveHeadImage:(UIImage *)image byAccount:(NSNumber *)account;
+ (UIImage *)readHeadImageByAccount:(NSNumber *)account;

+ (id)share;
/**
 保存当前登录账户
 */
- (void)saveCurrentUser:(PersonEntity *)person;
/**
 读取当前登录账户
 */
- (PersonEntity *)readCurrentUser;
/**
 保存上次登录账户
 */
- (void)saveLastAccount:(PersonEntity *)person;
/**
 读取上次保存的账户
 */
- (PersonEntity *)readLastAccount;

/**
 保存扫描医院信息
 */
- (void)saveHospital:(HospitalEntity *)entity;
/**
 读取医院信息
 */
- (HospitalEntity *)readHospital;
//保存当前账户
- (void)saveCurrentAccount:(AccountEntity *)entity;
//读取当前账户
- (AccountEntity *)readCurrentAccount;
@end

