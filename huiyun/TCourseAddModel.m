//
//  TCourseAddModel.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TCourseAddModel.h"

@implementation TCourseAddModel



-(void)setPlanName:(NSString *)PlanName{
    if ([PlanName isKindOfClass:[NSNull class]]) {
        _PlanName = @"暂无";
    }else{
        _PlanName = PlanName;
    }
}

-(void)setPlanNameId:(NSNumber *)PlanNameId{
    if ([PlanNameId isKindOfClass:[NSNull class]]) {
        _PlanNameId = [NSNumber numberWithInt:0];
    }else{
        _PlanNameId = PlanNameId;
    }
}

-(void)setPlanClassify:(NSString *)PlanClassify{
    if ([PlanClassify isKindOfClass:[NSNull class]]) {
        _PlanClassify = @"暂无";
    }else{
        _PlanClassify = PlanClassify;
    }
}

-(void)setPlanClassifyId:(NSNumber *)PlanClassifyId{
    if ([PlanClassifyId isKindOfClass:[NSNull class]]) {
        _PlanClassifyId = [NSNumber numberWithInt:0];
    }else{
        _PlanClassifyId = PlanClassifyId;
    }
}

-(void)setPlanCourse:(NSString *)PlanCourse{
    if ([PlanCourse isKindOfClass:[NSNull class]]) {
        _PlanCourse = @"暂无";
    }else{
        _PlanCourse = PlanCourse;
    }
}

-(void)setPlanCourseId:(NSNumber *)PlanCourseId{
    if ([PlanCourseId isKindOfClass:[NSNull class]]) {
        _PlanCourseId = [NSNumber numberWithInt:0];
    }else{
        _PlanCourseId = PlanCourseId;
    }
}

-(void)setPlanOwner:(NSString *)PlanOwner{
    if ([PlanOwner isKindOfClass:[NSNull class]]) {
        _PlanOwner = @"暂无";
    }else{
        _PlanOwner = PlanOwner;
    }
}

-(void)setPlanOwnerId:(NSNumber *)PlanOwnerId{
    if ([PlanOwnerId isKindOfClass:[NSNull class]]) {
        _PlanOwnerId = [NSNumber numberWithInt:0];
    }else{
        _PlanOwnerId = PlanOwnerId;
    }
}

-(void)setPlanGenre:(NSString *)PlanGenre{
    if ([PlanGenre isKindOfClass:[NSNull class]]) {
        _PlanGenre = @"暂无";
    }else if ([PlanGenre isEqualToString:@"for_classroom_course"]){
        _PlanGenre = @"课堂课程";
    }else if ([PlanGenre isEqualToString:@"for_online_course"]){
        _PlanGenre = @"在线课程";
    }else if ([PlanGenre isEqualToString:@"for_operating_course"]){
        _PlanGenre = @"实操课程";
    }else if([PlanGenre isEqualToString:@"for_teaching_rounds"]){
        _PlanGenre=@"教学查房";
    }else if([PlanGenre isEqualToString:@"for_case_discussion"]){
        _PlanGenre=@"病例讨论";
    }
}

-(void)setPlanGenreId:(NSString *)planGenreId{
    if ([planGenreId isKindOfClass:[NSNull class]]) {
        _planGenreId = @"暂无";
    }else{
        _planGenreId = planGenreId;
    }
}

-(void)setPlanDescription:(NSString *)PlanDescription{
    if ([PlanDescription isKindOfClass:[NSNull class]]) {
        _PlanDescription = @"暂无";
    }else{
        _PlanDescription = PlanDescription;
    }
}

-(void)setPlanPublic:(NSNumber *)PlanPublic{
    if ([PlanPublic isKindOfClass:[NSNull class]]) {
        _PlanPublic = [NSNumber numberWithInt:0];
    }else{
        _PlanPublic = PlanPublic;
    }
}

-(void)setPlanCourseStatus:(NSString *)PlanCourseStatus{
    if ([PlanCourseStatus isKindOfClass:[NSNull class]]) {
        _PlanCourseStatus = @"暂无";
    }else{
        _PlanCourseStatus = PlanCourseStatus;
    }
}


@end
