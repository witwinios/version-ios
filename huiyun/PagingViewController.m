//
//  PagingViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/11/28.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "PagingViewController.h"
#import "TitleButton.h"
#import "TCourseAddViewController.h"
#import "AddTeachingPlanViewController.h"

@interface PagingViewController ()<UIPageViewControllerDelegate,UIPageViewControllerDataSource,UIScrollViewDelegate>{
    UIScrollView *scrollView;
    
    UIView *titlesView;
    TitleButton *titleButton;

    UIImageView *backNavigation;
    UIButton *TitleBtnA;
    UIButton *TitleBtnB;
    

  
}
@property(nonatomic,weak)TitleButton *previousClickedTitleButton;//上一个点击的按钮
@property(nonatomic,strong) UIPageViewController *pageViewController;
@property(nonatomic,strong) NSArray *willDisPlayVCS;
@end

@implementation PagingViewController


//所有的内容控制器
-(NSArray *)willDisPlayVCS{
    
    if (_willDisPlayVCS == nil) {
        TCourseAddViewController *TCourseAdd=[[TCourseAddViewController alloc]init];
        AddTeachingPlanViewController *AddTeachingPlan= [[AddTeachingPlanViewController alloc]init];
        
        _willDisPlayVCS=@[AddTeachingPlan,TCourseAdd];
    }
    return _willDisPlayVCS;
    
}


//pageViewController
-(UIPageViewController *)pageViewController{
    if (_pageViewController == nil) {
        _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:@{UIPageViewControllerOptionInterPageSpacingKey:@(0)}];
        
   
        
        //设置数据源和代理
        _pageViewController.dataSource = self;
        _pageViewController.delegate = self;
        
        //设置内容控制器
        [_pageViewController setViewControllers:@[self.willDisPlayVCS[0]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    }
    return _pageViewController;
}



- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self setTitlesView];
    //导航
    [self setNav];
    
   //标题
    
    CGFloat page_Y=backNavigation.wwy_height + titlesView.wwy_height;
    self.pageViewController.view.frame=CGRectMake(0, page_Y, Swidth, Sheight-page_Y);
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    
    
    scrollView.delegate=self;
    
}

#pragma mark 导航栏：
- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"创建课程安排";
    [backNavigation addSubview:titleLab];
    
    
    
//    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    rightBtn.frame = CGRectMake(self.view.frame.size.width-80, 29.5, 100, 25);
//    [rightBtn setTitle:@"完成" forState:UIControlStateNormal];
//    
//    [rightBtn addTarget:self action:@selector(Complete:) forControlEvents:UIControlEventTouchUpInside];
//    [backNavigation addSubview:rightBtn];
  
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)Complete:(NSUInteger *)index{
    
   
    NSLog(@"%lu",self.willDisPlayVCS.count);
    
    
    
    
}



#pragma mark 分页控件：


#pragma mark 标题栏：
-(void)setTitlesView{
    
    titlesView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 44)];
    titlesView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    
    TitleBtnA=[UIButton buttonWithType:UIButtonTypeCustom];
    TitleBtnA.frame=CGRectMake(0, 6, Swidth/2, 32);
    [TitleBtnA setTitle:@"选取教案创建" forState:UIControlStateNormal];
    [TitleBtnA setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    TitleBtnB=[UIButton buttonWithType:UIButtonTypeCustom];
    TitleBtnB.frame=CGRectMake(Swidth/2, 6, Swidth/2, 32);
    [TitleBtnB setTitle:@"新增教案创建" forState:UIControlStateNormal];
    
    
    [titlesView addSubview:TitleBtnA];
    [titlesView addSubview:TitleBtnB];
    [self.view addSubview:titlesView];
    
    
    
}

#pragma mark  标题栏按钮：



#pragma mark 标题点击监听；


#pragma mark -UIPageViewControllerDataSource
//设置上一页内容控制器
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    //获取当前viewController的索引
      NSInteger Index = [self.willDisPlayVCS indexOfObject:viewController];

    
    NSLog(@"A:%lu",Index);
    
    [TitleBtnA setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    [TitleBtnB setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    return Index ? self.willDisPlayVCS[Index - 1] : nil;
    
    
}

//设置下一页内容控制器
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    //获取当前viewController的索引
     NSInteger Index = [self.willDisPlayVCS indexOfObject:viewController];

    NSLog(@"B:%lu",Index);
    
    [TitleBtnA setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [TitleBtnB setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
    
    return Index == self.willDisPlayVCS.count - 1 ? nil : self.willDisPlayVCS[Index + 1];
}




@end
