//
//  UpdateLeaveController.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/11.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryLeaveModel.h"
#import "UpdateLeaveDetailController.h"
#import "AbsenceItem.h"
#import "CellStyle1.h"
#import "CellStyle2.h"
#import "CellStyle3.h"
@interface UpdateLeaveController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) HistoryLeaveModel *model;
@end
