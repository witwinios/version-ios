//
//  TrainingAidsDetailsViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/11.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseDatailCell.h"
#import "DesCell.h"
#import "TeachingAidsModel.h"
#import "FileViewController.h"
@interface TrainingAidsDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)TeachingAidsModel *Model;


@end
