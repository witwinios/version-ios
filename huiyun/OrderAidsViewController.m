//
//  OrderAidsViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "OrderAidsViewController.h"
#import "TCourseDatailCell.h"
#import "TextFiledCell.h"

@interface OrderAidsViewController ()
{
    UIButton *rightBtn;
    
    UITableView *courseTable;

    //第一部分Cell
    NSArray *titleArray;
  //  NSArray *contentArray;
    //第二部分cell
    NSArray *TitleCellArray;
 //   NSArray *CellcontentArray;
    
    NSString *TextFieldNum;
    UITextField *MyTextField;
    int Num;
    int TextNum;
    
}



@end

@implementation OrderAidsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setNav];
    
    [self setUI];
}

- (void)setNav{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"预约教具";
    [backNavigation addSubview:titleLab];
    

    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [rightBtn setTitle:@" 完成" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(saveStudent:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveStudent:(UIButton *)sender{
    
    NSString *Num=[NSString stringWithFormat:@"%@",_Repertory];
    if([Num isEqualToString:@"0"]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"教具可预约数量为零，不可预约！" preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
           
            
        }]];
     
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        
    }else{
        NSString *URL=[NSString stringWithFormat:@"%@/courseSchedules/%@/trainingAidItems/appointment",LocalIP,_scheduleIdString];
        
        NSDictionary *params=@{
                               @"modelId":_ModelId,
                               @"amount":TextFieldNum,
                               };
        
        NSLog(@"%@",_ModelId);
        NSLog(@"%@",TextFieldNum);
        
        
        [RequestTools RequestWithURL:URL Method:@"post" Params:params Success:^(NSDictionary *result) {
            NSLog(@"result:%@",result);
            
            [MBProgressHUD showToastAndMessage:@"教具预约成功!" places:0 toView:nil];
            [self.navigationController popViewControllerAnimated:YES];
            
        } failed:^(NSString *result) {
            NSLog(@"result:%@",result);
            if ([result isEqualToString:@"200"]) {
                [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            }else{
                [MBProgressHUD showToastAndMessage:@"预约错误!" places:0 toView:nil];
            }
        }];
    }
 
    
}

-(void)setUI{
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStyleGrouped];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [courseTable registerNib:[UINib nibWithNibName:@"TCourseDatailCell" bundle:nil] forCellReuseIdentifier:@"cellStyle1"];
    [courseTable registerNib:[UINib nibWithNibName:@"TextFiledCell" bundle:nil] forCellReuseIdentifier:@"cellStyle2"];
    
    [self.view addSubview:courseTable];
    
   // contentArray=[NSMutableArray array];
    

    
    //第一组Title：
    titleArray = @[@"教具名称:",@"库存数量:",@"现有可用库存:",@"预约数量:"];

    //第二组Title：
    TitleCellArray=@[@"查看使用情况:"];
    
    

    
}

//返回组
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

//每组个数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section ==0) {
        return 4;
    }else {
        return 1;
    }
}

//每行高：
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.f;
}

//每组头高
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//
//    if(section == 1){
//        return 0.5;
//    }
//    return 0;
//}

//点击行事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1){
        [MBProgressHUD showToastAndMessage:@"该功能正在研发" places:0 toView:nil];
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {

       if(indexPath.row == 0){
           
            TCourseDatailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
            cell.title.text=titleArray[indexPath.row];
            cell.RightImg.hidden=YES;
            cell.content.text=_AidsName;
            return cell;
            
        }else if(indexPath.row == 1){
            TCourseDatailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
            cell.title.text=titleArray[indexPath.row];
            cell.RightImg.hidden=YES;
            cell.content.text=[NSString stringWithFormat:@"%@",_Inventory];
            return cell;
        }else if(indexPath.row == 2){
            TCourseDatailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
            cell.title.text=titleArray[indexPath.row];
            cell.RightImg.hidden=YES;
            cell.content.text=[NSString stringWithFormat:@"%@",_Repertory];
            return cell;
        }else{
                TextFiledCell *cell2=[tableView dequeueReusableCellWithIdentifier:@"cellStyle2"];
                cell2.FiledName.text = titleArray[indexPath.row];
            
            cell2.TextFiled.clearButtonMode = UITextFieldViewModeWhileEditing;
            cell2.TextFiled.clearsOnBeginEditing = YES;
            cell2.TextFiled.keyboardType=UIKeyboardTypeNumberPad;
            cell2.TextFiled.returnKeyType=UIReturnKeyDefault;
            cell2.TextFiled.delegate = self;
            
         //   int Num1=[_Subscribe intValue];
            int Num2=[_Repertory intValue];
            Num=Num2;
            
            
            
            UITapGestureRecognizer *myTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollTap:)];
            [courseTable addGestureRecognizer:myTap];
            
            
             cell2.TextFiled.placeholder=[NSString stringWithFormat:@"请入输入小于 %d 的库存数量",Num2];
            MyTextField=cell2.TextFiled;
            [cell2.TextFiled addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
                return cell2;
        }

    }else{

        TCourseDatailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.title.text=TitleCellArray[indexPath.row];
        
        return cell;

    }

    
}
//收回键盘

- (void)scrollTap:(id)sender {
    
    [courseTable endEditing:YES];
    
}



//监控输入
-(void)ApplyTextDidChange:(UITextField *)theTextField{
    
    TextFieldNum=theTextField.text;
    
    NSLog( @"text changed: %@", theTextField.text);
    
    TextNum=[theTextField.text intValue];
    
    if(TextNum > Num){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"提示" message:@"预约数大于最大预约数，请重新输入" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            theTextField.layer.borderColor=[UIColor redColor].CGColor;
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
 
}


@end
