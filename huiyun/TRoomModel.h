//
//  TRoomModel.h
//  yun
//
//  Created by MacAir on 2017/7/17.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRoomModel : NSObject
@property (strong, nonatomic) NSNumber *TRoomId;
@property (strong, nonatomic) NSString *TRoomName;
@property (strong, nonatomic) NSString *TRoomType;
//容量
@property (strong, nonatomic) NSNumber *TRoomCapacity;
@end
