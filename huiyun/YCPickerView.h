//
//  YCPickerView.h
//  xiaoyun
//
//  Created by MacAir on 17/2/24.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//
#import <UIKit/UIKit.h>

typedef void (^MyBasicBlock)(id result);

@interface YCPickerView : UIView

@property (retain, nonatomic) NSArray *arrPickerData;
@property (retain, nonatomic) UIPickerView *pickerView;
@property (nonatomic, copy) MyBasicBlock selectBlock;
- (void)popPickerView;
@end
