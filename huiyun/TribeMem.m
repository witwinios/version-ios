//
//  TribeMem.m
//  yun
//
//  Created by MacAir on 2017/5/25.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "TribeMem.h"

@implementation TribeMem
- (void)setPhone:(NSNumber *)phone{
    if ([phone isKindOfClass:[NSNull class]]) {
        _phone = @0;
    }else{
        _phone = phone;
    }
}
- (void)setSchool:(NSString *)school{
    if ([school isKindOfClass:[NSNull class]]) {
        _school = @"暂无";
    }else{
        _school = school;
    }
}
- (void)setFullName:(NSString *)fullName{
    if ([fullName isKindOfClass:[NSNull class]]) {
        _fullName = @"暂无";
    }else{
        _fullName = fullName;
    }
}
- (void)setEmail:(NSString *)email{
    if ([email isKindOfClass:[NSNull class]]) {
        _email = @"暂无";
    }else{
        _email = email;
    }
}
@end
