//
//  PaperCellModel.h
//  xiaoyun
//
//  Created by MacAir on 17/2/9.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuestionModel.h"
@interface PaperCellModel : NSObject
@property (strong, nonatomic) NSString *creatTime;
@property (strong, nonatomic) NSNumber *paperID;
@property (strong, nonatomic) NSNumber *recordID;
@property (strong, nonatomic) NSString *paperSubject;
@property (strong, nonatomic) NSString *paperCategory;
@property (strong, nonatomic) NSNumber *totalScore;
@property (strong, nonatomic) NSString *paperName;
@property (strong, nonatomic) NSString *paperDes;
@property (strong, nonatomic) NSString *paperStatus;
@property (strong, nonatomic) NSNumber *getScore;
@end
