//
//  DetailCellOne.h
//  huiyun
//
//  Created by MacAir on 2017/9/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCellOne : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@end
