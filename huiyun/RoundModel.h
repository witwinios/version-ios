//
//  RoundModel.h
//  huiyun
//
//  Created by MacAir on 2017/9/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoundModel : NSObject

@property (strong, nonatomic) NSString *departmentName;//科室名
@property (strong, nonatomic) NSString *recordName;//记录名
@property (strong, nonatomic) NSNumber *recordPass;//通过数
@property (strong, nonatomic) NSNumber *recordRefer;//未通过数
@property (strong, nonatomic) NSNumber *recordPend;//待审核数

@property (strong, nonatomic) NSString *caseStudentName;//病人姓名
@property (strong, nonatomic) NSNumber *caseNums;//病案号
@property (strong, nonatomic) NSString *caseTeacher;//带教老师
@property (strong, nonatomic) NSNumber *caseTime;//带教时间
@property (strong, nonatomic) NSString *caseStatus;//审核状态

@end
