//
//  SearchUI.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/10.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "SearchUI.h"
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height
@implementation SearchUI

- (instancetype)initWithCurrentvc:(UIViewController *)currentVC
{
    self = [super init];
    if (self) {
        //实例化UI
        self = [[[NSBundle mainBundle] loadNibNamed:@"SearchUI" owner:self options:nil] lastObject];
        self.frame = CGRectMake(0, HEIGHT, WIDTH, HEIGHT-64);
        [currentVC.view addSubview:self];
        self.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
        //设置UI样式
        _startBtn.layer.cornerRadius = 5;
        _startBtn.layer.masksToBounds = YES;
        _startBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
        _startBtn.backgroundColor = [UIColor whiteColor];
        _startBtn.layer.borderWidth = 2;
        _startBtn.layer.borderColor = [UIColor blackColor].CGColor;
        _endBtn.layer.cornerRadius = 5;
        _endBtn.layer.masksToBounds = YES;
        _endBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
        _endBtn.backgroundColor = [UIColor whiteColor];
        _endBtn.layer.borderWidth = 2;
        _endBtn.layer.borderColor = [UIColor blackColor].CGColor;
        [_startBtn setTitle:@"" forState:0];
        [_endBtn setTitle:@"" forState:0];
        _SCView.backgroundColor = UIColorFromHex(0x20B2AA);
        _pickView.backgroundColor = [UIColor lightGrayColor];
        _SCView1.backgroundColor = UIColorFromHex(0x20B2AA);
        //初始化控件
        [_datePick  setTimeZone:[NSTimeZone localTimeZone]];
        [_datePick setDate:[NSDate date]];
        _datePick.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_ch"];
        _datePick.timeZone = [NSTimeZone timeZoneWithName:@"GTM+8"];
        [_datePick setDatePickerMode:UIDatePickerModeDateAndTime];
        //设置开始时间呗选定
        [_startBtn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
        _startBtn.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
        
        _currentController = currentVC;
        _isShowSelf = [NSNumber numberWithInt:0];
        _isShowPick = [NSNumber numberWithInt:1];
        //设置开始时间被选定
        _currentBtn = [NSNumber numberWithInt:0];

    }
    return self;
}
//控制视图
- (void)showInVC{
    self.startBtn.titleLabel.text = @"";
    self.endBtn.titleLabel.text = @"";
    [self.startBtn setTitle:@"" forState:0];
    [self.endBtn setTitle:@"" forState:0];
    _isShowSelf = [NSNumber numberWithInt:1];
    [UIView animateWithDuration:0.2 animations:^{
        self.frame = CGRectMake(0, 64, WIDTH, HEIGHT-64);
    }];
}
- (void)hideInVC{
    _isShowSelf = [NSNumber numberWithInt:0];
    [UIView animateWithDuration:0.2 animations:^{
        self.frame = CGRectMake(0, HEIGHT, WIDTH, HEIGHT-64);
    }];
}
- (void)showDatePick{
    _isShowPick = [NSNumber numberWithInt:1];
    [UIView animateWithDuration:0.5 animations:^{
        self.pickView.frame = CGRectMake(0,HEIGHT-314, self.pickView.frame.size.width, self.pickView.frame.size.height);
    }];
}
- (void)hideDatePick{
    _isShowPick = [NSNumber numberWithInt:0];
    [UIView animateWithDuration:0.5 animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, self.pickView.frame.size.width, self.pickView.frame.size.height);
    }];
}
- (IBAction)cancelAction:(id)sender {
    [self hideInVC];
}

- (IBAction)sureAction:(id)sender {
    
    NSLog(@"%@---%@",_startBtn.titleLabel.text,_endBtn.titleLabel.text);
    
    //传值
    if ([_startBtn.titleLabel.text isEqualToString:@""] || [_endBtn.titleLabel.text isEqualToString:@""]) {
        [Maneger showAlert:@"时间错误!!" andCurentVC:_currentController];
    }else if ([_startBtn.titleLabel.text compare:_endBtn.titleLabel.text] == 1){
        [Maneger showAlert:@"时间错误!" andCurentVC:_currentController];
    }else{
        [self hideInVC];
        self.responseBlock(self.startBtn.titleLabel.text, self.endBtn.titleLabel.text);
    }
}

- (IBAction)dateCancelAction:(id)sender {
    [self hideDatePick];
}

- (IBAction)dateSureAction:(id)sender {
    NSString *strDate = [[Maneger shareObject] dateTolineString:self.datePick.date];
    if (_currentBtn == [NSNumber numberWithInt:0]) {
        [self.startBtn setTitle:strDate forState:UIControlStateNormal];
    }else{
        [self.endBtn setTitle:strDate forState:UIControlStateNormal];
    }
    [self hideDatePick];
}

- (IBAction)startAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (_isShowPick == [NSNumber numberWithInt:0]) {
        [self showDatePick];
    }else{
        [self hideDatePick];
    }
    if (_currentBtn != [NSNumber numberWithInt:0]) {
        _currentBtn = [NSNumber numberWithInt:0];
        [_endBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _endBtn.layer.borderColor = [UIColor blackColor].CGColor;
        
        [btn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
        btn.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
    }
}

- (IBAction)endAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (_isShowPick == [NSNumber numberWithInt:0]) {
        [self showDatePick];
    }else{
        [self hideDatePick];
    }
    if (_currentBtn != [NSNumber numberWithInt:1]) {
        _currentBtn = [NSNumber numberWithInt:1];
        [_startBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _startBtn.layer.borderColor = [UIColor blackColor].CGColor;
        
        [btn setTitleColor:UIColorFromHex(0x20B2AA) forState:UIControlStateNormal];
        btn.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
    }
}
@end
