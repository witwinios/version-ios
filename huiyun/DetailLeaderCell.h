//
//  DetailLeaderCell.h
//  huiyun
//
//  Created by MacAir on 2018/1/17.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailLeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UITextView *contentText;

@end
