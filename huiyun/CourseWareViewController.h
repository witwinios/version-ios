//
//  CourseWareViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/2.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseAddModel.h"
#import "CourseQuestionBView.h"
@interface CourseWareViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic)TCourseAddModel *PlanModel;


@property(nonatomic,getter=isEditing) BOOL editing;

-(void)setEditing:(BOOL)editing animated:(BOOL)animated;
@end
