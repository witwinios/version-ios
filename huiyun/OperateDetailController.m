//
//  OperateDetailController.m
//  huiyun
//
//  Created by MacAir on 2017/9/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "OperateDetailController.h"

@interface OperateDetailController ()
{
    NSArray *heightArr;
    NSArray *dataArray;
    NSArray *titleArray;
    
    
    UITextView *currentTextview;
    CGRect _activedTextFieldRect;
    CGPoint currentPoint;
    UIView *toolbar;
    
    PersonEntity *persion;
}
@end

@implementation OperateDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    //注册键盘出现的通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];
    
    
    
    persion = [[NSuserDefaultManager share] readCurrentUser];
    
    heightArr = @[@"44",@"44",@"44",@"44",@"44",@"44",@"103",@"103",@"103",@"44"];
    dataArray = @[_model.caseStatus,_model.caseResult,_model.casePatientName,[NSString stringWithFormat:@"%@",_model.caseNo],[[Maneger shareObject]timeFormatter1:_model.caseDate.stringValue],_model.caseTeacher,_model.caseReason,_model.caseDescription,_model.caseAdvice,@"查看"];
    
    if (_model.caseTeacher.length == 0) {
        _model.caseTeacher=@"暂无";
    }
    
    
    titleArray = @[@"审核状态:",@"审核结果:",@"病人姓名:",@"病案号:",@"操作时间",@"上级教师:",@"*成功/失败原因:",@"描述:",@"指导意见:",@"附件:"];
    
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if ([self.model.caseStatus isEqualToString:@"待审核"] && persion.roleId.intValue != 2) {
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rightBtn.frame = CGRectMake(Swidth-70, 29.5, 70, 25);
        [rightBtn setTitle:@"修改" forState:0];
        [rightBtn setTitleColor:[UIColor whiteColor] forState:0];
        [rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if (![_SDepartModel.SDepartStatus isEqualToString:@"出科"]) {
            [backNavigation addSubview:rightBtn];
        }

    }

    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"操作记录";
    [backNavigation addSubview:titleLab];
    _tableView.bounces = NO;
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [_tableView registerNib:[UINib nibWithNibName:@"DetailCellOne" bundle:nil] forCellReuseIdentifier:@"one"];
    [_tableView registerNib:[UINib nibWithNibName:@"DetailCellTwo" bundle:nil] forCellReuseIdentifier:@"two"];
    [_tableView registerNib:[UINib nibWithNibName:@"DetailCellThree" bundle:nil] forCellReuseIdentifier:@"three"];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
}
#pragma -action

- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)selectAction:(UIButton *)btn{
    [SubjectTool share].subjectType = subjectTypeUpdate;
    OperateRecordController *recordVC = [OperateRecordController new];
    recordVC.myModel = self.model;
    [self.navigationController pushViewController:recordVC animated:YES];
}
- (void)hideKb:(UIButton *)btn{
    [currentTextview resignFirstResponder];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
        self.tableView.contentOffset = currentPoint;
    }];
}
- (void)keyboardWillShow:(NSNotification *)notification {
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
    if ((_activedTextFieldRect.origin.y + _activedTextFieldRect.size.height) + 40 >  ([UIScreen mainScreen].bounds.size.height - rect.size.height)) {//键盘的高度 高于textView的高度 需要滚动
        currentPoint = self.tableView.contentOffset;
        [UIView animateWithDuration:duration animations:^{
            self.tableView.contentOffset = CGPointMake(0, 64 + _activedTextFieldRect.origin.y + _activedTextFieldRect.size.height - ([UIScreen mainScreen].bounds.size.height - rect.size.height));
        }];
    }
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    _activedTextFieldRect = [textView convertRect:textView.frame toView:_tableView];

    currentTextview = textView;
    return YES;
}

#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (persion.roleId.intValue == 2 && [_model.caseStatus isEqualToString:@"待审核"]) {
        return 9;
    }
    return 10;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *foot = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 40)];
    foot.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIButton *approveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    approveBtn.frame = CGRectMake(0, 0, Swidth/2, 40);
    approveBtn.backgroundColor = UIColorFromHex(0x20B2AA);
    [approveBtn setTitle:@"通过" forState:0];
    [approveBtn addTarget:self action:@selector(approveAction:) forControlEvents:UIControlEventTouchUpInside];
    [foot addSubview:approveBtn];
    
    UIButton *rejectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rejectBtn.frame = CGRectMake(Swidth/2, 0, Swidth/2, 40);
    rejectBtn.backgroundColor = [UIColor redColor];
    [rejectBtn setTitle:@"未通过" forState:0];
    [rejectBtn addTarget:self action:@selector(rejectAction:) forControlEvents:UIControlEventTouchUpInside];
    [foot addSubview:rejectBtn];
    return foot;
}
- (void)approveAction:(UIButton *)appBtn{
    
    if ([currentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"请输入指导意见~" places:0 toView:nil];
        return;
    }
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认通过?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentOperationRecords/%@/review",SimpleIp,_model.recordId];

        [RequestTools RequestWithURL:request Method:@"PUT" Params:@{@"reviewerId":persion.userID,@"reviewerComments":currentTextview.text,@"reviewStatus":@"approved"} Message:@"审批中..." Success:^(NSDictionary *result) {
    
            [MBProgressHUD showToastAndMessage:@"审批成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"审批失败~" places:0 toView:nil];
        }];
        
    }];
    [alertVC addAction:cancel];
    [alertVC addAction:sure];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}

- (void)rejectAction:(UIButton *)rejBtn{
    if ([currentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"请输入指导意见~" places:0 toView:nil];
        return;
    }

    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"确认未通过?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentOperationRecords/%@/review",SimpleIp,_model.recordId];
        
        [RequestTools RequestWithURL:request Method:@"PUT" Params:@{@"reviewerId":persion.userID,@"reviewerComments":currentTextview.text,@"reviewStatus":@"rejected"} Message:@"审批中..." Success:^(NSDictionary *result) {
            
            [MBProgressHUD showToastAndMessage:@"审批成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"审批失败~" places:0 toView:nil];
        }];
    }];
    [alertVC addAction:cancel];
    [alertVC addAction:sure];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (persion.roleId.intValue == 2 && [_model.caseStatus isEqualToString:@"待审核"]) {
        return 40.f;
    }
    return 0.f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [heightArr[indexPath.row] floatValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DetailCellOne *oneCell = [tableView dequeueReusableCellWithIdentifier:@"one"];
    DetailCellTwo *twoCell = [tableView dequeueReusableCellWithIdentifier:@"two"];
    DetailCellThree *threeCell = [tableView dequeueReusableCellWithIdentifier:@"three"];
    
   
    
    if (indexPath.row == 9) {
        threeCell.titleLab.text = titleArray[indexPath.row];
        [threeCell.contentBtn setTitle:dataArray[indexPath.row] forState:0];
        [threeCell.contentBtn addTarget:self action:@selector(imageAction:) forControlEvents:UIControlEventTouchUpInside];
        return threeCell;
    }else if (indexPath.row == 8 || indexPath.row == 7 || indexPath.row == 6){
        currentTextview = twoCell.contentText;
        if (indexPath.row == 8) {
           twoCell.contentText.userInteractionEnabled=YES;
            if (persion.roleId.intValue == 2) {
                [twoCell.contentText setEditable:YES];
                currentTextview.delegate = self;
            }else{
                [twoCell.contentText setEditable:YES];
            }
        }else{
            [twoCell.contentText setEditable:NO];
        }
        twoCell.titleText.text = titleArray[indexPath.row];
        twoCell.contentText.text = dataArray[indexPath.row];
        return twoCell;
    }else{
        oneCell.titleLab.text = titleArray[indexPath.row];
        oneCell.contentLab.text = dataArray[indexPath.row];
        return oneCell;
    }
    return twoCell;
}
- (void)imageAction:(UIButton *)img{
    NSLog(@"fileUrl==%@",self.model.fileUrl);
    if (self.model.fileUrl.length > 0) {
        NSString *imgStr = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",SimpleIp,self.model.fileUrl,[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
        NSString *imageStr =  [imgStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"%@",imgStr);
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
        UIColor *color = [UIColor lightGrayColor];
        view.backgroundColor = [color colorWithAlphaComponent:0.5];
        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 300 ,300)];
        imgView.center = CGPointMake(Swidth/2, Sheight/2);
        imgView.layer.cornerRadius = 5;
        imgView.layer.masksToBounds = YES;
        imgView.center = view.center;
        [view addSubview:imgView];
        
        __block UIProgressView *pv;
        __weak UIImageView *weakImageView = imgView;
        [imgView sd_setImageWithURL:[NSURL URLWithString:imageStr]
                   placeholderImage:[UIImage imageNamed:@"holdimage"]
                            options:SDWebImageCacheMemoryOnly
                           progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                               if (!pv) {
                                   [weakImageView addSubview:pv = [UIProgressView.alloc initWithProgressViewStyle:UIProgressViewStyleDefault]];
                                   pv.frame = CGRectMake(0, 0, Swidth/2, 40);
                               }
                               CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 8.0f);
                               pv.transform = transform;
                               float showProgress = (float)receivedSize/(float)expectedSize;
                               [pv setProgress:showProgress];
                           }
                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                              [pv removeFromSuperview];
                              pv = nil;
                          }];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeAction:)];
        tap.numberOfTapsRequired = 1;
        [view addGestureRecognizer:tap];
        
        [self.view addSubview:view];
    }else{
        [MBProgressHUD showToastAndMessage:@"暂无附件" places:0 toView:nil];
    }
    
    

}
- (void)removeAction:(UITapGestureRecognizer *)tap{
    UIView *view = tap.view;
    [view removeFromSuperview];
}

@end
