//
//  BaseController.h
//  TestParrent
//
//  Created by MacAir on 2018/1/2.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseController : UIViewController
/*
 params int
 0 左
 1 中
 2 右
 */
typedef void (^BarBlock)(int);

@property (strong, nonatomic) BarBlock barBC;

@property (strong, nonatomic) UIView *navibarView;
@property (strong, nonatomic) UIView *screenView;
@property (strong, nonatomic) UIButton *leftBtn;
@property (strong, nonatomic) UIButton *rightBtn;
@property (strong, nonatomic) UIButton *titleLab;

@end

