//
//  ErrorModel.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorModel : NSObject
@property (strong, nonatomic) NSNumber *testId;
@property (strong, nonatomic) NSString *testName;
@property (strong, nonatomic) NSString *testStatus;
@property (strong, nonatomic) NSString *testSubject;
@property (strong, nonatomic) NSNumber *testPaper;
@property (strong, nonatomic) NSString *testCategory;
@property (strong, nonatomic) NSString *testDescription;
@end
