//
//  MyAlertView.h
//  huiyun
//
//  Created by Mr.Wang on 2017/11/22.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyAlertViewDelegate<NSObject>

@optional
-(void)didClickButtonAtIndex:(NSUInteger)index password:(NSString *)password;
- (void)successPassword;
-(void)ChooseStarNum:(NSString *)ChooseStarNum;

@end




@interface MyAlertView : UIView

@property (nonatomic,strong)UIView *blackView;
@property (strong,nonatomic)UIView * alertview;
@property (strong,nonatomic)NSString * title;
@property (nonatomic,copy)NSString *contentStr;
@property (nonatomic,strong)UILabel *tipLable;
@property (weak,nonatomic) id<MyAlertViewDelegate> delegate;
@property (nonatomic,assign)NSInteger type;
@property (nonatomic,assign)NSInteger numBtn;
@property (nonatomic,copy)NSString *password;
@property (nonatomic,retain)NSArray *btnTitleArr;

-(void)initWithTitle:(NSString *) title contentStr:(NSString *)content type:(NSInteger)type btnNum:(NSInteger)btnNum btntitleArr:(NSArray *)btnTitleArr;

-(void)exChangeOut:(UIView *)changeOutView dur:(CFTimeInterval)dur;


@end
