//
//  DepartCell.m
//  huiyun
//
//  Created by MacAir on 2017/9/12.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "DepartCell.h"

@implementation DepartCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.statusBtn.layer.cornerRadius = 4;
    self.statusBtn.layer.masksToBounds = YES;
    self.statusBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.statusBtn.layer.borderWidth = 1;
    self.inTime.adjustsFontSizeToFitWidth = YES;
    self.outTime.adjustsFontSizeToFitWidth = YES;
    [self.inTime sizeToFit];
    [self.outTime sizeToFit];
}
@end
