//
//  IMController.m
//  yun
//
//  Created by MacAir on 2017/5/22.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "IMController.h"

@interface IMController ()
{
    YWConversationViewController *selfConversation;
    NSMutableArray *tribeArray;
    NSNumber *currentGroup;
    
    //
    NSString *currentDes;
}
@end

@implementation IMController
- (YWIMCore *)ywIMCore {
    return [SPKitExample sharedInstance].ywIMKit.IMCore;
}
- (id<IYWTribeService>)ywTribeService {
    return [[self ywIMCore] getTribeService];
}
//
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
//创建群
- (void)loadTribe{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/QAGroup",LocalIP];
    __weak typeof(self) weakSelf = self;
    NSDictionary *pamas = @{@"questionTitle":[[Maneger shareObject] currentTimeF],@"questionDescription":self.quesDes.text,@"userGroupIds":self.selectGroup,@"subjectIds":self.selectSubject};
    
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:pamas Message:@"创建中" Success:^(NSDictionary *response) {
        if ([[response objectForKey:@"responseBody"] isKindOfClass:[NSNull class]]) {
            [MBProgressHUD showToastAndMessage:@"该群没成员,创建失败!" places:0 toView:nil];
            return;
        }
        self.ywIMKit = [[YWAPI sharedInstance] fetchIMKitForOpenIM];
        self.tribe = [[YWTribe alloc]init];
        NSNumber *ids = [[response objectForKey:@"responseBody"] objectForKey:@"tribeId"];
        currentGroup =[[response objectForKey:@"responseBody"] objectForKey:@"qaGroupId"];
        self.tribe.tribeId = [NSString stringWithFormat:@"%@",ids];
        self.tribe.tribeName = @"群聊";
        //获取群信息
        NSArray *array = [[response objectForKey:@"responseBody"] objectForKey:@"tribeMembers"];
        for (int i=0; i<array.count; i++) {
            NSDictionary *dic = array[i];
            TribeMem *mem = [TribeMem new];
            mem.userId = [dic objectForKey:@"userId"];
            mem.fullName = [dic objectForKey:@"fullName"];
            mem.email = [dic objectForKey:@"email"];
            mem.phone = [dic objectForKey:@"personalPhoneNo"];
            mem.school = [dic objectForKey:@"schoolName"];
            [tribeArray addObject:mem];
        }
        //创建成功，清空数据
        [self.twoContent setTitle:@"" forState:0];
        [self.oneContent setTitle:@"" forState:0];
        [self.selectGroup removeAllObjects];
        [self.selectSubject removeAllObjects];
        currentDes = self.quesDes.text;
        self.quesDes.text = @"";
        [weakSelf exampleOpenConversationViewControllerWithTribe:self.tribe fromNavigationController:weakSelf.navigationController];

    } failed:^(NSString *code) {
        if ([code isEqualToString:@"500"]) {
            [MBProgressHUD showToastAndMessage:@"创建失败,请检查是否开启阿里服务!" places:0 toView:nil];
        }else if ([code isEqualToString:@"200"]){
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误,创建失败!" places:0 toView:nil];
        }
    }];
}
- (void)setUI{
    self.selectGroup = [NSMutableArray new];
    self.selectSubject = [NSMutableArray new];
    tribeArray = [NSMutableArray new];
    //标题居中
    self.biaoti.frame = CGRectMake(_navigationTView.frame.size.width/2-61.5, 32, 123, 21);
    //设置字体颜色
    NSMutableAttributedString *oneStr = [[NSMutableAttributedString alloc] initWithString:@"所属科室*:"];
    NSMutableAttributedString *threeStr = [[NSMutableAttributedString alloc] initWithString:@"问题描述*:"];
    [oneStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4,1)];
    [threeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4,1)];
    self.oneBiao.attributedText = oneStr;
    self.threeBiao.attributedText = threeStr;
    //设置控件
    self.navigationTView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    self.rightItem.layer.cornerRadius=5;
    self.rightItem.layer.masksToBounds=YES;
    self.rightItem.layer.borderColor = [UIColor whiteColor].CGColor;
    self.rightItem.layer.borderWidth = 1;
    
    self.quesDes.layer.cornerRadius = 5;
    self.quesDes.layer.masksToBounds = YES;
    self.quesDes.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.quesDes.layer.borderWidth = 1;
    self.quesDes.returnKeyType = UIReturnKeyDefault;
    self.quesDes.delegate = self;
    self.oneContent.layer.cornerRadius = 5;
    self.oneContent.layer.masksToBounds = YES;
    self.oneContent.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.oneContent.layer.borderWidth = 1;
    [self.oneContent addTarget:self action:@selector(groupAction:) forControlEvents:UIControlEventTouchUpInside];
    self.twoContent.layer.cornerRadius = 5;
    self.twoContent.layer.masksToBounds = YES;
    self.twoContent.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.twoContent.layer.borderWidth = 1;
    [self.twoContent addTarget:self action:@selector(subjectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //设置button
    self.commitBtn.layer.cornerRadius = 5;
    self.commitBtn.layer.masksToBounds = YES;
    self.commitBtn.backgroundColor = UIColorFromHex(0x20B2AA);
    self.commitBtn.center = CGPointMake(self.view.frame.size.width/2, self.backView.frame.size.height/2);
    //
    self.groupVC = [GroupController new];
    self.subjectVC = [SubjectController new];
    //
    
    //获取选择
    __weak typeof (self) weakSelf = self;
    self.groupVC.groupBlock = ^(NSArray *groupArray){
        //组装groupId
        NSMutableArray *gArray = [NSMutableArray new];
        for (int s=0; s<groupArray.count; s++) {
            NSDictionary *dictionary = groupArray[s];
            [gArray addObject:[dictionary objectForKey:@"groupID"]];
        }
        weakSelf.selectGroup = gArray;
        //
        NSString *groupFomat = @"";
        for (int s=0; s<groupArray.count; s++) {
            NSDictionary *dictionary = groupArray[s];
            groupFomat = [groupFomat stringByAppendingString:[NSString stringWithFormat:@" %@ ",[dictionary objectForKey:@"groupNAME"]]];
        }
        [weakSelf.oneContent setTitle:groupFomat forState:0];
    };
    //
    self.subjectVC.subjectBlock = ^(NSArray *subjectArray){
        NSMutableArray *sArray = [NSMutableArray new];
        for (int k =0; k<subjectArray.count; k++) {
            NSDictionary *dictionary = subjectArray[k];
            [sArray addObject:[dictionary objectForKey:@"subjectID"]];
        }
        weakSelf.selectSubject = sArray;
        NSString *subjectFomat = @"";
        for (int s=0; s<subjectArray.count; s++) {
            NSDictionary *dictionary = subjectArray[s];
            subjectFomat = [subjectFomat stringByAppendingString:[NSString stringWithFormat:@" %@ ",[dictionary objectForKey:@"subjectNAME"]]];
        }
        [weakSelf.twoContent setTitle:subjectFomat forState:0];
    };
}
#pragma -action
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.quesDes resignFirstResponder];
}
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)commitAction:(id)sender{
    if (self.selectGroup.count == 0) {
        [MBProgressHUD showToastAndMessage:@"科室不能为空!" places:0 toView:nil];
        return;
    }
    if ([self.quesDes.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"描述不能为空!" places:0 toView:nil];
        return;
    }
    [self loadTribe];
}

- (IBAction)groupAction:(id)sender {
    [self.selectGroup removeAllObjects];
    [self.oneContent setTitle:@"" forState:0];
    
    [self.navigationController pushViewController:self.groupVC animated:YES];
}

- (IBAction)subjectAction:(id)sender {
    [self.selectSubject removeAllObjects];
    [self.twoContent setTitle:@"" forState:0];
    
    [self.navigationController pushViewController:self.subjectVC animated:YES];
}

- (IBAction)quesAction:(id)sender {
    QuestionController *quesVC = [QuestionController new];
    [self.navigationController pushViewController:quesVC animated:YES];
}

- (void)exampleOpenConversationViewControllerWithTribe:(YWTribe *)aTribe fromNavigationController:(UINavigationController *)aNavigationController
{
    YWConversation *conversation = [YWTribeConversation fetchConversationByTribe:aTribe createIfNotExist:YES baseContext:self.ywIMKit.IMCore];
    [self exampleOpenConversationViewControllerWithConversation:conversation fromNavigationController:aNavigationController];
}
- (void)exampleOpenConversationViewControllerWithConversation:(YWConversation *)aConversation fromNavigationController:(UINavigationController *)aNavigationController
{
    __block YWConversationViewController *alreadyController = nil;
    [aNavigationController.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[YWConversationViewController class]]) {
            YWConversationViewController *c = obj;
            if (aConversation.conversationId && [c.conversation.conversationId isEqualToString:aConversation.conversationId]) {
                alreadyController = c;
                *stop = YES;
            }
        }
    }];
    
    if (alreadyController) {
        /// 必须判断当前是否已有该会话，如果有，则直接显示已有会话
        /// @note 目前IMSDK不允许同时存在两个相同会话的Controller
        [aNavigationController popToViewController:alreadyController animated:YES];
        [aNavigationController setNavigationBarHidden:NO];
        return;
    } else {
        YWConversationViewController *conversationController = [self.ywIMKit makeConversationViewControllerWithConversationId:aConversation.conversationId];
        selfConversation = conversationController;
        __weak typeof(conversationController) weakController = conversationController;
        [conversationController setViewWillAppearBlock:^(BOOL aAnimated) {
            weakController.view.backgroundColor = [UIColor whiteColor];
            //添加导航栏
            UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
            backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
            backNavigation.userInteractionEnabled = YES;
            [self.view addSubview:backNavigation];
            self.view.backgroundColor = [UIColor whiteColor];
            
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
            leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
            [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
            [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
            [backNavigation addSubview:leftBtn];
            
            UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
            [rightBtn setImage:[UIImage imageNamed:@"user"] forState:UIControlStateNormal];
            [rightBtn addTarget:self action:@selector(rightItemAction:) forControlEvents:UIControlEventTouchUpInside];
            [backNavigation addSubview:rightBtn];
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
            titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
            titleLab.textAlignment = 1;
            titleLab.font = [UIFont systemFontOfSize:20];
            titleLab.textColor = [UIColor whiteColor];
            titleLab.text = @"讨论组";
            [backNavigation addSubview:titleLab];
            [weakController.view addSubview:backNavigation];
            //
            UIButton *solveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            solveBtn.frame = CGRectMake(backNavigation.frame.size.width/2+60, 27, 80, 30);
            [solveBtn setTitle:@"已解决?" forState:0];
            //            solveBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
            solveBtn.layer.cornerRadius = 3;
            solveBtn.layer.masksToBounds=YES;
            solveBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            solveBtn.layer.borderWidth = 1;
            [solveBtn addTarget:self action:@selector(solveAction:) forControlEvents:UIControlEventTouchUpInside];
            [backNavigation addSubview:solveBtn];
            
            //添加textView
            UITextView *quesView = [UITextView new];
            quesView.frame = CGRectMake(0, 64, Swidth, 60);
            quesView.text = [NSString stringWithFormat:@"问题描述:%@",currentDes];
            quesView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            quesView.layer.borderWidth = 1;
            quesView.editable = NO;
            [weakController.view addSubview:quesView];
            //
            weakController.tableView.frame = CGRectMake(0, 124, Swidth, Sheight-124);
        }];
        [aNavigationController pushViewController:conversationController animated:YES];
        }
}
- (void)rightItemAction:(UIButton *)btn{
    TeacherController *teacherVC = [TeacherController new];
    teacherVC.tribeArray = tribeArray;
    //
    [self.navigationController pushViewController:teacherVC animated:YES];
}
#pragma -protocal
- (void)solveAction:(UIButton *)btn{
    UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"是否已经解决该问题并关闭讨论组?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"已解决", nil];
    [alerView show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self chageStatus];
    }
}
- (void)chageStatus{
    [MBProgressHUD showHUDAndMessage:@"解决中..." toView:nil];
    RequestTools *tool = [RequestTools new];
    [tool postRequestPrams:nil andURL:[NSString stringWithFormat:@"%@/QAGroup/%@/solved",LocalIP,currentGroup]];
    
    NSLog(@"url == %@",[NSString stringWithFormat:@"%@/QAGroup/%@/solved",LocalIP,currentGroup]);
    
    tool.errorBlock = ^(NSString *code){
        if (![code isEqualToString:@"200"]) {
            [MBProgressHUD hideHUDForView:self.view];
            [MBProgressHUD showToastAndMessage:@"失败，请重试" places:0 afterDelay:1 toView:self.view];
        }
    };
    tool.responseBlock = ^(NSDictionary *response){
        [MBProgressHUD hideHUDForView:nil];
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"成功" places:0 afterDelay:1 toView:nil];
            [NSThread sleepForTimeInterval:1];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showToastAndMessage:@"失败，请重试" places:0 afterDelay:1 toView:nil];
        }
    };
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return YES;
}
@end
