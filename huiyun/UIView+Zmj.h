//
//  UIView+WKFFrame.h
//  
//
//  Created by 郑敏捷 on 17/2/21.
//  Copyright (c) 2017年 郑敏捷. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  对于UIView 直接.语法 能拿到 下面四个属性
 */

@interface UIView (Zmj)

@property (nonatomic, assign) CGFloat width;

@property (nonatomic, assign) CGFloat height;

@property (nonatomic, assign) CGFloat x;

@property (nonatomic, assign) CGFloat y;

@end
