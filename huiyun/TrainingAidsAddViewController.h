//
//  TrainingAidsAddViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/11.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseAddModel.h"
#import "TeachingAidsCell.h"
#import "TeachingAidsModel.h"
typedef void (^BackTrainingAids) (NSArray *arr);
@interface TrainingAidsAddViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIPickerViewDataSource,UIPickerViewDelegate,UISearchBarDelegate,UISearchResultsUpdating,UITextFieldDelegate>
@property(strong,nonatomic)TCourseAddModel *PlanModel;
@property(nonatomic,getter=isEditing) BOOL editing;
@property(nonatomic,strong) NSString *str;
@property(nonatomic,strong)NSMutableArray *TempArr;
-(void)setEditing:(BOOL)editing animated:(BOOL)animated;
@property(strong,nonatomic)BackTrainingAids backArr;

@end
