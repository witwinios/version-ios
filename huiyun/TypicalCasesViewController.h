//
//  TypicalCasesViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/10.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TypicalCasesModel.h"
#import "TypicalCasesCell.h"
#import "TypicalCasesAddViewController.h"
#import "TypicalCasesDetailsViewController.h"
@interface TypicalCasesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(strong,nonatomic)TCourseAddModel *PlanModel;
@property(nonatomic,getter=isEditing) BOOL editing;

-(void)setEditing:(BOOL)editing animated:(BOOL)animated;

@end
