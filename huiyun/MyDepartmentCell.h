//
//  MyDepartmentCell.h
//  huiyun
//
//  Created by Bad on 2018/3/14.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DepartmentModel.h"
@interface MyDepartmentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *LeftImg;
@property (weak, nonatomic) IBOutlet UILabel *ContText;


-(void)setModel:(DepartmentModel *)model;

@end
