//
//  AppDelegate.h
//  FastSdkDemo
//
//  Created by jiangcj on 16/10/11.
//  Copyright © 2016年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LoginController.h"
#import "SPKitExample.h"
#import "TMainController.h"
#import "TIMController.h"
#import "MainViewController.h"
#import "IMController.h"
#import <PlayerSDK/PlayerSDK.h>
#import "TestScheduleController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,GSPPlayerManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,assign)UIInterfaceOrientationMask orientMask;

@property (strong, nonatomic) LoginController *loginVC;
@property (assign, nonatomic) long long myUserID;
@property (strong, nonatomic) NSDictionary *text2keyDic;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) UITabBarController *tabarVC;
@property (strong, nonatomic) UITabBarController *ttabarVC;

@property (strong, nonatomic) MainViewController *mainVC;
@property (strong, nonatomic) IMController *imVC;

@property (strong, nonatomic) TMainController *tMainVC;
@property (strong, nonatomic) TIMController *timVC;

@property (strong, nonatomic, readwrite) YWIMKit *ywIMKit;
@property (strong, nonatomic, readwrite) YWTribe *tribe;

@property (nonatomic, strong) GSPPlayerManager *playerManager;

@end

