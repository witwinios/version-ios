//
//  StationsController.h
//  FastSdkDemo
//
//  Created by MacAir on 2017/5/9.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StationCell.h"
@interface StationsController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)NSNumber *testId;
@end
