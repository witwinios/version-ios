//
//  TCaddNewPlanViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2017/12/14.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TCaddNewPlanViewController.h"

@interface TCaddNewPlanViewController ()<PassingValueDelegate>
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *EditBtn;
    UIButton *SaveBtn;
    
    //教案状态：
    NSNumber *JAZT;
    NSString *JAZTString;
   
    //教案科目
    NSString *MoldIDString; //ID
    NSString *MoldString;
    
    //tableView
    UITableView *courseTable;
    NSArray *titleArray;
    
    //蒙版：
    UIView *BackView;
    UIButton *Backbtn;
    
    
    //选择器：
    UIPickerView *Picker;//选择器
    
    NSMutableArray *GenreID; //教案类型 ID
    NSString *GenreIDString; //教案类型 ID 显示
    NSMutableArray *DateGenre;//教案类型数据
    NSString *DateGenreString; //教案类型数据 显示
    
    
    NSMutableArray *DateClassify;//教案分类数据
    NSMutableArray *ClassifyID; //教案分类 ID
    NSString *ClassifyIDString; //教案分类 ID 显示
    NSString *DateClassifyString; //教案分类数据 显示
    
    
    UIButton *OkBtn;
    UIButton *NoBtn;
    
    //修改：
    NSMutableArray *EditArr;
    NSString *PlanNameText;
    NSString *PlanIntroText;
    
    NSString *str;
    NSString *TextViewStr;
    
    NSMutableArray *NewArr;
    
    NSString *EditStr;
    
}

@end

@implementation TCaddNewPlanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
       //获取数据:
       [self loadData];
    
       [self setNav];
    
       [self setUI];
    
   
    
    
    if([_Condition isEqualToString:@"YES"]){
      
    self.NewPlanStateSegmented.selectedSegmentIndex=0;
        
    }else if([_Condition isEqualToString:@"NO"]){
        [self tableView];
        [self NotEdit];
    }
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"创建教案";
    if([_Condition isEqualToString:@"YES"]){
        titleLab.text = @"创建教案";
    }else if([_Condition isEqualToString:@"NO"]){
        titleLab.text = @"教案详情";
    }
    
    
    [backNavigation addSubview:titleLab];
    
    
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [rightBtn setTitle:@" 完成" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(saveStudent:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];


     EditBtn=[UIButton buttonWithType:UIButtonTypeCustom];
     SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (_PlanModel.PlanOwnerId.intValue == persion.userID.intValue ) {
        if ([_PlanModel.PlanCourseStatus isEqualToString:@"in_design"]) {
    
            EditBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
            [EditBtn setTitle:@"编辑" forState:UIControlStateNormal];
            [EditBtn addTarget:self action:@selector(EditPlan:) forControlEvents:UIControlEventTouchUpInside];
            [backNavigation addSubview:EditBtn];
           
            
           
            SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
            [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
            [SaveBtn addTarget:self action:@selector(EditSavePlan:) forControlEvents:UIControlEventTouchUpInside];
            [backNavigation addSubview:SaveBtn];
            SaveBtn.hidden=YES;
        }
    }
    
    
  
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
/*

 enum CourseStatus//课程状态
 {
 in_design,//设计中
 released,//已经发布
 }

*/

#pragma  mark 完成创建：
-(void)saveStudent:(UIButton *)sender{

    if([self.NewPlanText.text isEqualToString:@""]){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"教案名称为空，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else if([self.NewPlanGenreBtn.titleLabel.text isEqualToString:@"请选择教案类型"]){
        
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"教案类型为空，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else if([self.NewPlanSubjectBtn.titleLabel.text isEqualToString:@"请选择教案科目"]){
        
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"教案科目为空，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else if([DateClassifyString isEqualToString:@"请选择教案科目"]){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"教案科目为空，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else if([self.NewPlanClassifyBtn.titleLabel.text isEqualToString:@"请选择教案分类"]){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"教案分类为空，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        
        if(self.NewPlanIntroTextView.text.length == 0){
            self.NewPlanIntroTextView.text=@"";
        }
        
        NSLog(@"教案名称：%@",str);
        NSLog(@"教案类型：%@ 教案类型Id：%@",DateGenreString,GenreIDString);
        NSLog(@"教案科目：%@ 教案科目Id：%@",MoldString,MoldIDString);
        NSLog(@"教案分类：%@ 教案分类Id：%@",DateClassifyString,ClassifyIDString);
        NSLog(@"教案简介：%@",TextViewStr);
        NSLog(@"教案状态：%@",JAZTString);

        
    NSString *URL=[NSString stringWithFormat:@"%@/courses",LocalIP];
    
    NSDictionary *params=@{
                           @"courseName":str,
                           @"subjectId":MoldIDString,
                           @"categoryId":ClassifyIDString,
                           @"courseType":GenreIDString,
                           @"courseStatus":@"in_design",
                           @"description":self.NewPlanIntroTextView.text,
                           @"ownerId":LocalUserId,
                           @"isPublic":JAZT
                           };
    
    NSLog(@"%@",params);
    
    [RequestTools RequestWithURL:URL Method:@"post" Params:params Success:^(NSDictionary *result) {
        NSLog(@"%@",result);
        NSLog(@"%@",result);
        NSString *resultStr=[result objectForKey:@"errorCode"];
        NSLog(@"resultStr:%@",resultStr);
        if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){

            if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
                [MBProgressHUD showToastAndMessage:@"创建教案成功!" places:0 toView:nil];
                [self NotEdit];
                [self tableView];
                 _NewStr=@"新建";
                TCourseAddModel *model = [TCourseAddModel new];
                model.PlanName=[[result objectForKey:@"responseBody"] objectForKey:@"courseName"];
                model.PlanClassify=[[result objectForKey:@"responseBody"] objectForKey:@"categoryName"];
                model.PlanClassifyId=[[result objectForKey:@"responseBody"] objectForKey:@"categoryId"];
                model.PlanCourse=[[result objectForKey:@"responseBody"] objectForKey:@"subjectName"];
                model.PlanCourseId=[[result objectForKey:@"responseBody"] objectForKey:@"subjectId"];
                model.PlanOwner=[[result objectForKey:@"responseBody"] objectForKey:@"ownerName"];
                model.PlanOwnerId=[[result objectForKey:@"responseBody"] objectForKey:@"ownerId"];
                model.PlanNameId=[[result objectForKey:@"responseBody"] objectForKey:@"courseId"];
                model.PlanGenre=[[result objectForKey:@"responseBody"] objectForKey:@"courseType"];
                model.planGenreId=[[result objectForKey:@"responseBody"] objectForKey:@"courseType"];
                model.PlanDescription=[[result objectForKey:@"responseBody"] objectForKey:@"description"];
                model.PlanPublic=[[result objectForKey:@"responseBody"] objectForKey:@"isPublic"];
                model.PlanNameId=[[result objectForKey:@"responseBody"] objectForKey:@"courseId"];
                model.PlanCourseStatus=[[result objectForKey:@"responseBody"] objectForKey:@"courseStatus"];
                
                [NewArr addObject:model];
                
              
                self.NewPlanText.text=str;
                [self.NewPlanGenreBtn setTitle:DateGenreString forState:UIControlStateNormal];
                [self.NewPlanSubjectBtn setTitle:MoldString forState:UIControlStateNormal];
                [self.NewPlanClassifyBtn setTitle:DateClassifyString forState:UIControlStateNormal];
                 self.NewPlanIntroTextView.text=TextViewStr;
                 self.NewPlanStateSegmented.selectedSegmentIndex=[JAZTString integerValue];
                
            }
            }else{
                if ([resultStr isEqualToString:@"duplicate_course_info"]) {
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"重复的课程信息，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    NSLog(@"0");
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                
                if([resultStr isEqualToString:@"course_category_not_found"]){
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"该教案没有该分类，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                if([resultStr isEqualToString:@"subject_not_found"]){
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"该教案没有该科目，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
                if([resultStr isEqualToString:@"user_not_found"]){
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"没有该用户，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
                if([resultStr isEqualToString:@"invalid_course_status"]){
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"该教案以正常发布，无法修改，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            
        }

} failed:^(NSString *result) {
        NSLog(@"result:%@",result);
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
             NSLog(@"result:%@",result);
        }
    }];
 
        
    }
        
}

#pragma mark 修改保存
-(void)EditSavePlan:(UIButton *)sender{

    if (str.length == 0) {
        str=_PlanModel.PlanName;
    }
    
    if (DateGenreString.length == 0) {
        DateGenreString=_PlanModel.PlanGenre;
        GenreIDString =_PlanModel.planGenreId;
    }
    
    if(MoldString.length == 0){
        MoldString=_PlanModel.PlanCourse;
        MoldIDString=[NSString stringWithFormat:@"%@", _PlanModel.PlanCourseId];
    }
    
    if (DateClassifyString.length == 0) {
        DateClassifyString=_PlanModel.PlanClassify;
        ClassifyIDString=[NSString stringWithFormat:@"%@", _PlanModel.PlanClassifyId];
    }
    
    if (TextViewStr.length ==0 ) {
        TextViewStr=_PlanModel.PlanDescription;
    }
    
    
    
    NSLog(@"教案名称：%@",str);
    NSLog(@"教案类型：%@ 教案类型Id：%@",DateGenreString,GenreIDString);
    NSLog(@"教案科目：%@ 教案科目Id：%@",MoldString,MoldIDString);
    NSLog(@"教案分类：%@ 教案分类Id：%@",DateClassifyString,ClassifyIDString);
    NSLog(@"教案简介：%@",self.NewPlanIntroTextView.text);
    NSLog(@"教案状态：%@",JAZTString);
    NSLog(@"%@",_PlanModel.PlanCourseId);
    
    if([self.NewPlanText.text isEqualToString:@""]){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"教案名称为空，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else if([self.NewPlanGenreBtn.titleLabel.text isEqualToString:@"请选择教案类型"]){
        
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"教案类型为空，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else if([self.NewPlanSubjectBtn.titleLabel.text isEqualToString:@"请选择教案科目"]){
        
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"教案科目为空，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        NSString *URL=[NSString stringWithFormat:@"%@/courses/%@",LocalIP,_PlanModel.PlanNameId];

        NSDictionary *params=@{
                               @"courseName":str,
                               @"subjectId":MoldIDString,
                               @"categoryId":ClassifyIDString,
                               @"courseType":GenreIDString,
                               @"courseStatus":@"in_design",
                               @"description":self.NewPlanIntroTextView.text,
                               @"ownerId":LocalUserId,
                               @"isPublic":JAZT
                               };

        NSLog(@"%@",params);

        [RequestTools RequestWithURL:URL Method:@"put" Params:params Success:^(NSDictionary *result) {
            NSLog(@"%@",result);

            NSString *resultStr=[result objectForKey:@"errorCode"];
            NSLog(@"resultStr:%@",resultStr);

            if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){


                NSString *resultStr=[result objectForKey:@"errorCode"];
                NSLog(@"resultStr:%@",resultStr);

                if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
                    [MBProgressHUD showToastAndMessage:@"修改教案成功!" places:0 toView:nil];
                    [self NotEdit];
                    _editing=NO;
         
                    self.NewPlanText.text=str;
                    [self.NewPlanGenreBtn setTitle:DateGenreString forState:UIControlStateNormal];
                    [self.NewPlanSubjectBtn setTitle:MoldString forState:UIControlStateNormal];
                    [self.NewPlanClassifyBtn setTitle:DateClassifyString forState:UIControlStateNormal];
                    self.NewPlanIntroTextView.text=TextViewStr;
                    self.NewPlanStateSegmented.selectedSegmentIndex=[JAZTString integerValue];
                    self.stirngLenghLabel.text=[NSString stringWithFormat:@"%lu/100", (unsigned long)self.NewPlanIntroTextView.text.length];
                }

            }else{
                if ([resultStr isEqualToString:@"duplicate_course_info"]) {
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"重复的课程信息，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }else if([resultStr isEqualToString:@"course_category_not_found"]){
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"该教案没有该分类，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }else if([resultStr isEqualToString:@"subject_not_found"]){
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"该教案没有该科目，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }else if([resultStr isEqualToString:@"user_not_found"]){
                    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"没有该用户，无法正常创建！！" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }

        } failed:^(NSString *result) {
            NSLog(@"result:%@",result);
            if ([result isEqualToString:@"200"]) {
                [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            }else{
                [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
                NSLog(@"result:%@",result);
            }
        }];
        
        
    }
}

-(void)loadData{

    //获取教案分类：
    NSString *UrlMold=[NSString stringWithFormat:@"%@/courseCategories?pageSize=200",LocalIP];
    [RequestTools RequestWithURL:UrlMold Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if([[result objectForKey:@"responseStatus"] isEqualToString:@"succeed"]){
            NSArray *array=[[result objectForKey:@"responseBody"]objectForKey:@"result"];
            
            if (array.count == 0) {
                [Maneger showAlert:@"暂无课程分类" andCurentVC:self];
                
            }else{
                
                DateClassify=[NSMutableArray new];
                ClassifyID=[NSMutableArray new];
                for(int i=0;i<array.count;i++){
                    NSDictionary *dic=[array objectAtIndex:i];
                    
                    [DateClassify addObject:[dic objectForKey:@"categoryName"]];
                    
                    [ClassifyID addObject:[dic objectForKey:@"categoryId"]];
                    
                    [Picker reloadAllComponents];
                }
                
            };
        }
    } failed:^(NSString *result) {
        NSLog(@"请求错误");
    }];
 
}

-(void)setUI{
    
    //教案名称：
    NSMutableAttributedString *threeStr = [[NSMutableAttributedString alloc] initWithString:@"*教案名称："];
    [threeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.NewPlanLabel.attributedText = threeStr;
    
    self.NewPlanText.borderStyle=UITextBorderStyleRoundedRect; //边框样式
    self.NewPlanText.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.NewPlanText.clearsOnBeginEditing = YES;
    self.NewPlanText.keyboardType=UIKeyboardTypeDefault;
    self.NewPlanText.returnKeyType=UIReturnKeyDefault;
    self.NewPlanText.delegate = self;
    self.NewPlanText.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.NewPlanText.layer.borderWidth=1;
    self.NewPlanText.layer.cornerRadius=5;
    self.NewPlanText.textAlignment=NSTextAlignmentCenter;
    [self.NewPlanText addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    //教案类型：
    NSMutableAttributedString *threeStr2 = [[NSMutableAttributedString alloc] initWithString:@"*教案类型："];
    [threeStr2 addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.NewPlanGenreLabel.attributedText = threeStr2;
    
    self.NewPlanGenreBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.NewPlanGenreBtn.layer.borderWidth=1;
    self.NewPlanGenreBtn.layer.cornerRadius=5;
    [self.NewPlanGenreBtn addTarget:self action:@selector(PlanGenreAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //教案科目：
    NSMutableAttributedString *threeStr3 = [[NSMutableAttributedString alloc] initWithString:@"*教案科目："];
    [threeStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.NewPlanSubjectLabel.attributedText = threeStr3;
    
    self.NewPlanSubjectBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.NewPlanSubjectBtn.layer.borderWidth=1;
    self.NewPlanSubjectBtn.layer.cornerRadius=5;
    [self.NewPlanSubjectBtn addTarget:self action:@selector(PlanSubjectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    //教案分类：
    NSMutableAttributedString *threeStr4 = [[NSMutableAttributedString alloc] initWithString:@"*教案分类："];
    [threeStr4 addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
    self.NewPlanClassifyLabel.attributedText = threeStr4;
    
    self.NewPlanClassifyBtn.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.NewPlanClassifyBtn.layer.borderWidth=1;
    self.NewPlanClassifyBtn.layer.cornerRadius=5;
    [self.NewPlanClassifyBtn addTarget:self action:@selector(PlanClassifyAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //教案简介：
    self.NewPlanIntroTextView.delegate=self;
    self.NewPlanIntroTextView.showsVerticalScrollIndicator=NO;
    self.NewPlanIntroView.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
    self.NewPlanIntroView.layer.borderWidth=1;
    self.NewPlanIntroView.layer.cornerRadius=5;
    PlanIntroText=self.NewPlanIntroTextView.text;
    
    
    //教案状态：
    JAZT=@(NO);
    JAZTString=@"0";
    [self.NewPlanStateSegmented addTarget:self action:@selector(PlanChange:) forControlEvents:UIControlEventValueChanged];
    
    NewArr=[NSMutableArray new];
    _editing=NO;
   
}
-(void)ApplyTextDidChange:(UITextField *)theTextField{
    NSLog(@"%@",theTextField);
    str=theTextField.text;
}

#pragma mark 隐藏蒙版：
-(void)setNoBtn{
    BackView.hidden=YES;
 
}


#pragma mark 收回键盘:
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.NewPlanText resignFirstResponder];
    [self.NewPlanIntroTextView resignFirstResponder];
    BackView.hidden=YES;
}


#pragma mark 教案类型：
-(void)PlanGenreAction:(UIButton *)senser{
    [self.NewPlanText resignFirstResponder];
    [self.NewPlanIntroTextView resignFirstResponder];
    if(BackView !=nil){
        BackView.hidden=YES;
    }
    CGFloat Picker_Y=Sheight/3;
    BackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    BackView.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.4];
    
    //工具栏：
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0, Sheight-Picker_Y, Swidth, Picker_Y)];
    UIView *ToolView=[[UIView alloc]initWithFrame:CGRectMake(0, Picker.wwy_y-30, Swidth, 30)];
    ToolView.backgroundColor=UIColorFromHex(0x20B2AA);
    [BackView addSubview:ToolView];
    
    OkBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    OkBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    [OkBtn setTitle:@"确定" forState:UIControlStateNormal];
    [NoBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [OkBtn addTarget:self action:@selector(AddGenreAction:) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:OkBtn];
    
    NoBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    NoBtn.frame=CGRectMake(0, 0, 100, 30);
    [NoBtn setTitle:@"取消" forState:UIControlStateNormal];
    [NoBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [NoBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:NoBtn];
    
    
    
    Picker.backgroundColor=[UIColor whiteColor];
    Picker.tag=0;
    Picker.dataSource=self;
    Picker.delegate=self;
    
    //获取教案类型；
    DateGenre=[[NSMutableArray alloc]initWithObjects:         @"请选择教案类型",
                                                              @"课堂课程",
                                                              @"在线课程",
                                                              @"实操课程",
                                                              @"教学查房",
                                                              @"病例讨论",
                                                                nil];
    
    GenreID=[[NSMutableArray alloc]initWithObjects:  @"请选择教案类型",
                                                     @"for_classroom_course",
                                                     @"for_online_course",
                                                     @"for_operating_course",
                                                     @"for_teaching_rounds",
                                                     @"for_case_discussion",
                                                        nil];
    
    if (Picker.tag == 0) {
        DateGenreString = DateGenre[1];
        GenreIDString=GenreID[1];
    }
    
    [BackView addSubview:Picker];
    
   [[UIApplication sharedApplication].keyWindow addSubview:BackView];
   
}


-(void)AddGenreAction:(UIButton *)sender{
    [self.NewPlanGenreBtn setTitle:DateGenreString forState:UIControlStateNormal];
    BackView.hidden=YES;
}




#pragma mark 教案分类：
-(void)PlanClassifyAction:(UIButton *)senser{
    [self.NewPlanText resignFirstResponder];
    [self.NewPlanIntroTextView resignFirstResponder];
    
    if(BackView !=nil){
        BackView.hidden=YES;
    }
    CGFloat Picker_Y=Sheight/3;
    BackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    BackView.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.4];
    
    //工具栏：
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0, Sheight-Picker_Y, Swidth, Picker_Y)];
    UIView *ToolView=[[UIView alloc]initWithFrame:CGRectMake(0, Picker.wwy_y-30, Swidth, 30)];
    ToolView.backgroundColor=UIColorFromHex(0x20B2AA);
    [BackView addSubview:ToolView];
    
    OkBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    OkBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    [OkBtn setTitle:@"确定" forState:UIControlStateNormal];
    [NoBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [OkBtn addTarget:self action:@selector(AddClassifyAction:) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:OkBtn];
    
    NoBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    NoBtn.frame=CGRectMake(0, 0, 100, 30);
    [NoBtn setTitle:@"取消" forState:UIControlStateNormal];
    [NoBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [NoBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:NoBtn];
    
    
    
    Picker.backgroundColor=[UIColor whiteColor];
    Picker.tag=1;
    Picker.dataSource=self;
    Picker.delegate=self;
    
    
    if (Picker.tag == 1) {
        DateClassifyString = DateClassify[0];
        ClassifyIDString=ClassifyID[0];
    }
    
    [BackView addSubview:Picker];
    
 [[UIApplication sharedApplication].keyWindow addSubview:BackView];
    

}

-(void)AddClassifyAction:(UIButton *)sender{
    [self.NewPlanClassifyBtn setTitle:DateClassifyString forState:UIControlStateNormal];
    BackView.hidden=YES;

}


#pragma mark 教案科目：
-(void)PlanSubjectAction:(UIButton *)senser{
    TCourseMoldViewController *Mold=[[TCourseMoldViewController alloc]init];
    Mold.delegate=self;
    [self.navigationController pushViewController:Mold animated:YES];
}
-(void)text:(NSString *)str dateID:(NSString *)dateID{
    NSLog(@"%@",str);
    [self.NewPlanSubjectBtn setTitle:str forState:UIControlStateNormal];
    MoldIDString=dateID;
    MoldString=str;
    
}


#pragma mark 教案简介：
-(void)textViewDidChange:(UITextView *)textView{
    
//    if(_editing == NO){
         self.stirngLenghLabel.text=[NSString stringWithFormat:@"%lu/100", (unsigned long)textView.text.length];
//    }else if(_editing == YES){
//         self.stirngLenghLabel.text=[NSString stringWithFormat:@"%lu/100", (unsigned long)EditStr.length];
//    }
    
   
    if (textView.text.length >= 100) {
        textView.text = [textView.text substringToIndex:100];
        self.stirngLenghLabel.text = @"100/100";
    }
    
    TextViewStr=textView.text;
}


#pragma mark 教案状态：
-(void)PlanChange:(UISegmentedControl *)sender{

    if(sender.selectedSegmentIndex == 0){
        
        NSLog(@"不公开状态");
        JAZT=@(NO);
        JAZTString=@"0";

    }else if(sender.selectedSegmentIndex == 1){
        NSLog(@"公开状态");
        JAZT=@(YES);
        JAZTString=@"1";
    }
}


#pragma mark 允许编辑：
-(void)EditPlan:(UIButton *)sender{
    [MBProgressHUD showToastAndMessage:@"开启编辑模式" places:0 toView:nil];
    self.NewPlanText.userInteractionEnabled=YES;
    self.NewPlanGenreBtn.userInteractionEnabled=YES;
    self.NewPlanSubjectBtn.userInteractionEnabled=YES;
    self.NewPlanClassifyBtn.userInteractionEnabled=YES;
    self.NewPlanIntroTextView.userInteractionEnabled=YES;
    self.NewPlanStateSegmented.userInteractionEnabled=YES;

    EditBtn.hidden=YES;
    SaveBtn.hidden=NO;
    self.stirngLenghLabel.hidden=NO;
  
    NSLog(@"%@",_PlanModel.PlanCourseId);
    _editing=YES;
}


#pragma mark 禁止编辑：
-(void)NotEdit{
    self.NewPlanText.userInteractionEnabled=NO;
    self.NewPlanGenreBtn.userInteractionEnabled=NO;
    self.NewPlanSubjectBtn.userInteractionEnabled=NO;
    self.NewPlanClassifyBtn.userInteractionEnabled=NO;
    self.NewPlanIntroTextView.userInteractionEnabled=NO;
    self.NewPlanStateSegmented.userInteractionEnabled=NO;
   

    self.NewPlanText.text=_PlanModel.PlanName;
    self.NewPlanText.textAlignment=NSTextAlignmentCenter;
    
    [self.NewPlanGenreBtn setTitle:_PlanModel.PlanGenre forState:UIControlStateNormal];
    [self.NewPlanGenreBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.NewPlanSubjectBtn setTitle:_PlanModel.PlanCourse forState:UIControlStateNormal];
    [self.NewPlanSubjectBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.NewPlanClassifyBtn setTitle:_PlanModel.PlanClassify forState:UIControlStateNormal];
    [self.NewPlanClassifyBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    self.NewPlanIntroTextView.text=_PlanModel.PlanDescription;
   
    self.stirngLenghLabel.text=[NSString stringWithFormat:@"%lu/100", (unsigned long)self.NewPlanIntroTextView.text.length];
    
    
    self.NewPlanStateSegmented.selectedSegmentIndex=[_PlanModel.PlanPublic integerValue];
    
    NSLog(@"PlanPublic:%d",[_PlanModel.PlanPublic intValue]);
    
    rightBtn.hidden=YES;
    EditBtn.hidden=NO;
    SaveBtn.hidden=YES;
    self.stirngLenghLabel.hidden=YES;
    _editing=NO;
}

#pragma mark TableView:
-(void)tableView{
    
     CGFloat TableView_Y=self.NewPlanStateSegmented.wwy_y+self.NewPlanStateSegmented.wwy_height+backNavigation.wwy_height+15;
    
    NSLog(@"%f",TableView_Y);
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0,TableView_Y ,Swidth, Sheight-TableView_Y) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [courseTable registerNib:[UINib nibWithNibName:@"TCourseDatailCell" bundle:nil] forCellReuseIdentifier:@"cellStyle1"];
    [self.view addSubview:courseTable];

    UIView *backView=[[UIView alloc]initWithFrame:CGRectMake(0, TableView_Y-5, Swidth, 5)];
    backView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    [self.view addSubview:backView];
    
    
    titleArray=@[@"教案附件：",@"标准课件：",@"课前答题：",@"课后答题:",@"所需教具：",@"典型病案："];

    
}


//返回行数：
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

//点击行事件：
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        if ([_NewStr isEqualToString:@"新建"]) {
            FileViewController *AfVc=[[FileViewController alloc]init];
            AfVc.PlanModel=NewArr[0];
            AfVc.Str=@"教案";
            [self.navigationController pushViewController:AfVc animated:YES];
        }else{
            FileViewController *AfVc=[[FileViewController alloc]init];
            AfVc.PlanModel=_PlanModel;
            AfVc.Str=@"教案";
            [self.navigationController pushViewController:AfVc animated:YES];
        }
        
    }else if(indexPath.row == 1){
         if ([_NewStr isEqualToString:@"新建"]) {
        CourseWareViewController *Vc=[[CourseWareViewController alloc]init];
        Vc.PlanModel=NewArr[0];
        [self.navigationController pushViewController:Vc animated:YES];
         }else{
             CourseWareViewController *Vc=[[CourseWareViewController alloc]init];
             Vc.PlanModel=_PlanModel;
             [self.navigationController pushViewController:Vc animated:YES];
         }
    }else if(indexPath.row==2 || indexPath.row == 3){
       
        CourseQuestionViewController *vc=[[CourseQuestionViewController alloc]init];
        vc.lastStr=@"教案";
        if (indexPath.row == 2) {
            vc.str=@"课前";
        }else if(indexPath.row == 3){
            vc.str=@"课后";
        }
        
        
        if ([_NewStr isEqualToString:@"新建"]) {
            vc.courseStr=@"教案";
            vc.PlanModel=NewArr[0];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            vc.courseStr=@"教案";
            vc.PlanModel=_PlanModel;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
        
       
    }else if(indexPath.row == 4){
        
        if ([_NewStr isEqualToString:@"新建"]) {
            trainingAidsViewController *vc=[[trainingAidsViewController alloc]init];
            vc.PlanModel=NewArr[0];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            trainingAidsViewController *vc=[[trainingAidsViewController alloc]init];
            vc.PlanModel=_PlanModel;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
        
    }else{
        
       if ([_NewStr isEqualToString:@"新建"]) {
           TypicalCasesViewController *vc=[[TypicalCasesViewController alloc]init];
           vc.PlanModel=NewArr[0];
           [self.navigationController pushViewController:vc animated:YES];
       }else{
           TypicalCasesViewController *vc=[[TypicalCasesViewController alloc]init];
           vc.PlanModel=_PlanModel;
           [self.navigationController pushViewController:vc animated:YES];
       }
        
        
       
        
    }

    
}

//行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 32;
}

//加载cell：
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TCourseDatailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellStyle1"];
    cell.title.text=titleArray[indexPath.row];
   // cell.title.font=[UIFont systemFontOfSize:15];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}


#pragma mark PickerView:

//返回数据列数：
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(Picker.tag==0){
        return [DateGenre count];
    }else{
        
        return [DateClassify count];
    }
  
}

//获取数据
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    if(Picker.tag==0){
        return [DateGenre objectAtIndex:row];
    }else{
        return [DateClassify objectAtIndex:row];
    }
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if(Picker.tag==0){
        NSLog(@"%@",[DateGenre objectAtIndex:row]);
        DateGenreString = [DateGenre objectAtIndex:row];
        
        GenreIDString=[GenreID objectAtIndex:row];
        NSLog(@"%@",GenreIDString);
    }else{
        NSLog(@"%@",[DateClassify objectAtIndex:row]);
        DateClassifyString = [DateClassify objectAtIndex:row];
        
        ClassifyIDString=[ClassifyID objectAtIndex:row];
        NSLog(@"%@",ClassifyIDString);
    }

}
@end
