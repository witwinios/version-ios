//
//  ExamModelCell.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/4/5.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ExamModelCell.h"

@implementation ExamModelCell
- (void)awakeFromNib {
    [super awakeFromNib];
}
- (void)setProperty:(id)model andIndex:(NSInteger)index{
    if (index==0) {
        OSCEModel *transModel = (OSCEModel *)model;
        self.testName.text = transModel.osceName;
        self.testStatus.text = transModel.osceStatus;
        self.testCategory.text = @"OSCE考试";
        self.testClassRoom.text = transModel.stationName;
        self.testTime.text = [[Maneger shareObject] timeFormatter:transModel.osceTime.stringValue];
    }else if (index==1){
        onlineModel *transModel = (onlineModel *)model;
        self.testName.text = transModel.onlineName;
        self.testCategory.text = @"在线考试";
        self.testStatus.text = transModel.onlineStatus;
        self.testClassRoom.text = transModel.onlineRoom;
        self.testTime.text = [[Maneger shareObject] timeFormatter:transModel.onlineTime.stringValue];
    }else{
        SkillModel *transModel = (SkillModel *)model;
        self.testName.text = transModel.skillName;
        self.testStatus.text = transModel.skillStatus;
        self.testCategory.text = @"技能考试";
        self.testClassRoom.text = @"无详情";
        self.testTime.text = [[Maneger shareObject] timeFormatter:transModel.skillStartTime.stringValue];
    }
}
@end
