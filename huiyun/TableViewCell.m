//
//  TableViewCell.m
//  huiyun
//
//  Created by Bad on 2018/3/23.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.backgroundColor=[UIColorFromHex(0x20B2AA)colorWithAlphaComponent:0.3];
}

-(void)setTeacherAddModel:(TSkillTestCenterModel *)model{
    
    self.OneLabel.text=@"站点名称：";
    self.TwoLabel.text=model.SelectedStationName;
    
    self.ThreeLabel.text=@"考官：";
    
    NSString *TeacherStr;
    NSString *tempStr;
    NSString *OverStr;
    for (int i=0; i<model.TeacherArray.count; i++) {
        TeacherStr=[NSString stringWithFormat:@"%@",model.TeacherArray[i]];
        tempStr=[OverStr stringByAppendingString:TeacherStr];
        OverStr=TeacherStr;
    }
    self.FourLabel.text=model.TeacherName;
    
    
    
    self.FiveLabel.text=@"技能满分:";
    self.SixLabel.text=[NSString stringWithFormat:@"%@",model.OperationScoreValue];
    
    self.SevenLabel.text=@"考试地点";
    self.EightLabel.text=model.RoomName;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
