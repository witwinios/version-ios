//
//  QuesDetailController.m
//  yun
//
//  Created by MacAir on 2017/5/27.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "QuesDetailController.h"

@interface QuesDetailController ()
{
    YWConversationViewController *selfConversation;
    UITableView *tabView;
}
@end

@implementation QuesDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (YWIMCore *)ywIMCore {
    return [SPKitExample sharedInstance].ywIMKit.IMCore;
}
- (id<IYWTribeService>)ywTribeService {
    return [[self ywIMCore] getTribeService];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(10, 34.5, 15, 15);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(self.view.frame.size.width-85, 29.5, 80, 25);
    [rightBtn setTitle:@"置为解决?" forState:0];
    rightBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [rightBtn addTarget:self action:@selector(solveAction:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"详情";
    [backNavigation addSubview:titleLab];
    //设置描述
    UILabel *desLab = [UILabel new];
    desLab.frame = CGRectMake(10, 70, 80, 32);
    desLab.text = @"问题描述:";
    [self.view addSubview:desLab];
    UILabel *desContent = [UILabel new];
    desContent.font = [UIFont systemFontOfSize:18];
    CGFloat height = [[Maneger shareObject] getSpaceLabelHeight:self.model.quesDes withFont:[UIFont systemFontOfSize:15] withWidth:Swidth-95 andLineSpace:1];
    desContent.frame = CGRectMake(95, 70, Swidth-95, height);
    desContent.text = self.model.quesDes;
    [self.view addSubview:desContent];
    
    //
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 70+height+5, Swidth, Sheight-115-height) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    [tabView registerNib:[UINib nibWithNibName:@"TeacherCell" bundle:nil] forCellReuseIdentifier:@"teacher"];
    [self.view addSubview:tabView];
    //添加button
    UIButton *joinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    joinBtn.frame = CGRectMake(Swidth/2-50, Sheight-35, 100, 30);
    [joinBtn setTitle:@"进去会话" forState:0];
    joinBtn.layer.cornerRadius = 5;
    joinBtn.layer.masksToBounds = YES;
    [joinBtn addTarget:self action:@selector(joinAction:) forControlEvents:UIControlEventTouchUpInside];
    [joinBtn setTitleColor:[UIColor whiteColor] forState:0];
    joinBtn.backgroundColor = [UIColor blueColor];
    [self.view addSubview:joinBtn];

}
//
- (void)exampleOpenConversationViewControllerWithTribe:(YWTribe *)aTribe fromNavigationController:(UINavigationController *)aNavigationController
{
    YWConversation *conversation = [YWTribeConversation fetchConversationByTribe:aTribe createIfNotExist:YES baseContext:self.ywIMKit.IMCore];
    [self exampleOpenConversationViewControllerWithConversation:conversation fromNavigationController:aNavigationController];
}
- (void)exampleOpenConversationViewControllerWithConversation:(YWConversation *)aConversation fromNavigationController:(UINavigationController *)aNavigationController
{
    __block YWConversationViewController *alreadyController = nil;
    [aNavigationController.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[YWConversationViewController class]]) {
            YWConversationViewController *c = obj;
            if (aConversation.conversationId && [c.conversation.conversationId isEqualToString:aConversation.conversationId]) {
                alreadyController = c;
                *stop = YES;
            }
        }
    }];
    
    if (alreadyController) {
        /// 必须判断当前是否已有该会话，如果有，则直接显示已有会话
        /// @note 目前IMSDK不允许同时存在两个相同会话的Controller
        [aNavigationController popToViewController:alreadyController animated:YES];
        [aNavigationController setNavigationBarHidden:NO];
        return;
    } else {
        YWConversationViewController *conversationController = [self.ywIMKit makeConversationViewControllerWithConversationId:aConversation.conversationId];
        selfConversation = conversationController;
        __weak typeof(conversationController) weakController = conversationController;
        [conversationController setViewWillAppearBlock:^(BOOL aAnimated) {
            //            [weakController.navigationController setNavigationBarHidden:YES animated:aAnimated];
            weakController.view.backgroundColor = [UIColor whiteColor];
            //添加导航栏
            UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
            backNavigation.image = [UIImage imageNamed:@"navigationbar"];
            backNavigation.userInteractionEnabled = YES;
            [self.view addSubview:backNavigation];
            self.view.backgroundColor = [UIColor whiteColor];
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            leftBtn.frame = CGRectMake(10, 27, 50, 30);
            [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
            [leftBtn setTitle:@"返回" forState:0];
            [backNavigation addSubview:leftBtn];
            UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            rightBtn.frame = CGRectMake(self.view.frame.size.width-35, 29.5, 25, 25);
            [rightBtn setImage:[UIImage imageNamed:@"user"] forState:UIControlStateNormal];
            [rightBtn addTarget:self action:@selector(rightItemAction:) forControlEvents:UIControlEventTouchUpInside];
            [backNavigation addSubview:rightBtn];
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
            titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
            titleLab.textAlignment = 1;
            titleLab.font = [UIFont systemFontOfSize:20];
            titleLab.textColor = [UIColor whiteColor];
            titleLab.text = @"讨论组";
            [backNavigation addSubview:titleLab];
            
            UIButton *solveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            solveBtn.frame = CGRectMake(backNavigation.frame.size.width/2+60, 27, 80, 30);
            [solveBtn setTitle:@"已解决?" forState:0];
//            solveBtn.titleLabel.adjustsFontSizeToFitWidth=YES;
            solveBtn.layer.cornerRadius = 3;
            solveBtn.layer.masksToBounds=YES;
            solveBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            solveBtn.layer.borderWidth = 1;
            [solveBtn addTarget:self action:@selector(solveAction:) forControlEvents:UIControlEventTouchUpInside];
            [backNavigation addSubview:solveBtn];
            [weakController.view addSubview:backNavigation];
            
            //添加textView
            UITextView *quesView = [UITextView new];
            quesView.frame = CGRectMake(0, 64, Swidth, 60);
            quesView.text = [NSString stringWithFormat:@"问题描述:%@",self.model.quesDes];
            quesView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            quesView.layer.borderWidth = 1;
            quesView.editable = NO;
            [weakController.view addSubview:quesView];
            //
            weakController.tableView.frame = CGRectMake(0, 124, Swidth, Sheight-124);
        }];
        
        [aNavigationController pushViewController:conversationController animated:YES];
        
    }
}

#pragma -action
- (void)solveAction:(UIButton *)btn{
    UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"是否已经解决该问题并关闭讨论组?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"已解决", nil];
    [alerView show];
}
- (void)rightItemAction:(UIButton *)btn{
    TeacherController *teacherVC = [TeacherController new];
    teacherVC.tribeArray = self.model.memberArray;
    //
    [self.navigationController pushViewController:teacherVC animated:YES];
}
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)joinAction:(UIButton *)btn{
    self.ywIMKit = [[YWAPI sharedInstance] fetchIMKitForOpenIM];
    self.tribe = [[YWTribe alloc]init];
    
    self.tribe.tribeId = [NSString stringWithFormat:@"%@",self.model.tribeId];
    self.tribe.tribeName = @"群聊";
    [self exampleOpenConversationViewControllerWithTribe:self.tribe fromNavigationController:self.navigationController];
}
#pragma -delegate dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  self.model.memberArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TeacherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"teacher"];
    TribeMem *mem = self.model.memberArray[indexPath.row];
    cell.teacherName.text = mem.fullName;
    cell.teacherEmail.text = mem.email;
    cell.teacherPhone.text = [NSString stringWithFormat:@"%@",mem.phone];
    return cell;
}
#pragma -alerViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self chageStatus];
    }
}
- (void)chageStatus{
    [MBProgressHUD showHUDAndMessage:@"加载中..." toView:self.view];
    RequestTools *tool = [RequestTools new];
    [tool postRequestPrams:nil andURL:[NSString stringWithFormat:@"%@/QAGroup/%@/solved",LocalIP,self.model.qaGroupId]];
    tool.errorBlock = ^(NSString *code){
        if (![code isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"失败，请重试" places:0 afterDelay:1 toView:self.view];
            [MBProgressHUD hideHUDForView:self.view];
        }
    };
    tool.responseBlock = ^(NSDictionary *response){
        [MBProgressHUD hideHUDForView:self.view];
        if ([[response objectForKey:@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"成功" places:0 afterDelay:1 toView:self.view];
            [NSThread sleepForTimeInterval:1];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD showToastAndMessage:@"失败，请重试" places:0 afterDelay:1 toView:self.view];
        }
    };
}
@end
