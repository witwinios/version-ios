//
//  TimeCell.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/23.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *onetext;
@property (weak, nonatomic) IBOutlet UILabel *TwoText;
@property (weak, nonatomic) IBOutlet UILabel *OneLabel;

@end
