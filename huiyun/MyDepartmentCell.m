//
//  MyDepartmentCell.m
//  huiyun
//
//  Created by Bad on 2018/3/14.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "MyDepartmentCell.h"

@implementation MyDepartmentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


-(void)setModel:(DepartmentModel *)model{
    
    if([model.GroupType isEqualToString:@"department_type"]){
        self.LeftImg.image=[UIImage imageNamed:@"Department"];
    }else{
        self.LeftImg.image=[UIImage imageNamed:@"Class"];
    }
    
    
    self.ContText.text=model.GroupName;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
