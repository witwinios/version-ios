//
//  FileViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/25.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCourseAddModel.h"
#import "TeachingAidsModel.h"
#import "TypicalCasesDetailsModel.h"
#import "CourseAffixModel.h"
@interface FileViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property(strong,nonatomic)TCourseAddModel *PlanModel;
@property(strong,nonatomic)TCourseModel *CourseModel;
@property(strong,nonatomic)NSString *Str;
@property(nonatomic,getter=isEditing) BOOL editing;
-(void)setEditing:(BOOL)editing animated:(BOOL)animated;

@property(strong,nonatomic)NSString *BeforeStr;
@property(strong,nonatomic)TeachingAidsModel *TeachingModel;
@property(strong,nonatomic)TypicalCasesDetailsModel *TypicalCasesDetailsModel;






@end
