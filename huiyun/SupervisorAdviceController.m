//
//  SupervisorAdviceController.m
//  huiyun
//
//  Created by MacAir on 2018/1/23.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "SupervisorAdviceController.h"

@interface SupervisorAdviceController ()

@end

@implementation SupervisorAdviceController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 32.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 32, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"出科小结";
    [backNavigation addSubview:titleLab];
    //
    self.mainText.text = self.model.supervisorSummary;
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tempSave:(id)sender {
    if ([self.mainText.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"不能为空~" places:0 toView:nil];
        return;
    }
    NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/saveSupervisorFeedback",SimpleIp,self.model.SDepartRecordId];
    [RequestTools RequestWithURL:request Method:@"post" Params:@{@"supervisorId":LocalUserId,@"supervisorFeedback":self.mainText.text} Message:@"暂存中..." Success:^(NSDictionary *result) {
        [MBProgressHUD showToastAndMessage:@"已暂存~" places:0 toView:nil];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"暂存失败~" places:0 toView:nil];
    }];
    
}

- (IBAction)submitAction:(id)sender {
    if ([self.mainText.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"不能为空~" places:0 toView:nil];
        return;
    }
    NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/submitSupervisorFeedback",SimpleIp,self.model.SDepartRecordId];
    [RequestTools RequestWithURL:request Method:@"post" Params:@{@"supervisorId":LocalUserId,@"supervisorFeedback":self.mainText.text} Message:@"暂存中..." Success:^(NSDictionary *result) {
        NSString *errorCode = [result[@"errorCode"] isKindOfClass:[NSNull class]]?@"":result[@"errorCode"];
        if ([errorCode isEqualToString:@""]) {
            [MBProgressHUD showToastAndMessage:@"提交成功~" places:0 toView:nil];
            self.model.isSubmitSupervisorSummary = @1;
        }else if ([errorCode isEqualToString:@"mentor_feedback_is_submit"]){
            [MBProgressHUD showToastAndMessage:@"您已经提交,不能重复提交~" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
    }];
}

@end
