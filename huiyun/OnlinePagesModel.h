//
//  OnlinePagesModel.h
//  huiyun
//
//  Created by MacAir on 2018/2/4.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OnlinePagesModel : NSObject
@property (strong, nonatomic) NSNumber *pageId;
@property (strong, nonatomic) NSString *pageName;
@property (strong, nonatomic) NSString *pageCategory;
@property (strong, nonatomic) NSString *pageSubject;
@property (strong, nonatomic) NSNumber *pageScore;
@end
