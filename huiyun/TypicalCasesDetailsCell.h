//
//  TypicalCasesDetailsCell.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/12.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TypicalCasesDetailsModel.h"
@interface TypicalCasesDetailsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *OneLabel;
@property (weak, nonatomic) IBOutlet UILabel *TwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *ThreeLabel;

@property (weak, nonatomic) IBOutlet UIButton *RightBtn;

@property (weak, nonatomic) IBOutlet UILabel *oneText;
@property (weak, nonatomic) IBOutlet UILabel *TwoText;
@property (weak, nonatomic) IBOutlet UITextView *TextView;


-(void)setProperty:(TypicalCasesDetailsModel *)model;


@end
