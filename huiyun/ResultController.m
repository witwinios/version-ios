//
//  ResultController.m
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "ResultController.h"

@interface ResultController ()

@end

@implementation ResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}
- (void)setUI{
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 32.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 32, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"出科小结";
    [backNavigation addSubview:titleLab];
    //
    self.studentSummary.text = self.departModel.studentSummary;
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)tempSave:(id)sender {
    if ([self.studentSummary.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"不能为空~" places:0 toView:nil];
        return;
    }
    NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/saveCheckOutSummary",SimpleIp,self.departModel.SDepartRecordId];
    
    [RequestTools RequestWithURL:request Method:@"post" Params:@{@"studentSummary":self.studentSummary.text} Message:@"暂存中..." Success:^(NSDictionary *result) {
        self.departModel.studentSummary = self.studentSummary.text;
        [MBProgressHUD showToastAndMessage:@"已暂存~" places:0 toView:nil];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
    }];
}

- (IBAction)submitSave:(id)sender {
    if ([self.studentSummary.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"不能为空~" places:0 toView:nil];
        return;
    }
    NSString *request = [NSString stringWithFormat:@"%@/webappv2/studentClinicalRotationRecords/%@/submitCheckOutSummary",SimpleIp,self.departModel.SDepartRecordId];
    
    [RequestTools RequestWithURL:request Method:@"post" Params:@{@"studentSummary":self.studentSummary.text} Message:@"暂存中..." Success:^(NSDictionary *result) {
        NSLog(@"%@",result);
        NSString *errorCode = [result[@"errorCode"] isKindOfClass:[NSNull class]]?@"":result[@"errorCode"];
        if ([errorCode isEqualToString:@""]) {
            self.departModel.studentSummary = self.studentSummary.text;
            [MBProgressHUD showToastAndMessage:@"提交成功~" places:0 toView:nil];
            self.departModel.isSubmitStudentSummary = @1;
        }else if ([errorCode isEqualToString:@"mentor_feedback_is_submit"]){
            [MBProgressHUD showToastAndMessage:@"您已经提交,不能重复提交~" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"提交失败~" places:0 toView:nil];
    }];
}
@end
