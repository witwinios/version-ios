//
//  TextFiledCell.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/13.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFiledCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *TextFiled;
@property (weak, nonatomic) IBOutlet UILabel *FiledName;

@end
