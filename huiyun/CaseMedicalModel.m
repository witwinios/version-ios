//
//  CaseMedicalModel.m
//  huiyun
//
//  Created by MacAir on 2017/10/11.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseMedicalModel.h"

@implementation CaseMedicalModel
//@property (strong, nonatomic) NSString *caseOne;
- (void)setCaseOne:(NSString *)caseOne{
    if ([caseOne isKindOfClass:[NSNull class]]) {
        _caseOne = @"暂无";
    }else{
        _caseOne = caseOne;
    }
}

//@property (strong, nonatomic) NSString *caseTwo;
- (void)setCaseTwo:(NSString *)caseTwo{
    if ([caseTwo isKindOfClass:[NSNull class]]) {
        _caseTwo = @"暂无";
    }else if ([caseTwo isEqualToString:@"waiting_approval"]){
        _caseTwo = @"待审核";
    }else if ([caseTwo isEqualToString:@"approved"]){
        _caseTwo = @"已通过";
    }else {
        _caseTwo = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *caseThree;
- (void)setCaseThree:(NSString *)caseThree{
    if ([caseThree isKindOfClass:[NSNull class]]) {
        _caseThree = @"暂无";
    }else{
        _caseThree = caseThree;
    }
}
//@property (strong, nonatomic) NSString *caseFour;
- (void)setCaseFour:(NSString *)caseFour{
    if ([caseFour isKindOfClass:[NSNull class]]) {
        _caseFour = @"暂无";
    }else{
        _caseFour = caseFour;
    }
}
//@property (strong, nonatomic) NSNumber *caseFive;
- (void)setCaseFive:(NSNumber *)caseFive{
    if ([caseFive isKindOfClass:[NSNull class]]) {
        _caseFive = @0;
    }else{
        _caseFive = caseFive;
    }
}
//@property (strong, nonatomic) NSString *mainIos;
- (void)setMainIos:(NSString *)mainIos{
    if ([mainIos isKindOfClass:[NSNull class]]) {
        _mainIos = @"暂无";
    }else{
        _mainIos = mainIos;
    }
}
//@property (strong, nonatomic) NSString *secondIos;
- (void)setSecondIos:(NSString *)secondIos{
    if ([secondIos isKindOfClass:[NSNull class]]) {
        _secondIos = @"暂无";
    }else{
        _secondIos = secondIos;
    }
}
//@property (strong, nonatomic) NSString *advice;
- (void)setAdvice:(NSString *)advice{
    if ([advice isKindOfClass:[NSNull class]]) {
        _advice = @"暂无";
    }else{
        _advice = advice;
    }
}

-(void)setDoctor:(NSString *)doctor{
    if ([doctor isKindOfClass:[NSNull class]]) {
        _doctor = @"暂无";
    }else{
        _doctor = doctor;
    }
}
@end
