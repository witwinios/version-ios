//
//  RescueRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "RescueRecordController.h"
#import "THDatePickerView.h"
@interface RescueRecordController ()
{
    NSString *current_date;
    UIView *btnBack;
 
    NSNumber *has_image;
    
    UIView *toolbar;
    
    NSMutableArray *TeacherArray;
    NSMutableArray *TeacherIdArray;
    
    NSString *TeacherStr;
    NSString *TeacherIdStr;
    
    UIView *BackView;
    UIButton *BackBtn;
    
    UIButton *okBtn;
    UIButton *noBtn;
    
    UIPickerView *Picker;
    NSString *TeacherId;
}
@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) UIButton *btn;

@end

@implementation RescueRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    current_date = @"";
    has_image = @0;
    TeacherStr=@"请选择上级医生";
    if ([SubjectTool share].subjectType == subjectTypeUpdate) {
        self.oneField.text = self.myModel.caseRescueModelDiseaseName;
        self.twoField.text = self.myModel.caseRescueModelDiseaserName;
        self.threeField.text = [NSString stringWithFormat:@"%@",self.myModel.caseRescueModelDiseaseNum];
        self.contentTextview.text = self.myModel.caseRescueModelBackDescription;
        self.contentTextview1.text = self.myModel.caseRescueModelDescription;
        [self.TeacherBtn setTitle:self.myModel.doctorFullName forState:UIControlStateNormal];
        
        if (self.myModel.caseRescueModelDate.intValue == 0) {
            current_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseRescueModelDate];
            [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:self.myModel.caseRescueModelDate.stringValue] forState:0];
        }
    }

    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonHide:) name:UIKeyboardWillHideNotification object:nil];
    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];
    self.dateBtn.layer.cornerRadius = 4;
    self.dateBtn.layer.masksToBounds = YES;
    self.dateBtn.titleLabel.textAlignment = 0;
    self.dateBtn.layer.cornerRadius = 4;
    THDatePickerView *dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        NSLog(@"%@-%@",date,longDate);
        [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:longDate] forState:0];
        current_date = longDate;
    } andCancelBlock:^{
        self.btn.hidden = YES;
    } dateType:DateTypeDay];
    
    [self.view addSubview:dateView];
    self.dateView = dateView;

    [self setUI];
}

-(void)loadData{
    TeacherArray =[NSMutableArray array];
    TeacherIdArray=[NSMutableArray array];
    NSString *Url=[NSString stringWithFormat:@"%@/users/teachers",LocalIP];
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            
            return;
        }
        [TeacherArray removeAllObjects];
        
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            
            for (int i=0; i<array.count; i++) {
                NSDictionary *dictionary = [array objectAtIndex:i];
                TeacherModel *model=[TeacherModel new];
                model.TeacherName=[dictionary objectForKey:@"fullName"];
                model.TeacherId=[dictionary objectForKey:@"userId"];
                [TeacherArray addObject:model.TeacherName];
                [TeacherIdArray addObject:model.TeacherId];
                
            }
        }
        
        
        
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
    }];
}


- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"抢救记录";
    [backNavigation addSubview:titleLab];
    
    
    [self.TeacherBtn addTarget:self action:@selector(GetTeacher) forControlEvents:UIControlEventTouchUpInside];
    
}
- (void)resignFirstRespond{
    [self.oneField resignFirstResponder];
    [self.twoField resignFirstResponder];
    [self.threeField resignFirstResponder];
    [self.contentTextview resignFirstResponder];
    [self.contentTextview1 resignFirstResponder];
}
- (void)hideKb:(UIButton *)btn{
    [self resignFirstRespond];
}
-(void)doneButtonshow: (NSNotification *)notification {
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
}
- (void)doneButtonHide:(NSNotification *)notification{
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
    }];
}

-(void)GetTeacher{
    [self resignFirstRespond];
    
    CGFloat BackView_Y=Sheight/3;
    
    BackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    BackView.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.4];
    
    //工具栏：
    Picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0,Sheight-BackView_Y , Swidth, BackView_Y)];
    UIView *ToolView=[[UIView alloc]initWithFrame:CGRectMake(0, Picker.wwy_y-30, Swidth, 30)];
    ToolView.backgroundColor=UIColorFromHex(0x20B2AA);
    [BackView addSubview:ToolView];
    
    okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    okBtn.frame=CGRectMake(Swidth-100, 0, 100, 30);
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(setOkBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:okBtn];
    
    noBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    noBtn.frame=CGRectMake(0, 0, 100, 30);
    [noBtn setTitle:@"取消" forState:UIControlStateNormal];
    [noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [noBtn addTarget:self action:@selector(setNoBtn) forControlEvents:UIControlEventTouchUpInside];
    [ToolView addSubview:noBtn];
    
    Picker.backgroundColor=[UIColor whiteColor];
    
    TeacherIdStr=TeacherIdArray[0];
    TeacherStr=TeacherArray[0];
    
    Picker.delegate=self;
    Picker.dataSource=self;
    
    
    [BackView addSubview:Picker];
    
    [[UIApplication sharedApplication].keyWindow addSubview:BackView];
    
}

-(void)setOkBtn{
    [self.TeacherBtn setTitle:TeacherStr forState:UIControlStateNormal];
    BackView.hidden=YES;
}

-(void)setNoBtn{
    BackView.hidden=YES;
}


#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)saveAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"名称不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"姓名不能为空~" places:0 toView:nil];
        return;
    }
    if ([_threeField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"病历号不能为空~" places:0 toView:nil];
        return;
    }
    if (current_date.length == 0 ) {
        [MBProgressHUD showToastAndMessage:@"日期不能为空~" places:0 toView:nil];
        return;
    }
    

   
    if (has_image.intValue == 0) {
        [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
        SdepartRecordController *departVC = [SdepartRecordController shareObject];
        
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentRescueRecords",SimpleIp];
        NSDictionary *params;
        
        if (![self.TeacherBtn.titleLabel.text isEqualToString:@"请选择上级医生"]) {
            params = @{@"diseaseName":_oneField.text,
                                     @"patientName":_twoField.text,
                                     @"caseNo":_threeField.text,
                                     @"rescueTime":current_date,
                                     @"outcome":_contentTextview.text,
                                     @"description":_contentTextview1.text,
                                     @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,
                                     @"doctorId":TeacherIdStr
                                     };
        }else{
            params = @{@"diseaseName":_oneField.text,
                       @"patientName":_twoField.text,
                       @"caseNo":_threeField.text,
                       @"rescueTime":current_date,
                       @"outcome":_contentTextview.text,
                       @"description":_contentTextview1.text,
                       @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,
                       
                       };
        }
        
        NSLog(@"params==%@",params);
        
        
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];

            }else{
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
        [RequestTools RequestWithFile:_currentImage.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentRescueRecords",SimpleIp];
            NSDictionary *params;
            
            if (![self.TeacherBtn.titleLabel.text isEqualToString:@"请选择上级医生"]) {
             params = @{@"diseaseName":_oneField.text,
                                     @"patientName":_twoField.text,
                                     @"caseNo":_threeField.text,
                                     @"rescueTime":current_date,
                                     @"outcome":_contentTextview.text,
                                     @"description":_contentTextview1.text,
                                     @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,
                                     @"fileId":fileId,
                                     @"doctorId":TeacherIdStr};
            }else{
                params = @{@"diseaseName":_oneField.text,
                           @"patientName":_twoField.text,
                           @"caseNo":_threeField.text,
                           @"rescueTime":current_date,
                           @"outcome":_contentTextview.text,
                           @"description":_contentTextview1.text,
                           @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,
                           @"fileId":fileId,
                           };
            }

            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
                }else{
                    [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];

        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }

}
- (void)updateAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"名称不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"姓名不能为空~" places:0 toView:nil];
        return;
    }
    if ([_threeField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"病历号不能为空~" places:0 toView:nil];
        return;
    }
    if (current_date.length == 0) {
        [MBProgressHUD showToastAndMessage:@"日期不能为空~" places:0 toView:nil];
        return;
    }


     NSLog(@"%@",self.TeacherBtn.titleLabel.text);
    
    if ([TeacherStr isEqualToString:@"暂无"]) {
        [MBProgressHUD showToastAndMessage:@"请选择上级医师" places:0 toView:nil];
    }
    
    [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
    if (has_image.intValue == 0) {
        SdepartRecordController *departVC = [SdepartRecordController shareObject];

        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentRescueRecords/%@",SimpleIp,self.myModel.recordId];
        NSDictionary *params;
        
        NSLog(@"TeacherStr===%@",TeacherStr);
        
        if ([self.TeacherBtn.titleLabel.text isEqualToString:@"暂无"]) {
       params = @{@"diseaseName":_oneField.text,
                                 @"patientName":_twoField.text,
                                 @"caseNo":_threeField.text,
                                 @"rescueTime":current_date,
                                 @"outcome":_contentTextview.text,
                                 @"description":_contentTextview1.text,
                                 @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,
                  
                  
                                 };
        }else{
            params = @{@"diseaseName":_oneField.text,
                       @"patientName":_twoField.text,
                       @"caseNo":_threeField.text,
                       @"rescueTime":current_date,
                       @"outcome":_contentTextview.text,
                       @"description":_contentTextview1.text,
                       @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,
                       @"doctorId":TeacherIdStr
                       };
        }
        [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
            self.myModel = nil;
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
        [RequestTools RequestWithFile:_currentImage.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];

            SdepartRecordController *departVC = [SdepartRecordController shareObject];

            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentRescueRecords/%@",SimpleIp,self.myModel.recordId];
            NSDictionary *params;
            
            if (![self.TeacherBtn.titleLabel.text isEqualToString:@"暂无"]) {
            params = @{@"diseaseName":_oneField.text,
                                     @"patientName":_twoField.text,
                                     @"caseNo":_threeField.text,
                                     @"rescueTime":current_date,
                                     @"outcome":_contentTextview.text,
                                     @"description":_contentTextview1.text,
                                     @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,
                                     @"doctorId":TeacherIdStr,
                                     @"fileId":fileId
                                    
                                     };
            }else{
                params = @{@"diseaseName":_oneField.text,
                           @"patientName":_twoField.text,
                           @"caseNo":_threeField.text,
                           @"rescueTime":current_date,
                           @"outcome":_contentTextview.text,
                           @"description":_contentTextview1.text,
                           @"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId,
                           @"fileId":fileId
                           
                           };
            }
            [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
                self.myModel = nil;
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
                }else{
                    [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];

        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }

}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}
- (void)popLast{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveAction:(id)sender {
    
    if ([SubjectTool share].subjectType == subjectTypeAdd) {
        [self saveAction];
    }else{
        
        [self updateAction];
    }

}

- (IBAction)dateAction:(id)sender {
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];

}
- (IBAction)fileAction:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相册",@"拍照", nil];
    [sheet showInView:self.view];

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self getAlbumAction];
            break;
        case 1:
            [self photoAction];
            break;
        default:
            break;
    }
}
- (void)photoAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePickController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)getAlbumAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    [picker dismissViewControllerAnimated:YES completion:nil];
    has_image = @1;
    _currentImage.image = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
}

#pragma mark pickView
//返回数据列数：
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    
        return [TeacherArray count];
    
}
//获取数据
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
  
        return [TeacherArray objectAtIndex:row];
   
}
//返回值：
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    
    
        NSLog(@"%@",[TeacherArray objectAtIndex:row]);
        TeacherStr = [TeacherArray objectAtIndex:row];
        
        TeacherIdStr=[TeacherIdArray objectAtIndex:row];
        NSLog(@"%@",TeacherIdStr);
  
   
}

@end
