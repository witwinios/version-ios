//
//  AutoLogin.h
//  yun
//
//  Created by MacAir on 2017/8/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSuserDefaultManager.h"
#import "RequestTools.h"
#import "SPKitExample.h"
#import "AccountEntity.h"
typedef enum
{
    requestStatusSucess,
    requestStatusFailed,
}requestStatus;
typedef void (^StatusBlock)(requestStatus);
@interface AutoLogin : NSObject
@property (strong,nonatomic) StatusBlock statusBlock;
+ (id)share;
- (void)autoLogin;
@end
