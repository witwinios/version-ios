//
//  SDOExamController.m
//  huiyun
//
//  Created by MacAir on 2017/11/29.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "SDOExamController.h"
@interface SDOExamController ()
{
    CGFloat choiceLab_H;
    CGFloat questionTitle_H;
    CGFloat questionType_H;
    
    CGFloat downInternal;
    //页码
    int pageNum;
    //选择题角标
    int choiceNum;
    NSMutableArray *dataArray;
    NSMutableArray *formatArray;
    NSMutableArray *scroll_X_Array;
    NSMutableArray *manegerArray;
    //
    UITextField *currentField;
    UITextView *currentText;
    //记录是否做题
    NSMutableArray *indexArray;
    //
    NSArray *parrentB1Optons;
    //键盘高度
    CGFloat keyboard_H;
    //
    CGRect originTextview;
    //
    NSTimer *timer;
    //
    MZTimerLabel *_mzLabel;
}
@end

@implementation SDOExamController
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [_mzLabel pause];
}
- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, Sheight)];
        _scrollView.alpha = 0.5;
        _scrollView.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:_scrollView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideView:)];
        tap.numberOfTapsRequired = 1;
        [_scrollView addGestureRecognizer:tap];
    }
    return _scrollView;
}
- (void)buildUI{
    //
    choiceLab_H = 18;
    questionTitle_H = 20;
    questionType_H = 25;
    pageNum = 1;
    choiceNum = 0;
    scroll_X_Array = [NSMutableArray new];
    manegerArray = [NSMutableArray new];
    indexArray = [NSMutableArray new];
    dataArray = [NSMutableArray new];
    formatArray = [NSMutableArray new];
    self.submitBtn.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 100, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"答题卷";
    [backNavigation addSubview:titleLab];
    
    UILabel *indexLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth-100, 29.5, 90, 30)];
    indexLab.adjustsFontSizeToFitWidth = YES;
    indexLab.tag = 201;
    indexLab.textColor = [UIColor whiteColor];
    indexLab.textAlignment = NSTextAlignmentRight;
    [backNavigation addSubview:indexLab];
    //关闭左滑
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self loadData];
}
//
- (void)viewDidLoad {
    [super viewDidLoad];
    [self buildUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (higher:) name: UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (smaller:) name: UIKeyboardDidHideNotification object:nil];
}
- (void)loadData{
    
    //开始考试
    NSString *request = [NSString stringWithFormat:@"%@/testSchedules/%@/students/%@/start",LocalIP,_model.onlineId,LocalUserId];
    
    [RequestTools RequestWithURL:request Method:@"post" Params:nil Success:^(NSDictionary *result) {
        double actueStartTime = [result[@"responseBody"][@"actualStartTime"] doubleValue]/1000;
        double actueEndTime =  actueStartTime + _model.onlineInternal.floatValue * 60;
        
        double currentStamp = (double)[[NSDate date] timeIntervalSince1970];
        
        if (_isFirst.intValue == 0) {
            downInternal = _model.onlineInternal.floatValue * 60;
        }else{
            downInternal = actueEndTime - currentStamp;
        }
        
        //开始计时
        if (downInternal < 0) {
            [MBProgressHUD showToastAndMessage:@"你的考试时间已过~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(pop) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            return;
        }
        self.timeLabel.textColor = UIColorFromHex(0x20B2AA);
        self.timeLabel.text = @"00:00:00";
        _mzLabel =[[MZTimerLabel alloc] initWithLabel:self.timeLabel andTimerType:MZTimerLabelTypeTimer];
        [_mzLabel setCountDownTime:downInternal];
        [_mzLabel startWithEndingBlock:^(NSTimeInterval countTime) {
            //倒计时结束
            [MBProgressHUD showToastAndMessage:@"时间已到,在为您自动提交~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(autoSubmit) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
        }];
        //获取试卷内容
        NSString *requestUrl = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/responseAnswersTemp?pageSize=999&order=asc",LocalIP,_model.onlineId,LocalUserId];
        [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"" Success:^(NSDictionary *result) {
            NSArray *array = result[@"responseBody"][@"result"];
            for (int i =0; i<array.count; i++) {
                NSDictionary *dictionary = array[i];
                QuestionModel *model = [QuestionModel new];
                model.questionId = [dictionary objectForKey:@"questionId"];
                model.questionType = [dictionary objectForKey:@"questionType"];
                model.questTitle = [dictionary objectForKey:@"questionTitle"];
                model.choiceOptions = [dictionary objectForKey:@"choiceOptions"];
                //是否有父类
                if ([[dictionary objectForKey:@"parentQuestionId"] isKindOfClass:[NSNull class]]) {
                    model.parrentId = [NSNumber numberWithInt:0];
                }else{
                    model.parrentId = [dictionary objectForKey:@"parentQuestionId"];
                }
                model.childNum = [dictionary objectForKey:@"childrenQuestionsNum"];
                //图片
                if (![[dictionary objectForKey:@"questionFile"] isKindOfClass:[NSNull class]]) {
                    model.questionFile = [NSString stringWithFormat:@"%@%@?CTTS-Token=%@",SimpleIp,[[dictionary objectForKey:@"questionFile"] objectForKey:@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
                }else{
                    model.questionFile = @"";
                }
                NSArray *answerArr = [dictionary objectForKey:@"responseAnswer"];
                //填空题的答案默认为@""
                model.answerArray = [NSMutableArray arrayWithArray:answerArr];
                if ([model.questionType isEqualToString:@"填空题"]) {
                    model.answerFieldArr = [NSMutableArray new];
                    for (int i = 0; i < model.questTitle.length; i ++) {
                        if ([model.questTitle characterAtIndex:i] == '[') {
                            [model.answerFieldArr addObject:@""];
                        }
                    }
                }
                if (model.childNum.intValue == 0) {
                    if (model.answerArray.count == 0) {
                        [indexArray addObject:@0];
                    }else{
                        [indexArray addObject:@1];
                    }
                }
                [dataArray addObject:model];
            }
            //重组数组
            [self formatDataArr];
            //制作视图
            [self createUI];
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"请求失败~" places:0 toView:nil];
        }];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"开始失败~" places:0 toView:nil];
        [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(pop) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
    }];
}
//自动交卷
- (void)autoSubmit{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/submitResponseAnswers",LocalIP,_model.onlineId,LocalUserId];
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:nil Message:@"交卷中~" Success:^(NSDictionary *result) {
        [MBProgressHUD showToastAndMessage:@"已经提交答卷~" places:0 toView:nil];
        [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(pop) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"提交失败~" places:0 toView:nil];
    }];
}
- (void)pop{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)formatDataArr{
    for (int i=0; i<dataArray.count; i++) {
        QuestionModel *model = dataArray[i];
        
        if ([model.questionType isEqualToString:@"A3题型"] || [model.questionType isEqualToString:@"A4题型"] || [model.questionType isEqualToString:@"B1题型"]) {
            //为父题
            if (model.parrentId.intValue == 0) {
                model.childArray = [NSMutableArray new];
                for (int s=0; s<dataArray.count; s++) {
                    QuestionModel *childModel = dataArray[s];
                    if (childModel.parrentId.intValue == model.questionId.intValue) {
                        [model.childArray addObject:childModel];
                    }
                }
                [formatArray addObject:model];
            }
        }else{
            [formatArray addObject:model];
        }
    }
}
- (void)createFormatUI{
    _mainScroll.contentSize = CGSizeMake(Swidth * 3, _mainScroll.contentSize.height);
    _mainScroll.delegate = self;
    _mainScroll.bounces = NO;
    _mainScroll.showsHorizontalScrollIndicator = NO;
    
    _leftScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Swidth, _mainScroll.frame.size.height)];
    _centerScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(Swidth, 0, Swidth, _mainScroll.frame.size.height)];
    _rightScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(Swidth *2, 0, Swidth, _mainScroll.frame.size.height)];
    [_mainScroll addSubview:_leftScroll];
    [_mainScroll addSubview:_rightScroll];
    [_mainScroll addSubview:_centerScroll];
    //定位中间
    _mainScroll.contentOffset = CGPointMake(Swidth, 0);
    
    for (int i=0; i<dataArray.count; i++) {
        if (i == 0) {
            
        }
    }
    
    for (int i = 0; i < formatArray.count; i ++) {
        //        QuestionModel *model = formatArray[i];
        //        if ([model.questionType isEqualToString:@"A3题型"] || [model.questionType isEqualToString:@"A4题型"] || [model.questionType isEqualToString:@"B1题型"]) {
        //            [[QuesTools shareInstance] createPUI:model];
        //        }else{
        //            [[QuesTools shareInstance] createSUI:model];
        //        }
        
    }
}
- (void)createMyUI{
    _mainScroll.contentSize = CGSizeMake(formatArray.count * Swidth, _mainScroll.contentSize.height);
    _mainScroll.delegate = self;
    _mainScroll.bounces = NO;
    _mainScroll.showsHorizontalScrollIndicator = NO;
    
    for (int i = 0; i < formatArray.count; i++) {
        UIScrollView *pageScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(i * Swidth, 0, Swidth, Sheight - 64 - 40 - 40)];
        pageScroll.bounces = NO;
        pageScroll.showsVerticalScrollIndicator = NO;
        pageScroll.showsHorizontalScrollIndicator = NO;
        [_mainScroll addSubview:pageScroll];
        //
        QuestionModel *model = dataArray[i];
        //控件开始坐标
        CGFloat current_y = 5;
        if ([model.questionType isEqualToString:@"A1题型"] || [model.questionType isEqualToString:@"A2题型"]) {
            NSString *titleText = [NSString stringWithFormat:@"%@",model.questionType];
            CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
            
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
            
            current_y = current_y + height;
            
            titleLab.numberOfLines = 0;
            titleLab.text = titleText;
            titleLab.font = [UIFont systemFontOfSize:questionType_H];
            titleLab.textColor = UIColorFromHex(0x20B2AA);
            
            [pageScroll addSubview:titleLab];
            //说明和题干的距离
            current_y += 20;
            
            //题干
            NSString *quesString = [NSString stringWithFormat:@"%d: %@",pageNum,model.questTitle];
            CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
            
            UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
            current_y = current_y + ques_H;
            quesTitleLab.numberOfLines = 0;
            quesTitleLab.text = quesString;
            quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
            quesTitleLab.textColor = [UIColor blackColor];
            
            [pageScroll addSubview:quesTitleLab];
            
            current_y += 10;
            //选项
            ButtonManeger *maneger = [ButtonManeger new];
            NSArray *choiceOptionArray = model.choiceOptions;
            for (int j = 0; j < choiceOptionArray.count; j ++) {
                SelectButton *choiceBtn = [SelectButton buttonWithType:UIButtonTypeCustom];
                [choiceBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                [choiceBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                if (model.answerArray.count != 0) {
                    if ([model.answerArray[0] intValue] == j) {
                        choiceBtn.selected = YES;
                    }
                }
                choiceBtn.model = model;
                choiceBtn.currentOption = [NSNumber numberWithInt:j];
                choiceBtn.currentIndex = [NSNumber numberWithInt:choiceNum];
                choiceBtn.quesIndex = [NSNumber numberWithInt:pageNum - 1];
                
                [choiceBtn addTarget:self action:@selector(choiceAction:) forControlEvents:UIControlEventTouchUpInside];
                [maneger add:choiceBtn];
                
                
                NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                
                CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                
                UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                
                choiceBtn.frame = CGRectMake(5, choiceLab.frame.origin.y, 25, 25);
                choiceLab.text = choiceString;
                choiceLab.numberOfLines = 0;
                
                current_y = current_y + choice_H;
                
                choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                choiceLab.textColor = [UIColor lightGrayColor];
                
                [pageScroll addSubview:choiceLab];
                [pageScroll addSubview:choiceBtn];
                //选项与选项之间的距离
                current_y += 5;
            }
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                //                [imgView sd_setImageWithURL:@"" placeholderImage:@"" completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                //
                //                }];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            //题目坐标
            [manegerArray addObject:maneger];
            pageNum ++;
            choiceNum ++;
            //
            [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
        }else if ([model.questionType isEqualToString:@"A3题型"] || [model.questionType isEqualToString:@"A4题型"]){
            
        }else if ([model.questionType isEqualToString:@"B1题型"]){
            
        }else if ([model.questionType isEqualToString:@"填空题"]){
            
        }else if ([model.questionType isEqualToString:@"解答题"]){
            
        }else if ([model.questionType isEqualToString:@"X: 多选题"]){
            
        }
    }
}
- (void)createUI {
    UILabel *indexLab = (UILabel *)[self.view viewWithTag:201];
    indexLab.text = [NSString stringWithFormat:@"%.0f/%ld页",_mainScroll.contentOffset.x/Swidth+1,dataArray.count];
    _mainScroll.contentSize = CGSizeMake(dataArray.count * Swidth, _mainScroll.contentSize.height);
    _mainScroll.delegate = self;
    _mainScroll.bounces = NO;
    _mainScroll.showsHorizontalScrollIndicator = NO;
    for (int i = 0; i < dataArray.count; i ++) {
        UIScrollView *pageScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(i * Swidth, 0, Swidth, Sheight - 64 - 40 - 40)];
        pageScroll.bounces = NO;
        pageScroll.showsVerticalScrollIndicator = NO;
        pageScroll.showsHorizontalScrollIndicator = NO;
        [_mainScroll addSubview:pageScroll];
        //
        QuestionModel *model = dataArray[i];
        //创建控件的角标
        CGFloat current_y = 5;
        if ([model.questionType isEqualToString:@"A1选择题"] || [model.questionType isEqualToString:@"A1题型"] || [model.questionType isEqualToString:@"A2题型"]) {
            
            NSString *titleText = [NSString stringWithFormat:@"%@",model.questionType];
            CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
            
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
            
            current_y = current_y + height;
            
            titleLab.numberOfLines = 0;
            titleLab.text = titleText;
            titleLab.font = [UIFont systemFontOfSize:questionType_H];
            titleLab.textColor = UIColorFromHex(0x20B2AA);
            
            [pageScroll addSubview:titleLab];
            //说明和题干的距离
            current_y += 20;
            
            //题干
            NSString *quesString = [NSString stringWithFormat:@"%d: %@",pageNum,model.questTitle];
            CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
            
            UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
            current_y = current_y + ques_H;
            quesTitleLab.numberOfLines = 0;
            quesTitleLab.text = quesString;
            quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
            quesTitleLab.textColor = [UIColor blackColor];
            
            [pageScroll addSubview:quesTitleLab];
            
            current_y += 10;
            //选项
            ButtonManeger *maneger = [ButtonManeger new];
            NSArray *choiceOptionArray = model.choiceOptions;
            for (int j = 0; j < choiceOptionArray.count; j ++) {
                SelectButton *choiceBtn = [SelectButton buttonWithType:UIButtonTypeCustom];
                [choiceBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                [choiceBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                if (model.answerArray.count != 0) {
                    if ([model.answerArray[0] intValue] == j) {
                        choiceBtn.selected = YES;
                    }
                }
                choiceBtn.model = model;
                
                choiceBtn.currentOption = [NSNumber numberWithInt:j];
                choiceBtn.currentIndex = [NSNumber numberWithInt:choiceNum];
                choiceBtn.quesIndex = [NSNumber numberWithInt:pageNum - 1];
                
                [choiceBtn addTarget:self action:@selector(choiceAction:) forControlEvents:UIControlEventTouchUpInside];
                [maneger add:choiceBtn];
                
                
                NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                
                CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                
                UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                
                choiceBtn.frame = CGRectMake(5, choiceLab.frame.origin.y, 25, 25);
                choiceLab.text = choiceString;
                choiceLab.numberOfLines = 0;
                
                current_y = current_y + choice_H;
                
                choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                choiceLab.textColor = [UIColor lightGrayColor];
                
                [pageScroll addSubview:choiceLab];
                [pageScroll addSubview:choiceBtn];
                //选项与选项之间的距离
                current_y += 10;
            }
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            //答案保存情况
            //题目坐标
            [manegerArray addObject:maneger];
            pageNum ++;
            choiceNum ++;
            //
            [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
        }else if ([model.questionType isEqualToString:@"A3题型"] || [model.questionType isEqualToString:@"A4题型"]){
            if (model.parrentId.intValue == 0) {
                NSString *titleText = [NSString stringWithFormat:@"%@ (答题说明：以下提供若干个案例，每个案例下设若干道试题。请根据案例提供的信息，在每道试题下面的多个备选答案中选择一个最佳答案。)",model.questionType];
                CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
                current_y = current_y + height;
                
                titleLab.numberOfLines = 0;
                titleLab.text = titleText;
                titleLab.font = [UIFont systemFontOfSize:questionType_H];
                titleLab.textColor = UIColorFromHex(0x20B2AA);
                
                [pageScroll addSubview:titleLab];
                //说明和题干的距离
                current_y += 20;
            }else{
                NSString *titleText = [NSString stringWithFormat:@"(%@)",model.questionType];
                CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
                current_y = current_y + height;
                
                titleLab.numberOfLines = 0;
                titleLab.text = titleText;
                titleLab.font = [UIFont systemFontOfSize:questionType_H];
                titleLab.textColor = UIColorFromHex(0x20B2AA);
                
                [pageScroll addSubview:titleLab];
                //说明和题干的距离
                current_y += 20;
                
            }
            //题干
            if (model.parrentId.intValue == 0) {
                NSString *quesString = [NSString stringWithFormat:@"题干:  %@",model.questTitle];
                CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
                
                UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
                current_y = current_y + ques_H;
                quesTitleLab.numberOfLines = 0;
                quesTitleLab.text = quesString;
                quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
                quesTitleLab.textColor = [UIColor blackColor];
                
                [pageScroll addSubview:quesTitleLab];
                
            }else{
                NSString *quesString = [NSString stringWithFormat:@"%d: %@",pageNum,model.questTitle];
                CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
                
                UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
                current_y = current_y + ques_H;
                quesTitleLab.numberOfLines = 0;
                quesTitleLab.text = quesString;
                quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
                quesTitleLab.textColor = [UIColor blackColor];
                
                [pageScroll addSubview:quesTitleLab];
                
                
                [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
                //题干和选项的距离
                current_y += 10;
                //选项
                ButtonManeger *maneger = [ButtonManeger new];
                NSArray *choiceOptionArray = model.choiceOptions;
                for (int j = 0; j < choiceOptionArray.count; j ++) {
                    SelectButton *choiceBtn = [SelectButton buttonWithType:UIButtonTypeCustom];
                    [choiceBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                    [choiceBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                    
                    if (model.answerArray.count != 0) {
                        if ([model.answerArray[0] intValue] == j) {
                            choiceBtn.selected = YES;
                        }
                    }
                    
                    choiceBtn.model = model;
                    choiceBtn.currentOption = [NSNumber numberWithInt:j];
                    choiceBtn.currentIndex = [NSNumber numberWithInt:choiceNum];
                    
                    choiceBtn.quesIndex = [NSNumber numberWithInt:pageNum - 1];
                    
                    [choiceBtn addTarget:self action:@selector(choiceAction:) forControlEvents:UIControlEventTouchUpInside];
                    [maneger add:choiceBtn];
                    
                    
                    
                    NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                    
                    CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                    
                    UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                    choiceBtn.frame = CGRectMake(5, choiceLab.frame.origin.y, 25, 25);
                    
                    choiceLab.text = choiceString;
                    choiceLab.numberOfLines = 0;
                    
                    current_y = current_y + choice_H;
                    
                    choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                    choiceLab.textColor = [UIColor lightGrayColor];
                    
                    [pageScroll addSubview:choiceLab];
                    [pageScroll addSubview:choiceBtn];
                    //选项与选项之间的距离
                    current_y += 10;
                    
                }
                //题目坐标
                pageNum ++;
                choiceNum ++;
                [manegerArray addObject:maneger];
                
            }
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
        }else if ([model.questionType isEqualToString:@"B1题型"]){
            if (model.parrentId.intValue == 0) {
                NSString *titleText = [NSString stringWithFormat:@"%@ (答题说明：以下提供若干个案例，每个案例下设若干道试题。请根据案例提供的信息，在每道试题下面的多个备选答案中选择一个最佳答案。)",model.questionType];
                CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
                current_y = current_y + height;
                
                titleLab.numberOfLines = 0;
                titleLab.text = titleText;
                titleLab.font = [UIFont systemFontOfSize:questionType_H];
                titleLab.textColor = UIColorFromHex(0x20B2AA);
                
                [pageScroll addSubview:titleLab];
                //说明和题干的距离
                current_y += 20;
            }else{
                NSString *titleText = [NSString stringWithFormat:@"(%@)",model.questionType];
                CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
                current_y = current_y + height;
                
                titleLab.numberOfLines = 0;
                titleLab.text = titleText;
                titleLab.font = [UIFont systemFontOfSize:questionType_H];
                titleLab.textColor = UIColorFromHex(0x20B2AA);
                
                [pageScroll addSubview:titleLab];
                //说明和题干的距离
                current_y += 20;
            }
            //题干
            if (model.parrentId.intValue == 0) {
                //选项
                parrentB1Optons = model.choiceOptions;
                NSArray *choiceOptionArray = model.choiceOptions;
                for (int j = 0; j < choiceOptionArray.count; j ++) {
                    NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                    CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                    UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                    choiceLab.text = choiceString;
                    choiceLab.numberOfLines = 0;
                    current_y = current_y + choice_H;
                    choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                    choiceLab.textColor = [UIColor lightGrayColor];
                    [pageScroll addSubview:choiceLab];
                    //选项与选项之间的距离
                    current_y += 5;
                }
            }else{
                //
                NSString *quesString = [NSString stringWithFormat:@"%d: %@",pageNum,model.questTitle];
                CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
                
                UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
                current_y = current_y + ques_H;
                quesTitleLab.numberOfLines = 0;
                quesTitleLab.text = quesString;
                quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
                quesTitleLab.textColor = [UIColor blackColor];
                
                [pageScroll addSubview:quesTitleLab];
                //题干和选项的距离
                current_y += 10;
                //选项
                ButtonManeger *maneger = [ButtonManeger new];
                NSArray *choiceOptionArray = parrentB1Optons;
                for (int j = 0; j < choiceOptionArray.count; j ++) {
                    SelectButton *choiceBtn = [SelectButton buttonWithType:UIButtonTypeCustom];
                    [choiceBtn setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                    [choiceBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
                    
                    if (model.answerArray.count != 0) {
                        if ([model.answerArray[0] intValue] == j) {
                            choiceBtn.selected = YES;
                        }
                    }
                    
                    choiceBtn.model = model;
                    choiceBtn.currentOption = [NSNumber numberWithInt:j];
                    choiceBtn.currentIndex = [NSNumber numberWithInt:choiceNum];
                    choiceBtn.quesIndex = [NSNumber numberWithInt:pageNum - 1];
                    
                    
                    
                    [choiceBtn addTarget:self action:@selector(choiceAction:) forControlEvents:UIControlEventTouchUpInside];
                    [maneger add:choiceBtn];
                    
                    
                    NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                    
                    CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                    
                    UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                    choiceBtn.frame = CGRectMake(5, choiceLab.frame.origin.y, 25, 25);
                    
                    choiceLab.text = choiceString;
                    choiceLab.numberOfLines = 0;
                    
                    current_y = current_y + choice_H;
                    
                    choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                    choiceLab.textColor = [UIColor lightGrayColor];
                    
                    [pageScroll addSubview:choiceLab];
                    if (model.childNum.intValue == 0) {
                        [pageScroll addSubview:choiceBtn];
                    }
                    //选项与选项之间的距离
                    current_y += 10;
                }
                //
                [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
                //
                [manegerArray addObject:maneger];
                pageNum ++;
                choiceNum ++;
            }
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
            
        }else if ([model.questionType isEqualToString:@"填空题"]){
            NSString *titleText = @"    填空题:";
            CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth];
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
            current_y = current_y + height;
            
            titleLab.numberOfLines = 0;
            titleLab.text = titleText;
            titleLab.font = [UIFont systemFontOfSize:questionTitle_H];
            titleLab.textColor = UIColorFromHex(0x20B2AA);
            [pageScroll addSubview:titleLab];
            //填空题和题目距离
            current_y += 10;
            //题目
            NSString *quesString = [NSString stringWithFormat:@"%d: %@",pageNum,model.questTitle];
            CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-10];
            
            UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
            current_y = current_y + ques_H;
            quesTitleLab.numberOfLines = 0;
            quesTitleLab.text = quesString;
            quesTitleLab.font = [UIFont systemFontOfSize:16];
            quesTitleLab.textColor = [UIColor blackColor];
            
            [pageScroll addSubview:quesTitleLab];
            //
            [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
            //查询几个填空
            int fieldNum = 0;
            for (int i = 0; i < model.questTitle.length; i ++) {
                if ([model.questTitle characterAtIndex:i] == '[') {
                    fieldNum ++;
                }
            }
            //题目和field距离
            current_y += 10;
            //创建field
            for (int i = 0; i < fieldNum; i ++) {
                SelectField *field = [[SelectField alloc]initWithFrame:CGRectMake(5, current_y, Swidth-10, 30)];
                field.model = model;
                NSInteger answerCount = model.answerArray.count;
                if (i < answerCount) {
                    if (model.answerArray.count != 0) {
                        field.text = model.answerArray[i];
                    }else{
                        field.textAlignment = 1;
                        field.placeholder = [NSString stringWithFormat:@"第%d个空格",i + 1];
                    }
                }
                field.fieldNum = [NSNumber numberWithInt:i];
                field.quesIndex = [NSNumber numberWithInt:pageNum - 1];
                
                field.borderStyle = UITextBorderStyleRoundedRect;
                field.delegate = self;
                [pageScroll addSubview:field];
                if (i != fieldNum) {
                    current_y += 40;
                }
            }
            //题目坐标
            pageNum ++;
            //手势
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(fieldHide:)];
            tap.numberOfTapsRequired = 1;
            [pageScroll addGestureRecognizer:tap];
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
        }else if ([model.questionType isEqualToString:@"解答题"]){
            NSString *titleText = @"    解答题:";
            CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:25] andWidth:Swidth];
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
            current_y = current_y + height;
            
            titleLab.numberOfLines = 0;
            titleLab.text = titleText;
            titleLab.font = [UIFont systemFontOfSize:25];
            titleLab.textColor = UIColorFromHex(0x20B2AA);
            [pageScroll addSubview:titleLab];
            //题型和题目距离
            current_y += 10;
            
            //题目
            NSString *quesString = [NSString stringWithFormat:@"%d: %@",pageNum,model.questTitle];
            CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:16] andWidth:Swidth-10];
            
            UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
            current_y = current_y + ques_H;
            quesTitleLab.numberOfLines = 0;
            quesTitleLab.text = quesString;
            quesTitleLab.font = [UIFont systemFontOfSize:16];
            quesTitleLab.textColor = [UIColor blackColor];
            
            [pageScroll addSubview:quesTitleLab];
            //题目和答题区距离
            current_y += 10;
            SelectTextview *answerView = [[SelectTextview alloc]initWithFrame:CGRectMake(5, current_y, Swidth-10, 60)];
            answerView.model = model;
            if (model.answerArray.count != 0) {
                answerView.text = model.answerArray[0];
            }
            answerView.quesIndex = [NSNumber numberWithInt:pageNum - 1];
            answerView.delegate = self;
            answerView.backgroundColor = [UIColor whiteColor];
            answerView.layer.cornerRadius = 3;
            answerView.layer.masksToBounds = YES;
            answerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            answerView.layer.borderWidth = 1;
            [pageScroll addSubview:answerView];
            //
            pageNum ++;
            current_y += 70;
            //
            [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
            //手势
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textviewHide:)];
            tap.numberOfTapsRequired = 1;
            [pageScroll addGestureRecognizer:tap];
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
            
        }else if ([model.questionType isEqualToString:@"X: 多选题"]){
            NSString *titleText = @"X: 多选题";
            CGFloat height = [Maneger getPonentH:titleText andFont:[UIFont systemFontOfSize:questionType_H] andWidth:Swidth];
            
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, Swidth, height)];
            current_y = current_y + height;
            
            titleLab.numberOfLines = 0;
            titleLab.text = titleText;
            titleLab.font = [UIFont systemFontOfSize:questionType_H];
            titleLab.textColor = UIColorFromHex(0x20B2AA);
            
            [pageScroll addSubview:titleLab];
            //说明和题干的距离
            current_y += 20;
            
            //题干
            NSString *quesString = [NSString stringWithFormat:@"%d: %@",pageNum,model.questTitle];
            CGFloat ques_H = [Maneger getPonentH:quesString andFont:[UIFont systemFontOfSize:questionTitle_H] andWidth:Swidth-10];
            
            UILabel *quesTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(10, current_y, Swidth-10, ques_H)];
            current_y = current_y + ques_H;
            quesTitleLab.numberOfLines = 0;
            quesTitleLab.text = quesString;
            quesTitleLab.font = [UIFont systemFontOfSize:questionTitle_H];
            quesTitleLab.textColor = [UIColor blackColor];
            
            [pageScroll addSubview:quesTitleLab];
            
            current_y += 10;
            //选项
            NSArray *choiceOptionArray = model.choiceOptions;
            for (int j = 0; j < choiceOptionArray.count; j ++) {
                SelectButton *choiceBtn = [SelectButton buttonWithType:UIButtonTypeCustom];
                [choiceBtn setImage:[UIImage imageNamed:@"checkBox_off"] forState:UIControlStateNormal];
                [choiceBtn setImage:[UIImage imageNamed:@"checkBox_on"] forState:UIControlStateSelected];
                if (model.answerArray.count != 0) {
                    for (int s = 0; s<model.answerArray.count; s++) {
                        if ([model.answerArray[s] intValue] == j) {
                            choiceBtn.selected = YES;
                        }
                    }
                }
                choiceBtn.model = model;
                choiceBtn.currentOption = [NSNumber numberWithInt:j];
                
                choiceBtn.quesIndex = [NSNumber numberWithInt:pageNum - 1];
                
                
                [choiceBtn addTarget:self action:@selector(choiceAction:) forControlEvents:UIControlEventTouchUpInside];
                
                
                NSString *choiceString = [NSString stringWithFormat:@"%c: %@",j + 65,choiceOptionArray[j]];
                
                CGFloat choice_H = [Maneger getPonentH:choiceString andFont:[UIFont systemFontOfSize:choiceLab_H] andWidth:Swidth - 32 - 10];
                
                UILabel *choiceLab = [[UILabel alloc]initWithFrame:CGRectMake(32, current_y, Swidth - 32 - 10, choice_H)];
                choiceBtn.frame = CGRectMake(5, choiceLab.frame.origin.y, 23, 23);
                
                choiceLab.text = choiceString;
                choiceLab.numberOfLines = 0;
                
                current_y = current_y + choice_H;
                
                choiceLab.font = [UIFont systemFontOfSize:choiceLab_H];
                choiceLab.textColor = [UIColor lightGrayColor];
                
                [pageScroll addSubview:choiceLab];
                [pageScroll addSubview:choiceBtn];
                //选项与选项之间的距离
                current_y += 10;
            }
            //题目坐标
            pageNum ++;
            //
            [scroll_X_Array addObject:[NSString stringWithFormat:@"%f",pageScroll.frame.origin.x]];
            //图片
            if (![model.questionFile isEqualToString:@""]) {
                UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(Swidth/2-50, current_y, 100, 100)];
                [imgView sd_setImageWithURL:[NSURL URLWithString:model.questionFile] placeholderImage:[UIImage imageNamed:@"tab"]];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(biggerImage:)];
                tap.numberOfTapsRequired = 1;
                imgView.userInteractionEnabled = YES;
                [imgView addGestureRecognizer:tap];
                [pageScroll addSubview:imgView];
                current_y += 105;
            }
            //扩大宽度
            pageScroll.contentSize = CGSizeMake(Swidth, current_y);
        }
    }
}
#pragma -action
- (void)biggerImage:(UITapGestureRecognizer *)tap{
    //    UIImageView *imgView = (UIImageView *)tap.view;
    //
    //    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight - 144)];
    //    backView.backgroundColor = [UIColor blackColor];
    //    //    backView.alpha = 0.5;
    //    backView.userInteractionEnabled = YES;
    //    UITapGestureRecognizer *taps = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideImage:)];
    //    taps.numberOfTapsRequired = 1;
    //    [backView addGestureRecognizer:taps];
    //
    //    UIImageView *bigImgView = [[UIImageView alloc]initWithFrame:backView.frame];
    //    bigImgView.image = imgView.image;
    //    [backView addSubview:bigImgView];
    //    [tap.view.superview addSubview:backView];
    UIImageView *imgView = (UIImageView *)tap.view;
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    backView.backgroundColor = [UIColor blackColor];
    backView.backgroundColor = [UIColor colorWithWhite:0.f alpha:0.6];
    backView.userInteractionEnabled = YES;
    UITapGestureRecognizer *taps = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideImage:)];
    taps.numberOfTapsRequired = 1;
    [backView addGestureRecognizer:taps];
    
    UIImageView *bigImgView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 0, Swidth-10, Swidth-10)];
    bigImgView.center = CGPointMake(Swidth/2, Sheight/2);
    bigImgView.image = imgView.image;
    
    [backView addSubview:bigImgView];
    [self.view addSubview:backView];
}
- (void)hideImage:(UITapGestureRecognizer *)tap{
    [tap.view removeFromSuperview];
}
- (void)choiceAction:(SelectButton *)btn{
    btn.selected = !btn.selected;
    if (btn.selected) {
        QuestionModel *model = btn.model;
        if (![model.questionType isEqualToString:@"X: 多选题"]) {
            ButtonManeger *btnManeger =  (ButtonManeger *)manegerArray[[btn.currentIndex intValue]];
            [btnManeger changeStatus:btn.currentOption.longValue];
            NSLog(@"%@-%@",_model.onlineId,model.questionId);
            //保存答案
            NSString *url = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/saveResponseAnswers/%@",LocalIP,_model.onlineId,LocalUserId,model.questionId];
            [RequestTools RequestWithURL:url Method:@"post" Params:@[btn.currentOption] Success:^(NSDictionary *result) {
                
                if (![result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
                    btn.selected = !btn.selected;
                }else{
                    [indexArray replaceObjectAtIndex:btn.quesIndex.integerValue withObject:@1];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
                btn.selected = !btn.selected;

            }];
            
        }else {
            //多选题
            if (model.answerArray.count == 0) {
                [model.answerArray addObject:btn.currentOption.stringValue];
            }else{
                for (int i = 0; i<model.answerArray.count; i++) {
                    if (![model.answerArray containsObject:btn.currentOption.stringValue]) {
                        [model.answerArray addObject:btn.currentOption.stringValue];
                    }
                }
            }
            NSString *url = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/saveResponseAnswers/%@",LocalIP,_model.onlineId,LocalUserId,model.questionId];
            [RequestTools RequestWithURL:url Method:@"post" Params:model.answerArray Success:^(NSDictionary *result) {
                
                if (![result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
                    btn.selected = !btn.selected;

                }else{
                    [indexArray replaceObjectAtIndex:btn.quesIndex.integerValue withObject:@1];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
                btn.selected = !btn.selected;

            }];
            
        }
    }else {
        //按钮没选中
        QuestionModel *model = btn.model;
        if (![model.questionType isEqualToString:@"X: 多选题"]) {
            //保存答案
            [model.answerArray removeAllObjects];
            NSString *url = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/saveResponseAnswers/%@",LocalIP,_model.onlineId,LocalUserId,model.questionId];
            [RequestTools RequestWithURL:url Method:@"post" Params:@[] Success:^(NSDictionary *result) {
                
                if (![result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
                    btn.selected = !btn.selected;

                }else{
                    [indexArray replaceObjectAtIndex:btn.quesIndex.integerValue withObject:@0];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
                btn.selected = !btn.selected;

            }];
        }else{
            
            if ([model.answerArray containsObject:btn.currentOption.stringValue]) {
                [model.answerArray removeObject:btn.currentOption.stringValue];
            }
            
            NSString *url = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/saveResponseAnswers/%@",LocalIP,_model.onlineId,LocalUserId,model.questionId];
            [RequestTools RequestWithURL:url Method:@"post" Params:model.answerArray Success:^(NSDictionary *result) {
                
                if (![result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
                    btn.selected = !btn.selected;

                }else{
                    if (model.answerArray.count == 0) {
                        [indexArray replaceObjectAtIndex:btn.quesIndex.integerValue withObject:@0];
                    }else{
                        [indexArray replaceObjectAtIndex:btn.quesIndex.integerValue withObject:@1];
                    }
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
                btn.selected = !btn.selected;

            }];
        }
    }
}
- (void)hideView:(UITapGestureRecognizer *)tap{
    self.cardBtn.selected = !self.cardBtn.selected;
    [UIView animateWithDuration:0.3 animations:^{
        [self scrollView].frame = CGRectMake(0, Sheight, Swidth, Sheight);
    }];
}

int add(int num1 , int num2){
    return num1 + num2;
}
- (void)back{
    NSString *title;
    NSInteger noSelect = 0;
    for (NSString *obc in indexArray) {
        if ([obc intValue] == 0) {
            noSelect ++;
        }
    }
    if (noSelect != 0) {
        title = [NSString stringWithFormat:@"有%ld题未做,是否提交?",noSelect];
    }else{
        title = @"您已全部答题,是否提交?";
    }
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *canCel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *sureBtn = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *requestUrl = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/submitResponseAnswers",LocalIP,_model.onlineId,LocalUserId];
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:nil Message:@"" Success:^(NSDictionary *result) {
            [MBProgressHUD showToastAndMessage:@"已经提交答卷~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(pop) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"提交失败~" places:0 toView:nil];
        }];
    }];
    [alertVC addAction:canCel];
    [alertVC addAction:sureBtn];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (IBAction)submitAction:(id)sender {
    NSString *title;
    NSInteger noSelect = 0;
    for (NSString *obc in indexArray) {
        if ([obc intValue] == 0) {
            noSelect ++;
        }
    }
    if (noSelect != 0) {
        title = [NSString stringWithFormat:@"有%ld题未做,是否提交?",noSelect];
    }else{
        title = @"您已全部答题,是否提交?";
    }
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *canCel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *sureBtn = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *requestUrl = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/submitResponseAnswers",LocalIP,_model.onlineId,LocalUserId];
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:nil Message:@"" Success:^(NSDictionary *result) {
            [MBProgressHUD showToastAndMessage:@"已经提交答卷~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(pop) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"提交失败~" places:0 toView:nil];
        }];
    }];
    [alertVC addAction:canCel];
    [alertVC addAction:sureBtn];
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (IBAction)cardAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    UIScrollView  *questionView;
    if (btn.selected) {
        questionView = [UIScrollView new];
        questionView.frame = CGRectMake(0, 0,Swidth, Sheight);
        questionView.backgroundColor = [UIColor lightGrayColor];
        questionView.userInteractionEnabled = YES;
        questionView.alpha = 0.9;
        
        int tags = pageNum - 1;
        for (int i=0; i<tags; i++) {
            int j = i/4;
            UIView *view = [UIView new];
            view.userInteractionEnabled = YES;
            CGPoint point;
            view.frame = CGRectMake(Swidth/4*(i%4), Swidth/4*j, Swidth/4, Swidth/4);
            if (i==0 && j==0) {
                point = view.center;
            }
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(0, 0, 40, 40);
            button.center = point;
            button.tag = 300+j*4+(i%4)+1;
            
            button.titleLabel.font = [UIFont systemFontOfSize:13];
            [button setTitle:[NSString stringWithFormat:@"%d",i + 1] forState:0];
            //设置已做 未做颜色
            if ([indexArray[i] intValue] == 0) {
                [button setBackgroundImage:[UIImage imageNamed:@"weizuo"] forState:0];
                [button setTitleColor:UIColorFromHex(0x20B2AA) forState:0];
            }else{
                [button setBackgroundImage:[UIImage imageNamed:@"yizuo"] forState:0];
                [button setTitleColor:[UIColor whiteColor] forState:0];
            }
            
            [button addTarget:self action:@selector(moveToQues:) forControlEvents:UIControlEventTouchUpInside];
            [view bringSubviewToFront:button];
            [view addSubview:button];
            [questionView addSubview:view];
        }
        if (tags%4 == 0){
            questionView.contentSize = CGSizeMake(Swidth, tags/4*(Swidth/4) + 64);
        }else{
            questionView.contentSize = CGSizeMake(Swidth, (tags/4+1)*(Swidth/4) + 64);
        }
        [self.view addSubview:questionView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeQues:)];
        tap.numberOfTapsRequired = 1;
        [questionView addGestureRecognizer:tap];
        return;
    }
    [questionView removeFromSuperview];
}
- (void)moveToQues:(UIButton *)btn{
    self.cardBtn.selected = !self.cardBtn.selected;
    int tag = [btn.titleLabel.text intValue];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.mainScroll.contentOffset = CGPointMake([scroll_X_Array[tag - 1] floatValue], 0);
    }];
    UILabel *indexLab = [self.view viewWithTag:201];
    indexLab.text = [NSString stringWithFormat:@"%.0f/%ld页",_mainScroll.contentOffset.x/Swidth + 1,dataArray.count];
    [btn.superview.superview removeFromSuperview];
}
- (void)removeQues: (UITapGestureRecognizer *)tap{
    self.cardBtn.selected = !self.cardBtn.selected;
    [tap.view removeFromSuperview];
}
#pragma -delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    currentField = textField;
    
}
- (void)textFieldDidEndEditing:(SelectField *)textField{
    
    //保存
    QuestionModel *model = textField.model;
    NSMutableArray *fieldArr = model.answerFieldArr;
    [fieldArr replaceObjectAtIndex:textField.fieldNum.integerValue withObject:textField.text];
    //保存答案
    NSString *url = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/saveResponseAnswers/%@",LocalIP,_model.onlineId,LocalUserId,model.questionId];
    
    [RequestTools RequestWithURL:url Method:@"post" Params:fieldArr Success:^(NSDictionary *result) {
        
        if (![result[@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
        }else{
            [indexArray replaceObjectAtIndex:textField.quesIndex.integerValue withObject:@1];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
    }];
    
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    currentText = textView;
    originTextview = textView.superview.frame;
    
}
- (void)textViewDidEndEditing:(SelectTextview *)textView{
    
    //保存
    QuestionModel *model = textView.model;
    //保存答案
    NSString *url = [NSString stringWithFormat:@"%@/testSchedules/%@/answerSheets/%@/questions/saveResponseAnswers/%@",LocalIP,_model.onlineId,LocalUserId,model.questionId];
    [RequestTools RequestWithURL:url Method:@"post" Params:@[textView.text] Success:^(NSDictionary *result) {
        
        if (![result[@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
        }else{
            [indexArray replaceObjectAtIndex:textView.quesIndex.integerValue withObject:@1];
        }
        
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"保存失败,请试着重新选择~" places:0 toView:nil];
    }];
    
}
#pragma -scrollDelegate
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    [currentText resignFirstResponder];
    [currentField resignFirstResponder];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView == _mainScroll) {
        UILabel *indexLab = [self.view viewWithTag:201];
        indexLab.text = [NSString stringWithFormat:@"%.0f/%ld页",scrollView.contentOffset.x/Swidth + 1,dataArray.count];
    }
}
- (IBAction)leftAction:(id)sender {
    if (self.mainScroll.contentOffset.x == 0) {
        [MBProgressHUD showToastAndMessage:@"已经是第一页了~" places:0 toView:nil];
        return;
    }
    [currentText resignFirstResponder];
    [currentField resignFirstResponder];
    [UIView animateWithDuration:0.5 animations:^{
        self.mainScroll.contentOffset = CGPointMake(self.mainScroll.contentOffset.x - Swidth, self.mainScroll.contentOffset.y);
    } completion:^(BOOL finished) {
        UILabel *indexLab = [self.view viewWithTag:201];
        indexLab.text = [NSString stringWithFormat:@"%.0f/%ld页",_mainScroll.contentOffset.x/Swidth + 1,dataArray.count];
    }];
}

- (IBAction)rightAction:(id)sender {
    if (self.mainScroll.contentOffset.x/Swidth == dataArray.count - 1) {
        [MBProgressHUD showToastAndMessage:@"已经是最后一页了~" places:0 toView:nil];
        return;
    }
    [currentText resignFirstResponder];
    [currentField resignFirstResponder];
    [UIView animateWithDuration:0.5 animations:^{
        self.mainScroll.contentOffset = CGPointMake(self.mainScroll.contentOffset.x + Swidth, self.mainScroll.contentOffset.y);
    } completion:^(BOOL finished) {
        UILabel *indexLab = [self.view viewWithTag:201];
        indexLab.text = [NSString stringWithFormat:@"%.0f/%ld页",_mainScroll.contentOffset.x/Swidth + 1,dataArray.count];
    }];
}
- (void)fieldHide:(UITapGestureRecognizer *)tap{
    NSArray *viewArray = tap.view.subviews;
    for (UIView *view in viewArray) {
        if ([view isKindOfClass:[UITextField class]]) {
            [view resignFirstResponder];
        }
    }
}
- (void)textviewHide:(UITapGestureRecognizer *)tap{
    NSArray *viewArray = tap.view.subviews;
    for (UIView *view in viewArray) {
        if ([view isKindOfClass:[UITextView class]]) {
            [view resignFirstResponder];
        }
    }
}
- (void)higher:(NSNotification *)notification {
    //键盘高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    keyboard_H = keyboardRect.size.height;
    CGFloat keyboard_y = Sheight - 104 - keyboard_H;
    if (currentText.frame.origin.y > keyboard_y) {
        [UIView animateWithDuration:0.4 animations:^{
            UIScrollView *pageScroll = (UIScrollView *)currentText.superview;
            pageScroll.contentOffset = CGPointMake(pageScroll.contentOffset.x,pageScroll.contentSize.height);
            pageScroll.frame = CGRectMake(originTextview.origin.x, originTextview.origin.y, Swidth, originTextview.size.height - keyboard_H);
            
        }];
    }
}
- (void)smaller:(NSNotification *)notification{
    [UIView animateWithDuration:0.3 animations:^{
        currentText.superview.frame = originTextview;
    }];
    //    originTextview = currentText.superview.frame;
}
@end

