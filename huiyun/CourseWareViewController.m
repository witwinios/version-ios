//
//  CourseWareViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/2.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "CourseWareViewController.h"
#import "CourseWareCell.h"
#import "CourseWareModel.h"
#import "StandardCourseWareViewController.h"
@interface CourseWareViewController ()
{
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *courseTable;
    NSMutableArray *dataArray;
    int currentPage;
    
    NSInteger line;
}

@end

@implementation CourseWareViewController
-(void)viewWillAppear:(BOOL)animated{
    [self setUpRefresh];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
    [self setUpRefresh];
    [self setNav];
    

  
    [self setsearchBar];
}

- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"标准课件列表";
    
    
    [backNavigation addSubview:titleLab];
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (_PlanModel.PlanOwnerId.intValue == persion.userID.intValue ) {
     if([_PlanModel.PlanCourseStatus isEqualToString:@"in_design"]){
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [rightBtn setTitle:@"编辑" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(Choice:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;
     }
    }
}
- (void)back :(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)Choice:(id)sender{

    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"课件管理" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *AddAction = [UIAlertAction actionWithTitle:@"新增" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        StandardCourseWareViewController *vc=[[StandardCourseWareViewController alloc]init];
        vc.PlanModel=_PlanModel;
        vc.TempArray=dataArray;
        
//        vc.backArr=^(NSArray *addArr){
//            dataArray = [NSMutableArray arrayWithArray:addArr];
//            [courseTable reloadData];
//        };
        
        [self.navigationController pushViewController:vc animated:YES];
       
        
    }];
    UIAlertAction *EditAction = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [courseTable setEditing:YES animated:YES];
        rightBtn.hidden=YES;
        SaveBtn.hidden=NO;
  
    }];
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
    [AlertView addAction:BackAction];
    [AlertView addAction:AddAction];
    [AlertView addAction:EditAction];
    
    [self presentViewController:AlertView animated:YES completion:nil];

}


-(void)save{
    [courseTable setEditing:NO animated:YES];
    rightBtn.hidden=NO;
    SaveBtn.hidden=YES;
}

-(void)setUI{
    currentPage = 1;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64,Swidth, Sheight-64) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    
    [courseTable registerNib:[UINib nibWithNibName:@"CourseWareCell" bundle:nil] forCellReuseIdentifier:@"courseware"];
    [self.view addSubview:courseTable];
    
    dataArray=[NSMutableArray new];
    
    
}

#pragma mark 搜索框：
-(void)setsearchBar{
    
}

#pragma mark 筛选框：
-(void)screen{
    
}

#pragma mark 数据加载：
-(void)setUpRefresh{
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
//    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
//    courseTable.footerPullToRefreshText = @"上拉加载";
//    courseTable.footerReleaseToRefreshText = @"松开进行加载";
//    courseTable.footerRefreshingText = @"加载中。。。";
}

//http://www.hzwitwin.cn:81/witwin-ctts-web/courses/531/standardCourses?pageStart=1&pageSize=15
-(void)loadData{
    currentPage = 1;
    
     NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/standardCourses?pageStart=%d&pageSize=999",LocalIP,_PlanModel.PlanNameId,currentPage];
  
    
    NSLog(@"%@",Url);
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];
        
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                [Maneger showAlert:@"无课件!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    CourseWareModel *model=[CourseWareModel new];
                    model.CourseWareId=[dictionary objectForKey:@"courseId"];
                    model.CourseWareName=[dictionary objectForKey:@"courseName"];
                    model.CourseWarePublic=[dictionary objectForKey:@"isPublic"];
                    model.CourseWareSubject=[dictionary objectForKey:@"subjectName"];
                    model.CourseWareClass=[dictionary objectForKey:@"categoryName"];
                    
                    model.FileFormat=[dictionary objectForKey:@"fileFormat"];
                    model.FileUrl=[dictionary objectForKey:@"fileUrl"];
                    NSString *url=[NSString stringWithFormat:@"http://www.hzwitwin.cn:81%@",model.FileUrl];
                    model.FileUrl=url;
                    [dataArray addObject:model];
                    
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                [courseTable reloadData];
                [courseTable headerEndRefreshing];
                
                
            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
       
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}

-(void)loadRefresh{
    currentPage++;
   NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/standardCourses?pageStart=%d&pageSize=15",LocalIP,_PlanModel.PlanNameId,currentPage];
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable footerEndRefreshing];
            return;
        }
        [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];
        
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                [Maneger showAlert:@"无课件!" andCurentVC:self];
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    CourseWareModel *model=[CourseWareModel new];
                    
                    model.CourseWareName=[dictionary objectForKey:@"courseName"];
                    model.CourseWarePublic=[dictionary objectForKey:@"isPublic"];
                    model.CourseWareSubject=[dictionary objectForKey:@"subjectName"];
                    model.CourseWareClass=[dictionary objectForKey:@"categoryName"];
                    
                    [newData addObject:model];
                    
                    
                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);
                
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                
                
                [courseTable reloadData];
                
                
            }
        }
        else{
            currentPage--;
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

//点击Cell 跳转页面
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CourseWareModel *model=dataArray[indexPath.row];
 
    if ([model.FileFormat isEqualToString:@"png"]||[model.FileFormat isEqualToString:@"jpg"]) {
        CourseQuestionBView *vc=[CourseQuestionBView new];
        vc.ImgUrl=model.FileUrl;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CourseWareCell *cell=[tableView dequeueReusableCellWithIdentifier:@"courseware"];
    //  cell.selectionStyle=UITableViewCellEditingStyleNone;
    
    CourseWareModel *model=dataArray[indexPath.row];
    [cell setProperty:model];
    
    NSString *content=model.CourseWareClass;
    CGFloat test_H=[Maneger getPonentH:content andFont:[UIFont systemFontOfSize:17] andWidth:Swidth-200]+60;
    CGRect frame=cell.frame;
    frame.size.height=test_H+73;
    cell.frame=frame;
    
    cell.CourseWareChoice.hidden=YES;
    
  
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CourseWareCell *cell =(CourseWareCell *)[self tableView:tableView  cellForRowAtIndexPath:indexPath];
    
    return cell.frame.size.height;
}


#pragma 编辑：
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return   UITableViewCellEditingStyleDelete;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"你确定删除该课件？" preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            CourseWareModel *model=dataArray[indexPath.row];
            
            [dataArray removeObjectAtIndex:indexPath.row];
            [courseTable beginUpdates];
            [courseTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            
            [self DeleteDate:model CourseWareId:[NSString stringWithFormat:@"%@",model.CourseWareId]];
            [courseTable endUpdates];
            
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
        }]];
        
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}
//设置进入编辑状态时，Cell不会缩进

- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return NO;
    
}

///courses/{courseId}/standardCourses/{standardCourseId}
-(void)DeleteDate:(CourseWareModel *)model CourseWareId:(NSString *)CourseWareId{
    
    NSString *Url=[NSString stringWithFormat:@"%@/courses/%@/standardCourses/%@",LocalIP,_PlanModel.PlanNameId,CourseWareId];
    NSLog(@"%@",Url);
    
    
    [RequestTools RequestWithURL:Url Method:@"DELETE" Params:nil Success:^(NSDictionary *result) {
        
        
        
         if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
        
        [MBProgressHUD showToastAndMessage:@"课件成功删除!" places:0 toView:nil];
         }
    } failed:^(NSString *result) {
        NSLog(@"%@",result);
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
            NSLog(@"%@",result);
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
}


@end
