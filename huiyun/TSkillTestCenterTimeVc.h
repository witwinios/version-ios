//
//  TSkillTestCenterTimeVc.h
//  huiyun
//
//  Created by Bad on 2018/3/27.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSkillTimeCell.h"
#import "TSkillTestCenterModel.h"
#import "SkillModel.h"

@interface TSkillTestCenterTimeVc : UIViewController
@property (strong, nonatomic) SkillModel *model;
@end
