//
//  CourseQuestionAddViewController.m
//  huiyun
//
//  Created by Mr.Wang on 2018/1/5.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import "CourseQuestionAddViewController.h"
#import "CourseQuestionModel.h"
#import "CourseQuestionCell.h"
@interface CourseQuestionAddViewController ()<UITextFieldDelegate>
{
    NSMutableArray *keyboardManeger;
    
    UIImageView *backNavigation;
    UIButton *rightBtn;
    UIButton *SaveBtn;
    
    UITableView *courseTable;
    NSMutableArray *dataArray;
    int currentPage;
    
    NSInteger line;
    NSMutableArray *seleArr;
    
    NSString *Url;
    
    NSString *PlanId;
    NSString *Difficulty;
    
    NSMutableArray *NewArr;
    UIView *Children;
    
    NSMutableArray *CQArray;
    NSMutableDictionary *Params;
    
    UILabel *SerialNumberLable;
    UILabel *QusetionTitleLable;
    
    
    int AddCount;
    
    NSMutableArray *Add;
    
    NSIndexPath *Index;
    
    NSString *ChildrenId;
    
    NSMutableArray *Arr;
    NSMutableArray *TempArr;
    
    int A;
    
    NSMutableArray *paramArr;
    
    
    UIView *SearchView;
    UITextField *SearchField;
    UIButton *SearchBtn;
    BOOL isSearch;
    
    NSString *str;
}
@end

@implementation CourseQuestionAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"_TempArr===%@",_TempArr);
    
    
    str=@"sss";
    
    [self setsearchBar];
    
    [self setNav];
    [self setUI];

    [self setUpRefresh];
    
    keyboardManeger = [NSMutableArray new];
    
    
}
- (void)setNav{
    backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:leftBtn];
    
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    
    titleLab.text = @"课程题目选取";
    
    
    [backNavigation addSubview:titleLab];
    
    
    rightBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [rightBtn setTitle:@"添加" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(Choice:) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:rightBtn];
    
    SaveBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SaveBtn.frame=CGRectMake(Swidth-100, 29.5, 100, 25);
    [SaveBtn setTitle:@"保存" forState:UIControlStateNormal];
    [SaveBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [SaveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [backNavigation addSubview:SaveBtn];
    SaveBtn.hidden=YES;
    
}
- (void)back :(UIButton *)button{
    if(seleArr.count >0){
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您有尚未保存的题目，是否保存？" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self save];
        }];
        
        UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert addAction:noAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
        [_TempArr removeAllObjects];
    };
}
-(void)Choice:(id)sender{
 //   [courseTable setEditing:YES animated:YES];
    
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"状态提醒" message:@"您现在可以选择教案题目." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        _editing=1;
        rightBtn.hidden=YES;
        SaveBtn.hidden=NO;
    }];
    
    UIAlertAction *noAction=[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:noAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];

}

-(void)setUI{
    currentPage = 1;
    CGFloat courseTable_Y=backNavigation.wwy_height+SearchView.wwy_height;
    courseTable = [[UITableView alloc]initWithFrame:CGRectMake(0, courseTable_Y,Swidth, Sheight-courseTable_Y) style:UITableViewStylePlain];
    courseTable.delegate = self;
    courseTable.dataSource = self;
    courseTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    courseTable.showsVerticalScrollIndicator=NO;
    courseTable.estimatedRowHeight = 0;
    [courseTable registerNib:[UINib nibWithNibName:@"CourseQuestionCell" bundle:nil] forCellReuseIdentifier:@"CourseQuestion"];
    [self.view addSubview:courseTable];
    
    dataArray=[NSMutableArray new];
    seleArr=[NSMutableArray new];
    NewArr=[NSMutableArray new];
    CQArray=[NSMutableArray new];
    Add=[NSMutableArray new];
   // [seleArr addObjectsFromArray:_TempArr];
    _editing=0;
    NSLog(@"%@",_str);
    
}
-(void)setUpRefresh{
    [courseTable addHeaderWithTarget:self action:@selector(loadData)];
    //设置文字
    courseTable.headerPullToRefreshText = @"下拉刷新";
    courseTable.headerReleaseToRefreshText = @"松开进行刷新";
    courseTable.headerRefreshingText = @"刷新中。。。";
    [courseTable headerBeginRefreshing];
    
    [courseTable addFooterWithTarget:self action:@selector(loadRefresh)];
    courseTable.footerPullToRefreshText = @"上拉加载";
    courseTable.footerReleaseToRefreshText = @"松开进行加载";
    courseTable.footerRefreshingText = @"加载中。。。";
}

-(void)setsearchBar{
    
   
    isSearch=NO;
    
    SearchView=[[UIView alloc]initWithFrame:CGRectMake(0, 64, Swidth, 44)];
    SearchView.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    UIImageView *SearchImg=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sousuo"]];
    SearchImg.backgroundColor=[UIColor clearColor];
    SearchImg.frame=CGRectMake(10, 6, 32, 32);
    
    
    SearchField=[[UITextField alloc]initWithFrame:CGRectMake(44, 6, Swidth-88, 32)];
    SearchField.backgroundColor=[UIColor clearColor];
    SearchField.placeholder=@"请输入搜索的题目名称";
    SearchField.clearButtonMode = UITextFieldViewModeWhileEditing;
    SearchField.clearsOnBeginEditing = YES;
    SearchField.keyboardType=UIKeyboardTypeDefault;
    SearchField.returnKeyType=UIReturnKeySearch;
    SearchField.delegate = self;
    [SearchField addTarget:self action:@selector(ApplyTextDidChanges:) forControlEvents:UIControlEventEditingChanged];
    
    SearchBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    SearchBtn.frame=CGRectMake((SearchField.wwy_x+SearchField.wwy_width)+2, 6, 40, 32);
    [SearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [SearchBtn addTarget:self action:@selector(setUpRefresh) forControlEvents:UIControlEventTouchUpInside];
    SearchBtn.backgroundColor=UIColorFromHex(0x20B2AA);
    SearchBtn.layer.cornerRadius=5;
    SearchBtn.hidden=YES;
    [SearchView addSubview:SearchBtn];
    [SearchView addSubview:SearchField];
    [SearchView addSubview:SearchImg];
    [self.view addSubview:SearchView];
    
}



-(BOOL)ApplyTextDidChanges:(UITextField *)theTextField{
    
    NSLog( @"text changed: %@", theTextField.text);
    
    
    if (![theTextField.text isEqualToString:@""]) {
        SearchBtn.hidden=NO;
        isSearch=YES;
    }else{
        isSearch=NO;
        [self setUpRefresh];
        SearchBtn.hidden=YES;
        [SearchField  resignFirstResponder];
    }
    
    return YES;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField

{
    [SearchField  resignFirstResponder];
    
    
    [self setUpRefresh ];
    
    
    
    NSLog(@"点击了搜索");
    
    return YES;
    
}

//http://www.hzwitwin.cn:81/witwin-ctts-web/questions?pageStart=1&pageSize=10&currentPage=1&showAnswers=true&topLevelOnly=true&exceptQuestionTypes=essay_question&exceptQuestionTypes=completion_question 课前
//http://www.hzwitwin.cn:81/witwin-ctts-web/questions?pageStart=1&pageSize=10&currentPage=1&showAnswers=true&topLevelOnly=true&exceptQuestionTypes=essay_question&exceptQuestionTypes=completion_question 课后

-(void)loadData{
    
    if(_editing){
        [seleArr removeAllObjects];
    }
    
    currentPage=1;
   
    NSString *Url;
    
    if (isSearch == NO) {
        if ([_str isEqualToString:@"课前"]) {
            Url=[NSString stringWithFormat:@"%@/questions?pageStart=%d&pageSize=10&showAnswers=true&topLevelOnly=true&exceptQuestionTypes=essay_question&exceptQuestionTypes=completion_question",LocalIP,currentPage];
        }else  if ([_str isEqualToString:@"课后"]) {
            Url=[NSString stringWithFormat:@"%@/questions?pageStart=%d&pageSize=10&showAnswers=true&topLevelOnly=true&exceptQuestionTypes=essay_question&exceptQuestionTypes=completion_question",LocalIP,currentPage];
        }
    }else{
        if ([_str isEqualToString:@"课前"]) {
             NSString *str=SearchField.text;
            
            Url=[NSString stringWithFormat:@"%@/questions?pageStart=%d&pageSize=10&showAnswers=true&topLevelOnly=true&exceptQuestionTypes=essay_question&exceptQuestionTypes=completion_question&questionTitle=%@",LocalIP,currentPage,str];
            Url=[Url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }else  if ([_str isEqualToString:@"课后"]) {
            NSString *str=SearchField.text;
            
            Url=[NSString stringWithFormat:@"%@/questions?pageStart=%d&pageSize=10&showAnswers=true&topLevelOnly=true&exceptQuestionTypes=essay_question&exceptQuestionTypes=completion_question&questionTitle=%@",LocalIP,currentPage,str];
             Url=[Url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }
    }
    
   
    
    NSLog(@"%@",Url);

    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {


        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [dataArray removeAllObjects];

        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {

                if ([_str isEqualToString:@"课前"]) {
                    [Maneger showAlert:@"无课前答题!" andCurentVC:self];
                }else  if ([_str isEqualToString:@"课后"]) {
                    [Maneger showAlert:@"无课后答题!" andCurentVC:self];
                }


                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];

                    CourseQuestionModel *model=[CourseQuestionModel new];
                    model.QuestionId=[dictionary objectForKey:@"questionId"];
                    model.QuestionType=[dictionary objectForKey:@"questionType"];
                    model.CategoryName=[dictionary objectForKey:@"categoryName"];
                    model.SubjectName=[dictionary objectForKey:@"subjectName"];
                    model.QuestionTitle=[dictionary objectForKey:@"questionTitle"];
                    model.ChildrenQuestionsNum=[dictionary objectForKey:@"childrenQuestionsNum"];
                    
                    
                    model.childArray = [NSMutableArray new];
                    int Num=[model.ChildrenQuestionsNum intValue];
                    
                    NSArray *childer=[dictionary objectForKey:@"childrenQuestions"];
                    if (Num > 0) {
                        for (int j=0; j<Num; j++) {
                            NSDictionary *childDictionary=[childer objectAtIndex:j];
                            
                            CourseQuestionModel *childModel = [CourseQuestionModel new];
                            
                            childModel.ChildrenQuestionId=[childDictionary objectForKey:@"questionId"];
                            childModel.ChildrenQuestionTitle=[childDictionary objectForKey:@"questionTitle"];
                            
                            
                            [model.childArray addObject:childModel];
                            
                        }
                    }
                     NSLog(@"%@",model.childArray);
                    
                    
                    [dataArray addObject:model];

                }
                NSLog(@"dataArrayCount=%lu",(unsigned long)dataArray.count);

                [courseTable reloadData];
                [courseTable headerEndRefreshing];


            }
        }
        else{
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }

    } failed:^(NSString *result) {

        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
 
    
}

-(void)loadRefresh{
    currentPage++;
    
    NSString *Url;
    
    if (isSearch == NO) {
        if ([_str isEqualToString:@"课前"]) {
            Url=[NSString stringWithFormat:@"%@/questions?pageStart=%d&pageSize=10&showAnswers=true&topLevelOnly=true&exceptQuestionTypes=essay_question&exceptQuestionTypes=completion_question",LocalIP,currentPage];
        }else  if ([_str isEqualToString:@"课后"]) {
            Url=[NSString stringWithFormat:@"%@/questions?pageStart=%d&pageSize=10&showAnswers=true&topLevelOnly=true&exceptQuestionTypes=essay_question&exceptQuestionTypes=completion_question",LocalIP,currentPage];
        }
    }else{
        if ([_str isEqualToString:@"课前"]) {
            NSString *str=SearchField.text;
            
            Url=[NSString stringWithFormat:@"%@/questions?pageStart=%d&pageSize=10&showAnswers=true&topLevelOnly=true&exceptQuestionTypes=essay_question&exceptQuestionTypes=completion_question&questionTitle=%@",LocalIP,currentPage,str];
            Url=[Url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }else  if ([_str isEqualToString:@"课后"]) {
            NSString *str=SearchField.text;
            
            Url=[NSString stringWithFormat:@"%@/questions?pageStart=%d&pageSize=10&showAnswers=true&topLevelOnly=true&exceptQuestionTypes=essay_question&exceptQuestionTypes=completion_question&questionTitle=%@",LocalIP,currentPage,str];
            Url=[Url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }
    }
    
    
    
    [RequestTools RequestWithURL:Url Method:@"get" Params:nil Success:^(NSDictionary *result) {
        
        
        if ([[result objectForKey:@"responseStatus"] isEqualToString:@"failed"] && [[result objectForKey:@"errorCode"] isEqualToString:@"invalid_token"]) {
            [Maneger showMessageAlert:@"Token失效,请重新登录!" andCurrentVC:self];
            [courseTable headerEndRefreshing];
            return;
        }
        [courseTable footerEndRefreshing];
        NSMutableArray *newData=[NSMutableArray new];
        
        //获取数据源
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            NSArray *array = [[result objectForKey:@"responseBody"] objectForKey:@"result"];
            NSLog(@"array=%lu",(unsigned long)array.count);
            if (array.count ==0) {
                
                if ([_str isEqualToString:@"课前"]) {
                    [Maneger showAlert:@"无课前答题!" andCurentVC:self];
                }else  if ([_str isEqualToString:@"课后"]) {
                    [Maneger showAlert:@"无课后答题!" andCurentVC:self];
                }
                
                
                [courseTable headerEndRefreshing];
            }else{
                for (int i=0; i<array.count; i++) {
                    NSDictionary *dictionary = [array objectAtIndex:i];
                    
                    CourseQuestionModel *model=[CourseQuestionModel new];
                    model.QuestionId=[dictionary objectForKey:@"questionId"];
                    model.QuestionType=[dictionary objectForKey:@"questionType"];
                    model.CategoryName=[dictionary objectForKey:@"categoryName"];
                    model.SubjectName=[dictionary objectForKey:@"subjectName"];
                    model.QuestionTitle=[dictionary objectForKey:@"questionTitle"];
                    model.ChildrenQuestionsNum=[dictionary objectForKey:@"childrenQuestionsNum"];
                    
                    model.childArray = [NSMutableArray new];
                    
                    int Num=[model.ChildrenQuestionsNum intValue];
                    
                    NSArray *childer=[dictionary objectForKey:@"childrenQuestions"];
                    if (Num > 0) {
                        for (int j=0; j<Num; j++) {
                            NSDictionary *childDictionary=[childer objectAtIndex:j];
                            
                            CourseQuestionModel *childModel = [CourseQuestionModel new];
                            
                            childModel.ChildrenQuestionId=[childDictionary objectForKey:@"questionId"];
                            childModel.ChildrenQuestionTitle=[childDictionary objectForKey:@"questionTitle"];
                            
                            
                            [model.childArray addObject:childModel];
                            
                        }
                    }
                    NSLog(@"%@",model.childArray);
                   
                    [newData addObject:model];
                    
                }
                NSLog(@"newData=%lu",(unsigned long)newData.count);
                
                NSRange range = NSMakeRange(dataArray.count,newData.count );
                NSIndexSet *set = [NSIndexSet indexSetWithIndexesInRange:range];
                [dataArray insertObjects:newData atIndexes:set];
                
                
                [courseTable reloadData];
                
                
            }
        }
        else{
            currentPage--;
            [Maneger showAlert:@"请重新登录!" andCurentVC:self];
        }
        
    } failed:^(NSString *result) {
        currentPage--;
        [courseTable footerEndRefreshing];
        if (![result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
    
    
}


#pragma mark---TableView
//行数：
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
/*
获取 子题 http://www.hzwitwin.cn:81/witwin-ctts-web/questions/476840/answers
*/
//点击Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CourseQuestionModel *model=dataArray[indexPath.row];
    Index=indexPath;
    if(_editing){
       
        int ChildrenQuestionsNum=[model.ChildrenQuestionsNum intValue];
   
        NSLog(@"ChildrenQuestionsNum==%d",ChildrenQuestionsNum);
        
        model.isCheck=!model.isCheck;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        
        if (model.isCheck == 0) {
            
            [NewArr removeObject:model];
            
            for (int s=0; s<seleArr.count; s++) {
                NSMutableDictionary *doc = seleArr[s];
                if ([doc[@"questionId"] intValue] == model.QuestionId.intValue) {
                    [seleArr removeObjectAtIndex:s];
                }
            }
            
            NSLog(@"seleArr 取消:%@",seleArr);
            NSLog(@"NewArr  取消:%@",NewArr);
            
            
            
        }else if(model.isCheck == 1){
        
        if(ChildrenQuestionsNum >0){
            [MBProgressHUD showToastAndMessage:@"该题有子题！！" places:0 toView:nil];
            
            
            
            NSLog(@"父题ID:%@",model.QuestionId);
            
            [self ChildrenQuestionsNum:model AtIndexPath:indexPath];
            
        }else{
            UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"添加题目分数" message:@"请输入该到题目的分数" preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
                
                textField.placeholder=@"题目分数：1～999之间";
                textField.textColor=[UIColor blackColor];
                textField.borderStyle=UITextBorderStyleRoundedRect; //边框样式
                textField.borderStyle = UITextBorderStyleRoundedRect;
                textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                textField.keyboardType=UIKeyboardTypeNumberPad;
                textField.returnKeyType=UIReturnKeyDefault;
                textField.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
                textField.layer.borderWidth=1;
                textField.delegate=self;
                [textField addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
            }];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                CourseQuestionModel *model=dataArray[indexPath.row];
                model.ScoreNum=Difficulty;
                
                double Num=[model.ScoreNum doubleValue];
                NSString *ScoreNum=[NSString stringWithFormat:@"%.0lf",Num];
                
             
                
                Params=[NSMutableDictionary dictionary];
                [Params setObject:_PlanModel.PlanNameId forKey:@"courseId"];
                [Params setObject:model.QuestionId forKey:@"questionId"];
                [Params setObject:ScoreNum forKey:@"score"];
                
                [NewArr addObject:model];
                
                [seleArr addObject:Params];
                
                 NSLog(@"selectorPatnArray 点击:%@",seleArr);
                 NSLog(@"Params 点击:%@",Params);
            }]];
            
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
    }
    
    
}

-(void)ChildrenQuestionsNum:(CourseQuestionModel *)model AtIndexPath:(NSIndexPath *)indexPath {
    //
    [keyboardManeger removeAllObjects];
    
    paramArr = [NSMutableArray new];
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0, 0, Swidth, Sheight);
    [btn addTarget:self action:@selector(hidde) forControlEvents:UIControlEventTouchUpInside];
    
    
    Children=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, Sheight)];
    Children.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.4];
    
     Arr=[NSMutableArray new];
    
    [Arr addObjectsFromArray:model.childArray];
    
    UIView *QuestionsView=[[UIView alloc]init];
    
    TempArr=[NSMutableArray new];
    

  

    UILabel *ALabel=[[UILabel alloc]initWithFrame:CGRectMake(2, 34, 50, 20)];
    ALabel.text=@"序号";
    ALabel.textColor=UIColorFromHex(0x20B2AA);;
    ALabel.font=[UIFont boldSystemFontOfSize:16];
    [QuestionsView addSubview:ALabel];
    
    UILabel *BLabel=[[UILabel alloc]initWithFrame:CGRectMake(55, 34, 100, 20)];
    BLabel.text=@"题目";
    BLabel.textColor=UIColorFromHex(0x20B2AA);;
    BLabel.textAlignment=NSTextAlignmentCenter;
    BLabel.font=[UIFont boldSystemFontOfSize:16];
    [QuestionsView addSubview:BLabel];
    
    UILabel *CLabel=[[UILabel alloc]initWithFrame:CGRectMake(160, 34, 100, 20)];
    CLabel.text=@"分数";
    CLabel.textColor=UIColorFromHex(0x20B2AA);;
    CLabel.textAlignment=NSTextAlignmentRight;
    CLabel.font=[UIFont boldSystemFontOfSize:16];
    [QuestionsView addSubview:CLabel];
    
    
    int i=1;
    for (CourseQuestionModel *model in Arr) {
        
        [TempArr addObject:model.ChildrenQuestionId];
        NSLog(@"子题 %d Id:%@",i,TempArr[i-1]);
        
        SerialNumberLable =[[UILabel alloc]initWithFrame:CGRectMake(10,30 *(i)+30, 50, 20)];
        SerialNumberLable.text=[NSString stringWithFormat:@"%d.",i];
        [QuestionsView addSubview:SerialNumberLable];
        
        QusetionTitleLable=[[UILabel alloc]initWithFrame:CGRectMake(50, 30 *(i)+30, 150, 20)];
        QusetionTitleLable.text=[NSString stringWithFormat:@"%@",model.ChildrenQuestionTitle];
//        QusetionTitleLable.numberOfLines=0;
//        [QusetionTitleLable sizeToFit];
 //       CGFloat test_W=300-150;
 //       CGFloat test_H=[Maneger getPonentH:model.ChildrenQuestionTitle andFont:[UIFont systemFontOfSize:14] andWidth:test_W]+50;
//        CGRect frame=QusetionTitleLable.frame;
//        frame.size.height=test_H+20;
//        QusetionScoreTextField.frame=CGRectMake(50, 30 *(i)+30, 150, test_H+20);
        QusetionTitleLable.font=[UIFont systemFontOfSize:14];
        [QuestionsView addSubview:QusetionTitleLable];
        
        MyScoreField *QusetionScoreTextField=[[MyScoreField alloc]initWithFrame:CGRectMake(200, 30 *(i)+30, 80, 20)];
        QusetionScoreTextField.font=[UIFont boldSystemFontOfSize:13];
        QusetionScoreTextField.textColor=[UIColor blackColor];
        QusetionScoreTextField.borderStyle=UITextBorderStyleRoundedRect; //边框样式
        QusetionScoreTextField.borderStyle = UITextBorderStyleRoundedRect;
        QusetionScoreTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        QusetionScoreTextField.keyboardType=UIKeyboardTypeNumberPad;
        QusetionScoreTextField.returnKeyType=UIReturnKeyDefault;
        QusetionScoreTextField.layer.borderColor=UIColorFromHex(0x20B2AA).CGColor;
        QusetionScoreTextField.layer.borderWidth=1;
        QusetionScoreTextField.delegate=self;
        
        QusetionScoreTextField.quesID = model.ChildrenQuestionId;
        [QusetionScoreTextField addTarget:self action:@selector(ApplyTextDidChange:) forControlEvents:UIControlEventEditingChanged];
        [QuestionsView addSubview:QusetionScoreTextField];
        [keyboardManeger addObject:QusetionScoreTextField];
        
        NSLog(@"%lu",QusetionScoreTextField.tag);
        i++;
    }
    
    
    
    CGFloat textLabel_YH=SerialNumberLable.wwy_y+30;
    NSLog(@"textLabel_YH:%lf",textLabel_YH);
    
    QuestionsView.frame=CGRectMake(Swidth/2-150, Sheight/2-(textLabel_YH/4+150), 300, 200+textLabel_YH/2);
    
    
    QuestionsView.backgroundColor=[UIColor colorWithRed:220 green:220 blue:220 alpha:0.4];
    QuestionsView.layer.cornerRadius=15;
    
    UILabel *Titlelabel=[[UILabel alloc]initWithFrame:CGRectMake(QuestionsView.wwy_width/2-70, 10, 150, 20)];
    Titlelabel.text=@"添加题目分数";
    Titlelabel.font=[UIFont boldSystemFontOfSize:18];
 
    UIButton *QuestionsBtn=[UIButton buttonWithType:UIButtonTypeSystem];
    QuestionsBtn.frame=CGRectMake(0, QuestionsView.wwy_height-40, QuestionsView.wwy_width, 40);
    [QuestionsBtn setTitle:@"确定" forState:UIControlStateNormal];
    [QuestionsBtn addTarget:self action:@selector(FanHui) forControlEvents:UIControlEventTouchUpInside];

    
    UIView *Av=[[UIView alloc]initWithFrame:CGRectMake(0, QuestionsView.wwy_height-29, QuestionsBtn.wwy_width, 1)];
    Av.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.8];
    
    UIView *Bv=[[UIView alloc]initWithFrame:CGRectMake(0, ALabel.wwy_y-2, QuestionsBtn.wwy_width, 1)];
    Bv.backgroundColor=[[UIColor lightGrayColor]colorWithAlphaComponent:0.8];
    
    [QuestionsView addSubview:Titlelabel];
    [QuestionsView addSubview:QuestionsBtn];
    [QuestionsView addSubview:Av];
    [QuestionsView addSubview:Bv];
    [Children addSubview:btn];
    [Children addSubview:QuestionsView];
    
    [[UIApplication sharedApplication].keyWindow addSubview:Children];
}


- (void)keyboardHide{
    for (MyScoreField *field in keyboardManeger) {
        [field resignFirstResponder];
    }
}

-(void)hidde{
    Children.hidden=YES;
    
}

-(void)ApplyTextDidChange:(UITextField *)theTextField{
    
    [Add addObject:theTextField.text];
    Difficulty=theTextField.text;
    int Num=[theTextField.text intValue];
    
    if (Num<1) {
        [MBProgressHUD showToastAndMessage:@"分数小于最小系统分数!" places:0 toView:nil];
    }else if(Num>999){
        [MBProgressHUD showToastAndMessage:@"分数大于最大系统分数!" places:0 toView:nil];
    }
    
    CourseQuestionModel *model=dataArray[Index.row];
    
    int ChildrenQuestionsNum=[model.ChildrenQuestionsNum intValue];
    
    
    NSLog(@"ChildrenQuestionsNum == %d",ChildrenQuestionsNum);
    
    if (ChildrenQuestionsNum >0) {
        
        
        MyScoreField *scoreField = (MyScoreField *)theTextField;
        
        str=scoreField.text;
        model.ScoreNum=scoreField.text;
        double Nums=[model.ScoreNum doubleValue];
        
        
        if (Nums >999) {
            [MBProgressHUD showToastAndMessage:@"分数大于最大系统分数!" places:0 toView:nil];
        }else if(Nums<0){
             [MBProgressHUD showToastAndMessage:@"分数小于最大系统分数!" places:0 toView:nil];
        }else if(Nums == 0){
             [MBProgressHUD showToastAndMessage:@"请输入分数!" places:0 toView:nil];
        }
        
        
        NSString *ScoreNum=[NSString stringWithFormat:@"%.0lf",Nums];
        
        NSMutableDictionary *fieldParams=[NSMutableDictionary dictionary];
        [fieldParams setObject:scoreField.quesID forKey:@"questionId"];
        [fieldParams setObject:_PlanModel.PlanNameId forKey:@"courseId"];
        [fieldParams setObject:ScoreNum forKey:@"score"];
        
        
        for (int s=0; s<seleArr.count; s++) {
            NSMutableDictionary *dic = seleArr[s];
            if ([dic[@"questionId"] intValue] == scoreField.quesID.intValue) {
                [dic setObject:ScoreNum forKey:@"score"];
                break;
            }else{
                [seleArr addObject:fieldParams];
                break;
            }
        }
        
//        for (NSMutableDictionary *dic in seleArr) {
//            if ([dic[@"questionId"] intValue] == scoreField.quesID.intValue) {
//                [dic setObject:ScoreNum forKey:@"score"];
//            }else{
//                [seleArr addObject:fieldParams];
//            }
//        }
        
        NSLog(@"selectorPatnArray 点击:%@",seleArr);
        NSLog(@"fieldParams 点击:%@",fieldParams);
    }
    
}





-(void)FanHui{
    AddCount =0;
    
    if ([str isEqualToString:@"sss"]) {
        [MBProgressHUD showToastAndMessage:@"请输入分数" places:0 toView:nil];
        [self keyboardHide];
    }else{
        [self keyboardHide];
        
        int Temp;
        for (int i=0; i<Add.count; i++) {
            Temp=[Add[i] intValue];
            AddCount=AddCount+Temp;
        }
        NSLog(@"%d",AddCount);
        
        NSString *AddNum=[NSString stringWithFormat:@"%d",AddCount];
        
        CourseQuestionModel *model=dataArray[Index.row];
        model.ScoreNum=AddNum;
        double Num=[model.ScoreNum doubleValue];
        NSString *ScoreNum=[NSString stringWithFormat:@"%.0lf",Num];
        
        Params=[NSMutableDictionary dictionary];
        [Params setObject:_PlanModel.PlanNameId forKey:@"courseId"];
        [Params setObject:model.QuestionId forKey:@"questionId"];
        [Params setObject:ScoreNum forKey:@"score"];
        
        
        
        NSLog(@"父题Id:%@",model.QuestionId);
        
        [NewArr addObject:model];
        [seleArr addObject:Params];
        
        NSLog(@"selectorPatnArray 点击:%@",seleArr);
        NSLog(@"Params 点击:%@",Params);
        
        
        [Add removeAllObjects];
        Children.hidden=YES;
    }
    
   
}

//加载的Cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CourseQuestionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CourseQuestion"];
    
    CourseQuestionModel *model=dataArray[indexPath.row];
    
    [cell AddCourseQuestion:model];
    
    cell.CourseQuestionStem.userInteractionEnabled=NO;
    cell.TwoLable.text=@"分类:";
    cell.TwoLable.textColor=[UIColor blackColor];
    cell.CourseQuestionNum.textColor=[UIColor blackColor];
    cell.ThreeLable.text=@"科目:";
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 188.f;
}

#pragma mark 编辑：
//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
//}
//
//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    //从选中中取消
//    CourseQuestionModel *model=dataArray[indexPath.row];
//
//    [NewArr removeObject:model];
//
//    for (int s=0; s<seleArr.count; s++) {
//        NSMutableDictionary *doc = seleArr[s];
//        if ([doc[@"questionId"] intValue] == model.QuestionId.intValue) {
//            [seleArr removeObjectAtIndex:s];
//        }
//    }
//
//    NSLog(@"seleArr 取消:%@",seleArr);
//    NSLog(@"NewArr  取消:%@",NewArr);
//
//
//    //    if (seleArr.count > 0) {
////       // CourseQuestionModel *model=dataArray[indexPath.row];
////
////        [seleArr removeObject:Params];
////
////        NSLog(@"selectorPatnArray 取消:%@",seleArr);
////    }
//
//}
// %@/courses/%@/previewQuestions 课前
// /courses/{courseId}/homeworkQuestions 课后
-(void)save{

    PlanId=[NSString stringWithFormat:@"%@",_PlanModel.PlanNameId];
    NSString *Url;
    if ([_str isEqualToString:@"课前"]) {
         Url=[NSString stringWithFormat:@"%@/courses/%@/previewQuestions",LocalIP,PlanId];
    }else  if ([_str isEqualToString:@"课后"]) {
         Url=[NSString stringWithFormat:@"%@/courses/%@/homeworkQuestions",LocalIP,PlanId];
    }
    
    NSLog(@"%@",Url);
    NSLog(@"seleArr:%@",seleArr);
    NSLog(@"NewArr=%lu",(unsigned long)NewArr.count);
    NSMutableArray *NewArr=[NSMutableArray new];
    
    
//
//     NSDictionary *dic=[seleArr objectAtIndex:i+1];
//     NSDictionary *DIC=[seleArr objectAtIndex:j];
//     NSString *questionID=[DIC objectForKey:@"questionId"];
//     NSString *questionId=[dic objectForKey:@"questionId"];
     

    
    
    
//    for (int i=0; i<seleArr.count; i++) {
//
//        for (int j=i+1; j<seleArr.count; j++) {
//
//            NSDictionary *dic=[seleArr objectAtIndex:i];
//            NSDictionary *DIC=[seleArr objectAtIndex:j];
//            NSString *questionID=[DIC objectForKey:@"questionId"];
//            NSString *questionId=[dic objectForKey:@"questionId"];
//
//            if ([questionId isEqualToString:questionID]) {
//                [seleArr removeObjectAtIndex:j];
//            }
//        }
//
//    }
    
    [seleArr addObjectsFromArray:_TempArr];
    
    
    
    
    
    [RequestTools RequestWithURL:Url Method:@"put" Params:seleArr Success:^(NSDictionary *result) {
        NSLog(@"result:%@",result);

        if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
            [MBProgressHUD showToastAndMessage:@"添加题目成功!" places:0 toView:nil];
            _backArr(NewArr,@"教案");
            [seleArr removeAllObjects];

            [self.navigationController popViewControllerAnimated:YES];
        }


    } failed:^(NSString *result) {
        NSLog(@"result:%@",result);
        if ([result isEqualToString:@"200"]) {
            [MBProgressHUD showToastAndMessage:@"服务器错误!" places:0 toView:nil];
        }else{
            [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
        }
    }];
    
 
}


@end
