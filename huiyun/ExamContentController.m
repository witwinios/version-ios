//
//  ExamContentController.m
//  yun
//
//  Created by MacAir on 2017/7/20.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ExamContentController.h"

@interface ExamContentController ()
{
    NSArray *dataArray;
    ExamContentModel *contentModel;
    
    UITableView *tabView;
    
    NSArray *_heightArr;
    
}
@end

@implementation ExamContentController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    _heightArr = [NSMutableArray new];
    [self setUI];
    [self loadData];
}
- (void)refresh{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/currentStudent?osceStationId=%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,self.examModel.ExamPointModelStationId];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Success:^(NSDictionary *result) {
        [tabView headerEndRefreshing];
        contentModel = [ExamContentModel new];
        contentModel.ExamContentModelRecordId = result[@"responseBody"][@"recordId"];
        contentModel.ExamContentModelDate = result[@"responseBody"][@"startTime"];
        contentModel.ExamContentModelStationId = result[@"responseBody"][@"osceStations"][0][@"stationId"];
        contentModel.ExamContentModelStationName = result[@"responseBody"][@"osceStations"][0][@"stationName"];
        contentModel.ExamContentModelStudentId = result[@"responseBody"][@"studentId"];
        contentModel.ExamContentModelStudentName = result[@"responseBody"][@"studentFullName"];
        contentModel.ExamContentModelContent = result[@"responseBody"][@"osceStations"][0][@"description"];
        contentModel.ExamContentModelPurpose = result[@"responseBody"][@"osceStations"][0][@"testPurpose"];
        dataArray = [NSMutableArray arrayWithObjects:contentModel.ExamContentModelStationName,contentModel.ExamContentModelPurpose,contentModel.ExamContentModelContent,[NSString stringWithFormat:@"%@分钟",self.examModel.ExamPointModelInternal.stringValue],@"1",nil];
        [tabView reloadData];
        
    } failed:^(NSString *result) {
        [tabView headerEndRefreshing];
        [MBProgressHUD showToastAndMessage:@"考试内容获取失败,请尝试下拉刷新~" places:0 toView:nil];
    }];
}
- (void)loadData{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/skillTests/%@/selectedStations/%@/currentStudent?osceStationId=%@",LocalIP,self.skillModel.skillID,self.callModel.callModelStationId,self.examModel.ExamPointModelStationId];
    [RequestTools RequestWithURL:requestUrl Method:@"get" Params:nil Message:@"获取考试内容..." Success:^(NSDictionary *result) {
        contentModel = [ExamContentModel new];
        contentModel.ExamContentModelRecordId = result[@"responseBody"][@"recordId"];
        contentModel.ExamContentModelDate = result[@"responseBody"][@"startTime"];
        contentModel.ExamContentModelStationId = result[@"responseBody"][@"osceStations"][0][@"stationId"];
        contentModel.ExamContentModelStationName = result[@"responseBody"][@"osceStations"][0][@"stationName"];
        contentModel.ExamContentModelStudentId = result[@"responseBody"][@"studentId"];
        contentModel.ExamContentModelStudentName = result[@"responseBody"][@"studentFullName"];
        contentModel.ExamContentModelContent = result[@"responseBody"][@"osceStations"][0][@"description"];
        contentModel.ExamContentModelPurpose = result[@"responseBody"][@"osceStations"][0][@"testPurpose"];
        dataArray = [NSMutableArray arrayWithObjects:contentModel.ExamContentModelStationName,contentModel.ExamContentModelPurpose,contentModel.ExamContentModelContent,[NSString stringWithFormat:@"%@分钟",self.examModel.ExamPointModelInternal.stringValue],@"1",nil];
        [tabView reloadData];
        
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"考试内容获取失败,请尝试下拉刷新~" places:0 toView:nil];
    }];
}
- (void)setUI{
    //背景色
    self.view.backgroundColor = [UIColor whiteColor];
    //导航栏
    UIView *navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    navigationView.backgroundColor = UIColorFromHex(0x20B2AA);
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [navigationView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(Swidth/2-50, 0, 100, 35)];
    titleLab.center = CGPointMake(Swidth/2, leftBtn.center.y);
    titleLab.text = @"考试内容";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:17];
    [navigationView addSubview:titleLab];
    
    [self.view addSubview:navigationView];
    
    tabView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, Swidth, Sheight-110) style:UITableViewStylePlain];
    tabView.delegate = self;
    tabView.dataSource = self;
    [tabView addHeaderWithTarget:self action:@selector(refresh)];
    tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tabView registerNib:[UINib nibWithNibName:@"ExamContentCell" bundle:nil] forCellReuseIdentifier:@"contentCell"];
    tabView.tableFooterView.frame = CGRectZero;
    tabView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self.view addSubview:tabView];

    
    UIButton *markButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [markButton setTitle:@"打开评分表" forState:0];
    markButton.frame = CGRectMake(0, Sheight-46, Swidth, 46);
    markButton.titleLabel.textColor = [UIColor whiteColor];
    markButton.backgroundColor = UIColorFromHex(0x20B2AA);
    [markButton addTarget:self action:@selector(markAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:markButton];
}
#pragma -action
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)markAction{
    MarkTableController *markVC = [MarkTableController new];
    markVC.skillModel = self.skillModel;
    markVC.pointModel = self.examModel;
    markVC.callModel = self.callModel;
    [self.navigationController pushViewController:markVC animated:YES];
}
#pragma -delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 105.f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headView = [UIView new];
    headView.backgroundColor = [UIColor whiteColor];
    headView.layer.borderColor = UIColorFromHex(0x20B2AA).CGColor;
    headView.layer.borderWidth = 4;
    UILabel *nameLab = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 100, 20)];
    nameLab.text = @"考生姓名:";
    nameLab.font = [UIFont systemFontOfSize:18];
    [headView addSubview:nameLab];
    
    UILabel *nameContentlab = [[UILabel alloc]initWithFrame:CGRectMake(110, 5, Swidth/2, 20)];
    nameContentlab.font = [UIFont systemFontOfSize:20];
    nameContentlab.text = self.callModel.callModelUserName;
    [headView addSubview:nameContentlab];
    
    UILabel *teachLab = [[UILabel alloc]initWithFrame:CGRectMake(5, 30, 100, 20)];
    teachLab.font = [UIFont systemFontOfSize:18];
    teachLab.text = @"考官姓名:";
    [headView addSubview:teachLab];
    
    UILabel *teachContentLab = [UILabel new];
    teachContentLab.font = [UIFont systemFontOfSize:18];
    teachContentLab.text = self.callModel.callModelTeacher;
    teachContentLab.frame = CGRectMake(110, 30, Swidth-115, [Maneger autoCalculateWidth:self.callModel.callModelTeacher andFont:[UIFont systemFontOfSize:20] Width:Swidth-115]);
    teachContentLab.numberOfLines = 0;
    [headView addSubview:teachContentLab];
    
    UILabel *dateLab = [[UILabel alloc]initWithFrame:CGRectMake(5,CGRectGetMaxY(teachContentLab.frame) + 5, 100, 20)];
    dateLab.text = @"考试日期:";
    dateLab.font = [UIFont systemFontOfSize:18];
    [headView addSubview:dateLab];
    
    UILabel *dateContentLab = [[UILabel alloc]initWithFrame:CGRectMake(110,CGRectGetMaxY(teachContentLab.frame) + 5, Swidth/2, 20)];
    dateContentLab.font = [UIFont systemFontOfSize:18];
    dateContentLab.text =  [[Maneger shareObject] timeFormatter:contentModel.ExamContentModelDate.stringValue];
    [headView addSubview:dateContentLab];
    
    headView.frame = CGRectMake(0, 0, Swidth, CGRectGetMaxY(dateContentLab.frame)+25);
    _headHeight = @(CGRectGetMaxY(dateContentLab.frame)+25);
    //
    
    return headView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat row_h = [Maneger autoCalculateWidth:dataArray[indexPath.row] andFont:[UIFont systemFontOfSize:20] Width:Swidth-104] + 26;
    return row_h;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ExamContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contentCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *titleArr = @[@"考站名称:",@"考试目的:",@"考试内容:",@"考试时长:",@"考试难度:"];
    cell.title.text = titleArr[indexPath.row];
    cell.content.text = dataArray[indexPath.row];
    
    return cell;
}

@end
