//
//  CaseHistoryController.m
//  huiyun
//
//  Created by MacAir on 2017/9/19.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseHistoryController.h"

@interface CaseHistoryController ()
{
    UITableView *table;
    NSMutableArray *dataArray;
    UILabel *titleLab;
    int selectRecord;
    UIButton *rightBtn;
}
@end

@implementation CaseHistoryController



- (void)viewDidLoad {
    if ([_departModel.SDepartStatus isEqualToString:@"出科"]) {
        rightBtn.hidden = YES;
    }
    [super viewDidLoad];
    dataArray = [NSMutableArray new];
    [self setUI];
}
+ (instancetype)shareObject{
    static CaseHistoryController *hisVC = nil;
    if (hisVC == nil) {
        hisVC = [CaseHistoryController new];
    }
    return hisVC;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    SdepartRecordController *recordVC = [SdepartRecordController shareObject];
    SDepartModel *departModel = recordVC.departModel;
    PersonEntity *persion = [[NSuserDefaultManager share] readCurrentUser];
    if (persion.roleId.intValue == 3) {
        if (![departModel.SDepartStatus isEqualToString:@"未入科"]) {
            rightBtn.hidden = NO;
        }else{
            rightBtn.hidden = YES;
        }
        if ([_departModel.SDepartStatus isEqualToString:@"出科"]) {
            rightBtn.hidden = YES;
        }
        
    }else{
        rightBtn.hidden = YES;
        if ([_departModel.SDepartStatus isEqualToString:@"出科"]) {
            rightBtn.hidden = YES;
        }
        
        
    }
    
    if ([self.from isEqualToString:@"student"]) {
        SdepartRecordController *recordVC = [SdepartRecordController shareObject];
        selectRecord = [recordVC.selectRecordIndex intValue];
        if (selectRecord < 3) {
            self.departModel = recordVC.departModel;
        }
    }else{
        RoundRecordController *recordVC = [RoundRecordController shareObject];
        selectRecord = [recordVC.selectRecordIndex intValue];
        if (selectRecord < 3) {
            self.departModel = recordVC.departModel;
        }
    }
    NSArray *titleArr = @[@"病例记录",@"操作记录",@"活动记录",@"教学记录",@"科研记录",@"获奖记录",@"论文记录",@"抢救记录",@"出诊记录",@"差错记录"];
    titleLab.text = titleArr[selectRecord];

    [dataArray removeAllObjects];
    [table reloadData];
    [self loadData];
}
- (void)loadData{
    NSString *url;
    if ([self.from isEqualToString:@"student"]) {
        SdepartRecordController *recordVC = [SdepartRecordController shareObject];
        if (selectRecord == 0) {
            url = [NSString stringWithFormat:@"%@/webappv2/studentMedicalCaseRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@&phaseScheduleMedicalCaseRequirementId=%@",SimpleIp,recordVC.departModel.SDepartRecordId,self.caseRequestModel.CaseReuestModelRequirementId];
        }else if(selectRecord == 1){
            url = [NSString stringWithFormat:@"%@/webappv2/studentOperationRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@&phaseScheduleOperationRequirementId=%@",SimpleIp,recordVC.departModel.SDepartRecordId,self.caseRequestModel.CaseReuestModelRequirementId];
        }else if (selectRecord == 2){
            url = [NSString stringWithFormat:@"%@/webappv2/studentActivityRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@&phaseScheduleActivityRequirementId=%@",SimpleIp,recordVC.departModel.SDepartRecordId,self.caseRequestModel.CaseReuestModelRequirementId];
        }else if (selectRecord == 3){
            url = [NSString stringWithFormat:@"%@/webappv2/studentTutoringRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 4){
            url = [NSString stringWithFormat:@"%@/webappv2/studentResearchRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 5){
            url = [NSString stringWithFormat:@"%@/webappv2/studentAwardRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 6){
            url = [NSString stringWithFormat:@"%@/webappv2/studentEssayPaperRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 7){
            url = [NSString stringWithFormat:@"%@/webappv2/studentRescueRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 8){
            url = [NSString stringWithFormat:@"%@/webappv2/studentTreatmentRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 9){
            url = [NSString stringWithFormat:@"%@/webappv2/studentMalpracticeRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }
    }else{
        RoundRecordController *recordVC = [RoundRecordController shareObject];
        if (selectRecord == 0) {
            url = [NSString stringWithFormat:@"%@/webappv2/studentMedicalCaseRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@&phaseScheduleMedicalCaseRequirementId=%@",SimpleIp,recordVC.departModel.SDepartRecordId,self.caseRequestModel.CaseReuestModelRequirementId];
        }else if(selectRecord == 1){
            url = [NSString stringWithFormat:@"%@/webappv2/studentOperationRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@&phaseScheduleOperationRequirementId=%@",SimpleIp,recordVC.departModel.SDepartRecordId,self.caseRequestModel.CaseReuestModelRequirementId];
        }else if (selectRecord == 2){
            url = [NSString stringWithFormat:@"%@/webappv2/studentActivityRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@&phaseScheduleActivityRequirementId=%@",SimpleIp,recordVC.departModel.SDepartRecordId,self.caseRequestModel.CaseReuestModelRequirementId];
        }else if (selectRecord == 3){
            url = [NSString stringWithFormat:@"%@/webappv2/studentTutoringRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 4){
            url = [NSString stringWithFormat:@"%@/webappv2/studentResearchRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 5){
            url = [NSString stringWithFormat:@"%@/webappv2/studentAwardRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 6){
            url = [NSString stringWithFormat:@"%@/webappv2/studentEssayPaperRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 7){
            url = [NSString stringWithFormat:@"%@/webappv2/studentRescueRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 8){
            url = [NSString stringWithFormat:@"%@/webappv2/studentTreatmentRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }else if (selectRecord == 9){
            url = [NSString stringWithFormat:@"%@/webappv2/studentMalpracticeRecords?pageStart=1&pageSize=999&studentClinicalRotationRecordId=%@",SimpleIp,recordVC.departModel.SDepartRecordId];
        }
    }
    
    
    
    [RequestTools RequestWithURL:url Method:@"get" Params:nil Message:@"" Success:^(NSDictionary *result) {
        [MBProgressHUD hideHUDForView:nil];
        [dataArray removeAllObjects];
        NSArray *arr = result[@"responseBody"][@"result"];
        if (selectRecord == 0) {
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseMedicalModel *model = [CaseMedicalModel new];
                model.recordId = resDic[@"studentMedicalCaseRecordId"];
                model.requestmentId = resDic[@"phaseScheduleMedicalCaseRequirementId"];
                model.caseOne = resDic[@"patientName"];
                model.caseTwo = resDic[@"reviewStatus"];
                model.caseThree = resDic[@"caseNo"];
                model.doctor = resDic[@"doctorFullName"];
                model.caseFour = resDic[@"reviewerFullName"];
                model.caseFive = resDic[@"caseTime"];
                model.mainIos = resDic[@"principalDiagnosis"];
                model.secondIos = resDic[@"secondaryDiagnosis"];
                model.advice = resDic[@"reviewerComments"];
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 1){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseOperateModel *model = [CaseOperateModel new];
                model.recordId = resDic[@"studentOperationRecordId"];
                model.requestmentId = resDic[@"phaseScheduleOperationRequirementId"];
                model.caseStatus = resDic[@"reviewStatus"];
                model.caseResult = resDic[@"result"];
                model.casePatientName = resDic[@"patientName"];
                model.caseNo = resDic[@"caseNo"];
                model.caseDate = resDic[@"operationTime"];
                model.caseTeacher = resDic[@"reviewerFullName"];
                model.caseReason = resDic[@"analysis"];
                model.caseDescription = resDic[@"description"];
                model.caseAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 2){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseActivityModel *model = [CaseActivityModel new];
                model.recordId = resDic[@"studentActivityRecordId"];
                model.requestmentId = resDic[@"phaseScheduleActivityRequirementId"];
                model.caseStatus = resDic[@"reviewStatus"];
                model.caseActivityName = resDic[@"activityName"];
                model.casePersion = resDic[@"organizerFullName"];
                model.caseInternal = resDic[@"duration"];
                model.caseDate = resDic[@"activityTime"];
                model.caseTeacher = resDic[@"reviewerFullName"];
                model.caseActivityContent = resDic[@"description"];
                model.caseAdvice = resDic[@"reviewerComments"];
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 3){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseTeachModel *model = [CaseTeachModel new];
                model.recordId = resDic[@"studentTutoringRecordId"];
                model.caseStatus = resDic[@"reviewStatus"];
                model.caseProject = resDic[@"tutoringProject"];
                model.caseTeachObject = resDic[@"tutoringObject"];
                model.caseNums = resDic[@"headCount"];
                model.caseStartTime = resDic[@"tutoringStartTime"];
                model.caseEndTime = resDic[@"tutoringEndTime"];
                model.caseTeacher = resDic[@"reviewerFullName"];
                model.caseTeachContent = resDic[@"description"];
                model.caseAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 4){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseSienceModel *model = [CaseSienceModel new];
                model.recordId = resDic[@"studentResearchRecordId"];
                model.caseSienceTitle = resDic[@"researchTask"];
                model.caseSienceHeader = resDic[@"topicHead"];
                model.caseSienceRole = resDic[@"playRole"];
                model.caseSienceDate = resDic[@"researchStartTime"];
                model.caseSienceEndDate = resDic[@"researchEndTime"];
                model.caseSienceStatus = resDic[@"reviewStatus"];
                model.caseSienceTeacherId = resDic[@"reviewerId"];
                model.caseSienceTeacher = resDic[@"reviewerFullName"];
                model.caseSienceComepleteDes = resDic[@"description"];
                model.caseSienceAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 5){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseAwardModel *model = [CaseAwardModel new];
                model.recordId = resDic[@"studentAwardRecordId"];
                model.caseAwardModelStatus = resDic[@"reviewStatus"];
                model.caseAwardModelTitle = resDic[@"awardTitle"];
                model.caseAwardModelDegree = resDic[@"awardLevel"];
                model.caseAwardModelDate = resDic[@"awardTime"];
                model.caseAwardModelTeacherId = resDic[@"reviewerId"];
                model.caseAwardModelTeacher = resDic[@"reviewerFullName"];
                model.caseAwardModelDescroption = resDic[@"description"];
                model.caseAwardModelAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 6){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseEssayModel *model = [CaseEssayModel new];
                model.recordId = resDic[@"studentEssayPaperRecordId"];
                model.caseEssayModelStatus = resDic[@"reviewStatus"];
                model.caseEssayModelTitle = resDic[@"paperTitle"];
                model.caseEssayModelDegree = resDic[@"paperType"];
                model.caseEssayModelCategory = resDic[@"articleType"];
                model.caseEssayModelAuthor = resDic[@"authorRank"];
                model.caseEssayModelEdtion = resDic[@"publishedJournal"];
                model.caseEssayModelDate = resDic[@"publishedTime"];
                model.caseEssayModelTeacher = resDic[@"reviewerFullName"];
                model.caseEssayModelTeacherId = resDic[@"reviewerId"];
                model.caseEssayModelDescription = resDic[@"description"];
                model.caseEssayModelAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 7){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseRescueModel *model = [CaseRescueModel new];
                model.recordId = resDic[@"studentRescueRecordId"];
                model.caseRescueModelStatus = resDic[@"reviewStatus"];
                model.caseRescueModelDiseaseName = resDic[@"diseaseName"];
                model.caseRescueModelDiseaserName = resDic[@"patientName"];
                model.caseRescueModelDiseaseNum = resDic[@"caseNo"];
                model.caseRescueModelBackDescription = resDic[@"outcome"];
                model.caseRescueModelDate = resDic[@"rescueTime"];
                model.caseRescueModelTeacher = resDic[@"reviewerFullName"];
                model.caseRescueModelTeacherId = resDic[@"reviewerId"];
                model.caseRescueModelDescription = resDic[@"description"];
                model.caseRescueModelAdvice = resDic[@"reviewerComments"];
                model.doctorFullName=resDic[@"doctorFullName"];
                
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 8){
            
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseTreatModel *model = [CaseTreatModel new];
                model.recordId = resDic[@"studentTreatmentRecordId"];
                model.caseTreatModelStatus = resDic[@"reviewStatus"];
                model.caseTreatModelCategory = resDic[@"diseaseICDName"];
                model.caseTreatModelCategoryId = resDic[@"diseaseICDId"];
                model.caseTreatModelDate = resDic[@"visitTime"];
                model.caseTreatModelTeacher = resDic[@"reviewerFullName"];
                model.caseTreatModelTeacherId = resDic[@"reviewerId"];
                model.caseTreatModelDescription = resDic[@"description"];
                model.caseTreatModelAdvice = resDic[@"reviewerComments"];
                if (![resDic[@"file"] isKindOfClass:[NSNull class]]) {
                    model.fileUrl = resDic[@"file"][@"fileUrl"];
                }else{
                    model.fileUrl = @"";
                }
                [dataArray addObject:model];
            }
            [table reloadData];
        }else if (selectRecord == 9){
            for (int i=0; i<arr.count; i++) {
                NSDictionary *resDic = arr[i];
                CaseMistakeModel *model = [CaseMistakeModel new];
                model.recordId = resDic[@"studentMalpracticeRecordId"];
                model.caseMistakeModelStatus = resDic[@"reviewStatus"];
                model.caseMistakeModelCategory = resDic[@"accidentType"];
                model.caseMistakeModelDegree = resDic[@"accidentLevel"];
                model.caseMistakeModelDate = resDic[@"accidentTime"];
                model.caseMistakeModelTeacher = resDic[@"reviewerFullName"];
                model.caseMistakeModelTeacherId = resDic[@"reviewerId"];
                model.caseMistakeModelViaDescription = resDic[@"description"];
                model.caseMistakeModelReason = resDic[@"rootCause"];
                model.caseMistakeModelLession = resDic[@"lesson"];
                model.caseMistakeModelDepartmentId = resDic[@"accidentDepartmentId"];
                model.caseMistakeModelDepartmentName = resDic[@"accidentDepartmentName"];
                model.caseMistakeModelAdvice = resDic[@"reviewerComments"];
                model.caseMistakeModelLeader = resDic[@"departmentHead"];
                model.caseMistakeModelLeaderAdvice = resDic[@"departmentHeadComments"];
                [dataArray addObject:model];
            }
            [table reloadData];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD hideHUDForView:nil];
    }];
}
- (void)setUI {
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(Swidth-35, 29.5, 25, 25);
    [rightBtn setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if(![_departModel.SDepartStatus isEqualToString:@"出科"]){
         [backNavigation addSubview:rightBtn];
    }
   


    titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    [backNavigation addSubview:titleLab];
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, Swidth, Sheight-64)];
    table.delegate = self;
    table.dataSource = self;
    table.backgroundColor = UIColorFromHex(0xF0F0F0);
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    [table registerNib:[UINib nibWithNibName:@"RecordCell" bundle:nil] forCellReuseIdentifier:@"recordCell"];
    [self.view addSubview:table];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)selectAction:(UIButton *)button{
    [SubjectTool share].subjectType = subjectTypeAdd;
    int selects;
    if ([_from isEqualToString:@"student"]) {
        SdepartRecordController *recordVC = [SdepartRecordController shareObject];
        selects = recordVC.selectRecordIndex.intValue;
    }else{
        RoundRecordController *roundVC = [RoundRecordController shareObject];
        selects = roundVC.selectRecordIndex.intValue;
    }
    if (selects == 0) {
        SCaseRecordController *recordAddVC = [SCaseRecordController new];
        recordAddVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:recordAddVC animated:YES];
    }else if (selects == 1){
        OperateRecordController *operateVC = [OperateRecordController new];
        operateVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:operateVC animated:YES];
    }else if (selects == 2){
        ActivityRecordController *activityVC = [ActivityRecordController new];
        activityVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:activityVC animated:YES];
    }else if (selects == 3){
        TeachRecordController *teachVC = [TeachRecordController new];
        teachVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:teachVC animated:YES];
    }else if (selects == 4){
        ScienceRecordController *scienceVC = [ScienceRecordController new];
        scienceVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:scienceVC animated:YES];
    }else if (selects == 5){
        RewardRecordController *rewardVC = [RewardRecordController new];
        rewardVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:rewardVC animated:YES];
    }else if (selects == 6){
        PaperRecordController *paperVC = [PaperRecordController new];
        paperVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:paperVC animated:YES];
    }else if (selects == 7){
        RescueRecordController *rescueVC = [RescueRecordController new];
        rescueVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:rescueVC animated:YES];
    }else if (selects == 8){
        VisitRecordController *visitVC = [VisitRecordController new];
        visitVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:visitVC animated:YES];
    }else if (selects == 9){
        MistakeRecordController *misVC = [MistakeRecordController new];
        misVC.model = self.caseRequestModel;
        [self.navigationController pushViewController:misVC animated:YES];
    }
}
#pragma -delegate
- (void)selectIndexPathRow:(NSInteger)index{
    if (index ==0) {
        
    }else if (index == 1){
        
    }else if (index == 2){
        
    }else if (index == 3){
        
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    int select;
    
    NSLog(@"indexPath.row===%lu",indexPath.row);
   
    if ([_from isEqualToString:@"student"]) {
        SdepartRecordController *recordVC = [SdepartRecordController shareObject];
        select = recordVC.selectRecordIndex.intValue;
    }else{
        RoundRecordController *roundVC = [RoundRecordController shareObject];
        select = roundVC.selectRecordIndex.intValue;
    }
     NSLog(@"select===%d",select);
    if (select == 0) {
        DetailRecordController *errorVC = [DetailRecordController new];
        errorVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:errorVC animated:YES];
    }else if (select == 1){
        OperateDetailController *operateVC = [OperateDetailController new];
        operateVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:operateVC animated:YES];
    }else if (select == 2){
        ActivityDetailController *activityVC = [ActivityDetailController new];
        activityVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:activityVC animated:YES];
    }else if (select == 3){
        TeachDetailController *teachVC = [TeachDetailController new];
        teachVC.model = dataArray[indexPath.row];
        teachVC.SDepartModel=_departModel;
        
        [self.navigationController pushViewController:teachVC animated:YES];
    }else if (select == 4){
        SienceDetailController *sienceVC = [SienceDetailController new];
        sienceVC.SDepartModel=_departModel;
        sienceVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:sienceVC animated:YES];
    }else if (select == 5){
        AwardDetailController *awardVC = [AwardDetailController new];
        awardVC.SDepartModel=_departModel;
        awardVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:awardVC animated:YES];
    }else if (select == 6){
        EssayDetailController *essayVC = [EssayDetailController new];
        essayVC.SDepartModel=_departModel;
        essayVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:essayVC animated:YES];
    }else if (select == 7){
        RescueDetailController *rescueVC = [RescueDetailController new];
        rescueVC.SDepartModel=_departModel;
        rescueVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:rescueVC animated:YES];
    }else if (select == 8){
        TreatDetailController *treatVC = [TreatDetailController new];
        treatVC.SDepartModel=_departModel;
        treatVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:treatVC animated:YES];
    }else{
        MistakeDetailController *mistakeVC = [MistakeDetailController new];
        mistakeVC.SDepartModel=_departModel;
        mistakeVC.model = dataArray[indexPath.row];
        [self.navigationController pushViewController:mistakeVC animated:YES];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int select;
    RecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"recordCell"];
    if ([_from isEqualToString:@"student"]) {
        SdepartRecordController *recordVC = [SdepartRecordController shareObject];
        select = recordVC.selectRecordIndex.intValue;
    }else{
        RoundRecordController *roundVC = [RoundRecordController shareObject];
        select = roundVC.selectRecordIndex.intValue;
    }
    
    if (select == 0) {
        CaseMedicalModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"病人姓名:";
        cell.twolineLab.text = @"病案号:";
        cell.threelineLab.text = @"上级医师:";
        cell.dateLab.text = @"病案时间:";
        cell.onelineContent.text = model.caseOne;
        cell.status.text = model.caseTwo;
        cell.twolineContent.text = model.caseThree;
        cell.threelineContent.text = model.doctor;
        //控制显示隐藏
        cell.threelineLab.hidden = NO;
        cell.threelineContent.hidden = NO;
        
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseFive.stringValue];
        if ([model.caseTwo isEqualToString:@"待审核"]) {
            cell.status.backgroundColor = [UIColor orangeColor];
        }else if ([model.caseTwo isEqualToString:@"已通过"]){
            cell.status.backgroundColor = UIColorFromHex(0x20B2AA);
        }else{
            cell.status.backgroundColor = [UIColor redColor];
        }
    }else if (select == 1){
        CaseOperateModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"操作结果:";
        cell.twolineLab.text = @"病人姓名:";
        cell.threelineLab.text = @"病案号:";
        cell.dateLab.text = @"操作时间:";
        cell.onelineContent.text = model.caseResult;
        cell.twolineContent.text = model.casePatientName;
        cell.status.text = model.caseStatus;
        //控制显示隐藏
        cell.threelineLab.hidden = NO;
        cell.threelineContent.hidden = NO;

        cell.threelineContent.text = [NSString stringWithFormat:@"%@",model.caseNo];
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseDate.stringValue];
        if ([model.caseStatus isEqualToString:@"待审核"]) {
            cell.status.backgroundColor = [UIColor orangeColor];
        }else if ([model.caseStatus isEqualToString:@"已通过"]){
            cell.status.backgroundColor = UIColorFromHex(0x20B2AA);
        }else{
            cell.status.backgroundColor = [UIColor redColor];
        }
    }else if (select == 2){
        CaseActivityModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"活动名称:";
        cell.twolineLab.text = @"组织人:";
        cell.threelineLab.text = @"活动时长:";
        cell.dateLab.text = @"活动时间:";
        cell.status.text = model.caseStatus;
        cell.onelineContent.text = model.caseActivityName;
        cell.twolineContent.text = model.casePersion;
        cell.threelineContent.text = [NSString stringWithFormat:@"%@",model.caseInternal];
        //控制显示隐藏
        cell.threelineLab.hidden = NO;
        cell.threelineContent.hidden = NO;
        
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseDate.stringValue];
        if ([model.caseStatus isEqualToString:@"待审核"]) {
            cell.status.backgroundColor = [UIColor orangeColor];
        }else if ([model.caseStatus isEqualToString:@"已通过"]){
            cell.status.backgroundColor = UIColorFromHex(0x20B2AA);
        }else{
            cell.status.backgroundColor = [UIColor redColor];
        }
    }else if (select == 3){
        CaseTeachModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"教学项目:";
        cell.twolineLab.text = @"带教对象:";
        cell.threelineLab.text = @"人数:";
        cell.dateLab.text = @"时间:";
        cell.status.text = model.caseStatus;
        cell.onelineContent.text = model.caseProject;
        cell.twolineContent.text = model.caseTeachObject;
        cell.threelineContent.text = [NSString stringWithFormat:@"%@",model.caseNums];
        //控制显示隐藏
        cell.threelineLab.hidden = NO;
        cell.threelineContent.hidden = NO;
        
        NSString *start = [[Maneger shareObject] timeFormatter1:model.caseStartTime.stringValue];
        NSString *end = [[Maneger shareObject] timeFormatter1:model.caseEndTime.stringValue];
        cell.dateLab.text = [NSString stringWithFormat:@"开始:%@-",start];
        cell.dateLab.adjustsFontSizeToFitWidth = YES;
        cell.dateContent.text = [NSString stringWithFormat:@"-结束:%@",end];
        cell.dateContent.adjustsFontSizeToFitWidth = YES;
        if ([model.caseStatus isEqualToString:@"待审核"]) {
            cell.status.backgroundColor = [UIColor orangeColor];
        }else if ([model.caseStatus isEqualToString:@"已通过"]){
            cell.status.backgroundColor = UIColorFromHex(0x20B2AA);
        }else{
            cell.status.backgroundColor = [UIColor redColor];
        }
    }else if (select == 4){
        CaseSienceModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"题目:";
        cell.twolineLab.text = @"课程负责人:";
        cell.threelineLab.text = @"参与角色:";
        cell.dateLab.text = @"活动时间:";
        cell.onelineContent.text = model.caseSienceTitle;
        cell.twolineContent.text = model.caseSienceHeader;
        cell.threelineContent.text = model.caseSienceRole;
        //控制显示隐藏
        cell.threelineLab.hidden = NO;
        cell.threelineContent.hidden = NO;
        
        cell.status.text = model.caseSienceStatus;
        NSString *time = [[Maneger shareObject] timeFormatter1:model.caseSienceDate.stringValue];
        cell.dateContent.text = time;
        if ([model.caseSienceStatus isEqualToString:@"待审核"]) {
            cell.status.backgroundColor = [UIColor orangeColor];
        }else if ([model.caseSienceStatus isEqualToString:@"已通过"]){
            cell.status.backgroundColor = UIColorFromHex(0x20B2AA);
        }else{
            cell.status.backgroundColor = [UIColor redColor];
        }
    }else if (select == 5){
        CaseAwardModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"获奖名称:";
        cell.twolineLab.text = @"获奖级别:";
        cell.threelineLab.text = @"带教老师:";
        cell.dateLab.text = @"获奖时间:";
        cell.onelineContent.text = model.caseAwardModelTitle;
        cell.twolineContent.text = model.caseAwardModelDegree;
        cell.threelineContent.text = model.caseAwardModelTeacher;
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseAwardModelDate.stringValue];
        //控制显示隐藏
        cell.threelineLab.hidden = NO;
        cell.threelineContent.hidden = NO;

        
        
        cell.status.text = model.caseAwardModelStatus;
        if ([model.caseAwardModelStatus isEqualToString:@"待审核"]) {
            cell.status.backgroundColor = [UIColor orangeColor];
        }else if ([model.caseAwardModelStatus isEqualToString:@"已通过"]){
            cell.status.backgroundColor = UIColorFromHex(0x20B2AA);
        }else{
            cell.status.backgroundColor = [UIColor redColor];
        }
    }else if (select == 6){
        CaseEssayModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"论文题目:";
        cell.twolineLab.text = @"论文级别:";
        cell.threelineLab.text = @"论文类别:";
        cell.dateLab.text = @"发表时间:";
        cell.onelineContent.text = model.caseEssayModelTitle;
        cell.twolineContent.text = model.caseEssayModelDegree;
        cell.threelineContent.text = model.caseEssayModelCategory;
        //控制显示隐藏
        cell.threelineLab.hidden = NO;
        cell.threelineContent.hidden = NO;

        
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseEssayModelDate.stringValue];
        cell.status.text = model.caseEssayModelStatus;
        if ([model.caseEssayModelStatus isEqualToString:@"待审核"]) {
            cell.status.backgroundColor = [UIColor orangeColor];
        }else if ([model.caseEssayModelStatus isEqualToString:@"已通过"]){
            cell.status.backgroundColor = UIColorFromHex(0x20B2AA);
        }else{
            cell.status.backgroundColor = [UIColor redColor];
        }
    }else if (select == 7){
        CaseRescueModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"疾病名称:";
        cell.twolineLab.text = @"病人姓名:";
        cell.threelineLab.text = @"转归情况:";
        cell.dateLab.text = @"抢救时间:";
        
        NSTimeInterval interval    =[ model.caseRescueModelDate doubleValue] / 1000.0;
        NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString       = [formatter stringFromDate: date];
        
        
        cell.dateContent.text=[NSString stringWithFormat:@"%@",dateString];
        cell.status.text = model.caseRescueModelStatus;
        cell.onelineContent.text = model.caseRescueModelDiseaseName;
        cell.twolineContent.text = model.caseRescueModelDiseaserName;
        
        cell.threelineContent.text = model.caseRescueModelBackDescription;
        NSLog(@"threelineContent==%@",cell.threelineContent.text);
        //控制显示隐藏
        cell.threelineLab.hidden = NO;
        cell.threelineContent.hidden = NO;

        
        if ([model.caseRescueModelStatus isEqualToString:@"待审核"]) {
            cell.status.backgroundColor = [UIColor orangeColor];
        }else if ([model.caseRescueModelStatus isEqualToString:@"已通过"]){
            cell.status.backgroundColor = UIColorFromHex(0x20B2AA);
        }else{
            cell.status.backgroundColor = [UIColor redColor];
        }
    }else if (select == 8){
        CaseTreatModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"疾病种类:";
        cell.twolineLab.text = @"带教老师:";
        cell.status.text = model.caseTreatModelStatus;
        //控制显示隐藏
        cell.threelineLab.hidden = YES;
        cell.threelineContent.hidden = YES;
        
        cell.dateLab.text = @"出诊时间:";
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseTreatModelDate.stringValue];
        cell.onelineContent.text = model.caseTreatModelCategory;
        cell.twolineContent.text = model.caseTreatModelTeacher;
        if ([model.caseTreatModelStatus isEqualToString:@"待审核"]) {
            cell.status.backgroundColor = [UIColor orangeColor];
        }else if ([model.caseTreatModelStatus isEqualToString:@"已通过"]){
            cell.status.backgroundColor = UIColorFromHex(0x20B2AA);
        }else{
            cell.status.backgroundColor = [UIColor redColor];
        }
    }else{
        CaseMistakeModel *model = dataArray[indexPath.row];
        cell.onelineLab.text = @"类别:";
        cell.twolineLab.text = @"等级:";
        cell.threelineLab.text = @"院领导:";
        cell.dateLab.text = @"发生时间:";
        cell.status.text = model.caseMistakeModelStatus;
        
        cell.onelineContent.text = model.caseMistakeModelCategory;
        cell.twolineContent.text = model.caseMistakeModelDegree;
        cell.threelineContent.text = model.caseMistakeModelLeader;
        //控制显示隐藏
        cell.threelineLab.hidden = NO;
        cell.threelineContent.hidden = NO;

        
        cell.dateContent.text = [[Maneger shareObject] timeFormatter1:model.caseMistakeModelDate.stringValue];
        if ([model.caseMistakeModelStatus isEqualToString:@"待审核"]) {
            cell.status.backgroundColor = [UIColor orangeColor];
        }else if ([model.caseMistakeModelStatus isEqualToString:@"已通过"]){
            cell.status.backgroundColor = UIColorFromHex(0x20B2AA);
        }else{
            cell.status.backgroundColor = [UIColor redColor];
        }
    }
    return cell;
}
- (void)statusAction:(UIButton *)btn{
    
}
@end

