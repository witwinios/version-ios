//
//  ImageChangeController.h
//  yun
//
//  Created by MacAir on 2017/9/4.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ZZRGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
@interface ImageChangeController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
typedef void (^ImageBlock)(UIImage *image);
@property (strong, nonatomic) AccountEntity *entity;
@property (strong, nonatomic) UIImage *img;
@property (strong, nonatomic) ImageBlock imageBlock;
@end
