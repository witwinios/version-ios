//
//  ApplyAddStudentViewController.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/6.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^BackStudent) (NSArray *arr);
//typedef void () ;
@interface ApplyAddStudentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchResultsUpdating,UITextFieldDelegate>

@property(nonatomic,getter=isEditing) BOOL editing;
@property(nonatomic,strong) NSString *scheduleIdString;
@property(nonatomic,strong) NSMutableArray *selectorPatnArray;//存放选中数据
@property(nonatomic,strong) NSString *scheduleStatusString;

@property (strong, nonatomic) BackStudent backArr;

-(void)setEditing:(BOOL)editing animated:(BOOL)animated;

@end
