//
//  TeachRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "TeachRecordController.h"
#import "THDatePickerView.h"
@interface TeachRecordController ()
{
    NSString *current_date;
    NSString *current_end_date;
    UIView *btnBack;
    UIButton *currentDateBtn;
    
    NSNumber *has_image;
    
    UIView *toolbar;
}
@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) UIButton *btn;

@end

@implementation TeachRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    current_date = @"";
    current_end_date = @"";
    has_image = @0;
    if ([SubjectTool share].subjectType == subjectTypeUpdate) {
        _organizationField.text = self.myModel.caseProject;
        _objectField.text = self.myModel.caseTeachObject;
        _numField.text = [NSString stringWithFormat:@"%@",self.myModel.caseNums];
        _contentTextview.text = self.myModel.caseTeachContent;
        
        if (self.myModel.caseStartTime.intValue == 0) {
            current_date = @"";
        }else if (self.myModel.caseEndTime.intValue == 0){
            current_end_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseStartTime];
            current_end_date = [NSString stringWithFormat:@"%@",self.myModel.caseEndTime];
            [self.startBtn setTitle:[[Maneger shareObject] timeFormatter1:self.myModel.caseStartTime.stringValue] forState:0];
            [self.endBtn setTitle:[[Maneger shareObject] timeFormatter1:self.myModel.caseEndTime.stringValue] forState:0];
        }
    }
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonHide:) name:UIKeyboardWillHideNotification object:nil];
    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];
    self.startBtn.layer.cornerRadius = 4;
    self.startBtn.layer.masksToBounds = YES;
    self.startBtn.titleLabel.textAlignment = 0;
    self.startBtn.layer.cornerRadius = 4;
    self.startBtn.layer.masksToBounds = YES;
    self.startBtn.layer.cornerRadius = 4;
    self.endBtn.layer.masksToBounds = YES;
    self.endBtn.titleLabel.textAlignment = 0;
    self.endBtn.layer.cornerRadius = 4;
    self.endBtn.layer.masksToBounds = YES;
    THDatePickerView *dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        [currentDateBtn setTitle:[[Maneger shareObject] timeFormatter1:longDate] forState:0];
        if (currentDateBtn == _startBtn) {
            current_date = longDate;
        }else{
            current_end_date = longDate;
        }
    } andCancelBlock:^{
        self.btn.hidden = YES;
    } dateType:DateTypeDay];
    [self.view addSubview:dateView];
    self.dateView = dateView;
    
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"教学记录";
    [backNavigation addSubview:titleLab];
    
    self.numField.keyboardType = UIKeyboardTypeNumberPad;
}
- (void)resignFirstRespond{
    [self.organizationField resignFirstResponder];
    [self.numField resignFirstResponder];
    [self.objectField resignFirstResponder];
    [self.contentTextview resignFirstResponder];
}
- (void)hideKb:(UIButton *)btn{
    [self resignFirstRespond];
}
-(void)doneButtonshow: (NSNotification *)notification {
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
}
- (void)doneButtonHide:(NSNotification *)notification{
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
    }];
}
#pragma -action
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)startAction:(id)sender {
    currentDateBtn = (UIButton *)sender;
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];
    
}
- (IBAction)endAction:(id)sender {
    currentDateBtn = (UIButton *)sender;
    self.btn.hidden = NO;
    [btnBack removeFromSuperview];
    [self resignFirstRespond];
    [self.dateView show];
    
}
- (void)saveAction{
    if ([_organizationField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"教学项目不能为空~" places:0 toView:nil];
        return;
    }
    if ([_objectField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"带教对象不能为空~" places:0 toView:nil];
        return;
    }
    if ([_numField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"人数不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"开始时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_end_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"结束时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_end_date compare:current_date] < 0) {
        [MBProgressHUD showToastAndMessage:@"结束时间要大于开始时间~" places:0 toView:nil];
        return;
    }
    [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
    if (has_image.intValue == 0) {
        SdepartRecordController *departVC = [SdepartRecordController shareObject];
        
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentTutoringRecords",SimpleIp];
        
        NSDictionary *params = @{@"tutoringProject":_organizationField.text,@"headCount":_numField.text,@"tutoringStartTime":current_date,@"tutoringEndTime":current_end_date,@"tutoringObject":_objectField.text,@"description":_contentTextview.text,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        
        [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
            [MBProgressHUD hideHUDForView:nil];
            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
        [RequestTools RequestWithFile:_current_Image.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentTutoringRecords",SimpleIp];
            
            NSDictionary *params = @{@"tutoringProject":_organizationField.text,@"headCount":_numField.text,@"tutoringStartTime":current_date,@"tutoringEndTime":current_end_date,@"tutoringObject":_objectField.text,@"description":_contentTextview.text,@"fileId":fileId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
            
            [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
                [MBProgressHUD hideHUDForView:nil];
                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];

                }
                else{
                    [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
            }];
        } failed:^(NSString *result) {
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }
    
}
- (void)updateAction{
    if ([_organizationField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"教学项目不能为空~" places:0 toView:nil];
        return;
    }
    if ([_objectField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"带教对象不能为空~" places:0 toView:nil];
        return;
    }
    if ([_numField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"人数不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"开始时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_end_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"结束时间不能为空~" places:0 toView:nil];
        return;
    }
    if (has_image.intValue == 0) {
        [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
        SdepartRecordController *departVC = [SdepartRecordController shareObject];
        
        NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentTutoringRecords/%@",SimpleIp,self.myModel.recordId];
        
        NSDictionary *params = @{@"tutoringProject":_organizationField.text,@"headCount":_numField.text,@"tutoringStartTime":current_date,@"tutoringEndTime":current_end_date,@"tutoringObject":_objectField.text,@"description":_contentTextview.text,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
        
        [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
            [MBProgressHUD hideHUDForView:nil];

            if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
                [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
            }else{
                [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
            }
        } failed:^(NSString *result) {
            [MBProgressHUD hideHUDForView:nil];

            [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
        }];
    }else{
        [MBProgressHUD showHUDAndMessage:@"加载中~" toView:nil];
        [RequestTools RequestWithFile:_current_Image.image andParams:@{@"fileName":@"appimage",@"description":@"test",@"isPublic":@"true"} andUrl:[NSString stringWithFormat:@"%@/files",LocalIP] Success:^(NSDictionary *result) {
            NSNumber *fileId= [[result objectForKey:@"responseBody"] objectForKey:@"fileId"];
            
            SdepartRecordController *departVC = [SdepartRecordController shareObject];
            
            NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentTutoringRecords/%@",SimpleIp,self.myModel.recordId];
            
            NSDictionary *params = @{@"tutoringProject":_organizationField.text,@"headCount":_numField.text,@"tutoringStartTime":current_date,@"tutoringEndTime":current_end_date,@"tutoringObject":_objectField.text,@"description":_contentTextview.text,@"fileId":fileId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
            
            [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
                [MBProgressHUD hideHUDForView:nil];

                if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
                    [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
                    [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1.5 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
                }else{
                    [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
                }
            } failed:^(NSString *result) {
                [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
            }];
        } failed:^(NSString *result) {
            [MBProgressHUD hideHUDForView:nil];
            [MBProgressHUD showToastAndMessage:@"文件上传失败~" places:0 toView:nil];
        }];
    }
}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}
- (void)popLast{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)fileAction:(id)sender {
    UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相册",@"拍照", nil];
    [sheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self getAlbumAction];
            break;
        case 1:
            [self photoAction];
            break;
        default:
            break;
    }
}
- (void)photoAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    _imagePickController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)getAlbumAction{
    _imagePickController = [[UIImagePickerController alloc] init];
    _imagePickController.delegate = self;
    _imagePickController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickController.allowsEditing = YES;
    _imagePickController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePickController animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    [picker dismissViewControllerAnimated:YES completion:nil];
    has_image = @1;
    _current_Image.image = [editingInfo objectForKey:UIImagePickerControllerOriginalImage];
}

- (IBAction)saveAction:(id)sender {
    if ([SubjectTool share].subjectType == subjectTypeAdd) {
        [self saveAction];
    }else{
        [self updateAction];
    }
}
@end

