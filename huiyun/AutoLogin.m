//
//  AutoLogin.m
//  yun
//
//  Created by MacAir on 2017/8/30.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "AutoLogin.h"

@implementation AutoLogin
+ (id)share{
    static AutoLogin *maneger = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        if (maneger == nil) {
            maneger = [[AutoLogin alloc]init];
        }
    });
    return maneger;
}
- (void)autoLogin{
    NSString *requestUrl = [NSString stringWithFormat:@"%@/accounts/login",LocalIP];
    PersonEntity *person = [[NSuserDefaultManager share] readLastAccount];
    NSDictionary *params = @{@"userName":person.userAccount,@"userPwd":person.userPassword,@"organizationCode":LocalCODE};
    
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
        [self autoLoginIM];
    } failed:^(NSString *result) {
        _statusBlock(requestStatusFailed);
    }];
}
- (void)autoLoginIM{
    PersonEntity *person = [[NSuserDefaultManager share] readLastAccount];
    
    [[SPKitExample sharedInstance] callThisAfterISVAccountLoginSuccessWithYWLoginId:person.userAccountIM passWord:person.userPasswordIM preloginedBlock:^{
        //登录之前
    } successBlock:^{
        //成功
        [self autoLoginAccount];
    } failedBlock:^(NSError *aError) {
        _statusBlock(requestStatusFailed);
    }];
}
- (void)autoLoginAccount{
    AccountEntity *accountEntity = [AccountEntity new];
    PersonEntity *person = [[NSuserDefaultManager share] readLastAccount];
    NSString *requestIM = [NSString stringWithFormat:@"%@/users/%@",LocalIP,person.userID];
    
    [RequestTools RequestWithURL:requestIM Method:@"get" Params:nil Success:^(NSDictionary *result) {
        if (person.roleId.intValue == 3) {
            //组装数据
            if (![result[@"responseBody"][@"picture"] isKindOfClass:[NSNull class]]) {
                accountEntity.userFileUrl = [NSString stringWithFormat:@"%@?CTTS-Token=%@",result[@"responseBody"][@"picture"][@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
            }
            accountEntity.userFullName = result[@"responseBody"][@"fullName"];
            accountEntity.userSex = result[@"responseBody"][@"gender"];
            accountEntity.userNo = result[@"responseBody"][@"userNo"];
            accountEntity.userMajor = result[@"responseBody"][@"major"];
            accountEntity.userPhoneNo = result[@"responseBody"][@"personalPhoneNo"];
            accountEntity.userEmail = result[@"responseBody"][@"email"];
            accountEntity.userCard = result[@"responseBody"][@"identificationNo"];
            accountEntity.userTitle = result[@"responseBody"][@"titleName"];
            accountEntity.userTitleType = result[@"responseBody"][@"titleType"];
            accountEntity.isDoctor = result[@"responseBody"][@"isDoctor"];
            accountEntity.userDegree = result[@"responseBody"][@"degree"];
            accountEntity.userSchool = result[@"responseBody"][@"schoolName"];
            accountEntity.userSource = result[@"responseBody"][@"source"];
            accountEntity.userDescription = result[@"responseBody"][@"description"];
            accountEntity.userID = result[@"responseBody"][@"userId"];
            accountEntity.roleId = @3;
//            accountEntity.userAccount = pers.userAccount;
//            accountEntity.userPassword = entity.userPassword;
            accountEntity.userName = result[@"responseBody"][@"userName"];
            accountEntity.userDegreeType = result[@"responseBody"][@"degreeType"];
            accountEntity.userEducationType = result[@"responseBody"][@"educationType"];
            accountEntity.userBlankcardOutlets = result[@"responseBody"][@"bankOutlets"];
            accountEntity.userBlankNumber = result[@"responseBody"][@"bankCardNumber"];
            
            NSString *blankCardFomat = @"";
            NSString *blankCard = [NSString stringWithFormat:@"%@",accountEntity.userBlankNumber];
            for (int i=0; i<blankCard.length; i++) {
                if (i == 0 || i == blankCard.length-1) {
                    blankCardFomat = [blankCardFomat stringByAppendingString:[NSString stringWithFormat:@"%c",[blankCard characterAtIndex:i]]];
                }else{
                    blankCardFomat = [blankCardFomat stringByAppendingString:@"*"];
                }
            }
            accountEntity.userHideBlankCard = blankCardFomat;
            
            NSString *cardFomat= @"";
            NSString *cardStr = [NSString stringWithFormat:@"%@",accountEntity.userCard];
            if (cardStr.length !=0 && cardStr.length != 1) {
                for (int i=0; i<cardStr.length; i++) {
                    if (i == 0 || i == cardFomat.length-1) {
                        cardFomat = [cardFomat stringByAppendingString:[NSString stringWithFormat:@"%c",[cardStr characterAtIndex:i]]];
                    }else{
                        cardFomat = [cardFomat stringByAppendingString:@"*"];
                    }
                }
            }
            NSString *phoneFomat=  @"";
            NSString *phone = [NSString stringWithFormat:@"%@",accountEntity.userPhoneNo];
            if (phone.length != 0 && phone.length != 1) {
                for (int i=0; i<phone.length; i++) {
                    if (i == 0 || i == phone.length-1) {
                        phoneFomat = [phoneFomat stringByAppendingString:[NSString stringWithFormat:@"%c",[phone characterAtIndex:i]]];
                    }else{
                        phoneFomat = [phoneFomat stringByAppendingString:@"*"];
                    }
                }
            }
            accountEntity.userHideCard = cardFomat;
            accountEntity.userHidePhone = phoneFomat;
            //保存
            [[NSuserDefaultManager share] saveCurrentAccount:accountEntity];
            //
            _statusBlock(requestStatusSucess);
        }else if (person.roleId.intValue == 2){
            if (![result[@"responseBody"][@"picture"] isKindOfClass:[NSNull class]]) {
                accountEntity.userFileUrl = [NSString stringWithFormat:@"%@?CTTS-Token=%@",result[@"responseBody"][@"picture"][@"fileUrl"],[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]];
            }
            accountEntity.userFullName = result[@"responseBody"][@"fullName"];
            accountEntity.userSex = result[@"responseBody"][@"gender"];
            accountEntity.userNo = result[@"responseBody"][@"userNo"];
            accountEntity.userMajor = result[@"responseBody"][@"major"];
            accountEntity.userPhoneNo = result[@"responseBody"][@"personalPhoneNo"];
            accountEntity.userEmail = result[@"responseBody"][@"email"];
            accountEntity.userCard = result[@"responseBody"][@"identificationNo"];
            accountEntity.userTitle = result[@"responseBody"][@"titleName"];
            accountEntity.userTitleType = result[@"responseBody"][@"titleType"];
            accountEntity.isDoctor = result[@"responseBody"][@"isDoctor"];
            accountEntity.userDegree = result[@"responseBody"][@"degree"];
            accountEntity.userSchool = result[@"responseBody"][@"schoolName"];
            accountEntity.userSource = result[@"responseBody"][@"source"];
            accountEntity.userCareer = result[@"responseBody"][@"teachingRecord"][@"career"];
            accountEntity.userTeacherType = result[@"responseBody"][@"teachingRecord"][@"teacherType"];
            accountEntity.userWorkStart = result[@"responseBody"][@"teachingRecord"][@"startDate"];
            accountEntity.userWorkEnd = result[@"responseBody"][@"teachingRecord"][@"endDate"];
            accountEntity.userDescription = result[@"responseBody"][@"description"];
            accountEntity.userID = result[@"responseBody"][@"userId"];
            accountEntity.roleId = @2;
//            accountEntity.userAccount = entity.userAccount;
//            accountEntity.userPassword = entity.userPassword;
            accountEntity.userName = result[@"responseBody"][@"userName"];
            NSString *cardFomat= @"";
            NSString *cardStr = [NSString stringWithFormat:@"%@",accountEntity.userCard];
            if (cardStr.length !=0 && cardStr.length != 1) {
                for (int i=0; i<cardStr.length; i++) {
                    if (i == 0 || i == cardStr.length-1) {
                        cardFomat = [cardFomat stringByAppendingString:[NSString stringWithFormat:@"%c",[cardStr characterAtIndex:i]]];
                    }else{
                        cardFomat = [cardFomat stringByAppendingString:@"*"];
                    }
                }
            }
            NSString *phoneFomat=  @"";
            NSString *phone = [NSString stringWithFormat:@"%@",accountEntity.userPhoneNo];
            if (phone.length != 0 && phone.length != 1) {
                for (int i=0; i<phone.length; i++) {
                    if (i == 0 || i == phone.length-1) {
                        phoneFomat = [phoneFomat stringByAppendingString:[NSString stringWithFormat:@"%c",[phone characterAtIndex:i]]];
                    }else{
                        phoneFomat = [phoneFomat stringByAppendingString:@"*"];
                    }
                }
            }
            accountEntity.userHideCard = cardFomat;
            accountEntity.userHidePhone = phoneFomat;
            //保存教师账户
            [[NSuserDefaultManager share] saveCurrentAccount:accountEntity];
            _statusBlock(requestStatusSucess);
        }else{
            _statusBlock(requestStatusSucess);
        }

    } failed:^(NSString *result) {
        _statusBlock(requestStatusFailed);
    }];
}
@end
