//
//  CaseSienceModel.m
//  huiyun
//
//  Created by MacAir on 2017/11/1.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "CaseSienceModel.h"

@implementation CaseSienceModel
//@property (strong, nonatomic) NSNumber *recordId;
//@property (strong, nonatomic) NSString *caseSienceTitle;
- (void)setCaseSienceTitle:(NSString *)caseSienceTitle{
    if ([caseSienceTitle isKindOfClass:[NSNull class]]) {
        _caseSienceTitle = @"暂无";
    }else{
        _caseSienceTitle = caseSienceTitle;
    }
}
//@property (strong, nonatomic) NSString *caseSienceStatus;
- (void)setCaseSienceStatus:(NSString *)caseSienceStatus{
    if ([caseSienceStatus isKindOfClass:[NSNull class]]) {
        _caseSienceStatus = @"暂无";
    }else if ([caseSienceStatus isEqualToString:@"waiting_approval"]){
        _caseSienceStatus = @"待审核";
    }else if ([caseSienceStatus isEqualToString:@"approved"]){
        _caseSienceStatus = @"已通过";
    }else{
        _caseSienceStatus = @"未通过";
    }
}
//@property (strong, nonatomic) NSString *caseSienceHeader;
- (void)setCaseSienceHeader:(NSString *)caseSienceHeader{
    if ([caseSienceHeader isKindOfClass:[NSNull class]]) {
        _caseSienceHeader = @"暂无";
    }else{
        _caseSienceHeader = caseSienceHeader;
    }
}
//@property (strong, nonatomic) NSString *caseSienceRole;
- (void)setCaseSienceRole:(NSString *)caseSienceRole{
    if ([caseSienceRole isKindOfClass:[NSNull class]]) {
        _caseSienceRole = @"暂无";
    }else{
        _caseSienceRole = caseSienceRole;
    }
}
//@property (strong, nonatomic) NSNumber *caseSienceDate;
- (void)setCaseSienceDate:(NSNumber *)caseSienceDate{
    if ([caseSienceDate isKindOfClass:[NSNull class]]) {
        _caseSienceDate = @0;
    }else{
        _caseSienceDate = caseSienceDate;
    }
}
- (void)setCaseSienceEndDate:(NSNumber *)caseSienceEndDate{
    if ([caseSienceEndDate isKindOfClass:[NSNull class]]) {
        _caseSienceEndDate = @0;
    }else{
        _caseSienceEndDate = caseSienceEndDate;
    }
}
//@property (strong, nonatomic) NSString *caseSienceTeacher;
- (void)setCaseSienceTeacher:(NSString *)caseSienceTeacher{
    if ([caseSienceTeacher isKindOfClass:[NSNull class]]) {
        _caseSienceTeacher = @"暂无";
    }else{
        _caseSienceTeacher = caseSienceTeacher;
    }
}
//@property (strong, nonatomic) NSString *caseSienceComepleteDes;
- (void)setCaseSienceComepleteDes:(NSString *)caseSienceComepleteDes{
    if ([caseSienceComepleteDes isKindOfClass:[NSNull class]]) {
        _caseSienceComepleteDes = @"暂无";
    }else{
        _caseSienceComepleteDes = caseSienceComepleteDes;
    }
}
//@property (strong, nonatomic) NSString *caseSienceAdvice;
- (void)setCaseSienceAdvice:(NSString *)caseSienceAdvice{
    if ([caseSienceAdvice isKindOfClass:[NSNull class]]) {
        _caseSienceAdvice = @"暂无";
    }else{
        _caseSienceAdvice = caseSienceAdvice;
    }
}
//@property (strong, nonatomic) NSString *fileUrl;
- (void)setFileUrl:(NSString *)fileUrl{
    if ([fileUrl isKindOfClass:[NSNull class]]) {
        _fileUrl = @"暂无";
    }else{
        _fileUrl = fileUrl;
    }
}
@end

