//
//  TStudentModel.h
//  yun
//
//  Created by MacAir on 2017/7/18.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TStudentModel : NSObject
@property (strong, nonatomic) NSNumber *TStudentModelId;
@property (strong, nonatomic) NSString *TStudentModelImage;
@property (strong, nonatomic) NSString *TStudentModelName;
@property (strong, nonatomic) NSNumber *TStudentModelXuehao;
@property (strong, nonatomic) NSString *TStudentModelProfessional;
@end
