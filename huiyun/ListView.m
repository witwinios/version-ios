//
//  MenuView.m
//  FastSdkDemo
//
//  Created by MacAir on 2017/3/31.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "ListView.h"

@implementation ListView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _listArray = [NSArray new];
        self.backgroundColor = [UIColor lightGrayColor];
        _menuTable = [[UITableView alloc]initWithFrame:CGRectMake(1, 1, frame.size.width-2, frame.size.height-2)];
        _menuTable.delegate = self;
        _menuTable.dataSource = self;
        [self addSubview:_menuTable];
    }
    return self;
}
#pragma -table协议
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _listArray.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self removeFromSuperview];
    //传值
    _selectBlock(_listArray[indexPath.row],indexPath.row);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"menuCell";
    UITableViewCell *cell;
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.textLabel.text = _listArray[indexPath.row];
        
        cell.textLabel.textAlignment = 1;
    }
    return cell;
}
@end

