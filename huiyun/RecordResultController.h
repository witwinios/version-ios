//
//  RecordResultController.h
//  huiyun
//
//  Created by MacAir on 2017/9/15.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordResultController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *studentSummary;
@property (weak, nonatomic) IBOutlet UITextView *teacherSummary;
@property (weak, nonatomic) IBOutlet UITextView *supervisorSummary;
@property (weak, nonatomic) IBOutlet UIButton *tempBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)tempAction:(id)sender;
- (IBAction)saveAction:(id)sender;

@property (strong, nonatomic) SDepartModel *model;
@end
