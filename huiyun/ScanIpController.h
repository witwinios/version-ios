//
//  CameraViewController.h
//  QunShuo
//
//  Created by 周忠 on 2016/11/16.
//  Copyright © 2016年 周忠. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LoadingView.h"
@interface ScanIpController: UIViewController<CLLocationManagerDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) NSNumber *btnIndex;
@end
