//
//  ExamTestViewController.h
//  huiyun
//
//  Created by Bad on 2018/3/12.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionModel.h"
#import "CheckButton.h"
#import "UIImageView+WebCache.h"
#import "LMJDropdownMenu.h"
#import "MainViewController.h"
#import "CourseQuestionBView.h"
@interface ExamTestViewController : UIViewController<UIScrollViewDelegate,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *Back;
@property (weak, nonatomic) IBOutlet UIButton *Details;
@property (weak, nonatomic) IBOutlet UIButton *Submit;

@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *QuestionTypesLabel;

@property (weak, nonatomic) IBOutlet UIImageView *Left;
@property (weak, nonatomic) IBOutlet UIImageView *Right;
@property (weak, nonatomic) IBOutlet UILabel *PageNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *TimeLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *MainScrollVc;

@property (copy, nonatomic) NSString *fromVC;
@property (strong, nonatomic) NSNumber *recordId;
@property (strong, nonatomic) NSNumber *paperId;
@property (strong, nonatomic) NSNumber *testId;

@property(strong,nonatomic)NSString *courseId;
@property(strong,nonatomic)NSString *str;


@end
