//
//  LeaveCellStyle2.m
//  xiaoyun
//
//  Created by MacAir on 17/2/23.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "LeaveCellStyle2.h"

@implementation LeaveCellStyle2

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leaveReason.returnKeyType = UIReturnKeyDone;
    self.leaveReason.delegate = self;
    self.leaveReason.layer.borderColor = UIColor.grayColor.CGColor;
    self.leaveReason.layer.borderWidth = 5;
}
#pragma -协议
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        [self.leaveReason resignFirstResponder];
        return NO;
    }else{
        return YES;
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"end");
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [self.leaveReason resignFirstResponder];
    return YES;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.leaveReason resignFirstResponder];
}
@end
