//
//  CameraViewController.m
//  QunShuo
//
//  Created by 周忠 on 2016/11/16.
//  Copyright © 2016年 周忠. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import "CameraScanViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "MainViewController.h"
#define QRCodeWidth  260.0   //正方形二维码的边长
#define ScreenW self.view.frame.size.width
#define ScreenH self.view.frame.size.height
@interface ScanIpController ()<AVCaptureMetadataOutputObjectsDelegate>{
    
    AVCaptureSession * session;//输入输出的中间桥梁
    //
    CLLocationManager *locationManager;
    NSString *currentCity;//城市
    NSString *Strlatitude;//精度
    NSString *Strlongitude;//纬度
    //
    LoadingView *loadView;
    //
    UIView *tishiView;
}

@end

@implementation ScanIpController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createNavView];
    [self setupMaskView];
    [self setupScanWindowView];
    [self beginScanning];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
//创建导航视图
-(void)createNavView{
    UIView * myview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    myview.backgroundColor = UIColorFromHex(0x20B2AA);
    
    [self.view addSubview:myview];
    
    UILabel * titleLb = [[UILabel alloc]initWithFrame:CGRectMake(50, 20, ScreenW-100, 44)];
    titleLb.text = @"扫描IP";
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.textColor = [UIColor whiteColor];
    [myview addSubview:titleLb];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [myview addSubview:leftBtn];
    //
    loadView = [[LoadingView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
    loadView.loadTitle = @"设置成功";
    loadView.center = self.view.center;
    //
    tishiView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    tishiView.center = self.view.center;
    tishiView.backgroundColor = [UIColor greenColor];
}

//创建扫描区域之外的阴影视图
-(void)setupMaskView{
    UIColor * color = [UIColor blackColor];
    float alpha = 0.7;
    //    设置扫描区域外部上部的视图
    UIView * topView = [[UIView alloc]init];
    topView.frame = CGRectMake(0, 64, ScreenW, (ScreenH-64-QRCodeWidth)/2.0-64);
    topView.backgroundColor = color;
    topView.alpha = alpha;
    
    //    设置扫描区域外部左边的视图
    UIView * leftView = [[UIView alloc]init];
    leftView.frame = CGRectMake(0, 64+topView.frame.size.height, (ScreenW-QRCodeWidth)/2.0, QRCodeWidth);
    leftView.backgroundColor = color;
    leftView.alpha = alpha;
    
    //    设置扫描区域外部右边的视图
    UIView * rightView = [[UIView alloc]init];
    rightView.frame = CGRectMake((ScreenW-QRCodeWidth)/2.0+QRCodeWidth, 64+topView.frame.size.height, (ScreenW-QRCodeWidth)/2.0, QRCodeWidth);
    rightView.backgroundColor = color;
    rightView.alpha = alpha;
    
    //    设置扫描区域外部底部的视图
    UIView * botView = [[UIView alloc]init];
    botView.frame = CGRectMake(0, 64+QRCodeWidth+topView.frame.size.height, ScreenW, ScreenH-64-QRCodeWidth-topView.frame.size.height);
    botView.backgroundColor = color;
    botView.alpha = alpha;
    
    [self.view addSubview:topView];
    [self.view addSubview:leftView];
    [self.view addSubview:rightView];
    [self.view addSubview:botView];
    
    UILabel * tishiLb = [[UILabel alloc]initWithFrame:CGRectMake(ScreenW/2-125, ScreenH-190, 250, 40)];
    tishiLb.text = @"将二维码/条形码放入框内，即可自动扫描";
    tishiLb.numberOfLines = 0;
    tishiLb.font = [UIFont systemFontOfSize:15];
    tishiLb.textAlignment = NSTextAlignmentCenter;
    tishiLb.textColor = [UIColor whiteColor];
    [self.view addSubview:tishiLb];
    
}
//设置扫描二维码区域的视图
-(void)setupScanWindowView{
    UIView * scanWindow = [[UIView alloc]initWithFrame:CGRectMake((ScreenW-QRCodeWidth)/2.0, (ScreenH-QRCodeWidth-64)/2.0, QRCodeWidth, QRCodeWidth)];
    scanWindow.clipsToBounds = YES;
    [self.view addSubview:scanWindow];
    
    //    设置扫描区域的动画效果
    CGFloat scanNetImagViewH = 241;
    CGFloat scanNetImagViewW = scanWindow.frame.size.width;
    UIImageView * scanNetImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"scan_net"]];
    scanNetImageView.frame = CGRectMake(0, -scanNetImagViewH, scanNetImagViewW, scanNetImagViewH);
    CABasicAnimation * scanNetAnimation = [CABasicAnimation animation];
    scanNetAnimation.keyPath = @"transform.translation.y";
    scanNetAnimation.byValue = @(QRCodeWidth);
    scanNetAnimation.duration = 1.0;
    scanNetAnimation.repeatCount = MAXFLOAT;
    [scanNetImageView.layer addAnimation:scanNetAnimation forKey:nil];
    [scanWindow addSubview:scanNetImageView];
    
    //    设置扫描区域的四个角的边框
    CGFloat buttonWH = 18;
    
    UIButton * topLeft = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, buttonWH, buttonWH)];
    [topLeft setImage:[UIImage imageNamed:@"scan_1"] forState:UIControlStateNormal];
    [scanWindow addSubview:topLeft];
    
    UIButton * topRight = [[UIButton alloc]initWithFrame:CGRectMake(QRCodeWidth-buttonWH, 0, buttonWH, buttonWH)];
    [topRight setImage:[UIImage imageNamed:@"scan_2"] forState:UIControlStateNormal];
    [scanWindow addSubview:topRight];
    
    UIButton * bottomLeft = [[UIButton alloc]initWithFrame:CGRectMake(0, QRCodeWidth-buttonWH, buttonWH, buttonWH)];
    [bottomLeft setImage:[UIImage imageNamed:@"scan_3"] forState:UIControlStateNormal];
    [scanWindow addSubview:bottomLeft];
    
    UIButton * bottomRight = [[UIButton alloc]initWithFrame:CGRectMake(QRCodeWidth-buttonWH, QRCodeWidth-buttonWH, buttonWH, buttonWH)];
    [bottomRight setImage:[UIImage imageNamed:@"scan_4"] forState:UIControlStateNormal];
    [scanWindow addSubview:bottomRight];
}
//开始扫描二维码
-(void)beginScanning{
    //获取摄像设备
    AVCaptureDevice * device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    //    创建输入流
    AVCaptureDeviceInput * input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    if(!input)return;
    AVCaptureMetadataOutput * output = [[AVCaptureMetadataOutput alloc]init];
    //    注意：有效的扫描区域，定位是以设置的右顶点为原点。屏幕宽所在的那条线为y轴，屏幕高所在额线为x轴
    CGFloat x = ((ScreenH -QRCodeWidth - 64)/2.0)/ScreenH;
    CGFloat y = ((ScreenH -QRCodeWidth)/2.0)/ScreenH;
    CGFloat width = QRCodeWidth/ScreenH;
    CGFloat height = QRCodeWidth/ScreenW;
    
    output.rectOfInterest = CGRectMake(x, y, width, height);
    
    //    设置代理在主线程里刷新
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    //  初始化链接对象
    session = [[AVCaptureSession alloc]init];
    //    高质量采集率
    [session setSessionPreset:AVCaptureSessionPresetHigh];
    [session addInput:input];
    [session addOutput:output];
    
    //    设置扫描支持的编码格式
    output.metadataObjectTypes=@[AVMetadataObjectTypeQRCode,AVMetadataObjectTypeEAN13Code,AVMetadataObjectTypeEAN8Code,AVMetadataObjectTypeCode128Code];
    
    AVCaptureVideoPreviewLayer * layer = [AVCaptureVideoPreviewLayer layerWithSession:session];
    layer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    layer.frame = self.view.layer.bounds;
    [self.view.layer insertSublayer:layer atIndex:0];
    //    开始捕获
    [session startRunning];
}
#pragma mark 扫描协议方法
-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (metadataObjects.count>0) {
        //停止扫描
        [session stopRunning];
        static SystemSoundID shake_sound_male_id = 0;
        //播放声音
        NSString * path = [[NSBundle mainBundle]pathForResource:@"5383" ofType:@"wav"];
        if (path) {
            AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:path]),&shake_sound_male_id);
            AudioServicesPlaySystemSound(shake_sound_male_id);
        }
        AudioServicesPlaySystemSound(shake_sound_male_id);
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex : 0 ];
        //扫描信息结果
        NSLog(@"扫描结果%@",metadataObject.stringValue);
       NSString *hosName = [[Maneger parseJSONStringToNSDictionary:metadataObject.stringValue] objectForKey:@"organizationName"];
        HospitalEntity *hosEntity = [HospitalEntity new];
        hosEntity.hosName = hosName;
        [[NSuserDefaultManager share] saveHospital:hosEntity];
        
        //字符串分割
       NSString *ipStr = [[Maneger parseJSONStringToNSDictionary:metadataObject.stringValue] objectForKey:@"url"];
        NSArray *ipArray = [ipStr componentsSeparatedByString:@"?code="];
        //判断扫描是否正确
        if (ipArray.count == 2) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            //拼接URL
            [defaults setObject:[NSString stringWithFormat:@"%@/witwin-ctts-web",ipArray[0] ]forKey:@"scanIP"];
            NSLog(@"扫描:%@",[NSString stringWithFormat:@"%@/witwin-ctts-web",ipArray[0] ]);
            [defaults setObject:ipArray[0] forKey:@"simpleIp"];
            [defaults setObject:ipArray[1] forKey:@"scanCODE"];
            NSLog(@"scanCode=%@",ipArray[1]);
            [defaults synchronize];
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"设置成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];

        }else{
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"设置失败，请重新扫描!" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }

    }
}


#pragma -Action
-(void)leftBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
