//
//  ActivityRecordController.m
//  huiyun
//
//  Created by MacAir on 2017/9/20.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import "ActivityRecordController.h"
#import "THDatePickerView.h"
@interface ActivityRecordController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,ZCAssetsPickerViewControllerDelegate>
{
    NSString *current_date;
    UIView *btnBack;
    
    UIView *toolbar;
    
    UIAlertController *alert;
    NSMutableArray *FileArray;
}
@property (weak, nonatomic) THDatePickerView *dateView;
@property (strong, nonatomic) UIButton *btn;

@end

@implementation ActivityRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    current_date = @"";
    if ([SubjectTool share].subjectType == subjectTypeUpdate) {
        self.oneField.text = self.myModel.caseActivityName;
        self.twoField.text = self.myModel.casePersion;
        self.threeField.text = [NSString stringWithFormat:@"%@",self.myModel.caseInternal];
        self.contentTextview.text = self.myModel.caseActivityContent;
        if (self.myModel.caseDate.intValue == 0) {
            current_date = @"";
        }else{
            current_date = [NSString stringWithFormat:@"%@",self.myModel.caseDate];
            [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:self.myModel.caseDate.stringValue] forState:0];
        }
    }
    toolbar = [[UIView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 40)];
    toolbar.backgroundColor = UIColorFromHex(0x20B2AA);
    [self.view addSubview:toolbar];
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(Swidth-60, 5, 50, 30);
    [doneBtn setTitle:@"完成" forState:0];
    [doneBtn addTarget:self action:@selector(hideKb:) forControlEvents:UIControlEventTouchUpInside];
    [toolbar addSubview:doneBtn];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (doneButtonshow:) name: UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneButtonHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.btn = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.btn.backgroundColor = [UIColor blackColor];
    self.btn.hidden = YES;
    self.btn.alpha = 0.5;
    [self.view addSubview:self.btn];
    self.dateBtn.layer.cornerRadius = 4;
    self.dateBtn.layer.masksToBounds = YES;
    self.dateBtn.titleLabel.textAlignment = 0;
    self.saveBtn.layer.cornerRadius = 4;
    self.saveBtn.layer.masksToBounds = YES;
    THDatePickerView *dateView = [[THDatePickerView alloc]initWithFrame:CGRectMake(0, Sheight, Swidth, 300) andSureBlock:^(NSString *date, NSString *longDate) {
        self.btn.hidden = YES;
        [self.dateBtn setTitle:[[Maneger shareObject] timeFormatter1:longDate] forState:0];
        current_date = longDate;
    } andCancelBlock:^{
        self.btn.hidden = YES;
    } dateType:DateTypeDay];
    [self.view addSubview:dateView];
    self.dateView = dateView;
    FileArray=[NSMutableArray array];
    [self.FileBtn addTarget:self action:@selector(CourseOtherBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self setUI];
}
- (void)setUI{
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    UIImageView *backNavigation = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Swidth, 64)];
    backNavigation.backgroundColor = UIColorFromHex(0x20B2AA);
    backNavigation.userInteractionEnabled = YES;
    [self.view addSubview:backNavigation];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 29.5, 80, 25);
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10, 0, 55);
    [leftBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backNavigation addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, 200, 25)];
    titleLab.center = CGPointMake(backNavigation.center.x, leftBtn.center.y);
    titleLab.textAlignment = 1;
    titleLab.font = [UIFont systemFontOfSize:20];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.text = @"活动记录";
    [backNavigation addSubview:titleLab];
    
    self.threeField.keyboardType = UIKeyboardTypeNumberPad;
}
- (void)resignFirstRespond{
    [self.oneField resignFirstResponder];
    [self.twoField resignFirstResponder];
    [self.threeField resignFirstResponder];
    [self.contentTextview resignFirstResponder];
}
- (void)hideKb:(UIButton *)btn{
    [self resignFirstRespond];
}
-(void)doneButtonshow: (NSNotification *)notification {
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:duration animations:^{
        [self.view bringSubviewToFront:toolbar];
        toolbar.frame = CGRectMake(0, Sheight-rect.size.height-toolbar.frame.size.height, Swidth, toolbar.frame.size.height);
    }];
}
- (void)doneButtonHide:(NSNotification *)notification{
    // 取得键盘的动画时间，这样可以在视图上移的时候更连贯
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        toolbar.frame = CGRectMake(0, Sheight, Swidth, toolbar.frame.size.height);
    }];
}
- (void)saveAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"名称不能为空~" places:0 toView:nil];
        return;
    }
  
    if ([_threeField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时长不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"内容不能为空~" places:0 toView:nil];
        return;
    }
    [MBProgressHUD showHUDAndMessage:@"创建中~" toView:nil];
    SdepartRecordController *departVC = [SdepartRecordController shareObject];
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentActivityRecords",SimpleIp];
    
    NSDictionary *params = @{@"activityName":_oneField.text,@"activityTime":current_date,@"duration":_threeField.text,@"organizerFullName":_twoField.text,@"description":_contentTextview.text,@"phaseScheduleActivityRequirementId":self.model.CaseReuestModelRequirementId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
    
    [RequestTools RequestWithURL:requestUrl Method:@"post" Params:params Success:^(NSDictionary *result) {
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popLast) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
        }else{
            [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"失败~" places:0 toView:nil];
    }];
    
}
- (void)updateAction{
    if ([_oneField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"名称不能为空~" places:0 toView:nil];
        return;
    }
    if ([_twoField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"主讲人不能为空~" places:0 toView:nil];
        return;
    }
    if ([_threeField.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时长不能为空~" places:0 toView:nil];
        return;
    }
    if ([current_date isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"时间不能为空~" places:0 toView:nil];
        return;
    }
    if ([_contentTextview.text isEqualToString:@""]) {
        [MBProgressHUD showToastAndMessage:@"内容不能为空~" places:0 toView:nil];
        return;
    }
    [MBProgressHUD showHUDAndMessage:@"修改中~" toView:nil];
    SdepartRecordController *departVC = [SdepartRecordController shareObject];
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@/webappv2/studentActivityRecords/%@",SimpleIp,self.myModel.recordId];
    
    NSDictionary *params = @{@"activityName":_oneField.text,@"activityTime":current_date,@"duration":_threeField.text,@"organizerFullName":_twoField.text,@"description":_contentTextview.text,@"phaseScheduleActivityRequirementId":self.myModel.requestmentId,@"studentClinicalRotationRecordId":departVC.departModel.SDepartRecordId};
    
    [RequestTools RequestWithURL:requestUrl Method:@"put" Params:params Success:^(NSDictionary *result) {
        if ([result[@"responseStatus"] isEqualToString:@"succeed"]) {
            [MBProgressHUD showToastAndMessage:@"修改成功~" places:0 toView:nil];
            [[NSRunLoop currentRunLoop] addTimer:[NSTimer timerWithTimeInterval:1 target:self selector:@selector(popAc) userInfo:nil repeats:NO] forMode:NSRunLoopCommonModes];
        }else{
            [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
        }
    } failed:^(NSString *result) {
        [MBProgressHUD showToastAndMessage:@"修改失败~" places:0 toView:nil];
    }];
    
}
#pragma -action
- (void)popLast{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)popAc{
    [self.navigationController popToViewController:[CaseHistoryController shareObject] animated:YES];
}
- (void)back:(UIButton *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)dateAction:(id)sender {
    self.btn.hidden = NO;
    [self resignFirstRespond];
    [self.dateView show];
}

- (IBAction)saveAction:(id)sender {
    if ([SubjectTool share].subjectType == subjectTypeAdd) {
        [self saveAction];
    }else{
        [self updateAction];
    }
}

-(void)CourseOtherBtnAction{
    
    UIAlertController *AlertView =[UIAlertController alertControllerWithTitle:@"添加附件" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *AddAction = [UIAlertAction actionWithTitle:@"图片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        ZCAssetsPickerViewController *vc=[[ZCAssetsPickerViewController alloc]init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        vc.type = ChooseTypePhoto;
        vc.maximumNumbernMedia = 9;
        vc.delegate = self;
        
        vc.backArr=^(NSArray *arr){
            NSLog(@"arr：%@",arr);
            [FileArray addObjectsFromArray:arr];
            
        };
        
        
        vc.CourseStr=@"新建";
        
        [self presentViewController:nav animated:YES completion:nil];
        
        
        
    }];
    UIAlertAction *EditAction = [UIAlertAction actionWithTitle:@"视频" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
        ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//sourcetype有三种分别是camera，photoLibrary和photoAlbum
        NSArray *availableMedia = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];//Camera所支持的Media格式都有哪些,共有两个分别是@"public.image",@"public.movie"
        ipc.mediaTypes = [NSArray arrayWithObject:availableMedia[1]];//设置媒体类型为public.movie
        [self presentViewController:ipc animated:YES completion:nil];
        ipc.delegate = self;//设置委托
        
        
    }];
    UIAlertAction *BackAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    
    [AlertView addAction:BackAction];
    [AlertView addAction:AddAction];
    [AlertView addAction:EditAction];
    
    [self presentViewController:AlertView animated:YES completion:nil];

}
#pragma mark 视频：

- (NSString *)getCurrentTime{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    //    NSString *str = [NSString stringWithFormat:@"%@mdxx",dateTime];
    //    NSString *tokenStr = [str stringToMD5:str];
    return dateTime;
    
}
- (NSURL *)condenseVideoNewUrl: (NSURL *)url{
    // 沙盒目录
    NSString *docuPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *destFilePath = [docuPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.MOV",[self getCurrentTime]]];
    NSURL *destUrl = [NSURL fileURLWithPath:destFilePath];
    //将视频文件copy到沙盒目录中
    NSFileManager *manager = [NSFileManager defaultManager];
    NSError *error = nil;
    [manager copyItemAtURL:url toURL:destUrl error:&error];
    NSLog(@"压缩前--%.2fk",[self getFileSize:destFilePath]);
    // 播放视频
    /*
     NSURL *videoURL = [NSURL fileURLWithPath:destFilePath];
     AVPlayer *player = [AVPlayer playerWithURL:videoURL];
     AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
     playerLayer.frame = self.view.bounds;
     [self.view.layer addSublayer:playerLayer];
     [player play];
     */
    // 进行压缩
    AVAsset *asset = [AVAsset assetWithURL:destUrl];
    //创建视频资源导出会话
    /**
     NSString *const AVAssetExportPresetLowQuality; // 低质量
     NSString *const AVAssetExportPresetMediumQuality;
     NSString *const AVAssetExportPresetHighestQuality; //高质量
     */
    
    AVAssetExportSession *session = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
    // 创建导出的url
    NSString *resultPath = [docuPath stringByAppendingPathComponent:[NSString stringWithFormat:@"lyhg%@.MOV",[self getCurrentTime]]];
    session.outputURL = [NSURL fileURLWithPath:resultPath];
    // 必须配置输出属性
    session.outputFileType = @"com.apple.quicktime-movie";
    // 导出视频
    [session exportAsynchronouslyWithCompletionHandler:^{
        NSLog(@"压缩后---%.2fk",[self getFileSize:resultPath]);
        NSLog(@"视频导出完成");
        
    }];
    
    return session.outputURL;
}
// 获取视频的大小
- (CGFloat) getFileSize:(NSString *)path
{
    NSFileManager *fileManager = [[NSFileManager alloc] init] ;
    float filesize = -1.0;
    if ([fileManager fileExistsAtPath:path]) {
        NSDictionary *fileDic = [fileManager attributesOfItemAtPath:path error:nil];//获取文件的属性
        unsigned long long size = [[fileDic objectForKey:NSFileSize] longLongValue];
        filesize = 1.0*size/1024;
    }
    return filesize;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    alert=[UIAlertController alertControllerWithTitle:@"文件上传" message:@"您选择的文件正在上传，请等待...." preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert animated:YES completion:nil];
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString*)kUTTypeMovie]) {
        // 如果是视频
        NSURL *url = info[UIImagePickerControllerMediaURL];
        // 获取视频总时长
        CGFloat lengthTime = [self getVideoLength:url];
        NSLog(@"%f",lengthTime);
        // 保存视频至相册 (异步线程)
        //        NSString *urlStr = [url path];
        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //
        //            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(urlStr)) {
        //
        //                UISaveVideoAtPathToSavedPhotosAlbum(urlStr, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
        //            }
        //
        //        });
        
        //压缩视频
        //     NSData *videoData = [NSData dataWithContentsOfURL:[self condenseVideoNewUrl:url]];
        NSData *videoData = [NSData dataWithContentsOfURL:url];
        
        //视频上传
        
        
        NSDictionary *dict = @{@"username":@"syl"};
        
        
        
        NSString *FileUrl=[NSString stringWithFormat:@"%@/files",LocalIP];
        
        [RequestTools RequestUpLoadVideoWithParams:dict URL:FileUrl data:videoData Success:^(NSDictionary *result) {
            
            NSLog(@"%@",result);
            if([[result objectForKey:@"errorCode"]isKindOfClass:[NSNull class]]){
                CourseAffixModel *model=[CourseAffixModel new];
                model.CreatedBy=[[result objectForKey:@"responseBody"]objectForKey:@"createdBy"];
                model.FileFormat=[[result objectForKey:@"responseBody"]objectForKey:@"fileFormat"];
                model.FileId=[[result objectForKey:@"responseBody"]objectForKey:@"fileId"];
                model.FileName=[[result objectForKey:@"responseBody"]objectForKey:@"fileName"];
                model.FileSize=[[result objectForKey:@"responseBody"]objectForKey:@"fileSize"];
                model.FileType=[[result objectForKey:@"responseBody"]objectForKey:@"fileType"];
                model.FileUrl=[[result objectForKey:@"responseBody"]objectForKey:@"fileUrl"];
                model.CreatedTime=[[result objectForKey:@"responseBody"]objectForKey:@"createdTime"];
                model.Description=[[result objectForKey:@"responseBody"]objectForKey:@"description"];
                model.IsPublic=[[result objectForKey:@"responseBody"]objectForKey:@"isPublic"];
                
                [FileArray addObject:model.FileId];
                
                
            }else{
                [MBProgressHUD showToastAndMessage:@"请求错误!" places:0 toView:nil];
            }
            NSLog(@"dataArrayCount=%lu",(unsigned long)FileArray.count);
            
        } faild:^(NSString *result) {
            NSLog(@"请求错误");
            NSLog(@"%@",result);
        } progress:^(NSProgress *progress) {
            NSLog(@"%f",progress.fractionCompleted);
            
            
            if (progress.fractionCompleted == 1) {
                alert.view.hidden=YES;
            }
            
            
        }];
    }
    
    
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
// 获取视频时间
- (CGFloat) getVideoLength:(NSURL *)URL
{
    AVURLAsset *avUrl = [AVURLAsset assetWithURL:URL];
    CMTime time = [avUrl duration];
    int second = ceil(time.value/time.timescale);
    return second;
}
#pragma mark 图片保存完毕的回调
- (void) image: (UIImage *) image didFinishSavingWithError:(NSError *) error contextInfo: (void *)contextIn {
    NSLog(@"照片保存成功");
}

#pragma mark 视频保存完毕的回调
- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextIn {
    if (error) {
        NSLog(@"保存视频过程中发生错误，错误信息:%@",error.localizedDescription);
    }else{
        NSLog(@"视频保存成功.");
    }
}


@end
