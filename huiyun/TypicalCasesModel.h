//
//  TypicalCasesModel.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/10.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TypicalCasesModel : NSObject

/*
 病例 ID：
 病例 Name:
 病种 diseaseICDName
 病例记录个数 medicalRecordsNum
 病例记录ID recordId
 
 判断是否被选中

*/
@property(strong,nonatomic)NSString *TypicalCasesName;
@property(strong,nonatomic)NSNumber *TypicalCasesId;
@property(strong,nonatomic)NSString *TypicalCasesDiseaseICDName;
@property(strong,nonatomic)NSNumber *TypicalCasesMedicalRecordsNum;
@property(strong,nonatomic)NSNumber *TypicalCasesRecordId;

@property(strong,nonatomic)NSNumber *TypicalCasesBool;

@property (nonatomic, assign) BOOL isCheck;


@end
