//
//  SkillModel.m
//  xiaoyun
//
//  Created by MacAir on 17/2/28.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//


#import "SkillModel.h"

@implementation SkillModel
//@property (strong, nonatomic) NSString *skillStatus;
- (void)setTimeBlock:(NSNumber *)timeBlock{
    if ([timeBlock isKindOfClass:[NSNull class]]) {
        _timeBlock = [NSNumber numberWithInt:0];
    }else{
        _timeBlock = timeBlock;
    }
}
- (void)setSkillStationNums:(NSNumber *)skillStationNums{
    if ([skillStationNums isKindOfClass:[NSNull class]]) {
        _skillStationNums = [NSNumber numberWithInt:0];
    }else{
        _skillStationNums = skillStationNums;
    }
//    arranged
}
- (void)setSkillStatus:(NSString *)skillStatus{
    if ([skillStatus isKindOfClass:[NSNull class]]) {
        _skillStatus = @"暂无";
    }else if ([skillStatus isEqualToString:@"PLANNING"] || [skillStatus isEqualToString:@"planning"]){
        _skillStatus = @"计划中";
    }else if ([skillStatus isEqualToString:@"STARTED"] || [skillStatus isEqualToString:@"started"]){
        _skillStatus = @"已开始";
    }else if ([skillStatus isEqualToString:@"COMPLETED"] || [skillStatus isEqualToString:@"completed"]){
        _skillStatus = @"已完成";
    }else if ([skillStatus isEqualToString:@"RELEASED"] || [skillStatus isEqualToString:@"released"]){
        _skillStatus = @"已发布";
    }else if ([skillStatus isEqualToString:@"PREORDERED"] || [skillStatus isEqualToString:@"preordered"]){
        _skillStatus = @"已排考";
    }else if ([skillStatus isEqualToString:@"ARRANGE"] || [skillStatus isEqualToString:@"arrange"]){
        _skillStatus = @"已安排";
    }
}
//@property (strong, nonatomic) NSString *skillName;
- (void)setSkillName:(NSString *)skillName{
    if ([skillName isKindOfClass:[NSNull class]]) {
        _skillName = @"暂无";
    }else{
        _skillName = skillName;
    }
}
//@property (strong, nonatomic) NSString *skillDes;
- (void)setSkillDes:(NSString *)skillDes{
    if ([skillDes isKindOfClass:[NSNull class]]) {
        _skillDes = @"暂无";
    }else{
        _skillDes = skillDes;
    }
}
//@property (strong, nonatomic) NSString *skillCategory;
- (void)setSkillCategory:(NSString *)skillCategory{
    if ([skillCategory isKindOfClass:[NSNull class]]) {
        _skillCategory = @"暂无";
    }else{
        _skillCategory = skillCategory;
    }
}
//@property (strong, nonatomic) NSString *skillSubkect;
- (void)setSkillSubject:(NSString *)skillSubject{
    if ([skillSubject isKindOfClass:[NSNull class]]) {
        _skillSubject = @"暂无";
    }else{
        _skillSubject = skillSubject;
    }
}
//@property (strong, nonatomic) NSNumber *skillStartTime;
- (void)setSkillStartTime:(NSNumber *)skillStartTime{
    if ([skillStartTime isKindOfClass:[NSNull class]]) {
        _skillStartTime = [NSNumber numberWithInt:0];
    }else{
        _skillStartTime = skillStartTime;
    }
}
//@property (strong, nonatomic) NSNumber *skillEndTime;
- (void)setSkillEndTime:(NSNumber *)skillEndTime{
    if ([skillEndTime isKindOfClass:[NSNull class]]) {
        _skillEndTime = [NSNumber numberWithInt:0];
    }else{
        _skillEndTime = skillEndTime;
    }
}
//@property (strong, nonatomic) NSNumber *createTime;
- (void)setCreateTime:(NSNumber *)createTime{
    if ([createTime isKindOfClass:[NSNull class]]) {
        _createTime = [NSNumber numberWithInt:0];
    }else{
        _createTime = createTime;
    }
}
//@property (strong, nonatomic) NSString *createPerson;
- (void)setCreatePerson:(NSString *)createPerson{
    if ([createPerson isKindOfClass:[NSNull class]]) {
        _createPerson = @"暂无";
    }else{
        _createPerson = createPerson;
    }
}
//
@end
