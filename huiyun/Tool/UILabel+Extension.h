//
//  UILabel+Extension.h
//  huiyun
//
//  Created by Mr.Wang on 2018/1/30.
//  Copyright © 2018年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extension)
-(CGFloat)getSpaceLabelHeight:(NSString *)str withWidh:(CGFloat)width;
-(CGFloat)getSpaceLabelWidth:(NSString *)str withHight:(CGFloat)hight;
@end
