//
//  CellStyle3.m
//  yun
//
//  Created by MacAir on 2017/6/22.
//  Copyright © 2017年 gensee. All rights reserved.
//

#import "CellStyle3.h"

@implementation CellStyle3

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imgBtn.layer.cornerRadius = 5;
    self.imgBtn.layer.masksToBounds = YES;
    self.imgBtn.layer.borderWidth = 1;
    self.imgBtn.layer.borderColor = [UIColor grayColor].CGColor;
    
}


@end
