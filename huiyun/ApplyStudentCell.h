//
//  ApplyStudentCell.h
//  huiyun
//
//  Created by Mr.Wang on 2017/12/5.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseAffixModel.h"
#import "ApplyStudent.h"
@interface ApplyStudentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ApplyStudentImg;
@property (weak, nonatomic) IBOutlet UILabel *ApplyStudentName;
@property (weak, nonatomic) IBOutlet UIView *ApplyBgVc;

- (void)setProperty:(ApplyStudent *)model;

-(void)setCourseAffix:(CourseAffixModel *)model;






@end
