//
//  PaperRecordController.h
//  huiyun
//
//  Created by MacAir on 2017/9/21.
//  Copyright © 2017年 慧云医疗. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListView.h"
@interface PaperRecordController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *oneField;
@property (weak, nonatomic) IBOutlet UIButton *threeBtn;
@property (weak, nonatomic) IBOutlet UIButton *twoBtn;
@property (strong, nonatomic)UIImagePickerController *imagePickController;

@property (strong, nonatomic)ListView *menuView;

@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (weak, nonatomic) IBOutlet UITextField *fourField;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)threeAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *fiveField;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview;
- (IBAction)twoAction:(id)sender;

- (IBAction)fileAction:(id)sender;
- (IBAction)saveAction:(id)sender;
- (IBAction)dateAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *currentImage;

@property (strong, nonatomic) CaseReuestModel *model;
@property (strong, nonatomic) CaseEssayModel *myModel;
@end
